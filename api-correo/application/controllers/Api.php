<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
   $this->load->model(array('api_model'));
  }

  function index()
  {

  }

  public function correo()
  {
      header("content-type: application/json; charset=utf-8");
      header("Access-Control-Allow-Origin: *");
        $this->load->config('email');
        $this->load->library('user_agent');
        $this->load->helper('string');
        $this->lang->load('form_validation');

        $response = array();
        $valid = true;
        $message = "Lo sentimos, ocurrió un error y su solicitud no pudo ser completada. Por favor inténtelo nuevamente.";
        $target = "";

  		if ($this->agent->is_browser())
  		{
  			$agent = $this->agent->browser().' '.$this->agent->version();
  		}
  		elseif ($this->agent->is_robot())
  		{
  			$agent = $this->agent->robot();
  		}
  		elseif ($this->agent->is_mobile())
  		{
  			$agent = $this->agent->mobile();
  		}
  		else
  		{
  			$agent = "Unidentified User Agent";
  		}
  		$agent .= "|" .  $this->agent->platform();

  		
  			$DateTime = date("Y-m-d H:i:s");

  			$data = array("nombre" => $this->input->post('nombre'),
  				      "tel" => $this->input->post('tel'),
  				      "correo" => $this->input->post('correo'),
  				      "tipo_vehiculo" => $this->input->post('tipo_vehiculo'),
  							"anio" => $this->input->post('anio'),
  							"marca" => $this->input->post('marca'),
  				      "submarca" => $this->input->post('submarca'),
                "cp" => $this->input->post('cp'),
                "tipo_cobertura" => $this->input->post('tipo_cobertura'),
  				      "version" => $this->input->post('version'));
  				      // "ip_address" => $this->input->ip_address(),
  				      // "usr_agent" => $agent);

  			//Stores the prospect.
  			// $this->process_model->insert('leads', $data);
        $insert_lead = $this->api_model->insert_lead(array('lp' => 'asse', 'data' => json_encode($data), 'created_at' => date('Y-m-d H:i:s')));
  			$subject = 'Registro de prospecto';

  			$to = $this->config->item('email_to');

  			// Send email to web master
  			//send_email($this->config->item('email_to'), 'Registro de prospecto', 'frontend/emails/contact_form_admin', $data);
  			send_email('asseseguros@asrg.mx', 'Registro de prospecto', 'frontend/emails/contact_form_admin', $data);
        // send_email('fcedillo@trespuntos.mx', 'Registro de prospecto', 'frontend/emails/contact_form_admin', $data);

  			// Send email to user
  			send_email($this->input->post('correo'), 'Gracias por contactarnos', 'frontend/emails/contact_form_user', $data);

  			$message = "";
  			$response['redirect'] = 'http://www.asseseguros.com/gracias.aspx';
  		// }

  		$response['valid'] = $valid;
  		$response['response'] = $message;
  		$response['target'] = $target;

  		echo(json_encode($response));
  	}
        
 public function correo_ksh() {
     
     header("content-type: application/json; charset=utf-8");
     header("Access-Control-Allow-Origin: *");
     
     $this->load->config('email');
     $this->load->library('user_agent');
     $this->load->helper('string');
     $this->lang->load('form_validation');
     
     $response = array();
     $valid = true;
     $message = "Lo sentimos, ocurrió un error y su solicitud no pudo ser completada. Por favor inténtelo nuevamente.";
     $target = "";
     
     if ($this->agent->is_browser()) { $agent = $this->agent->browser().' '.$this->agent->version(); }
     elseif ($this->agent->is_robot()) { $agent = $this->agent->robot(); }
     elseif ($this->agent->is_mobile()){ $agent = $this->agent->mobile(); }
     else { $agent = "Unidentified User Agent"; }
     $agent .= "|" .  $this->agent->platform();
     
     $data = array();
     $DateTime = date("Y-m-d H:i:s");
     
     $data = array("tipo" => $this->input->post('tipo'),
                        "nombre" => $this->input->post('nombre'),
                        "empresa" => $this->input->post('empresa'),
                        "telefono" => $this->input->post('telefono'),
                        "correo" => $this->input->post('correo'),
                        "ciudad" => $this->input->post('ciudad'),
                        "mensaje" => $this->input->post('mensaje')
                        );
     
     //Stores the prospect.
     $insert_lead = $this->api_model->insert_lead(array('contact_form'=>'contacto','status'=>1,'readed'=>0, 'data' => json_encode($data), 'created_at' => date('Y-m-d H:i:s')));
//     $subject = 'Registro de prospecto';

     $data = $_POST;
     $data['array'] = $_POST;
     $data['titulo'] = 'Ksh';
     $data['logo'] = 'http://mrwasabiserver.com/webpage/ksh17/public/theme/images/logo.png';
     $data['telefono'] = '';
     
     $subject = 'Cotizaciones o información de fichas técnicas';
// $insert_lead = $this->api_model->insert_lead(array('lp' => 'beepack', 'data' => json_encode($data), 'created_at' => date('Y-m-d H:i:s')));
    $to = $this->config->item('email_to');
    // Send email to web master
    //send_email($this->config->item('email_to'), 'Registro de prospecto', 'frontend/emails/contact_form_admin', $data);
    // send_email('asseseguros@asrg.mx', 'Registro de prospecto', 'frontend/emails/contact_form_admin', $data);
    // send_email('felipe@mrwasabi.mx,ingrid.legorreta@gmail.com,josemanuel@mrwasabi.mx', 'Registro de prospecto', 'frontend/emails/contact_form_admin_generic', $data,'no-reply@mrwasabi.mx','Mr Wasabi');
     send_email('felipe@mrwasabi.mx,benjamin@mrwasabi.mx', 'Cotizaciones Ksh', 'frontend/emails/contact_form_admin_generic', $data,'no-reply@mrwasabi.mx','Mr Wasabi');

     $message = "";
     $response['redirect'] = 'http://beepack.mx/gracias.html';
     $response['valid'] = $valid;
     $response['response'] = $message;
     $response['target'] = $target;
     echo(json_encode($response));
     
     }
     
      public function contacto_ksh() {
     
     header("content-type: application/json; charset=utf-8");
     header("Access-Control-Allow-Origin: *");
     
     $this->load->config('email');
     $this->load->library('user_agent');
     $this->load->helper('string');
     $this->lang->load('form_validation');
     
     $response = array();
     $valid = true;
     $message = "Lo sentimos, ocurrió un error y su solicitud no pudo ser completada. Por favor inténtelo nuevamente.";
     $target = "";
     
     if ($this->agent->is_browser()) { $agent = $this->agent->browser().' '.$this->agent->version(); }
     elseif ($this->agent->is_robot()) { $agent = $this->agent->robot(); }
     elseif ($this->agent->is_mobile()){ $agent = $this->agent->mobile(); }
     else { $agent = "Unidentified User Agent"; }
     $agent .= "|" .  $this->agent->platform();
     
     $data = array();
     $DateTime = date("Y-m-d H:i:s");
     
     $data = $_POST;
     $data['array'] = $_POST;
     $data['titulo'] = 'Ksh';
     $data['logo'] = 'http://mrwasabiserver.com/webpage/ksh17/public/theme/images/logo.png';
     $data['telefono'] = '';
     
     $subject = 'Contacto KSH';
// $insert_lead = $this->api_model->insert_lead(array('lp' => 'beepack', 'data' => json_encode($data), 'created_at' => date('Y-m-d H:i:s')));
    $to = $this->config->item('email_to');
    // Send email to web master
    //send_email($this->config->item('email_to'), 'Registro de prospecto', 'frontend/emails/contact_form_admin', $data);
    // send_email('asseseguros@asrg.mx', 'Registro de prospecto', 'frontend/emails/contact_form_admin', $data);
    // send_email('felipe@mrwasabi.mx,ingrid.legorreta@gmail.com,josemanuel@mrwasabi.mx', 'Registro de prospecto', 'frontend/emails/contact_form_admin_generic', $data,'no-reply@mrwasabi.mx','Mr Wasabi');
     send_email('felipe@mrwasabi.mx,benjamin@mrwasabi.mx', 'Contacto KSH', 'frontend/emails/contact_form_admin_generic', $data,'no-reply@mrwasabi.mx','Mr Wasabi');

     $message = "";
     $response['redirect'] = 'http://beepack.mx/gracias.html';
     $response['valid'] = $valid;
     $response['response'] = $message;
     $response['target'] = $target;
     echo(json_encode($response));
     
     }
     
    public function comprobante_venta() 
    {
     header("content-type: application/json; charset=utf-8");
     header("Access-Control-Allow-Origin: *");
     
     $this->load->config('email');
     $this->load->library('user_agent');
     $this->load->helper('string');
     $this->lang->load('form_validation');
     
     $response = array();
     $valid = true;
     $message = "Lo sentimos, ocurrió un error y su solicitud no pudo ser completada. Por favor inténtelo nuevamente.";
     $target = "";
     
     if ($this->agent->is_browser()) { $agent = $this->agent->browser().' '.$this->agent->version(); }
     elseif ($this->agent->is_robot()) { $agent = $this->agent->robot(); }
     elseif ($this->agent->is_mobile()){ $agent = $this->agent->mobile(); }
     else { $agent = "Unidentified User Agent"; }
     $agent .= "|" .  $this->agent->platform();
     
     $data = array();
     $DateTime = date("Y-m-d H:i:s");
     
     
     $bc = $_POST['barcode'];
     $mj = $_POST['comentario'];
     $ie =  $_POST['id_envio'];
     $tp = $_POST['tipopago'];
     $ed = $_POST['estado_nombre'];
     $cd = $_POST['ciudad_nombre'];
     $dir = $_POST['dir_envio'];
     
     if($dir == 2){         
     $ed2 = $_POST['estado_nombre_dos'];
     $cd2 = $_POST['ciudad_nombre_dos'];
     }
     
//     $carr = array();
//     $cliente = array();
     $carr = unserialize($_POST['carrito']);
     $coti = unserialize($_POST['cotizacion']);
     $cliente = unserialize($_POST['cliente']);
     
     $data = $_POST;
    // $data['array'] = $_POST;
     
     $data['barcode'] = $bc;
     $data['comentario'] = $mj;
     $data['id_envio'] = $ie;
     $data['tipopago'] = $tp;
     $data['estado_nombre'] =  $ed;
     $data['ciudad_nombre'] = $cd;
     $data['dir_envio'] = $dir;
     if($dir == 2){
     $data['estado_nombre_dos'] = $ed2;
     $data['ciudad_nombre_dos'] = $cd2;
     }
     
     $data['carrito'] = $carr;
     $data['cotizacion'] = $coti;
     $data['cliente'] = $cliente;
     $data['titulo'] = 'Ksh';
     $data['logo'] = 'http://mrwasabiserver.com/webpage/ksh17/public/theme/images/logo.png';
     $data['telefono'] = '';     
     $subject = 'Comprobante de Venta KSH';
     
     
     $mail_cliente="";
      if(!empty($cliente)):
         foreach($cliente as $cs=>$client):
          $mail_cliente = $client['email'];
         endforeach; else:
         endif;
//      var_dump($data);
     // $insert_lead = $this->api_model->insert_lead(array('lp' => 'beepack', 'data' => json_encode($data), 'created_at' => date('Y-m-d H:i:s')));
//     $to = $this->config->item('email_to');
     //$to = $mail_cliente;
  //send_email('felipe@mrwasabi.mx,josemanuel@mrwasabi.mx,'.$mail_cliente.'', 'Comprobante de Venta KSH', 'frontend/emails/comprobante_form_venta', $data,'no-reply@mrwasabi.mx','Mr Wasabi');
     send_email($mail_cliente, 'Comprobante de Venta KSH', 'frontend/emails/comprobante_form_venta', $data,'no-reply@mrwasabi.mx','Mr Wasabi');
     $message = "";
     $response['redirect'] = 'http://beepack.mx/gracias.html';
     $response['valid'] = $valid;
     $response['response'] = $message;
     $response['target'] = $target;
     echo(json_encode($response));

        


    }
    public function responde_correo_mensaje()
    {
     header("content-type: application/json; charset=utf-8");
     header("Access-Control-Allow-Origin: *");
     
     $this->load->config('email');
     $this->load->library('user_agent');
     $this->load->helper('string');
     $this->lang->load('form_validation');
     
     $response = array();
     $valid = true;
     $message = "Lo sentimos, ocurrió un error y su solicitud no pudo ser completada. Por favor inténtelo nuevamente.";
     $target = "";
     
     if ($this->agent->is_browser()) { $agent = $this->agent->browser().' '.$this->agent->version(); }
     elseif ($this->agent->is_robot()) { $agent = $this->agent->robot(); }
     elseif ($this->agent->is_mobile()){ $agent = $this->agent->mobile(); }
     else { $agent = "Unidentified User Agent"; }
     $agent .= "|" .  $this->agent->platform();
     
     $data = array();
     $DateTime = date("Y-m-d H:i:s");
     
     $data = $_POST;
     $data['array'] = $_POST;
     $data['titulo'] = 'Ksh';
     $data['logo'] = 'http://mrwasabiserver.com/webpage/ksh17/public/theme/images/logo.png';
     $data['telefono'] = '';
     
     $subject = 'Contacto KSH';
// $insert_lead = $this->api_model->insert_lead(array('lp' => 'beepack', 'data' => json_encode($data), 'created_at' => date('Y-m-d H:i:s')));
    $to = $this->config->item('email_to');
    // Send email to web master
    //send_email($this->config->item('email_to'), 'Registro de prospecto', 'frontend/emails/contact_form_admin', $data);
    // send_email('asseseguros@asrg.mx', 'Registro de prospecto', 'frontend/emails/contact_form_admin', $data);
    // send_email('felipe@mrwasabi.mx,ingrid.legorreta@gmail.com,josemanuel@mrwasabi.mx', 'Registro de prospecto', 'frontend/emails/contact_form_admin_generic', $data,'no-reply@mrwasabi.mx','Mr Wasabi');
     send_email($this->input->post('correo'), $this->input->post('asunto'), 'frontend/emails/contact_form_admin_generic', $data,'no-reply@mrwasabi.mx','ksh');

     $message = "";
     $response['redirect'] = 'http://beepack.mx/gracias.html';
     $response['valid'] = $valid;
     $response['response'] = $message;
     $response['target'] = $target;
     //echo(json_encode($response));
     echo json_encode(array('valid' => true, 'mensaje' => 'Correo enviado correctamente'));
    }
              
    public function recupera_password_ksh()
    {
         header("content-type: application/json; charset=utf-8");
         header("Access-Control-Allow-Origin: *");
         //$correo = base64_decode($correo);

        $this->load->config('email');
        $this->load->library('user_agent');
        $this->load->helper('string');
        $this->lang->load('form_validation');

        $response = array();
        $valid = true;
        $message = "Lo sentimos, ocurrió un error y su solicitud no pudo ser completada. Por favor inténtelo nuevamente.";
        $target = "";
        if ($this->agent->is_browser()){  $agent = $this->agent->browser().' '.$this->agent->version();}
        elseif ($this->agent->is_robot()){$agent = $this->agent->robot();}
        elseif ($this->agent->is_mobile()){$agent = $this->agent->mobile();}
        else{$agent = "Unidentified User Agent";}
        $agent .= "|".$this->agent->platform();
        $DateTime = date("Y-m-d H:i:s");
        $correo =  $this->input->post('correo');
        //Stores the prospect.
        $id_url='';
        $this->data['recupera'] = $this->api_model->get_cliente($correo);
//        var_dump($this->data['recupera']->IdCliente);
        
        if($this->data['recupera']):
            foreach ($this->data['recupera'] as $re):
            $id_url = $this->data['recupera']->IdCliente;
            endforeach;
        
        //var_dump($id_url);
        $data = array();
        $info  = array();
        $info['instrucciones'] = 'Haz click en el siguiente enlace para cambiar tu contraseña';
        $info['link'] = "<a target='_blank' href='http://mrwasabiserver.com/webpage/ksh17/cambia/{$id_url}'>http://mrwasabiserver.com/webpage/ksh17/cambia/{$id_url}</a>";
        $data['array'] = $info;
        $data['titulo'] = 'Kuroda soluciones hidráuilicas';
        $data['logo'] = 'http://mrwasabiserver.com/webpage/ksh17/public/theme/images/logo.png';
        $data['telefono'] = '';


        $subject = 'Contacto de Ayuda';
        // $insert_lead = $this->api_model->insert_lead(array('lp' => 'beepack', 'data' => json_encode($data), 'created_at' => date('Y-m-d H:i:s')));
        $to = $this->config->item('email_to');

        // Send email to web master
        //send_email($correo, $subject, 'frontend/emails/contact_form_admin_generic', $data,'no-reply@mrwasabi.mx','KSH');
        send_email($correo,$subject, 'frontend/emails/contact_form_admin_generic', $data,'no-reply@mrwasabi.mx','ksh');
       // $message = "Si requieres de una respuesta inmediata por favor contáctanos por teléfono o Whatsapp al +";
        $response['redirect'] = 'http://beepack.mx/gracias.html';
        $response['valid'] = $valid;
        $response['response'] = $message;
        $response['target'] = $target;
        $response['correo'] = $correo;
//        $response['link'] = $data['link'];
        //echo(json_encode($response));
          redirect('http://mrwasabiserver.com/webpage/ksh17', 'refresh');             
          else:
              
                  //$this->session->set_flashdata('msg_recupera', '<div class="alert alert-danger text-center">El correo que usted ingreso no se encuentra registrado.</div>');
              $_SESSION['error'] = '<div class="alert alert-danger text-center">El correo que usted ingreso no se encuentra registrado.</div>';
                  redirect('http://mrwasabiserver.com/webpage/ksh17/recupera/error');     
         endif;
    }
    
}
