<?php if (! defined('BASEPATH')) exit('No direct script access');

/**
 * Sending emails templates
 *
 * @author
 * @version    1.0 01/01/2014
 * @version    1.1 03/02/2015
 *                 -added blank copy email
 * @link
 *
 * @param string $email: Recipient email address.
 * @param string $subject: Email subject.
 * @param string $template: File name email template.
 * @param array $params: Variables to use on te template.
 * @param string $email_from: Sender email.
 * @param string $name_from: Sender name.
 * @param string $blank_to: Recipient blank copy email.
 * @param string $email_reply_to: Sender email reply to.
 * @param string $name_reply_to: Sender name reply to.
 */
//function send_email($email, $subject, $template, $params = false, $email_from = false, $name_from = false, $blank_to, $email_reply_to = false, $name_reply_to = false)
function send_email($email, $subject, $template, $email_data = false, $email_from = false, $name_from = false, $blank_to = false, $email_reply_to = false, $name_reply_to = false)
{
	$CI = & get_instance();
	$CI->load->config('email');
	//
	// $NewContent = $CI->load->view($template, $email_data, TRUE);
	// if (isset($_SERVER) && isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST'] == "localhost")
	// {
	// 	$to = "notificaciones.tp@gmail.com";
	// }
	//
	// $Header = "MIME-Version: 1.0" . "\r\n";
	// $Header .= "Content-type:text/html;charset=UTF-8" . "\r\n";
	// $Header .= "From: " . $name_from . " <" . $email_from . ">\r\n".
	// 		 "Bcc: notificaciones.tp@gmail.com\r\n";
	// $result = mail($email, $subject, $NewContent,$Header);

	//print_r(error_get_last());
	// return $result;
	//
	// // Send mail with Mailgun
	$result = false;
	if (!$email_from)
	{
		$email_from = $CI->config->item('email_from');
	}

	if (!$name_from)
	{
		$name_from = $CI->config->item('name_from');
	}

	// Email to
	$email_to = (is_array($email) ? implode(",", $email) : $email);

	// BCC
	$email_bcc = (is_array($CI->config->item('email_bcc')) ? implode(",", $CI->config->item('email_bcc')) : $CI->config->item('email_bcc'));

	$NewContent = $CI->load->view($template, $email_data, TRUE);

	// Envía el correo al user via MailGun
	$mg_api = 'key-aaca0481b9555b651dcc444c01ecebe4';
	$mg_version = 'api.mailgun.net/v3/';
	$mg_domain = "mg.auroralegorreta.com";

	$mg_from_email = $email_from;

	$mg_reply_to_email = ($email_reply_to ? $email_reply_to : $CI->config->item('email_reply_to'));
	$mg_reply_to_name = ($name_reply_to ? $name_reply_to : $CI->config->item('name_reply_to'));

	$mg_message_url = "https://".$mg_version.$mg_domain."/messages";


	$ch = curl_init();
	curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

	curl_setopt ($ch, CURLOPT_MAXREDIRS, 3);
	curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, false);
	curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt ($ch, CURLOPT_VERBOSE, 0);
	curl_setopt ($ch, CURLOPT_HEADER, 1);
	curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 10);
	curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);

	curl_setopt($ch, CURLOPT_USERPWD, 'api:' . $mg_api);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	curl_setopt($ch, CURLOPT_POST, true);
	//curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
	curl_setopt($ch, CURLOPT_HEADER, false);

	//curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
	curl_setopt($ch, CURLOPT_URL, $mg_message_url);
	curl_setopt($ch, CURLOPT_POSTFIELDS,
		array(  'from'      => $name_from . ' <' . $mg_from_email . '>',
			'to'        => $email_to,
			// 'bcc'       => $email_bcc,
			//'h:Reply-To'=> $mg_reply_to_name. ' <' . $mg_reply_to_email . '>',
			'subject'   => $subject,
			'html'      => $NewContent
		    ));
	$result = curl_exec($ch);

	curl_close($ch);

	return (json_decode($result));
	// END Send mail with Mailgun

}
