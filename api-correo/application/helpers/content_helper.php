<?php

function create_form($action, $current) {
?>
<!--contatc-->
<div id="contact-form" class="fixed-form" style="display: none;">
	<a href="#contact-form" class="btn-close-form btn-slide-toggle">
		<i class="fa fa-times" aria-hidden="true"></i>
	</a>
	<form class="form-content generic_form" action="<?php echo($action); ?>" method="get">

		<h2 class="main-h text-center">¿Tienes dudas?</h2>
		<p class="text-center">
			Si deseas mayor información acerca de nuestros productos o servicios
			llámanos o envíanos un mensaje, nosotros nos pondremos en contacto
			contigo para asesorarte en tu campaña.
		</p>
		<div class="info-phone">
			<i class="fa fa-phone" aria-hidden="true"></i>
			(81) 8881 9951
		</div>
		<p class="text-center">
			Formulario de contacto:
		</p>
		<input type="hidden" name="form" value="contact" />
		<input type="hidden" name="current" value="<?php echo($current); ?>" />
		<div class="form-group">
			<div class="row">
				<div class="col-sm-6">
					<input type="text" id="nombre" name="nombre" class="form-control" placeholder="Nombre" value="" maxlength="100">
					<br>
					<input type="tel" id="nombre" name="telefono" class="form-control" placeholder="Telefono" value="" maxlength="100">
				</div>
				<div class="col-sm-6">
					<input type="email" id="email" name="email" class="form-control" placeholder="Email" value="" maxlength="100">
					<br>
					<select class="form-control" name="servicio">
						<option>Publicidad Exterior</option>
						<option>Publicidad Móvil</option>
						<option>Impresión Digital a gran formato</option>
						<option>Publicidad Digital</option>
					</select>
				</div>
			</div>

		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-sm-12">
					<textarea id="comentario" name="comentario" class="form-control" placeholder="Mensaje" rows="3"></textarea>
				</div>

			</div>
		</div>
		<!--<div class="form-group">
			<input type="tel" id="telefono" name="telefono" class="form-control" placeholder="Teléfono:" value="" maxlength="20">
		</div>
		<div class="form-group">

		</div>-->
		<div class="form-group">
			<div class="g-recaptcha" data-sitekey="6LeGGRkUAAAAALNh5Y2ROJiGQJI1CZONTaz1O6LB"></div>
		</div>
		<div class="form-group">
			<button type="submit" class="btn-c-t-a primary">
				Enviar			</button>
		</div>

		<div class="social-list-2">

			<ul>
				<li><a href="https://www.facebook.com/GrupoPOL" target="_blank"><i class="fa fa-facebook fa-2x-"></i></a></li>
				<li><a href="https://twitter.com/GrupoPOL" target="_blank"><i class="fa fa-twitter fa-2x-"></i></a></li>
				<li><a href="https://www.youtube.com/user/GrupoPOL" target="_blank"><i class="fa fa-youtube-play fa-2x-"></i></a></li>
			</ul>

		</div>

	</form>
</div>
<?php
}



function create_request($action, $current) {
?>
<!--contatc-->
<div id="contact-form-2" class="fixed-form" style="display: none;">
	<a href="#contact-form" class="btn-close-form btn-slide-toggle">
		<i class="fa fa-times" aria-hidden="true"></i>
	</a>
	<form class="form-content generic_form" action="<?php echo($action); ?>" method="get">
		<h2 class="main-h text-center">Solicitar cotización</h2>
		<p class="text-center">
			Por favor llena el siguiente formulario para completar tu solicitud, un
			ejecutivo se pondrá en contacto contigo para darle seguimiento.
		</p>
		<p class="text-center">
			<small>Los campos marcados con * son obligatorios.</small>
		</p>
		<input type="hidden" name="form" value="request" />
		<input type="hidden" name="current" value="<?php echo($current); ?>" />
		<div class="form-group">
			<div class="row">
				<div class="col-sm-6">
					<input type="text" id="nombre" name="nombre" class="form-control" placeholder="Nombre*" value="" maxlength="100" />
				</div>
				<div class="col-sm-6">
					<input type="text" id="apellido" name="apellido" class="form-control" placeholder="Apellido*" value="" maxlength="100" />
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-sm-6">
					<input type="text" id="empresa" name="empresa" class="form-control" placeholder="Empresa*" value="" maxlength="100" />
				</div>
				<div class="col-sm-6">
					<input type="tel" id="telefono" name="telefono" class="form-control" placeholder="Teléfono*" value="" maxlength="100" />
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-sm-12">
					<input type="email" id="email" name="email" class="form-control" placeholder="Email*" value="" maxlength="100">
				</div>

			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-sm-6">
					<input type="text" id="tiempo" name="tiempo" class="form-control" placeholder="Tiempo de pauta" value="" maxlength="100" />
				</div>
				<div class="col-sm-6">
					<input type="date" id="inicio" name="inicio" class="form-control" placeholder="A partir de" value="" maxlength="100" />
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-sm-12">
					<textarea id="comentario" name="comentario" class="form-control" placeholder="Comentarios adicionales" rows="3"></textarea>
				</div>

			</div>
		</div>
		<!--<div class="form-group">
			<input type="tel" id="telefono" name="telefono" class="form-control" placeholder="Teléfono:" value="" maxlength="20">
		</div>
		<div class="form-group">

		</div>-->
		<div class="form-group">
			<button type="submit" class="btn-c-t-a primary">
				Completar solicitud			</button>
		</div>

		<div class="social-list-2">

			<ul>
				<li><a href="https://www.facebook.com/GrupoPOL" target="_blank"><i class="fa fa-facebook fa-2x-"></i></a></li>
				<li><a href="https://twitter.com/GrupoPOL" target="_blank"><i class="fa fa-twitter fa-2x-"></i></a></li>
				<li><a href="https://www.youtube.com/user/GrupoPOL" target="_blank"><i class="fa fa-youtube-play fa-2x-"></i></a></li>
			</ul>

		</div>
	</form>
</div>
<?php
}
