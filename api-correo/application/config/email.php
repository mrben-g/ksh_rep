<?php if (! defined('BASEPATH')) exit('No direct script access');

/*
 | -------------------------------------------------------------------
 |  Email configuration
 | -------------------------------------------------------------------
 |
 | NOTE: Working with email templates.
 | @version    1.0 28/05/2015
 |
 | @link
 |
 */

$config['protocol'] = 'mail'; // [mail | smtp]

//$config['smtp_host'] = 'ssl://smtp.gmail.com';
$config['smtp_host'] = 'ssl://smtpout.secureserver.net';
$config['smtp_port'] = 465;
$config['smtp_user'] = 'tpdemos@tpdemos.com';
$config['smtp_pass'] = 'Candad0*';
$config['mailtype'] = 'html';
$config['charset'] = 'utf-8';
$config['newline'] = "\r\n";
$config['priority'] = 1;

// Sender Email address
$config['email_from'] = 'no-responder@asse-seguros.com';

// Sender Name
$config['name_from'] = 'ASSE SEGUROS';

// Receivers email address
$config['email_to'] = array('sistemas@trespuntos.mx', 'fcedillo@trespuntos.mx', 'glopez@trespuntos.mx');
$config['email_bcc'] = array('notificaciones.tp@gmail.com');
