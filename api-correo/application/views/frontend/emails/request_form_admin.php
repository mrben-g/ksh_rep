<?php include("header.php"); ?>
<span style="color: #595959;">Se ha registrado un nuevo prospecto a trav&eacute;s del formulario de solicitud de cotización
del sitio <a href="<?php echo(base_url('cotizador')); ?>" target="_blank" style="color: #595959;"><?php echo(base_url('cotizador')); ?></a>.<br />
Se obtuvo la siguiente informaci&oacute;n:</span>
<br /><br />
<strong style="color: #595959;">Nombre:</strong> <?php echo($name); ?><br />
<strong style="color: #595959;">Apellido:</strong> <?php echo($lastname); ?><br />
<strong style="color: #595959;">Empresa:</strong> <?php echo($company); ?><br />
<strong style="color: #595959;">Teléfono:</strong> <?php echo($phone); ?><br />
<strong style="color: #595959;">Email:</strong> <?php echo($email); ?><br />
<strong style="color: #595959;">Tiempo de pauta:</strong> <?php echo($guideline_time); ?><br />
<strong style="color: #595959;">A partir de:</strong> <?php echo($start_date); ?><br />
<strong style="color: #595959;">Comentarios:</strong> <?php echo($message); ?><br />
<strong style="color: #595959;">Fecha de registro:</strong> <?php echo($created_at); ?><br />
<strong style="color: #595959;">Dirección IP:</strong> <?php echo($ip_address); ?>
<?php include("footer.php"); ?>