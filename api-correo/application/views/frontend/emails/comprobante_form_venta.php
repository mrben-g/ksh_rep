<?php
/*
  $step_dos[$idr] = array('nombre' => $nombre,
                                'apellido' => $apellidos,
                                'puesto'=>$puesto,
                                'razon_social'=>$razon_social,
                                'rfc'=>$rfc,
                                'estado' => $estado,
                                'ciudad' => $ciudad,
                                'email' => $email,
                                'zipcode'=>$zipcode,
                                'direccion'=>$dir,
                                'numero_dir'=>$nu_dir,
                                'colonia'=>$col_dir,
                                'lada_tel'=> $lada_tel,
                                'tel'=>$tel,
                                'lada_cel'=>$lada_cel,
                                'cel'=>$cel,
                                'dirEnvio'=>$dir_envio,
                                'nombre_dos' => $nom2,
                                'apellido_dos' => $ap2,
                                'estado_dos' => $edo2,
                                'ciudad_dos' => $cd2,
                                'email_dos' => $email2,
                                'zipcode_dos'=>$cp2,
                                'direccion_dos'=>$dir2,
                                'numero_dir_dos'=>$nu_dir_2,
                                'colonia_dos'=>$col_dir_2,    
                                'lada_tel_dos'=>$lada_tel_2,
                                'tel_dos'=>$tel2,
                                'lada_cel_dos'=>$lada_cel_2,
                                'cel_dos'=>$cel2               
                                );*/

$nombre_ef = "";
$apellidos_ef = "";
$dir_ef = "";
$nu_dir = "";
$col_dir = "";
$tel_ef = "";
$zipcode_ef = "";
$mail_cliente = "";
$nom2 = "";
$ap2 = "";
$dir2 = "";
$nu_dir2= "";
$col_dir2= "";
$lada_tel_2 = "";
$tel2 = "";
$cp2 = "";         
 foreach($cliente as $cs=>$client):
         $nombre_ef = $client['nombre'];
         $apellidos_ef = $client['apellido'];
         $dir_ef = $client['direccion'];
         $nu_dir= $client['numero_dir'];
         $col_dir= $client['colonia'];
         $tel_ef= $client['tel'];
         $zipcode_ef= $client['zipcode'];
         $mail_cliente = $client['email'];
         if($client['dirEnvio'] == 2){
            $nom2 = $client['nombre_dos'];
            $ap2 = $client['apellido_dos'];
            $dir2 = $client['direccion_dos'];
            $nu_dir2= $client['numero_dir_dos'];
            $col_dir2= $client['colonia_dos'];
            $lada_tel_2 = $client['lada_tel_dos'];
            $tel2 = $client['tel_dos'];
            $cp2 = $client['zipcode_dos'];         
         }
         endforeach; 
         
?>
<!DOCTYPE html >
<html >
    <head>
           <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
           <meta charset="utf-8">
        <title></title>

    <style type="text/css">
		#outlook a{
			padding:0;
		}
		body{
			width:100% !important;
		}
		.ReadMsgBody{
			width:100%;
		}
		.ExternalClass{
			width:100%;
		}
		body{
			-webkit-text-size-adjust:none;
		}
		body{
			margin:0;
			padding:0;
		}
		img{
			border:0;
			height:auto;
			line-height:100%;
			outline:none;
			text-decoration:none;
		}
		table td{
			border-collapse:collapse;
		}
		#bodyTable{
			height:98% !important;
			margin:0;
			padding:0;
			width:100% !important;
		}
	/*
	@tab Page
	@section background color
	@tip Set the background color for your email. You may want to choose one that matches your companys branding.
	@theme page
	*/
		body,#bodyTable{
			/*@editable*/background-color:#ebebeb;
                  /*      background: url("images_css/body_bg.jpg") repeat top left*/
		}
		.borderBar{
			/*background-image:url("http://gallery.mailchimp.com/27aac8a65e64c994c4416d6b8/images/diagborder.png");*/
			background-position:top left;
			background-repeat:repeat;
			height:5px !important;
		}
	/*
	@tab Page
	@section heading 1
	@tip Set the styling for all first-level headings in your emails. These should be the largest of your headings.
	@style heading 1
	*/
		h1{
			/*@editable*/color:#3F3A38;
			display:block;
			/*@editable*/font-family:Georgia;
			/*@editable*/font-size:40px;
			/*@editable*/font-weight:normal;
			/*@editable*/line-height:100%;
			/*@editable*/letter-spacing:normal;
			margin-top:0;
			margin-right:0;
			margin-bottom:10px;
			margin-left:0;
			/*@editable*/text-align:left;
		}
	/*
	@tab Page
	@section heading 2
	@tip Set the styling for all second-level headings in your emails.
	@style heading 2
	*/
		h2{
			/*@editable*/color:#3F3A38;
			display:block;
			/*@editable*/font-family:Georgia;
			/*@editable*/font-size:24px;
			/*@editable*/font-weight:normal;
			/*@editable*/line-height:100%;
			/*@editable*/letter-spacing:normal;
			margin-top:0;
			margin-right:0;
			margin-bottom:10px;
			margin-left:0;
			/*@editable*/text-align:left;
		}
	/*
	@tab Page
	@section heading 3
	@tip Set the styling for all third-level headings in your emails.
	@style heading 3
	*/
		h3{
			/*@editable*/color:#ED5E29;
			display:block;
			/*@editable*/font-family:Georgia;
			/*@editable*/font-size:16px;
			/*@editable*/font-weight:normal;
			/*@editable*/line-height:100%;
			/*@editable*/letter-spacing:normal;
			margin-top:0;
			margin-right:0;
			margin-bottom:10px;
			margin-left:0;
			/*@editable*/text-align:left;
		}
	/*
	@tab Page
	@section heading 4
	@tip Set the styling for all fourth-level headings in your emails. These should be the smallest of your headings.
	@style heading 4
	*/
 		h4{

			/*@editable*/color:#3F3A38;
			display:inline-block;
			/*@editable*/font-family:Georgia;
			/*@editable*/font-size:12px;
			/*@editable*/font-weight:normal;
			/*@editable*/line-height:100%;
			/*@editable*/letter-spacing:normal;
			margin-top:0;
			margin-right:0;
			margin-bottom:10px;
			margin-left:0;
			/*@editable*/text-align:left;
		}
	/*
	@tab Header
	@section preheader text
	@tip Set the styling for your emails preheader text. Choose a size and color that is easy to read.
	*/
		.preheaderContent{
			/*@editable*/color:#505050;
			/*@editable*/font-family:Helvetica;
			/*@editable*/font-size:11px;
			/*@editable*/line-height:125%;
			/*@editable*/text-align:left;
		}
	/*
	@tab Header
	@section preheader link
	@tip Set the styling for your emails preheader links. Choose a color that helps them stand out from your text.
	*/
		.preheaderContent a:link,.preheaderContent a:visited,.preheaderContent a .yshortcuts {
			/*@editable*/color:#505050;
			/*@editable*/font-weight:normal;
			/*@editable*/text-decoration:underline;
		}
	/*
	@tab Header
	@section header style
	@tip Set the background color and top border for your emails header area.
	@theme header
	*/
		#templateHeader{
			/*@editable*/background-color:#000;
			/*@editable*border-top:10px solid #CB5000;*/
		}
	/*
	@tab Header
	@section header text
	@tip Set the styling for your emails header text. Choose a size and color that is easy to read.
	*/
     	.headerContent{

			/*@editable*/color:#505050;
			/*@editable*/font-family:Helvetica;
			/*@editable*/font-size:18px;
			/*@editable*/font-weight:bold;
			/*@editable*/line-height:100%;
			/*@editable*/padding-top:20px;
			/*@editable*/padding-right:40px;
			/*@editable*/padding-bottom:20px;
			/*@editable*/padding-left:40px;
			/*@editable*/text-align:left;
			/*@editable*/vertical-align:middle;
		}
	/*
	@tab Header
	@section header link
	@tip Set the styling for your emails header links. Choose a color that helps them stand out from your text.
	*/
		.headerContent a:link,.headerContent a:visited,.headerContent a .yshortcuts {
			/*@editable*/color:#505050;
			/*@editable*/font-weight:normal;
			/*@editable*/text-decoration:underline;
		}
		#headerImage{
			height:auto;
			max-width:600px !important;
		}
	/*
	@tab Body
	@section body style
	@tip Set the background color for your emails body area.
	*/
		#templateBody{
			/*@editable*/background-color:#F8F8F8;
		}
	/*
	@tab Body
	@section body text
	@tip Set the styling for your emails main content text. Choose a size and color that is easy to read.
	@theme main
	*/
		.bodyContent{
			/*@editable*/color:#43404D;
			/*@editable*/font-family:Georgia;
			/*@editable*/font-size:16px;
			/*@editable*/line-height:150%;
			/*@editable*/text-align:left;
		}
	/*
	@tab Body
	@section body link
	@tip Set the styling for your emails main content links. Choose a color that helps them stand out from your text.
	*/
		.bodyContent a:link,.bodyContent a:visited,.bodyContent a .yshortcuts {
			/*@editable*/color:#ED5E29;
			/*@editable*/font-weight:normal;
			/*@editable*/text-decoration:underline;
		}
	/*
	@tab Body
	@section button style
	@tip Set the styling for your emails button. Choose a style that draws attention.
	*/
		.templateButton{
			/*@tab Body
@section button style
@tip Set the styling for your emails button. Choose a style that draws attention.*/-moz-border-radius:5px;
			-webkit-border-radius:5px;
			/*@editable*/background-color:#99cc00;
			border-radius:5px;
		}
	/*
	@tab Body
	@section button style
	@tip Set the styling for your emails button. Choose a style that draws attention.
	*/
		.templateButtonContent,.templateButtonContent a:link,.templateButtonContent a:visited,.templateButtonContent a .yshortcuts {
			/*@editable*/color:#000;
			/*@editable*/font-family:Georgia;
			/*@editable*/font-size:31px;
			/*@editable*/font-weight:normal;
			text-align:center;
			text-decoration:none;
		}
		.bodyContent img{
			display:inline;
			height:auto;
		}
	/*
	@tab Footer
	@section footer style
	@tip Set the background color and borders for your emails footer area.
	@theme footer
	*/
		#templateFooter{
			/*@editable*/background-color:#F8F8F8;
			/*@editable*border-bottom:10px solid #F9AE31;*/
		}
	/*
	@tab Footer
	@section footer text
	@tip Set the styling for your emails footer text. Choose a size and color that is easy to read.
	@theme footer
	*/
		.footerContent{
			/*@editable*/color:#3F3A38;			
			/*@editable*/font-size:14px;	
		}
	/*
	@tab Footer
	@section footer link
	@tip Set the styling for your emails footer links. Choose a color that helps them stand out from your text.
	*/
		.footerContent a:link,.footerContent a:visited,.footerContent a .yshortcuts {
			/*@editable*/color:#EB4102;
			/*@editable*/font-weight:normal;
			/*@editable*/text-decoration:underline;
		}
		.footerContent img{
			display:inline;
		}
		#monkeyRewards img{
			max-width:190px;
		}
		.bodyContent a:link,.bodyContent a:visited,.bodyContent a .yshortcuts{
			color:#0000cd;
		}
		.footerContent a:link,.footerContent a:visited,.footerContent a .yshortcuts{
			color:#99cc00;
		}
                table.p thead th{
                    border: 1px solid #ebebeb;
                }
                table.p tbody td{
                    border: 1px solid #ebebeb;
                }
                .right-col{
              
  width: 336px;
  display:block;
  position:relative;
 float: right;
}
</style></head>
    <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0" style="backgrond:#fff;">
    	<center>
        	<table border="0" cellpadding="0" cellspacing="0" height="90%" width="100%" id="bodyTable" bgcolor="#ebebeb">
            	<tr>
                	<td align="center" valign="top" style="">
                    	<!-- // BEGIN CONTAINER -->
                        <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateContainer"  style="background:#fff;color:#fff;">
                        	          	<tr>
                            	<td align="center" valign="top">
                                	<!-- // BEGIN HEADER -->
                                	<table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateHeader"  style="background:#000;">
                                   	<tr>
                                        	<td align="center" valign="top" style="padding-top:0px; padding-bottom:10px;">
                                            	<table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td class="headerContent">                                                          
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        
                                       <tr>
                                           <td class="borderBar" >
                                               <img src="http://ksh.mx/images/logo.png"  alt="" mc:label="image" mc:edit="body_image" mc:"allowdesigner" style="width:38%;"/>
                                            </td>
                                        	<td style="">
                                             CONFIRMACIÓN DE PEDIDO
                                            </td>
                                        </tr>
                                        <tr>
                                        	<td class="borderBar" >
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- END HEADER \\ -->
                                </td>
                            </tr>
                        	<tr>
                            	<td align="center" valign="top">
                                	<!-- // BEGIN BODY -->
                                	<table border="0" cellpadding="40" cellspacing="0" width="600" id="templateBody"  style="background:#fff;color:#fff;">
                                    	<tr>
                                        	<td align="center" valign="top" style="padding-bottom:20px;;">
                                            	<table border="0" cellpadding="0" cellspacing="0" width="100%" style="color:#000">
                                                                	<tr>
<td colspan="3" valign="top" class="bodyContent" style="padding-top:40px;" mc:edit="body_content01" style="color:#000" >
<font face="arial, helvetica, sans-serif" style="">Estimado (a): &nbsp;</font><b style=""> <?php echo ($nombre_ef)."&nbsp;".($apellidos_ef) ?></b><br/>
<table>
    <tr>
<td style="width:50%;" valign="top">            
<strong><span style="font-family:arial,helvetica,sans-serif;color:#000">Datos de Cliente:</span></strong><br/>
<font face="arial, helvetica, sans-serif;color:#000;">Nombre: <?php echo ($nombre_ef).' '.($apellidos_ef) ?></font><br/>
<font face="arial, helvetica, sans-serif;color:#000;">Dirección: <?php echo ($dir_ef.' '.$nu_dir.'  Col. '.$col_dir) ?></font><br/>
<font face="arial, helvetica, sans-serif;color:#000;">Teléfono: <?php echo  $tel_ef ?></font><br/>    
<font face="arial, helvetica, sans-serif;color:#000;">Estado: <?php echo ($estado_nombre) ?></font><br/>
<font face="arial, helvetica, sans-serif;color:#000;">Ciudad:<?php echo  ($ciudad_nombre) ?> </font><br/> 
<font face="arial, helvetica, sans-serif;color:#000;">Código Postal<?php echo ($zipcode_ef) ?></font><br/>
<font face="arial, helvetica, sans-serif;color:#000;">Tipo de Pago<?php echo ($tipopago) ?></font><br/>    
</td>
<?php
if($dir_envio == 2){
?>
 <td>
<strong><span style="font-family:arial,helvetica,sans-serif;color:#000">Datos de Envio:</span></strong><br/>
<font face="arial, helvetica, sans-serif;color:#000;">Nombre: <?php echo ($nom2).' '.($ap2) ?></font><br/>
<font face="arial, helvetica, sans-serif;color:#000;">Dirección: <?php echo ($dir2.' '.$nu_dir2.'  Col. '.$col_dir2) ?></font><br/>
<font face="arial, helvetica, sans-serif;color:#000;">Teléfono: <?php echo '('.$lada_tel_2.') '.$tel2 ?></font><br/>
<font face="arial, helvetica, sans-serif;color:#000;">Estado: <?php echo ($estado_nombre_dos) ?></font><br/>
<font face="arial, helvetica, sans-serif;color:#000;">Ciudad: <?php echo ($ciudad_nombre_dos) ?></font><br/>
<font face="arial, helvetica, sans-serif;color:#000;">Código Postal <?php echo ($cp2) ?></font><br/>    
</td>
<?php } ?> 

    </tr>
</table>
<br/>
<br/>
<table cellspacing="0" class="p" style="width: 100%;color:#000;">
    <thead>
            <tr>
            <th>ARTÍCULO</th>
            <th>CANTIDAD</th>
            <th>PRECIO UNITARIO</th>
            <th>SUBTOTAL</th>
            </tr>
    </thead>
    <tbody>
			<?php  					  
                                                       $suma =0;
                                                       $total_articulos=0;
                                                       $cont_forms = 1;
                                                       
                                                       if(!empty($carrito)):
         foreach($carrito as $cs=>$carro_shop):
           $subtotal = $carro_shop['cantidad']*$carro_shop['precio'];
            $suma += $subtotal;
            $total_articulos += $carro_shop['cantidad'];      
            ?>
                    <tr>
                    <td>
                    <h3><?php echo utf8_encode($carro_shop['nombre']) ?></h3>
                      <span style="font-size:12px"></span>
                    </td>
                    <td align="center"><?php $cant =  $carro_shop['cantidad']; echo $cant ?></td>
                    <td class="price" align="center">$ <?php echo $carro_shop['precio'] ?></td>
                    <td class="" align="right" style="">$ <?php echo number_format($subtotal, 2, '.', ' ') ?></td>
                    </tr>
        
        <?php
          $cont_forms++;
         endforeach; else:
          
         endif; 
         ?>
<?php 
 
         if(!empty($cotizacion)){
?>
                                <tr>
                                    <td colspan="5">
                                        <h1>LISTA DE PRODUCTOS COTIZADOS</h1>
                                    </td>
                                </tr>                                
                                <?php
                                $total_articulos=0;$cont_forms = 1;$i = 0;$total =0;
        /*ajax*/
        
        if(!empty($cotizacion)): foreach($cotizacion as $cs=>$cotizacion_shop):
            $total_articulos += $cotizacion_shop['cantidad'];
            
            ?>
                    <tr id="tr<?php echo $cs;?>">                        
                        <td>
                            <h3 id="nom_prod"><?php echo $cotizacion_shop['nombre']; ?></h3>
                            <span id="descrip" style="font-size:14px"><?php echo utf8_encode($cs) ?></span>
                            <div class="delete"><!--<a href="#" >Delete</a>--></div>
                        </td>
                        <td><?php echo $cotizacion_shop['cantidad'] ?></td>
                        <td class="price" ><?php //echo number_format($cotizacion_shop['precio'], 2, '.', ' ') ?></td>
                        <td class="subtot" align="right" style=""><?php //echo number_format($subtotal, 2, '.', ' ');  ?></td>
                    </tr>
                        <?php
                        
                        $cont_forms++;
                 
                        endforeach; else:
                            
                        endif;
                        
                                       
  
 }
 
 ?>                    
    </tbody>
</table>
				
<div class="right-col" style="">
              <table style="padding:3px 0px 12px 0px;" border="0" align="right">
                <tbody><tr>
                    <td style="text-align:right;white-space:nowrap;font-size:12px;font-family:Arial;color:#000;"><b>Subtotal:</b>&nbsp;</td>
                    <td style="text-align:right;white-space:nowrap;font-size:12px;font-family:Arial;color:#000;">$ <?php echo number_format($suma, 2, '.', ' ') ?></td>
                </tr>
                <tr>
                    <td colspan="2"><hr style="border:0px solid #3d3d3d;border-top-width:1px"></td>
                </tr>
                <tr>
                    <td style="text-align:right;white-space:nowrap;font:15px Tahoma;text-align:right;color:#000;">Costo total MXN:&nbsp;</td>
                    <td style="text-align:right;white-space:nowrap;font:15px Tahoma;text-align:right;color:#000;"><strong style="font:bold 17px Tahoma">$ <?php echo number_format(($suma), 2, '.', ' ') ?></strong></td>
                </tr>
                
                </tbody></table>

</div>
<strong><font face="arial, helvetica, sans-serif"> </font><a href="#" style="font-family: arial, helvetica, sans-serif; "></a></strong><br/>
</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- END BODY \\ -->
                                </td>
                            </tr>
                        	<tr>
                            	<td align="center" valign="top">
                                	<!-- // BEGIN FOOTER -->
                                	<table border="0" cellpadding="20" cellspacing="0" width="100%" id="templateFooter"  style="background:#fff;">';
                                           <tr>
                                        	<td align="center" valign="top" style="padding-right:40px; padding-left:40px;">
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                               <tr>
                                                <td valign="top" class="footerContent" style="color:#000" mc:edit="footer_content"><?php echo str_replace('/[\., ]+/',"",$barcode).'<br>'.utf8_decode($comentario) ?></td>
                                               </tr>
                                            </table>
                                            </td>
                                        </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                            </tr>
                                    </table>
                                    <!-- END FOOTER \\ -->
                                </td>
                            </tr>
                        </table>
                        <!-- END CONTAINER \\ -->
                    </td>
                </tr>
            </table>
        </center>
    </body>
</html>