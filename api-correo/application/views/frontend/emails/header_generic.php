<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<!-- If you delete this tag, the sky will fall on your head -->
	<meta name="viewport" content="width=device-width" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<title><?php echo $titulo; ?></title>
	<style>
		body {
			font-family: Arial !important;
		}
	</style>
</head>
<body yahoo bgcolor="#FFFFFF" style="margin: 0; padding: 0; min-width: 100%!important;">
        <table width="100%" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" style="font-family: Arial; font-weight: normal;font-size: 14px;">
		<tr>
			<td>
				<table align="center" cellpadding="0" cellspacing="0" border="0" style="width: 100%; max-width: 600px; background: #FFFFFF; margin-top: 25px;margin-bottom: 25px; font-family: Arial; font-size:12px;">
				<tr>
					<td style="background: #ffffff; text-align: center; border-bottom: 1px solid #fff; padding-bottom: 0; padding-top: 0;">
						<?php if ($logo == ''): ?>
							<h2><?php echo $titulo; ?></h2>
							<?php else: ?>
								<img src="<?php echo $logo; ?>" alt="<?php echo $titulo; ?>" style="width:150px;" />
						<?php endif; ?>

					</td>
				</tr>
				<tr>
					<td>
						<div style="padding: 25px 15px; color: #595959; font-family: tahoma; font-size: 12px; text-align: left;">
