<?php

$lang['process_error'] = 'Lo sentimos, ocurrió un error y su solicitud no pudo ser completada. Por favor inténtelo nuevamente.';

/* End of file common_lang.php */
/* Location: ./system/language/spanish/common_lang.php */