<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_model extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  public function insert_lead($data = array())
  {
    return $this->db->insert('contact_form', $data);
  }
  
      public function get_cliente($correo = '') 
  { 
      $this->db->select('*'); 
      $this->db->from('t_clients'); 
      $this->db->where('email', $correo); 
      $query = $this->db->get(); 
      //return $query->row(); 
      if($query->num_rows() > 0){
        return $query->row();        
      }else{
     return false;
    }
  }

}
