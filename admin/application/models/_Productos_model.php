<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Productos_model extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  public function get_records($start = 1, $length = 1, $column_order, $column_direction, $search = false)
  {
    $start = ((($start > 0 ? $start : 1) - 1) * $length);

		$query = $this->db
			      ->from('tbl_productos')
			      ->limit($length, $start)
			      ->order_by($column_order, $column_direction);

		if (!empty($search))
		{
			$search_terms = explode(" ", $search['value']);

			foreach ($search_terms as $term)
			{
				if (!empty($term))
				{
					$query = $query->or_like('tbl_productos.Nombre', $term, 'both')
						       ->or_like('tbl_productos.NombreIngles', $term, 'both')
                   ->or_like('tbl_productos.Descripcion', $term, 'both')
                   ->or_like('tbl_productos.DescripcionIngles', $term, 'both');
				}
			}
		}
		$query = $query->get();

		return $query->result();
  }

  public function count_productos()
  {
    $this->db->select('idProducto');
    $this->db->from('tbl_productos');
    $query = $this->db->get();
    return $query->num_rows();
  }

  public function get($id = 0)
  {
    $this->db->select('*');
    $this->db->from('tbl_productos');
    $this->db->where('idProducto', $id);
    $query = $this->db->get();
    return $query->row();
  }

  public function search($filter = '')
  {
    $this->db->select('*');
    $this->db->from('tbl_productos');
    $this->db->where('Estatus', 1);
    if ($filter !== '') {

    $this->db->like('tbl_productos.NombreIngles', $filter, 'both');
    $this->db->or_like('tbl_productos.Nombre', $filter, 'both');
    $this->db->or_like('tbl_productos.Descripcion', $filter, 'both');
    $this->db->or_like('tbl_productos.DescripcionIngles', $filter, 'both');
  }
    $query = $this->db->get();
    return $query->result();
  }

}
