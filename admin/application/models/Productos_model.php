<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Productos_model extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  public function get_records($start = 1, $length = 1, $column_order, $column_direction, $search = false)
  {
    $start = ((($start > 0 ? $start : 1) - 1) * $length);

		$query = $this->db
			      ->from('tbl_productos')
			      ->limit($length, $start)
			      ->order_by($column_order, $column_direction);

		if (!empty($search))
		{
			$search_terms = explode(" ", $search['value']);

			foreach ($search_terms as $term)
			{
				if (!empty($term))
				{
					$query = $query->or_like('tbl_productos.Nombre', $term, 'both')
						       ->or_like('tbl_productos.NombreIngles', $term, 'both')
                   ->or_like('tbl_productos.Descripcion', $term, 'both')
                   ->or_like('tbl_productos.DescripcionIngles', $term, 'both');
				}
			}
		}
		$query = $query->get();

		return $query->result();
  }

  public function count_productos()
  {
    $this->db->select('idProducto');
    $this->db->from('tbl_productos');
    $query = $this->db->get();
    return $query->num_rows();
  }

  public function get_all()
  {
    $this->db->select('*');
    $this->db->from('tbl_productos');
    $this->db->where('Estatus', '1');
    $query = $this->db->get();
    return $query->result();
  }

  public function get($id = 0)
  {
    $this->db->select('*');
    $this->db->from('tbl_productos');
    $this->db->where('idProducto', $id);
    $query = $this->db->get();
    return $query->row();
  }

  public function search($filter = '')
  {
    // $this->output->enable_profiler(TRUE);
    $this->db->select('*');
    $this->db->from('tbl_productos');
    $this->db->where('Estatus', '1');
    if ($filter !== '') {
      // $this->db->where('tbl_productos.NombreIngles', '%'.$filter.'%');
      // $this->db->or_where('tbl_productos.Nombre', '%'.$filter.'%');
      // $this->db->or_where('tbl_productos.Descripcion', '%'.$filter.'%');
      // $this->db->or_where('tbl_productos.DescripcionIngles', '%'.$filter.'%');
      $this->db->group_start();
    $this->db->like('tbl_productos.NombreIngles', $filter, 'both');
    $this->db->or_like('tbl_productos.Nombre', $filter, 'both');
    $this->db->or_like('tbl_productos.Descripcion', $filter, 'both');
    $this->db->or_like('tbl_productos.DescripcionIngles', $filter, 'both');
    // $this->db->or_like('tbl_productos.Estatus', '1', 'both');
    $this->db->group_end();
  }
  // $this->db->where('Estatus', '1');
    $query = $this->db->get();
    return $query->result();
  }

  public function getfiltroProducto($id=0)
  {
              if($id == 1){
        $this->db->select('*');
        $this->db->from('t_producto');
        $this->db->where('DiametroNominal !=',"");
        $this->db->like('Nombre', 'Abrazadera','both');
        $this->db->order_by('IdProducto','ASC');        
        $query = $this->db->get();
        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
          
      }else if($id == 2){
          
        $this->db->select('*');
        $this->db->from('t_producto');
        $this->db->where('DiametroNominal !=',"");
        $this->db->like('Nombre', 'Junta','both');
        $this->db->order_by('IdProducto','ASC');        
        $query = $this->db->get();
        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
          
      }else if($id == 3){
        
        $this->db->select('*');
        $this->db->from('t_producto');        
        $this->db->where('DiametroNominal !=',"");
        $this->db->like('Nombre', 'ABRAZ BELL JOINT 3232 POWERSEAL','both');
        $this->db->order_by('IdProducto','ASC');        
        $query = $this->db->get();
        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
          
      }else if($id == 4){
          
        $this->db->select('*');
        $this->db->from('t_producto');
        $this->db->where('RangoDiametro !=',"");
        $this->db->like('Nombre', 'COPLE DAYTON PVC ');
        $this->db->or_like('Nombre', 'COPLE DAYTON PVC ','both');
        $this->db->or_like('Nombre', 'COPLE DAYTON PVC ','after');
        $this->db->order_by('IdProducto','ASC');      
        $query = $this->db->get();
        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
          
          
      }
      else if($id == 5){
          
        $this->db->select('*');
        $this->db->from('t_producto');
        $this->db->order_by('IdProducto','ASC');      
        $query = $this->db->get();
        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
          
          
      }
  }
}
