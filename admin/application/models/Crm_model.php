<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Crm_model extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  public function get_correos()
  {
    $this->db->select('*');
    $this->db->from('contact_form');
    $query = $this->db->get();
    return $query->result();
  }

  public function get_correo($id = 0)
  {
    $this->db->select('*');
    $this->db->from('contact_form');
    $this->db->where('id', $id);
    $query = $this->db->get();
    return $query->row();
  }

  public function count_email($type = '')
  {
    $this->db->select('*');
    $this->db->from('contact_form');
    if ($type == 'leido') {
      $this->db->where('readed', 1);
    } elseif ($type == 'noleido') {
      $this->db->where('readed', 0);
    }
    $query = $this->db->get();
    return $query->num_rows();

  }

  public function get_cotizaciones()
  {
    $this->db->select('*');
    $this->db->from('tbl_solicitudes');
    $this->db->where('Estatus', 1);
    $query = $this->db->get();
    return $query->result();
  }

  public function get_cotizacion($id = 0)
  {
    $this->db->select('*');
    $this->db->from('tbl_solicitudes');
    $this->db->where('IdSolicitud', $id);
    $query = $this->db->get();
    return $query->row();
  }

}
