<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_model extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  public function login($data = array())
  {
    $valid = false;
    $mensaje = 'Ocurrio un error desconocido';
    $redirect = '';
    $result = array();
    $user = $this->check_user($data);
    if (is_null($user)) {
      $valid = false;
      $mensaje = 'No existe el usuario';
    } else {
//      $pass = $this->encryption->decrypt($user->password);
         $pass = $user->Contrasena;
      if ($pass === $data['password']) {
        $valid = true;
        $mensaje = '';
        $redirect = base_url('');
        $result['redirect'] = $redirect;
        $_SESSION['login']['admin'] = $user->Nombre;
        $_SESSION['login']['rol'] = $user->TipoUsuarioId;
      } else {
        $valid = false;
        $mensaje = 'Usuario o contraseña incorrectas';
      }

    }
    // echo "<pre>";
    // print_r($user);
    // echo "</pre>";
    // var_dump($user);
    $result['valid'] = $valid;
    $result['mensaje'] = $mensaje;




    return $result;
  }

  public function check_user($data = array())
  {
    $this->db->select('*');
    $this->db->from('tbl_usuarios');
    $this->db->where('email', $data['usuario']);
    $query = $this->db->get();
    return $query->row();
  }

  public function insert($data = array())
  {
    return $this->db->insert('usuarios', $data);
  }

  public function update()
  {
      #code
  }

  public function get_all()
  {

  }

  public function get($id = 0)
  {
      #code
  }

}
