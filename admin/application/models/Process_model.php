<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Process_model extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More

  }

  public function insert($table = '', $data = array())
  {
    return $this->db->insert($table, $data);
  }
   public function clear($table = '')
  {
    $this->db->from($table); 
    $this->db->truncate(); 
  }
  public function update($table = '', $data = array(),$campo = '', $id = 0)
  {
    // $this->output->enable_profiler(TRUE);
      $this->db->where($campo, $id);
      return $this->db->update($table, $data);
  }

  public function update_no_w($table = '', $data = array())
  {
     return $this->db->update($table, $data);
  }

  public function get_all($table = '')
  {
    // $this->output->enable_profiler(TRUE);
    $this->db->select('*');
    $this->db->from($table);
    $query = $this->db->get();
    return $query->result();
  }
  
  public function precio_por_usuario()
  {
       
    $this->db->select('t_clients.*,tbl_precios_usuarios.TipoDePrecioId,tbl_tipo_precios.NombrePrecio,tbl_tipo_precios.Descuento');
    $this->db->from('t_clients');
    $this->db->join('tbl_precios_usuarios', 't_clients.IdCliente = tbl_precios_usuarios.UsuarioId', 'LEFT');
    $this->db->join('tbl_tipo_precios', 'tbl_precios_usuarios.TipoDePrecioId = tbl_tipo_precios.id', 'LEFT');
    
    //$this->db->where('tbl_usuarios');
    $query = $this->db->get();
    $this->output->enable_profiler(TRUE);
    return $query->result();
  }
  
  public function precio_lista()
  {
    $this->db->select('*');
    $this->db->from('tbl_tipo_precios');
    //$this->db->where('tbl_usuarios');
    $query = $this->db->get();
    return $query->result();
  }
  
   public function editar_precio_lista($id=0)
  {
       
    $this->db->select('*');
    $this->db->from('tbl_tipo_precios');
    $this->db->where('id', $id);
    $query = $this->db->get();
    return $query->result();
    
  }
  
  
  public function tipo_precio_usuario($id_user, $id_precio){
    
      $this->db->select('*');
      $this->db->from('tbl_precios_usuarios');
      $this->db->where('UsuarioId',$id_user);
      $query = $this->db->get();
      
      
      if ($query->num_rows() == 0) {
           //INSERTAR
            return $this->db->insert('tbl_precios_usuarios', array('UsuarioId'=>$id_user,'TipoDePrecioId'=>$id_precio));
            
       }else{
         //ACTUALIZA  
         $this->db->where('UsuarioId',$id_user);
         return $this->db->update('tbl_precios_usuarios', array('UsuarioId'=>$id_user,'TipoDePrecioId'=>$id_precio));
       }
      
      
  }
  
  public function precio_producto()
  {
    $this->db->select('*');
    $this->db->from('tbl_codigos');
    $query = $this->db->get();
     return ($query->num_rows() > 0)?$query->result():FALSE;
     //return $query->row();     
     
//    $this->db->join('t_producto', 'tbl_codigos.SKU = t_producto.SKU ', 'RIGHT');
//    $this->db->join('tbl_contra_incendio', 'tbl_codigos.SKU = tbl_contra_incendio.SKU', 'RIGHT');
//    $this->db->join('t_diametro_derivacion', 'tbl_codigos.SKU = t_diametro_derivacion.SKU', 'RIGHT');
//    $this->db->join('t_alcantarillado', 'tbl_codigos.SKU = t_alcantarillado.SKU', 'RIGHT');
//    $this->db->join('t_filtros', 'tbl_codigos.SKU = t_filtros.SKU', 'RIGHT');
//    $this->db->join('t_medidores', 'tbl_codigos.SKU = t_medidores.SKU', 'RIGHT');
     
     
    
   
  
  }

}
