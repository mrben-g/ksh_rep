<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Precios_model extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  public function insert_prices($name = '',$desc='')
  {
      return $this->db->insert('tbl_tipo_precios', array('NombrePrecio'=>$name,'Descuento'=>$desc));  
  }
  
    public function update_prices($id=0,$name = '',$desc='')
  {
        
      $this->db->where('id',$id);
      return $this->db->update('tbl_tipo_precios', array('NombrePrecio'=>$name,'Descuento'=>$desc));  
  }

}
