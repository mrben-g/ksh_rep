<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pedidos_model extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  public function get_records($start = 1, $length = 1, $column_order, $column_direction, $search = false)
  {
    $start = ((($start > 0 ? $start : 1) - 1) * $length);

		$query = $this->db
            // ->select('');
			      ->from('t_ventas')
            ->join('t_clients', 't_clients.IdCliente = t_ventas.UsuarioId', 'left')
			      ->limit($length, $start)
			      ->order_by($column_order, $column_direction);

		if (!empty($search))
		{
			$search_terms = explode(" ", $search['value']);

			foreach ($search_terms as $term)
			{
				if (!empty($term))
				{
					$query = $query->or_like('t_ventas.CodigoId', $term, 'both')
						       ->or_like('t_ventas.TotalCompra', $term, 'both')
                   ->or_like('t_clients.Nombre', $term, 'both')
                   ->or_like('t_clients.Apellido', $term, 'both');
				}
			}
		}
		$query = $query->get();

		return $query->result();
  }

  public function get($id = 0)
  {
    $this->db->select('*');
    $this->db->from('t_ventas');
    $this->db->join('t_clients', 't_clients.IdCliente = t_ventas.UsuarioId', 'left');
    $this->db->join('t_ventas_detalle', 't_ventas_detalle.PedidoId = t_ventas.IdPedido', 'left');
    // $this->db->join('tbl_productos', 'tbl_productos.IdProducto = tbl_ventas_detalle.ProductoId', 'left');
    //$this->db->where('CodigoId', $id);
    $query = $this->db->get();
    return $query->row();
  }

  public function get_estado($id = 0)
  {
    $this->db->select('*');
    $this->db->from('tbl_estados');
    $this->db->where('id', $id);
    $query = $this->db->get();
    return $query->row();
  }

  public function get_product_invoice($id = 0)
  {
    $this->db->select('t_producto.*,t_ventas_detalle.PrecioProducto,t_ventas_detalle.CantidadProducto,t_ventas_detalle.ProductoId');
    $this->db->from('t_ventas_detalle');
    $this->db->join('t_producto', 't_producto.IdProducto = t_ventas_detalle.ProductoId', 'left');
    $this->db->where('t_ventas_detalle.PedidoId', $id);
    $query = $this->db->get();
    return $query->result();
  }

  public function get_payment_method()
  {
      #code
  }

  public function get_direccion($id = 0)
  {
    $this->db->select('*');
    $this->db->from('t_clients');
    $this->db->where('IdCliente', $id);
    $query = $this->db->get();
    return $query->row();
  }

}
