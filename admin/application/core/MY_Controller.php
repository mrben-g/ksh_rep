<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_controller extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More

    //Seccion seguridad
    // var_dump($_SESSION['login']);
    if (!isset($_SESSION['login'])) {
      redirect(base_url('admin/login'), 'refresh');
    }

  }

  function index()
  {

  }

}
