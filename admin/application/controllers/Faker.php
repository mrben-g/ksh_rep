<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Faker extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
    $this->load->model(array('process_model'));
    ini_set('max_execution_time', 300);
  }

  function index()
  {
    // var_dump(getcwd());
      require_once getcwd().'/vendor/fzaninotto/Faker/src/autoload.php';

      $faker = Faker\Factory::create();

      $limit = 10000;

      for ($i=0; $i < $limit; $i++) {

        $data = array(
          'Nombre' => $faker->name,
          'AperturaValvula' => $faker->randomDigitNotNull.' "',
          'TamanoValvula' => $faker->randomDigitNotNull.' "',
          'NumeroVias' => $faker->randomDigitNotNull,
          'LargoBarril' => $faker->randomFloat,
          'TipoJunta' => $faker->randomElement(array('JM','BRID')),
          'SKU' => $faker->numberBetween(2000,6000),
        );

        // $insert = $this->process_model->insert('tbl_contra_incendio',$data);
        // var_dump($insert);

      }


  }

}
