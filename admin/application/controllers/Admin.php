<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MY_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
    $this->load->model(array('productos_model','pedidos_model','process_model','crm_model'));
    $this->data['nuevos_correos'] = $this->crm_model->count_email('noleido');
  }

  function index()
  {
    $this->data['vista'] = '';
    $this->data['titulo'] = 'Dashboard';
    $this->data['sub'] = '';
    $this->data['bread'] = array(
      'Dashboard' => base_url()
    );
    $this->load->view('layout/inicio');
  }

  public function productos()
  {
    $this->data['vista'] = 'admin/productos/table';
    $this->data['titulo'] = 'Productos';
    $this->data['sub'] = '';
    $this->data['bread'] = array(
      'Dashboard' => base_url(),
      'Productos' => base_url('productos'),
    );
    // var_dump($this->data['vista']);
    $this->load->view('layout/inicio');
  }

  public function producto($id = 0, $type = '')
  {
    $this->data['vista'] = 'admin/productos/editar';
    $this->data['titulo'] = 'Producto/Editar';
    $this->data['sub'] = '';
    $this->data['bread'] = array(
      'Dashboard' => base_url(),
      'Productos' => base_url('productos'),
      'Producto - Editar' => base_url("productos/{$id}/editar"),
    );

    $this->data['record'] = false;
    if ($type == 'editar') {
      $this->data['record'] = $this->productos_model->get($id);
    } elseif ($type == 'nuevo') {
     $this->data['record'] = false;
    }

    $this->load->view('layout/inicio');
  }

  
  
  public function pedidos()
  {
      
    $this->data['vista'] = 'admin/pedidos/table';
    $this->data['titulo'] = 'Pedidos';
    $this->data['sub'] = '';
    $this->data['bread'] = array(
      'Dashboard' => base_url(),
      'Pedidos' => base_url('pedidos'),

    );
    $this->load->view('layout/inicio');
    
  }
  

  public function pedido($num = 0)
  {
      $this->data['record'] = $this->pedidos_model->get($num);
      $this->data['vista'] = 'admin/pedidos/invoice';
      $this->data['titulo'] = "Pedido";
      $this->data['sub'] = " # {$num}";
      $this->data['bread'] = array(
        'Dashboard' => base_url(),
        'Pedidos' => base_url('pedidos'),
        'Pedido - Detalle' => base_url("pedido/{$num}"),
      );
      $this->load->view('layout/inicio');
  }

 public function productos_primeros()
  {
    $this->data['seleccionados'] = $this->process_model->get_all('productos_lugares');
    $this->data['record'] = false;
    $this->data['productos'] = $this->productos_model->get_all();
    $this->data['vista'] = 'admin/productos/primeros';
    $this->data['titulo'] = "Primeros lugares";
    $this->data['sub'] = "";
    $this->data['bread'] = array(
      'Dashboard' => base_url(),
      'Productos' => base_url('productos'),
      'Productos - Lugares' => base_url("productos/lugares"),
    );
    $this->load->view('layout/inicio');
  }

  public function productos_alcantarillado()
  {
    $this->data['productos'] = $this->process_model->get_all('t_alcantarillado');
    $this->data['vista'] = 'admin/productos/alcantarillado';
    $this->data['titulo'] = 'Productos - Alcantarillado';
    $this->data['sub'] = '';
    $this->data['bread'] = array(
      'Dashboard' => base_url(),
      'Productos' => base_url('alcantarillado'),
    );
    // var_dump($this->data['vista']);
    $this->load->view('layout/inicio');
  }

  public function productos_derivacion()
  {
    $this->data['productos'] = $this->process_model->get_all('t_diametro_derivacion');
    $this->data['vista'] = 'admin/productos/derivacion';
    $this->data['titulo'] = 'Productos - Derivacion';
    $this->data['sub'] = '';
    $this->data['bread'] = array(
      'Dashboard' => base_url(),
      'Productos' => base_url('derivacion'),
    );
    // var_dump($this->data['vista']);
    $this->load->view('layout/inicio');
  }

  public function productos_filtros()
  {
    $this->data['productos'] = $this->process_model->get_all('t_filtros');
    $this->data['vista'] = 'admin/productos/filtros';
    $this->data['titulo'] = 'Productos - Filtros';
    $this->data['sub'] = '';
    $this->data['bread'] = array(
      'Dashboard' => base_url(),
      'Productos' => base_url('derivacion'),
    );
    // var_dump($this->data['vista']);
    $this->load->view('layout/inicio');
  }

  public function productos_medidores()
  {
    $this->data['productos'] = $this->process_model->get_all('t_medidores');
    $this->data['vista'] = 'admin/productos/medidores';
    $this->data['titulo'] = 'Productos - Medidores';
    $this->data['sub'] = '';
    $this->data['bread'] = array(
      'Dashboard' => base_url(),
      'Productos' => base_url('medidores'),
    );
    // var_dump($this->data['vista']);
    $this->load->view('layout/inicio');
  }

  public function productos_new()
  {
    $this->data['productos'] = $this->process_model->get_all('t_producto');
    $this->data['vista'] = 'admin/productos/productos_new';
    $this->data['titulo'] = 'Productos - Reparación de tubería';
    $this->data['sub'] = '';
    $this->data['bread'] = array(
      'Dashboard' => base_url(),
      'Productos' => base_url('productos_nuevo'),
    );
    // var_dump($this->data['vista']);
    $this->load->view('layout/inicio');
  }

  public function productos_valvulas()
  {
    $this->data['productos'] = $this->process_model->get_all('t_valvulas');
    $this->data['vista'] = 'admin/productos/valvulas';
    $this->data['titulo'] = 'Productos - Valvulas';
    $this->data['sub'] = '';
    $this->data['bread'] = array(
      'Dashboard' => base_url(),
      'Productos' => base_url('valvulas'),
    );
    // var_dump($this->data['vista']);
    $this->load->view('layout/inicio');
  }

  public function contacto()
  {
    $this->data['correos'] = $this->crm_model->get_correos();
    $this->data['vista'] = 'admin/contacto/list';
    $this->data['titulo'] = 'Mailbox';
    $this->data['sub'] = '';
    $this->data['bread'] = array(
      'Dashboard' => base_url()
    );
    $this->load->view('layout/inicio');
  }

  public function contacto_correo($id = 0)
  {
    $correo_visto = $this->process_model->update('contact_form', array('readed' => 1),'id', $id);

    $this->data['correo'] = $this->crm_model->get_correo($id);
    $this->data['vista'] = 'admin/contacto/correo';
    $this->data['titulo'] = 'Mailbox';
    $this->data['sub'] = '';
    $this->data['bread'] = array(
      'Dashboard' => base_url()
    );
    $this->load->view('layout/inicio');
  }

  public function enviar($id = 0)
  {
    $this->data['correo'] = $this->crm_model->get_correo($id);
    $this->data['vista'] = 'admin/contacto/enviar';
    $this->data['titulo'] = 'Responder correo';
    $this->data['sub'] = '';
    $this->data['bread'] = array(
      'Dashboard' => base_url()
    );
    $this->load->view('layout/inicio');
  }

  public function contra_incendios()
  {
    $this->data['productos'] = $this->process_model->get_all('tbl_contra_incendio');
    $this->data['vista'] = 'admin/productos/contra_incendios';
    $this->data['titulo'] = 'Productos - Contra incendios';
    $this->data['sub'] = '';
    $this->data['bread'] = array(
      'Dashboard' => base_url(),
      'Productos' => base_url('derivacion'),
    );
    // var_dump($this->data['vista']);
    $this->load->view('layout/inicio');
  }

  
    public function precio_usuarios()
  {
    $this->data['precio_user'] = $this->process_model->precio_por_usuario();
    
//    var_dump( $this->data['precio_user'] );
    $this->data['vista'] = 'admin/precio_usuarios/precio_usuario';
    $this->data['titulo'] = 'Precios de Usuarios';
    $this->data['sub'] = '';
    $this->data['bread'] = array(
      'Dashboard' => base_url()
    );    
    $this->load->view('layout/inicio');
  }
  
  public function editar_precio_usuarios()
  {
 
    $this->data['precio_list'] = $this->process_model->precio_lista();
    $this->data['vista'] = 'admin/precio_usuarios/editar_precio_usuarios';
    $this->data['titulo'] = 'Editar precio de usuarios';
    $this->data['sub'] = '';
    $this->data['bread'] = array(
      'Dashboard' => base_url()
    );    
    $this->load->view('layout/inicio');
  }
  
  
      public function lista_precios()
  {
    $this->data['precio_list'] = $this->process_model->precio_lista();
    
//    var_dump( $this->data['precio_user'] );
    $this->data['vista'] = 'admin/precio_usuarios/lista_precios';
    $this->data['titulo'] = 'Lista de Precios';
    $this->data['sub'] = '';
    $this->data['bread'] = array(
      'Dashboard' => base_url()
    );    
    $this->load->view('layout/inicio');
  }
  
  public function ingresa_precio()
  {
  //  $this->data['precio_list'] = $this->process_model->precio_lista();    
//    var_dump( $this->data['precio_user'] );
    $this->data['vista'] = 'admin/precio_usuarios/ingresa_precio';
    $this->data['titulo'] = 'Ingresar un nuevo Precio';
    $this->data['sub'] = '';
    $this->data['bread'] = array(
      'Dashboard' => base_url()
    );    
    $this->load->view('layout/inicio');
  }
  
  public function editar_precio()
  {
    $id_tipo = ( $this->uri->segment(2) != null)?  $this->uri->segment(2) : '';
     $this->data['precio_tipo']  = $this->process_model->editar_precio_lista($id_tipo);
   //   var_dump($this->data['precio_tipo']);
    $this->data['vista'] = 'admin/precio_usuarios/editar_precio';
    $this->data['titulo'] = 'Editar precio';
    $this->data['sub'] = '';
    $this->data['bread'] = array(
      'Dashboard' => base_url()
    );
    $this->load->view('layout/inicio');
      
  }
  
  public function ingresa_precio_db()
  {
      
     $this->load->model(array('precios_model'));
     $nombre = ($this->input->post('nombre_precio') != null)? $this->input->post('nombre_precio') : '';   
     $desc = ($this->input->post('desc') != null)? $this->input->post('desc') : '';     
     $response =  $this->precios_model->insert_prices($nombre,$desc);
       if($response){
          $this->session->set_flashdata('msg_success', '<div class="alert alert-danger text-center">El proceso se ha realizado con éxito.</div>');
                redirect(base_url('ingresa_precio/'));
     }
//     insert_pirces
  }
  
  public function editar_precio_db()
  {        
     $this->load->model(array('precios_model'));
     $id  = ($this->input->post('id') != null)? $this->input->post('id') : '';   
     $nombre = ($this->input->post('nombre_precio') != null)? $this->input->post('nombre_precio') : '';   
     $desc = ($this->input->post('desc') != null)? $this->input->post('desc') : '';     
     $response =  $this->precios_model->update_prices($id,$nombre,$desc);
     if($response){
          $this->session->set_flashdata('msg_success', '<div class="alert alert-danger text-center">El proceso se ha realizado con éxito.</div>');
                redirect(base_url('editar_precio/'.$id));
     }
  }
  
   public function precio_producto()
  {
   
    $this->data['precio_prod'] = $this->process_model->get_all('tbl_codigos');
    $this->data['vista'] = 'admin/precio_producto/precio_producto';
    $this->data['titulo'] = 'Precios de los productos';
    $this->data['sub'] = '';
    $this->data['bread'] = array(
      'Dashboard' => base_url(),
      'Productos' => base_url('precio_producto'),
    );
      $this->load->view('layout/inicio');
    
  }
  
  
  public function ingresa_precio_usuario()
  {
    $this->data['vista'] = 'admin/precio_usuarios/ingresa_precio_usuario';
    $this->data['titulo'] = 'Ingresar Precio a Usuario';
    $this->data['sub'] = '';
    $this->data['bread'] = array(
      'Dashboard' => base_url()
    );    
    $this->load->view('layout/inicio');
     
  }

  public function conversion_dollar()
  {
    $this->data['dollar'] = $this->process_model->get_all('tbl_conversion_dollar');
    $this->data['vista'] = 'admin/conversion_dollar/conversion_dollar';
    $this->data['titulo'] = 'Conversion de Dollar a Pesos';
    $this->data['sub'] = '';
    $this->data['bread'] = array(
      'Dashboard' => base_url()
    );    
    $this->load->view('layout/inicio');
  }
  
  public function update_dollar(){
       $d = ($this->input->post('dollar') != null)? $this->input->post('dollar') : '';
       
       if($d != ""){
        $response = $this->process_model->update('tbl_conversion_dollar',array('PrecioPesos'=>$d),'id',1);
        
        if($response){
             $this->session->set_flashdata('msg_success', '<div class="alert alert-danger text-center">El proceso se ha realizado con éxito.</div>');
                redirect(base_url('conversion_dollar'));
        }
        
       }
       
  }


  public function  ingresa_precio_usuario_db()
    {
       $id_user = ($this->input->post('user_id') != null)? $this->input->post('user_id') : '';
       $id_tipo = ($this->input->post('tipo') != null)? $this->input->post('tipo') : '';
       $response = $this->process_model->tipo_precio_usuario($id_user,$id_tipo);
       
       if($response){
               $this->session->set_flashdata('msg_success', '<div class="alert alert-danger text-center">El proceso se ha realizado con éxito.</div>');
                redirect(base_url('editar_precio_usuarios/'.$id_user));
       }
 //UsuarioId
//TipoDePrecioId
        
    }
  
    
  public function real_excel()
  {

    require getcwd().'/application/libraries/PHPExcel/Classes/PHPExcel.php';
//    require_once(APPPATH."libraries/PHPExcel/Classes/PHPExcel.php");
    $config['upload_path'] = 'upload/';
    $config['allowed_types'] = 'gif|jpg|png|xls|xlsx';
    $config['max_size'] = 10000;
    $config['max_width'] = 1024;
    $config['max_height'] = 768;
    $this->load->library('upload', $config);
    if (!$this->upload->do_upload('excel'))
    {
                 // Error
                 // echo "Ocurrio un error";
                 //       $error = array('error' => $this->upload->display_errors());
                 //       var_dump($error);
                       //
                       // $this->load->view('upload_form', $error);
    }else{
    $data = $this->upload->data();
                 // echo "<pre>";
                 // print_r($this->upload->data());
                 // echo "</pre>";
                       // $data = array('upload_data' => $this->upload->data());
                       //
                       // $this->load->view('upload_success', $data);
   }

    $objPHPExcel = PHPExcel_IOFactory::load($data['full_path']);
    $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);

    $repeticiones = 0;
//    $titulos = $sheetData[0];
//    unset($titulos['A']);
//    unset($titulos['B']);
//    unset($sheetData[0]);
     // var_dump($sheetData);
  //  $clear = $this->proces_model->clear('tbl_codigos');
     $this->db->from('tbl_codigos'); 
    $this->db->truncate(); 
    foreach ($sheetData as $row) {
       $data = array(
         'SKU' => $row['A'],
         'UMB' => $row['B'],
         'Precio' => $row['C'],
         'Existencia' => $row['D'],
         'TipoMoneda'=> $row['E']
        );
        
//        foreach ($titulos as $index => $value)
//        {
//         print_r($index);
//         $inventarios_id = explode(' ', $value);
//        }
        
      
        $insert = $this->process_model->insert('tbl_codigos',$data);
    }

     redirect(base_url('precio_producto'), 'refresh');

  }
  public function real_excel_2()
  {
       require_once(APPPATH."libraries/PHPExcel/Classes/PHPExcel.php");
        $this->excel = new PHPExcel(); 
        $file_info = pathinfo($_FILES["result_file"]["name"]);
        $file_directory = "upload/";
        $new_file_name = date("d-m-Y ") . rand(000000, 999999) .".". $file_info["extension"];

        if(move_uploaded_file($_FILES["result_file"]["tmp_name"], $file_directory . $new_file_name))
        {   
            $file_type	= PHPExcel_IOFactory::identify($file_directory . $new_file_name);
            $objReader	= PHPExcel_IOFactory::createReader($file_type);
            $objPHPExcel = $objReader->load($file_directory . $new_file_name);
            $sheet_data	= $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
            foreach($sheet_data as $data)
            {
                $result = array(
                 'SKU' => $data['A'],
                 'UMB' => $data['B'],
                 'Precio' => $data['C'],
                 'Existencia' => $data['D'],
                 'TipoMoneda'=> $data['E']
                );

                $insert = $this->process_model->insert('tbl_codigos',$result);
            }
        }
    

  }





    
}
