<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Process extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
    $this->load->model(array('process_model'));
  }

  function index()
  {

  }

  public function productos_()
  {
    echo "<pre>";
    print_r($_POST);
    echo "</pre>";
  }

  public function productos()
  {
    // echo "<pre>";
    // print_r($_POST);
    // echo "</pre>";
    // $opciones = array();
    $opciones = implode(',', $_POST['Evento']);
    $data = array();

    if (!isset($_POST['Estatus'])) {
      $estatus = 0;
    } else {
      $estatus = $this->input->post('Estatus');
    }
    $data['SKU'] = $this->input->post('SKU');
    $data['Nombre'] = $this->input->post('Nombre');
    $data['NombreIngles'] = $this->input->post('NombreIngles');
    $data['Costo'] = $this->input->post('Costo');
    $data['Precio'] = $this->input->post('Precio');
    $data['PrecioDollar'] = $this->input->post('PrecioDollar');
    $data['Disponibilidad'] = $this->input->post('Disponibilidad');
    $data['Largo'] = $this->input->post('Largo');
    $data['Alto'] = $this->input->post('Alto');
    $data['Medida'] = $this->input->post('Medida');
    $data['Material'] = $this->input->post('Material');
    $data['Ancho'] = $this->input->post('Ancho');
    $data['Peso'] = $this->input->post('Peso');
    $data['MedidaIngles'] = $this->input->post('MedidaIngles');
    $data['Empaque'] = $this->input->post('Empaque');
    $data['Material_Ingles'] = $this->input->post('Material_Ingles');
    $data['Descripcion'] = $this->input->post('Descripcion');
    $data['DescripcionIngles'] = $this->input->post('DescripcionIngles');
    $data['Evento'] = $opciones;
    $data['Estatus'] = $estatus;

    if (isset($_POST['id'])) {
      $response = $this->process_model->update('tbl_productos', $data, 'IdProducto', $_POST['id']);
    } else {
      $response = $this->process_model->insert($data);
    }

    if ($response) {
      $valid = true;
    } else {
      $valid = false;
    }

    $respuesta['valid'] = $valid;
    $respuesta['reload'] = true;

    echo json_encode($respuesta);

  }

  public function guardar_lugares()
  {
    $data['data'] = json_encode($_POST);
    $response = $this->process_model->update('productos_lugares', $data,'id', '4');
    // $data['data'] = json_encode($_POST);
    $response = $this->process_model->update_no_w('tbl_productos',array('orden' => 100));
    foreach ($_POST['productos'] as $key => $value) {
      // var_dump($value);
      $response = $this->process_model->update('tbl_productos', array('orden' => $key),'IdProducto', $value);
      // echo "<pre>";
      // print_r($value);
      // echo "</pre>";

    }
    // $respuesta['valid'] = $response;
    // $respuesta['reload'] = true;

    // echo json_encode($respuesta);
  }

  public function guarda_usuario()
  {

  }

  public function enviar_correo()
  {

    $this->load->helper(array('email_helper'));
    $data['mensaje'] = $this->input->post('mensaje');
    send_email($this->input->post('correo'), $this->input->post('asunto'), 'frontend/emails/generic', $data,'KSH');
    // echo $email;
    echo json_encode(array('valid' => true, 'mensaje' => 'Correo enviado correctamente'));
  }

}
