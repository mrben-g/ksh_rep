<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
    $this->load->model(array('auth_model'));
  }

  function index()
  {
    $this->load->view('layout/login');
  }

  public function login()
  {
   $data = array(
      'usuario' => $this->input->post('user'),
      'password' => $this->input->post('pass')
    );

    $auth = $this->auth_model->login($data);

    // var_dump($auth);
    echo json_encode($auth);
  }

  public function log_out()
  {
       //session_destroy();
       $_SESSION['login']['admin'] = null;
        $_SESSION['login']['rol'] = null;
      redirect(base_url('admin/login'), 'refresh');
  }

  public function register_test()
  {
      $data = array(
        'nombre' => 'admin',
        'email' => 'admin@admin.com',
        'password' => $this->encryption->encrypt('admin'),
        'rol_id' => 1,
        'created_at' => date('Y-m-d H:i:s'),
        'created_by' => 1
      );
      $response = $this->auth_model->insert($data);
      var_dump($response);
  }



}
