<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Crm extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
    $this->load->model(array('productos_model','pedidos_model','process_model','crm_model'));
    $this->data['nuevos_correos'] = $this->crm_model->count_email('noleido');
  }

  function index()
  {

  }

  public function cotizaciones()
  {
    $this->data['cotizaciones'] = $this->crm_model->get_cotizaciones();
    $this->data['vista'] = 'crm/cotizaciones/list';
    $this->data['titulo'] = 'Productos - Alcantarillado';
    $this->data['sub'] = '';
    $this->data['bread'] = array(
      'Dashboard' => base_url(),
      'Productos' => base_url('alcantarillado'),
    );
    // var_dump($this->data['vista']);
    $this->load->view('layout/inicio');
  }

}
