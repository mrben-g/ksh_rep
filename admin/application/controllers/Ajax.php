<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends MY_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
    $this->load->model(array('productos_model', 'pedidos_model','crm_model'));
  }

  function index()
  {

  }

  public function productos()
  {
      
$start = $this->input->get('start');
$length = $this->input->get('length');
$columns = array(
     0 => "tbl_productos.IdProducto",
     1 => "tbl_productos.Nombre",
     2 => "tbl_productos.NombreIngles",
     3 => "tbl_productos.Precio",
     4 => "tbl_productos.PrecioDollar",
     5 => "tbl_productos.id"
    );
$search = $this->input->get('search');

//ORDER
$order = $this->input->get('order');
$column_order = (isset($order[0]) && isset($order[0]['column']) ? $columns[$order[0]['column']] : $columns[0]);
$column_direction = (isset($order[0]) && isset($order[0]['dir']) ? $order[0]['dir'] : 'asc');

// Create search
$records = $this->productos_model->get_records($start, $length, $column_order, $column_direction, $search);
$records_all = $this->productos_model->get_records(0, 1000, $column_order, $column_direction, $search);

$total = count($records);

$arrayOut = array(
"draw"            => intval($this->input->get('draw')),
"recordsTotal"    => count($records_all),
"recordsFiltered" => count($records_all),
"data"            => array()
);

$infoArrayx = array();

foreach ($records as $row)
{
// $username = $this->ion_auth->user()->row();

$infoArray = array();
$infoArray[0] = $row->IdProducto;
$infoArray[1] = $row->Nombre;
$infoArray[2] = $row->NombreIngles;
$infoArray[3] = $row->Precio;
$infoArray[4] = $row->PrecioDollar;
$bufferButtons = '';

$bufferButtons .= '<div class="btn-group">
<a href="'.base_url("producto/{$row->IdProducto}/editar").'" class="btn btn-primary">Editar</a>
<a href="'.base_url("producto/{$row->IdProducto}/eliminar").'" class="btn btn-danger">Eliminar</a>

</div>';
$infoArray[5] = '<div class="text-center" style="margin: 0 auto; width: 200px;">' . $bufferButtons . '</div>';
$infoArrayx[] = $infoArray;
}

		$arrayOut['data'] = $infoArrayx;
		echo json_encode($arrayOut);
  }

  public function contra_incendios()
  {
    $start = $this->input->get('start');
    $length = $this->input->get('length');
    $columns = array(
          0 => "tbl_contra_incendio.id",
          1 => "tbl_contra_incendio.SKU",
          2 => "tbl_contra_incendio.Nombre",
          3 => "tbl_contra_incendio.AperturaValvula",
          4 => "tbl_contra_incendio.TamanoValvula",
          5 => "tbl_contra_incendio.NumeroVias",
          6 => "tbl_contra_incendio.LargoBarril",
          7 => "tbl_contra_incendio.TipoJunta",
          8 => "tbl_contra_incendio.id"
         );
    $search = $this->input->get('search');

    //ORDER
    $order = $this->input->get('order');
    $column_order = (isset($order[0]) && isset($order[0]['column']) ? $columns[$order[0]['column']] : $columns[0]);
    $column_direction = (isset($order[0]) && isset($order[0]['dir']) ? $order[0]['dir'] : 'asc');

    // Create search
    $records = $this->productos_model->get_records($start, $length, $column_order, $column_direction, $search);
    $records_all = $this->productos_model->get_records(0, 1000, $column_order, $column_direction, $search);

    $total = count($records);

    $arrayOut = array(
      "draw"            => intval($this->input->get('draw')),
      "recordsTotal"    => count($records_all),
      "recordsFiltered" => count($records_all),
      "data"            => array()
    );

    $infoArrayx = array();



    foreach ($records as $row)
    {
      // $username = $this->ion_auth->user()->row();

      $infoArray = array();
      $infoArray[0] = $row->IdProducto;
      $infoArray[1] = $row->Nombre;
      $infoArray[2] = $row->NombreIngles;
      $infoArray[3] = $row->Precio;
      $infoArray[4] = $row->PrecioDollar;
      $bufferButtons = '';

      $bufferButtons .= '<div class="btn-group">
                      <a href="'.base_url("producto/{$row->IdProducto}/editar").'" class="btn btn-primary">Editar</a>
                      <a href="'.base_url("producto/{$row->IdProducto}/eliminar").'" class="btn btn-danger">Eliminar</a>
                    </div>';   

      $infoArray[5] = '<div class="text-center" style="margin: 0 auto; width: 200px;">' . $bufferButtons . '</div>';
      $infoArrayx[] = $infoArray;
    }

    $arrayOut['data'] = $infoArrayx;
    echo json_encode($arrayOut);
  }

public function pedidos()
{
    $start = $this->input->get('start');
    $length = $this->input->get('length');
    $columns = array(
        0 => "t_ventas.IdPedido",
        1 => "tbl_usuarios.Nombre",
        2 => "t_ventas.FechaPedido",
        3 => "t_ventas.TotalCompra",
        4 => "t_ventas.IdPedido"
    );
    
    $search = $this->input->get('search');
    //ORDER
    $order = $this->input->get('order');
    $column_order = (isset($order[0]) && isset($order[0]['column']) ? $columns[$order[0]['column']] : $columns[0]);
    $column_direction = (isset($order[0]) && isset($order[0]['dir']) ? $order[0]['dir'] : 'asc');

    // Create search
    $records = $this->pedidos_model->get_records($start, $length, $column_order, $column_direction, $search);
    $records_all = $this->pedidos_model->get_records(0, 1000, $column_order, $column_direction, $search);
    $total = count($records);
    $arrayOut = array(
            "draw"            => intval($this->input->get('draw')),
            "recordsTotal"    => count($records_all),
            "recordsFiltered" => count($records_all),
            "data"            => array()
    );
    $infoArrayx = array();

foreach ($records as $row)
{
$infoArray = array();
$infoArray[0] = $row->IdPedido;
$infoArray[1] = 'Invitado';
$infoArray[2] = $row->FechaPedido;
$infoArray[3] = $row->TotalCompra;
$bufferButtons = '';
$bufferButtons .= '<div class="btn-group"><a href="'.base_url("pedido/{$row->CodigoId}").'" class="btn btn-primary">Ver</a></div>';
$infoArray[4] = '<div class="text-center" style="margin: 0 auto; width: 200px;">' . $bufferButtons . '</div>';
$infoArrayx[] = $infoArray;
}

$arrayOut['data'] = $infoArrayx;
echo json_encode($arrayOut);
  }

  public function cotizacion_crm()
  {
    $id = $this->input->post('id');
    $response = $this->crm_model->get_cotizacion($id);
    // echo "<pre>";
    // print_r($response);
    // echo "</pre>";
    echo json_encode(array('buffer' => $response->InteresadoEn));
  }
  
  public function precio_producto()
  {
//    $this->db->select('*');
//    $this->db->from('tbl_codigos');
//    $query = $this->db->get();
//     return ($query->num_rows() > 0)?$query->result():FALSE;

     $start = $this->input->get('start');
$length = $this->input->get('length');
$columns = array(
     0 => "tbl_codigos.IdCodigo",
     1 => "tbl_codigos.SKU",
     2 => "tbl_codigos.UMB",
     3 => "tbl_codigos.Precio",
     4 => "tbl_codigos.Existencia",
     5 => "tbl_codigos.TipoMoneda"
    );
$search = $this->input->get('search');

//ORDER
$order = $this->input->get('order');
$column_order = (isset($order[0]) && isset($order[0]['column']) ? $columns[$order[0]['column']] : $columns[0]);
$column_direction = (isset($order[0]) && isset($order[0]['dir']) ? $order[0]['dir'] : 'asc');
// Create search
$records = $this->productos_model->get_records($start, $length, $column_order, $column_direction, $search);
$records_all = $this->productos_model->get_records(0, 1000, $column_order, $column_direction, $search);
$total = count($records);

$arrayOut = array(
"draw"            => intval($this->input->get('draw')),
"recordsTotal"    => count($records_all),
"recordsFiltered" => count($records_all),
"data"            => array()
);

$infoArrayx = array();

foreach ($records as $row)
{
$infoArray = array();
$infoArray[0] = $row->IdCodigo;
$infoArray[1] = $row->SKU;
$infoArray[2] = $row->UMB;
$infoArray[3] = $row->Precio;
$infoArray[4] = $row->Existencia;
$infoArray[5] = $row->TipoMoneda;
$bufferButtons = '';
$bufferButtons .= '<div class="btn-group">
<a href="'.base_url("producto/{$row->IdCodigo}/editar").'" class="btn btn-primary">Editar</a>
<a href="'.base_url("producto/{$row->IdCodigo}/eliminar").'" class="btn btn-danger">Eliminar</a>
</div>';

$infoArray[5] = '<div class="text-center" style="margin: 0 auto; width: 200px;">' . $bufferButtons . '</div>';
$infoArrayx[] = $infoArray;

}

$arrayOut['data'] = $infoArrayx;
echo json_encode($arrayOut);
     
    
   
  
  }

  
  public function filtro_tuberia_reparacion()
  {
   $this->load->model(array('productos_model'));
   $id = ($this->input->post('id') != null)? $this->input->post('id') : '';
   $this->data['product']= $this->productos_model->getfiltroProducto($id);
//   var_dump($this->data['product']);
   if ($this->input->is_ajax_request()) {
   $vista = $this->load->view('admin/productos/ajax_producto',array(),true);                      
      echo $vista;          
      
   }
      
      
  }
  
  

}
