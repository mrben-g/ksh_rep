<?php //session_destroy(); 

 if(!isset($_SESSION['login']['admin'])){
        redirect(base_url(''));
   }
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>KSH :: Kuroda soluciones hidráuilicas  | Admin</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url('public') ?>/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url('public') ?>/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url('public') ?>/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <!-- Select2 -->
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
  <link rel="stylesheet" href="<?php echo base_url('public') ?>/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url('public') ?>/dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="<?php echo base_url('public') ?>/bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo base_url('public') ?>/bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?php echo base_url('public') ?>/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url('public') ?>/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?php echo base_url('public') ?>/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <!-- Data tables -->
  <link rel="stylesheet" href="<?php echo base_url('public') ?>/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!--<link rel="stylesheet" href="<?php echo base_url('public/swich') ?>/switchery.css" />-->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.css" />

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="<?php echo base_url('') ?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>KSH</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg">KSH <b></b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
            
          <!-- Messages: style can be found in dropdown.less--          
          <li class="dropdown messages-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-envelope-o"></i>
              <span class="label label-success">4</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 4 messages</li>
              <li>
                <!-- inner menu: contains the actual data --
                <ul class="menu">
                  <li><!-- start message --
                    <a href="#">
                      <div class="pull-left">
                        <img src="<?php echo base_url('public') ?>/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        Support Team
                        <small><i class="fa fa-clock-o"></i> 5 mins</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <!-- end message --
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="<?php echo base_url('public') ?>/dist/img/user3-128x128.jpg" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        AdminLTE Design Team
                        <small><i class="fa fa-clock-o"></i> 2 hours</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="<?php echo base_url('public') ?>/dist/img/user4-128x128.jpg" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        Developers
                        <small><i class="fa fa-clock-o"></i> Today</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="<?php echo base_url('public') ?>/dist/img/user3-128x128.jpg" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        Sales Department
                        <small><i class="fa fa-clock-o"></i> Yesterday</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="<?php echo base_url('public') ?>/dist/img/user4-128x128.jpg" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        Reviewers
                        <small><i class="fa fa-clock-o"></i> 2 days</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                </ul>
              </li>
              <li class="footer"><a href="#">See All Messages</a></li>
            </ul>
          </li>-->
          
          <!-- Notifications: style can be found in dropdown.less --
          <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning">10</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 10 notifications</li>
              <li>
                <!-- inner menu: contains the actual data --
                <ul class="menu">
                  <li>
                    <a href="#">
                      <i class="fa fa-users text-aqua"></i> 5 new members joined today
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-warning text-yellow"></i> Very long description here that may not fit into the
                      page and may cause design problems
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-users text-red"></i> 5 new members joined
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-shopping-cart text-green"></i> 25 sales made
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-user text-red"></i> You changed your username
                    </a>
                  </li>
                </ul>
              </li>
              <li class="footer"><a href="#">View all</a></li>
            </ul>
          </li>-->
          
          <!-- Tasks: style can be found in dropdown.less --
          <li class="dropdown tasks-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-flag-o"></i>
              <span class="label label-danger">9</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 9 tasks</li>
              <li>
                <!-- inner menu: contains the actual data --
                <ul class="menu">
                  <li><!-- Task item --
                    <a href="#">
                      <h3>
                        Design some buttons
                        <small class="pull-right">20%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar"
                             aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">20% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item --
                  <li><!-- Task item --
                    <a href="#">
                      <h3>
                        Create a nice theme
                        <small class="pull-right">40%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-green" style="width: 40%" role="progressbar"
                             aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">40% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item --
                  <li><!-- Task item --
                    <a href="#">
                      <h3>
                        Some task I need to do
                        <small class="pull-right">60%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-red" style="width: 60%" role="progressbar"
                             aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">60% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item --
                  <li><!-- Task item --
                    <a href="#">
                      <h3>
                        Make beautiful transitions
                        <small class="pull-right">80%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-yellow" style="width: 80%" role="progressbar"
                             aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">80% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item --
                </ul>
              </li>
              <li class="footer">
                <a href="#">View all tasks</a>
              </li>
            </ul>
          </li>-->
          
          
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <!-- <img src="dist/img/user2-160x160.jpg" class="user-image" alt="User Image"> -->
              <span class="hidden-xs"><?php echo $_SESSION['login']['admin']; ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <!-- <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image"> -->

                <p>
                  <?php echo $_SESSION['login']['admin'] ?> Administrador
                  <!-- <small>Member since Nov. 2012</small> -->
                </p>
              </li>
              <!-- Menu Body --
              <li class="user-body">
               <!-- <div class="row">
                  <div class="col-xs-4 text-center">
                    <a href="#">Followers</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Sales</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Friends</a>
                  </div>
                </div>-->
                <!-- /.row --
              </li>-->
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <!--<a href="#" class="btn btn-default btn-flat">Perfil</a>-->
                </div>
                <div class="pull-right">
                  <a href="<?php echo base_url('salir') ?>" class="btn btn-default btn-flat">Cerrar sesión</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <!-- <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li> -->
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
          <div class="pull-left image" style="width:36px;height: 36px;">
          <!--<img src="<?php echo base_url('public') ?>/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">-->
        </div>
        <div class="pull-left info">

          <p><?php echo $_SESSION['login']['admin']; ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form --
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <?php
        $route_aparte = implode('/', $this->uri->segments);
        $route_actual = base_url($route_aparte);
         ?>
        <!-- <li class="header"></li> -->
        <li class="treeview <?php if ($this->uri->segment(1) == 'productos' || $this->uri->segment(1) == 'alcantarillado' || $this->uri->segment(1) == 'derivacion'|| $this->uri->segment(1) == 'filtros'|| $this->uri->segment(1) == 'medidores'|| $this->uri->segment(1) == 'productos_nuevo'|| $this->uri->segment(1) == 'valvulas' ): ?>menu-open<?php endif;  ?> ">
          <a href="#">
            <i class="fa fa-opencart"></i> <span>productos</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu" style="<?php if ($this->uri->segment(1) == 'productos' || $this->uri->segment(1) == 'alcantarillado' || $this->uri->segment(1) == 'derivacion'|| $this->uri->segment(1) == 'filtros'|| $this->uri->segment(1) == 'medidores'|| $this->uri->segment(1) == 'productos_nuevo'|| $this->uri->segment(1) == 'valvulas' ): ?>display: block;<?php endif;  ?>">
            <li class="<?php echo ($route_actual == base_url('alcantarillado'))? 'active' : '' ?>"><a href="<?php echo base_url('alcantarillado') ?>"><i class="fa fa-circle-o"></i> Alcantarillado</a></li>
            <li class="<?php echo ($route_actual == base_url('derivacion'))? 'active' : '' ?>"><a href="<?php echo base_url('derivacion') ?>"><i class="fa fa-circle-o"></i> Derivacion</a></li>
            <li class="<?php echo ($route_actual == base_url('filtros'))? 'active' : '' ?>"><a href="<?php echo base_url('filtros') ?>"><i class="fa fa-circle-o"></i> Filtros</a></li>
            <li class="<?php echo ($route_actual == base_url('medidores'))? 'active' : '' ?>"><a href="<?php echo base_url('medidores') ?>"><i class="fa fa-circle-o"></i> Medidores</a></li>
            <li class="<?php echo ($route_actual == base_url('productos_nuevo'))? 'active' : '' ?>"><a href="<?php echo base_url('productos_nuevo') ?>"><i class="fa fa-circle-o"></i> Reparación de tubería</a></li>
            <li class="<?php echo ($route_actual == base_url('valvulas'))? 'active' : '' ?>"><a href="<?php echo base_url('valvulas') ?>"><i class="fa fa-circle-o"></i> Valvulas</a></li>
          </ul>
        </li>
        <li class="treeview <?php if ($this->uri->segment(1) == 'lista_precios' || $this->uri->segment(1) == 'ingresa_precio' || $this->uri->segment(1) == 'ingresa_precio_usuario' || $this->uri->segment(1) == 'precio_usuarios'): ?>menu-open<?php endif;  ?>">
          <a href="<?php echo base_url('precio_usuarios') ?>">
            <i class="fa fa-dollar"></i> <span>Precio de Usuarios</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu" style=" <?php if ($this->uri->segment(1) == 'lista_precios' || $this->uri->segment(1) == 'ingresa_precio' || $this->uri->segment(1) == 'ingresa_precio_usuario' || $this->uri->segment(1) == 'precio_usuarios'): ?>display: block;<?php endif;  ?>">
             <li class="<?php echo ($route_actual == base_url('precio_usuarios'))? 'active' : '' ?>"><a href="<?php echo base_url('precio_usuarios') ?>"><i class="fa fa-circle-o"></i> Precio de usuarios</a></li>
            <li class="<?php echo ($route_actual == base_url('lista_precios'))? 'active' : '' ?>"><a href="<?php echo base_url('lista_precios') ?>"><i class="fa fa-circle-o"></i> Lista de Precio</a></li>
            <li class="<?php echo ($route_actual == base_url('ingresa_precio'))? 'active' : '' ?>"><a href="<?php echo base_url('ingresa_precio') ?>"><i class="fa fa-circle-o"></i> Ingresar Precio</a></li>
            <!--<li class=""><a href="<?php echo base_url('') ?>"><i class="fa fa-circle-o"></i> Modificar Precio</a></li>-->
            <!--<li class="<?php echo ($route_actual == base_url('ingresa_precio_usuario'))? 'active' : '' ?>"><a href="<?php echo base_url('ingresa_precio_usuario') ?>"><i class="fa fa-circle-o"></i> Agregar Precio a Usuario</a></li>-->            
           </ul>
        </li>
        <li class="<?php echo ($route_actual == base_url('precio_producto'))? 'active' : '' ?>">
          <a href="<?php echo base_url('precio_producto') ?>">
            <i class="fa fa-dollar"></i> <span>Precio de Producto</span>
            <span class="pull-right-container">
              <!--<i class="fa fa-angle-left pull-right"></i>-->
            </span>
          </a>          
        </li>
        
           <li class="<?php echo ($this->uri->segment(1) == 'conversion_dollar')? 'active' : '' ; ?>">
          <a href="<?php echo base_url('conversion_dollar') ?>">
            <i class="fa fa-money"></i> <span>Conversión del Dollar</span>
            <span class="pull-right-container">
              <!-- <small class="label pull-right bg-green">new</small> -->
            </span>
          </a>
        </li>
        <li class="<?php echo ($this->uri->segment(1) == 'pedidos')? 'active' : '' ; ?>">
          <a href="<?php echo base_url('pedidos') ?>">
            <i class="fa fa-th"></i> <span>Pedidos</span>
            <span class="pull-right-container">
              <!-- <small class="label pull-right bg-green">new</small> -->
            </span>
          </a>
        </li>

        <li class="<?php echo ($this->uri->segment(1) == 'contacto')? 'active' : '' ; ?>">
          <a href="<?php echo base_url('contacto') ?>">
            <i class="fa fa-th"></i> <span>Contacto</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-green"><?php echo $this->data['nuevos_correos']; ?></small>
            </span>
          </a>
        </li>
      
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- Seccion -->
    <!-- Titulo -->
    <section class="content-header">
      <h1>

          <?php echo $this->data['titulo']; ?>

        <!-- Subtitulo -->
        <small><?php echo $this->data['sub']; ?></small>
      </h1>
      <!-- breadcrumb -->
      <ol class="breadcrumb">
        <?php foreach ($this->data['bread'] as $key => $value): ?>
          <li><a href="<?php echo $value ?>"> <?php echo $key; ?></a></li>
        <?php endforeach; ?>
      </ol>
      <!-- breadcrumb -->
    </section>
    <!-- Seccion -->

    <!-- Main content -->
    <section class="content">

      <!-- Pagina contenido -->
      <?php if ($this->data['vista'] !== ''): ?>

        <?php echo $this->load->view($this->data['vista'], array(), TRUE); ?>
      <?php endif; ?>
      <!-- Pagina contenido -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; <?php echo date('Y'); ?> <a href="http://www.mrwasabi.mx/">Mr. Wasabi</a>.</strong> Todos los derechos Reservados.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-user bg-yellow"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>

                <p>New phone +1(800)555-1234</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>

                <p>nora@example.com</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-file-code-o bg-green"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>

                <p>Execution time 5 seconds</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="label label-danger pull-right">70%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Update Resume
                <span class="label label-success pull-right">95%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-success" style="width: 95%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Laravel Integration
                <span class="label label-warning pull-right">50%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Back End Framework
                <span class="label label-primary pull-right">68%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Allow mail redirect
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Other sets of options are available
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Expose author name in posts
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Allow the user to show his name in blog posts
            </p>
          </div>
          <!-- /.form-group -->

          <h3 class="control-sidebar-heading">Chat Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Show me as online
              <input type="checkbox" class="pull-right" checked>
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Turn off notifications
              <input type="checkbox" class="pull-right">
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Delete chat history
              <a href="javascript:void(0)" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
            </label>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo base_url('public') ?>/bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo base_url('public') ?>/bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url('public') ?>/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="<?php echo base_url('public') ?>/bower_components/raphael/raphael.min.js"></script>
<!--<script src="<?php echo base_url('public') ?>/bower_components/morris.js/morris.min.js"></script>-->
<!-- Sparkline -->
<script src="<?php echo base_url('public') ?>/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- Select2 -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<!-- jvectormap -->
<script src="<?php echo base_url('public') ?>/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo base_url('public') ?>/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url('public') ?>/bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="<?php echo base_url('public') ?>/bower_components/moment/min/moment.min.js"></script>
<script src="<?php echo base_url('public') ?>/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?php echo base_url('public') ?>/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url('public') ?>/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?php echo base_url('public') ?>/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url('public') ?>/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('public') ?>/dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo base_url('public') ?>/dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url('public') ?>/dist/js/demo.js"></script>
<!-- Data Tables -->
<script src="<?php echo base_url('public') ?>/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url('public') ?>/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!--<script src="<?php echo base_url('public/swich') ?>/switchery.js"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.js"></script>
<!-- <script src="<?php echo base_url('public') ?>/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script> -->

  <script type="text/javascript">
    
//    $(document).ready(function() {
//        $('.select2').select2();
//        var elem = document.querySelector('.js-switch');
//         var init = new Switchery(elem);
//    });
//    
  </script>

  <script type="text/javascript">
  $(document).ready(function() {
      
    $('.generic_form_form').submit(function(event) {
      event.preventDefault();
      $.ajax({
        url: $(this).attr('action'),
        type: 'POST',
        dataType: 'json',
        data: $(this).serialize()
      })
      .done(function(response) {
        if (response.valid) {
          if (response.reload) {
            location.reload();
          }
          if (response.mensaje) {
            swal('Correo enviado!',response.mensaje,'success');
             $('#asunto').val("");
             $('#mcompose-textarea').val("");
       
          }
        }
        // console.log("success");
      })
      .fail(function() {
        console.log("error");
      })
      .always(function() {
        // console.log("complete");
      });

    });
  });
  </script>

  <script type="text/javascript">
    $(document).ready(function() {
      $('.refill').click(function(event) {
        var url = $(this).data('refill');
        var target = $(this).data('refilltarget');
        var id = $(this).data('refillid');

        $.ajax({
          url: url,
          type: 'POST',
          dataType: 'json',
          data: {id: id}
        })
        .done(function(response) {
          $('.'+target).empty();
          $('.'+target).append(response.buffer);
          // console.log("success");
        })
        .fail(function() {
          console.log("error");
        })
        .always(function() {
          console.log("complete");
        });

      });
    });
  </script>

  <script type="text/javascript">
    $(document).ready(function() {
        
       $('#simple1').DataTable();

       <?php if ($this->uri->segment(1) == 'responder'): ?>
       $("#compose-textarea").wysihtml5();
       <?php endif; ?>

    });
  </script>

  <?php if ($this->uri->segment(1) == 'productos'): ?>
    <script type="text/javascript">
      $(document).ready(function() {
        $('#example1').DataTable({
          "ajax": '<?php echo base_url('ajax/productos') ?>',
          'paging'      : true,
          'lengthChange': false,
          'searching'   : true,
          'ordering'    : true,
          'info'        : true,
          'autoWidth'   : false
        })
      });
    </script>
  <?php endif; ?>
   

  <?php if ($this->uri->segment(1) == 'pedidos'): ?>
    <script type="text/javascript">
      $(document).ready(function() {
        $('#example1').DataTable({
          "ajax": '<?php echo base_url('ajax/pedidos') ?>',
          'paging'      : true,
          'lengthChange': false,
          'searching'   : true,
          'ordering'    : true,
          'info'        : true,
          'autoWidth'   : false
        })
      });
    </script>
  <?php endif; ?>
    <script>
    
    function MuestraProd(id){
        
                  $.ajax({
            url: '<?php echo site_url('ajax/filtro_tuberia_reparacion') ?>',
            type: 'POST',
            dataType: 'html',
            data:{id:id}
            })
                 .done(function(data) {
                console.log(data);
                $('#product_filter').empty();
                $('#product_filter').append(data);
                 $('#simple1').DataTable();
            })
                 .fail(function(data) {
                    console.log(data);
                    $('#product_filter').empty();
                    $('#product_filter').append(data);
                    console.log("error");
            })
                .always(function() {
                            
            });
    }
    
    </script>

</body>
</html>
