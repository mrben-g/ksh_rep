
  <div class="row">
    <div class="col-xs-12">


      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Tabla de productos</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="simple1" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th></th>
              <th>Fecha</th>
              <th>Nombre</th>
              <th>Teléfono</th>
              <th>Correo</th>
              <th>*</th>
            </tr>
            </thead>
            <tbody>
              <?php foreach ($this->data['cotizaciones'] as $key => $row): ?>
                <tr>
                  <td><?php echo $row->IdSolicitud; ?></td>
                  <td><?php echo $row->Fecha; ?></td>
                  <td><?php echo $row->NombreCliente; ?></td>
                  <td><?php echo $row->Telefono; ?></td>
                  <td><?php echo $row->Correo; ?></td>
                  <td><button data-toggle="modal" data-target="#myModal" type="button" data-refill="<?php echo base_url('ajax/cotizacion_crm') ?>" data-refillid="<?php echo $row->IdSolicitud ?>" data-refilltarget="productos" class="btn btn-large btn-block btn-primary refill">Productos</button></td>
                </tr>
              <?php endforeach; ?>
            </tbody>
            <tfoot>
            <tr>
              <th></th>
              <th>Fecha</th>
              <th>Nombre</th>
              <th>Teléfono</th>
              <th>Correo</th>
              <th>*</th>
            </tr>
            </tfoot>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3721.0255696710856!2d-86.8452364846802!3d21.151380685932136!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8f4c2b8cdb6736d1%3A0x400e54fbf740b96e!2sAurora+21%2C+44%2C+77506+Canc%C3%BAn%2C+Q.R.!5e0!3m2!1ses!2smx!4v1506439044865" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3720.86421469376!2d-86.83611388468012!3d21.1578013859286!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8f4c2bf7b9fa2fdb%3A0xc569fab080f72e77!2zUG9zYWRhIENoaWNow6luIEl0esOh!5e0!3m2!1ses!2smx!4v1506445719863" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
      <!-- Modal -->
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title" id="myModalLabel">Listado de productos</h4>
            </div>
            <div class="modal-body">
              <div class="productos">

              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
              <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
            </div>
          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->

      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
