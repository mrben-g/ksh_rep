 <div class="row">
    <div class="col-xs-12">
        <?php echo form_open_multipart(base_url('admin/update_dollar'), array('id' => 'frm-dollar')) ?>
        <div class="box">
            <div class="box-header">
                <h3 class="box-title"></h3>
            </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="col-md-9">
                 <div class="form-group">
                  <label for="">Precio Dollar</label>
                  
                  <input type="text" class="form-control" name="dollar" id="dollar" value="<?php if($this->data['dollar']): foreach ($this->data['dollar'] as $row): echo $row->PrecioPesos; endforeach; endif; ?>">
                </div>
                  
            </div>
        </div>
         <div class="box-footer">
                <button type="submit" class="btn btn-primary">Actualizar Precio</button>
        </div>
        <?php echo form_close(); ?>
         <div class="alerta">
                <?php echo '<br>'.$this->session->flashdata('msg_success');

                ?>
            </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
