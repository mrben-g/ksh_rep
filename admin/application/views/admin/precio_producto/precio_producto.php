<div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title"></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="row" style="margin-bottom: 20px;">
                 <?php echo form_open_multipart(base_url('admin/real_excel'), array('id' => 'frm-excel' ,'class'=>"form-horizontal")) ?>
                <div class="col-md-3"></div>
                <div class="col-md-3"><input type="file" name="excel" class="form-control" value=""></div>
                <div class="col-md-3"> <button type="submit" class="btn btn-success">Subir productos en existencia</button></div>
                <div class="col-md-3"></div>
                <?php echo form_close(); ?>
            </div>
          <table id="simple1" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th></th>
              <th>SKU</th>
              <th>Unidad de Medida</th>
              <th>Precio</th>
              <th>Existencia</th>
              <th>Tipo de Moneda</th>
              <th>*</th>
            </tr>
            
            </thead>
            <tbody>
              <?php  if($this->data['precio_prod']):foreach ($this->data['precio_prod'] as $row): ?>
                <tr>
                   <td></td>
                  <td><?php echo $row->SKU; ?></td>
                  <td><?php echo $row->UMB; ?></td>
                  <td><?php echo $row->Precio; ?></td>
                  <td><?php echo $row->Existencia; ?></td>
                  <td><?php echo $row->TipoMoneda; ?></td>
                  <td></td>
                </tr>
              <?php  endforeach; endif; ?>
            </tbody>
            <tfoot>
            <tr>
              <th></th>
              <th>SKU</th>
              <th>Unidad de Medida</th>
              <th>Precio</th>
              <th>Existencia</th>
              <th>Tipo de Moneda</th>
              <th>*</th>
            </tr>
            </tfoot>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
