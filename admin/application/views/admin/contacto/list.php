
  <div class="row">
    <div class="col-md-3">
      <!-- <a href="compose.html" class="btn btn-primary btn-block margin-bottom">Compose</a> -->

      <div class="box box-solid">
        <div class="box-header with-border">
          <h3 class="box-title">Folders</h3>

          <div class="box-tools">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="box-body no-padding">
          <ul class="nav nav-pills nav-stacked">
            <li class="active"><a href="<?php echo base_url('contacto') ?>"><i class="fa fa-inbox"></i> Inbox
              <span class="label label-primary pull-right"><?php echo $this->data['nuevos_correos']; ?></span></a></li>
            <!--<li><a href="#"><i class="fa fa-envelope-o"></i> Sent</a></li>-->
            <!-- <li><a href="#"><i class="fa fa-file-text-o"></i> Drafts</a></li> -->
            <!-- <li><a href="#"><i class="fa fa-filter"></i> Junk <span class="label label-warning pull-right">65</span></a> -->
            </li>
            <!--<li><a href="#"><i class="fa fa-trash-o"></i> Trash</a></li>-->
          </ul>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /. box -->

      <!-- /.box -->
    </div>
    <!-- /.col -->
    <div class="col-md-9">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Inbox</h3>

          <div class="box-tools pull-right">
            <div class="has-feedback">
              <input type="text" class="form-control input-sm" placeholder="Search Mail">
              <span class="glyphicon glyphicon-search form-control-feedback"></span>
            </div>
          </div>
          <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">
          <div class="mailbox-controls">
            <!-- Check all button -->
            <!-- <button type="button" class="btn btn-default btn-sm checkbox-toggle"><i class="fa fa-square-o"></i>
            </button> -->
            <!-- <div class="btn-group">
              <button type="button" class="btn btn-default btn-sm"><i class="fa fa-trash-o"></i></button>
              <button type="button" class="btn btn-default btn-sm"><i class="fa fa-reply"></i></button>
              <button type="button" class="btn btn-default btn-sm"><i class="fa fa-share"></i></button>
            </div> -->
            <!-- /.btn-group -->
            <!-- /<button type="button" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i></button> -->
            <div class="pull-right">
              1-50/200
              <div class="btn-group">
                <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-left"></i></button>
                <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-right"></i></button>
              </div>
              <!-- /.btn-group -->
            </div>
            <!-- /.pull-right -->
          </div>
          <div class="table-responsive mailbox-messages">
            <table class="table table-hover table-striped">
              <tbody>
                <?php foreach ($this->data['correos'] as $key => $row): ?>
                  <?php
                  $correo_info = json_decode($row->data);
                   ?>
                  <tr>


                    <td class="mailbox-star"><a href="<?php echo base_url("contacto/{$row->id}") ?>"><i class="fa  fa-envelope-square text-<?php echo ($row->readed == 0)? 'yellow' : 'blue' ?>"></i></a></td>
                    <td class="mailbox-name"><a href="<?php echo base_url("contacto/{$row->id}") ?>"><?php echo $correo_info->nombre ?></a></td>
                    <td class="mailbox-subject"><b><?php echo $correo_info->correo; ?></b></td>
                    <!-- <td class="mailbox-attachment"></td> -->
                    <?php
                    $date1 = new DateTime($row->created_at);
                    $date2 = new DateTime(date('Y-m-d H:i:s'));
                    $diff = $date1->diff($date2);

                    $hace = 'Hace';
                    if ($diff->m != 0) {
                      $hace .= " {$diff->m} meses";
                    }
                    if ($diff->d != 0) {
                      $hace .= " {$diff->d} días";
                    }
                    if ($diff->h != 0) {
                      $hace .= " {$diff->h} horas";
                    }
                    if ($diff->i != 0) {
                      $hace .= " {$diff->i} minutos";
                    }
                    if ($diff->s != 0) {
                      $hace .= " {$diff->s} segundos";
                    }
                    // echo $diff->days . ' days ';
                     ?>
                    <td class="mailbox-date"><?php
                    // echo "<pre>";
                    // print_r($diff);
                    // echo "</pre>";
                    echo $hace;
                    ?></td>
                  </tr>
                <?php endforeach; ?>

              </tbody>
            </table>
            <!-- /.table -->
          </div>
          <!-- /.mail-box-messages -->
        </div>
        <!-- /.box-body -->
        <div class="box-footer no-padding">
          <div class="mailbox-controls">
            <!-- Check all button -->
            <!-- <button type="button" class="btn btn-default btn-sm checkbox-toggle"><i class="fa fa-square-o"></i>
            </button> -->
            <!-- <div class="btn-group">
              <button type="button" class="btn btn-default btn-sm"><i class="fa fa-trash-o"></i></button>
              <button type="button" class="btn btn-default btn-sm"><i class="fa fa-reply"></i></button>
              <button type="button" class="btn btn-default btn-sm"><i class="fa fa-share"></i></button>
            </div> -->
            <!-- /.btn-group -->
            <button type="button" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i></button>
            <div class="pull-right">
              1-50/200
              <div class="btn-group">
                <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-left"></i></button>
                <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-right"></i></button>
              </div>
              <!-- /.btn-group -->
            </div>
            <!-- /.pull-right -->
          </div>
        </div>
      </div>
      <!-- /. box -->
    </div>
    <!-- /.col -->
  </div>
