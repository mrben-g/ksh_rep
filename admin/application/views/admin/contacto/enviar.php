<?php

$info = $this->data['correo'];
$content = json_decode($info->data);

?>
  <div class="row">
    <div class="col-md-3">


      <div class="box box-solid">
        <div class="box-header with-border">
          <h3 class="box-title">Folders</h3>

          <div class="box-tools">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="box-body no-padding">
          <ul class="nav nav-pills nav-stacked">
            <li><a href="<?php echo base_url('contacto') ?>"><i class="fa fa-inbox"></i> Inbox
              <span class="label label-primary pull-right"><?php echo $this->data['nuevos_correos']; ?></span></a></li>
            <!--<li><a href="#"><i class="fa fa-envelope-o"></i> Sent</a></li>-->
            <!--<li><a href="#"><i class="fa fa-file-text-o"></i> Drafts</a></li>-->
            <!--<li><a href="#"><i class="fa fa-filter"></i> Junk <span class="label label-warning pull-right">65</span></a></li>-->
            <!--<li><a href="#"><i class="fa fa-trash-o"></i> Trash</a></li>-->
          </ul>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /. box -->

      <!-- /.box -->
    </div>
    <!-- /.col -->
    <div class="col-md-9">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Responder correo</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <form class="generic_form_form" action="http://mrwasabiserver.com/webpage/ksh17/api-correo/index.php/api/responde_correo_mensaje" method="post">


          <div class="form-group">
            <input class="form-control" readonly value="<?php echo $content->correo; ?>" name="correo" placeholder="Para:">
          </div>
          <div class="form-group">
            <input class="form-control" name="asunto" id="asunto" placeholder="Asunto:">
          </div>
          <div class="form-group">
                <textarea id="compose-textarea" class="form-control" name="mensaje" style="height: 300px">
                </textarea>
          </div>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          <div class="pull-right">
            <!-- <button type="button" class="btn btn-default"><i class="fa fa-pencil"></i> Draft</button> -->
            <button type="submit" class="btn btn-primary"><i class="fa fa-envelope-o"></i> Enviar</button>
          </div>
          <!-- <button type="reset" class="btn btn-default"><i class="fa fa-times"></i> Discard</button> -->
        </div>
        <!-- /.box-footer -->
      </div>
      <!-- /. box -->
    </div>
      </form>
    <!-- /.col -->
  </div>
