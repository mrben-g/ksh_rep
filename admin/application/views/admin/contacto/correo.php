<?php
$info = $this->data['correo'];
  $correo_info = json_decode($info->data);
 ?>
  <div class="row">
    <div class="col-md-3">
      <a href="<?php echo base_url("responder/{$info->id}") ?>" class="btn btn-primary btn-block margin-bottom">Responder</a>

      <div class="box box-solid">
        <div class="box-header with-border">
          <h3 class="box-title">Folders</h3>

          <div class="box-tools">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="box-body no-padding">
          <ul class="nav nav-pills nav-stacked">
            <li><a href="<?php echo base_url('contacto') ?>"><i class="fa fa-inbox"></i> Inbox
              <span class="label label-primary pull-right"><?php echo $this->data['nuevos_correos']; ?></span></a></li>
            <!--<li><a href="#"><i class="fa fa-envelope-o"></i> Sent</a></li>-->
            <!--<li><a href="#"><i class="fa fa-file-text-o"></i> Drafts</a></li>-->
            <!--<li><a href="#"><i class="fa fa-filter"></i> Junk <span class="label label-warning pull-right">65</span></a></li>-->
            <!--<li><a href="#"><i class="fa fa-trash-o"></i> Trash</a></li>-->
          </ul>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /. box -->

      <!-- /.box -->
    </div>
    <!-- /.col -->
    <div class="col-md-9">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Read Mail</h3>

          <div class="box-tools pull-right">
            <a href="#" class="btn btn-box-tool" data-toggle="tooltip" title="Previous"><i class="fa fa-chevron-left"></i></a>
            <a href="#" class="btn btn-box-tool" data-toggle="tooltip" title="Next"><i class="fa fa-chevron-right"></i></a>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">
          <div class="mailbox-read-info">
            <h3>Correo de contacto</h3>
            <h5>De: <?php echo $correo_info->correo; ?>
              <span class="mailbox-read-time pull-right"><?php echo $info->created_at; ?></span></h5>
          </div>
          <!-- /.mailbox-read-info -->
          <div class="mailbox-controls with-border text-center">
            <div class="btn-group">
              <button type="button" class="btn btn-default btn-sm" data-toggle="tooltip" data-container="body" title="Delete">
                <i class="fa fa-trash-o"></i></button>
              <button type="button" class="btn btn-default btn-sm" data-toggle="tooltip" data-container="body" title="Reply">
                <i class="fa fa-reply"></i></button>
              <button type="button" class="btn btn-default btn-sm" data-toggle="tooltip" data-container="body" title="Forward">
                <i class="fa fa-share"></i></button>
            </div>
            <!-- /.btn-group -->
            <button type="button" class="btn btn-default btn-sm" data-toggle="tooltip" title="Print">
              <i class="fa fa-print"></i></button>
          </div>
          <!-- /.mailbox-controls -->
          <div class="mailbox-read-message">
              <p>
          <?php if(!empty($correo_info->tipo)){ ?>        
          <b>Producto de interés:</b> <?php echo $correo_info->tipo; ?><br>
          <?php } ?>
           <?php if(!empty($correo_info->nombre)){ ?>      
          <b>Nombre:</b> <?php echo $correo_info->nombre; ?><br>
          <?php } ?>
           <?php if(!empty($correo_info->empresa)){ ?>      
          <b>Empresa:</b> <?php echo $correo_info->empresa; ?><br>
          <?php } ?>
           <?php if(!empty($correo_info->telefono)){ ?>      
          <b>Teléfono:</b> <?php echo $correo_info->telefono; ?><br>
          <?php } ?>
           <?php if(!empty($correo_info->correo)){ ?>      
          <b>Correo:</b> <?php echo $correo_info->correo; ?><br>
          <?php } ?>
           <?php if(!empty($correo_info->ciudad)){ ?>      
          <b>Ciudad:</b> <?php echo $correo_info->ciudad; ?><br>
          <?php } ?>
           <?php if(!empty($correo_info->mensaje)){ ?>      
          <b>Mensaje:</b> <?php echo $correo_info->mensaje; ?><br>
          <?php } ?>
          </p>
          </div>
          <!-- /.mailbox-read-message -->
        </div>
        <!-- /.box-body -->
        <div class="box-footer">

        </div>
        <!-- /.box-footer -->
        <div class="box-footer">
          <div class="pull-right">
            <button type="button" class="btn btn-default"><i class="fa fa-reply"></i> Reply</button>
            <button type="button" class="btn btn-default"><i class="fa fa-share"></i> Forward</button>
          </div>
          <button type="button" class="btn btn-default"><i class="fa fa-trash-o"></i> Delete</button>
          <button type="button" class="btn btn-default"><i class="fa fa-print"></i> Print</button>
        </div>
        <!-- /.box-footer -->
      </div>
      <!-- /. box -->
    </div>
    <!-- /.col -->
  </div>
