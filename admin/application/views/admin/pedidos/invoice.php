<?php
// echo '<pre>' ;
 //print_r($this->data['record']);
// echo "</pre>";
$info = $this->data['record'];
$direccion = $this->pedidos_model->get_direccion($info->UsuarioId);
$estado = $this->pedidos_model->get_estado($direccion->Estado);


// echo '<pre>' ;
// print_r($direccion);
// echo "</pre>";
?>


<section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-globe"></i> Mr. Wasabi
            <small class="pull-right">Fecha: <?php echo $this->data['record']->FechaPedido; ?></small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          Vendedor
          <address>
            <strong>Kuroga Soluciones.</strong><br>
            Monterrey<br>
            Nuevo León<br>
            Teléfono: (81) 8388-9800<br>
            Email: contacto@ksh.mx
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          Comprador
          <address>

            <strong><?php echo (is_string($direccion->Nombre))? $direccion->Nombre.' '.$direccion->Apellidos : 'Invitado' ; ?></strong><br>
            <?php echo $direccion->Calle.' '.$direccion->NoDireccion.' '.$direccion->ColDireccion; ?><br>
            <?php if (isset($estado->nombre)): ?>
              <?php echo 'Nuevo León CP:'.$direccion->CodigoPostal; ?><br>
            <?php endif; ?>

            <!-- Phone: (555) 539-1037<br> -->
            <!-- Email: john.doe@example.com -->
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          <b>Invoice #<?php echo $info->CodigoId; ?></b><br>
          <br>
          <!-- <b>Order ID:</b> 4F3S8J<br> -->
          <!-- <b>Payment Due:</b> 2/22/2014<br>
          <b>Account:</b> 968-34567 -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Table row -->
      <?php
      $productos = $this->pedidos_model->get_product_invoice($info->IdPedido);
      // echo "<pre>";
      // print_r($productos);
      // echo "</pre>";
       ?>
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped">
            <thead>
            <tr>
              <th>Qty</th>
              <th>Producto</th>
              <th>Serial #</th>
              <th>Descripción</th>
              <th>Subtotal</th>
            </tr>
            </thead>
            <tbody>
              <?php foreach ($productos as $key => $row): ?>
                <tr>
                  <td><?php echo $row->CantidadProducto; ?></td>
                  <td><?php // echo $row->Nombre; ?></td>
                  <td><?php echo $row->ProductoId; ?></td>
                  <td><?php //echo $row->RangoDiametro; ?></td>
                  <td>$<?php echo $row->PrecioProducto * $row->CantidadProducto; ?></td>
                </tr>
              <?php endforeach; ?>

            <!-- <tr>
              <td>1</td>
              <td>Need for Speed IV</td>
              <td>247-925-726</td>
              <td>Wes Anderson umami biodiesel</td>
              <td>$50.00</td>
            </tr> -->
            <!-- <tr>
              <td>1</td>
              <td>Monsters DVD</td>
              <td>735-845-642</td>
              <td>Terry Richardson helvetica tousled street art master</td>
              <td>$10.70</td>
            </tr> -->
            <!-- <tr>
              <td>1</td>
              <td>Grown Ups Blue Ray</td>
              <td>422-568-642</td>
              <td>Tousled lomo letterpress</td>
              <td>$25.99</td>
            </tr> -->
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6">
          <p class="lead">Método de pago:</p>
          <?php

          switch ($info->TipoPagoId) {
            case 1: ?>
            <img src="<?php echo base_url('public') ?>/metodos_pago/visa.png" style="    width: 65px;" alt="Visa">
            <img src="<?php echo base_url('public') ?>/metodos_pago/mastercard.png" style="    width: 65px;" alt="Mastercard">
              <?php
              break;
            case 2: ?>
            <img src="<?php echo base_url('public') ?>/metodos_pago/money.png" style="    width: 65px;" alt="Visa">
              <?php break;
            case 3: ?>
              <img src="<?php echo base_url('public') ?>/metodos_pago/wallet.png" style="    width: 65px;" alt="Visa">
              <?php break;

            default:
              # code...
              break;
          }

          ?>

          <!-- <img src="<?php echo base_url('public') ?>/dist/img/credit/american-express.png" alt="American Express">
          <img src="<?php echo base_url('public') ?>/dist/img/credit/paypal2.png" alt="Paypal"> -->

          <p class="text-muted well well-sm no-shadow hidden" style="margin-top: 10px;">
            <!-- Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem plugg
            dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra. -->
          </p>
        </div>
        <!-- /.col -->
        <div class="col-xs-6">
          <!-- <p class="lead">Amount Due 2/22/2014</p> -->

          <div class="table-responsive">
            <table class="table">
              <tbody>
                <tr>
                <th style="width:50%">Total:</th>
                <td>$<?php echo $info->TotalCompra ?></td>
              </tr>
              <!-- <tr>
                <th>Tax (9.3%)</th>
                <td>$10.34</td>
              </tr>
              <tr>
                <th>Shipping:</th>
                <td>$5.80</td>
              </tr>
              <tr>
                <th>Total:</th>
                <td>$265.24</td>
              </tr> -->
            </tbody></table>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
          <!-- <a href="invoice-print.html" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a> -->
          <!-- <button type="button" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Submit Payment
          </button>
          <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
            <i class="fa fa-download"></i> Generate PDF
          </button> -->
        </div>
      </div>
    </section>
