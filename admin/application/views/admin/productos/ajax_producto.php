<table id="simple1" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th></th>
              <th>SKU</th>
              <th>Nombre</th>
              <th>Diametro</th>
              <th>Rango</th>
              <th>Ancho panel</th>
              <th>Material</th>
              <th>*</th>
            </tr>
            </thead>
            <tbody>
              <?php if(!empty($this->data['product'])):foreach ($this->data['product'] as $key => $row): ?>
                <tr>
                  <td><?php echo $row['IdProducto']; ?></td>
                  <td><?php echo $row['SKU']; ?></td>
                  <td><?php echo $row['Nombre']; ?></td>
                  <td><?php echo $row['DiametroNominal']; ?></td>
                  <td><?php echo $row['RangoDiametro']; ?></td>
                  <td><?php echo $row['AnchoPanel']; ?></td>
                  <td><?php echo $row['MaterialOreja']; ?></td>

                  <td></td>
                </tr>
              <?php endforeach; endif;?>
            </tbody>
            <tfoot>
            <tr>
              <th></th>
              <th>SKU</th>
              <th>Nombre</th>
              <th>Diametro</th>
              <th>Rango</th>
              <th>Ancho panel</th>
              <th>Material</th>
              <th>*</th>
            </tr>
            </tfoot>
          </table>