
  <div class="row">
    <div class="col-xs-12">


      <div class="box">
        <div class="box-header">
          <h3 class="box-title"></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="simple1" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th></th>
              <th>SKU</th>
              <th>Nombre</th>
              <th>Apertura de Valvula</th>
              <th>Tamaño de Valvula</th>
              <th>Numero de Vias</th>
              <th>Largo del Barril</th>
              <th>Tipo de Junta</th>
              
              <th>*</th>
            </tr>
            </thead>
            <tbody>
              <?php foreach ($this->data['productos'] as $key => $row): ?>
                <tr>
                  <td><?php echo $row->id; ?></td>
                  <td><?php echo $row->SKU; ?></td>
                  <td><?php echo $row->Nombre; ?></td>
                  <td><?php echo $row->AperturaValvula; ?></td>
                  <td><?php echo $row->TamanoValvula; ?></td>
                  <td><?php echo $row->NumeroVias; ?></td>
                  <td><?php echo $row->LargoBarril; ?></td>
                  <td><?php echo $row->TipoJunta; ?></td>
                  <td></td>
                </tr>
              <?php endforeach; ?>
            </tbody>
            <tfoot>
            <tr>
              <th></th>
              <th>SKU</th>
              <th>Nombre</th>
              <th>Material</th>
              <th>Diametro</th>
              <th>*</th>
            </tr>
            </tfoot>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
