<div class="row">
        <!-- left column -->
        <div class="col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">General</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->

            <form class="generic_form_form" action="<?php echo base_url('process/productos') ?>" role="form">
              <div class="box-body">
                <?php if ($this->data['record']): ?>
                  <input type="hidden" name="id" value="<?php echo $this->data['record']->IdProducto; ?>">
                <?php endif; ?>
                <div class="form-group">
                  <label for="">SKU</label>
                  <input type="text" value="<?php echo ($this->data['record'])? $this->data['record']->SKU : ''?>" class="form-control" id="" name="SKU">
                </div>
                <div class="form-group">
                  <label for="">Nombre</label>
                  <input value="<?php echo ($this->data['record'])? $this->data['record']->Nombre : ''?>" type="text" class="form-control" id="" name="Nombre">
                </div>
                <div class="form-group">
                  <label for="">Nombre Ingles</label>
                  <input type="text" class="form-control" value="<?php echo ($this->data['record'])? $this->data['record']->NombreIngles : ''?>" id="" name="NombreIngles">
                </div>
                <div class="form-group">
                  <label for="">Costo</label>
                  <input type="text" class="form-control" value="<?php echo ($this->data['record'])? $this->data['record']->Costo : ''?>" id="" name="Costo">
                </div>
                <div class="form-group">
                  <label for="">Precio</label>
                  <input type="text" class="form-control" value="<?php echo ($this->data['record'])? $this->data['record']->Precio : ''?>" id="" name="Precio">
                </div>
                <div class="form-group">
                  <label for="">Precio dolar</label>
                  <input type="text" class="form-control" value="<?php echo ($this->data['record'])? $this->data['record']->PrecioDollar : ''?>" id="" name="PrecioDollar">
                </div>
                <div class="form-group">
                  <label for="">Disponibilidad</label>
                  <input type="text" class="form-control" value="<?php echo ($this->data['record'])? $this->data['record']->Disponibilidad : ''?>" id="" name="Disponibilidad">
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="">Largo</label>
                    <input type="text" class="form-control" value="<?php echo ($this->data['record'])? $this->data['record']->Largo : ''?>" id="" name="Largo">
                  </div>
                  <div class="form-group">
                    <label for="">Alto</label>
                    <input type="text" class="form-control" value="<?php echo ($this->data['record'])? $this->data['record']->Alto : ''?>" id="" name="Alto">
                  </div>
                  <div class="form-group">
                    <label for="">Medida</label>
                    <input type="text" class="form-control" value="<?php echo ($this->data['record'])? $this->data['record']->Medida : ''?>" id="" name="Medida">
                  </div>
                  <div class="form-group">
                    <label for="">Material</label>
                    <input type="text" class="form-control" value="<?php echo ($this->data['record'])? $this->data['record']->Material : ''?>" id="" name="Material">
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="">Ancho</label>
                    <input type="text" class="form-control" value="<?php echo ($this->data['record'])? $this->data['record']->Ancho : ''?>" id="" name="Ancho">
                  </div>
                  <div class="form-group">
                    <label for="">Peso</label>
                    <input type="text" class="form-control" value="<?php echo ($this->data['record'])? $this->data['record']->Peso : ''?>" id="" name="Peso">
                  </div>
                  <div class="form-group">
                    <label for="">Medida Ingles</label>
                    <input type="text" class="form-control" value="<?php echo ($this->data['record'])? $this->data['record']->MedidaIngles : ''?>" id="" name="MedidaIngles">
                  </div>
                  <div class="form-group">
                    <label for="">Empaque</label>
                    <input type="text" class="form-control" value="<?php echo ($this->data['record'])? $this->data['record']->Empaque : ''?>" id="" name="Empaque">
                  </div>

                  <div class="form-group">
                    <label for="">Material Ingles</label>
                    <input type="text" class="form-control" value="<?php echo ($this->data['record'])? $this->data['record']->{'Material_Ingles'}: ''?>" id="" name="Material_Ingles">
                  </div>
                </div>

              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Guardar</button>
              </div>
            <!-- </form> -->
          </div>
          <!-- /.box -->



        </div>
        <!--/.col (left) -->
        <!-- right column -->
        <div class="col-md-6">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Descripcion</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <!-- <form class="form-horizontal"> -->
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Español</label>

                  <div class="col-sm-10">
                    <textarea class="form-control" rows="8" cols="80" name="Descripcion">
                      <?php echo ($this->data['record'])? $this->data['record']->Descripcion : ''?>
                    </textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Ingles</label>

                  <div class="col-sm-10">
                    <textarea class="form-control" rows="8" cols="80" name="DescripcionIngles">
                      <?php echo ($this->data['record'])? $this->data['record']->DescripcionIngles : ''?>
                    </textarea>
                  </div>
                </div>

              </div>
              <!-- /.box-body -->
              <!-- <div class="box-footer">
                <button type="submit" class="btn btn-default">Cancel</button>
                <button type="submit" class="btn btn-info pull-right">Sign in</button>
              </div> -->
              <!-- /.box-footer -->

          </div>

        </div>



        <div class="col-sm-6">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Sección</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <!-- <form role="form"> -->
              <div class="box-body">
                <div class="form-group">
                  <label>Multiple</label>
                  <select class="form-control select2" name="Evento[]" multiple="multiple" data-placeholder="Selecciona un evento" style="width: 100%;">
                    <?php
                    $seleccionados = explode(',', $this->data['record']->Evento);
                    // echo "<pre>";
                    // print_r($seleccionados);
                    // echo "</pre>";
                    $opciones = array(
                      1 => 'Bautizo',
                      2 => 'Bodas',
                      3 => 'San Valentín',
                      4 => 'Día de Muertos',
                      5 => 'Regalos de Trabajo',
                      7 => 'Día de las madres',
                      8 => 'Navidad'
                    );

                     ?>
                     <?php foreach ($opciones as $key => $row): ?>
                       <?php
                       $seleccionado_ = '';
                        ?>
                       <?php foreach ($seleccionados as $index => $val): ?>
                         <?php if ($val == $key):
                            $seleccionado_ = 'selected';
                          endif; ?>
                       <?php endforeach; ?>
                       <option <?php echo $seleccionado_ ?> value="<?php echo $key ?>"><?php echo $row ?></option>
                     <?php endforeach; ?>

                    <!-- <option value="2"></option>
                    <option value="3"></option>
                    <option value="4"></option>
                    <option value="5"></option>
                    <option value="7"></option> -->
                    <!-- <option>Washington</option> -->
                  </select>
                </div>
                <div class="form-group">
                  <label for="">Producto Activo/Inactivo</label>
                  <?php
                  $checked = '';
                  if ($this->data['record'] && $this->data['record']->Estatus == 1) {
                    $checked = 'checked';
                  }
                   ?>
                  <input type="checkbox" name="Estatus" value="1" class="js-switch" <?php echo $checked; ?> />
                </div>

              </div>
              <!-- /.box-body -->

              <!-- <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div> -->

          </div>
        </div>
        <div class="col-sm-6">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Imagenes</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <!-- <form role="form"> -->
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputFile">Imagen</label>
                  <input type="file" disabled="disabled" id="exampleInputFile">

                  <!-- <p class="help-block">Example block-level help text here.</p> -->
                </div>

              </div>
              <!-- /.box-body -->

              <!-- <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div> -->

          </div>
        </div>
          </form>
        <!--/.col (right) -->
      </div>
