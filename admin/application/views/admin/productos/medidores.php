
  <div class="row">
    <div class="col-xs-12">


      <div class="box">
        <div class="box-header">
          <h3 class="box-title"></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="simple1" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th></th>
              <th>SKU</th>
              <th>Nombre</th>
              <th>Tamaño</th>
              <th>Rosca</th>
              <th>Material</th>
              <th>Medidor</th>
              <th>*</th>
            </tr>
            </thead>
            <tbody>
              <?php foreach ($this->data['productos'] as $key => $row): ?>
                <tr>
                  <td><?php echo $row->id; ?></td>
                  <td><?php echo $row->SKU; ?></td>
                  <td><?php echo $row->Nombre; ?></td>
                  <td><?php echo $row->Tamano; ?></td>
                  <td><?php echo $row->TipoRosca; ?></td>
                  <td><?php echo $row->Material; ?></td>
                  <td><?php echo $row->TipoMedidor; ?></td>


                  <td></td>
                </tr>
              <?php endforeach; ?>
            </tbody>
            <tfoot>
            <tr>
              <th></th>
              <th>SKU</th>
              <th>Nombre</th>
              <th>Tamaño</th>
              <th>Rosca</th>
              <th>Material</th>
              <th>Medidor</th>
              <th>*</th>
            </tr>
            </tfoot>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
