
  <div class="row">
    <div class="col-xs-12">


      <div class="box">
        <div class="box-header">
          <h3 class="box-title"></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="simple1" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th></th>
              <th>SKU</th>
              <th>Nombre</th>
              <th>Medida</th>
              <th>Material</th>
              <th>*</th>
            </tr>
            </thead>
            <tbody>
              <?php foreach ($this->data['productos'] as $key => $row): ?>
                <tr>
                  <td><?php echo $row->idValvulas; ?></td>
                  <td><?php echo $row->SKU; ?></td>
                  <td><?php echo $row->Nombre; ?></td>
                  <td><?php echo $row->Medida; ?></td>
                  <td><?php echo $row->Material; ?></td>


                  <td></td>
                </tr>
              <?php endforeach; ?>
            </tbody>
            <tfoot>
            <tr>
              <th></th>
              <th>SKU</th>
              <th>Nombre</th>
              <th>Medida</th>
              <th>Material</th>
              <th>*</th>
            </tr>
            </tfoot>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
