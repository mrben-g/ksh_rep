<div class="row">
        <!-- left column -->
        <div class="col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">General</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->

            <form class="generic_form_form" action="<?php echo base_url('process/guardar_lugares') ?>" role="form">
              <div class="box-body">
                <?php if ($this->data['record']): ?>
                  <input type="hidden" name="id" value="<?php echo $this->data['record']->IdProducto; ?>">
                <?php endif; ?>
                <?php

                 $seleccionados = json_decode($this->data['seleccionados']->data);
                //  echo '<pre>';
                 //
                //  print_r($seleccionados->productos['0']);
                //  echo "</pre>";
                 ?>
                <div class="form-group">
                  <label>Producto #1</label>
                  <select class="form-control select-limit select2" name="productos[0]">
                    <?php foreach ($this->data['productos'] as $key => $row): ?>
                      <option <?php echo ($row->IdProducto == $seleccionados->productos[0]) ? 'selected="selected"' : '' ; ?> value="<?php echo $row->IdProducto ?>"><?php echo $row->Nombre ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>

                <div class="form-group">
                  <label>Producto #2</label>
                  <select class="form-control select-limit select2" name="productos[1]">
                    <?php foreach ($this->data['productos'] as $key => $row): ?>
                      <option <?php echo ($row->IdProducto == $seleccionados->productos['1']) ? 'selected="selected"' : '' ; ?> value="<?php echo $row->IdProducto ?>"><?php echo $row->Nombre ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>

                <div class="form-group">
                  <label>Producto #3</label>
                  <select class="form-control select-limit select2" name="productos[2]">
                    <?php foreach ($this->data['productos'] as $key => $row): ?>
                      <option <?php echo ($row->IdProducto == $seleccionados->productos['2']) ? 'selected="selected"' : '' ; ?> value="<?php echo $row->IdProducto ?>"><?php echo $row->Nombre ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>

                <div class="form-group">
                  <label>Producto #4</label>
                  <select class="form-control select-limit select2" name="productos[3]">
                    <?php foreach ($this->data['productos'] as $key => $row): ?>
                      <option <?php echo ($row->IdProducto == $seleccionados->productos['3']) ? 'selected="selected"' : '' ; ?> value="<?php echo $row->IdProducto ?>"><?php echo $row->Nombre ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>

                <div class="form-group">
                  <label>Producto #5</label>
                  <select class="form-control select-limit select2" name="productos[4]">
                    <?php foreach ($this->data['productos'] as $key => $row): ?>
                      <option <?php echo ($row->IdProducto == $seleccionados->productos['4']) ? 'selected="selected"' : '' ; ?> value="<?php echo $row->IdProducto ?>"><?php echo $row->Nombre ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>

                <div class="form-group">
                  <label>Producto #6</label>
                  <select class="form-control select-limit select2" name="productos[5]">
                    <?php foreach ($this->data['productos'] as $key => $row): ?>
                      <option <?php echo ($row->IdProducto == $seleccionados->productos['5']) ? 'selected="selected"' : '' ; ?> value="<?php echo $row->IdProducto ?>"><?php echo $row->Nombre ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>

                <div class="form-group">
                  <label>Producto #7</label>
                  <select class="form-control select-limit select2" name="productos[6]">
                    <?php foreach ($this->data['productos'] as $key => $row): ?>
                      <option <?php echo ($row->IdProducto == $seleccionados->productos['6']) ? 'selected="selected"' : '' ; ?> value="<?php echo $row->IdProducto ?>"><?php echo $row->Nombre ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>

                <div class="form-group">
                  <label>Producto #8</label>
                  <select class="form-control select-limit select2" name="productos[7]">
                    <?php foreach ($this->data['productos'] as $key => $row): ?>
                      <option <?php echo ($row->IdProducto == $seleccionados->productos['7']) ? 'selected="selected"' : '' ; ?> value="<?php echo $row->IdProducto ?>"><?php echo $row->Nombre ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>

                <div class="form-group">
                  <label>Producto #9</label>
                  <select class="form-control select-limit select2" name="productos[8]">
                    <?php foreach ($this->data['productos'] as $key => $row): ?>
                      <option <?php echo ($row->IdProducto == $seleccionados->productos['8']) ? 'selected="selected"' : '' ; ?> value="<?php echo $row->IdProducto ?>"><?php echo $row->Nombre ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>

              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Guardar</button>
              </div>
            <!-- </form> -->
          </div>
          <!-- /.box -->



        </div>
        <!--/.col (left) -->
        <!-- right column -->






          </form>
        <!--/.col (right) -->
      </div>
