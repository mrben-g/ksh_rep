
  <div class="row">
    <div class="col-xs-12">


      <div class="box">
        <div class="box-header">
          <h3 class="box-title"></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="row" style="margin-bottom: 25px;">
                <div class="col-md-6">
                    <a href="javascript:MuestraProd(1)" class="btn btn-primary">Filtrar por Abrazaderas</a>
                    <a href="javascript:MuestraProd(2)" class="btn btn-primary">Filtrar por Juntas</a>
                    <a href="javascript:MuestraProd(3)" class="btn btn-primary">Filtrar por Abrazaderas Campana</a>
                    <a href="javascript:MuestraProd(4)" class="btn btn-primary">Filtrar por Cople Dayton</a>
                    <a href="javascript:MuestraProd(5)" class="btn btn-primary">Mostrar Todos</a>
                </div>
            </div>
            <div id="product_filter">
          <table id="simple1" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th></th>
              <th>SKU</th>
              <th>Nombre</th>
              <th>Diametro</th>
              <th>Rango</th>
              <th>Ancho panel</th>
              <th>Material</th>
              <th>*</th>
            </tr>
            </thead>
            <tbody>
              <?php foreach ($this->data['productos'] as $key => $row): ?>
                <tr>
                  <td><?php echo $row->IdProducto; ?></td>
                  <td><?php echo $row->SKU; ?></td>
                  <td><?php echo $row->Nombre; ?></td>
                  <td><?php echo $row->DiametroNominal; ?></td>
                  <td><?php echo $row->RangoDiametro; ?></td>
                  <td><?php echo $row->AnchoPanel; ?></td>
                  <td><?php echo $row->MaterialOreja; ?></td>

                  <td></td>
                </tr>
              <?php endforeach; ?>
            </tbody>
            <tfoot>
            <tr>
              <th></th>
              <th>SKU</th>
              <th>Nombre</th>
              <th>Diametro</th>
              <th>Rango</th>
              <th>Ancho panel</th>
              <th>Material</th>
              <th>*</th>
            </tr>
            </tfoot>
          </table>
            </div>
            
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
