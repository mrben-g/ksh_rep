<div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title"></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="tbl_usuarios" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th></th>
              <th>Nombre</th>
              <th>Descuento</th>
              <th></th>
              <th>*</th>
            </tr>
            </thead>
            <tbody>
              <?php  foreach ($this->data['precio_list']as $key => $row): ?>
                <tr>
                    <td></td>
                  <td><?php echo $row->NombrePrecio; ?></td>
                  <td><?php echo $row->Descuento; ?></td>
                  <td>
                      <div class="text-center" style="margin: 0 auto;"><div class="btn-group">
                      <a href="<?php echo base_url('editar_precio/').$row->id ?>" class="btn btn-primary">Editar</a>
                      <!--<a href="<?php ?>" class="btn btn-danger">Eliminar</a>-->

                    </div></div>
                  </td>
                  <td></td>
                </tr>
              <?php  endforeach; ?>
            </tbody>
            <tfoot>
            <tr>
              <th></th>
              <th>Nombre</th>
              <th>Descuento</th>
              <th></th>
              <th>*</th>
            </tr>
            </tfoot>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->