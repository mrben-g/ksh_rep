<div class="col-md-9">
    <div class="box box-primary">
        <?php echo form_open_multipart(base_url('admin/editar_precio_db'), array('id' => 'frm-price')) ?>
        <div class="box-header with-border">
            <h3 class="box-title">Ingresar Precio</h3>
        </div>
        <div class="box-body">
            <?php  if($this->data['precio_tipo']): foreach ($this->data['precio_tipo'] as $list): ?>
             <input type="hidden" name="id" id="id" value="<?php echo $list->id ?>" />
            <div class="form-group">
                  <label for="">Nombre Precio</label>
                  <input type="text" class="form-control" name="nombre_precio" id="nombre_precio" value="<?php echo $list->NombrePrecio ?>">
                </div>
            <div class="form-group">
                  <label for="">Descuento</label>
                  <input type="text" class="form-control" name="desc" id="desc" value="<?php echo $list->Descuento ?>">
                </div>
            
             <?php 
                          endforeach; 
                          endif;
                      ?>
        </div>
        <div class="box-footer">
                <button type="submit" class="btn btn-primary">Actualizar Precio</button>
        </div>
        <?php echo form_close(); ?>
         <div class="alerta">
                <?php echo '<br>'.$this->session->flashdata('msg_success');
//                                             if($auth['valid']){redirect(base_url(''), 'refresh');}else{echo $auth['mensaje'];}
                ?>
            </div>
     </div>
<div class="col-md-3"></div>