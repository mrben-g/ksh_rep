<div class="row">
<div class="col-md-9">
    <div class="box">
        <?php echo form_open_multipart(base_url('admin/ingresa_precio_db'), array('id' => 'frm-price')) ?>
        <div class="box-header">
            <h3 class="box-title">Ingresar Precio</h3>
        </div>
        <div class="box-body">
            <div class="form-group">
                  <label for="">Nombre Precio</label>
                  <input type="text" class="form-control" name="nombre_precio" id="nombre_precio">
                </div>
            <div class="form-group">
                  <label for="">Descuento</label>
                  <input type="text" class="form-control" name="desc" id="desc">
                </div>            
        </div>
        <div class="box-footer">
                <button type="submit" class="btn btn-primary">Guardar</button>
        </div>
        <?php echo form_close(); ?>
            <div class="alerta">
                <?php echo '<br>'.$this->session->flashdata('msg_success'); ?>
            </div>
     </div>
<div class="col-md-3"></div>
</div>