<div class="row">
    <div class="col-xs-12">
      <div class="box">
          <?php echo form_open_multipart(base_url('admin/ingresa_precio_usuario_db'), array('id' => 'frm-price-user')) ?>
        <div class="box-header">
          <h3 class="box-title"></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
              
            <input type="hidden" name="user_id" id="user_id" value="<?php echo $this->uri->segment(2) ?>" />
            <div class="col-md-9">
                  
                <div class="form-group">
                  <label for="">Selecciona el tipo de precio</label>
                  <select id="tipo" name="tipo"  class="form-control">
                      <option value="">Selecciona</option>
                      <?php if($this->data['precio_list']): foreach ($this->data['precio_list'] as $list): ?>
                      <option value="<?php echo $list->id ?>"><?php   echo $list->NombrePrecio  ?></option>
                          <?php 
                          endforeach; 
                          endif;
                      ?>
                  </select>
                </div>
            
            </div>
             
        </div>
           <div class="box-footer">
                <button type="submit" class="btn btn-primary">Guardar</button>
                <a href="<?php echo base_url('precio_usuarios') ?>" class="btn btn-danger">Cancelar</a>
        </div>
        <div>
           
        </div>
        <?php echo form_close(); ?>
        <!-- /.box-body -->
      </div>
         
            <div class="alerta">
                <?php echo '<br>'.$this->session->flashdata('msg_success');
//                                             if($auth['valid']){redirect(base_url(''), 'refresh');}else{echo $auth['mensaje'];}
                ?>
            </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
