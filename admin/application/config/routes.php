<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'admin/index';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['productos/lugares'] = 'admin/productos_primeros';

$route['alcantarillado'] = 'admin/productos_alcantarillado';
$route['derivacion'] = 'admin/productos_derivacion';
$route['filtros'] = 'admin/productos_filtros';
$route['medidores'] = 'admin/productos_medidores';
$route['productos_nuevo'] = 'admin/productos_new';
$route['valvulas'] = 'admin/productos_valvulas';
$route['contacto'] = 'admin/contacto';
$route['contacto/(:num)'] = 'admin/contacto_correo/$1';
$route['responder/(:num)'] = 'admin/enviar/$1';

$route['contra_incendios'] = 'admin/contra_incendios';

$route['cotizaciones'] = 'crm/cotizaciones';

$route['productos'] = 'admin/productos';
$route['pedidos'] = 'admin/pedidos';
$route['pedido/(:any)'] = 'admin/pedido/$1';
$route['producto/(:num)/(:any)'] = 'admin/producto/$1/$2';
$route['admin/login'] = 'auth/index';
$route['login'] = 'auth/login';
$route['salir'] = 'auth/log_out';
$route[''] = 'admin/index';

$route['precio_usuarios'] = 'admin/precio_usuarios';
$route['editar_precio_usuarios/(:any)'] = 'admin/editar_precio_usuarios/$1';

$route['lista_precios'] = 'admin/lista_precios';
$route['ingresa_precio'] = 'admin/ingresa_precio';
$route['editar_precio/(:any)'] = 'admin/editar_precio/$1';

$route['precio_producto'] = 'admin/precio_producto';
$route['ingresa_precio_usuario'] = 'admin/ingresa_precio_usuario';
$route['conversion_dollar'] = 'admin/conversion_dollar';

// $route['pedidos'] = 'admin/pedidos';
// $route['pedido/(:num)'] = 'admin/pedido/$1';
