$(document).ready(function() {

  $('.generic_form').submit(function(event) {
    event.preventDefault();

    $.ajax({
      url: $(this).attr('action'),
      type: 'POST',
      dataType: 'json',
      data: $(this).serialize()
    })
    .done(function(response) {
      // Termina
        console.log(response);
      if (response.valid) {
        if (response.redirect) {
          window.location.href = response.redirect;
        } else {
          swal(
            'Éxito!',
            response.mensaje,
            'success'
          );
        }
      }
      // Termina
      else {
        swal(
          'Oops...',
          response.mensaje,
          'error'
        );
      }
      // console.log("success");
    })
    .fail(function() {
       console.log("error");
    })
    .always(function() {
      // console.log("complete");s
    });


  });

});
