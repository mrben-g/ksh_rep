<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['about'] = 'welcome/about';
$route['proyectos'] = 'welcome/proyectos';
$route['curso_reparacion_fugas'] = 'welcome/curso_reparacion_fugas';
$route['curso_hidrantes'] = 'welcome/curso_hidrantes';
$route['contacto'] = 'welcome/contacto';
$route['guias'] = 'welcome/guias';
/*productos*/
$route['producto'] = 'welcome/producto';
$route['producto_reparacion_de_tuberia/(:any)'] = 'welcome/producto_reparacion_de_tuberia';
$route['producto_tuberia_conexiones/(:any)'] = 'welcome/producto_tuberia_conexiones';
$route['producto_contra_incendio/(:any)'] = 'welcome/producto_contra_incendio';
$route['producto_conduccion_derivacion/(:any)'] = 'welcome/producto_conduccion_derivacion';
$route['producto_alcantarillado/(:any)'] = 'welcome/producto_alcantarillado';
$route['producto_sistema_bombeo/(:any)'] = 'welcome/producto_sistema_bombeo';
$route['producto_valvulas/(:any)'] = 'welcome/producto_valvulas';
$route['producto_filtros/(:any)'] = 'welcome/producto_filtros';
$route['producto_medidores/(:any)'] = 'welcome/producto_medidores';
/*tienda*/
$route['tienda'] = 'welcome/tienda';
$route['tienda_reparacion_de_tuberia'] = 'welcome/tienda_reparacion_de_tuberia';
$route['tienda_reparacion_de_tuberia_prod/(:any)'] = 'welcome/tienda_reparacion_de_tuberia_prod';
$route['tienda_tuberia_conexiones'] = 'welcome/tienda_tuberia_conexiones';
$route['tienda_contra_incendio'] = 'welcome/tienda_contra_incendio';
$route['tienda_contra_incendio_prod/(:any)'] = 'welcome/tienda_contra_incendio_prod';
$route['tienda_conduccion_derivacion'] = 'welcome/tienda_conduccion_derivacion';
$route['tienda_conduccion_derivacion_prod/(:any)'] = 'welcome/tienda_conduccion_derivacion_prod';
$route['tienda_alcantarillado'] = 'welcome/tienda_alcantarillado';
$route['tienda_alcantarillado_prod/(:any)'] = 'welcome/tienda_alcantarillado_prod';
$route['tienda_sistema_bombeo'] = 'welcome/tienda_sistema_bombeo';
$route['tienda_valvulas'] = 'welcome/tienda_valvulas';
$route['tienda_filtros'] = 'welcome/tienda_filtros';
$route['tienda_filtros_prod/(:any)'] = 'welcome/tienda_filtros_prod';
$route['tienda_medidores'] = 'welcome/tienda_medidores';
$route['tienda_medidores_prod/(:any)'] = 'welcome/tienda_medidores_prod';
/*login*/
$route['login/(:any)'] = 'welcome/login';
/*PERFIL*/
$route['perfil'] = 'welcome/perfil';
/*RECUPERA PASSWORD*/
$route['recupera/(:any)'] = 'welcome/recupera';
$route['recupera_process'] = 'welcome/recupera_process';
$route['cambia/(:any)'] = 'welcome/cambia';

/*HISTORIAL*/
$route['historial_compra_usuario'] = 'welcome/historial_compra_usuario';
$route['pedido_usuario_detalle/(:any)'] = 'welcome/pedido_usuario_detalle/$1';
/*CARRITO*/
$route['cart'] = 'welcome/cart';
/*CHECKOUT*/
$route['checkout'] = 'welcome/checkout';
$route['exito'] = 'welcome/exito';
/*Busqueda*/
//$route['busqueda/(:any)'] = 'welcome/busqueda';
$route['busqueda/(:any)'] = 'posts';
/*TERMINOS*/
$route['terminos'] = 'welcome/terminos';
$route['avisos'] = 'welcome/avisos';