<!DOCTYPE html >
<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0">
        <meta name="keywords" content="">
        <meta name="description" content="">
        <title>KSH :: Kuroda soluciones hidráuilicas</title>
        <link rel="shortcut icon" href="<?php echo base_url('public/theme') ?>/images/favicon.png">
        <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
        <!-- CSS -->
        <link rel="stylesheet" href="<?php echo base_url('public/theme') ?>/css/checkout.css" type="text/css"/>   
        <link rel="stylesheet" href="<?php echo base_url('public/theme') ?>/css/payment.css"  type="text/css"/>
        <link rel="stylesheet" href="<?php echo base_url('public/theme') ?>/css/cart.css">
        <link rel="stylesheet" href="<?php echo base_url('public/theme') ?>/css/forms.css">
        <link rel="stylesheet" href="<?php echo base_url('public/theme') ?>/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url('public/theme') ?>/css/style.css">
        <link rel="stylesheet" href="<?php echo base_url('public/theme') ?>/css/style-responsive.css">
        <link rel="stylesheet" href="<?php echo base_url('public/theme') ?>/css/animate.min.css">
        <link rel="stylesheet" href="<?php echo base_url('public/theme') ?>/css/vertical.css">
        <link rel="stylesheet" href="<?php echo base_url('public/theme') ?>/css/owl.carousel.css">
        <link rel="stylesheet" href="<?php echo base_url('public/theme') ?>/css/magnific-popup.css">
        <link rel="stylesheet" href="<?php echo base_url('public/theme') ?>/css/camera.css">
        <link rel="stylesheet" href="<?php echo base_url('public/theme') ?>/js/datatables.net-bs/css/dataTables.bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url('public/theme') ?>/js/datatables.net-bs/css/pagination.less">
<style>
  .carousel_pink {

  }
.titulo_producto {
    position: relative;
    z-index: 2;
    margin-bottom: 8px;
    font-size: 2em;
    color: #000;
    font-weight: 600;
}
  .tituloproductos1 {
    position: relative;
    z-index: 2;
    background-color: #f2f2f2;
    float: left;
    padding-right: 20px;
    font-family: 'Crimson Text', serif;
    font-style: italic;
    font-size: 2em;
    color: #000;
    font-weight: 600;

}
.linea{
    background-color: #ffa800;
    content: "";
    display: inline-block;
    height: 2px;
    position: relative;
    vertical-align: middle;
    width: 100%;
    top: -35px;
    z-index: 0;
}
#ven{    padding-left: 18px;}
#ven li {
    color: #ffa800;
}
#ven li span {
    color: black;
    font-family: 'AvenirNextLTProRegular';
}
label{display: block;}
legend {
    color: #000;
    background: none;
    /* font-style: italic; */
    /*top: -15px;*/
    border-bottom: 1px solid #ffa800;
    position: relative;
}
fieldset {
    padding: .35em .625em .75em;
    margin: 0 2px;
    border: none;
    border-top: 1px solid #ffa800;
}
.active_new{
    background: #ff9e07;
    color:#fff !important;
}
.banner_p p:hover{
	background: #ffa800;
         color:#000!important;
         text-decoration:none;
}
.colores{
    background: #ffa800!important;
    color:#000!important;
}
</style>
<style type="text/css">
.green1{
   background:#9e3151;

}
.green2{
   background:#ffa800;
    
}

h3{ text-align: left;}
 @media(max-width:768px){
     
.checkout-progress{
    width:100%;
    display: block;
    margin-bottom: 2em;
}
.checkout{
     width:100%;
}

    }
    
@media (max-width: 480px) {
 .checkout h2 {
        font-size: 16px;
    }
}
@media (max-width:320px) {
 .checkout h2 {
        font-size: 16px;
    }
}

 
</style>            
<script type="text/javascript" src="<?php echo base_url('public/theme') ?>/js/js/jquery-1.11.2.min.js"></script>
<script type="text/javascript">

    function buscar(e){
       var palabra = $('#buscar').val();
        if(palabra.trim()!="")
        {
        window.location="<?php echo base_url('busqueda/') ?>"+palabra    
        }
        else
        {
            alert("Proporciona una palabra clave para la busqueda de resultados");
        }
        }
 
  function EnviaMail(){

        var str= $("#frm");
        var fields = str.serialize();
        $.ajax({

        type:'post',
        url:str.attr('action'),
        data: fields,
        dataType:'json',
      /*  success: function(data){
            var sep = data.split('|');
             if(sep[0] == 0){
                     //$("input[type=text]").val('');
                     alert(sep[1]);
        }
                if(sep[0] == 1){
                 alert(sep[1]);
                 $("input[type=text]").val('');
                 $("#frm textarea").val('');
                 }
        }*/

             })
                 .done(function(data) {
                console.log(data);
   
             })
            .fail(function(data) {
                console.log(data);
            })
            .always(function() {
                //console.log("complete");
            });
       }
    </script>
    
    </head>

    <body class="appear-animate">
     <!-- Page Loader -->
     <div class="page-loader">
         <div class="loader">Loading...</div>
     </div>
     <!-- End Page Loader -->

     <!-- Page Wrap -->
     <div class="page" id="top">
            <!-- Logo Section -->
            <section class="small-section pt-10 pt-xs-0 pb-10 pb-xs-0" style="background:#000;">
                <div class="container align-center">
                    <div class="row">
                        <div class="col-md-4 col-sm-4 text-left">
                            <a href="<?php echo base_url() ?>"><img src="<?php echo base_url('public/theme') ?>/images/logo.png" alt="KSH logo" style="max-width:204px;" class="mt-20 mt-xs-10 mb-xs-10"></a>
                        </div>
                        <div class="col-md-4 col-sm-3"></div>
                        <div class="col-md-4 col-sm-5 text-left">
                            <div class="row">
                                <div class="col-md-12" style="color:#ff9e07;">COTIZA CON NUESTRAS SUCURSALES</div>
                                <div class="col-md-6 col-sm-6 col-xs-6 white"><label class="mb-0">Monterrey</label><label class="mb-0">Guadalajara</label><label class="mb-0">Silao</label><label class="mb-0">Aguascalientes</label></div>
                                <div class="col-md-6 col-sm-6 col-xs-6 white"><label class="mb-0 white">(81) 83 88 98 00</label><label class="mb-0 white">(33)  36 59 05 29</label><label class="white"> (472) 7 91 02 68</label><label class="white">(449) 9 12 80 00</label></div>
                            </div>
                            <div  class="row">
                                <div class="col-md-12 white"><a href="mailto:contacto@ksh.mx" style="color:#fff">contacto@ksh.mx</a></div>
                            </div>
                                <!-- NOTA: Esto todavia no poner -->
                            <!--    <div class="col-sm-6">
                                    <a href="mailto:contacto@ksh.mx" style="color:#fff;">contacto@ksh.mx</a>
                                </div>
                                <div class="col-sm-6 text-left">
                                    <img src="<?php echo base_url('public/theme') ?>/images/icono.png" alt="" style="" >
                                    <span class="white">Cotiza en una Sucursal</span>
                                      <a href="#" style="color:#ff9e07;margin-left: 30px;">(Monterrey)</a>
                                </div>-->
                            </div>
                        </div>
                    </div>
            </section>
            <!-- End Logo Section -->
          <!-- Menu Wrapper background: url(<?php //echo base_url('public/theme') ?>/images/bg_menu.png) repeat; -->
          <div style="background:#cccccc;border-top: 3px solid #ffa800;">
            <div class="container clearfix">
                <!-- Navigation panel  mn-centered-->
                <nav class="main-nav mn-align-left mb-0">
                    <div class="full-wrapper relative clearfix">
                        <div class="mobile-nav">
                            <i class="fa fa-bars"></i>
                        </div>
                        <!-- Main Menu -->
                        <div class="inner-nav desktop-nav">
                            <ul class="clearlist">
                                <li><a href="<?php echo base_url() ?>" class="<?php echo ($this->uri->segment(1) == '')? "active_new" : ''; ?>">Inicio</a></li>
                                <li><a href="<?php echo base_url('about') ?>" class="mn-has-sub <?php echo ($this->uri->segment(1) == 'about')? "active_new" : ''; ?> <?php echo ($this->uri->segment(1) == 'proyectos')? "active_new" : ''; ?>">Quiénes Somos <i class="fa fa-angle-down"></i></a>
                                    <ul class="mn-sub">
                                        <li><a href="<?php echo base_url('about') ?>" class="">KSH</a></li>
                                        <li><a href="<?php echo base_url('proyectos') ?>" class="">Proyectos</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('curso_reparacion_fugas') ?>" class="mn-has-sub <?php echo ($this->uri->segment(1) == 'curso_reparacion_fugas')? "active_new" : ''; ?> <?php echo ($this->uri->segment(1) == 'curso_hidrantes')? "active_new" : ''; ?>">Cursos <i class="fa fa-angle-down"></i></a>
                                    <ul class="mn-sub">
                                        <li><a href="<?php echo base_url('curso_reparacion_fugas') ?>" class="">Reparación de Fugas</a></li>
                                        <li><a href="<?php echo base_url('curso_hidrantes') ?>" class="">Hidratantes y equipo contra incendio</a></li>
                                        <!--<li><a href="<?php //echo base_url() ?>">Sistema de bombeo</a></li>
                                        <li><a href="<?php //echo base_url() ?>">Válvulas de seccionamiento y control</a></li>
                                        <li><a href="<?php //echo base_url() ?>">Válvulas expulsortas de aire</a></li>-->
                                    </ul>
                                </li>
                                <li><a href="<?php echo base_url('producto') ?>"  class="mn-has-sub <?php echo ($this->uri->segment(1) == 'producto')? "active_new" : ''; ?>
                                <?php echo ($this->uri->segment(1) == 'producto_reparacion_de_tuberia')? "active_new" : ''; ?>
                                <?php echo ($this->uri->segment(1) == 'producto_tuberia_conexiones')? "active_new" : ''; ?>
                                <?php echo ($this->uri->segment(1) == 'producto_contra_incendio')? "active_new" : ''; ?>
                                <?php echo ($this->uri->segment(1) == 'producto_conduccion_derivacion')? "active_new" : ''; ?>
                                <?php echo ($this->uri->segment(1) == 'producto_alcantarillado')? "active_new" : ''; ?>
                                <?php echo ($this->uri->segment(1) == 'producto_sistema_bombeo')? "active_new" : ''; ?>
                                <?php echo ($this->uri->segment(1) == 'producto_valvulas')? "active_new" : ''; ?>
                                <?php echo ($this->uri->segment(1) == 'producto_filtros')? "active_new" : ''; ?>
                                <?php echo ($this->uri->segment(1) == 'producto_medidores')? "active_new" : ''; ?>">Productos <i class="fa fa-angle-down"></i></a>
                                  <ul class="mn-sub">
                                        <li><a href="<?php echo base_url('producto_reparacion_de_tuberia/1') ?>">Reparación de tubería</a></li>
                                        <li><a href="<?php echo base_url('producto_tuberia_conexiones/1') ?>">Tubería y conexiones</a></li>
                                        <li><a href="<?php echo base_url('producto_contra_incendio/1') ?>">Contra incendio</a></li>
                                        <li><a href="<?php echo base_url('producto_conduccion_derivacion/1') ?>">Conducción y derivación</a></li>
                                        <li><a href="<?php echo base_url('producto_alcantarillado/1') ?>">Alcantarillado</a></li>
                                        <li><a href="<?php echo base_url('producto_sistema_bombeo/1') ?>">Sistemas de bombeo</a></li>
                                        <li><a href="<?php echo base_url('producto_valvulas/1') ?>">Válvulas y cajas de válvulas</a></li>
                                        <li><a href="<?php echo base_url('producto_filtros/1') ?>">Filtros</a></li>
                                        <li><a href="<?php echo base_url('producto_medidores/1') ?>">Medidores</a></li>
                                    </ul>
                                </li>
                                <li><a href="<?php echo base_url('tienda') ?>" class="<?php echo ($this->uri->segment(1) == 'tienda')? "active_new" : ''; ?>
                                <?php echo ($this->uri->segment(1) == 'tienda_reparacion_de_tuberia')? "active_new" : ''; ?>
                                <?php echo ($this->uri->segment(1) == 'tienda_reparacion_de_tuberia_prod')? "active_new" : ''; ?>
                                <?php echo ($this->uri->segment(1) == 'tienda_tuberia_conexiones')? "active_new" : ''; ?>
                                <?php echo ($this->uri->segment(1) == 'tienda_contra_incendio')? "active_new" : ''; ?>
                                <?php echo ($this->uri->segment(1) == 'tienda_conduccion_derivacion')? "active_new" : ''; ?>
                                <?php echo ($this->uri->segment(1) == 'tienda_alcantarillado')? "active_new" : ''; ?>
                                <?php echo ($this->uri->segment(1) == 'tienda_sistema_bombeo')? "active_new" : ''; ?>
                                <?php echo ($this->uri->segment(1) == 'tienda_valvulas')? "active_new" : ''; ?>
                                <?php echo ($this->uri->segment(1) == 'tienda_filtros')? "active_new" : ''; ?>
                                <?php echo ($this->uri->segment(1) == 'tienda_medidores')? "active_new" : ''; ?>">Tienda</a></li>
                                <li><a href="<?php echo base_url('contacto') ?>" class=" <?php echo ($this->uri->segment(1) == 'contacto')? "active_new" : ''; ?>">Contacto</a></li>
                                <!-- cart -->
                                <li class="right"><a href="<?php echo base_url('cart') ?>" class="mn-has-sub"><i class="fa fa-shopping-cart"></i> (<span id="update_cart"><?php 
                                if(isset($_SESSION['carro'])){$cart_session =  $_SESSION['carro']; $num_articulos = 0;if($cart_session){foreach ($cart_session as $prod){$num_articulos += $prod['cantidad'];}echo $num_articulos;} else { echo '0'; }}else { echo '0'; } ?></span>)</a>
                                    <ul class="mn-sub">
                                        <li>  <?php
                                if(isset($_SESSION['login']['nombre'])){
                                    echo ($_SESSION['login']['nombre']) ? "<span style='font-size:11px;display:block;'>Bienvenido: ".$_SESSION['login']['nombre']."</span>"
                                            . "<a href='". base_url('perfil') ."' style='font-size:10px;'>Perfil de Usuario</a>"
                                            . "<a href='". base_url('historial_compra_usuario') ."' style='font-size:10px;'>Historial de Compras</a>"
                                            . "<a href='". site_url('welcome/log_out') ."' style='font-size:10px;'>Cerrar Sesión</a>":'';

                                } else { ?>
                                    <a href="<?php echo base_url('login') ?>/1" class=" <?php echo ($this->uri->segment(2) == '1' && $this->uri->segment(1) == 'login')? "active_new" : ''; ?>"><i class="fa fa-user"></i> Iniciar sesión</a>
                                 <?php
                                }
                                 ?></li>
                                          <?php
                                if(!isset($_SESSION['login']['nombre'])){
                                    ?>
                                        <li><a href="<?php echo base_url('login') ?>/2" class="<?php echo ($this->uri->segment(2) == '2'  && $this->uri->segment(1) == 'login')? "active_new" : ''; ?>"><i class="fa fa-user"></i> Regsitrarse</a></li>
                                        <?php } ?>
                                    </ul>
                                </li>
                                <!-- End cart  -->
                                <!-- login --
                                <li class="right">
                                <?php
                                if(isset($_SESSION['login']['nombre'])){
                                    echo ($_SESSION['login']['nombre']) ? "<span style='font-size:11px;display:block;'>Bienvenido: ".$_SESSION['login']['nombre']."</span><a href='". site_url('welcome/log_out') ."' style='font-size:10px;'>Cerrar Sesión</a>":'';

                                } else {?>
                                    <a href="<?php echo base_url('login') ?>" class=" <?php echo ($this->uri->segment(1) == 'login')? "active_new" : ''; ?>"><i class="fa fa-user"></i></a>
                                 <?php
                                }
                                 ?>
                                </li>
                                <!-- End login -->
                                <!-- search -->
                                <li class="right">
                                    <a href="#" class="mn-has-sub"><img src="<?php echo base_url('public/theme/images/search.png') ?>" alt="" />&nbsp;<i class="fa fa-angle-down"></i></a>
                                    <ul class="mn-sub">
                                    <li>
                                        <div class="mn-wrap">
                                            <form action="javascript:buscar()" id="search_" class="search" method="post">
                                                <div class="search-wrap">
                                                    <button class="search-button animate" type="button" title="Empieza a Buscar">
                                                        <i class="fa fa-search"></i>
                                                    </button>
                                                    <input type="text" class="form-control search-field" placeholder="Buscar producto" name="buscar" id="buscar">
                                                </div>
                                            </form>
                                        </div>
                                    </li>
                                </ul>
                                </li>
                                <!-- End search -->
                            </ul>
                        </div>
                        <!-- End Main Menu -->
                    </div>
                </nav>
                <!-- End Navigation panel -->
            </div>
            <!-- End Menu Wrapper -->
            </div>

  <!-- Contenido -->
      <?php
      echo $this->load->view($this->data['vista'], array(),true);
      ?>
  <div id="prod"></div>
  <!-- Contenido -->
  <section class="page-section hidden-lg hidden-md"></section>

   <!-- Foter -->
   <footer class="small-section footer pb-10 pt-10" style="background:#000;">
                <div class="container">

                    <!-- Footer Widgets -->
                    <div class="row align-left">

                        <!-- Widget -->
                        <div class="col-xs-6 col-sm-3 col-md-2">
                            <div class="widget mb-0">
                                <h5 class="widget-title font-alt">Sucursal Monterrey</h5>
                                <div class="widget-body">
                                    <div class="widget-text clearfix">
                                    <label style="color:#fff;font-size:12px;font-weight: lighter;font-family: 'AvenirNextLTProRegular';">Tel. (81) 83 88 98 00 y 01<br>
Calle Alamo #3040<br>
Col. Moderna<br>
Monterrey, Nuevo León<br>
CP: 64530<br>
</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Widget -->

                        <!-- Widget -->
                        <div class="col-xs-6 col-sm-3 col-md-2">
                            <div class="widget mb-0">

                                <h5 class="widget-title font-alt">Sucursal Guadalajara</h5>

                                <div class="widget-body">
                                   <label style="color:#fff;font-size:12px;font-weight: lighter;font-family: 'AvenirNextLTProRegular';">Tel. 36590529 y 36590273<br>
Dr. R. Michel #2142<br>
Col. Atlas<br>
Guadalajara, Jalisco<br>
CP: 44767<br>
<br><br>
</label>
                                </div>

                            </div>
                        </div>
                        <!-- End Widget -->

                        <!-- Widget -->
                        <div class="col-xs-6 col-sm-3 col-md-2">
                            <div class="widget mb-0">

                                <h5 class="widget-title font-alt">Sucursal Silao</h5>

                                <div class="widget-body">
                                  <label style="color:#fff;font-size:12px;font-weight: lighter;font-family: 'AvenirNextLTProRegular';">Tel. 4727910268 <br>
Paseo de los Industriales Ote.<br>
Lot. 91 M01 Parque Industrial FIPASI<br>
Silao, Guanajuato.<br>
CP: 36270<br>
</label>
                                </div>

                            </div>
                            <!-- End Widget -->
                        </div>
                        <!-- End Widget -->

                        <!-- Widget -->
                        <div class="col-xs-6 col-sm-3 col-md-3">
                            <div class="widget mb-0">
                                <h5 class="widget-title font-alt">Sucursal Aguascalientes</h5>
                                <div class="widget-body">
                                  <label style="color:#fff;font-size:12px;font-weight: lighter;font-family: 'AvenirNextLTProRegular';">Tel. 4499128000 <br>
Blvd. A. Zacatecas No.113 Int 1<br>
San José de Arenal,<br>
Aguascalientes, Ags<br>
CP: 20140<br>
</label>
                                </div>

                            </div>
                        </div>
                        <!-- End Widget -->
                        <div class="col-xs-12 col-sm-12 col-md-3">
                            <label class="mt-60 mt-xs-20 text-center" style="color:#fff;font-size:14px;font-weight: lighter;font-family: 'AvenirNextLTProRegular';">
                                 &copy; 2015 Kuroda Soluciones Hidráulicas S.A. de C.V. Todos los Derechos. </label>
                        </div>
                    </div>
                    <!-- End Footer Widgets -->


                 </div>


                 <!-- Top Link -->
                 <div class="local-scroll">
                     <a href="#top" class="link-to-top"><i class="fa fa-caret-up"></i></a>
                 </div>
                 <!-- End Top Link -->

            </footer>
            <!-- End Foter -->
        </div>



        
        <script type="text/javascript" src="<?php echo base_url('public/theme') ?>/js/js/jquery.easing.1.3.js"></script>
        <script type="text/javascript" src="<?php echo base_url('public/theme') ?>/js/jquery.mobile.customized.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('public/theme') ?>/js/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('public/theme') ?>/js/js/SmoothScroll.js"></script>
        <script type="text/javascript" src="<?php echo base_url('public/theme') ?>/js/js/jquery.scrollTo.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('public/theme') ?>/js/js/jquery.localScroll.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('public/theme') ?>/js/js/jquery.viewport.mini.js"></script>
        <script type="text/javascript" src="<?php echo base_url('public/theme') ?>/js/js/jquery.countTo.js"></script>
        <script type="text/javascript" src="<?php echo base_url('public/theme') ?>/js/js/jquery.appear.js"></script>
        <script type="text/javascript" src="<?php echo base_url('public/theme') ?>/js/js/jquery.sticky.js"></script>
        <script type="text/javascript" src="<?php echo base_url('public/theme') ?>/js/js/jquery.parallax-1.1.3.js"></script>
        <script type="text/javascript" src="<?php echo base_url('public/theme') ?>/js/js/jquery.fitvids.js"></script>
        <script type="text/javascript" src="<?php echo base_url('public/theme') ?>/js/js/owl.carousel.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('public/theme') ?>/js/js/isotope.pkgd.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('public/theme') ?>/js/js/imagesloaded.pkgd.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('public/theme') ?>/js/js/jquery.magnific-popup.min.js"></script>
        <!--<script type="text/javascript" src="<?php echo base_url('public/theme') ?>/js/camera.min.js"></script>-->
           <!-- Replace test API Key "" with your own one below
        **** You can get API Key here - https://developers.google.com/maps/documentation/javascript/get-api-key -->
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCJttQY3k3BZzwVNmaRgyOSe6Bkvgnp4o0"></script>
        <script type="text/javascript" src="<?php echo base_url('public/theme') ?>/js/js/gmap3.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('public/theme') ?>/js/js/wow.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('public/theme') ?>/js/js/masonry.pkgd.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('public/theme') ?>/js/js/jquery.simple-text-rotator.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('public/theme') ?>/js/js/all.js"></script>
        <script type="text/javascript" src="<?php echo base_url('public/theme') ?>/js/js/contact-form.js"></script>
        <!--<script type="text/javascript" src="<?php //echo base_url('public/theme') ?>/js/js/jquery.ajaxchimp.min.js"></script>-->
        <!--[if lt IE 10]><script type="text/javascript" src="js/placeholder.js"></script><![endif]-->
        <script type="text/javascript" src="https://openpay.s3.amazonaws.com/openpay.v1.min.js"></script>
        <script type='text/javascript' src="https://openpay.s3.amazonaws.com/openpay-data.v1.min.js"></script>
        <script src="<?php echo base_url('public/theme') ?>/js/jquery.payment.js" type="text/javascript"></script>
        <script src="<?php echo base_url('public/theme') ?>/js/checkout.js" type="text/javascript"></script>
        
        <script src="<?php echo base_url('public/theme') ?>/js/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url('public/theme') ?>/js/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script>

  <?php  if ($this->uri->segment(1) == 'producto_reparacion_de_tuberia') { ?>
              //$(".accordion > dt > a").first().addClass("active");
   $(".accordion > dt > a:eq(0)").addClass("active");
    $(".accordion > dt > a:eq(0)").parent().next().slideDown("easeOutExpo");
    $(".accordion > dd").not($(".accordion > dt > a:eq(0)").parent().next("dd")).slideUp("easeInExpo");
  <?php } ?>
<?php  if ($this->uri->segment(1) == 'producto_tuberia_conexiones') { ?>
    $(".accordion > dt > a:eq(1)").addClass("active");
    $(".accordion > dt > a:eq(1)").parent().next().slideDown("easeOutExpo");
    $(".accordion > dd").not($(".accordion > dt > a:eq(1)").parent().next("dd")).slideUp("easeInExpo");
<?php } ?>

<?php  if ($this->uri->segment(1) == 'producto_contra_incendio') { ?>
    $(".accordion > dt > a:eq(2)").addClass("active");
    $(".accordion > dt > a:eq(2)").parent().next().slideDown("easeOutExpo");
    $(".accordion > dd").not($(".accordion > dt > a:eq(2)").parent().next("dd")).slideUp("easeInExpo");
<?php } ?>
<?php  if ($this->uri->segment(1) == 'producto_conduccion_derivacion') { ?>
    $(".accordion > dt > a:eq(3)").addClass("active");
    $(".accordion > dt > a:eq(3)").parent().next().slideDown("easeOutExpo");
    $(".accordion > dd").not($(".accordion > dt > a:eq(3)").parent().next("dd")).slideUp("easeInExpo");
<?php } ?>

<?php  if ($this->uri->segment(1) == 'producto_alcantarillado') { ?>
    $(".accordion > dt > a:eq(4)").addClass("active");
    $(".accordion > dt > a:eq(4)").parent().next().slideDown("easeOutExpo");
    $(".accordion > dd").not($(".accordion > dt > a:eq(4)").parent().next("dd")).slideUp("easeInExpo");
<?php } ?>
<?php  if ($this->uri->segment(1) == 'producto_sistema_bombeo') { ?>
    $(".accordion > dt > a:eq(5)").addClass("active");
    $(".accordion > dt > a:eq(5)").parent().next().slideDown("easeOutExpo");
    $(".accordion > dd").not($(".accordion > dt > a:eq(5)").parent().next("dd")).slideUp("easeInExpo");
<?php } ?>
<?php  if ($this->uri->segment(1) == 'producto_valvulas') { ?>
    $(".accordion > dt > a:eq(6)").addClass("active");
    $(".accordion > dt > a:eq(6)").parent().next().slideDown("easeOutExpo");
    $(".accordion > dd").not($(".accordion > dt > a:eq(6)").parent().next("dd")).slideUp("easeInExpo");
<?php } ?>
<?php  if ($this->uri->segment(1) == 'producto_filtros') { ?>
    $(".accordion > dt > a:eq(7)").addClass("active");
    $(".accordion > dt > a:eq(7)").parent().next().slideDown("easeOutExpo");
    $(".accordion > dd").not($(".accordion > dt > a:eq(7)").parent().next("dd")).slideUp("easeInExpo");
<?php } ?>
<?php  if ($this->uri->segment(1) == 'producto_medidores') { ?>
    $(".accordion > dt > a:eq(8)").addClass("active");
    $(".accordion > dt > a:eq(8)").parent().next().slideDown("easeOutExpo");
    $(".accordion > dd").not($(".accordion > dt > a:eq(8)").parent().next("dd")).slideUp("easeInExpo");
<?php } ?>
jQuery(function(){

  $(':checkbox').on('change',function(){
 var th = $(this), 
 name = th.attr('name'); 
 if(name == "panel"){
 if(th.is(':checked')){
     $(':checkbox[name="'  + name + '"]').not(th).prop('checked',false);   
  }
}
});

  $(':checkbox').on('change',function(){
 var th = $(this), 
 name = th.attr('name'); 
 if(name == "material"){
 if(th.is(':checked')){
     $(':checkbox[name="'  + name + '"]').not(th).prop('checked',false);   
  }
}
});

    });
    
    /*
      jQuery('#camera_wrap_2').camera({
                height: '26%',
                                          minHeight: '105px',
                loader: 'none',
                pagination: false,
                                         navigation: true,
                                         playPause: false,
                                         fx: 'simpleFade'

        });
});*/
    /*
    function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
   }*/

   function ValidateEmail(mail)
   {
        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))
         {
           return (true)
         }           
           return (false)
       }

   function checkLength(textbox,opcion){
    //var textbox = document.getElementById("textbox");
  //  alert(textbox.length);
  if(opcion == 1){
    if(textbox.length < 5){
      //alert("Se requiere como mínimo 5 caracteres.")
      return false;
    }
    }
    else if(opcion == 2){
        if(textbox.length < 10){
      //alert("Se requiere como mínimo 5 caracteres.")
      return false;
    }
    }
   }
   
   function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

   function EnviaProdContacto(){

        var $frm = $('#frm');
        var fields = $frm.serialize();
        var nombre =  $("#nombre").val();
        var empresa =  $("#empresa").val();
        var telefono =  $("#telefono").val();
        var email =  $("#correo").val();
        var cd = $("#ciudad").val();
        var mensaje =  $("#mensaje").val();
        var mailt = ValidateEmail(email);

        if(nombre == ""){
            alert('Se requiere del Nombre');
        }
        else if(checkLength(nombre,1) == false){
            alert("Se requiere como mínimo 5 caracteres para el Nombre.")
        }
        else if(empresa == ""){
            alert('Requerido el nombre de tu Empresa a la que perteneces');
        }
        else if(checkLength(empresa,1) == false){
            alert("Se requiere como mínimo 5 caracteres para la Empresa.")
        }
        else if(telefono == ""){
            alert('Requerido el número de Teléfono');
        }
        else if(checkLength(telefono,2) == false){
            alert("Se requiere como mínimo 10 caracteres para el teléfono.")
        }
        else if(checkLength(email,2) == false){
            alert("Se requiere como mínimo 10 caracteres paea el email.")
        }
        else if(mailt == false){
            alert('El Correo es invalido');

        } else if(cd == ""){
            alert('Requerido la ciudad de origen');
            
        } else if(checkLength(cd,1) == false){
            alert('Se requiere como mínimo 5 caracteres en el campo de la ciudad.');
            
        }else if(mensaje == ""){
            alert('Se requiere la información de tu mensaje');
        }
        else if(checkLength(mensaje,2) == false){
            alert("Se requiere como mínimo 10 caracteres para el mensaje.")
        }
        else if(nombre != '' && empresa != '' && telefono != '' && email != '' && mailt == true && mensaje != ''){
            $.ajax({
            type: "POST",
            url: $frm.attr('action'),
            data: fields,
            success: function(response) {
                 console.log(response);
                if(response.valid){
                    alert("Su petición ha sido enviada en breve alguien de nuestro equipo dará seguimiento a la solicitud recibida. Gracias");
                    $('#frm input[type=text]').val('');
                    $('#frm textarea').val('');
                }else{
                    alert(response.response);
                }
            }
        });
    }
}

$(function(){
   $(".banner_p").hover(function(){
      $(this).find("h3").toggleClass("colores");
    }
                   ,function(){
                        $(this).find("h3").toggleClass("colores");
                    }
                   );



   $(".hov").hover(function(){
      $(this).find("img").toggleClass("hover");
    }
                   ,function(){
                        $(this).find("img").toggleClass("hover");
                    }
                   );


});
	</script>
<script>
         function TipoFlujo(id){

             var str= $("#frm").serialize();
             if(id != ""){
             $.ajax({
                 type:'post',
                 url: '<?php echo site_url('ajax/flujos') ?>',
                 data:'id='+id,
                 dataType:'json',
                 success: function(data){
                     //alert(data);
                     if(data != ""){
                         console.log(data);
                         $("#opera").html(data.html);
                     }else{
                         $("#aplica").html("");
                         $("#instala").html("");
                         $("#informa").html("");
                         $("#acciona").html("");
                         $("#informa_acciona").html("");
                         $("#opera").html("");
                         document.getElementById("inf").style.display='none';
                         document.getElementById("agr").style.display='none';
                         document.getElementById("obp").style.display='none';
                         document.getElementById("min").style.display='none';
                         document.getElementById("dom").style.display='none';

                         document.getElementById("city").style.display='block';
                         document.getElementById("rsum").style.display='none';
                         document.getElementById("rsup").style.display='none';
                         document.getElementById("lim_sup").style.display='none';
                         document.getElementById("lim_sum").style.display='none';
                         document.getElementById("eci").style.display='none';
                         document.getElementById("epc").style.display='none';
                     }
                 }
             });
             }

         }

    function Filtros(id)
    {

        var str= $("#frm").serialize();
       var ci =   document.getElementById('condicion_operacion').value;

       if(id != ""){
        $.ajax({
        type:'post',
        url:'<?php echo site_url('ajax/filtros') ?>',
        data:'id='+id,
        dataType:'json',
        success: function(data){
              //alert(data);
        console.log(data);
         var sep = data.html.split('|');
         var sep = data.html.split('|');
          $("#aplica").html(sep[0]);
          $("#instala").html(sep[1]);
          $("#informa").html(sep[2]);
          $("#acciona").html(sep[3]);
          $("#informa_acciona").html(sep[4]);
          if(ci != 'equipos contra incendios'){
             document.getElementById('acciona').style.display = 'block';
             document.getElementById('informa_acciona').style.display = 'block';
          }else{
               document.getElementById('informa').style.display = 'none';
               document.getElementById('acciona').style.display = 'none';
               document.getElementById('informa_acciona').style.display = 'none';

          }
          if(id=='superficie'){
                     document.getElementById("inf").style.display='none';
            document.getElementById("agr").style.display='none';
            document.getElementById("obp").style.display='none';
            document.getElementById("min").style.display='none';
            document.getElementById("dom").style.display='none';
            document.getElementById("eci").style.display='none';
            document.getElementById("city").style.display='none';
            document.getElementById("epc").style.display='none';
             document.getElementById("rsum").style.display='none';
             document.getElementById("rsup").style.display='none';
             document.getElementById("lim_sup").style.display='block';
             document.getElementById("lim_sum").style.display='none';

           }else if(id=='sumergible'){
                     document.getElementById("inf").style.display='none';
            document.getElementById("agr").style.display='none';
            document.getElementById("obp").style.display='none';
            document.getElementById("min").style.display='none';
            document.getElementById("dom").style.display='none';
            document.getElementById("eci").style.display='none';
            document.getElementById("city").style.display='none';
            document.getElementById("epc").style.display='none';
             document.getElementById("rsum").style.display='none';
             document.getElementById("rsup").style.display='none';
             document.getElementById("lim_sup").style.display='none';
             document.getElementById("lim_sum").style.display='block';

           }else if(id=='superficie_residual'){
                     document.getElementById("inf").style.display='none';
            document.getElementById("agr").style.display='none';
            document.getElementById("obp").style.display='none';
            document.getElementById("min").style.display='none';
            document.getElementById("dom").style.display='none';
            document.getElementById("eci").style.display='none';
            document.getElementById("city").style.display='none';
            document.getElementById("epc").style.display='none';
             document.getElementById("rsum").style.display='none';
             document.getElementById("rsup").style.display='block';
             document.getElementById("lim_sup").style.display='none';
             document.getElementById("lim_sum").style.display='none';

           }else if(id=='sumergible_residual'){
                     document.getElementById("inf").style.display='none';
            document.getElementById("agr").style.display='none';
            document.getElementById("obp").style.display='none';
            document.getElementById("min").style.display='none';
            document.getElementById("dom").style.display='none';
            document.getElementById("eci").style.display='none';
            document.getElementById("city").style.display='none';
            document.getElementById("epc").style.display='none';
            document.getElementById("rsum").style.display='block';
             document.getElementById("rsup").style.display='none';
               document.getElementById("lim_sup").style.display='none';
             document.getElementById("lim_sum").style.display='none';

           }else if(id == 'equipos de presión-booster'){
                   document.getElementById("inf").style.display='none';
            document.getElementById("agr").style.display='none';
            document.getElementById("obp").style.display='none';
            document.getElementById("min").style.display='none';
            document.getElementById("dom").style.display='none';
            document.getElementById("eci").style.display='none';
            document.getElementById("city").style.display='none';
            document.getElementById("epc").style.display='block';
            document.getElementById("rsum").style.display='none';
             document.getElementById("rsup").style.display='none';
               document.getElementById("lim_sup").style.display='none';
             document.getElementById("lim_sum").style.display='none';
          }
          else if(id== 'equipos contra incendios'){
            document.getElementById("inf").style.display='none';
            document.getElementById("agr").style.display='none';
            document.getElementById("obp").style.display='none';
            document.getElementById("min").style.display='none';
            document.getElementById("dom").style.display='none';
            document.getElementById("epc").style.display='none';
            document.getElementById("city").style.display='none';
            document.getElementById("eci").style.display='block';
             document.getElementById("rsum").style.display='none';
             document.getElementById("rsup").style.display='none';
               document.getElementById("lim_sup").style.display='none';
             document.getElementById("lim_sum").style.display='none';
          } else if(id== ''){

                document.getElementById("city").style.display='block';
                document.getElementById("rsum").style.display='none';
                document.getElementById("rsup").style.display='none';
                document.getElementById("lim_sup").style.display='none';
                document.getElementById("lim_sum").style.display='none';
                document.getElementById("eci").style.display='none';
                document.getElementById("epc").style.display='none';
          }


        document.getElementById('acciona').style.display = 'none';
               document.getElementById('informa_acciona').style.display = 'none';
               document.getElementById('informa').style.display = 'none';


        }
        });
        }
     }

       function EnviaInformacion()
       {

        var str= $("#frm").serialize();
        $.ajax({

        type:'post',
        url:'<?php echo site_url('ajax/sistemas_de_bombeo') ?>',
        data:str,
        dataType:'json',
        success: function(data){
            var sep = data.split('|');
             if(sep[0] == 0){
                     //$("input[type=text]").val('');
                     alert(sep[1]);
        }
                if(sep[0] == 1){
                 alert(sep[1]);
                 $("input[type=text]").val('');
                 $("#frm textarea").val('');
                 }
        }

             });
       }

       function Abre(id)
       {

           if(id==1){
               document.getElementById('si_normado').style.display = 'block';
               document.getElementById('informa').style.display = 'block';
               document.getElementById('acciona').style.display = 'block';
               document.getElementById('informa_acciona').style.display = 'block';
               document.getElementById('no_normado').style.display = 'none';

           }else if(id==2){
               document.getElementById('si_normado').style.display = 'none';
               document.getElementById('informa').style.display = 'none';
               document.getElementById('acciona').style.display = 'none';
               document.getElementById('informa_acciona').style.display = 'none';
               document.getElementById('no_normado').style.display = 'block';

           }
       }

    function showText(id)
    {
        if(id==1){
               document.getElementById('presion_constante_si').style.display = 'block';
               document.getElementById('presion_constante_si').value="";
               document.getElementById('tit').style.display = 'block';


           }else if(id==2){
               document.getElementById('presion_constante_si').style.display = 'none';
               document.getElementById('presion_constante_si').value="";
            document.getElementById('tit').style.display = 'none';

           }
    }
    
    function ShowR(id)
    {
           if(id==1){
               document.getElementById('acciona').style.display = 'block';
               document.getElementById('informa_acciona').style.display = 'block';
               document.getElementById('informa').style.display = 'block';
               document.getElementById('observa_text').style.display = 'block';


           }else if(id==2){
                document.getElementById('acciona').style.display = 'none';
               document.getElementById('informa_acciona').style.display = 'none';
               document.getElementById('informa').style.display = 'none';
                document.getElementById('observa_text').style.display = 'none';


           }

    }

    function C(id)
    {

        if(id == 'Combustión'){
            document.getElementById('comb').style.display = 'block';
        }else{
            document.getElementById('comb').style.display = 'none';
        }
    }

     function MuestraImagen(id)
     {

        if(id=="Domestisco"){
            document.getElementById("dom").style.display='block';
            document.getElementById("obp").style.display='none';
            document.getElementById("inf").style.display='none';
            document.getElementById("agr").style.display='none';
            document.getElementById("min").style.display='none';
            document.getElementById("eci").style.display='none';
            document.getElementById("epc").style.display='none';

        }else if(id=="Edificacion"){
            document.getElementById("inf").style.display='none';
            document.getElementById("agr").style.display='none';
            document.getElementById("obp").style.display='none';
            document.getElementById("min").style.display='none';
            document.getElementById("dom").style.display='none';
                document.getElementById("eci").style.display='none';
            document.getElementById("epc").style.display='none';
        }else if(id=="Industrial"){
            document.getElementById("inf").style.display='none';
            document.getElementById("agr").style.display='none';
            document.getElementById("obp").style.display='none';
            document.getElementById("min").style.display='none';
            document.getElementById("dom").style.display='none';
                document.getElementById("eci").style.display='none';
            document.getElementById("epc").style.display='none';
        }else if(id=="Mineria"){
            document.getElementById("min").style.display='block';
            document.getElementById("dom").style.display='none';
            document.getElementById("agr").style.display='none';
            document.getElementById("inf").style.display='none';
            document.getElementById("obp").style.display='none';
                document.getElementById("eci").style.display='none';
            document.getElementById("epc").style.display='none';

        }else if(id=="Obra Pública"){
            document.getElementById("obp").style.display='block';
            document.getElementById("inf").style.display='none';
            document.getElementById("agr").style.display='none';
            document.getElementById("dom").style.display='none';
            document.getElementById("min").style.display='none';
                document.getElementById("eci").style.display='none';
            document.getElementById("epc").style.display='none';

        }else if(id=="Agropecuario"){
            document.getElementById("agr").style.display='block';
            document.getElementById("dom").style.display='none';
            document.getElementById("min").style.display='none';
            document.getElementById("obp").style.display='none';
            document.getElementById("inf").style.display='none';
                document.getElementById("eci").style.display='none';
            document.getElementById("epc").style.display='none';

        }else if(id=="Infraestructura"){
            document.getElementById("inf").style.display='block';
            document.getElementById("agr").style.display='none';
            document.getElementById("obp").style.display='none';
            document.getElementById("min").style.display='none';
            document.getElementById("dom").style.display='none';
                document.getElementById("eci").style.display='none';
            document.getElementById("epc").style.display='none';

        }else if(id=="" || id=='Otro'){
             document.getElementById("inf").style.display='none';
            document.getElementById("agr").style.display='none';
            document.getElementById("obp").style.display='none';
            document.getElementById("min").style.display='none';
            document.getElementById("dom").style.display='none';
                document.getElementById("eci").style.display='none';
            document.getElementById("epc").style.display='none';
            document.getElementById("city").style.display='block';
        }

    }
      $(document).ready(function() {

     $('#buscar').keyup(function(event) {
          var str= $("#search_").serialize();
      $.ajax({
        url:'<?php echo site_url('ajax/filtro_productos') ?>',
        type: 'POST',
        data: $(this).serialize(),
        dataType: 'json',
        success: function(data) {
        $('#prod').empty();
        console.log(data); 
        // console.log("success");
      }
  /*    .fail(function() {
        console.log(data);*/
      })
    /*  .always(function() {
        // console.log("complete");
      });*/

    });
    
    
  });
</script>
<script type="text/javascript">

                  function submit_form(select_tag,tipo)
                  {
                         
                      var d = 1000;
                      var c =    document.getElementById("cantidad_agregar"+select_tag).value;
                      var sk =   document.forms['frm'+select_tag]['skus'].value;
                      var can = document.forms['frm'+select_tag]['cantidad'].value;
                      var exis = document.forms['frm'+select_tag]['existencia'].value;

                      if(tipo == 'mas'){
                          
                          if(parseInt(c) <= parseInt(d)){
                              if(parseInt(can) < parseInt(exis))
                              {
                               document.getElementById("cantidad_agregar"+select_tag).value = parseInt(c)+parseInt(1);
                               document.forms['frm'+select_tag]['cantidad'].value = parseInt(c)+parseInt(1);                               
                              }else if(parseInt(can) == parseInt(exis))
                              {
                               document.forms['frm'+select_tag]['cantidad'].value = parseInt(c)+parseInt(1);
                              }
                              
                              // document.getElementById('frm'+select_tag).submit();
                              // var $frm = document.getElementById('frm'+select_tag);
                              var $frm = $('#frm'+select_tag);
                              
                             $.ajax({
                                  url:$frm.attr('action'),
                                  type: 'POST',
                                  data: $frm.serialize(),
                                  dataType: 'json',
                                  success: function(data) {
                                    console.log(data);
                                    var sep = data.split('|');
                                    //i=0;i<count;i++;
                                    //alert(can+' '+exis);
                                    if(sep[0] == 'no')
                                    {
                                    $('#table_content').empty();
                                    $('#table_content').append(sep[1]);
                                    
                                    }else if(sep[0] == 'si'){
                                     // alert("imprime_cotizacion");
                                    $(".cart_quantity_up").css({'display':'none'});
                                    $('#table_content_cot').empty();
                                    $('#table_content_cot').append(sep[1]);
                                   }

                                  }
                              });


                          }


                      }else if(tipo == 'menos'){
                          
                           var r =  parseInt(c) - parseInt(1);
                            if(r != 0){
                                
//                                 document.getElementById("cantidad_agregar"+select_tag).value = parseInt(c) - parseInt(1);
                                 document.forms['frm'+select_tag]['cantidad'].value = parseInt(c) - parseInt(1);
                              
                              var $frm = $('#frm'+select_tag);
                              
                             $.ajax({
                                  url:$frm.attr('action'),
                                  type: 'POST',
                                  data: $frm.serialize(),
                                  dataType: 'json',
                                  success: function(data) {
                                    console.log(data);
                                    var sep = data.split('|');
//                                    alert(sep[2]);
                                    
                                    
                                    if(sep[0] == 'no')
                                   {
                                    $(".cart_quantity_up").css({'display':'block'});                                      
                                    $('#table_content').empty();
                                    $('#table_content').append(sep[1]);
                                   }else if(sep[0] == 'si'){
                                     // alert("imprime_cotizacion");
                                   if(sep[2] == ""){
                                       document.getElementById("cantidad_agregar"+select_tag).value = parseInt(c) - parseInt(1);
                                   }
                                    $('#table_content_cot').empty();
                                    $('#table_content_cot').append(sep[1]);
                                   }
                                  }
                              });
                          }

                      }

            //    var form_id = select_tag.parentNode.id;
                /*console.log(form_id);*/

            }
            
              function submit_form_cot(select_tag,tipo)
              {
                         //alert(select_tag);
                      var d = 1000;
                      var c = document.getElementById("cantidad_agregar_cot"+select_tag).value;
                      var sk = document.forms['frm_cot'+select_tag]['skus'].value;
                      //alert(sk);
                      //alert(parseInt(c)+parseInt(1));
                      if(tipo == 'mas'){

                          if(parseInt(c) <= parseInt(d)){
                              
                              document.getElementById("cantidad_agregar_cot"+select_tag).value = parseInt(c)+parseInt(1);
                              document.forms['frm_cot'+select_tag]['cantidad'].value = parseInt(c)+parseInt(1);
                              var $frm = $('#frm_cot'+select_tag);
                              $.ajax({
                                  url:$frm.attr('action'),
                                  type: 'POST',
                                  data: $frm.serialize(),
                                  dataType: 'json',
                                  success: function(data) {
                                    console.log(data);
                                    $('#table_content_cot').empty();
                                    $('#table_content_cot').append(data);

                                  }
                              });


                          }


                      }else if(tipo == 'menos'){
                           var r =  parseInt(c) - parseInt(1);

                            if(r != 0){
                              document.getElementById("cantidad_agregar_cot"+select_tag).value = parseInt(c) - parseInt(1);
//                               form.document.getElementById("cantidad").value = parseInt(c) - parseInt(1);
                             document.forms['frm_cot'+select_tag]['cantidad'].value = parseInt(c) - parseInt(1);
                             // document.getElementById('frm'+select_tag).submit();
                           var $frm = $('#frm_cot'+select_tag);                          
                             $.ajax({
                                  url:$frm.attr('action'),
                                  type: 'POST',
                                  data: $frm.serialize(),
                                  dataType: 'json',
                                  success: function(data) {
                                    console.log(data);
                                    $('#table_content_cot').empty();
                                    $('#table_content_cot').append(data);                                     
                                  }
                              });
                          }

                      }

            //    var form_id = select_tag.parentNode.id;
                /*console.log(form_id);*/

            }
            
            function MuestraProdconDiam(id)
            {
               // var panel = $("#panel").val();
                //var panel = $("input[name=panel[]]:checked").val();
                //var material= $("#material").val();
                var material= $("input[name=material]:checked").val();
                var rango = $("#rango").val();
                   var checked = []
          $("input[name='panel[]']:checked").each(function (){
              checked.push(parseInt($(this).val())); 
          });
          
                $.ajax({
                    url: '<?php echo site_url('welcome/diametros') ?>',
                    type: 'POST',
                    dataType: 'html',
                    data:{diametro:id,panel:checked,material:material,rango:rango}
        })
                .done(function(data) {
                    console.log(data);
            $('#filtros_de_productos').empty();
            $('#filtros_de_productos').append(data);
             ActivaPanel(id);
             MuestraProdDiam_rango(id);
//             MuestraImagen_abra();
        })
                .fail(function(data) {
                    console.log(data);
            $('#filtros_de_productos').empty();
            $('#filtros_de_productos').append(data.responseText);   
            console.log("error");
        })
                .always(function() {
                    
                });

            }
            
            function ActivaPanel(id){
            
          //   var panel = $("input[name=panel]:checked").val();
          var checked = []
          $("input[name='panel[]']:checked").each(function (){
              checked.push(parseInt($(this).val())); 
          });
          
           // alert(panel);
            $.ajax({
                    url: '<?php echo site_url('welcome/activa_panel') ?>',
                    type: 'POST',
                    dataType: 'html',
                    data:{diametro:id,panel:checked}
           })
                .done(function(data) {
                    console.log(data);
                    $('#paneles').empty();
                    $('#paneles').append(data);
          //  var cont = data.length;
           // alert(cont);
           })
                .fail(function(data) {
                    console.log(data);
                  
          })
               .always(function() {
                    
                });
                              
            
            }
            
            
            function MuestraProdDiam_rango(id){
            
             $.ajax({
                    url: '<?php echo site_url('welcome/rango_de_diametro') ?>',
                    type: 'POST',
                    dataType: 'html',
                    data:{diametro:id}
           })
                .done(function(data) {
                    console.log(data);
                    $('#lista_rango').empty();
                    $('#lista_rango').append(data);
          //  var cont = data.length;
           // alert(cont);
           })
                .fail(function(data) {
                    console.log(data);
                  
          })
               .always(function() {
                    
                });
        
            }
            
            
            function MuestraProdPanel(id)
            {
            
            
            var d = $("#diam").val();
            //var material= $("input[name=material]").val();
            var material= $("input[name=material]:checked").val();           
            var rango = $("#rango").val();
            var checked = []
            $("input[name='panel[]']:checked").each(function (){
                checked.push(parseInt($(this).val()));
            });
            //alert(checked);
            $.ajax({
                url: '<?php echo site_url('welcome/paneles') ?>',
                type: 'POST',
                dataType: 'html',
                data:{panel:checked,diametro:d,material:material,rango:rango}
             })
            .done(function(data) {
                console.log(data);
        $('#filtros_de_productos').empty();
        $('#filtros_de_productos').append(data);
        MuestraProdPanel_diam(checked,d);
        MuestraProdPanel_rango(checked,d);
//        MuestraImagen_abra();
    })
            .fail(function(data) {
                console.log(data);
                $('#filtros_de_productos').empty();
                $('#filtros_de_productos').append(data.responseText);
    })
            .always(function() {
                //console.log("complete");
      });
            
            }
            
               function MuestraProdPanel_diam(id,d)
            {
                $.ajax({
                    url: '<?php echo site_url('welcome/panel_diam') ?>',
                    type: 'POST',
                    dataType: 'html',
                    data:{panel:id,diametro:d}
               })
                .done(function(data) {
                    console.log(data);
            $('#lista_nominal').empty();
            $('#lista_nominal').append(data);
              })
                .fail(function(data) {
                    console.log(data);
            $('#lista_nominal').empty();
            $('#lista_nominal').append(data.responseText);
              })
                .always(function() {
                    //console.log("complete");
              });
            
            }
              
            function MuestraProdPanel_rango(id,d)
            {
                
                $.ajax({
                    url: '<?php echo site_url('welcome/panel_rang') ?>',
                    type: 'POST',
                    dataType: 'html',
                    data:{panel:id,diametro:d}
         })
                .done(function(data) {
                    console.log(data);
            $('#lista_rango').empty();
            $('#lista_rango').append(data);
        })
                .fail(function(data) {
                    console.log(data);
            $('#lista_rango').empty();
            $('#lista_rango').append(data.responseText);
        })
                .always(function() {
                    //console.log("complete");
            });
            
            }
            
            
            function MuestraProdMaterial(id)
            {
            
            var d = $("#diam").val();
          //  var panel= $("input[name=panel]:checked").val();//$("#panel").val();
            var rango = $("#rango").val();
            var checked = []
            $("input[name='panel[]']:checked").each(function ()
            {
                checked.push(parseInt($(this).val()));
            });
            
      /*      if(panel == 1 && id== 1){
                
                $("#img_simple").css({'display':'block'});
                $("#img_doble").css({'display':'none'});
                $("#img_triple").css({'display':'none'});
                $("#img_simple_ai").css({'display':'none'});
                $("#img_doble_ai").css({'display':'none'});
                $("#img_triple_ai").css({'display':'none'});
                
            }else if(panel == 2 && id == 1){
                
                $("#img_simple").css({'display':'none'});
                $("#img_doble").css({'display':'block'});
                $("#img_triple").css({'display':'none'});
                $("#img_simple_ai").css({'display':'none'});
                $("#img_doble_ai").css({'display':'none'});
                $("#img_triple_ai").css({'display':'none'});
                
            }else if(panel == 3 && id== 1){
                
                $("#img_simple").css({'display':'none'});
                $("#img_doble").css({'display':'none'});
                $("#img_triple").css({'display':'block'});
                $("#img_simple_ai").css({'display':'none'});
                $("#img_doble_ai").css({'display':'none'});
                $("#img_triple_ai").css({'display':'none'}); 
                
            }else if(panel == 1 && id== 2){
                
                $("#img_simple").css({'display':'none'});
                $("#img_doble").css({'display':'none'});
                $("#img_triple").css({'display':'none'});
                $("#img_simple_ai").css({'display':'block'});
                $("#img_doble_ai").css({'display':'none'});
                $("#img_triple_ai").css({'display':'none'});
                
            }else if(panel == 2 && id == 2){
                
                $("#img_simple").css({'display':'none'});
                $("#img_doble").css({'display':'none'});
                $("#img_triple").css({'display':'none'});
                $("#img_simple_ai").css({'display':'none'});
                $("#img_doble_ai").css({'display':'block'});
                $("#img_triple_ai").css({'display':'none'});
                
            }else if(panel == 3 && id== 3){
                
                $("#img_simple").css({'display':'none'});
                $("#img_doble").css({'display':'none'});
                $("#img_triple").css({'display':'none'});
                $("#img_simple_ai").css({'display':'none'});
                $("#img_doble_ai").css({'display':'none'});
                $("#img_triple_ai").css({'display':'block'});
            }*/
//
//
//
//
//
//  
            
            $.ajax({
                url: '<?php echo site_url('welcome/materiales') ?>',
                type: 'POST',
                dataType: 'html',
                data:{panel:checked,diametro:d,material:id,rango:rango}
          }).done(function(data) {
                console.log(data);
                $('#filtros_de_productos').empty();
                $('#filtros_de_productos').append(data);
                
          }).fail(function(data) {
                console.log(data);
                $('#filtros_de_productos').empty();
                $('#filtros_de_productos').append(data.responseText);
         }).always(function() {
                 //console.log("complete");
         });
            }
            
            function MuestraImagen_abra()
            {
             var d = $("#diam").val();
            var panel= $("input[name=panel]:checked").val();//$("#panel").val();
            var rango = $("#rango").val();
            var id = $("input[name=material]:checked").val();
           
            if(panel == 1 && id== 1){
                
                $("#img_simple").css({'display':'block'});
                $("#img_doble").css({'display':'none'});
                $("#img_triple").css({'display':'none'});
                $("#img_simple_ai").css({'display':'none'});
                $("#img_doble_ai").css({'display':'none'});
                $("#img_triple_ai").css({'display':'none'});
                
            }else if(panel == 2 && id == 1){
                
                $("#img_simple").css({'display':'none'});
                $("#img_doble").css({'display':'block'});
                $("#img_triple").css({'display':'none'});
                $("#img_simple_ai").css({'display':'none'});
                $("#img_doble_ai").css({'display':'none'});
                $("#img_triple_ai").css({'display':'none'});
                
            }else if(panel == 3 && id== 1){
                
                $("#img_simple").css({'display':'none'});
                $("#img_doble").css({'display':'none'});
                $("#img_triple").css({'display':'block'});
                $("#img_simple_ai").css({'display':'none'});
                $("#img_doble_ai").css({'display':'none'});
                $("#img_triple_ai").css({'display':'none'}); 
                
            }else if(panel == 1 && id== 2){
                
                $("#img_simple").css({'display':'none'});
                $("#img_doble").css({'display':'none'});
                $("#img_triple").css({'display':'none'});
                $("#img_simple_ai").css({'display':'block'});
                $("#img_doble_ai").css({'display':'none'});
                $("#img_triple_ai").css({'display':'none'});
                
            }else if(panel == 2 && id == 2){
                
                $("#img_simple").css({'display':'none'});
                $("#img_doble").css({'display':'none'});
                $("#img_triple").css({'display':'none'});
                $("#img_simple_ai").css({'display':'none'});
                $("#img_doble_ai").css({'display':'block'});
                $("#img_triple_ai").css({'display':'none'});
                
            }else if(panel == 3 && id== 2){
//                alert("entro");
                $("#img_simple").css({'display':'none'});
                $("#img_doble").css({'display':'none'});
                $("#img_triple").css({'display':'none'});
                $("#img_simple_ai").css({'display':'none'});
                $("#img_doble_ai").css({'display':'none'});
                $("#img_triple_ai").css({'display':'block'});
            }
            }
            
            
            function MuestraProdRango(id){
            
               
            var d = $("#diam").val();
//            var panel= $("input[name=panel]").val();//$("#panel").val();
            var material= $("input[name=material]:checked").val();
             var checked = []
            $("input[name='panel[]']:checked").each(function ()
            {
                checked.push(parseInt($(this).val()));
            });
            $.ajax({
                url: '<?php echo site_url('welcome/rangos') ?>',
                type: 'POST',
                dataType: 'html',
                data:{panel:checked,diametro:d,material:material,rango:id}
    })
            .done(function(data) {
                console.log(data);
        $('#filtros_de_productos').empty();
        $('#filtros_de_productos').append(data);
    })
            .fail(function(data) {
                console.log(data);
                $('#filtros_de_productos').empty();
                $('#filtros_de_productos').append(data.responseText);
    })
            .always(function() {
                //console.log("complete");
      });
      
            }
            
          function escapeHtml(text) {
  var map = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#039;'
  };

  return text.replace(/[&<>"']/g, function(m) { return map[m]; });
}  
            
            function AddCart(skus,nombre,precio,tipom){
            
            if(precio == ""){ precio = "0";}
            if(tipom == ""){ tipom = "no"}
            
            if(skus != "" && nombre != "" && precio !="" && tipom !=""){
                $("#skus").val(skus);
                $("#nprod").val(nombre);
                $("#prices").val(precio);
                $("#tipo_moneda").val(tipom);
                $("#frm-prod").submit();
            }
             
           /*   $.ajax({
                url: '<?php //echo site_url('welcome/crea_sesion_producto') ?>',
                type: 'POST',
                dataType: 'html',
                data:{sku:skus,nombre:nombre,precio:precio,cantidad:1,tipo_moneda:tipom}
                }).done(function(data) {
                            console.log(data);
                }).fail(function(data) {
                            console.log(data);
                            $('#filtros_de_productos').empty();
                            $('#filtros_de_productos').append(data.responseText);
                }).always(function() {
                            //console.log("complete");
               });*/
      
            
            }
            
            
            /*JUNTAS UNIVERSALES*/
            function MuestraRangoUni(id)
            {
                $.ajax({
                url: '<?php echo site_url('welcome/rango_universal') ?>',
                type: 'POST',
                dataType: 'html',
                data:{diametro:id}
               })
                .done(function(data) {
                    console.log(data);
                    $('#lista_rango_uni').empty();
                    $('#lista_rango_uni').append(data);
              })
                .fail(function(data) {
                    console.log(data);
                    $('#lista_rango_uni').empty();
                    $('#lista_rango_uni').append(data.responseText);
              })
                .always(function() {
                    //console.log("complete");
              });
            }
            function MuestraTamano(id){
               $.ajax({
                url: '<?php echo site_url('welcome/tamano_universal') ?>',
                type: 'POST',
                dataType: 'html',
                data:{rango:id}
               })
                .done(function(data) {
                    console.log(data);
                    $('#lista_tamano').empty();
                    $('#lista_tamano').append(data);
              })
                .fail(function(data) {
                    console.log(data);
                    $('#lista_tamano').empty();
                    $('#lista_tamano').append(data.responseText);
              })
                .always(function() {
                    //console.log("complete");
              });
            
            }
            
            function MuestraProdUniversal(id){
            
                
               var material= $("input[name=material]:checked").val();                 
               var rango = $("#rango").val();
              //  alert(material);
                
               $.ajax({
                url: '<?php echo site_url('welcome/product_universal') ?>',
                type: 'POST',
                dataType: 'html',
                data:{tamano:id,material:material,rango:rango}
               })
                .done(function(data) {
                    console.log(data);
                    $('#filtros_de_productos').empty();
                    $('#filtros_de_productos').append(data);
              })
                .fail(function(data) {
                    console.log(data);
                    $('#filtros_de_productos').empty();
                    $('#filtros_de_productos').append(data.responseText);
              })
                .always(function() {
                    //console.log("complete");
              });
            
            }
            
            function MuestraProdMaterialUni(id)
            {
                  var rango = $("#rango").val();
                  var tamano = $("#tamanio").val();
                  $.ajax({
                url: '<?php echo site_url('welcome/product_universal') ?>',
                type: 'POST',
                dataType: 'html',
                data:{tamano:tamano,material:id,rango:rango}
          }).done(function(data) {
                console.log(data);
                $('#filtros_de_productos').empty();
                $('#filtros_de_productos').append(data);
                
          }).fail(function(data) {
                console.log(data);
                $('#filtros_de_productos').empty();
                $('#filtros_de_productos').append(data.responseText);
         }).always(function() {
                 //console.log("complete");
         });
            }
            
            /*ABRAZADERAS BELL JOINT*/
            
               function MuestraProdconDiamJoint(id)
            {
           //   alert(id);
          $.ajax({
                    url: '<?php echo site_url('welcome/products_joint') ?>',
                    type: 'POST',
                    dataType: 'html',
                    data:{diametro:id}
           })
                .done(function(data) {
                    console.log(data);
            $('#filtros_de_productos').empty();
            $('#filtros_de_productos').append(data);           
        })
                .fail(function(data) {
                    console.log(data);
            $('#filtros_de_productos').empty();
            $('#filtros_de_productos').append(data.responseText);   
            console.log("error");
        })
                .always(function() {
                    
                });

            }
            
            /*CPLE DAYTON*/
            
            function MuestraProdCople(id)
            {
                $.ajax({
                    url: '<?php echo site_url('welcome/products_cople') ?>',
                    type: 'POST',
                    dataType: 'html',
                    data:{rango:id}
           })
                .done(function(data) {
                    console.log(data);
            $('#filtros_de_productos').empty();
            $('#filtros_de_productos').append(data);           
        })
                .fail(function(data) {
                    console.log(data);
            $('#filtros_de_productos').empty();
            $('#filtros_de_productos').append(data.responseText);   
            console.log("error");
        })
                .always(function() {
                    
                });

                
            }
            
            /*CONTRA INCENDIO*/
            
            function MuestraProdHidranteSeco(id)
            {
     var t  = $("#valvula").val();
     var v = $("#vias").val();
     var b = $("#barril").val();
     var j = $("#junta").val(); 
                 
                $.ajax({
                    url: '<?php echo site_url('welcome/products_hidrante_seco') ?>',
                    type: 'POST',
                    dataType: 'html',
                    data:{valvula:t,vias:v,barril:b,junta:j}
           })
                .done(function(data) {
                    console.log(data);
            $('#filtros_de_productos').empty();
            $('#filtros_de_productos').append(data);           
        })
                .fail(function(data) {
                    console.log(data);
            $('#filtros_de_productos').empty();
            $('#filtros_de_productos').append(data.responseText);   
            console.log("error");
        })
                .always(function() {
                    
                });
                
            }
            function MuestraProdHidrante(id){
            
            var t  = $("#valvula").val();
            var b = $("#barril").val();
            
        $.ajax({
            url: '<?php echo site_url('welcome/products_extension_hidrante') ?>',
            type: 'POST',
            dataType: 'html',
            data:{valvula:t,barril:b}
    })
            .done(function(data) {
                console.log(data);
            $('#filtros_de_productos').empty();
            $('#filtros_de_productos').append(data);
        })
                .fail(function(data) {
                    console.log(data);
            $('#filtros_de_productos').empty();
            $('#filtros_de_productos').append(data.responseText);   
            console.log("error");
        })
                .always(function() {
                    
                });
            
            }
            
            function MuestraProdKit(id)
            {          
             var t  = $("#valvula").val();
        
        $.ajax({
            url: '<?php echo site_url('welcome/products_kit') ?>',
            type: 'POST',
            dataType: 'html',
            data:{valvula:t}
       })
            .done(function(data) {
                console.log(data);
            $('#filtros_de_productos').empty();
            $('#filtros_de_productos').append(data);
        })
                .fail(function(data) {
                    console.log(data);
            $('#filtros_de_productos').empty();
            $('#filtros_de_productos').append(data.responseText);   
            console.log("error");
        })
               .always(function() {
                    
        });

       }
       
       /***VALVULAS***/
       
       function MuestraProdNombreVal(id){
       
              var d  = $("#diam").val();
              var j  = $("#tipo_junta").val();
                
               $.ajax({
            url: '<?php echo site_url('welcome/products_valvula') ?>',
            type: 'POST',
            dataType: 'html',
            data:{valvula:id,diametro:d,junta:j}
       })
            .done(function(data) {
                console.log(data);
            $('#filtros_de_productos').empty();
            $('#filtros_de_productos').append(data);
        })
                .fail(function(data) {
                    console.log(data);
            $('#filtros_de_productos').empty();
            $('#filtros_de_productos').append(data.responseText);   
            console.log("error");
        })
               .always(function() {
                    
        });
       
       }
       
       function MuestraProdDiamVal(id){
              var v  = $("#nombr_valvula").val();
              var j  = $("#tipo_junta").val();
                
               $.ajax({
            url: '<?php echo site_url('welcome/products_valvula') ?>',
            type: 'POST',
            dataType: 'html',
            data:{valvula:v,diametro:id,junta:j}
       })
            .done(function(data) {
                console.log(data);
            $('#filtros_de_productos').empty();
            $('#filtros_de_productos').append(data);
        })
                .fail(function(data) {
                    console.log(data);
            $('#filtros_de_productos').empty();
            $('#filtros_de_productos').append(data.responseText);   
            console.log("error");
        })
               .always(function() {
                    
        });
       
       }
       
            function MuestraProdValvula(id)
            {
                var v  = $("#nombr_valvula").val();
                var d  = $("#diam").val();
                
               $.ajax({
            url: '<?php echo site_url('welcome/products_valvula') ?>',
            type: 'POST',
            dataType: 'html',
            data:{valvula:v,diametro:d,junta:id}
       })
            .done(function(data) {
                console.log(data);
            $('#filtros_de_productos').empty();
            $('#filtros_de_productos').append(data);
        })
                .fail(function(data) {
                    console.log(data);
            $('#filtros_de_productos').empty();
            $('#filtros_de_productos').append(data.responseText);   
            console.log("error");
        })
               .always(function() {
                    
        });

        }
        
        /*TEE PARTIDA*/
        function MuestraRangoPartida(id){
        
                var m  = $("#modelo").val();
                var b  = $("#brida").val();
                
               $.ajax({
            url: '<?php echo site_url('welcome/lista_partida_rango') ?>',
            type: 'POST',
            dataType: 'html',
            data:{modelo:m,diametro:id,brida:b}
       })
            .done(function(data) {
                console.log(data);
                $('#lista_rango').empty();
                $('#lista_rango').append(data);
        })
                .fail(function(data) {
                    console.log(data);
                    $('#lista_rango').empty();
                    $('#lista_rango').append(data);
            console.log("error");
        })
               .always(function() {
                    
        });
        
        }
            function MuestraBridaPartida(id){
                
               var m  = $("#modelo").val();
               var d  = $("#diam_nom").val();
                
               $.ajax({
            url: '<?php echo site_url('welcome/lista_partida_brida') ?>',
            type: 'POST',
            dataType: 'html',
            data:{modelo:m,diametro:d,rango:id}
       })
            .done(function(data) {
                console.log(data);
                $('#lista_brida').empty();
                $('#lista_brida').append(data);
        })
                .fail(function(data) {
                    console.log(data);
                    $('#lista_brida').empty();
                    $('#lista_brida').append(data);
            console.log("error");
        })
               .always(function() {
                    
        });
            }
            
            function MuestraProdTPartida(id){
            
               var m  = $("#modelo").val();
               var d  = $("#diam_nom").val();
               var r = $("#rango").val();
               
            $.ajax({                
            url: '<?php echo site_url('welcome/producto_tee_partida') ?>',
            type: 'POST',
            dataType: 'html',
            data:{modelo:m,diametro:d,rango:r,brida:id}
            })
                 .done(function(data) {
                console.log(data);
                $('#filtros_de_productos').empty();
                $('#filtros_de_productos').append(data);
            })
                 .fail(function(data) {
                    console.log(data);
                    $('#filtros_de_productos').empty();
                    $('#filtros_de_productos').append(data);
                    console.log("error");
            })
                .always(function() {
                            
            });
            }
            
       /*COLLARINES*/
       
       function MuestraInfoCollarin(id)
       {
           if(id== '3411'){
               $("#hierro").css({'display':'block'});
               $("#acero").css({'display':'none'});
           }
           if(id== '3415'){
               $("#hierro").css({'display':'block'});
               $("#acero").css({'display':'none'});
           }
           if(id== '3417'){
               $("#hierro").css({'display':'block'});
               $("#acero").css({'display':'none'});
           }
           if(id== '3421'){
               $("#hierro").css({'display':'none'});
               $("#acero").css({'display':'block'});
           }
           if(id== ''){
               $("#hierro").css({'display':'none'});
               $("#acero").css({'display':'none'});
           }
       }
            
       function MuestraDiamCollarin(id){
            
               var m  = $("#modelo").val();               
               
            $.ajax({                
            url: '<?php echo site_url('welcome/lista_diametro_collarin') ?>',
            type: 'POST',
            dataType: 'html',
            data:{modelo:id,}
            })
                 .done(function(data) {
                console.log(data);
                $('#lista_diam').empty();
                $('#lista_diam').append(data);
            })
                 .fail(function(data) {
                    console.log(data);
                    $('#lista_diam').empty();
                    $('#lista_diam').append(data);
                    console.log("error");
            })
                .always(function() {
                            
            });
      }
      
      function MuestraRangoCollarin(id){
       
         var m  = $("#modelo").val();               
               
            $.ajax({                
            url: '<?php echo site_url('welcome/lista_rango_collarin') ?>',
            type: 'POST',
            dataType: 'html',
            data:{modelo:m,diametro:id}
            })
                 .done(function(data) {
                console.log(data);
                $('#lista_rango').empty();
                $('#lista_rango').append(data);
            })
                 .fail(function(data) {
                    console.log(data);
                    $('#lista_rango').empty();
                    $('#lista_rango').append(data);
                    console.log("error");
            })
                .always(function() {
                            
            });
      
      }
      
      
      function MuestraSalida(id){
      
              var m  = $("#modelo").val();
               var d  = $("#diam_nom").val();
                
               $.ajax({
            url: '<?php echo site_url('welcome/lista_collarin_salida') ?>',
            type: 'POST',
            dataType: 'html',
            data:{modelo:m,diametro:d,rango:id}
       })
            .done(function(data) {
                console.log(data);
                $('#lista_salida').empty();
                $('#lista_salida').append(data);
        })
                .fail(function(data) {
                    console.log(data);
                    $('#lista_salida').empty();
                    $('#lista_salida').append(data);
                    console.log("error");
        })
               .always(function() {
                    
        });

      }
      function MuestraRosca(id)
      {
            var m  = $("#modelo").val();
            var d  = $("#diam_nom").val();
            var r = $("#rango").val();
                
               $.ajax({
            url: '<?php echo site_url('welcome/lista_collarin_rosca') ?>',
            type: 'POST',
            dataType: 'html',
            data:{modelo:m,diametro:d,rango:r,salida:id}
       })
            .done(function(data) {
                console.log(data);
                $('#lista_rosca').empty();
                $('#lista_rosca').append(data);
        })
                .fail(function(data) {
                    console.log(data);
                    $('#lista_rosca').empty();
                    $('#lista_rosca').append(data);
                    console.log("error");
        })
               .always(function() {
                    
        });
      }
            
            function MuestraProdCollarin(id){
            
            var m = $("#modelo").val();
            var d = $("#diam_nom").val();
            var r = $("#rango").val();
            var s = $("#salida").val();
                
          $.ajax({              
            url: '<?php echo site_url('welcome/producto_collarin') ?>',
            type: 'POST',
            dataType: 'html',
            data:{modelo:m,diametro:d,rango:r,salida:s,rosca:id}
        })
            .done(function(data) {
                console.log(data);
                $('#filtros_de_productos').empty();
                $('#filtros_de_productos').append(data);
        })
            .fail(function(data) {
                    console.log(data);
                    $('#filtros_de_productos').empty();
                    $('#filtros_de_productos').append(data);
                    console.log("error");
        })
           .always(function() {
                    
       });

            }
            
            /*JUNTAS UNIVERSALES*/
            
            function MuestraDiamJU(id){
           
            $.ajax({                
            url: '<?php echo site_url('welcome/lista_diametro_ju') ?>',
            type: 'POST',
            dataType: 'html',
            data:{material:id,}
            })
                 .done(function(data) {
                console.log(data);
                $('#lista_diam').empty();
                $('#lista_diam').append(data);
            })
                 .fail(function(data) {
                    console.log(data);
                    $('#lista_diam').empty();
                    $('#lista_diam').append(data);
                    console.log("error");
            })
                .always(function() {
                            
            });
            
            }
            
            function MuestraRangoJU(id)
            {
             var m  = $("input[name=material]:checked").val();            
             
            $.ajax({                
            url: '<?php echo site_url('welcome/lista_rango_ju') ?>',
            type: 'POST',
            dataType: 'html',
            data:{material:m,diametro:id}
            })
                 .done(function(data) {
                console.log(data);
                $('#lista_rango').empty();
                $('#lista_rango').append(data);
            })
                 .fail(function(data) {
                    console.log(data);
                    $('#lista_rango').empty();
                    $('#lista_rango').append(data);
                    console.log("error");
            })
                .always(function() {
                            
            });
                
            }
            
            function MuestraBarril(id){
            
                var m  = $("input[name=material]:checked").val();            
                var d = $("#diam_nom").val();
              //var r = $("#rango").val();
            $.ajax({               
            url: '<?php echo site_url('welcome/lista_barril_ju') ?>',
            type: 'POST',
            dataType: 'html',
            data:{material:m,diametro:d,rango:id}
            })
                 .done(function(data) {
                console.log(data);
                $('#lista_barril').empty();
                $('#lista_barril').append(data);
            })
                 .fail(function(data) {
                    console.log(data);
                    $('#lista_barril').empty();
                    $('#lista_barril').append(data);
                    console.log("error");
            })
                .always(function() {
                            
            });            
            }
            
            function MuestraProductoJU(id){
            
           var m  = $("input[name=material]:checked").val();    
            var d = $("#diam_nom").val();
            var r = $("#rango").val();
            //var s = $("#salida").val();
                
          $.ajax({              
            url: '<?php echo site_url('welcome/producto_ju') ?>',
            type: 'POST',
            dataType: 'html',
            data:{modelo:m,diametro:d,rango:r,barril:id}
        })
            .done(function(data) {
                console.log(data);
                $('#filtros_de_productos').empty();
                $('#filtros_de_productos').append(data);
        })
            .fail(function(data) {
                    console.log(data);
                    $('#filtros_de_productos').empty();
                    $('#filtros_de_productos').append(data);
                    console.log("error");
        })
           .always(function() {
                    
       });
            
            }
            
            /*ADAPTADORES BRIDADOS*/
            
            function MuestraDiamAdapt(id){
           
            $.ajax({                
            url: '<?php echo site_url('welcome/lista_diam_adapt') ?>',
            type: 'POST',
            dataType: 'html',
            data:{modelo:id,}
            })
                 .done(function(data) {
                console.log(data);
                $('#lista_diam').empty();
                $('#lista_diam').append(data);
            })
                 .fail(function(data) {
                    console.log(data);
                    $('#lista_diam').empty();
                    $('#lista_diam').append(data);
                    console.log("error");
            })
                .always(function() {
                            
            });
            
            }
            
        function MuestraRangoAdapt(id){
            
            var m  = $("#modelo").val();            
             
            $.ajax({                
            url: '<?php echo site_url('welcome/lista_rango_adapt') ?>',
            type: 'POST',
            dataType: 'html',
            data:{modelo:m,diametro:id}
            })
                 .done(function(data) {
                console.log(data);
                $('#lista_rango').empty();
                $('#lista_rango').append(data);
            })
                 .fail(function(data) {
                    console.log(data);
                    $('#lista_rango').empty();
                    $('#lista_rango').append(data);
                    console.log("error");
            })
                .always(function() {
                            
            });
                
            
            }
            
            function MuestraProductAdapt(id){
            
            var m  = $("#modelo").val();        
            var d = $("#diam_nom").val();
            
        $.ajax({
            url: '<?php echo site_url('welcome/producto_adapt') ?>',
            type: 'POST',
            dataType: 'html',
            data:{modelo:m,diametro:d,rango:id}
        })
            .done(function(data) {
                console.log(data);
                $('#filtros_de_productos').empty();
                $('#filtros_de_productos').append(data);
        })
            .fail(function(data) {
                    console.log(data);
                    $('#filtros_de_productos').empty();
                    $('#filtros_de_productos').append(data);
                    console.log("error");
        })
           .always(function() {
                    
       });
            }
            
            /*BRIDAS RÁPIDAS*/
            function MuestraBridaBR(id){
            
            $.ajax({
                url: '<?php echo site_url('welcome/lista_brida_br') ?>',
                type: 'POST',
                dataType: 'html',
                data:{modelo:id}
            })
                    .done(function(data) {
                     console.log(data);
                     $('#lista_brida').empty();
                     $('#lista_brida').append(data);
             })
                     .fail(function(data) {
                         console.log(data);
                         $('#lista_brida').empty();
                         $('#lista_brida').append(data);
                         console.log("error");
             })
                    .always(function() {

             });
            
            }
            
            function MuestraRangoBR(id){
             
             var m  = $("#modelo").val();
            
             
            $.ajax({
            url: '<?php echo site_url('welcome/lista_rango_br') ?>',
            type: 'POST',
            dataType: 'html',
            data:{modelo:m,brida:id}
            })
                 .done(function(data) {
                console.log(data);
                $('#lista_rango').empty();
                $('#lista_rango').append(data);
            })
                 .fail(function(data) {
                    console.log(data);
                    $('#lista_rango').empty();
                    $('#lista_rango').append(data);
                    console.log("error");
            })
                .always(function() {
                            
            });
            
            }
            
            function MuestraProductBR(id)
            {
            var m  = $("#modelo").val();   
            var b = $("#brida").val();
            
            
        $.ajax({
            url: '<?php echo site_url('welcome/producto_br') ?>',
            type: 'POST',
            dataType: 'html',
            data:{modelo:m,brida:b,rango:id}
        })
            .done(function(data) {
                console.log(data);
                $('#filtros_de_productos').empty();
                $('#filtros_de_productos').append(data);
        })
            .fail(function(data) {
                    console.log(data);
                    $('#filtros_de_productos').empty();
                    $('#filtros_de_productos').append(data);
                    console.log("error");
        })
           .always(function() {
                    
       });
            
            
            }
            
            /*ALCANTARILLADO*/
            /*BROCALES*/
            function MuestraProductoAlcantarillado(id){
       
        $.ajax({
            url: '<?php echo site_url('welcome/product_alcantarillado') ?>',
            type: 'POST',
            dataType: 'html',
            data:{peso:id}
        })
            .done(function(data) {
                console.log(data);
                $('#filtros_de_productos').empty();
                $('#filtros_de_productos').append(data);
        })
            .fail(function(data) {
                    console.log(data);
                    $('#filtros_de_productos').empty();
                    $('#filtros_de_productos').append(data);
                    console.log("error");
        })
           .always(function() {
                    
       });
            
            }
            
            /*ESCALONES*/
            function MuestraProdEscalones(id){
             $.ajax({
            url: '<?php echo site_url('welcome/product_escalones') ?>',
            type: 'POST',
            dataType: 'html',
            data:{material:id}
        })
            .done(function(data) {
                console.log(data);
                $('#filtros_de_productos').empty();
                $('#filtros_de_productos').append(data);
        })
            .fail(function(data) {
                    console.log(data);
                    $('#filtros_de_productos').empty();
                    $('#filtros_de_productos').append(data);
                    console.log("error");
        })
           .always(function() {
                    
       });
            }
            /*ANTI RETORNO*/
            function MuestraProdAntiRetorno(id){
            
        $.ajax({
            url: '<?php echo site_url('welcome/product_antiretorno') ?>',
            type: 'POST',
            dataType: 'html',
            data:{diametro:id}
        })
            .done(function(data) {
                console.log(data);
                $('#filtros_de_productos').empty();
                $('#filtros_de_productos').append(data);
        })
            .fail(function(data) {
                    console.log(data);
                    $('#filtros_de_productos').empty();
                    $('#filtros_de_productos').append(data);
                    console.log("error");
        })
           .always(function() {
                    
       });
       
            }
            
            
            /*MANGA*/
            function MuestraProdManga(id){
           
       $.ajax({
            url: '<?php echo site_url('welcome/product_manga') ?>',
            type: 'POST',
            dataType: 'html',
            data:{tamano:id}
        })
            .done(function(data) {
                console.log(data);
                $('#filtros_de_productos').empty();
                $('#filtros_de_productos').append(data);
        })
            .fail(function(data) {
                    console.log(data);
                    $('#filtros_de_productos').empty();
                    $('#filtros_de_productos').append(data);
                    console.log("error");
        })
           .always(function() {
                    
       });
            
            }
            
            /*FILTROS FILTRO CANASTA*/
            function MuestraProdFiltro(id){
           
       $.ajax({
            url: '<?php echo site_url('welcome/product_filtro_canasta') ?>',
            type: 'POST',
            dataType: 'html',
            data:{tamano:id}
        })
            .done(function(data) {
                console.log(data);
                $('#filtros_de_productos').empty();
                $('#filtros_de_productos').append(data);
        })
            .fail(function(data) {
                    console.log(data);
                    $('#filtros_de_productos').empty();
                    $('#filtros_de_productos').append(data);
                    console.log("error");
        })
           .always(function() {
                    
       });
            
            }
            
            /*MEDIDOR RESIDENCIAL*/
       function MuestraTipoRoscaResidencial(id){
             
         $.ajax({
            url: '<?php echo site_url('welcome/lista_rosca_residencial') ?>',
            type: 'POST',
            dataType: 'html',
            data:{tamano:id}
            })
                 .done(function(data) {
                console.log(data);
                $('#lista_rosca').empty();
                $('#lista_rosca').append(data);
            })
                 .fail(function(data) {
                    console.log(data);
                    $('#lista_rosca').empty();
                    $('#lista_rosca').append(data);
                    console.log("error");
            })
                .always(function() {
                            
            });
            
            }
            
       function MuestraMaterialResidencial(id){
         
         //var r = $("#rosca").val();
         var t = $("#tamano").val();
         
        $.ajax({
            url: '<?php echo site_url('welcome/lista_material_residencial') ?>',
            type: 'POST',
            dataType: 'html',
            data:{tamano:t,rosca:id}
            })
                 .done(function(data) {
                console.log(data);
                $('#lista_material').empty();
                $('#lista_material').append(data);
            })
                 .fail(function(data) {
                    console.log(data);
                    $('#lista_material').empty();
                    $('#lista_material').append(data);
                    console.log("error");
            })
                .always(function() {
                            
            });
       
       }     
            
       function MuestraTipoMedidorResidencial(id)
       {
         var r = $("#rosca").val();
         var t = $("#tamano").val();
         
              $.ajax({
            url: '<?php echo site_url('welcome/lista_tipo_medidor_residencial') ?>',
            type: 'POST',
            dataType: 'html',
            data:{tamano:t,rosca:r,material:id}
            })
                 .done(function(data) {
                console.log(data);
                $('#lista_tipo_medidor').empty();
                $('#lista_tipo_medidor').append(data);
            })
                 .fail(function(data) {
                    console.log(data);
                    $('#lista_tipo_medidor').empty();
                    $('#lista_tipo_medidor').append(data);
                    console.log("error");
            })
                .always(function() {
                            
            });
       } 
       
       function MuestraProdResidencial(id){
         var r = $("#rosca").val();
         var t = $("#tamano").val();
         var m = $("#material").val();
         
              $.ajax({
            url: '<?php echo site_url('welcome/product_residencial') ?>',
            type: 'POST',
            dataType: 'html',
            data:{tamano:t,rosca:r,material:m,medidor:id}
            })
                 .done(function(data) {
                console.log(data);
                $('#filtros_de_productos').empty();
                $('#filtros_de_productos').append(data);
            })
                 .fail(function(data) {
                    console.log(data);
                    $('#filtros_de_productos').empty();
                    $('#filtros_de_productos').append(data);
                    console.log("error");
            })
                .always(function() {
                            
            });
       }
       /*MEDIDOR COMERCIAL*/
        function MuestraTipoRoscaComercial(id){
             
         $.ajax({
            url: '<?php echo site_url('welcome/lista_rosca_comercial') ?>',
            type: 'POST',
            dataType: 'html',
            data:{tamano:id}
            })
                 .done(function(data) {
                console.log(data);
                $('#lista_rosca').empty();
                $('#lista_rosca').append(data);
            })
                 .fail(function(data) {
                    console.log(data);
                    $('#lista_rosca').empty();
                    $('#lista_rosca').append(data);
                    console.log("error");
            })
                .always(function() {
                            
            });
            
            }
            
       function MuestraMaterialComercial(id){
         
         //var r = $("#rosca").val();
         var t = $("#tamano").val();
         
        $.ajax({
            url: '<?php echo site_url('welcome/lista_material_comercial') ?>',
            type: 'POST',
            dataType: 'html',
            data:{tamano:t,rosca:id}
            })
                 .done(function(data) {
                console.log(data);
                $('#lista_material').empty();
                $('#lista_material').append(data);
            })
                 .fail(function(data) {
                    console.log(data);
                    $('#lista_material').empty();
                    $('#lista_material').append(data);
                    console.log("error");
            })
                .always(function() {
                            
            });
       
       }     
            
       function MuestraTipoMedidorComercial(id)
       {
         var r = $("#rosca").val();
         var t = $("#tamano").val();
         
              $.ajax({
            url: '<?php echo site_url('welcome/lista_tipo_medidor_comercial') ?>',
            type: 'POST',
            dataType: 'html',
            data:{tamano:t,rosca:r,material:id}
            })
                 .done(function(data) {
                console.log(data);
                $('#lista_tipo_medidor').empty();
                $('#lista_tipo_medidor').append(data);
            })
                 .fail(function(data) {
                    console.log(data);
                    $('#lista_tipo_medidor').empty();
                    $('#lista_tipo_medidor').append(data);
                    console.log("error");
            })
                .always(function() {
                            
            });
       } 
       
       function MuestraProdComercial(id){
         var r = $("#rosca").val();
         var t = $("#tamano").val();
         var m = $("#material").val();
         
              $.ajax({
            url: '<?php echo site_url('welcome/product_comercial') ?>',
            type: 'POST',
            dataType: 'html',
            data:{tamano:t,rosca:r,material:m,medidor:id}
            })
                 .done(function(data) {
                console.log(data);
                $('#filtros_de_productos').empty();
                $('#filtros_de_productos').append(data);
            })
                 .fail(function(data) {
                    console.log(data);
                    $('#filtros_de_productos').empty();
                    $('#filtros_de_productos').append(data);
                    console.log("error");
            })
                .always(function() {
                            
            });
       }
       
       function ActualizaDatos(){
       
       
           var n = document.getElementById("nombre").value;
           var a = document.getElementById("apellidos").value;
           var e = document.getElementById("edo").value;
           var c = document.getElementById("cd").value;
           var email = document.getElementById("email_ef").value;
           var cp = document.getElementById("codigopostal").value;
           var d = document.getElementById("direccion_ef").value;
           var no = document.getElementById("nu_dir").value;
           var col = document.getElementById("col_dir").value;
           var lada = document.getElementById("lada").value;
           var tel = document.getElementById("tel").value;
           var dir2 =  $('input[name="billing"]:checked').val();
           var combo1 = document.getElementById("edo");
           var combo2 = document.getElementById("cd");
          
                 if(n == ""){     alert("Se requiere el nombre para actualizar su perfil.");}
           else if(a == ""){     alert("Se requiere los apellidos para actualizar su perfil.");}
           else if(d == ""){     alert("Se requiere dirección completa actualizar su perfil.");}
           else if(no == ""){    alert("Se requiere el numero de tu dirección para actualizar su perfil.");}
           else if(col == ""){   alert("Se requiere la colonia para actualizar su perfil.");}           
           else if(e == ""){     alert("Se requiere seleccionar el estado donde vive para actualizar su perfil.");}
           else if(c == ""){     alert("Se requiere selecionar el municipio donde vive para actualizar su perfil.");}
           else if(cp==""){      alert("Se requiere el código postal para actualizar su perfil.");}
           else if(lada == ""){  alert("Se requiere la lada para actualizar su perfil.");}
           else if(tel == ""){    alert("Se requiere el teléfono paraactualizar su perfil."); }
           else if(email == ""){ alert("Se requiere el email del cliente para actualizar su perfil.");}
           
           if(n != "" && a != "" && e != "" && c != "" && email != "" && cp != "" && lada != "" && d != ""  && no != "" && col !="" && tel != ""){
               var selected_edo = combo1.options[combo1.selectedIndex].text;
               var selected_cd = combo2.options[combo2.selectedIndex].text;
          
            if(dir2 == 2){
                
           var n2 = document.getElementById("nombre_dos").value;
           var a2 = document.getElementById("apellidos_dos").value;
           var e2 = document.getElementById("edo2").value;
           var c2 = document.getElementById("cd2").value;
           var email2 = document.getElementById("email_dos").value;
           var cp2 = document.getElementById("codigopostal_dos").value;
           var d2 = document.getElementById("dir_dos").value;
           var no2 = document.getElementById("no_dir_2").value;
           var col2 = document.getElementById("col_dir_2").value;
           var lada2 = document.getElementById("lada_2_1").value;
           var tel2 = document.getElementById("tel_dos").value;
           
                 if(n2 == ""){    alert("Se requiere el nombre de la persona para actualizar su perfil.");}
           else if(a2 == ""){    alert("Se requiere los apellidos de la persona para actualizar su perfil");}
           else if(d2 == ""){    alert("Se requiere dirección completa para actualizar su perfil.");}
           else if(no2 == ""){   alert("Se requiere el numero de tu dirección para actualizar su perfil");}
           else if(col2 == ""){  alert("Se requiere la colonia para actualizar su perfil.");}
           else if(e2 == ""){    alert("Se requiere seleccionar el estado donde vive para actualizar su perfil");}
           else if(c2 == ""){    alert("Se requiere selecionar el municipio donde vive para actualizar su perfil");}
           else if(cp2==""){    alert("Se requiere el codigo postal para actualizar su perfil");}
           else if(lada2 == ""){alert("Se requiere la lada para actualizar su perfil.");}
           else if(tel2 == ""){  alert("Se requiere el teléfono para actualizar su perfil.");}
           else if(email2 == ""){alert("Se requiere el email del cliente para actualizar su perfil.");}
                
                if(n2 != "" && a2 != "" && e2 != "" && c2 != "" && email2 != "" && cp2 != "" && lada2 != ""  && d2 != ""  && no2 != "" && col2 !="" && tel2 != ""){
                   
                   //ENVIA FORM
                     $("#perfil_form").submit();
               }
          
           }else{
               
               //ENVIA FORM
               $("#perfil_form").submit();
           }
       
       }
   }
            </script>
 <?php if ($this->uri->segment(1) == 'historial_compra_usuario'): ?>            
            <script type="text/javascript">
      $(document).ready(function() {
        $('#example1').DataTable({
          "ajax": '<?php echo base_url('welcome/pedidos_usuario') ?>',
          'paging'      : true,
          'lengthChange': false,
          'searching'   : true,
          'ordering'    : true,
          'info'        : true,
          'autoWidth'   : false
        })
      });
    </script>
    <?php endif; ?>
</body>
</html>
