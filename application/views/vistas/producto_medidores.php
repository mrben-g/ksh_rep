<?php
//$prod_id = $this->input->get('id', TRUE);

$prod_id = $this->uri->segment(2);
?>
 
<section class="page-section pt-20 pb-40">
    <div class="container relative">
        <!-- Intro Text -->
        <div class="row">
            <div class="col-md-12">
                <div class="section-text align-left">
                    <h3 class="tituloproductos1 mt-0 mb-0">Medidores</h3>
                    <div class="linea"></div>
                </div>
            </div>
        </div>
        <!-- End Intro Text -->
        <!--<div class="clearfix"></div>-->
        
<?php if($prod_id == 1){  ?>          
        <div class="row">
            <div class="col-md-3">
                <?php 
                $this->load->view('vistas/menu_productos');
                ?>
            </div>
            <div class="col-md-9" style="background: #fff;padding: 30px;">
                <div class="row">
                   <div class="col-md-4">
                       <img src="<?php echo base_url('public/theme/images/productos/medidores/medidores.jpg') ?>" alt=""  style="width: 100%;"/>
                   </div>
                    <div class="col-md-8" style="text-align:justify;">
                        <h6 class="titulo_producto">Residenciales</h6>
                        <fieldset></fieldset>
                        <div  class="section-text"  style="color:#000;">
                            Un medidor de agua es un artefacto que permite contabilizar la cantidad de agua que pasa a través de él y es utilizado en las instalaciones residenciales acueductos para realizar los cobros pertinentes a los usuarios del mismo.
                        </div>                       
                    </div>
                </div>
                <div class="row pt-40 pb-0">
                      <div class="col-md-4" style="text-align:justify;">                        
                           
                     </div>                      
                    <div class="col-md-8">
                         <h6 class="titulo_producto" style="font-size: 16px;">Clasificación de medidores</h6>
                         <div class="row">
                            <div class="section-text col-md-12 text-justify mt-20 mb-20">
                              <ul  id="ven"><li><span><b>Medidores de desplazamiento o volumétricos.</b></span></li></ul>                              
                              En estos medidores el gasto se determina subdividiendo la corriente total del fluido en fracciones de volumen conocido.<br>
                              La medición se efectúa mediante el recuento del número de fracciones en la unidad de tiempo. Los tipos de medidores por desplazamiento son: disco oscilante o nutativo y pistón oscilante.<br>
                              Los medidores de disco oscilante o nutativo funcionan de manera que tan pronto como el fluido penetra en el espacio entre el disco y las paredes de la cámara, empuja al disco hacia delante imprimiéndole un movimiento de oscilación rotatoria.<br>
                              El movimiento del disco y del eje es similar al de un trompo en el momento en que llega al final de su impulso de giro.<br>
                              El medidor de pistón oscilante va guiado por un eje, el cual sigue una trayectoria circular entre el anillo interno y un rodillo central. Una prolongación del eje que atraviesa la cubierta de la cámara comunica el volumen total del fluido que ha circulado por el medidor. El fluido penetra y pasa alrededor del espacio anular, entre los anillos externo e interno, hacia el orificio de descarga.                                       
                            </div>
                            <div class="section-text col-md-12  text-justify mb-20">
                            <ul  id="ven"><li><span><b>Medidores de turbina o velocidad.</b></span></li></ul>
                               Estos medidores emplean un procedimiento mecánico y que por acción de la velocidad del agua giran un mecanismo móvil, el cual puede ser una turbina o hélice. Pueden ser de dos tipos chorro único y chorro múltiple.<br>
                               Los de chorro único son accionados por medio de un chorro único de agua. Y los de chorro múltiple son accionados por varios chorros tangenciales de agua, se distinguen del chorro único, en que la turbina está dentro de la cámara con varios orificios de entrada y salida, diametralmente opuesta.<br>
                               Ambos tipos de medidores miden el volumen de agua por la velocidad de la turbina. Tanto en el sistema único como en el múltiple, la turbina está en contacto con el agua, puesto que es en la que recibe el impulso que trasmite el movimiento al mecanismo indicador. Además, están dotados de un colador, por las posibles materias que pudieran pasar a través de él.
                                       
                            </div>
                              <div class="section-text col-md-12  text-justify mb-20">
                                  <ul  id="ven"><li><span><b>Medidores electromagnéticos.</b></span></li></ul>
                                  Los medidores electromagnéticos consisten en un tubo metálico, el cual generalmente es de acero inoxidable o aluminio, ya que las propiedades magnéticas de estos materiales son bajas, recubierto con neopreno, plástico, teflón, cerámica o cualquier material no magnético y no conductor.<br>
                                  Alrededor del tubo se encuentran una serie de bobinas de diseño parecido al devanado de un motor, y con un núcleo semejante a los que se usan en un transformador, siendo las que producen el campo magnético. También cuenta con un par de electrodos que detectan la fuerza electromotriz que genera el agua a su paso por el campo magnético, enviando la señal para medición a un registrador que traduce la señal en información de caudales o volúmenes.<br>
                                  A medida que un líquido conductor pasa a través del campo magnético existente dentro de un medidor, se genera un voltaje, este voltaje es directamente proporcional a la velocidad promedio del flujo. Al ser el diámetro del tubo una variable conocida, el medidor magnético “calcula” el caudal que se desplaza por la tubería.
                                       
                            </div>
                        </div>
                    </div>

            </div>
                <div class="row pt-0 pb-40">
                      <div class="col-md-4" style="text-align:justify;">                        
                           
                     </div>                      
                    <div class="col-md-8">
                             <h6 class="titulo_producto" style="font-size: 16px;">Marca del Produto</h6>
                             <div class="row">
                            <div class=" col-md-10" style="">
                                <img src="<?php echo base_url('public/theme/images/productos/medidores/p/logo_dorot.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>                                
                            </div>
                           </div>
                     </div>      
                </div>
                <div class="row">
                    <div class="col-md-12" style="text-align:justify;">
                        <h4 class="mb-10">Cotizaciones o información de fichas técnicas</h4>
                         <fieldset></fieldset>
                        <form name="frm" id="frm" action="http://mrwasabiserver.com/webpage/ksh17/api-correo/index.php/api/correo_ksh" method="post">
                            <input type="hidden" name="tipo" id="tipo" value="Residenciales" />
                            <div class="row pb-20">
                            <div class="col-md-3"> 
                                Nombre*:<br/>
                                <input type="text" name="nombre" id="nombre" placeholder="Nombre" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3"> 
                                Empresa*:<br>
                                <input type="text" name="empresa" id="empresa"  placeholder="Empresa" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3">
                                Teléfono*:<br>
                                <input type="text" name="telefono" id="telefono"  placeholder="Teléfono" class="input-md round form-control" onkeypress="return isNumberKey(event)" maxlength="10"/> 
                            </div>
                            <div class="col-md-3">
                                E-mail*:<br>
                                <input type="text" name="correo" id="correo"  placeholder="E-mail" class="input-md round form-control"/>
                            </div>
                            </div>
                            <div class="row pb-20">
                            <div class="col-md-3">
                                Ciudad*:<br>
                                <input type="text" name="ciudad" id="ciudad"  placeholder="Ciudad a la que pertences" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-9">
                                Mensaje*:<br>
                                <textarea name="mensaje" id="mensaje" style="width: 100%;height:60px;" placeholder="Mensaje." class="input-md round form-control"></textarea>
                            </div>
                            </div>
                            <div class="row pb-20">
                             <div class="col-md-12 align-right">
                                     <a href="javascript:EnviaProdContacto()" style="padding:10px 25px;font-size: 15px;color:#fff;background:#000;text-decoration: none;">ENVIAR</a>
                             </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
<?php } if($prod_id == 2){  ?>  
   <div class="row">
            <div class="col-md-3">
                <?php 
                $this->load->view('vistas/menu_productos');
                ?>
            </div>
            <div class="col-md-9" style="background: #fff;padding: 30px;">
                <div class="row">
                   <div class="col-md-4 text-center">
                       <img src="<?php echo base_url('public/theme/images/productos/medidores/medidor.jpg') ?>" alt=""  style="width: 100%;max-width: 216px" class="mt-xs-20 mt-40" />
                   </div>
                    <div class="col-md-8" style="text-align:justify;">
                        <h6 class="titulo_producto">Comercial</h6>
                        <fieldset></fieldset>
                        <div  class="section-text"  style="color:#000;">
                          Los medidores comerciales, están diseñados para medir altos caudales con una mínima pérdida de carga, ofreciendo alta confiabilidad y exactitud de funcionamiento por un largo tiempo de uso. Están diseñados y producidos de acuerdo a los requisitos de la norma Internacional ISO 4064 Clase B. y cumplen con la norma Mexicana NOM-008-SCFI-2002.
                          <br><br>
                          Este tipo de medidores se clasifican igual que los residenciales pero de un diámetro mayor que va de 1” a diámetros mayores. Los más usados en la industria son los electromagnéticos ya que son más precisos a los demás tipos ya que no posee partes móviles en contacto con el agua y para su instalación requiere una pequeña longitud de tramo recto aguas arriba. Su error de medición ¼ - ½ % y pueden manejar líquidos con sólidos en suspensión
                        </div>                            
                    </div>
                </div>
                <div class="row pt-40 pb-40">
                      <div class="col-md-4">                      
                         
                     </div>                      
                    <div class="col-md-8">
                             <h6 class="titulo_producto" style="font-size: 16px;">Marca del Produto</h6>
                             <div class="row">
                            <div class=" col-md-12">                               
                                    <img src="<?php echo base_url('public/theme/images/productos/medidores/p/logo_dorot.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>                                     
                            </div>
                           </div>
                    </div>                    
            </div>
                <div class="row">
                    <div class="col-md-12" style="text-align:justify;">
                        <h4 class="mb-10">Cotizaciones o información de fichas técnicas</h4>
                         <fieldset></fieldset>
                        <form name="frm" id="frm" action="http://mrwasabiserver.com/webpage/ksh17/api-correo/index.php/api/correo_ksh" method="post">
                            <input type="hidden" name="tipo" id="tipo" value="Comercial" />
                            <div class="row pb-20">
                            <div class="col-md-3"> 
                                Nombre*:<br/>
                                <input type="text" name="nombre" id="nombre" placeholder="Nombre" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3"> 
                                Empresa*:<br>
                                <input type="text" name="empresa" id="empresa"  placeholder="Empresa" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3">
                                Teléfono*:<br>
                                <input type="text" name="telefono" id="telefono"  placeholder="Teléfono" class="input-md round form-control" onkeypress="return isNumberKey(event)" maxlength="10"/> 
                            </div>
                            <div class="col-md-3">
                                E-mail*:<br>
                                <input type="text" name="correo" id="correo"  placeholder="E-mail" class="input-md round form-control"/>
                            </div>
                            </div>
                            <div class="row pb-20">
                            <div class="col-md-3">
                                Ciudad*:<br>
                                <input type="text" name="ciudad" id="ciudad"  placeholder="Ciudad a la que pertences" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-9">
                                Mensaje*:<br>
                                <textarea name="mensaje" id="mensaje" style="width: 100%;height:60px;" placeholder="Mensaje." class="input-md round form-control"></textarea>
                            </div>
                            </div>
                            <div class="row pb-20">
                             <div class="col-md-12 align-right">
                                <a href="javascript:EnviaProdContacto()" style="padding:10px 25px;font-size: 15px;color:#fff;background:#000;text-decoration: none;">ENVIAR</a>
                             </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>        

<?php } ?>        
    </div>
</section>