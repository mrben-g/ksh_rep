<section class="page-section pt-20 pb-40">
    <div class="container">
        <div class="row">
            <div class="col-md-12 mb-40">
                <div class="section-text align-left">
                    <h3 class="tituloproductos1 mt-0 mb-0" style="">Términos y Condiciones</h3>
                    <div class="linea"></div>
                </div>
            </div>
        </div>
        <!-- End Intro Text -->
        <div class="clearfix"></div>
                <div class="row" style="background: #fff;padding: 30px 10px;">
              <div class="col-md-12 text-justify" style="padding-top:1em;"> 
<label style="font-size:14px;">Kuroda Soluciones Hidráulicas es una marca de la empresa KS Tubería S.A. de C.V., con RFC KST890224TU9 y dirección fiscal en la Calle Dr. R. Michel #2142
Col. Atlas, Guadalajara, Jalisco, México, Código Postal 44767, en lo consiguiente referida como "KSH".
</label><br/><br/>
<label style="font-size:14px;">
    KSH presenta a sus "Usuarios", entendiendo a éstos como toda persona que realiza el acceso a contenido digital mediante equipo de cómputo y/o cualquier dispositivo de comunicación, los siguientes <b>Términos y Condiciones de Uso y Aviso de Privacidad</b>, a los cuáles los usuarios están sujetos al entrar al sitio web www.ksh.mx y cualquiera de sus secciones, E-commerce, cuentas de redes sociales asociadas y contenido digital emitido por la empresa en general, referidos en lo adelante como "sitio web". 
</label><br/>
<label style="font-size:14px;">
    <b>Al acceder al sitio web el usuario acepta de manera plena los presentes Términos y Condiciones así como conoce el Aviso de Privacidad y las Políticas <a href="http://localhost:8080/ksh17/ksh/avisos">http://www.ksh.mx/avisos</a> , <a href="http://localhost:8080/ksh17/ksh/terminos">http://ksh.mx/terminos</a></b>
</label><br/><br/>
<label style="font-size:14px;">El ingreso a al sitio web es de manera gratuita y voluntaria por parte de los usuarios, por lo que se da por hecho su aprobación a su contenido y siendo los usuarios responsables de su interpretación y cualquier consecuencia derivada del contenido expuesto por la empresa. En caso de no estar de acuerdo con los términos y condiciones favor de abstenerse a ingresar al sitio web, que puede hacerlo siguiendo esta https://www.google.com.mx</label><br/><br/>

<label style="font-size:14px;">
    <h4>I. DEL PROPÓSITO, USO Y RESTICCIONES </h4><br>
KSH pone a disposición de los usuarios a través de su sitio web un contenido de información de su catálogo de productos, servicios y herramientas de comercio electrónico con un propósito comercial. Toda información sobre características de productos, criterios de selección de productos, forma de instalación y uso, disponibilidad de garantías, certificaciones y demás contenido proporcionado por KSH <b>deben ser considerados por el Usuario como recomendaciones y consejos, sujetos a cambios por parte del proveedor y/o fabricante y a desactualización de la información</b>. A su vez, el usuario debe conocer la normativa vigente en su localidad, los productos autorizados y el ambiente físico y condición de las instalaciones, sistemas constructivos, ingeniería de proyecto y seguridad para hacer una selección y compra adecuada, quedando ésta bajo su total responsabilidad.</label><br/><br/>
<label style="font-size:14px;">
    KSH valida y actualiza constantemente su información para brindar recomendaciones de compra al Usuario, pero <b>bajo ninguna circunstancia dicha información complementa, sustituye o invalida al material oficial proporcionado por el proveedor y/o fabricante original del producto, como pueden ser instructivos de instalación y uso, mantenimiento, garantías, certificaciones, etc. A su vez, tampoco sustituye cualquier legislación, normativa o autorización por parte de las autoridades locales y federales, como es CONAGUA o los Organismos Operadores de Agua, o de las indicaciones dentro de la ingeniería del proyecto, sistemas constructivos o de seguridad que el mismo proyecto demande.</b>
</label><br/><br/>
<label style="font-size:14px;">
    KSH se reserva el derecho de modificar en cualquier momento y bajo cualquier circunstancia el contenido de su sitio web sin dar previo aviso y se desliga de cualquier consecuencia que provoque a sus usuarios dichos cambios, pausa, inactividad parcial o total por tiempos indefinidos. El catálogo de productos, los precios, disponibilidad y tiempos de entrega están sujetos a cambios constantes durante el día sin previo aviso.
</label><br/><br/>
<label style="font-size:14px;">
    KSH posee la capacidad de restringir, condicionar y/o negar la información parcial o total de los contenidos o servicios que presenta el sitio web al usuario sin aviso previo y sin obligación alguna de justificar sus restricciones. En caso de que los operadores de KSH consideren que el manejo de la cuenta de algún usuario es indebido se tendrá pleno derecho de reservar su admisión a la comunidad de usuarios de los sitios web o de suspender de manera parcial o total su cuenta en cualquier momento. KSH se reserva en su sección de E-commerce del sitio web a autorizar el ingreso a usuarios específicamente seleccionados por la empresa, pudiendo en cualquier momento no autorizar o revocar una autorización otorgada a los usuarios.
</label><br/><br/>
<label style="font-size:14px;">
    La información publicada por KSH es constantemente monitoreada, pero podría sufrir errores tipográficos, ortográficos o gráficos, ante lo cual KSH no se responsabiliza de un mal entendimiento por parte del usuario o de una falla humana o de sistema en su sitio web. Si se llegase a presentar algún error en la captura de información sensible, como son los precios de artículos o cualquier cargo por servicio o disponibilidad de inventario, el usuario debe estar consiente de que al momento de emitir la factura su monto a pagar puede variar así como pueden variar los tiempos de entrega estipulados.<b>En caso de un error en precios, disponibilidad o tiempos de entrega por parte de KSH, un representante de KSH se contactará a la brevedad posible con el Usuario.</b>  En caso de haber incurrido en un pago se realizará devoluciones total o parciales,  según el caso conforme a las <b>POLÍTICAS DE CAMBIO, DEVOLUCIÓN Y GARANTÍA</b>, a la brevedad que le sea posible, sin incurrir KSH en ninguna circunstancia en una devolución mayor al pago inicial recibido, como puede ser a cuenta de interés, daños, perjuicios o cualquier otro rubro.
</label><br/><br/>
<label style="font-size:14px;">
    El usuario debe saber y entender que no todos los servicios presentados por KSH y su sitio web están disponibles en todo lugar y/o momento. Algunos servicios y contenidos solo podrán ser vistos o utilizados posteriormente a su registro, activación, contratación previa y/o a criterio de KSH así <b>como estar limitados y/o poder variar conforme a una cierta área geográfica o por tipología de cliente</b>.
</label><br/><br/>
<label style="font-size:14px;">
    No se garantiza la disponibilidad constante y/o permanente del sitio web, contenidos y servicios; así como la continuidad de sus operaciones; KSH no será responsable por daño o pérdida alguna de cualquier naturaleza que pueda ser causada debido a la falta de disponibilidad o continuidad de operación del sitio web, servicios y contenidos.
</label><br/><br/>
<label style="font-size:14px;">
<h4>II. DE LA RESPONSABILIDAD DE LOS  USUARIOS</h4><br>
Todos los Usuarios conocen y entienden la naturaleza de la información y contenido del sitio web, así como su debido criterio y juicio sobre la información, conforme el inciso anterior <b>I. DEL PROPÓSITO, USO Y RESTICCIONES</b>    
</label><br/><br/>
<label style="font-size:14px;">
    El aprovechamiento y uso que haga el usuario del sitio web y su contenido será bajo entera responsabilidad del usuario. Si KSH llegara a encontrar anomalías o considerar que el uso de su sitio web es mal empleado por algún usuario, se reserva el derecho de restringir o revocar el acceso de dicho usuario.
</label><br/><br/>
<label style="font-size:14px;">
    La sección de E-commerce del sitio web requiere usuarios registrados, otorgando la información veraz para la obtención de dicho registro que será comprobada por representantes de KSH. Se autorizará como usuario registrado para E-commerce a las empresas que tengan experiencia demostrable y verificable trabajando dentro el ramo del agua y drenaje y que puedan entablar una relación comercial positiva con KSH, como es el caso de subdistribuidores, Organismos Operadores de Agua, constructoras/urbanizadoras, proyectistas e instaladores. Tras el llenado del formato de registro se contará con un periodo aproximado a <b>tres días</b> hábiles para la obtención de una respuesta. En caso favorable, se recibirá una notificación del usuario y contraseña. En caso negativo, se podrán continuar utilizando los servicios del sitio web como un usuario no registrado y realizar cotizaciones con representantes de venta sin acceso directo al E-commerce.
</label><br/><br/>
<label style="font-size:14px;">
    Son motivos para no obtener una autorización de usuario registrado en E-commerce:<br>
    <ul>
        <li>	No ser una empresa con imagen y experiencia demostrable en el ramo.</li>
<li>	Personas físicas con correo electrónico personal.</li>
<li>	Contar con un mal historial crediticio o de relaciones con KSH o cualquiera de sus filiales.</li>
<li>	Situarse en una región geográfica donde ya se cuenten con subdistribuidores exclusivos de KSH.</li>
<li>	Ser una empresa de competencia directa en el mismo punto de la cadena de distribución que KSH o con sospecha de comprometer la información sensible brindada en E-commerce.</li>
<li>	Cualquier punto que KSH no considere adecuado a una relación positiva y de largo plazo comercial.</li>
</ul>
</label><br/><br/>
<label style="font-size:14px;">
    A su vez, KSH se reserva el derecho a revocar los permisos de un usuario registrado, quedando estrictamente prohibido para los usuarios realizar acciones como:<br>
 <ul>
 <li>	Divulgación a terceros de información sensible brindada a usuarios como es el catálogo de productos, precios, disponibilidades y tiempos de entrega.</li> 
 <li>	Uso y/o divulgación de las marcas, elementos distintivos, fotografías y multimedia del sitio web sin autorización escrita previa. Para mayor información ver el siguiente inciso de PROPIEDAD INTELECTUAL.</li>
 <li>	Solicitudes de cotización constantes con motivo de obtención de previos sin incurrir en una compra, a criterio de KSH.</li>
 <li>	SPAM o uso indebido de formas de contacto o cualquier campo que requiera información del usuario.</li>
 <li>	Constantes errores o mal uso de las herramientas del sitio web.</li>
 <li>	Mal historial de pago, crediticio o de relaciones en las operaciones con KSH.</li>
</ul>
</label><br/><br/>
<label style="font-size:14px;">
    <h4>III. PROPIEDAD INTELECTUAL</h4><br>
KSH a través de su sitio web presta servicios y contenidos que buscan brindar una mejor atención e información al usuario. Dicho contenido, incluyendo textos, fotografías, folletos, fichas técnicas y/o videos que son propiedad de KSH, de sus filiales o de los proveedores y fabricantes originales que autorizaron la divulgación de sus contenidos a KSH. Por consiguiente bajo ninguna circunstancia el usuario tendrá permitido alterar, agraviar, tomar, copiar, editar, divulgar o hacer un uso de cualquier contenido sin autorización escrita previa por parte de KSH.

</label><br/><br/>
<label style="font-size:14px;">
    Los derechos de propiedad intelectual de los contenidos y servicios, elementos distintivos, marcas y dominios presentes en el sitio web son exclusivos de KSH y/o propietarios que autorizaron a KSH su uso, siendo los únicos con el poder de hacer uso de ellos, de su divulgación, publicación, reproducción, distribución y transformación. Bajo ningún momento el usuario tendrá derecho sobre este contenido.
</label><br/><br/>
<label style="font-size:14px;">
    El material presente en el sitio web no esta disponible para uso personal o comercial de los usuarios, ni en su estado original ni con modificación, debiendo el Usuario abstenerse de cualquier extracción y uso de contenido. KSH y/o los propietarios de los elementos distintivos podrán tomar medidas legales sobre quien haga uso indebido de dicho contenido, que pueden ir desde una solicitud de retiro de la información hasta penalizaciones monetarias.
</label><br/><br/>
<label style="font-size:14px;">
    El usuario comprende que el contenido de terceros presentado en el sitio web o ligas vinculadas de servicios o proveedores alternos a KSH dentro del sitio web se encuentran bajo el cobijo del presente documento y de sus anexos. Complementando siempre sus Términos y Condiciones propias, y no sustituyéndolos o invalidándolos.
</label><br/><br/>
<label style="font-size:14px;">
<h4>IV. COOKIES</h4><br>
Las cookies son archivos de texto que son descargados automáticamente y almacenados en el disco duro del equipo de cómputo del usuario al navegar en una página de internet específica, que permiten recordar al servidor de Internet algunos datos sobre los usuarios que acceden al portal, rastrear determinados comportamientos o actividades, preferencia y tráfico. El usuario autoriza a KSH el uso de cookies dentro del sitio web. KSH no usará la información recabada con otro propósito mas que el de monitorear y dar seguimiento a las preferencias del usuario, en busca de dar un mejor servicio y atención, por lo que KSH se compromete a no transmitir ni traspasar los datos recabados a terceros.

</label><br/><br/>
<label style="font-size:14px;">
    Las cookies pueden deshabilitarse en cualquier momento o ser rechazadas si su navegador le da la opción al ingresar el sitio web.
</label><br/><br/>
<label style="font-size:14px;">
<h4>V. SOBRE EL AVISO DE PRIVACIDAD</h4><br>
Todo usuario, como parte de estos Términos y Condiciones, avala haber leído, entendido y estar de acuerdo con el Aviso De Privacidad. http://www.ksh.mx/avisos.php

</label><br/><br/>
<label style="font-size:14px;">
<h4>VI. SEGURIDAD DE USUARIOS</h4><br>
En los usuarios registrados recae la responsabilidad del cuidado de sus cuentas, es decir, información como nombre de usuario y contraseña deberá de ser salvaguardada por el propio usuario y mantener su divulgación a criterio propio, ya que KSH no se hará responsable de suplantación, robo de identidad, etc. 
</label><br/><br/>
<label style="font-size:14px;">
    El Usuario es el responsable total de todas las transacciones y modificaciones realizadas a través de su nombre de usuario, incluyendo la comunicación de órdenes de compra para su procesamiento en KSH, datos clave como lugares de envío o datos de facturación. KSH tomará como buena y válida cualquier comunicación realizada a través de un usuario registrado.
</label><br/><br/>
<label style="font-size:14px;">
    En caso de detectar anomalías en su cuenta, detectar un mal uso por parte de terceros no autorizados o considerar que la seguridad de la cuenta está comprometida, el Usuario debe <b>contactar</b> http://www.ksh.mx/contacto al personal de KSH inmediatamente. KSH realizará preguntas para validar la autenticidad de quien contacta y tomar las acciones necesarias como es la generación de un nuevo usuario y/o contraseña.
</label><br/><br/>
<label style="font-size:14px;">
<h4>VII. MODIFICACIONES</h4><br>
KSH tendrá el derecho de realizar modificaciones parciales o totales, así como eliminación de el contenido del sitio web sin previo aviso.
</label><br/><br/>
<label style="font-size:14px;">
    Se podrá denunciar o eliminar comentarios de carácter ofensivo, irrelevante o en calidad de spam, basados en el criterio de sus operadores. Las cuentas de los usuarios podrán ser modificadas, restringidas o eliminadas por KSH sin aviso previo y en base a diversos motivos a su consideración, de los cuales se posee el derecho de reservar.
</label><br/><br/>
<label style="font-size:14px;">
Habrá modificaciones que podrán alterar condiciones particulares que sustituyen, completan y/o modifican los Términos y Condiciones, Aviso de Privacidad y Políticas, por lo cual KSH no tendrá responsabilidad alguna en el caso de que el usuario desconozca estos cambios.
</label><br/><br/>
<label style="font-size:14px;">
Los productos, precios, tiempos de entrega, disponibilidad y métodos de pago están sujetos a cambios sin previo aviso, para mayor información http://www.ksh.mx/avisos, http://ksh.mx/terminos
</label><br/><br/>
<label style="font-size:14px;">
    
</label><br/><br/>
<label style="font-size:14px;">
<h4>VIII. CONDICIONES DE PRECIO, DISPONIBILIDAD Y MÉTODOS DE PAGO</h4><br>
Todo usuario, como parte de estos Términos y Condiciones, acepta haber leído, entendido y estar de acuerdo con las http://www.ksh.mx/avisos, http://ksh.mx/terminos  vigentes.


</label><br/><br/>
<label style="font-size:14px;">
    <h4>IX. CONDICIONES DE ENTREGA Y ENVÍO DE MERCANCÍA</h4><br>
Todo usuario, como parte de estos Términos y Condiciones, acepta haber leído, entendido y estar de acuerdo con las http://www.ksh.mx/avisos, http://ksh.mx/terminos vigentes.

</label><br/><br/>
<label style="font-size:14px;">
    <h4>X. CONDICIONES DE CAMBIOS, DEVOLUCIONES Y GARANTIAS DE PRODUCTOS</h4><br>
Todo usuario, como parte de estos Términos y Condiciones, acepta haber leído, entendido y estar de acuerdo con las http://www.ksh.mx/avisos, http://ksh.mx/terminos vigentes.


</label><br/><br/>
<label style="font-size:14px;">
    <h4>XI. CONTACTO</h4><br>
    Para cualquier duda, aclaración o consulta se puede contactar con KSH vía telefónica a nuestras sucursales en:<br>
Monterrey, México: (81) 83 88 98 00<br>
O de manera electrónica al correo: contacto@ksh.mx
</label><br/><br/>
<br/><br/>

<h4 class="text-center">POLITICAS DE CAMBIOS, DEVOLUCIONES Y GARANTÍAS</h4><br>
<label style="font-size:14px;">  
Fecha de Última Actualización: 12/04/2017<br>
KSH, en su relación comercial positiva con los fabricantes y sus clientes, realiza cambios, devoluciones y aplicaciones de garantía de acuerdo a los parámetros expuestos en esta política. 
</label><br/>
<label style="font-size:14px;">
    Se define como cambio aquel intercambio en especie de un producto dañado o errado por otro en condiciones operativas adecuadas, entre KSH y el cliente.</label><br/>
<label style="font-size:14px;">
    Se define como devolución al regreso físico de un producto dañado o errado a KSH con su devolución subsecuente de una nota de crédito o dinero a la cuenta de origen del cliente.</label><br/>
<label style="font-size:14px;">
    Se define como garantía al acuerdo de protección sobre la calidad de un producto que ofrece el fabricante. KSH no ofrece garantías propias de la tienda pero apoya a los clientes a gestionar garantías con los fabricantes.</label><br/>
<label style="font-size:14px;">
    1. Se realizarán un cambio de producto en especie si:<br>
a)	Existe un defecto de fábrica comprobable habiendo seguido los lineamientos del fabricante en sus manuales de instalación y uso, de acuerdo a la garantía ofrecida. En tal caso el cliente debe enviar fotografías del daño o fallo a KSH. Si es posible, un representante de KSH o del fabricante acudirá a supervisar el problema en sitio. En caso de proceder, se debe regresar el producto dañado a KSH, corriendo el costo por cuenta del cliente. El envío del producto para  sustituir el dañado, no tendrá costo para el cliente si es enviado a la misma dirección de entrega original. KSH no se responsabiliza de costos por re-instalación y daños de otros equipos en productos con defecto de fábrica.</label><br/>
<label style="font-size:14px;">
b)	Se efectuaron daños ocurridos en su transportación <b>únicamente</b> en casos en los cuales KSH realizó este traslado. No aplicable a traslados del cliente y servicios de envío tercerizados. El cliente debe percatarse de este daño al momento de la entrega y señalárselo a los encargados de la entrega de KSH, sin recibir la mercancía. Si el producto fue recibido por el cliente no procede el cambio. Si se señala el daño al momento, el envío del producto en cambio no tendrá costo para el cliente a la misma dirección de entrega.
</label><br/>
<label style="font-size:14px;">
c)	Si el material recibido no corresponde al solicitado en la orden de compra o lo adquirido en el carrito de E-commerce. El cliente debe percatarse de este error al momento de la entrega y señalárselo a los encargados de la entrega de KSH, sin recibir la mercancía. En caso de recibirlo por servicio de entrega tercerizado, notificarlo inmediatamente a representantes de KSH, enviado fotografías. KSH procederá a verificar el error y en caso de proceder, el cliente debe mandar el producto equivocado de regreso sin haberlo maltratado o dañado. El envío del producto en cambio no tendrá costo para el cliente a la misma dirección de entrega.

</label><br/>
<label style="font-size:14px;">
    2. Se realizarán una devolución monetaria o nota de crédito en caso de:<br>    
    a) Cancelación de una orden de compra errada por parte del cliente siempre y cuando se realice de manera inmediata y previa a cualquier costo u operación que haya incurrido KSH por procesarla. No se aceptará si KSH ya realizó una orden de compra propia al fabricante.
</label><br/>
<label style="font-size:14px;">    
    b) En caso de una desinformación hacia el cliente en el sitio web debido a errores o desactualización del sitio, como puede ser de precio, disponibilidad y tiempo de entrega. En caso de detectar un error, un representante de KSH contactará al cliente y si ante los cambios de precio, disponibilidad o tiempo de entrega no está satisfecho, su dinero se regresará de manera íntegra.</label><br/>
    <label style="font-size:14px;">
    c) En caso de haber realizado un pago en transferencia o depósito de cheque mayor al monto de compra.</label><br/>
    <label style="font-size:14px;">
    d) En caso de haber realizado un pago en transferencia o depósito de cheque menor al monto de compra y haber KSH procedido a una cancelación de la misma.</label><br/>
    <label style="font-size:14px;">
    e) <b>En caso de un incumplimiento en el tiempo de entrega estipulado en la cotización por un lapso mayor al 50%</b>
</label><br/><br/>

<label style="font-size:14px;">
    3. KSH <b>no realizará cambios ni devoluciones</b> en los siguientes casos:<br>
    a)	No se cuenta con un comprobante de la venta del producto por parte de KSH que garantice que ese artículo en específico fue adquirido en la empresa.</label><br/>
    <label style="font-size:14px;">
    b)	Exista evidencia clara (ejemplo: código de registro) que el artículo presentado no fue comprado en KSH.</label><br/>
    <label style="font-size:14px;">
    c)	Si hubo un error por parte del cliente en los datos de especificación para recibir una recomendación de compra, un error del cliente en la selección del artículo en E-commerce y/o un error en el levantamiento de orden de compra.</label><br/>
    <label style="font-size:14px;">
    d)	Por robo o extravío una vez en posesión del transportista o del cliente.</label><br/>
    <label style="font-size:14px;">
    e)	Si el producto fue maltratado, reparado o alterado; Esto incluye daños por una manera inadecuada de uso conforme lo especificado en el manual de uso o instalación del fabricante; Daños ocasionados por la instalación, conexión o configuración inadecuada; Presión inadecuada de una línea de agua; Si se usaron partes no manufacturadas y/o autorizadas por el fabricante en la instalación, operación o mantenimiento; Daños durante la descarga de la mercancía ya en posición del cliente o del servicio de envío tercerizado.</label><br/>
    <label style="font-size:14px;">
     f)	En productos altamente especializados de fabricación individual o personalizados para el cliente.</label><br/><br/>


<label style="font-size:14px;">
    4. Los tiempos de entrega de un producto en cambio dependerán de la disponibilidad del producto en KSH y/o de la disponibilidad del fabricante, así como de los tiempos de envío. Estos serán dados a conocer en cada caso particular al cliente. KSH no se compromete a tener una disponibilidad inmediata ni se responsabiliza sobre ningún daño, costo de proyecto o interés por motivo de estas demoras.</label><br/><br/>
<label style="font-size:14px;">
    5. Los tiempos de devolución monetaria por un producto o generación de nota de crédito dependerán de la disponibilidad de recursos de la empresa KSH pero no deberán superar un lapso mayor a <b>1 mes</b> a partir de la resolución positiva a favor del cliente sobre la devolución. KSH no se compromete a tener una disponibilidad inmediata ni se responsabiliza sobre ningún, daño, costo de proyecto o interés por motivo de estas demoras.</label><br/><br/>
<label style="font-size:14px;">
6. Los términos y lapsos de garantía de cada producto será especificada por el fabricante. KSH no otorga garantías de tienda. Para hacerse válida la garantía se deberán seguir las instrucciones expresadas en cada una de ellas y contactar al numero telefónico y con el departamento o persona que estas indiquen. También se podrá contactar a KSH para asesoría o apoyo, sin que esto lo responsabilice en ningún momento. La validez de la garantía dependerá enteramente de la resolución del fabricante.</label><br/><br/>
<label style="font-size:14px;">
7. El cliente acepta que podrán existir casos particulares en los que KSH no podrá aceptar o realizar cambios, devoluciones o aplicación de garantías.</label><br/><br/>
<label style="font-size:14px;">
8. En caso de cualquier duda o gestionar cualquier acción de cambio, devolución o garantía, favor de mandar un correo electrónico a contacto@ksh.mx. Revisar haber recibido una confirmación de la solicitud y una respuesta. Los tiempos de respuesta empiezan a contar sólo tras una confirmación de la solicitud.<br>
</label><br/><br/>

<h4 class="text-center">POLITICAS DE PRECIO, DISPONIBILIDAD DE PRODUCTOS Y MÉTODOS DE PAGO</h4><br>
<label style="font-size:14px;">Fecha de Última Actualización: 12/04/2017<br></label><br/><br/>
<h4>A. PRECIO</h4><br>
<label style="font-size:14px;">
    1. Kuroda Soluciones Hidráulicas (KSH) en su sitio web y servicios de E-commerce presentará los precios de sus productos en dólares estadounidenses como moneda base. Al elegir la opción de visualizar los precios en pesos mexicanos, los precios mostrados estarán sujetos a cambios por la valoración de dicha moneda en el mercado y a un tipo de cambio a consideración de KSH.
</label><br/><br/>
<label style="font-size:14px;">
    2. KSH se reservara el derecho de modificar precios que aparezcan en el sitio web sin previo aviso. Los precios pueden variar en la facturación por cambios y/o actualizaciones de precio por parte los proveedores, errores en su captura, problemas del servidor, entre otros. Ante esto KSH notificará al cliente previa facturación y no se hace responsable de que exista una diferencia entre su cotización electrónica y su facturación final, incluso tras haberse realizado un pago por su concepto.
</label><br/><br/>
<label style="font-size:14px;">
    3. En caso de que el precio de algún producto aparezca en ceros ($0.00) en ningún caso significará que éste sea su precio real y deberá ser considerado un error no intencional en el sistema. En caso de que esto suceda, si desea adquirir dicho producto deberá estar consiente de que al momento de que entre su orden de compra, el departamento de ventas de KSH se pondrá en contacto con usted para precisar el precio real del producto y establecer el monto final de su cotización y factura.
</label><br/><br/>
<label style="font-size:14px;">
    4. El usuario registrado tendrá acceso a conocer los precios de los productos de la tienda en línea, pero bajo ningún concepto podrá divulgar o transmitir dichos datos a terceros. Al aceptar los Términos y Condiciones accede a mantener estos datos en estado confidencial.
</label><br/><br/>
<label style="font-size:14px;">
    5. Los precios pueden varían conforme a la categoría de usuario en tipología, volumen de compra y localización geográfica. KSH sólo podrá otorgar los precios y facturar a la empresa registrada.
</label><br/><br/>

<label style="font-size:14px;">
    <h4>B. DISPONIBILIDAD DE PRODUCTOS</h4><br>
</label><br/><br/>
<label style="font-size:14px;">1. Kuroda Soluciones Hidráulicas (KSH) en su sitio web y servicios de E-commerce presentará los datos de inventario de productos disponibles. Estos datos pueden cambiar sin previo aviso. A su vez, están sujetos a tiempos de actualización y mantenimiento por lo que pueden mostrar información desactualizada. En tal caso, un representante de KSH se comunicará con el cliente y le presentaría una nueva estimación de tiempo de entrega y/o costo. En caso de haber incurrido en un pago y no estar de acuerdo con los nuevos tiempos de entrega y/o costos, se le realizará la devolución íntegra de su dinero de acuerdo a las http://www.ksh.mx/avisos.php, http://ksh.mx/shop/_terminos.php</label><br/><br/>
<label style="font-size:14px;">2. La disponibilidad estará marcada como un cantidad numérica seguida por su unidad (piezas, tramos). El cliente es responsable de verificar que las unidades corresponden a las mismas unidades en las que desea realizar su compra (ejemplo. Verificar si son tramos o metros). </label><br/><br/>
<label style="font-size:14px;">3. Algunos artículos especializados se mandan a comprar directamente al fabricante y no tienen disponibilidad inmediata, estando marcados en 0. Para estos productos el cliente debe levantar una cotización por medio del sitio web para recibir respuesta sobre sus precios y/o tiempos de entrega. </label><br/><br/>
<label style="font-size:14px;">4. Para el cumplimiento de tiempos de entrega el cliente es responsable de verificar que el total de la cantidad en existencia esté disponible conforme a su compra. De lo contrario, un representante contactará al cliente sobre el faltante comunicando nuevos costos y/o tiempos de entrega. </label><br/><br/>
<label style="font-size:14px;">5. En caso de que los productos marcados en su pedido se encuentre en existencia, su tiempo de entrega estará estipulado por <b>3 días hábiles LAB Sucursal Monterrey</b> y sujetos a las http://www.ksh.mx/avisos.php, http://ksh.mx/shop/_terminos.php vigentes.</label><br/><br/>
<label style="font-size:14px;">6. Si la disponibilidad de su pedido cambiara por alguna circunstancia, KSH le informará al cliente de la situación y le presentaría una nueva estimación de tiempo de entrega y/o costo. Para ello deberá contar KSH con los datos actualizados y verídicos conforme a su registro de usuarios, en caso de no localizar al cliente, KSH no se hace responsable sobre órdenes de compra suspendidas.</label><br/><br/>
<label style="font-size:14px;">7. Algunos artículos manejados por KSH no se encuentran presentes en la gama de productos de la tienda en línea, ya sea por su naturaleza, especificación o por política de los proveedores. En dicho caso para pedir informes de productos alternos se le invita a contactar al departamento de ventas mediante la forma de contacto, vía telefónica o presencial para recibir atención y respuesta.</label><br/><br/>
<label style="font-size:14px;">
    <h4>C. MÉTODOS DE PAGO</h4><br>

</label><br/><br/>


<label style="font-size:14px;">
    1. KSH pone a disposición del cliente en su E-commerce los siguientes métodos de pago:<br/>
</label><br/>
<label style="font-size:14px;">
    - Pago con tarjeta de crédito.
</label><br/>
<label style="font-size:14px;">
    - Transferencia bancaria a la cuenta autorizada por KSH.
</label><br/>
<label style="font-size:14px;">
    - Depósito con cheque a la cuenta autorizada por KSH.
</label><br/><br/>

<label style="font-size:14px;">
    2. Para seguridad. el pago con tarjeta de crédito se realiza a partir de servicios de una empresa de manejo de pagos y ninguna información será almacenada. <b>Sin embargo, para efectuar una factura se solicitarán los últimos números de la tarjeta</b>.
</label><br/><br/>
<label style="font-size:14px;">
    3. Si se realiza un pago por medio de transferencia bancaria o cheque, el monto total <b>deberá ser pagado o depositado el mismo día en que se efectuó la compra de E-commerce</b>, teniendo en cuenta que no existan fluctuaciones en el precio por tipo de cambio a pesos o actualizaciones en el precio del producto mismo. De existir un cambio, el personal de KSH se comunicará con el cliente para realizar el pago faltante o cancelar la compra. La disponibilidad del producto podrá variar en el tiempo en que se realizó el pago, estando sujeta a cambios.
</label><br/><br/>
<label style="font-size:14px;">
    4. Cualquier compra registrada en E-commerce que para el día siguiente no se tenga el pago completo será cancelada y se notificará al cliente.
</label><br/><br/>
<label style="font-size:14px;">
    5. Toda orden de compra por medio de E-commerce será procesada única y exclusivamente si KSH recibió en firme el pago por el monto completo de los pedidos. 
</label><br/><br/>

<h4 class="text-center">POLÍTICAS DE ENTREGA Y ENVÍO</h4><br/>
<label style="font-size:14px;">Fecha de Última Actualización: 12/04/2017</label><br/><br/>

<label style="font-size:14px;">1.	Kuroda Soluciones Hidráulicas (KSH) en su sitio web y servicios de E-commerce presentará <b>sus precios y tiempos de entrega</b> considerando como destino del producto <b>Libre a Bordo (LAB) en sus oficinas de Monterrey, Nuevo León, situadas en Alamo No. 3040, Colonia Moderna</b>, contando con montacargas a nivel de piso para hacer la carga de material en <b>horario de atención de 9:00 am a 5:00 pm de Lunes a Viernes</b>, salvo asuetos. El cliente deberá notificar con anticipación la fecha y hora para recoger su mercancía, sujeta a confirmación, al teléfono (81) 83 88 98 00.
</label><br/><br/>
<label style="font-size:14px;">2.	Para productos con disponibilidad en el E-commerce, confirmados por representantes de KSH, se ofrece un tiempo de entrega de 3 días hábiles tras recibir el pago en firme.
</label><br/><br/>
<label style="font-size:14px;">3.	Para productos sin disponibilidad en el E-commerce se deberá contar con la cotización enviada y el tiempo de respuesta prometido. Un representante de KSH se comunicará cuando la mercancía esté disponible para recogerse.
</label><br/><br/>
<label style="font-size:14px;">4.	El transporte del cliente enviado a recoger el producto LAB en la sucursal KSH deberá ser el adecuado para el correcto y seguro traslado del producto y de su personal, según dimensiones, peso y acomodo, y deberá cumplir con los lineamientos vehiculares gubernamentales de tránsito. El cliente debe traer consigo todo tipo de sujeción y señalización requerida. KSH no proporcionará materiales para estos fines ni se responsabiliza por daños ocasionados por un mal transporte y daños de traslado.
</label><br/><br/>
<label style="font-size:14px;">5.	Si se desea que el producto sea enviado a domicilio (en obra) u ocurre, el cliente podrá seleccionar y contratar un servicio de envío tercerizado (paquetería, fletes, transportistas), comunicándolo en las opciones de envío del E-commerce. Los servicios de envío podrán recoger el material LAB en la sucursal de Monterrey o podrán ser llevados a los lugares de acopio si se encuentran dentro del área Metropolitana de Monterrey. <b>El costo y tiempos de entrega del servicio dependerán del transportista y no están considerados dentro de los precios y tiempos de entrega otorgados por KSH, siendo todo cotizado por separado. Considere que productos de grandes dimensiones o peso, como tubería, pueden tener costos muy elevados de traslado</b>. KSH no puede garantizar que existan opciones de envío disponibles para todo cliente y dirección por cuestiones demográficas, lineamientos propios de los servicios de envío y del monto, volumen, peso o fragilidad que los pedidos presenten.
</label><br/><br/>
<label style="font-size:14px;">6.	Los datos requeridos por KSH y/o el servicio de envío seleccionado para efectuar la entrega deberán estar completos y correctos con el fin de que el pedido sea entregado de manera exitosa. Si llegase ha haber un error en los datos proporcionados por el usuario toda la responsabilidad recaerá en el cliente, y la situación del pedido será dispuesta por el criterio de la compañía de envíos y KSH. En dicho caso KSH se pondría en contacto con el usuario para exponer la situación y establecer un plan a realizar.
</label><br/><br/>
<label style="font-size:14px;">7.	La guía de embarque o firma de recolección del material fungirá como comprobante de entrega y recepción del producto por parte del cliente para cualquier fin que KSH requiera. La factura será enviada por correo electrónico al cliente.
</label><br/><br/>
<label style="font-size:14px;">8.	Todo envío tercerizado corre por cuenta del cliente y bajo su propio riesgo y acuerdo con el transportista. KSH también podrá recomendar transportistas, sin obtener una ganancia de estos envíos y sin incurrir  en ninguna responsabilidad sobre sus servicios.
</label><br/><br/>
<label style="font-size:14px;">9.	KSH no se responsabiliza de los daños y demoras causadas por los transportistas tercerizados. Si el producto se llegase a dañar en su transportación deberá ser solucionado entre el cliente y el servicio de envío.
</label><br/><br/>
<label style="font-size:14px;">10.	Al momento de recibir la mercancía en domicilio, el usuario deberá contar con el tiempo, espacio, equipo y personal requerido para la descarga del material. KSH en ningún momento se responsabiliza de la descarga de material y de daños o demoras causadas durante la misma. El usuario debe asegurarse de contar con montacargas, grúa, patín o cualquier equipo necesario para la descarga segura y a tiempo del material así como del espacio e instalaciones para su almacenaje.
</label><br/><br/>
<label style="font-size:14px;">11.	Cualquier retraso en la entrega ocasionado por el cliente a un servicio tercerizado de transporte que implique un mayor costo, deberá ser asumido y pagado por el cliente bajo su entera responsabilidad y de manera inmediata. De no contar con las condiciones adecuadas y atención de descarga en tiempo y forma, el transportista se reserva el derecho de no hacer entrega de la mercancía y regresar con ella, asumiendo el usuario el costo de ese traslado y el de futuros traslados que ocasione. KSH no se responsabiliza de ningún costo derivado de los servicios elegidos y contratados con transportistas tercerizados.
</label><br/><br/>
<label style="font-size:14px;">12.	Una vez que el pedido haya sido entregado, ya sea al cliente en las instalaciones de KSH o a un transportista tercerizado, KSH se deslinda de cualquier responsabilidad de la mercancía, como pueden ser daños en el traslado, robo o pérdida, estando sujetos a las http://www.ksh.mx/avisos.php, http://ksh.mx/shop/_terminos.php
</label><br/><br/>
<label style="font-size:14px;">13.	Los tiempos de entregas se verán sujetos a los tiempos dictados por la disponibilidad de los productos y de los servicios de envíos. En el caso en el que se haya pedido más de un producto es posible que su entrega sea en etapas por cuestiones de disponibilidad (Véase Política De Precio Y Disponibilidad De Productos).</label><br/><br/>

<label style="font-size:14px;">
    
    
</label><br/><br/>
                                    
                                </div>                       
                                            
                </div>
                
                    
            </div>
    
</section>