<?php 
$login_id="";
$login_id = $this->uri->segment(2);
?>
<!-- Section -->
            <section class="page-section">
                <div class="container relative">
                    
                    <!-- Nav Tabs -->
                    <div class="align-center mb-40 mb-xxs-30">
                        <ul class="nav nav-tabs tpl-minimal-tabs">
                            
                            <li class="<?php if($login_id == 1){ ?>active<?php } else if($login_id == ""){ ?>active <?php } ?>">
                                <a href="#mini-one" data-toggle="tab">Iniciar Sesión</a>
                            </li>
                            
                            <li class="<?php if($login_id == 2){ ?>active<?php } ?>">
                                <a href="#mini-two" data-toggle="tab">Registro</a>
                            </li>
                            
                        </ul>
                    </div>
                    <!-- End Nav Tabs -->
                    
                    <!-- Tab panes -->
                    <div class="tab-content tpl-minimal-tabs-cont section-text">
                        
                        <div class="tab-pane fade <?php if($login_id == 1){ ?>in active<?php } else if($login_id == ""){ ?>active <?php } ?>" id="mini-one">
                            
                            <!-- Login Form -->                            
                            <div class="row">
                                <div class="col-md-4 col-md-offset-4">
                                    
                                    <!--<form class="form contact-form" id="contact_form">-->
                                       <?php echo form_open_multipart(base_url('welcome/inicia_session'), array('id' => 'contact_form', 'class' => 'form contact-form')) ?>
                                        <div class="clearfix">                                            
                                            <!-- Username -->
                                            <div class="form-group">
                                                <input type="text" name="username" id="username" class="input-md round form-control" placeholder="Usuario" required>
                                            </div>                                            
                                            <!-- Password -->
                                            <div class="form-group">
                                                <input type="password" name="password" id="password" class="input-md round form-control" placeholder="Contraseña" required>
                                            </div>                                                
                                        </div>                                        
                                        <div class="clearfix">                                            
                                            <div class="cf-left-col">                                                
                                                <!-- Inform Tip -->                                        
                                                <div class="form-tip pt-20">
                                                    <a href="<?php echo base_url('recupera/1') ?>">Olvido la contraseña ?</a>
                                                </div>                                                
                                            </div>                                            
                                            <div class="cf-right-col">                                                
                                                <!-- Send Button -->
                                                <div class="align-right pt-10">
                                                    <button  type="submit"  class="submit_btn btn btn-mod btn-medium btn-round" id="login-btn">Entrar</button>
                                                </div>                                                
                                            </div>                                            
                                        </div> 
                                    
                                    <!--</form>-->
                                    <?php echo form_close(); ?>
                                    <div class="clearfix">
                                        <div class="alerta">
                                            <?php echo '<br>'.$this->session->flashdata('msg');
//                                             if($auth['valid']){redirect(base_url(''), 'refresh');}else{echo $auth['mensaje'];}
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End Login Form -->
                            
                        </div>
                        
                        <div class="tab-pane fade <?php if($login_id == 2){ ?>in active<?php } ?>" id="mini-two">
                            
                            <!-- Registry Form -->                            
                            <div class="row">
                                <div class="col-md-4 col-md-offset-4">
                                    <?php echo form_open_multipart(base_url('welcome/registra_usuario'), array('id' => 'contact_register', 'class' => 'form contact-form')) ?>
                                    <!--<form class="form contact-form" id="contact_form">-->
                                        <div class="clearfix">
                                            
                                            <!-- Email -->
                                            <div class="form-group">
                                                <input type="text" name="Email" id="Email" class="input-md round form-control" placeholder="E-mail" required>
                                            </div>
                                            
                                            <!-- Username -->
<!--                                            <div class="form-group">
                                                <input type="text" name="username" id="username" class="input-md round form-control" placeholder="Usuario" pattern=".{3,100}" required>
                                            </div>-->
                                            
                                            <!-- Password -->
                                            <div class="form-group">
                                                <input type="password" name="pwd" id="pwd" class="input-md round form-control" placeholder="Contraseña" required>
                                            </div>
                                            
                                            <!-- Re-enter Password -->
                                            <div class="form-group">
                                                <input type="password" name="re-password" id="re-password" class="input-md round form-control" placeholder="Confirmar contraseña" required>
                                            </div>
                                                
                                        </div>
                                        
                                        <!-- Send Button -->
                                        <div class="pt-10">
                                            <button type="submit" class="submit_btn btn btn-mod btn-medium btn-round btn-full" id="reg-btn">Registrar</button>
                                        </div>
                                        
                                    </form>
                                    <div class="clearfix">
                                        <div class="alerta">
                                            <?php echo '<br>'.$this->session->flashdata('msg_register');
//                                             if($auth['valid']){redirect(base_url(''), 'refresh');}else{echo $auth['mensaje'];}
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End Registry Form -->
                            
                        </div>
                        
                    </div>
                    
                </div>
            </section>
            <!-- End Section -->