<?php
//$prod_id = $this->input->get('id', TRUE);

$prod_id = $this->uri->segment(2);
?>
 
<section class="page-section pt-20 pb-40">
    <div class="container relative">
        <!-- Intro Text -->
        <div class="row">
            <div class="col-md-12">
                <div class="section-text align-left">
                    <h3 class="tituloproductos1 mt-0 mb-0">Conducción y derivación</h3>
                    <div class="linea"></div>
                </div>
            </div>
        </div>
        <!-- End Intro Text -->
        <!--<div class="clearfix"></div>-->
        
<?php if($prod_id == 1){  ?>          
        <div class="row">
            <div class="col-md-3">
                <?php 
                $this->load->view('vistas/menu_productos');
                ?>
            </div>
            <div class="col-md-9" style="background: #fff;padding: 30px;">
                <div class="row">
                   <div class="col-md-4">
                       <img src="<?php echo base_url('public/theme/images/conduccion_derivacion/tee_partida.jpg') ?>" alt=""  style="width: 100%;"/>
                   </div>
                    <div class="col-md-8" style="text-align:justify;">
                        <h6 class="titulo_producto">Tee partida</h6>
                        <fieldset></fieldset>
                        <div  class="section-text"  style="color:#000;">
                            Este tipo de TEE PARTIDA es el método más rápido, económico y simple para hacer derivaciones sobre líneas de conducción de agua potable. Una de sus principales ventajas es que no necesitas interrumpir el suministro de agua.
                        </div>                            
                    </div>                        
                </div>
                <div class="row pt-40 pb-40">
                      <div class="col-md-4" style="text-align:justify;">                        
                          
                     </div>                      
                    <div class="col-md-8">
   <h6 class="titulo_producto" style="font-size: 16px;">Marca del Produto</h6>
                             <div class="row">
                            <div class=" col-md-10" style="">
                                <img src="<?php echo base_url('public/theme/images/conduccion_derivacion/p/logo-lg.png') ?>" alt="" style="width: 100%;max-width: 162px;"/>                                
                            </div>
                           </div>
                    </div>                    
            </div>
                <div class="row">
                    <div class="col-md-12" style="text-align:justify;">
                        <h4 class="mb-10">Cotizaciones o información de fichas técnicas</h4>
                         <fieldset></fieldset>
                        <form name="frm" id="frm" action="http://mrwasabiserver.com/webpage/ksh17/api-correo/index.php/api/correo_ksh" method="post">
                            <input type="hidden" name="tipo" id="tipo" value="Tee partida" />
                            <div class="row pb-20">
                            <div class="col-md-3"> 
                                Nombre*:<br/>
                                <input type="text" name="nombre" id="nombre" placeholder="Nombre" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3"> 
                                Empresa*:<br>
                                <input type="text" name="empresa" id="empresa"  placeholder="Empresa" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3">
                                Teléfono*:<br>
                                <input type="text" name="telefono" id="telefono"  placeholder="Teléfono" class="input-md round form-control" onkeypress="return isNumberKey(event)" maxlength="10"/> 
                            </div>
                            <div class="col-md-3">
                                E-mail*:<br>
                                <input type="text" name="correo" id="correo"  placeholder="E-mail" class="input-md round form-control"/>
                            </div>
                            </div>
                            <div class="row pb-20">
                            <div class="col-md-3">
                                Ciudad*:<br>
                                <input type="text" name="ciudad" id="ciudad"  placeholder="Ciudad a la que pertences" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-9">
                                Mensaje*:<br>
                                <textarea name="mensaje" id="mensaje" style="width: 100%;height:60px;" placeholder="Mensaje." class="input-md round form-control"></textarea>
                            </div>
                            </div>
                            <div class="row pb-20">
                             <div class="col-md-12 align-right">
                                    <a href="javascript:EnviaProdContacto()" style="padding:10px 25px;font-size: 15px;color:#fff;background:#000;text-decoration: none;">ENVIAR</a>
                             </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
<?php } if($prod_id == 2){  ?>  
   <div class="row">
            <div class="col-md-3">
                <?php 
                $this->load->view('vistas/menu_productos');
                ?>
            </div>
            <div class="col-md-9" style="background: #fff;padding: 30px;">
                <div class="row">
                   <div class="col-md-4">
                       <img src="<?php echo base_url('public/theme/images/conduccion_derivacion/collarines.jpg') ?>" alt=""  style="width: 100%;"/>
                   </div>
                    <div class="col-md-8" style="text-align:justify;">
                        <h6 class="titulo_producto">Collarines (Abrazaderas)</h6>
                        <fieldset></fieldset>
                        <div  class="section-text"  style="color:#000;">
                         Los collarines también conocidos como abrazaderas proporcionan un método conveniente para hacer instalaciones en cualquier clase de tubería  para agua de hierro dúctil, PVC y asbesto cemento. Están disponibles en bronce y acero inoxidable. Unos de sus principales usos es el de separación de tuberías y para reparar irregularidades y daños en los collarines de PVC que originan fugas. <br><br>También pueden reforzar el punto crítico de conexiones al tubo dada a su estructura rígida y su empaque especial. Una de sus principales ventajas es que pueden ser instalados sin interrumpir el suministro de agua.
                        </div>                            
                    </div>
                </div>
                <div class="row pt-40 pb-40">
                      <div class="col-md-4" style="text-align:justify;">
                            
                     </div>                      
                    <div class="col-md-8">
                        <h6 class="titulo_producto" style="font-size: 16px;">Marca del Produto</h6>
                             <div class="row">                            
                                <div class=" col-md-12" >
                                    <img src="<?php echo base_url('public/theme/images/conduccion_derivacion/p/logo-lg.png') ?>" alt="" style="width: 100%;max-width: 162px;"/>     
                            </div>
                           </div>                         
                    </div>
                    
            </div>
                <div class="row">
                    <div class="col-md-12" style="text-align:justify;">
                        <h4 class="mb-10">Cotizaciones o información de fichas técnicas</h4>
                         <fieldset></fieldset>
                        <form name="frm" id="frm" action="http://mrwasabiserver.com/webpage/ksh17/api-correo/index.php/api/correo_ksh" method="post">
                            <input type="hidden" name="tipo" id="tipo" value="Collarines (Abrazaderas)" />
                            <div class="row pb-20">
                            <div class="col-md-3"> 
                                Nombre*:<br/>
                                <input type="text" name="nombre" id="nombre" placeholder="Nombre" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3"> 
                                Empresa*:<br>
                                <input type="text" name="empresa" id="empresa"  placeholder="Empresa" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3">
                                Teléfono*:<br>
                                <input type="text" name="telefono" id="telefono"  placeholder="Teléfono" class="input-md round form-control" onkeypress="return isNumberKey(event)" maxlength="10"/> 
                            </div>
                            <div class="col-md-3">
                                E-mail*:<br>
                                <input type="text" name="correo" id="correo"  placeholder="E-mail" class="input-md round form-control"/>
                            </div>
                            </div>
                            <div class="row pb-20">
                            <div class="col-md-3">
                                Ciudad*:<br>
                                <input type="text" name="ciudad" id="ciudad"  placeholder="Ciudad a la que pertences" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-9">
                                Mensaje*:<br>
                                <textarea name="mensaje" id="mensaje" style="width: 100%;height:60px;" placeholder="Mensaje." class="input-md round form-control"></textarea>
                            </div>
                            </div>
                            <div class="row pb-20">
                             <div class="col-md-12 align-right">
                                  <a href="javascript:EnviaProdContacto()" style="padding:10px 25px;font-size: 15px;color:#fff;background:#000;text-decoration: none;">ENVIAR</a>
                             </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>        

<?php } if($prod_id == 3){  ?>  
<div class="row">
            <div class="col-md-3">
                <?php 
                $this->load->view('vistas/menu_productos');
                ?>
            </div>
            <div class="col-md-9" style="background: #fff;padding: 30px;">
                <div class="row">
                   <div class="col-md-4">
                       <img src="<?php echo base_url('public/theme/images/conduccion_derivacion/juntas_universales.jpg') ?>" alt=""  style="width: 100%;"/>
                   </div>
                   
                    <div class="col-md-8" style="text-align:justify;">
                        <h6 class="titulo_producto">Juntas Universales</h6>
                        <fieldset></fieldset>
                        <div  class="section-text"  style="color:#000;">
                            Se les conoce así porque pueden reparar una fuga en todo tipo de tubería, teniendo una conexión flexible y segura en reparaciones en donde las secciones de tubería dañadas son demasiado largas para utilizar una abrazadera de reparación.                         
                        </div>                            
                    </div>
                </div>
                <div class="row pt-40 pb-0">                    
                      <div class="col-md-4" style="text-align:justify;"></div>
                    <div class="col-md-8">                                             
                          <h6 class="titulo_producto" style="font-size: 16px;">Ventajas</h6>
                        <div>
                            <ul id="ven" style="">
                             <li><span>Empaque con diseño cónico para garantizar un desempeño 100%  bajo condiciones diferentes de presión.</span></li>
                             <li><span>Se adaptan a cualquier tipo de material</span></li>                                                         
                            </ul>
                        </div>                                            
                    </div>           
            </div>
                <div class="row pt-0 pb-40">                    
                        <div class="col-md-4" style="text-align:justify;"></div>
                    <div class="col-md-8">                       
                             <h6 class="titulo_producto" style="font-size: 16px;">Marca del Produto</h6>
                                <div class="row">
                                <div class="col-md-12">
                                    <img src="<?php echo base_url('public/theme/images/conduccion_derivacion/p/logo-lg.png') ?>" alt="" style="width: 100%;max-width: 162px;"/>     
                                </div>
                           </div>
                     </div>        
                </div>

                <div class="row">
                    <div class="col-md-12" style="text-align:justify;">
                        <h4 class="mb-10">Cotizaciones o información de fichas técnicas</h4>
                         <fieldset></fieldset>
                        <form name="frm" id="frm" action="http://mrwasabiserver.com/webpage/ksh17/api-correo/index.php/api/correo_ksh" method="post">
                            <input type="hidden" name="tipo" id="tipo" value="Juntas Universales" />
                            <div class="row pb-20">
                            <div class="col-md-3"> 
                                Nombre*:<br/>
                                <input type="text" name="nombre" id="nombre" placeholder="Nombre" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3"> 
                                Empresa*:<br>
                                <input type="text" name="empresa" id="empresa"  placeholder="Empresa" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3">
                                Teléfono*:<br>
                                <input type="text" name="telefono" id="telefono"  placeholder="Teléfono" class="input-md round form-control" onkeypress="return isNumberKey(event)" maxlength="10"/> 
                            </div>
                            <div class="col-md-3">
                                E-mail*:<br>
                                <input type="text" name="correo" id="correo"  placeholder="E-mail" class="input-md round form-control"/>
                            </div>
                            </div>
                            <div class="row pb-20">
                            <div class="col-md-3">
                                Ciudad*:<br>
                                <input type="text" name="ciudad" id="ciudad"  placeholder="Ciudad a la que pertences" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-9">
                                Mensaje*:<br>
                                <textarea name="mensaje" id="mensaje" style="width: 100%;height:60px;" placeholder="Mensaje." class="input-md round form-control"></textarea>
                            </div>
                            </div>
                            <div class="row pb-20">
                             <div class="col-md-12 align-right">
                                   <a href="javascript:EnviaProdContacto()" style="padding:10px 25px;font-size: 15px;color:#fff;background:#000;text-decoration: none;">ENVIAR</a>
                             </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>                

<?php } if($prod_id == 4){  ?>  
 <div class="row">
            <div class="col-md-3">
                <?php 
                $this->load->view('vistas/menu_productos');
                ?>
            </div>
            <div class="col-md-9" style="background: #fff;padding: 30px;">
                <div class="row">
                   <div class="col-md-4">
                       <img src="<?php echo base_url('public/theme/images/conduccion_derivacion/juntas_gibault.jpg') ?>" alt=""  style="width: 100%;"/>
                   </div>                   
                    <div class="col-md-8" style="text-align:justify;">
                        <h6 class="titulo_producto">Juntas GIBAULT</h6>
                        <fieldset></fieldset>
                        <div  class="section-text"  style="color:#000;">
                            Este tipo de juntas se usa para unir dos tuberías entre sí, son de hierro dúctil por lo cual son económicas. Para este tipo de junta necesitaras saber el diámetro exterior exacto de la tubería para garantizarte un buen sellado.
                        </div>                            
                    </div>
                </div>
                
                <div class="row pt-40 pb-40">                    
                      <div class="col-md-4" style="text-align:justify;">                        
                     </div>                      
                    <div class="col-md-8">  
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" style="text-align:justify;">
                        <h4 class="mb-10">Cotizaciones o información de fichas técnicas</h4>
                         <fieldset></fieldset>
                        <form name="frm" id="frm" action="http://mrwasabiserver.com/webpage/ksh17/api-correo/index.php/api/correo_ksh" method="post">
                            <input type="hidden" name="tipo" id="tipo" value="Juntas GIBAULT" />
                            <div class="row pb-20">
                            <div class="col-md-3"> 
                                Nombre*:<br/>
                                <input type="text" name="nombre" id="nombre" placeholder="Nombre" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3"> 
                                Empresa*:<br>
                                <input type="text" name="empresa" id="empresa"  placeholder="Empresa" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3">
                                Teléfono*:<br>
                                <input type="text" name="telefono" id="telefono"  placeholder="Teléfono" class="input-md round form-control" onkeypress="return isNumberKey(event)" maxlength="10"/> 
                            </div>
                            <div class="col-md-3">
                                E-mail*:<br>
                                <input type="text" name="correo" id="correo"  placeholder="E-mail" class="input-md round form-control"/>
                            </div>
                            </div>
                            <div class="row pb-20">
                            <div class="col-md-3">
                                Ciudad*:<br>
                                <input type="text" name="ciudad" id="ciudad"  placeholder="Ciudad a la que pertences" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-9">
                                Mensaje*:<br>
                                <textarea name="mensaje" id="mensaje" style="width: 100%;height:60px;" placeholder="Mensaje." class="input-md round form-control"></textarea>
                            </div>
                            </div>
                            <div class="row pb-20">
                             <div class="col-md-12 align-right">
                                    <a href="javascript:EnviaProdContacto()" style="padding:10px 25px;font-size: 15px;color:#fff;background:#000;text-decoration: none;">ENVIAR</a>
                             </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>                     

<?php } if($prod_id == 5){  ?>  
 <div class="row">
            <div class="col-md-3">
                <?php 
                $this->load->view('vistas/menu_productos');
                ?>
            </div>
            <div class="col-md-9" style="background: #fff;padding: 30px;">
                <div class="row">
                   <div class="col-md-4">
                       <img src="<?php echo base_url('public/theme/images/conduccion_derivacion/adapt_bridad_ps_1.jpg') ?>" alt=""  style="width: 100%;"/>
                   </div>
                   
                    <div class="col-md-8" style="text-align:justify;">
                        <h6 class="titulo_producto">Adaptadores bridados</h6>
                        <fieldset></fieldset>
                        <div  class="section-text"  style="color:#000;">
Este tipo de adaptadores se conectan fácil y rápidamente a los extremos del tubo de acero, PBVC hierro y asbesto cemento. Es una combinación de junta y brida convencional.                       
                            
                        </div>                            
                    </div>
                </div>
                <div class="row pt-40 pb-40">
                    <div class="col-md-4" style="text-align:justify;">                        
                    </div>
                    <div class="col-md-8">  
                     <h6 class="titulo_producto" style="font-size: 16px;">Marca del Produto</h6>
                    <div class="row">
                    <div class="col-md-12" style="">
                        <img src="<?php echo base_url('public/theme/images/conduccion_derivacion/p/logo-lg.png') ?>" alt="" style="width: 100%;max-width: 162px;"/>
                    </div>
                    </div>
                   </div>
                  </div>
                <div class="row">
                    <div class="col-md-12" style="text-align:justify;">
                        <h4 class="mb-10">Cotizaciones o información de fichas técnicas</h4>
                         <fieldset></fieldset>
                        <form name="frm" id="frm" action="http://mrwasabiserver.com/webpage/ksh17/api-correo/index.php/api/correo_ksh" method="post">
                            <input type="hidden" name="tipo" id="tipo" value="Adaptadores bridados" />
                            <div class="row pb-20">
                            <div class="col-md-3"> 
                                Nombre*:<br/>
                                <input type="text" name="nombre" id="nombre" placeholder="Nombre" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3"> 
                                Empresa*:<br>
                                <input type="text" name="empresa" id="empresa"  placeholder="Empresa" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3">
                                Teléfono*:<br>
                                <input type="text" name="telefono" id="telefono"  placeholder="Teléfono" class="input-md round form-control" onkeypress="return isNumberKey(event)" maxlength="10"/> 
                            </div>
                            <div class="col-md-3">
                                E-mail:<br>
                                <input type="text" name="correo" id="correo"  placeholder="E-mail" class="input-md round form-control"/>
                            </div>
                            </div>
                            <div class="row pb-20">
                            <div class="col-md-3">
                                Ciudad*:<br>
                                <input type="text" name="ciudad" id="ciudad"  placeholder="Ciudad a la que pertences" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-9">
                                Mensaje*:<br>
                                <textarea name="mensaje" id="mensaje" style="width: 100%;height:60px;" placeholder="Mensaje." class="input-md round form-control"></textarea>
                            </div>
                            </div>
                            <div class="row pb-20">
                             <div class="col-md-12 align-right">
                                 <a href="javascript:EnviaProdContacto()" style="padding:10px 25px;font-size: 15px;color:#fff;background:#000;text-decoration: none;">ENVIAR</a>
                             </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>                

<?php } if($prod_id == 6){  ?>  
  <div class="row">
            <div class="col-md-3">
                <?php 
                $this->load->view('vistas/menu_productos');
                ?>
            </div>
            <div class="col-md-9" style="background: #fff;padding: 30px;">
                <div class="row">
                   <div class="col-md-4">
                       <img src="<?php echo base_url('public/theme/images/conduccion_derivacion/bridas.jpg') ?>" alt=""  style="width: 100%;margin-bottom: 2em;"/>
                   </div>
                   
                    <div class="col-md-8">
                        <h6 class="titulo_producto">Bridas rápidas</h6>
                        <fieldset></fieldset>
                        <div class="section-text text-justify">
                         Este tipo de brida se adapta rápidamente a cualquier tipo de tubería, su diseño permite una perfecta hermeticidad sin necesidad de soldar.
                        </div>
                         
                    </div>
                </div>
                <div class="row pt-40 pb-40">                    
                      <div class="col-md-4" style="text-align:justify;">                        
                            
                     </div>                      
                    <div class="col-md-8"> 
                      <h6 class="titulo_producto" style="font-size: 16px;">Marca del Produto</h6>
                                <div class="row">
                                    <div class="col-md-12" style="">
                                        <img src="<?php echo base_url('public/theme/images/conduccion_derivacion/p/logo-lg.png') ?>" alt="" style="width: 100%;max-width: 162px;"/>
                                    </div>                      
                           </div>
                    </div>                 
                </div>
                <div class="row">
                    <div class="col-md-12" style="text-align:justify;">
                        <h4 class="mb-10">Cotizaciones o información de fichas técnicas</h4>
                         <fieldset></fieldset>
                        <form name="frm" id="frm" action="http://mrwasabiserver.com/webpage/ksh17/api-correo/index.php/api/correo_ksh" method="post">
                            <input type="hidden" name="tipo" id="tipo" value="Bridas rápidas" />
                            <div class="row pb-20">
                            <div class="col-md-3"> 
                                Nombre*:<br/>
                                <input type="text" name="nombre" id="nombre" placeholder="Nombre" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3"> 
                                Empresa*:<br>
                                <input type="text" name="empresa" id="empresa"  placeholder="Empresa" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3">
                                Teléfono*:<br>
                                <input type="text" name="telefono" id="telefono"  placeholder="Teléfono" class="input-md round form-control" onkeypress="return isNumberKey(event)" maxlength="10"/> 
                            </div>
                            <div class="col-md-3">
                                E-mail*:<br>
                                <input type="text" name="correo" id="correo"  placeholder="E-mail" class="input-md round form-control"/>
                            </div>
                            </div>
                            <div class="row pb-20">
                            <div class="col-md-3">
                                Ciudad*:<br>
                                <input type="text" name="ciudad" id="ciudad"  placeholder="Ciudad a la que pertences" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-9">
                                Mensaje*:<br>
                                <textarea name="mensaje" id="mensaje" style="width: 100%;height:60px;" placeholder="Mensaje." class="input-md round form-control"></textarea>
                            </div>
                            </div>
                            <div class="row pb-20">
                             <div class="col-md-12 align-right">
                                    <a href="javascript:EnviaProdContacto()" style="padding:10px 25px;font-size: 15px;color:#fff;background:#000;text-decoration: none;">ENVIAR</a>
                             </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>                

<?php } if($prod_id == 7){  ?>  
<div class="row">
            <div class="col-md-3">
                <?php 
                $this->load->view('vistas/menu_productos');
                ?>
            </div>
            <div class="col-md-9" style="background: #fff;padding: 30px;">
                <div class="row">
                   <div class="col-md-4">
                       <img src="<?php echo base_url('public/theme/images/conduccion_derivacion/VegaDrill3900.jpg') ?>" alt=""  style="width: 100%;"/>
                   </div>
                   
                    <div class="col-md-8">
                        <h6 class="titulo_producto">Taladro vega</h6>
                        <fieldset></fieldset>
                        <div class="section-text text-justify">
                            Este tipo de taladro es especial para perforar la tubería sin necesidad de cortar el suministro de agua cuando se usa una válvula compuerta de por medio. Este se compone de  taladro, mecanismo de manejo (manivela) y tipo específico de adaptador. El Taladro "Vega" de manivela rápida y mecanismo de manejo es apto únicamente sobre tuberías de 3/4" y 1".<br/>
                            La unidad de 1 1/2" y 2" vienen con adaptadores de rosca hembra para tubos de hierro (FIP), a menos que se especifique lo contrario.
                        </div>                         
                    </div>
                </div>
                <div class="row pt-40 pb-40">                    
                      <div class="col-md-4" style="text-align:justify;">                        
                          
                     </div>                      
                    <div class="col-md-8">  
                           <h6 class="titulo_producto" style="font-size: 16px;">Marca del Produto</h6>
                             <div class="row">                           
                                <div class="col-md-12">
                                    <img src="<?php echo base_url('public/theme/images/conduccion_derivacion/p/logo-lg.png') ?>" alt="" style="width: 100%;max-width: 162px;"/>
                                </div>
                                </div>                           
                    </div>                 
                </div>
                <div class="row">
                    <div class="col-md-12" style="text-align:justify;">
                        <h4 class="mb-10">Cotizaciones o información de fichas técnicas</h4>
                         <fieldset></fieldset>
                        <form name="frm" id="frm" action="http://mrwasabiserver.com/webpage/ksh17/api-correo/index.php/api/correo_ksh" method="post">
                            <input type="hidden" name="tipo" id="tipo" value="Taladro vega" />
                            <div class="row pb-20">
                            <div class="col-md-3"> 
                                Nombre*:<br/>
                                <input type="text" name="nombre" id="nombre" placeholder="Nombre" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3"> 
                                Empresa*:<br>
                                <input type="text" name="empresa" id="empresa"  placeholder="Empresa" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3">
                                Teléfono*:<br>
                                <input type="text" name="telefono" id="telefono"  placeholder="Teléfono" class="input-md round form-control" onkeypress="return isNumberKey(event)" maxlength="10"/> 
                            </div>
                            <div class="col-md-3">
                                E-mail*:<br>
                                <input type="text" name="correo" id="correo"  placeholder="E-mail" class="input-md round form-control"/>
                            </div>
                            </div>
                            <div class="row pb-20">
                            <div class="col-md-3">
                                Ciudad*:<br>
                                <input type="text" name="ciudad" id="ciudad"  placeholder="Ciudad a la que pertences" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-9">
                                Mensaje*:<br>
                                <textarea name="mensaje" id="mensaje" style="width: 100%;height:60px;" placeholder="Mensaje." class="input-md round form-control"></textarea>
                            </div>
                            </div>
                            <div class="row pb-20">
                             <div class="col-md-12 align-right">
                                  <a href="javascript:EnviaProdContacto()" style="padding:10px 25px;font-size: 15px;color:#fff;background:#000;text-decoration: none;">ENVIAR</a>
                             </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>                    

<?php } if($prod_id == 8){  ?>  
<div class="row">
            <div class="col-md-3">
                <?php 
                $this->load->view('vistas/menu_productos');
                ?>
            </div>
            <div class="col-md-9" style="background: #fff;padding: 30px;">
                <div class="row">
                   <div class="col-md-4">
                       <img src="<?php echo base_url('public/theme/images/conduccion_derivacion/toma_domiciliaria.jpg') ?>" alt=""  style="width: 100%;"/>
                   </div>
                   
                    <div class="col-md-8">
                        <h6 class="titulo_producto">Tomas domiciliarias</h6>
                        <fieldset></fieldset>
                        <div class="section-text text-justify">
                       Este tipo de toma se conforma de un collarín, medidor y un tubo de cobre que los une, una de sus ventajas es el tipo de collarín que permite ser instalado sin necesidad de cortar el suministro de agua.
                        </div>                       
                    </div>
                </div>
                <div class="row pt-40 pb-40">                    
                      <div class="col-md-4" style="text-align:justify;">                        
                       
                     </div>                      
                    <div class="col-md-8">
                        <h6 class="titulo_producto" style="font-size: 16px;">Marca del Produto</h6>
                        <div class="row">
                            <div class="col-md-4">
                                <img src="<?php echo base_url('public/theme/images/conduccion_derivacion/p/logo-lg.png') ?>" alt="" style="width: 100%;max-width: 162px;"/>
                            </div>  
                            <div class="col-md-4 ">
                                <img src="<?php echo base_url('public/theme/images/conduccion_derivacion/p/NACOBRE.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>
                            </div>
                        </div>                      
                    </div>                 
                </div>
                
                <div class="row">
                    <div class="col-md-12" style="text-align:justify;">
                        <h4 class="mb-10">Cotizaciones o información de fichas técnicas</h4>
                         <fieldset></fieldset>
                        <form name="frm" id="frm" action="http://mrwasabiserver.com/webpage/ksh17/api-correo/index.php/api/correo_ksh" method="post">
                            <input type="hidden" name="tipo" id="tipo" value="Tomas domiciliarias" />
                            <div class="row pb-20">
                            <div class="col-md-3"> 
                                Nombre*:<br/>
                                <input type="text" name="nombre" id="nombre" placeholder="Nombre" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3"> 
                                Empresa*:<br>
                                <input type="text" name="empresa" id="empresa"  placeholder="Empresa" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3">
                                Teléfono*:<br>
                                <input type="text" name="telefono" id="telefono"  placeholder="Teléfono" class="input-md round form-control" onkeypress="return isNumberKey(event)" maxlength="10"/> 
                            </div>
                            <div class="col-md-3">
                                E-mail*:<br>
                                <input type="text" name="correo" id="correo"  placeholder="E-mail" class="input-md round form-control"/>
                            </div>
                            </div>
                            <div class="row pb-20">
                            <div class="col-md-3">
                                Ciudad*:<br>
                                <input type="text" name="ciudad" id="ciudad"  placeholder="Ciudad a la que pertences" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-9">
                                Mensaje*:<br>
                                <textarea name="mensaje" id="mensaje" style="width: 100%;height:60px;" placeholder="Mensaje." class="input-md round form-control"></textarea>
                            </div>
                            </div>
                            <div class="row pb-20">
                             <div class="col-md-12 align-right">
                                    <a href="javascript:EnviaProdContacto()" style="padding:10px 25px;font-size: 15px;color:#fff;background:#000;text-decoration: none;">ENVIAR</a>
                             </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>                           

<?php } if($prod_id == 9){  ?>  
<div class="row">
            <div class="col-md-3">
                <?php 
                $this->load->view('vistas/menu_productos');
                ?>
            </div>
            <div class="col-md-9" style="background: #fff;padding: 30px;">
                <div class="row">
                   <div class="col-md-4">
                       <img src="<?php echo base_url('public/theme/images/conduccion_derivacion/herramientas.jpg') ?>" alt=""  style="width: 100%;" class="mt-20"/>
                   </div>                   
                    <div class="col-md-8">
                        <h6 class="titulo_producto">Herramientas</h6>
                        <fieldset></fieldset>
                        <div class="section-text text-justify">
                       Estas herramientas son de tipo Ratchet, todos los equipos contienen una Palanca, un adaptador de 150 mm (6") y una caja hecha específicamente para estas herramientas. Los adaptadores de 150 mm de largo permiten que el obrero instale cualquier pieza correctamente. Además los adaptadores funcionan con todas las llaves estándar.
                        </div>                            
                    </div>
                </div>
                <div class="row pt-40 pb-40">                    
                      <div class="col-md-4" style="text-align:justify;">                        
                         
                     </div>                      
                    <div class="col-md-8">  
                        <h6 class="titulo_producto" style="font-size: 16px;">Marca del Produto</h6>
                             <div class="row">
                                <div class="col-md-12">
                                    <img src="<?php echo base_url('public/theme/images/conduccion_derivacion/p/logo-lg.png') ?>" alt="" style="width: 100%;max-width: 162px;"/>
                                </div>
                                </div>
                    </div>                 
                </div>
                <div class="row">
                    <div class="col-md-12" style="text-align:justify;">
                        <h4 class="mb-10">Cotizaciones o información de fichas técnicas</h4>
                         <fieldset></fieldset>
                        <form name="frm" id="frm" action="http://mrwasabiserver.com/webpage/ksh17/api-correo/index.php/api/correo_ksh" method="post">
                            <input type="hidden" name="tipo" id="tipo" value="Herramientas" />
                            <div class="row pb-20">
                            <div class="col-md-3"> 
                                Nombre*:<br/>
                                <input type="text" name="nombre" id="nombre" placeholder="Nombre" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3"> 
                                Empresa*:<br>
                                <input type="text" name="empresa" id="empresa"  placeholder="Empresa" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3">
                                Teléfono*:<br>
                                <input type="text" name="telefono" id="telefono"  placeholder="Teléfono" class="input-md round form-control" onkeypress="return isNumberKey(event)" maxlength="10"/> 
                            </div>
                            <div class="col-md-3">
                                E-mail*:<br>
                                <input type="text" name="correo" id="correo"  placeholder="E-mail" class="input-md round form-control"/>
                            </div>
                            </div>
                            <div class="row pb-20">
                            <div class="col-md-3">
                                Ciudad*:<br>
                                <input type="text" name="ciudad" id="ciudad"  placeholder="Ciudad a la que pertences" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-9">
                                Mensaje*:<br>
                                <textarea name="mensaje" id="mensaje" style="width: 100%;height:60px;" placeholder="Mensaje." class="input-md round form-control"></textarea>
                            </div>
                            </div>
                            <div class="row pb-20">
                             <div class="col-md-12 align-right">
                                  <a href="javascript:EnviaProdContacto()" style="padding:10px 25px;font-size: 15px;color:#fff;background:#000;text-decoration: none;">ENVIAR</a>
                             </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>          

<?php } ?>        
    </div>
</section>