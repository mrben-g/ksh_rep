<?php
//$prod_id = $this->input->get('id', TRUE);

$prod_id = $this->uri->segment(2);
?>
 
<section class="page-section pt-20 pb-40">
    <div class="container relative">
        <!-- Intro Text -->
        <div class="row">
            <div class="col-md-12">
                <div class="section-text align-left">
                    <h3 class="tituloproductos1 mt-0 mb-0">Reparación de tubería</h3>
                    <div class="linea"></div>
                </div>
            </div>
        </div>
        <!-- End Intro Text -->
        <!--<div class="clearfix"></div>-->
        
<?php if($prod_id == 1){  ?>          
        <div class="row">
            <div class="col-md-3">
                <?php 
                $this->load->view('vistas/menu_productos');
                ?>
            </div>
            <div class="col-md-9" style="background: #fff;padding: 30px;">
                <div class="row">
                   <div class="col-md-4">
                       <img src="<?php echo base_url('public/theme/images/reparacion_tuberia/abrazadera_reparacion.jpg') ?>" alt=""  style="width: 100%;margin-bottom: 2em;"/>
                   </div>
                    <div class="col-md-8" style="text-align:justify;">
                        <h6 class="titulo_producto">Abrazaderas de Reparación</h6>
                        <fieldset></fieldset>
                        <div  class="section-text"  style="color:#000;">
                            La abrazadera es sencilla, rápida de instalar y no requiere herramientas especiales. Una vez ajustadas en la posición correcta, la abrazadera envuelve a la tubería en la zona dañada. El efecto logrado es el comprimir el empaque contra la pared  de la tubería, abarcando toda la circunferencia y formando un sello perfecto.
                        </div>                        
                    </div>
                </div>
                <div class="row pt-40 pb-0">
                    <!--<div class="col-md-8 col-md-offset-4" >-->            
                    <div class="col-md-4" style="text-align:justify;">  </div>
                    <div class="col-md-8">
                    <h6 class="titulo_producto" style="font-size: 16px;">Ventajas</h6>
                        <div>
                            <ul id="ven" style="">
                                <li><span>Rápido de instalar y de alta durabilidad.</span></li>
                                <li><span>No necesita conexiones extras.</span></li>
                                <li><span>No necesita cortar el suministro .</span></li>
                                <li><span>No se necesitan herramientas especiales para su instalación.</span></li>
                            </ul>
                        </div>
                    </div>
                    <!--</div>-->  
            </div>
                <div class="row pt-0 pb-40">
                    <!--<div class="col-md-8 col-md-offset-4" >-->
                       <div class="col-md-4" style="text-align:justify;">  </div>
                    <div class="col-md-8">                    
                             <h6 class="titulo_producto" style="font-size: 16px;">Marca del Produto</h6>
                             <div class="row">
                            <div class=" col-md-10" style="">
                                <img src="<?php echo base_url('public/theme/images/reparacion_tuberia/p/logo-PowerSeal.png') ?>" alt="" style="width: 100%;max-width: 162px;"/>                                
                            </div>
                           </div>
                     </div>         
                </div>
                <div class="row">
                    <div class="col-md-12" style="text-align:justify;">
                        <h4 class="mb-10">Cotizaciones o información de fichas técnicas</h4>
                         <fieldset></fieldset>
                        <form name="frm" id="frm" action="http://mrwasabiserver.com/webpage/ksh17/api-correo/index.php/api/correo_ksh" method="post">
                            <input type="hidden" name="tipo" id="tipo" value="Abrazaderas de Reparación" />
                            <div class="row pb-20">
                            <div class="col-md-3"> 
                                Nombre*:<br/>
                                <input type="text" name="nombre" id="nombre" placeholder="Nombre" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3"> 
                                Empresa*:<br>
                                <input type="text" name="empresa" id="empresa"  placeholder="Empresa" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3">
                                Teléfono*:<br>
                                <input type="text" name="telefono" id="telefono"  placeholder="Teléfono" class="input-md round form-control" onkeypress="return isNumberKey(event)" maxlength="10"/> 
                            </div>
                            <div class="col-md-3">
                                E-mail*:<br>
                                <input type="text" name="correo" id="correo"  placeholder="E-mail" class="input-md round form-control"/>
                            </div>
                            </div>
                            <div class="row pb-20">
                             <div class="col-md-3">
                                Ciudad*:<br>
                                <input type="text" name="ciudad" id="ciudad"  placeholder="Ciudad a la que pertences" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-9">
                                Mensaje*:<br>
                                <textarea name="mensaje" id="mensaje" style="width: 100%;height:60px;" placeholder="Mensaje." class="input-md round form-control"></textarea>
                            </div>
                            </div>
                            <div class="row pb-20">
                             <div class="col-md-12 align-right">
                                  <a href="javascript:EnviaProdContacto()" style="padding:10px 25px;font-size: 15px;color:#fff;background:#000;text-decoration: none;">ENVIAR</a>
                             </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
<?php } if($prod_id == 2){  ?>     
  <div class="row">
            <div class="col-md-3">
                <?php 
                $this->load->view('vistas/menu_productos');
                ?>
            </div>
            <div class="col-md-9" style="background: #fff;padding: 30px;">
                <div class="row">
                   <div class="col-md-4">
                       <img src="<?php echo base_url('public/theme/images/reparacion_tuberia/abrazadera_campana_bell_joint.jpg') ?>" alt=""  style="width: 100%;margin-bottom: 2em;"/>
                   </div>
                    <div class="col-md-8" style="text-align:justify;">
                        <h6 class="titulo_producto">Abrazaderas campana Bell Joint</h6>
                        <fieldset></fieldset>
                        <div  class="section-text"  style="color:#000;">
                         Es un tipo de abrazadera que proporciona una solución definitiva a las fugas entre uniones y se le llama así porque sirve para reparar fugas en donde haya uniones en juntas tipo campana proporcionando una unión entre formas no uniformes.
                        </div>                        
                    </div>
                </div>
                <div class="row pt-40 pb-0">
                    <!--<div class="col-md-8 col-md-offset-4" >-->
                      <div class="col-md-4" style="text-align:justify;"></div>
                    <div class="col-md-8">
                    <h6 class="titulo_producto" style="font-size: 16px;">Ventajas</h6>
                        <div>
                            <ul id="ven" style="">
                            <li><span>Su empaque proporciona un desempeño del 100% bajo diferentes condiciones de presión y material.</span></li>
                            <li><span>Soportan rangos de temperaturas altas.</span></li>
                            <li><span>Construidas con material normado según la norma ASTM A325 y ASTM A563.</span></li>
                            <li><span>Pintura epoxica.</span></li>
                            </ul>
                        </div>
                    </div>
                    <!--</div>-->  
            </div>
                <div class="row pt-0 pb-40">
                     <div class="col-md-4" style="text-align:justify;"></div>
                    <div class="col-md-8"> 
                             <h6 class="titulo_producto" style="font-size: 16px;">Marca del Produto</h6>
                             <div class="row">
                            <div class=" col-md-10" style="">
                                <img src="<?php echo base_url('public/theme/images/reparacion_tuberia/p/logo-PowerSeal.png') ?>" alt="" style="width: 100%;max-width: 162px;"/>                                
                            </div>
                           </div>
                     </div>        
                </div>
                <div class="row">
                    <div class="col-md-12" style="text-align:justify;">
                        <h4 class="mb-10">Cotizaciones o información de fichas técnicas</h4>
                         <fieldset></fieldset>
                        <form name="frm" id="frm" action="http://mrwasabiserver.com/webpage/ksh17/api-correo/index.php/api/correo_ksh" method="post">
                            <input type="hidden" name="tipo" id="tipo" value="Abrazaderas campana Bell Joint" />
                            <div class="row pb-20">
                            <div class="col-md-3"> 
                                Nombre*:<br/>
                                <input type="text" name="nombre" id="nombre" placeholder="Nombre" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3"> 
                                Empresa*:<br>
                                <input type="text" name="empresa" id="empresa"  placeholder="Empresa" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3">
                                Teléfono*:<br>
                                <input type="text" name="telefono" id="telefono"  placeholder="Teléfono" class="input-md round form-control" onkeypress="return isNumberKey(event)" maxlength="10"/> 
                            </div>
                            <div class="col-md-3">
                                E-mail*:<br>
                                <input type="text" name="correo" id="correo"  placeholder="E-mail" class="input-md round form-control"/>
                            </div>
                            </div>
                            <div class="row pb-20">
                            <div class="col-md-3">
                                Ciudad*:<br>
                                <input type="text" name="ciudad" id="ciudad"  placeholder="Ciudad a la que pertences" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-9">
                                Mensaje*:<br>
                                <textarea name="mensaje" id="mensaje" style="width: 100%;height:60px;" placeholder="Mensaje." class="input-md round form-control"></textarea>
                            </div>
                            </div>
                            <div class="row pb-20">
                             <div class="col-md-12 align-right">
                                   <a href="javascript:EnviaProdContacto()" style="padding:10px 25px;font-size: 15px;color:#fff;background:#000;text-decoration: none;">ENVIAR</a>
                             </div>
                            </div>
                        </form>
                    </div>
                </div>
                
                
        </div>
        </div>        
<?php } if($prod_id == 3){  ?>   
<div class="row">
            <div class="col-md-3">
                <?php 
                $this->load->view('vistas/menu_productos');
                ?>
            </div>
            <div class="col-md-9" style="background: #fff;padding: 30px;">
                <div class="row">
                   <div class="col-md-4">
                       <img src="<?php echo base_url('public/theme/images/reparacion_tuberia/juntas_universales.jpg') ?>" alt=""  style="width: 100%;margin-bottom: 2em;"/>
                   </div>
                    <div class="col-md-8" style="text-align:justify;">
                        <h6 class="titulo_producto">Juntas Universales</h6>
                        <fieldset></fieldset>
                        <div  class="section-text"  style="color:#000;">
                        Se les conoce así porque pueden reparar una fuga en todo tipo de tubería, teniendo una conexión flexible y segura en reparaciones en donde las secciones de tubería dañadas son demasiado largas para utilizar una abrazadera de reparación.
                        </div>                        
                    </div>
                </div>
                <div class="row pt-40 pb-0">
                    <!--<div class="col-md-8 col-md-offset-4" >-->
                      <div class="col-md-4" style="text-align:justify;"></div>
                    <div class="col-md-8">
                    <h6 class="titulo_producto" style="font-size: 16px;">Ventajas</h6>
                        <div>
                            <ul id="ven" style="">
                               <li><span>Empaque con diseño cónico para garantizar un desempeño 100%  bajo condiciones diferentes de presión.</span></li>
                                         <li><span>Se adaptan a cualquier tipo de material</span></li>    
                            </ul>
                        </div>
                    </div>
                    <!--</div>-->  
            </div>
                 <div class="row pt-0 pb-40">
                      <div class="col-md-4" style="text-align:justify;"></div>
                    <div class="col-md-8">                     
                             <h6 class="titulo_producto" style="font-size: 16px;">Marca del Produto</h6>
                             <div class="row">
                            <div class=" col-md-10" style="">
                                <img src="<?php echo base_url('public/theme/images/reparacion_tuberia/p/logo-PowerSeal.png') ?>" alt="" style="width: 100%;max-width: 162px;"/>                                
                            </div>
                           </div>
                     </div>             
                 </div>
                <div class="row">
                    <div class="col-md-12" style="text-align:justify;">
                        <h4 class="mb-10">Cotizaciones o información de fichas técnicas</h4>
                         <fieldset></fieldset>
                        <form name="frm" id="frm" action="http://mrwasabiserver.com/webpage/ksh17/api-correo/index.php/api/correo_ksh" method="post">
                            <input type="hidden" name="tipo" id="tipo" value="Juntas Universales" />
                            <div class="row pb-20">
                            <div class="col-md-3"> 
                                Nombre*:<br/>
                                <input type="text" name="nombre" id="nombre" placeholder="Nombre" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3"> 
                                Empresa*:<br>
                                <input type="text" name="empresa" id="empresa"  placeholder="Empresa" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3">
                                Teléfono*:<br>
                                <input type="text" name="telefono" id="telefono"  placeholder="Teléfono" class="input-md round form-control" onkeypress="return isNumberKey(event)" maxlength="10"/> 
                            </div>
                            <div class="col-md-3">
                                E-mail*:<br>
                                <input type="text" name="correo" id="correo"  placeholder="E-mail" class="input-md round form-control"/>
                            </div>
                            </div>
                            <div class="row pb-20">
                            <div class="col-md-3">
                                Ciudad*:<br>
                                <input type="text" name="ciudad" id="ciudad"  placeholder="Ciudad a la que pertences" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-9">
                                Mensaje*:<br>
                                <textarea name="mensaje" id="mensaje" style="width: 100%;height:60px;" placeholder="Mensaje." class="input-md round form-control"></textarea>
                            </div>
                            </div>
                            <div class="row pb-20">
                             <div class="col-md-12 align-right">
                                     <a href="javascript:EnviaProdContacto()" style="padding:10px 25px;font-size: 15px;color:#fff;background:#000;text-decoration: none;">ENVIAR</a>
                             </div>
                            </div>
                        </form>
                    </div>
                </div>
                
                
        </div>
        </div>        
<?php } if($prod_id == 4){  ?>   
<div class="row">
            <div class="col-md-3">
                <?php 
                $this->load->view('vistas/menu_productos');
                ?>
            </div>
            <div class="col-md-9" style="background: #fff;padding: 30px;">
                <div class="row">
                   <div class="col-md-4">
                       <img src="<?php echo base_url('public/theme/images/reparacion_tuberia/coples_dayton.jpg') ?>" alt=""  style="width: 100%;margin-bottom: 2em;"/>
                   </div>
                    <div class="col-md-8" style="text-align:justify;">
                        <h6 class="titulo_producto">Coples DAYTON</h6>
                        <fieldset></fieldset>
                        <div  class="section-text"  style="color:#000;">
                        Este tipo de coples son usados en la unión de segmentos de tuberías. Permiten que los tubos que son unidos resistan tanto las fuerzas internas como externas, las vibraciones y la presión ejercida por los líquidos que fluyen a lo largo de la tubería. Basta aflojar la tuerca (sin llegar a extraerla) y el tubo puede introducirse.
                        </div>                        
                    </div>
                </div>
                <div class="row pt-40 pb-0">
                    <!--<div class="col-md-8 col-md-offset-4" >-->
                    <div class="col-md-4" style="text-align:justify;"></div>
                    <div class="col-md-8">
                    <h6 class="titulo_producto" style="font-size: 16px;">Ventajas</h6>
                        <div>
                            <ul id="ven" style="">
                                <li><span>Menor tiempo de instalación.</span></li>
                                <li><span>Excelentes para distribución de agua.</span></li>
                                <li><span>Empaque resistente a la corrosión.</span></li>        
                            </ul>
                        </div>
                    </div>
                    <!--</div>-->  
            </div>
                <div class="row pt-0 pb-40">
                 <div class="col-md-4" style="text-align:justify;"></div>
                    <div class="col-md-8">                   
                             <h6 class="titulo_producto" style="font-size: 16px;">Marca del Produto</h6>
                             <div class="row">
                            <div class=" col-md-10" style="">
                                <img src="<?php echo base_url('public/theme/images/reparacion_tuberia/p/Logo_kbi.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>                                
                            </div>
                           </div>
                     </div>        
                </div>
                <div class="row">
                    <div class="col-md-12" style="text-align:justify;">
                        <h4 class="mb-10">Cotizaciones o información de fichas técnicas</h4>
                         <fieldset></fieldset>
                        <form name="frm" id="frm" action="http://mrwasabiserver.com/webpage/ksh17/api-correo/index.php/api/correo_ksh" method="post">
                            <input type="hidden" name="tipo" id="tipo" value="Coples DAYTON" />
                            <div class="row pb-20">
                            <div class="col-md-3"> 
                                Nombre*:<br/>
                                <input type="text" name="nombre" id="nombre" placeholder="Nombre" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3"> 
                                Empresa*:<br>
                                <input type="text" name="empresa" id="empresa"  placeholder="Empresa" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3">
                                Teléfono*:<br>
                                <input type="text" name="telefono" id="telefono"  placeholder="Teléfono" class="input-md round form-control" onkeypress="return isNumberKey(event)" maxlength="10"/> 
                            </div>
                            <div class="col-md-3">
                                E-mail*:<br>
                                <input type="text" name="correo" id="correo"  placeholder="E-mail" class="input-md round form-control"/>
                            </div>
                            </div>
                            <div class="row pb-20">
                            <div class="col-md-3">
                                Ciudad*:<br>
                                <input type="text" name="ciudad" id="ciudad"  placeholder="Ciudad a la que pertences" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-9">
                                Mensaje*:<br>
                                <textarea name="mensaje" id="mensaje" style="width: 100%;height:60px;" placeholder="Mensaje." class="input-md round form-control"></textarea>
                            </div>
                            </div>
                            <div class="row pb-20">
                             <div class="col-md-12 align-right">
                                    <a href="javascript:EnviaProdContacto()" style="padding:10px 25px;font-size: 15px;color:#fff;background:#000;text-decoration: none;">ENVIAR</a>
                             </div>
                            </div>
                        </form>
                    </div>
                </div>
                
                
        </div>
        </div>        
<?php } ?>           
    </div>
</section>
 
