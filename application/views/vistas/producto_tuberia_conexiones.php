<?php
//$prod_id = $this->input->get('id', TRUE);

$prod_id = $this->uri->segment(2);
?>
 
<section class="page-section pt-20 pb-40">
    <div class="container relative">
        <!-- Intro Text -->
        <div class="row">
            <div class="col-md-12">
                <div class="section-text align-left">
                    <h3 class="tituloproductos1 mt-0 mb-0">Tubería y conexiones</h3>
                    <div class="linea"></div>
                </div>
            </div>
        </div>
        <!-- End Intro Text -->
        <!--<div class="clearfix"></div>-->
        
<?php if($prod_id == 1){  ?>          
        <div class="row">
            <div class="col-md-3">
                <?php 
                $this->load->view('vistas/menu_productos');
                ?>
            </div>
            <div class="col-md-9" style="background: #fff;padding: 30px;">
                <div class="row">
                   <div class="col-md-4">
                       <img src="<?php echo base_url('public/theme/images/tuberia_conexiones/fofo.jpg') ?>" alt=""  style="width: 100%;margin-bottom: 2em;"/>
                   </div>
                    <div class="col-md-8" style="text-align:justify;">
                        <h6 class="titulo_producto">Fierro vaciado (FOFO)</h6>
                        <fieldset></fieldset>
                        <div  class="section-text"  style="color:#000;">
                     El hierro gris era uno de los materiales ferrosos más empleados para agua potable y su nombre se debe a la apariencia de su superficie al romperse. Una característica distintiva del hierro gris es que el carbono se encuentra en general como grafito, adoptando formas irregulares descritas como “hojuelas”. Este grafito es el que da la coloración gris a las superficies de ruptura de las piezas elaboradas con este material.
                     <br>
                   
                        </div>
                        
                    </div>
                </div>
                <div class="row pt-0 pb-40">
                    <!--<div class="col-md-8 col-md-offset-4" >-->
                      <div class="col-md-4" style="text-align:justify;"></div>
                    <div class="col-md-8">
                         <div class="row mt-20">
                            <div class="section-text col-md-7 text-justify">
                                  También tiene buena resistencia al desgaste, debido a que las "hojuelas" de grafito sirven de autolubricante. Este tipo de tubería es usada en la ingeniería debido a su relativo bajo costo pero con la llegada de nuevas aleaciones de hierro se fue sustituyendo. La fundición gris posee una rotura frágil, es decir, no es dúctil, por lo que no es resistente a las caídas.
                            </div>
                            <div class="section-text col-md-5">
                                   <img src="<?php echo base_url('public/theme/images/tuberia_conexiones/tuberia_conexiones_dos.jpg') ?>" alt=""  style="width: 100%;"/>
                            </div>
                        </div>
<!--                    <h6 class="titulo_producto" style="font-size: 16px;">Ventajas</h6>
                        <div>
                            <ul id="ven" style="">
                                <li><span>Rápido de instalar y de alta durabilidad.</span></li>
                                <li><span>No necesita conexiones extras.</span></li>
                                <li><span>No necesita cortar el suministro .</span></li>
                                <li><span>No se necesitan herramientas especiales para su instalación.</span></li>
                            </ul>
                        </div>-->
                    </div>
                    <!--</div>-->  
            </div>
                <div class="row pt-0 pb-40">
                    <div class="col-md-4" style="text-align:justify;"></div>
                    <div class="col-md-8">           
                             <h6 class="titulo_producto" style="font-size: 16px;">Marca del Produto</h6>
                             <div class="row">
                            <div class=" col-md-10" style="">
                                <img src="<?php echo base_url('public/theme/images/tuberia_conexiones/p/logoCifunsa.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>                                
                            </div>
                           </div>
                     </div>         
                <div class="row">
                    <div class="col-md-12" style="text-align:justify;">
                        <h4 class="mb-10">Cotizaciones o información de fichas técnicas</h4>
                         <fieldset></fieldset>
                        <form name="frm" id="frm" action="http://mrwasabiserver.com/webpage/ksh17/api-correo/index.php/api/correo_ksh" method="post">
                            <input type="hidden" name="tipo" id="tipo" value="FIERRO VACIADO (FOFO)" />
                            <div class="row pb-20">
                            <div class="col-md-3"> 
                                Nombre*:<br/>
                                <input type="text" name="nombre" id="nombre" placeholder="Nombre" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3"> 
                                Empresa*:<br>
                                <input type="text" name="empresa" id="empresa"  placeholder="Empresa" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3">
                                Teléfono*:<br>
                                <input type="text" name="telefono" id="telefono"  placeholder="Teléfono" class="input-md round form-control" onkeypress="return isNumberKey(event)" maxlength="10"/> 
                            </div>
                            <div class="col-md-3">
                                E-mail*:<br>
                                <input type="text" name="correo" id="correo"  placeholder="E-mail" class="input-md round form-control"/>
                            </div>
                            </div>
                            <div class="row pb-20">
                            <div class="col-md-3">
                                Ciudad*:<br>
                                <input type="text" name="ciudad" id="ciudad"  placeholder="Ciudad a la que pertences" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-9">
                                Mensaje*:<br>
                                <textarea name="mensaje" id="mensaje" style="width: 100%;height:60px;" placeholder="Mensaje." class="input-md round form-control"></textarea>
                            </div>
                            </div>
                            <div class="row pb-20">
                             <div class="col-md-12 align-right">
                                      <a href="javascript:EnviaProdContacto()" style="padding:10px 25px;font-size: 15px;color:#fff;background:#000;text-decoration: none;">ENVIAR</a>
                             </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
<?php } if($prod_id == 2){  ?>  
   <div class="row">
            <div class="col-md-3">
                <?php 
                $this->load->view('vistas/menu_productos');
                ?>
            </div>
            <div class="col-md-9" style="background: #fff;padding: 30px;">
                <div class="row">
                   <div class="col-md-4">
                       <img src="<?php echo base_url('public/theme/images/tuberia_conexiones/hierro_ductil_uno.jpg') ?>" alt=""  style="width: 100%;margin-bottom: 2em;"/>
                   </div>
                    <div class="col-md-8" style="text-align:justify;">
                        <h6 class="titulo_producto">Hierro Dúctil</h6>
                        <fieldset></fieldset>
                        <div  class="section-text"  style="color:#000;">
                            El hierro fundido dúctil se diferencia de los hierros fundidos grises tradicionales por sus notables propiedades mecánicas (elasticidad, resistencia a los choques, alargamiento) que se deben a la forma esferoidal de las partículas de grafito.
                        </div>                            
                    </div>
                </div>
                <div class="row pt-0 pb-40">
                    <!--<div class="col-md-8 col-md-offset-4" >-->
                      <div class="col-md-4" style="text-align:justify;"></div>
                    <div class="col-md-8">
                         <div class="row">
                            <div class="col-md-8">
                          <h6 class="titulo_producto" style="font-size: 16px;">Ventajas</h6>
                        <div>
                            <ul id="ven" style="">
                                <li><span>Mayor resistencia a la corrosión que el hierro gris.</span></li>
                                <li><span>Mayor resistencia al alto impacto.</span></li>
                                <li><span>Alta resistencia al reventamiento.</span></li>
                                <li><span>Facilidad de instalación unión campana o brida.</span></li>
                                <li><span>Ahorro en bombeo, mayor diámetro interior.</span></li>
                                <li><span>Mayor vida útil que el hierro.</span></li>
                            </ul>
                        </div>
                            </div>
                            <div class="col-md-4">
                                   <img src="<?php echo base_url('public/theme/images/tuberia_conexiones/hierro_ductil_dos.jpg') ?>" alt=""  style="width: 100%;"/>
                            </div>
                        </div>
                         
                    </div>
                    <!--</div>-->  
            </div>
                   <div class="row pt-0 pb-40">
                    <!--<div class="col-md-8 col-md-offset-4" >-->
                    <div class="col-md-4" style="text-align:justify;"></div>
                    <div class="col-md-8">                       
                             <h6 class="titulo_producto" style="font-size: 16px;">Marca del Produto</h6>
                             <div class="row">
                            <div class=" col-md-10" style="">
                                <div class=" col-md-6" style="text-align:center;">
                                    <img src="<?php echo base_url('public/theme/images/tuberia_conexiones/p/Star-Pipe-logo.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>     
                                </div>  
                                <div class="col-md-6" style="">
                                    <img src="<?php echo base_url('public/theme/images/tuberia_conexiones/p/us-pipe-logo.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>
                                </div>   
                            </div>
                           </div>
                     </div>               
                   </div>
                <div class="row">
                    <div class="col-md-12" style="text-align:justify;">
                        <h4 class="mb-10">Cotizaciones o información de fichas técnicas</h4>
                         <fieldset></fieldset>
                        <form name="frm" id="frm" action="http://mrwasabiserver.com/webpage/ksh17/api-correo/index.php/api/correo_ksh" method="post">
                            <input type="hidden" name="tipo" id="tipo" value="Hierro Dúctil" />
                            <div class="row pb-20">
                            <div class="col-md-3"> 
                                Nombre*:<br/>
                                <input type="text" name="nombre" id="nombre" placeholder="Nombre" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3"> 
                                Empresa*:<br>
                                <input type="text" name="empresa" id="empresa"  placeholder="Empresa" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3">
                                Teléfono*:<br>
                                <input type="text" name="telefono" id="telefono"  placeholder="Teléfono" class="input-md round form-control" onkeypress="return isNumberKey(event)" maxlength="10"/> 
                            </div>
                            <div class="col-md-3">
                                E-mail*:<br>
                                <input type="text" name="correo" id="correo"  placeholder="E-mail" class="input-md round form-control"/>
                            </div>
                            </div>
                            <div class="row pb-20">
                            <div class="col-md-3">
                                Ciudad*:<br>
                                <input type="text" name="ciudad" id="ciudad"  placeholder="Ciudad a la que pertences" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-9">
                                Mensaje*:<br>
                                <textarea name="mensaje" id="mensaje" style="width: 100%;height:60px;" placeholder="Mensaje." class="input-md round form-control"></textarea>
                            </div>
                            </div>
                            <div class="row pb-20">
                             <div class="col-md-12 align-right">
                                  <a href="javascript:EnviaProdContacto()" style="padding:10px 25px;font-size: 15px;color:#fff;background:#000;text-decoration: none;">ENVIAR</a>
                             </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>        

<?php } if($prod_id == 3){  ?>  
<div class="row">
            <div class="col-md-3">
                <?php 
                $this->load->view('vistas/menu_productos');
                ?>
            </div>
            <div class="col-md-9" style="background: #fff;padding: 30px;">
                <div class="row">
                   <div class="col-md-4">
                       <img src="<?php echo base_url('public/theme/images/tuberia_conexiones/PVC_C-900.png') ?>" alt=""  style="width: 100%;margin-bottom: 2em;"/>
                   </div>
                   
                    <div class="col-md-8" style="text-align:justify;">
                        <h6 class="titulo_producto">Pvc C-900</h6>
                        <fieldset></fieldset>
                        <div  class="section-text"  style="color:#000;">
                           La tubería PVC C-900 está hecha de  compuestos aprobados por la norma ASTM D1784, de acuerdo con la dimensión química, y los requisitos físicos de AWWA C900. Esta norma contiene los requisitos de fabricación y control de calidad de tubería de PVC conjunta con tres clases de presión: PR 165 (DR25), PR 235 (DR18) y PR 305 (DR 14).
                           <br><br>
                            <div class="row">
                            <div class="col-md-8">
                                PVC C-900 utiliza una junta, según la norma ASTM F477, para sellar la toma de campana integral a la espiga de la siguiente articulación (que se ajusta a los requisitos de ASTM D3139). Cada extremo macho está biselado para facilitar el montaje de la junta, y el grifo hace referencia marcada para asegurar la profundidad de inserción correcta.
                            </div>
                            <div class="col-md-4">
                                   <img src="<?php echo base_url('public/theme/images/tuberia_conexiones/PVC_C-900_dos.jpg') ?>" alt=""  style="width: 100%;"/>
                            </div>
                           </div>
                        </div>                            
                    </div>
                </div>
                <div class="row pt-40 pb-0">                    
                      <div class="col-md-4" style="text-align:justify;"></div>
                    <div class="col-md-8">
                          <h6 class="titulo_producto" style="font-size: 16px;">Ventajas</h6>
                        <div>
                            <ul id="ven" style="">
                                 <li><span>La campana se puede unir a espigas de hierro.</span></li>
                                 <li><span>Cumple con las normas de fabricación y control de calidad.</span></li>     
                            </ul>
                        </div>                                            
                    </div>           
            </div>
                <div class="row pt-0 pb-40">                    
                     <div class="col-md-4" style="text-align:justify;"></div>
                    <div class="col-md-8">             
                             <h6 class="titulo_producto" style="font-size: 16px;">Marca del Produto</h6>
                             <div class="row">
                            <div class=" col-md-10" style="">
                                <div class="row">
                                <div class="col-md-4" style="text-align:center;">
                                    <img src="<?php echo base_url('public/theme/images/tuberia_conexiones/p/diamond-plastics.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>     
                                </div>  
                                <div class="col-md-4" style="">
                                    <img src="<?php echo base_url('public/theme/images/tuberia_conexiones/p/NAPCO_logo.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>
                                </div>  
                                 <div class="col-md-4" style="">
                                    <img src="<?php echo base_url('public/theme/images/tuberia_conexiones/p/DURMAN.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>
                                </div>  
                                </div>
                            </div>
                           </div>
                     </div>           
                </div>
                <div class="row">
                    <div class="col-md-12" style="text-align:justify;">
                        <h4 class="mb-10">Cotizaciones o información de fichas técnicas</h4>
                         <fieldset></fieldset>
                        <form name="frm" id="frm" action="http://mrwasabiserver.com/webpage/ksh17/api-correo/index.php/api/correo_ksh" method="post">
                            <input type="hidden" name="tipo" id="tipo" value="Pvc C-900" />
                            <div class="row pb-20">
                            <div class="col-md-3"> 
                                Nombre*:<br/>
                                <input type="text" name="nombre" id="nombre" placeholder="Nombre" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3"> 
                                Empresa*:<br>
                                <input type="text" name="empresa" id="empresa"  placeholder="Empresa" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3">
                                Teléfono*:<br>
                                <input type="text" name="telefono" id="telefono"  placeholder="Teléfono" class="input-md round form-control" onkeypress="return isNumberKey(event)" maxlength="10"/> 
                            </div>
                            <div class="col-md-3">
                                E-mail*:<br>
                                <input type="text" name="correo" id="correo"  placeholder="E-mail" class="input-md round form-control"/>
                            </div>
                            </div>
                            <div class="row pb-20">
                            <div class="col-md-3">
                                Ciudad*:<br>
                                <input type="text" name="ciudad" id="ciudad"  placeholder="Ciudad a la que pertences" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-9">
                                Mensaje*:<br>
                                <textarea name="mensaje" id="mensaje" style="width: 100%;height:60px;" placeholder="Mensaje." class="input-md round form-control"></textarea>
                            </div>
                            </div>
                            <div class="row pb-20">
                             <div class="col-md-12 align-right">
                                   <a href="javascript:EnviaProdContacto()" style="padding:10px 25px;font-size: 15px;color:#fff;background:#000;text-decoration: none;">ENVIAR</a>
                             </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>                

<?php } if($prod_id == 4){  ?>  
 <div class="row">
            <div class="col-md-3">
                <?php 
                $this->load->view('vistas/menu_productos');
                ?>
            </div>
            <div class="col-md-9" style="background: #fff;padding: 30px;">
                <div class="row">
                   <div class="col-md-4">
                       <img src="<?php echo base_url('public/theme/images/tuberia_conexiones/alcantarillado_tuberia.jpg') ?>" alt=""  style="width: 100%;margin-bottom: 2em;"/>
                   </div>
                   
                    <div class="col-md-8" style="text-align:justify;">
                        <h6 class="titulo_producto">Alcantarillado S. 20 y 25</h6>
                        <fieldset></fieldset>
                        <div  class="section-text"  style="color:#000;">
                         La tubería de PVC Alcantarillado se fabrica en Sistema Métrico bajo la norma nacional NMX-E-215, se fabrica con Resina ( materia prima ) virgen 12454-B, su fabricación es de campana tipo RIEBER o tipo ANGER en uno de sus extremos y el otro es terminación espiga, el Color es en Marrón y se puede conectar con cualquier conexión de sistema Métrico la más recomendada seria la conexión alcantarillada métrica Fabricada o Inyectada de campana y anillo, la tubería de PVC de alcantarillado es compatible con la línea sanitaria ya que ambos sistemas son métricos. Se fabrica en dos series, SERIE - 25 para uso de drenaje en general en poblaciones y ciudades de trafico normal y SERIE - 20 para uso de drenajes en zonas en donde el peso volumétrico del material de relleno sea igual o mayor a 2,000 kg/m3.
                       
                            
                        </div>                            
                    </div>
                </div>
                <div class="row pt-40 pb-0">                    
                   <div class="col-md-4" style="text-align:justify;"></div>
                    <div class="col-md-8">  
                        <div class="row">
                            <div class="col-md-8">
                               <h6 class="titulo_producto" style="font-size: 16px;">Ventajas</h6>
                               <div>
                                   <ul id="ven" style="">
                                       <li><span>Hermeticidad total sin filtraciones.</span></li>
                                       <li><span>Paredes lisas que permiten transportar un mayor volumen.</span></li>
                                       <li><span>Resistencia química a la corrosión.</span></li>
                                       <li><span>Útil para sistemas de alcantarillado, sanitario y pluvial.</span></li>   
                                   </ul>
                               </div>
                            </div>
                            <div class="col-md-4">
                                <img src="<?php echo base_url('public/theme/images/tuberia_conexiones/alcantarillado_tee.jpg') ?>" alt=""  style="width: 100%;"/>
                            </div>
                        </div>
                    </div>
                      <div class="col-md-4" style="text-align:justify;"></div>
                    <div class="col-md-8">  
                        <div class="row">
                            <div  class="section-text text-justify"  style="color:#000;">
                                Nota: <br>No utilice Aire o Gas Comprimido para hacer pruebas en productos o sistemas compuestos por tuberías Termoplásticas de PVC o CPVC, y no utilice dispositivos impulsados con Aire o Gas Comprimido para depurar dichos sistemas, estas prácticas podrían producir la Fragmentación Explosiva de las tuberías del sistema o sus componentes y causar lesiones personales serias o fatales.
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row pt-40 pb-40">                    
                   <div class="col-md-4" style="text-align:justify;"></div>
                    <div class="col-md-8">         
                             <h6 class="titulo_producto" style="font-size: 16px;">Marca del Produto</h6>
                             <div class="row">
                            <div class=" col-md-10" style="">
                                <div class="row">                                
                                <div class="col-md-6" style="">
                                    <img src="<?php echo base_url('public/theme/images/tuberia_conexiones/p/AMANCO.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>
                                </div>  
                                 <div class="col-md-6" style="">
                                    <img src="<?php echo base_url('public/theme/images/tuberia_conexiones/p/DURMAN.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>
                                </div>  
                                </div>
                            </div>
                           </div>
                     </div>         
                </div>
                <div class="row">
                    <div class="col-md-12" style="text-align:justify;">
                        <h4 class="mb-10">Cotizaciones o información de fichas técnicas</h4>
                         <fieldset></fieldset>
                        <form name="frm" id="frm" action="http://mrwasabiserver.com/webpage/ksh17/api-correo/index.php/api/correo_ksh" method="post">
                            <input type="hidden" name="tipo" id="tipo" value="Alcantarillado S. 20 y 25" />
                            <div class="row pb-20">
                            <div class="col-md-3"> 
                                Nombre*:<br/>
                                <input type="text" name="nombre" id="nombre" placeholder="Nombre" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3"> 
                                Empresa*:<br>
                                <input type="text" name="empresa" id="empresa"  placeholder="Empresa" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3">
                                Teléfono*:<br>
                                <input type="text" name="telefono" id="telefono"  placeholder="Teléfono" class="input-md round form-control" onkeypress="return isNumberKey(event)" maxlength="10"/> 
                            </div>
                            <div class="col-md-3">
                                E-mail*:<br>
                                <input type="text" name="correo" id="correo"  placeholder="E-mail" class="input-md round form-control"/>
                            </div>
                            </div>
                            <div class="row pb-20">
                            <div class="col-md-3">
                                Ciudad*:<br>
                                <input type="text" name="ciudad" id="ciudad"  placeholder="Ciudad a la que pertences" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-9">
                                Mensaje*:<br>
                                <textarea name="mensaje" id="mensaje" style="width: 100%;height:60px;" placeholder="Mensaje." class="input-md round form-control"></textarea>
                            </div>
                            </div>
                            <div class="row pb-20">
                             <div class="col-md-12 align-right">
                                   <a href="javascript:EnviaProdContacto()" style="padding:10px 25px;font-size: 15px;color:#fff;background:#000;text-decoration: none;">ENVIAR</a>
                             </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>                     

<?php } if($prod_id == 5){  ?>  
 <div class="row">
            <div class="col-md-3">
                <?php 
                $this->load->view('vistas/menu_productos');
                ?>
            </div>
            <div class="col-md-9" style="background: #fff;padding: 30px;">
                <div class="row">
                   <div class="col-md-4">
                       <img src="<?php echo base_url('public/theme/images/tuberia_conexiones/pead.jpg') ?>" alt=""  style="width: 100%;margin-bottom: 2em;"/>
                   </div>
                   
                    <div class="col-md-8" style="text-align:justify;">
                        <h6 class="titulo_producto">Pead grandes Diametros</h6>
                        <fieldset></fieldset>
                        <div  class="section-text"  style="color:#000;">
Este tipo de tubos son utilizados en sistemas de alcantarillados cloacales o pluviales, sustituyendo materiales de concreto y acero dúctil.<br><br>
Utilizando tubos perfilados, se puede ahorrar hasta el 65% del peso comparado con el peso de un tubo equivalente de pared sólida (es decir, con la misma capacidad estática).<br><br>
Los tubos fabricados en Polietileno y Polipropileno presentan considerables ventajas sobre otros materiales como el concreto, el acero, el hierro dúctil, e, inclusive, algunos plásticos. Una de estas ventajas es la alta flexibilidad que poseen, propia de los materiales termoplásticos. Esto hace que, aún en áreas muy afectadas por temblores y terremotos, los tubos no sufran prácticamente daños.
                       
                            
                        </div>                            
                    </div>
                </div>
                <div class="row pt-40 pb-40">                    
                      <div class="col-md-4" style="text-align:justify;">                        
                             
                     </div>                      
                    <div class="col-md-8"> 
                        <h6 class="titulo_producto" style="font-size: 16px;">Marca del Produto</h6>
                             <div class="row">
                            <div class=" col-md-10" style="">
                                <div class="row">                                
                                <div class="col-md-12" style="">
                                    <img src="<?php echo base_url('public/theme/images/tuberia_conexiones/p/krah_log.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>
                                </div>  
                               
                                </div>
                            </div>
                           </div>
<!--                        <div class="row">
                            <div class="col-md-8">
                           
                            </div>
                            <div class="col-md-4">
                           
                            </div>
                        </div>-->
                    </div>
                 
                </div>
                <div class="row">
                    <div class="col-md-12" style="text-align:justify;">
                        <h4 class="mb-10">Cotizaciones o información de fichas técnicas</h4>
                         <fieldset></fieldset>
                        <form name="frm" id="frm" action="http://mrwasabiserver.com/webpage/ksh17/api-correo/index.php/api/correo_ksh" method="post">
                            <input type="hidden" name="tipo" id="tipo" value="Pead grandes Diametros" />
                            <div class="row pb-20">
                            <div class="col-md-3"> 
                                Nombre*:<br/>
                                <input type="text" name="nombre" id="nombre" placeholder="Nombre" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3"> 
                                Empresa*:<br>
                                <input type="text" name="empresa" id="empresa"  placeholder="Empresa" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3">
                                Teléfono*:<br>
                                <input type="text" name="telefono" id="telefono"  placeholder="Teléfono" class="input-md round form-control" onkeypress="return isNumberKey(event)" maxlength="10"/> 
                            </div>
                            <div class="col-md-3">
                                E-mail*:<br>
                                <input type="text" name="correo" id="correo"  placeholder="E-mail" class="input-md round form-control"/>
                            </div>
                            </div>
                            <div class="row pb-20">
                            <div class="col-md-3">
                                Ciudad*:<br>
                                <input type="text" name="ciudad" id="ciudad"  placeholder="Ciudad a la que pertences" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-9">
                                Mensaje*:<br>
                                <textarea name="mensaje" id="mensaje" style="width: 100%;height:60px;" placeholder="Mensaje." class="input-md round form-control"></textarea>
                            </div>
                            </div>
                            <div class="row pb-20">
                             <div class="col-md-12 align-right">
                                   <a href="javascript:EnviaProdContacto()" style="padding:10px 25px;font-size: 15px;color:#fff;background:#000;text-decoration: none;">ENVIAR</a>
                             </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>                

<?php } if($prod_id == 6){  ?>  
  <div class="row">
            <div class="col-md-3">
                <?php 
                $this->load->view('vistas/menu_productos');
                ?>
            </div>
            <div class="col-md-9" style="background: #fff;padding: 30px;">
                <div class="row">
                   <div class="col-md-4">
                       <img src="<?php echo base_url('public/theme/images/tuberia_conexiones/galvanizado.jpg') ?>" alt=""  style="width: 100%;margin-bottom: 2em;"/>
                   </div>
                   
                    <div class="col-md-8">
                        <h6 class="titulo_producto">Acero</h6>
                        <fieldset></fieldset>
                        <div class="section-text text-justify">
                            Este tipo de material puede ser galvanizado, estructural y acero negro, conforme al uso que se le dé, se cubre un metal con otro teniendo como principal objetivo evitar la oxidación y corrosión que la humedad y la contaminación ambiental pueden ocasionar sobre el hierro.
                        </div>
                         <div class="row mt-10">                             
                            <div class="section-text col-md-8 text-justify">
                                Esta actividad representa aproximadamente el 50% del consumo de zinc en el mundo y desde hace más de 150 años se ha ido afianzando como el procedimiento más fiable y económico de protección del hierro contra la corrosión.                                                        
                            </div>
                            <div class="section-text col-md-4">
                                <img src="<?php echo base_url('public/theme/images/tuberia_conexiones/niple.jpg') ?>" alt=""  style="width: 100%;" class=""/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row pt-40 pb-40">                    
                      <div class="col-md-4" style="text-align:justify;">                        
                           
                     </div>                      
                    <div class="col-md-8">  
                          <h6 class="titulo_producto" style="font-size: 16px;">Marca del Produto</h6>
                             <div class="row">
                            <div class=" col-md-10" style="">
                                <div class="row">                                
                                <div class="col-md-6" style="">
                                    <img src="<?php echo base_url('public/theme/images/tuberia_conexiones/p/logo_ternium.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>
                                </div>  
                               <div class="col-md-6" style="">
                                    <img src="<?php echo base_url('public/theme/images/tuberia_conexiones/p/LOGO_pytco.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>
                                </div>  
                                    <div class="col-md-6" style="">
                                    <img src="<?php echo base_url('public/theme/images/tuberia_conexiones/p/LOGO_TENARIStamsa.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>
                                </div>  
                                    <div class="col-md-6" style="">
                                    <img src="<?php echo base_url('public/theme/images/tuberia_conexiones/p/LOGO_TUBERIA_LAGUNA.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>
                                </div>  
                                       
                                </div>
                            </div>
                           </div>
<!--                        <div class="row">
                            <div class="col-md-8">
                           
                            </div>
                            <div class="col-md-4">
                           
                            </div>
                        </div>-->
                    </div>
                 
                </div>
                <div class="row">
                    <div class="col-md-12" style="text-align:justify;">
                        <h4 class="mb-10">Cotizaciones o información de fichas técnicas</h4>
                         <fieldset></fieldset>
                        <form name="frm" id="frm" action="http://mrwasabiserver.com/webpage/ksh17/api-correo/index.php/api/correo_ksh" method="post">
                            <input type="hidden" name="tipo" id="tipo" value="Acero" />
                            <div class="row pb-20">
                            <div class="col-md-3"> 
                                Nombre*:<br/>
                                <input type="text" name="nombre" id="nombre" placeholder="Nombre" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3"> 
                                Empresa*:<br>
                                <input type="text" name="empresa" id="empresa"  placeholder="Empresa" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3">
                                Teléfono*:<br>
                                <input type="text" name="telefono" id="telefono"  placeholder="Teléfono" class="input-md round form-control" onkeypress="return isNumberKey(event)" maxlength="10" /> 
                            </div>
                            <div class="col-md-3">
                                E-mail*:<br>
                                <input type="text" name="correo" id="correo"  placeholder="E-mail" class="input-md round form-control"/>
                            </div>
                            </div>
                            <div class="row pb-20">
                           <div class="col-md-3">
                                Ciudad*:<br>
                                <input type="text" name="ciudad" id="ciudad"  placeholder="Ciudad a la que pertences" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-9">
                                Mensaje*:<br>
                                <textarea name="mensaje" id="mensaje" style="width: 100%;height:60px;" placeholder="Mensaje." class="input-md round form-control"></textarea>
                            </div>
                            </div>
                            <div class="row pb-20">
                             <div class="col-md-12 align-right">
                                   <a href="javascript:EnviaProdContacto()" style="padding:10px 25px;font-size: 15px;color:#fff;background:#000;text-decoration: none;">ENVIAR</a>
                             </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>                

<?php } if($prod_id == 7){  ?>  
<div class="row">
            <div class="col-md-3">
                <?php 
                $this->load->view('vistas/menu_productos');
                ?>
            </div>
            <div class="col-md-9" style="background: #fff;padding: 30px;">
                <div class="row">
                   <div class="col-md-4">
                       <img src="<?php echo base_url('public/theme/images/tuberia_conexiones/tubo_cobre.jpg') ?>" alt=""  style="width: 100%;margin-bottom: 2em;"/>
                   </div>
                   
                    <div class="col-md-8">
                        <h6 class="titulo_producto">Cobre</h6>
                        <fieldset></fieldset>
                        <!--<div class="section-text text-justify"></div>-->
                         <div class="row mt-10">                             
                            <div class="section-text col-md-7 text-justify">
                            El cobre desoxidado con alto contenido de fósforo residual es un cobre puro electrolíticamente refinado, desoxidado con fósforo.
                            </div>
                            <div class="section-text col-md-5">
                                <img src="<?php echo base_url('public/theme/images/tuberia_conexiones/coneccionesdecobre.jpg') ?>" alt=""  style="width: 100%;" class=""/>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row pt-40 pb-0">                    
                      <div class="col-md-4" style="text-align:justify;">
                     </div>                      
                    <div class="col-md-8">  
                        <div class="row">
                            <div class="col-md-6">
                           <h6 class="titulo_producto" style="font-size: 16px;">Usos</h6>
                               <div>
                                   <ul id="ven" style="">
                                   <li><span>Intercambiadores de calor.</span></li>
                                         <li><span>Condensadores y evaporadores.</span></li>
                                         <li><span>Calderas.</span></li>
                                         <li><span>Plantas de energía.</span></li>
                                         <li><span>Ingenios y refinerías.</span></li>
                                         <li><span>Equipos de aire acondicionado, refrigeración y calefacción.</span></li>
                                   </ul>
                               </div>
                            </div>
                            <div class="col-md-6">
                           <h6 class="titulo_producto" style="font-size: 16px;">Características</h6>
                               <div>
                                   <ul id="ven" style="">
                                    <li><span>Mayor conductividad térmica.</span></li>
                                         <li><span>Alta resistencia a la corrosión.</span></li>          
                                   </ul>
                               </div>
                            </div>
                        </div>
                    </div>                 
                </div>
                
                <div class="row pt-0 pb-40">                    
                      <div class="col-md-4" style="text-align:justify;">
                     </div>                      
                    <div class="col-md-8">                
                             <h6 class="titulo_producto" style="font-size: 16px;">Marca del Produto</h6>
                             <div class="row">
                            <div class=" col-md-10" style="">
                                <div class="row">                                
                                <div class="col-md-6" style="">
                                    <img src="<?php echo base_url('public/theme/images/tuberia_conexiones/p/NACOBRE.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>
                                </div>  
                               <div class="col-md-6" style="">
                                    <img src="<?php echo base_url('public/theme/images/tuberia_conexiones/p/logo_muellerindustrial.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>
                                </div>  
                                
                                       
                                </div>
                            </div>
                           </div>
                     </div>           
                </div>
                <div class="row">
                    <div class="col-md-12" style="text-align:justify;">
                        <h4 class="mb-10">Cotizaciones o información de fichas técnicas</h4>
                         <fieldset></fieldset>
                        <form name="frm" id="frm" action="http://mrwasabiserver.com/webpage/ksh17/api-correo/index.php/api/correo_ksh" method="post">
                            <input type="hidden" name="tipo" id="tipo" value="Cobre" />
                            <div class="row pb-20">
                            <div class="col-md-3"> 
                                Nombre*:<br/>
                                <input type="text" name="nombre" id="nombre" placeholder="Nombre" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3"> 
                                Empresa*:<br>
                                <input type="text" name="empresa" id="empresa"  placeholder="Empresa" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3">
                                Teléfono*:<br>
                                <input type="text" name="telefono" id="telefono"  placeholder="Teléfono" class="input-md round form-control" onkeypress="return isNumberKey(event)" maxlength="10" /> 
                            </div>
                            <div class="col-md-3">
                                E-mail*:<br>
                                <input type="text" name="correo" id="correo"  placeholder="E-mail" class="input-md round form-control"/>
                            </div>
                            </div>
                            <div class="row pb-20">
                         <div class="col-md-3">
                                Ciudad*:<br>
                                <input type="text" name="ciudad" id="ciudad"  placeholder="Ciudad a la que pertences" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-9">
                                Mensaje*:<br>
                                <textarea name="mensaje" id="mensaje" style="width: 100%;height:60px;" placeholder="Mensaje." class="input-md round form-control"></textarea>
                            </div>
                            </div>
                            <div class="row pb-20">
                             <div class="col-md-12 align-right">
                                   <a href="javascript:EnviaProdContacto()" style="padding:10px 25px;font-size: 15px;color:#fff;background:#000;text-decoration: none;">ENVIAR</a>
                             </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>                    

<?php } if($prod_id == 8){  ?>  
<div class="row">
            <div class="col-md-3">
                <?php 
                $this->load->view('vistas/menu_productos');
                ?>
            </div>
            <div class="col-md-9" style="background: #fff;padding: 30px;">
                <div class="row">
                   <div class="col-md-4">
                       <img src="<?php echo base_url('public/theme/images/tuberia_conexiones/PVCSIySM.jpg') ?>" alt=""  style="width: 100%;"/>
                   </div>
                   
                    <div class="col-md-8">
                        <h6 class="titulo_producto">Pvc SI y SM</h6>
                        <fieldset></fieldset>
                        <div class="section-text text-justify">
                        La tubería de PVC se puede clasificar según el Sistema de dimensionamiento, la presión de trabajo y el tipo de unión que posee. La base de esta clasificación es el tipo de sistema que se usa, ya sea Serie Inglesa o Serie Métrica.
                        </div>
                         <div class="section-text text-justify">
                         <h6 class="titulo_producto"  style="font-style: italic;font-size: 16px;">Serie Inglesa (SI)</h6>
                         Se basa en tuberías cuyas especificaciones originales son de EE.UU. normalmente de la American Society for Testing and Materials (ASTM - Asociación Americana para Pruebas y Materiales-). Una característica importante es que el diámetro nominal (DN) no corresponde al diámetro externo (DE) ni al diámetro interno (DI). Mantiene constante el DE para los diferentes espesores de pared (e), por lo que el diseño del tubo se basa en esta característica. Se mide en pulgadas y pueden ser expresadas además en milímetros.
                         </div>
                         <div class="row mt-10">                             
                            <div class="section-text col-md-8 text-justify">
                          <h6 class="titulo_producto"  style="font-style: italic;font-size: 16px;">Serie Métrica (SM)</h6>
                          Las especificaciones originales para este tipo de tubería proceden de la International Standars Organization (ISO - Organización Internacional de Normas-). En este caso el DN corresponde al DE. Al igual que la tubería de Serie Inglesa mantiene constante el DE a diferentes espesores de pared según la presión nominal. Se mide en milímetros.<br>
                          Los diámetros de los dos tipos de tuberías no coinciden dimensionalmente por lo que no se pueden hacer uniones directamente, sino mediante el uso de una transición. Es muy común por ejemplo utilizar piezas adaptadoras para la unión de tuberías milimétricas a válvulas del sistema ingles.
                            </div>
                            <div class="section-text col-md-4">
                                <img src="<?php echo base_url('public/theme/images/tuberia_conexiones/PVC_SI_SM_dos.png') ?>" alt=""  style="width: 100%;" class="mt-40 mt-xs-20"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row pt-40 pb-40">                       
                     <div class="col-md-4" style="text-align:justify;">
                     </div>                      
                    <div class="col-md-8">                        
                             <h6 class="titulo_producto" style="font-size: 16px;">Marca del Produto</h6>
                             <div class="row">
                            <div class=" col-md-10" style="">
                                <div class="row">                                
                                <div class="col-md-6" style="">
                                    <img src="<?php echo base_url('public/theme/images/tuberia_conexiones/p/DURMAN.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>
                                </div>  
                               <div class="col-md-6" style="">
                                    <img src="<?php echo base_url('public/theme/images/tuberia_conexiones/p/AMANCO.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>
                                </div>  
                                
                                     
                                </div>
                            </div>
                           </div>
                     </div>                      
                    <div class="col-md-8">  
<!--                        <div class="row">
                            <div class="col-md-6">
                           
                            </div>
                            <div class="col-md-6">
                           
                            </div>
                        </div>-->
                    </div>
                 
                </div>
                <div class="row">
                    <div class="col-md-12" style="text-align:justify;">
                        <h4 class="mb-10">Cotizaciones o información de fichas técnicas</h4>
                         <fieldset></fieldset>
                        <form name="frm" id="frm" action="http://mrwasabiserver.com/webpage/ksh17/api-correo/index.php/api/correo_ksh" method="post">
                            <input type="hidden" name="tipo" id="tipo" value="Pvc SI y SM" />
                            <div class="row pb-20">
                            <div class="col-md-3"> 
                                Nombre*:<br/>
                                <input type="text" name="nombre" id="nombre" placeholder="Nombre" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3"> 
                                Empresa*:<br>
                                <input type="text" name="empresa" id="empresa"  placeholder="Empresa" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3">
                                Teléfono*:<br>
                                <input type="text" name="telefono" id="telefono"  placeholder="Teléfono" class="input-md round form-control" onkeypress="return isNumberKey(event)" maxlength="10" /> 
                            </div>
                            <div class="col-md-3">
                                E-mail*:<br>
                                <input type="text" name="correo" id="correo"  placeholder="E-mail" class="input-md round form-control"/>
                            </div>
                            </div>
                            <div class="row pb-20">
                           <div class="col-md-3">
                                Ciudad*:<br>
                                <input type="text" name="ciudad" id="ciudad"  placeholder="Ciudad a la que pertences" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-9">
                                Mensaje*:<br>
                                <textarea name="mensaje" id="mensaje" style="width: 100%;height:60px;" placeholder="Mensaje." class="input-md round form-control"></textarea>
                            </div>
                            </div>
                            <div class="row pb-20">
                             <div class="col-md-12 align-right">
                                   <a href="javascript:EnviaProdContacto()" style="padding:10px 25px;font-size: 15px;color:#fff;background:#000;text-decoration: none;">ENVIAR</a>
                             </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>                           

<?php } if($prod_id == 9){  ?>  
<div class="row">
            <div class="col-md-3">
                <?php 
                $this->load->view('vistas/menu_productos');
                ?>
            </div>
            <div class="col-md-9" style="background: #fff;padding: 30px;">
                <div class="row">
                   <div class="col-md-4">
                       <img src="<?php echo base_url('public/theme/images/tuberia_conexiones/PVC-Hidraulica_.jpg') ?>" alt=""  style="width: 100%;"/>
                   </div>
                   
                    <div class="col-md-8">
                        <h6 class="titulo_producto">PVC C-80</h6>
                        <fieldset></fieldset>
                        <div class="section-text text-justify">
                       Fabricada con las más modernas técnicas de producción, para cumplir con las especificaciones establecidas en las normas mexicanas e internacionales. La cédula indica el espesor de la pared, esto sirve para saber cuál usar dependiendo de las presiones que se manejen, en el caso de la cedula 80 se usa para altas preciones. Los diámetros nominales van desde ½ hasta 14” (13 a 350 mm). Según norma NMX-E-224- VIGENTE, ASTM D-1785 Y ASTM D-2466.
                        </div>
                        
                         <div class="row mt-10">                             
                            <div class="section-text col-md-7 text-justify">
                          
                      Esta línea de productos tiene su principal aplicación en la conducción de fluidos a alta presión, tales como la distribución de agua potable, sistemas de riego agrícola y residencial.
                            </div>
                            <div class="section-text col-md-5">
                                <img src="<?php echo base_url('public/theme/images/tuberia_conexiones/Plomeria.png') ?>" alt=""  style="width: 100%;" class=""/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row pt-40 pb-40">                    
                      <div class="col-md-4" style="text-align:justify;">
                     </div>                      
                    <div class="col-md-8">                    
                             <h6 class="titulo_producto" style="font-size: 16px;">Marca del Produto</h6>
                             <div class="row">
                            <div class=" col-md-10" style="">
                                <div class="row">                                
                                <div class="col-md-6" style="">
                                    <img src="<?php echo base_url('public/theme/images/tuberia_conexiones/p/DURMAN.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>
                                </div>  
                               <div class="col-md-6" style="">
                                    <img src="<?php echo base_url('public/theme/images/tuberia_conexiones/p/AMANCO.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>
                                </div>  
                                
                                     
                                </div>
                            </div>
                           </div>                   
                    </div>
                 
                </div>
                <div class="row">
                    <div class="col-md-12" style="text-align:justify;">
                        <h4 class="mb-10">Cotizaciones o información de fichas técnicas</h4>
                         <fieldset></fieldset>
                        <form name="frm" id="frm" action="http://mrwasabiserver.com/webpage/ksh17/api-correo/index.php/api/correo_ksh" method="post">
                            <input type="hidden" name="tipo" id="tipo" value="PVC C-80" />
                            <div class="row pb-20">
                            <div class="col-md-3"> 
                                Nombre*:<br/>
                                <input type="text" name="nombre" id="nombre" placeholder="Nombre" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3"> 
                                Empresa*:<br>
                                <input type="text" name="empresa" id="empresa"  placeholder="Empresa" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3">
                                Teléfono*:<br>
                                <input type="text" name="telefono" id="telefono"  placeholder="Teléfono" class="input-md round form-control" onkeypress="return isNumberKey(event)" maxlength="10" /> 
                            </div>
                            <div class="col-md-3">
                                E-mail*:<br>
                                <input type="text" name="correo" id="correo"  placeholder="E-mail" class="input-md round form-control"/>
                            </div>
                            </div>
                            <div class="row pb-20">
                           <div class="col-md-3">
                                Ciudad*:<br>
                                <input type="text" name="ciudad" id="ciudad"  placeholder="Ciudad a la que pertences" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-9">
                                Mensaje*:<br>
                                <textarea name="mensaje" id="mensaje" style="width: 100%;height:60px;" placeholder="Mensaje." class="input-md round form-control"></textarea>
                            </div>
                            </div>
                            <div class="row pb-20">
                             <div class="col-md-12 align-right">
                                  <a href="javascript:EnviaProdContacto()" style="padding:10px 25px;font-size: 15px;color:#fff;background:#000;text-decoration: none;">ENVIAR</a>
                             </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>          

<?php } if($prod_id == 10){  ?>  
 <div class="row">
            <div class="col-md-3">
                <?php 
                $this->load->view('vistas/menu_productos');
                ?>
            </div>
            <div class="col-md-9" style="background: #fff;padding: 30px;">
                <div class="row">
                   <div class="col-md-4">
                       <img src="<?php echo base_url('public/theme/images/tuberia_conexiones/PVC_C40_.jpg') ?>" alt=""  style="width: 100%;"/>
                   </div>
                   
                    <div class="col-md-8">
                        <h6 class="titulo_producto">PVC C-40</h6>
                        <fieldset></fieldset>
                        <div class="section-text text-justify">
                   Fabricada con las más modernas técnicas de producción, para cumplir con las especificaciones establecidas en las normas mexicanas e internacionales. La cédula indica el espesor de la pared, esto sirve para saber cuál usar dependiendo de las presiones que se manejen, en el caso de la cedula 40 se usa para medianas. Los diámetros nominales van desde ½ hasta 14” (13 a 350 mm). Según norma NMX-E-224- VIGENTE, ASTM D-1785 Y ASTM D-2466.
                        </div>
                        
                         <div class="row mt-20">                             
                            <div class="section-text col-md-7 text-justify">
                      Esta línea de productos tiene su principal aplicación en la conducción de fluidos a mediana presión, tales como la distribución de agua potable, sistemas de riego agrícola y residencial.
                            </div>
                            <div class="section-text col-md-5">
                                <img src="<?php echo base_url('public/theme/images/tuberia_conexiones/PVC_C40_2.png') ?>" alt=""  style="width: 100%;" class=""/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row pt-40 pb-40">                    
                      <div class="col-md-4" style="text-align:justify;">
                     </div>                      
                    <div class="col-md-8">                      
                             <h6 class="titulo_producto" style="font-size: 16px;">Marca del Produto</h6>
                             <div class="row">
                            <div class=" col-md-10" style="">
                                <div class="row">                                
                                <div class="col-md-6" style="">
                                    <img src="<?php echo base_url('public/theme/images/tuberia_conexiones/p/DURMAN.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>
                                </div>  
                               <div class="col-md-6" style="">
                                    <img src="<?php echo base_url('public/theme/images/tuberia_conexiones/p/AMANCO.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>
                                </div>  
                                
                                     
                                </div>
                            </div>
                           </div>
                    
                    </div>
                 
                </div>
                <div class="row">
                    <div class="col-md-12" style="text-align:justify;">
                        <h4 class="mb-10">Cotizaciones o información de fichas técnicas</h4>
                         <fieldset></fieldset>
                        <form name="frm" id="frm" action="http://mrwasabiserver.com/webpage/ksh17/api-correo/index.php/api/correo_ksh" method="post">
                            <input type="hidden" name="tipo" id="tipo" value="PVC C-40" />
                            <div class="row pb-20">
                            <div class="col-md-3"> 
                                Nombre*:<br/>
                                <input type="text" name="nombre" id="nombre" placeholder="Nombre" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3"> 
                                Empresa*:<br>
                                <input type="text" name="empresa" id="empresa"  placeholder="Empresa" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3">
                                Teléfono*:<br>
                                <input type="text" name="telefono" id="telefono"  placeholder="Teléfono" class="input-md round form-control" onkeypress="return isNumberKey(event)" maxlength="10" /> 
                            </div>
                            <div class="col-md-3">
                                E-mail*:<br>
                                <input type="text" name="correo" id="correo"  placeholder="E-mail" class="input-md round form-control"/>
                            </div>
                            </div>
                            <div class="row pb-20">
                          <div class="col-md-3">
                                Ciudad*:<br>
                                <input type="text" name="ciudad" id="ciudad"  placeholder="Ciudad a la que pertences" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-9">
                                Mensaje*:<br>
                                <textarea name="mensaje" id="mensaje" style="width: 100%;height:60px;" placeholder="Mensaje." class="input-md round form-control"></textarea>
                            </div>
                            </div>
                            <div class="row pb-20">
                             <div class="col-md-12 align-right">
                                   <a href="javascript:EnviaProdContacto()" style="padding:10px 25px;font-size: 15px;color:#fff;background:#000;text-decoration: none;">ENVIAR</a>
                             </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>          
        
<?php } if($prod_id == 11){  ?>  
<div class="row">
            <div class="col-md-3">
                <?php 
                $this->load->view('vistas/menu_productos');
                ?>
            </div>
            <div class="col-md-9" style="background: #fff;padding: 30px;">
                <div class="row">
                   <div class="col-md-4">
                       <img src="<?php echo base_url('public/theme/images/tuberia_conexiones/polipropileno.jpg') ?>" alt=""  style="width: 100%;"/>
                   </div>
                   
                    <div class="col-md-8">
                        <h6 class="titulo_producto">Polipropileno</h6>
                        <fieldset></fieldset>
                        <div class="section-text text-justify">
                            <h6 class="titulo_producto" style="font-size: 16px;">¿Qué es el polipropileno?</h6>
                   El Polipropileno es un termoplástico que es obtenido por la polimerización del propileno, subproducto gaseoso de la refinación del petróleo, en presencia de un catalizador, bajo un cuidadoso control de temperatura y presión.
                        </div>
                        
                         <div class="row mt-20">                             
                            <div class="section-text col-md-7 text-justify">
                     El Polipropileno se puede clasificar en tres tipos: homopolímero, copolímero rándom y copolímero de alto impacto, los cuales pueden ser modificados y adaptados para determinados usos, a través de múltiples técnicas de aditivación. Históricamente el polipropileno ha sido una de las resinas de  mayor crecimiento, alcanzando hoy en día, el mayor consumo a nivel mundial entre todos los termoplásticos.
                            </div>
                            <div class="section-text col-md-5">
                                <img src="<?php echo base_url('public/theme/images/tuberia_conexiones/Polipropileno_dos.jpg') ?>" alt=""  style="width: 100%;" class=""/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row pt-40 pb-40">                    
                      <div class="col-md-4" style="text-align:justify;">                        
                             
                     </div>                      
                    <div class="col-md-8">  
                        <h6 class="titulo_producto" style="font-size: 16px;">Marca del Produto</h6>
                             <div class="row">
                            <div class=" col-md-10" style="">
                                <div class="row">                                
                                <div class="col-md-6" style="">
                                    <img src="<?php echo base_url('public/theme/images/tuberia_conexiones/p/URREA.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>
                                </div>  
                               <div class="col-md-6" style="">
                                    <img src="<?php echo base_url('public/theme/images/tuberia_conexiones/p/tuboplus.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>
                                </div>  
                                
                                     
                                </div>
                            </div>
                           </div>
<!--                        <div class="row">
                            <div class="col-md-6">
                           
                            </div>
                            <div class="col-md-6">
                           
                            </div>
                        </div>-->
                    </div>
                 
                </div>
                <div class="row">
                    <div class="col-md-12" style="text-align:justify;">
                        <h4 class="mb-10">Cotizaciones o información de fichas técnicas</h4>
                         <fieldset></fieldset>
                        <form name="frm" id="frm" action="http://mrwasabiserver.com/webpage/ksh17/api-correo/index.php/api/correo_ksh" method="post">
                            <input type="hidden" name="tipo" id="tipo" value="Polipropileno" />
                            <div class="row pb-20">
                            <div class="col-md-3"> 
                                Nombre*:<br/>
                                <input type="text" name="nombre" id="nombre" placeholder="Nombre" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3"> 
                                Empresa*:<br>
                                <input type="text" name="empresa" id="empresa"  placeholder="Empresa" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3">
                                Teléfono*:<br>
                                <input type="text" name="telefono" id="telefono"  placeholder="Teléfono" class="input-md round form-control" onkeypress="return isNumberKey(event)" maxlength="10" /> 
                            </div>
                            <div class="col-md-3">
                                E-mail*:<br>
                                <input type="text" name="correo" id="correo"  placeholder="E-mail" class="input-md round form-control"/>
                            </div>
                            </div>
                            <div class="row pb-20">
                           <div class="col-md-3">
                                Ciudad*:<br>
                                <input type="text" name="ciudad" id="ciudad"  placeholder="Ciudad a la que pertences" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-9">
                                Mensaje*:<br>
                                <textarea name="mensaje" id="mensaje" style="width: 100%;height:60px;" placeholder="Mensaje." class="input-md round form-control"></textarea>
                            </div>
                            </div>
                            <div class="row pb-20">
                             <div class="col-md-12 align-right">
                                   <a href="javascript:EnviaProdContacto()" style="padding:10px 25px;font-size: 15px;color:#fff;background:#000;text-decoration: none;">ENVIAR</a>
                             </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>             

<?php } if($prod_id == 12){  ?>  
<div class="row">
            <div class="col-md-3">
                <?php 
                $this->load->view('vistas/menu_productos');
                ?>
            </div>
            <div class="col-md-9" style="background: #fff;padding: 30px;">
                <div class="row">
                   <div class="col-md-4">
                       <img src="<?php echo base_url('public/theme/images/tuberia_conexiones/dwv_.jpg') ?>" alt=""  style="width: 100%;"/>
                   </div>
                   
                    <div class="col-md-8">
                        <h6 class="titulo_producto">DWV</h6>
                        <fieldset></fieldset>
                        <div class="section-text text-justify">
                     Este tipo de tubería es de PVC con núcleo de espuma, es usada en aplicaciones de drenaje sanitario, desperdicios y ventilación en donde no son presurizadas y donde la temperatura de operación no excede un rango, es más resistente y liviana al PVC normal.
                        </div>
                        
                         <div class="row mt-20">                             
                            <div class="section-text col-md-7 text-justify">
                  Es fabricada a partir de compuestos vírgenes rígidos de PVC como la identifica la Norma ASTM D 4396 y sus conexiones con la Norma ASTM D 1784.
                            </div>
                            <div class="section-text col-md-5">
                                <img src="<?php echo base_url('public/theme/images/tuberia_conexiones/dwv_dos.jpg') ?>" alt=""  style="width: 100%;" class=""/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row pt-40 pb-40">                    
                      <div class="col-md-4" style="text-align:justify;">                        
                            
                     </div>                      
                    <div class="col-md-8">  
                           <h6 class="titulo_producto" style="font-size: 16px;">Marca del Produto</h6>
                             <div class="row">
                            <div class=" col-md-10" style="">
                                <div class="row">                                
                                <div class="col-md-12" style="">
                                    <img src="<?php echo base_url('public/theme/images/tuberia_conexiones/p/Charlotte.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>
                                </div>  
                                                                
                                </div>
                            </div>
                           </div>
                    </div>
                 
                </div>
                <div class="row">
                    <div class="col-md-12" style="text-align:justify;">
                        <h4 class="mb-10">Cotizaciones o información de fichas técnicas</h4>
                         <fieldset></fieldset>
                        <form name="frm" id="frm" action="http://mrwasabiserver.com/webpage/ksh17/api-correo/index.php/api/correo_ksh" method="post">
                            <input type="hidden" name="tipo" id="tipo" value="DWV" />
                            <div class="row pb-20">
                            <div class="col-md-3"> 
                                Nombre*:<br/>
                                <input type="text" name="nombre" id="nombre" placeholder="Nombre" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3"> 
                                Empresa*:<br>
                                <input type="text" name="empresa" id="empresa"  placeholder="Empresa" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3">
                                Teléfono*:<br>
                                <input type="text" name="telefono" id="telefono"  placeholder="Teléfono" class="input-md round form-control" onkeypress="return isNumberKey(event)" maxlength="10" /> 
                            </div>
                            <div class="col-md-3">
                                E-mail*:<br>
                                <input type="text" name="correo" id="correo"  placeholder="E-mail" class="input-md round form-control"/>
                            </div>
                            </div>
                            <div class="row pb-20">
                            <div class="col-md-3">
                                Ciudad*:<br>
                                <input type="text" name="ciudad" id="ciudad"  placeholder="Ciudad a la que pertences" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-9">
                                Mensaje*:<br>
                                <textarea name="mensaje" id="mensaje" style="width: 100%;height:60px;" placeholder="Mensaje." class="input-md round form-control"></textarea>
                            </div>
                            </div>
                            <div class="row pb-20">
                             <div class="col-md-12 align-right">
                                   <a href="javascript:EnviaProdContacto()" style="padding:10px 25px;font-size: 15px;color:#fff;background:#000;text-decoration: none;">ENVIAR</a>
                             </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>              
        
<?php } ?>        
    </div>
</section>