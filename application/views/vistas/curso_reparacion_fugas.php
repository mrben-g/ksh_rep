<section class="page-section pt-20 pb-40">
    <div class="container relative">
        <!-- Intro Text -->
        <div class="row">
            <div class="col-md-12">
                <div class="section-text align-left">
                    <h3 class="tituloproductos1 mt-0 mb-0" style="">Reparación de Fugas</h3>
                    <div class="linea"></div>
                </div>
            </div>
        </div>
        <!-- End Intro Text -->
        <!--<div class="clearfix"></div>-->
        <div class="row" style="background: #fff;padding: 30px 10px;">
            <div class="col-md-12">
                
                <div class="col-md-6" ><img src="<?php echo base_url('public/theme/images/cursos/cero_fugas.jpg') ?>" style="width: 100%;"/></div>
                <div class=" col-md-6">
                    <div  class="section-text mt-140 mt-xs-20 text-center" >A medida que los sistemas de conducción y drenaje de las ciudades envejecen es necesario conocer las tecnologías más efectivas para la reparación y derivación de tubería.</div>                        
                </div>       
                
  <div class="row">                                 
<div class="col-md-12 pt-20 pb-20" >   
<p style="text-align:justify;color:#333;font-size: 1.5em;" class="mb-10">Mediante este curso práctico los participantes aprenderán a:</p>
<span class="section-text" style="display:block;">-Realizar una selección adecuada de productos de reparación de acuerdo al daño, material y diámetros de tubería.</span>
<span class="section-text" style="display:block;">-Preparar las herramientas y hacer una adecuada instalación de productos para la reparación y derivación, como son abrazaderas, uniones, tee-partidas, adaptadores bridados, y bell-joints.  Hacer uso del taladro vega.</span>
<span class="section-text" style="display:block;">-Realizar reparaciones en una línea presurizada en nuestras instalaciones.</span>
<span class="section-text" style="display:block;">-Compartir experiencias de casos difíciles de resolver.</span>
<br>
</div>
  </div>
                <div class="row">
<div class=" col-md-12 pb-20">   
<p style="text-align:justify;color:#333;font-size: 1.5em;"  class="mb-10">Dirigido a:</p>
<span class="section-text" style="display:block;">-Supervisores y Personal de campo de los Organismos Operadores de Agua</span>
<span class="section-text" style="display:block;">-Ingenieros interesados en conocer los productos comerciales en el área de reparación y derivación.</span>
<span class="section-text" style="display:block;">-Personal administrativo y de compras interesado en conocer la selección de productos y costos considerando todo el ciclo de vida del producto. </span>
<br>
</div>
                </div>
                
<div class="row  pb-20">
<div class=" col-md-6" >   
<p style="text-align:justify;color:#333;font-size: 1.5em;" class="mb-10">Lugar:</p>           
<span class="section-text" style="display:block;">Instalaciones de KSH ubicado en</span> 
<span class="section-text" style="display:block;"> Calle Alamo  #3040, Col. Moderna.</span>
<span class="section-text" style="display:block;">Monterrey, Nuevo León. México. CP: 6430. </span>
</div>                                 
<div class=" col-md-6 " >   
<p style="text-align:justify;color:#333;font-size: 1.5em;" class="mb-10">Costo:</p>           
<span class="section-text" style="display:block;margin-bottom: 10px;">El costo es GRATUITO en fechas asignadas. </span>
<span class="section-text" style="display:block;margin-bottom: 10px;">NO incluye traslados nacionales, locales, ni estancia en la ciudad (hoteles).</span>
</div>
</div>
<div class="row  pb-20">
<div class=" col-md-6">   
<p style="text-align:justify;color:#333;font-size: 1.5em;" class="mb-10">Duración:</p>           
<span class="section-text" style="display:block;margin-bottom: 10px;">8 horas – incluye 1 hora de comida cortesía.</span>
</div>
<div class=" col-md-6" >   
<p style="text-align:justify;color:#333;font-size: 1.5em;" class="mb-10">Cupo:</p>           
<span class="section-text" style="display:block;margin-bottom: 10px;">Cupo limitado a 15 personas.</span>
<span class="section-text" style="display:block;margin-bottom: 10px;">Nos reservamos derecho de registro a clientes.</span>
</div>
</div>
                <div class="row  pb-20">
<div class=" col-md-12">   
<p style="text-align:justify;color:#333;font-size: 1.5em;" class="mb-10">Fechas:</p>           
<span class="section-text" style="display:block;margin-bottom: 10px;">Contáctenos para  conocer próximas fechas o crear una fecha especial para su grupo. </span>
</div>
                     </div>
                    <div class="row pb-20">
                                 <div class=" col-md-4 mb-xs-20">   
                                   
                                     <img src="<?php echo base_url('public/theme/images/cursos/cero_fugas/invitacion_KSH_CERO_FUGAS2.jpg') ?>" alt=""  style="width: 100%;"/>
                                 </div>       
                                    
                                 <div class=" col-md-4 mb-xs-20">
                                         
                                     <img src="<?php echo base_url('public/theme/images/cursos/cero_fugas/curso_cerofugas01.jpg') ?>" alt="" style="width: 100%;"/>
                                 </div>
                                 <div class=" col-md-4"> 
                                          
                                     <img src="<?php echo base_url('public/theme/images/cursos/cero_fugas/curso_cerofugas02.jpg') ?>" alt="" style="width: 100%;"/>
                                 </div>
                    </div>
                    <div class="row pb-20">
                                 <div class=" col-md-4 mb-xs-20"> 
                                         
                                     <img src="<?php echo base_url('public/theme/images/cursos/cero_fugas/curso_cerofugas03.jpg') ?>" alt="" style="width: 100%;"/>
                                 </div>                                       
                                 <div class=" col-md-4 mb-xs-20"> 
                                      
                                     <img src="<?php echo base_url('public/theme/images/cursos/cero_fugas/curso_cerofugas04.jpg') ?>" alt="" style="width: 100%;"/>
                                 </div>
                                 <div class=" col-md-4 mb-xs-20">
                                       
                                     <img src="<?php echo base_url('public/theme/images/cursos/cero_fugas/curso_cerofugas05.jpg') ?>" alt="" style="width: 100%;"/>
                                 </div>
                </div>
           
      </div>
  </section>