<section class="page-section cart">
    <div class="items">
        <div class="container">
            <table style="color:#333;">
                <thead>
                    <tr>
                        <th id="h_img">&nbsp;</th>
                        <th>ARTÍCULO</th>
                        <th>CANTIDAD</th>
                        <th>PRECIO</th>
                        <th>SUBTOTAL</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                    $suma =0;$total_articulos=0;$cont_forms = 1;$hay_congelado=0;$ct="";$st="";
//                    if(isset($_SESSION['carro'])){
//                        foreach($carro as $sku => $value){
//                            $cant = 1;
//                            $subtotal = $value['cantidad']*$value['precio'];
//                            $suma += $subtotal;
//                            $total_articulos += $value['cantidad'];
                            ?>
<?php
//$cart_session = @$this->session->userdata('cart_session');
if(isset($_SESSION['carro'])){
$cart_session =  $_SESSION['carro'];
//var_dump($_SESSION['carro']);
                            if($cart_session){
                            $i = 0;
                            $total =0;
                           foreach($cart_session as $cs=>$value){
                             
                            $subtotal = $value['cantidad']*$value['precio'];
                            $suma += $subtotal;
                            $total_articulos += $value['cantidad'];
                            //$row = $this->product_model->product_detail($cs);
                            //$total += $row->product_price*$value;
?>
                    <tr id="tr<?php echo $cs;?>">
                        <td id="b_img">
                        <a href="#" class="pic">
                        <!--<img src="images/<?php echo $value['img_thumb'] ?>" alt="">-->
                        </a>
                        </td>
                        <td>
                            <h3 id="nom_prod"><?php 
                            echo $value['nombre'];
                            //echo $carro_shop['nombre']; 
                            ?></h3>
                            <span id="descrip" style="font-size:14px"><?php //echo utf8_encode($value['descripcion'])."..."  ?></span>
                            <div class="delete"><!--<a href="#" >Delete</a>--></div>
                        </td>
                        <td>
                            <!--<form id="frm<?php echo $cont_forms ?>" method="post" action="<?php echo base_url();?>/">-->
                                 <?php echo form_open_multipart(base_url('welcome/crea_sesion_producto'), array('id' => 'frm'.$cont_forms)) ?>
                                <div class="cart_quantity_button">
                                    <a class="cart_quantity_up" href="javascript:submit_form(<?php echo $cont_forms ?>,'mas')" style="<?php if(100 == $value['cantidad']){ ?>display: none;<?php } ?>text-decoration:none;"> + </a>
                                        <?php
                                        for($i=0;$i<100;$i++){
                                            if($value['cantidad'] == ($i+1)){ ?>
                                       <input class="cart_quantity_input" type="text" name="cantidad_agregar<?php echo $cont_forms ?>" id="cantidad_agregar<?php echo $cont_forms ?>" value="<?php echo $value['cantidad'] ?>" autocomplete="off" size="2" readonly="true">
                                        <?php
                                            }                                            
                                            } 
                                       ?>
                                    <a class="cart_quantity_down" href="javascript:submit_form(<?php echo $cont_forms ?>,'menos')" style="<?php if($value['cantidad'] == 1){?>display: none;<?php } ?>text-decoration:none;"> - </a>
                                </div>
                                <input type="hidden" name="skus" id="skus" value="<?php echo $cs ?>" />
                                <input type="hidden" name="id_ca" id="id_ca" value="<?php echo $cont_forms ?>" />
                                <input type="hidden" name="nprod" id="nprod" value="<?php echo $value['nombre'] ?>" />
                                <input type="hidden" name="prices" id="prices" value="<?php echo $value['precio'] ?>" />
                                 <input type="hidden" name="cantidad" id="cantidad" value="<?php echo $value['cantidad'] ?>" />
                                <input type="hidden" name="tipo_moneda" id="tipo_moneda" value="MXN" />
                                <!--<input name="images" type="hidden" id="images" value="<?php echo $value['img_thumb'] ?>" />-->
                                <input name="agregar_mas" type="hidden" id="agregar_mas" value="0"/>
                            </form>
                        </td>
                        <td class="price" >$<?php echo number_format($value['precio'], 2, '.', ' ') ?></td>
                        <td class="subtot" align="right" style="">$<?php echo number_format($subtotal, 2, '.', ' ');  ?>
                            <?php 
                            echo '<a href="'. base_url('welcome/elimina_producto') .'/'.$cs.'" ><img id="dele" src="'.base_url('public/theme').'/images/iconoX.png" alt="" title="Eliminar Producto"  /></a>';
                            ?>
                        </td>
                    </tr>
                        <?php
                        $cont_forms++;
                        
                                    }
                                    
                                    }  
}
                        ?>
                    
                    
                    
                </tbody>
            </table>
            <div class="clearfix">
                <ul class="payment-nav">
                    <li>Tarjetas participantes</li>
                    <li><a href="#"><img src="<?php echo base_url('public/theme') ?>/images/payment-nav/mc.png" width="27" height="17" alt="MasterCard"></a></li>
                    <li><a href="#"><img src="<?php echo base_url('public/theme') ?>/images/payment-nav/maestro.png" width="27" height="17" alt="Maestro"></a></li>
                    <li><a href="#"><img src="<?php echo base_url('public/theme') ?>/images/payment-nav/visa.png" width="27" height="17" alt="Visa"></a></li>
                    <li><a href="#"><img src="<?php echo base_url('public/theme') ?>/images/payment-nav/amex.png" width="16" height="17" alt="AmEx"></a></li>
                    <li>Nota: El cargo de envío es por cuenta del Cliente.</li>
                </ul>
                <div class="right-col">
                    <dl>
                        <?php //CARGO DE ENVIO
                        $en="0";
                        ?>
                        <dd></dd>
                        <dt>$<?php echo number_format($suma, 2, '.', ' ');  ?></dt>
                        <dd>Subtotal</dd>
<!--                       <dt>$<?php  echo number_format($en, 2, '.', ' ') ?></dt>
                            <dd>Cargo de Envio</dd>-->
                            <!-- <dt><?php $iva = ($en * 0.16); echo $iva ?></dt>
                            <dd>I.V.A.</dd>-->
                            <!--<dt class="discount">- $20</dt>
                            <dd>
                              <label class="input"><input type="text" placeholder="Enter coupon code here" /></label>
                            </dd>-->
                    </dl>
                    <div class="total">Costo Total: &nbsp;$<?php echo  number_format(($suma), 2, '.', ' '); ?></div>
                </div>
            </div>
            <footer>
                <a href="derivacion.php?action=1<?php //if($ct != ""){echo $value['categoria'];}else{echo 1;} ?>" class="button button-secondary" style="">Seguir Comprando</a>
                <a id="cancel" href="<?php echo base_url(); ?>/welcome/CancelaCompra" class="button button-secondary" style="">Cancelar Compra&nbsp</a>
                <a href="checkout.php" class="button button_cart">PAGAR</a>
            </footer>
        </div>
    </div>
</section>
<!--/ cart -->