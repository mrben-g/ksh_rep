<?php
//$prod_id = $this->input->get('id', TRUE);

$prod_id = $this->uri->segment(2);
?>
 
<section class="page-section pt-20 pb-40">
    <div class="container relative">
        <!-- Intro Text -->
        <div class="row">
            <div class="col-md-12">
                <div class="section-text align-left">
                    <h3 class="tituloproductos1 mt-0 mb-0">Válvulas y cajas de válvulas</h3>
                    <div class="linea"></div>
                </div>
            </div>
        </div>
        <!-- End Intro Text -->
        <!--<div class="clearfix"></div>-->
        
<?php if($prod_id == 1){  ?>          
        <div class="row">
            <div class="col-md-3">
                <?php 
                $this->load->view('vistas/menu_productos');
                ?>
            </div>
            <div class="col-md-9" style="background: #fff;padding: 30px;">
                <div class="row">
                   <div class="col-md-4 text-center">
                       <img src="<?php echo base_url('public/theme/images/productos/valvulas/valv_comp_sola.jpg') ?>" alt=""  style="width: 100%;max-width: 141px"/>
                   </div>
                    <div class="col-md-8" style="text-align:justify;">
                        <h6 class="titulo_producto">Válvulas compuerta</h6>
                        <fieldset></fieldset>
                        <div  class="section-text"  style="color:#000;">
                    La válvula de compuerta es un tipo de válvula que abre mediante el levantamiento de una compuerta permitiendo así el paso del fluido. Son de dos tipos, de vástago fijo y vástago saliente, a las de vástago fijo les llaman así porque su vástago permanece en su lugar al momento de hacer los giros para abrir, una de sus ventajas es que pueden utilizarse subterráneas y operarse desde un registro telescopiado así se protege su vástago del intemperie, las de vástago sobresaliente son llamadas así porque al abrirlas su vástago sube en cada giro, estas son usadas cuando la válvula tiene que estar a la vista, para saber de una manera rápida y sencilla si la válvula está abierta o cerrada, uno de sus principales usos es  para los equipos contra incendio.<br>
                    Una de sus principales distinciones es el tipo de sello, el cual se hace mediante el asiento del disco en dos áreas distribuidas en los contornos de ambas caras del disco. Las caras del disco pueden ser paralelas o en forma de cuña. Las válvulas de compuerta no son empleadas para regulación de un fluido.                    
                   
                        </div>
                        
                    </div>
                </div>
                <div class="row pt-40 pb-0">                    
                      <div class="col-md-4">                        
                           
                     </div>                      
                    <div class="col-md-8">
                        <h6 class="titulo_producto" style="font-size: 16px;">Ventajas</h6>
                        <div>
                            <ul id="ven" style="">
                                <li><span>Alta capacidad.</span></li>
                                <li><span>Cierre hermético.</span></li>
                                <li><span>Bajo costo.</span></li>
                                <li><span>Diseño y funcionamiento sencillos.</span></li>
                                <li><span>Poca resistencia a la circulación.</span></li>        
                            </ul>
                        </div>
                    </div>
                    
            </div>
                 <div class="row pt-0 pb-40">                    
                   <div class="col-md-4">                        
                           
                     </div>                      
                    <div class="col-md-8">          
                              <h6 class="titulo_producto" style="font-size: 16px;">Marca del Produto</h6>
                             <div class="row">
                            <div class=" col-md-12">
                                <div class=" col-md-12">
                                    <a href="pdf/valvulas/valvulascompuertadecierreelastico.pdf" target="_blank">Ficha Técnica</a>
                                </div>  
                                
                            </div>
                           </div>
                     </div>         
                 </div>
                <div class="row">
                    <div class="col-md-12" style="text-align:justify;">
                        <h4 class="mb-10">Cotizaciones o información de fichas técnicas</h4>
                         <fieldset></fieldset>
                        <form name="frm" id="frm" action="http://mrwasabiserver.com/webpage/ksh17/api-correo/index.php/api/correo_ksh" method="post">
                            <input type="hidden" name="tipo" id="tipo" value="Válvulas compuerta" />
                            <div class="row pb-20">
                            <div class="col-md-3"> 
                                Nombre*:<br/>
                                <input type="text" name="nombre" id="nombre" placeholder="Nombre" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3"> 
                                Empresa*:<br>
                                <input type="text" name="empresa" id="empresa"  placeholder="Empresa" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3">
                                Teléfono*:<br>
                                <input type="text" name="telefono" id="telefono"  placeholder="Teléfono" class="input-md round form-control" onkeypress="return isNumberKey(event)" maxlength="10"/> 
                            </div>
                            <div class="col-md-3">
                                E-mail*:<br>
                                <input type="text" name="correo" id="correo"  placeholder="E-mail" class="input-md round form-control"/>
                            </div>
                            </div>
                            <div class="row pb-20">
                            <div class="col-md-3">
                                Ciudad*:<br>
                                <input type="text" name="ciudad" id="ciudad"  placeholder="Ciudad a la que pertences" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-9">
                                Mensaje*:<br>
                                <textarea name="mensaje" id="mensaje" style="width: 100%;height:60px;" placeholder="Mensaje." class="input-md round form-control"></textarea>
                            </div>
                            </div>
                            <div class="row pb-20">
                             <div class="col-md-12 align-right">
                                    <a href="javascript:EnviaProdContacto()" style="padding:10px 25px;font-size: 15px;color:#fff;background:#000;text-decoration: none;">ENVIAR</a>
                             </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
<?php } if($prod_id == 2){  ?>  
   <div class="row">
            <div class="col-md-3">
                <?php 
                $this->load->view('vistas/menu_productos');
                ?>
            </div>
            <div class="col-md-9" style="background: #fff;padding: 30px;">
                <div class="row">
                   <div class="col-md-4">
                       <img src="<?php echo base_url('public/theme/images/productos/valvulas/valvula_mariposa.jpg') ?>" alt=""  style="width: 100%;"/>
                   </div>
                    <div class="col-md-8" style="text-align:justify;">
                        <h6 class="titulo_producto">Válvulas mariposa</h6>
                        <fieldset></fieldset>
                        <div  class="section-text"  style="color:#000;">
                           Las válvulas mariposa son usadas para  interrumpir un fluido en un conducto, es una válvula muy práctica ya que con media vuelta se cierra en comparación con la válvula compuerta que debes de dar barios giros al volante para cerrarla, se considera que para diámetros mayores a 10” es mejor una válvula mariposa por su peso y fácil manejo, se les denominan mariposa porque gira sobre un eje. Al disminuir el área de paso, aumenta la pérdida de carga local en la válvula, reduciendo el flujo.
                        </div>                            
                    </div>
                </div>
                <div class="row pt-40 pb-0">                    
                      <div class="col-md-4" >                        
                          
                     </div>                      
                    <div class="col-md-8">
                         <div class="row">
                            <div class="col-md-12">
                          <h6 class="titulo_producto" style="font-size: 16px;">Se pueden clasificar en tres tipos:</h6>
                        <div>
                            <ul id="ven" style="">
                                <li><span>Válvula mariposa bridada</span></li>
                                <li><span>Válvula mariposa con palanca</span></li>
                                <li><span>Válvula mariposa con reductor</span></li>
                            </ul>
                        </div>
                            </div>
                         </div>
                    </div>
                </div>
                <div class="row pt-0 pb-40">                    
                     <div class="col-md-4">                          
                     </div>
                      <div class="col-md-8">
                             <h6 class="titulo_producto" style="font-size: 16px;">Marca del Produto</h6>
                             <div class="row">
                            <div class=" col-md-12">
                                <div class=" col-md-4">
                                    <img src="<?php echo base_url('public/theme/images/productos/valvulas/p/LogoBray.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>     
                                </div>  
                                <div class="col-md-6">
                                    <a href="pdf/valvulas/valvula_con_reductor.pdf" target="_blank">Válvula con Reductor</a><br>
                                    <a href="pdf/valvulas/valvula_mariposa_bridada.pdf" target="_blank">Válvula mariposa bridada</a><br>
                                    <a href="pdf/valvulas/valvula_mariposa_con_palanca.pdf" target="_blank">Válvula mariposa palanca</a><br>
                                </div>   
                            </div>
                           </div>
                     </div>                
                </div>
                <div class="row">
                    <div class="col-md-12" style="text-align:justify;">
                        <h4 class="mb-10">Cotizaciones o información de fichas técnicas</h4>
                         <fieldset></fieldset>
                        <form name="frm" id="frm" action="http://mrwasabiserver.com/webpage/ksh17/api-correo/index.php/api/correo_ksh" method="post">
                            <input type="hidden" name="tipo" id="tipo" value="Válvulas mariposa" />
                            <div class="row pb-20">
                            <div class="col-md-3"> 
                                Nombre*:<br/>
                                <input type="text" name="nombre" id="nombre" placeholder="Nombre" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3"> 
                                Empresa*:<br>
                                <input type="text" name="empresa" id="empresa"  placeholder="Empresa" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3">
                                Teléfono*:<br>
                                <input type="text" name="telefono" id="telefono"  placeholder="Teléfono" class="input-md round form-control" onkeypress="return isNumberKey(event)" maxlength="10"/> 
                            </div>
                            <div class="col-md-3">
                                E-mail*:<br>
                                <input type="text" name="correo" id="correo"  placeholder="E-mail" class="input-md round form-control"/>
                            </div>
                            </div>
                            <div class="row pb-20">
                            <div class="col-md-3">
                                Ciudad*:<br>
                                <input type="text" name="ciudad" id="ciudad"  placeholder="Ciudad a la que pertences" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-9">
                                Mensaje*:<br>
                                <textarea name="mensaje" id="mensaje" style="width: 100%;height:60px;" placeholder="Mensaje." class="input-md round form-control"></textarea>
                            </div>
                            </div>
                            <div class="row pb-20">
                             <div class="col-md-12 align-right">
                                    <a href="javascript:EnviaProdContacto()" style="padding:10px 25px;font-size: 15px;color:#fff;background:#000;text-decoration: none;">ENVIAR</a>
                             </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>        

<?php } if($prod_id == 3){  ?>  
<div class="row">
            <div class="col-md-3">
                <?php 
                $this->load->view('vistas/menu_productos');
                ?>
            </div>
            <div class="col-md-9" style="background: #fff;padding: 30px;">
                <div class="row">
                   <div class="col-md-4 text-center">
                       <img src="<?php echo base_url('public/theme/images/productos/valvulas/valvula_cuchilla.jpg') ?>" alt=""  style="width: 100%;max-width: 160px;"/>
                   </div>
                   
                    <div class="col-md-8" style="text-align:justify;">
                        <h6 class="titulo_producto">Válvula cuchilla</h6>
                        <fieldset></fieldset>
                        <div  class="section-text"  style="color:#000;">
                            Las válvulas de cuchilla son similares a las válvulas compuerta ya que tienen en su interior una lámina parecida a la de las mariposas, solo que esta sube y baja con un vástago, en lugar de girar en un eje, una ventaja es que permite cortar el fluido de manera más sencilla. Las válvulas de cuchilla son diseñadas para operarlas en condiciones donde el fluido es muy alto y contiene  sólidos.<br><br>
                            Las aplicaciones más comunes de estas válvulas de cuchilla son en presas, minería donde el lodo es sumamente abrasivo. También pueden ser usadas en cementeras y en general como ya lo dijimos, donde el fluido es muy alto, espeso, viscoso, arenoso y con alto grado de sólidos.                           
                        </div>                            
                    </div>
                </div>
                <div class="row pt-40 pb-40">                    
                      <div class="col-md-4">                        
                             
                     </div>                      
                    <div class="col-md-8">                                             
                      <h6 class="titulo_producto" style="font-size: 16px;">Marca del Produto</h6>
                             <div class="row">
                                <div class=" col-md-12">
                                    <div class="row">
                                    <div class="col-md-12">
                                        <img src="<?php echo base_url('public/theme/images/productos/valvulas/p/LOGO_LVC.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>     
                                    </div>
                                    </div>
                                </div>
                           </div>                                        
                    </div>           
            </div>
                <div class="row">
                    <div class="col-md-12" style="text-align:justify;">
                        <h4 class="mb-10">Cotizaciones o información de fichas técnicas</h4>
                         <fieldset></fieldset>
                        <form name="frm" id="frm" action="http://mrwasabiserver.com/webpage/ksh17/api-correo/index.php/api/correo_ksh" method="post">
                            <input type="hidden" name="tipo" id="tipo" value="Válvula cuchilla" />
                            <div class="row pb-20">
                            <div class="col-md-3"> 
                                Nombre*:<br/>
                                <input type="text" name="nombre" id="nombre" placeholder="Nombre" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3"> 
                                Empresa*:<br>
                                <input type="text" name="empresa" id="empresa"  placeholder="Empresa" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3">
                                Teléfono*:<br>
                                <input type="text" name="telefono" id="telefono"  placeholder="Teléfono" class="input-md round form-control" onkeypress="return isNumberKey(event)" maxlength="10"/> 
                            </div>
                            <div class="col-md-3">
                                E-mail*:<br>
                                <input type="text" name="correo" id="correo"  placeholder="E-mail" class="input-md round form-control"/>
                            </div>
                            </div>
                            <div class="row pb-20">
                            <div class="col-md-3">
                                Ciudad*:<br>
                                <input type="text" name="ciudad" id="ciudad"  placeholder="Ciudad a la que pertences" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-9">
                                Mensaje*:<br>
                                <textarea name="mensaje" id="mensaje" style="width: 100%;height:60px;" placeholder="Mensaje." class="input-md round form-control"></textarea>
                            </div>
                            </div>
                            <div class="row pb-20">
                             <div class="col-md-12 align-right">
                                    <a href="javascript:EnviaProdContacto()" style="padding:10px 25px;font-size: 15px;color:#fff;background:#000;text-decoration: none;">ENVIAR</a>
                             </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>                

<?php } if($prod_id == 4){  ?>  
 <div class="row">
            <div class="col-md-3">
                <?php 
                $this->load->view('vistas/menu_productos');
                ?>
            </div>
            <div class="col-md-9" style="background: #fff;padding: 30px;">
                <div class="row">
                   <div class="col-md-4 text-center">
                       <img src="<?php echo base_url('public/theme/images/productos/valvulas/5401-AIR-RELESE-VALVE.jpg') ?>" alt=""  style="width: 100%;"/>
                   </div>
                   
                    <div class="col-md-8" style="text-align:justify;">
                        <h6 class="titulo_producto">Válvulas de expulsión y/o admisión de aire</h6>
                        <fieldset></fieldset>
                        <div  class="section-text"  style="color:#000;">
                            La válvula de Admisión y Expulsión de Aire están provistas de un flotador interno y un gran orificio de venteo para automáticamente expulsar y admitir aire al llenar o vaciar un sistema. Cuando la línea se llena el aire será sustituido por el líquido, las válvulas de Admisión y Expulsión de Aire colocadas en los puntos altos cerrarán y abrirán solo cuando la línea sea vaciada o la presión interna sea igual o menor a la atmosférica.<br><br>
                         
                        </div>                            
                    </div>
                </div>
                <div class="row pt-0 pb-40">                    
                      <div class="col-md-4">                        
                        
                     </div>                      
                    <div class="col-md-8">  
                        <div  class="section-text"  style="color:#000;">
                           Un buen ejemplo para el uso de este tipo de válvulas es cuando el suministro de agua en las poblaciones, no es constante, es decir opera a intervalos de tiempo, al cesar el flujo de agua en las líneas de conducción, provoca que exista un vacío en su interior, que viene a ser ocupado  por aire; así al renovarse el servicio, el agua empuja el aire entrampado hacia las tomas domiciliarias y éste es cuantificado en las lecturas de los medidores. Con este tipo de válvulas evitas el estampado de aire al medidor.<br><br>
                             Nota: <br> Las válvulas de Admisión y Expulsión de Aire no purgan aire cuando el sistema se encuentre en operación y bajo presión. 
                        </div>
                    </div>
                    
                </div>
                  <div class="row pt-0 pb-40">                    
                      <div class="col-md-4">                        
                        
                     </div>                      
                    <div class="col-md-8">                   
                             <h6 class="titulo_producto" style="font-size: 16px;">Marca del Produto</h6>                         
                                <div class="row">                                
                                <div class="col-md-4">
                                    <img src="<?php echo base_url('public/theme/images/productos/valvulas/p/89_logo_bermad.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>
                                </div>  
                                 <div class="col-md-4">
                                     <img src="<?php echo base_url('public/theme/images/productos/valvulas/p/logo_dorot.jpg') ?>" alt="" style="width: 100%;max-width: 162px;" class="mt-20"/>
                                </div>  
                                </div>                         
                     </div>                      
                  </div>
                <div class="row">
                    <div class="col-md-12" style="text-align:justify;">
                        <h4 class="mb-10">Cotizaciones o información de fichas técnicas</h4>
                         <fieldset></fieldset>
                        <form name="frm" id="frm" action="http://mrwasabiserver.com/webpage/ksh17/api-correo/index.php/api/correo_ksh" method="post">
                            <input type="hidden" name="tipo" id="tipo" value="Válvulas de expulsión y/o admisión de aire" />
                            <div class="row pb-20">
                            <div class="col-md-3"> 
                                Nombre*:<br/>
                                <input type="text" name="nombre" id="nombre" placeholder="Nombre" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3"> 
                                Empresa*:<br>
                                <input type="text" name="empresa" id="empresa"  placeholder="Empresa" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3">
                                Teléfono*:<br>
                                <input type="text" name="telefono" id="telefono"  placeholder="Teléfono" class="input-md round form-control" onkeypress="return isNumberKey(event)" maxlength="10"/> 
                            </div>
                            <div class="col-md-3">
                                E-mail*:<br>
                                <input type="text" name="correo" id="correo"  placeholder="E-mail" class="input-md round form-control"/>
                            </div>
                            </div>
                            <div class="row pb-20">
                            <div class="col-md-3">
                                Ciudad*:<br>
                                <input type="text" name="ciudad" id="ciudad"  placeholder="Ciudad a la que pertences" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-9">
                                Mensaje*:<br>
                                <textarea name="mensaje" id="mensaje" style="width: 100%;height:60px;" placeholder="Mensaje." class="input-md round form-control"></textarea>
                            </div>
                            </div>
                            <div class="row pb-20">
                             <div class="col-md-12 align-right">
                                   <a href="javascript:EnviaProdContacto()" style="padding:10px 25px;font-size: 15px;color:#fff;background:#000;text-decoration: none;">ENVIAR</a>
                             </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>                     

<?php } if($prod_id == 5){  ?>  
 <div class="row">
            <div class="col-md-3">
                <?php 
                $this->load->view('vistas/menu_productos');
                ?>
            </div>
            <div class="col-md-9" style="background: #fff;padding: 30px;">
                <div class="row">
                   <div class="col-md-4 text-center">
                       <img src="<?php echo base_url('public/theme/images/productos/valvulas/valvula_control.jpg') ?>" alt=""  style="width: 100%;"/>
                   </div>
                   
                    <div class="col-md-8" style="text-align:justify;">
                        <h6 class="titulo_producto">Válvulas de control</h6>
                        <fieldset></fieldset>
                        <div  class="section-text"  style="color:#000;">
                          Las válvulas de control se usan para regular la cantidad de flujo que pasa por una tubería. Existen diferentes tipos que se adaptan a las necesidades según las ocupes.<br>
                          Las válvulas de control automático cuentan con un motor para abrir o cerrar la válvula. Los motores reciben señales para ajustar el grado de apertura mediante una conexión a un sistema de control electrónico. En este caso, la válvula dispone de un actuador integrado que afecta al grado de apertura de la válvula. El actuador responde a un sensor, que mide la temperatura o la presión.
                        </div>                            
                    </div>
                </div>
                <div class="row pt-40 pb-40">                    
                      <div class="col-md-4" >                        
                             
                     </div>                      
                    <div class="col-md-8">  
                        <h6 class="titulo_producto" style="font-size: 16px;">Marca del Produto</h6>                             
                                <div class="row">                                
                                <div class="col-md-4" style="">
                                    <img src="<?php echo base_url('public/theme/images/productos/valvulas/p/89_logo_bermad.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>
                                </div>  
                                <div class="col-md-4" style="">
                                    <img src="<?php echo base_url('public/theme/images/productos/valvulas/p/logo_dorot.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>
                                </div> 
                                </div>                           
                    </div>
                 
                </div>
                <div class="row">
                    <div class="col-md-12" style="text-align:justify;">
                        <h4 class="mb-10">Cotizaciones o información de fichas técnicas</h4>
                         <fieldset></fieldset>
                        <form name="frm" id="frm" action="http://mrwasabiserver.com/webpage/ksh17/api-correo/index.php/api/correo_ksh" method="post">
                            <input type="hidden" name="tipo" id="tipo" value="Válvulas de control" />
                            <div class="row pb-20">
                            <div class="col-md-3"> 
                                Nombre*:<br/>
                                <input type="text" name="nombre" id="nombre" placeholder="Nombre" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3"> 
                                Empresa*:<br>
                                <input type="text" name="empresa" id="empresa"  placeholder="Empresa" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3">
                                Teléfono*:<br>
                                <input type="text" name="telefono" id="telefono"  placeholder="Teléfono" class="input-md round form-control" onkeypress="return isNumberKey(event)" maxlength="10"/> 
                            </div>
                            <div class="col-md-3">
                                E-mail*:<br>
                                <input type="text" name="correo" id="correo"  placeholder="E-mail" class="input-md round form-control"/>
                            </div>
                            </div>
                            <div class="row pb-20">
                           <div class="col-md-3">
                                Ciudad*:<br>
                                <input type="text" name="ciudad" id="ciudad"  placeholder="Ciudad a la que pertences" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-9">
                                Mensaje*:<br>
                                <textarea name="mensaje" id="mensaje" style="width: 100%;height:60px;" placeholder="Mensaje." class="input-md round form-control"></textarea>
                            </div>
                            </div>
                            <div class="row pb-20">
                             <div class="col-md-12 align-right">
                                        <a href="javascript:EnviaProdContacto()" style="padding:10px 25px;font-size: 15px;color:#fff;background:#000;text-decoration: none;">ENVIAR</a>
                             </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>                

<?php } if($prod_id == 6){  ?>  
  <div class="row">
            <div class="col-md-3">
                <?php 
                $this->load->view('vistas/menu_productos');
                ?>
            </div>
            <div class="col-md-9" style="background: #fff;padding: 30px;">
                <div class="row">
                   <div class="col-md-4 text-center">
                       <img src="<?php echo base_url('public/theme/images/productos/valvulas/valvula_check.jpg') ?>" alt=""  style="width: 100%;"/>
                   </div>                   
                    <div class="col-md-8">
                        <h6 class="titulo_producto">Válvulas check</h6>
                        <fieldset></fieldset>
                        <div class="section-text text-justify">
                              Las válvulas check, también llamadas antirretorno, tienen por objetivo de cerrar por completo el paso de un fluido en circulación bien sea gaseoso o líquido en un sentido y dejar paso libre en el contrario. Se utilizan cuando se pretende mantener a presión una tubería en servicio y poner en descarga la alimentación. El flujo del fluido que se dirige desde el orificio de entrada hacia el de utilización tiene el paso libre, mientras que en el sentido opuesto se encuentra bloqueado.<br>
                              Las válvulas antirretorno son ampliamente utilizadas en tuberías conectadas a sistemas de bombeo para evitar golpes de ariete, principalmente en la línea de descarga de la bomba.
                        </div>
                         
                    </div>
                </div>
                <div class="row pt-40 pb-40">                    
                      <div class="col-md-4">
                        
                     </div>                      
                    <div class="col-md-8">  
     <h6 class="titulo_producto" style="font-size: 16px;">Marca del Produto</h6>
                             
                                <div class="row">
                                <div class="col-md-4">
                                    <img src="<?php echo base_url('public/theme/images/productos/valvulas/p/89_logo_bermad.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>
                                </div>  
                               <div class="col-md-4">
                                    <img src="<?php echo base_url('public/theme/images/productos/valvulas/p/logo_dorot.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>
                                </div>  
                                    <div class="col-md-4">
                                        <img src="<?php echo base_url('public/theme/images/productos/valvulas/p/logo cla-val.jpg') ?>" alt="" style="width: 100%;max-width: 162px;" class="mt-30"/>
                                </div>
                                </div>
                        
                    </div>
                 
                </div>
                <div class="row">
                    <div class="col-md-12" style="text-align:justify;">
                        <h4 class="mb-10">Cotizaciones o información de fichas técnicas</h4>
                         <fieldset></fieldset>
                        <form name="frm" id="frm" action="http://mrwasabiserver.com/webpage/ksh17/api-correo/index.php/api/correo_ksh" method="post">
                            <input type="hidden" name="tipo" id="tipo" value="válvulas check" />
                            <div class="row pb-20">
                            <div class="col-md-3"> 
                                Nombre*:<br/>
                                <input type="text" name="nombre" id="nombre" placeholder="Nombre" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3"> 
                                Empresa*:<br>
                                <input type="text" name="empresa" id="empresa"  placeholder="Empresa" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3">
                                Teléfono*:<br>
                                <input type="text" name="telefono" id="telefono"  placeholder="Teléfono" class="input-md round form-control" onkeypress="return isNumberKey(event)" maxlength="10"/> 
                            </div>
                            <div class="col-md-3">
                                E-mail*:<br>
                                <input type="text" name="correo" id="correo"  placeholder="E-mail" class="input-md round form-control"/>
                            </div>
                            </div>
                            <div class="row pb-20">
                            <div class="col-md-3">
                                Ciudad*:<br>
                                <input type="text" name="ciudad" id="ciudad"  placeholder="Ciudad a la que pertences" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-9">
                                Mensaje*:<br>
                                <textarea name="mensaje" id="mensaje" style="width: 100%;height:60px;" placeholder="Mensaje." class="input-md round form-control"></textarea>
                            </div>
                            </div>
                            <div class="row pb-20">
                             <div class="col-md-12 align-right">
                                      <a href="javascript:EnviaProdContacto()" style="padding:10px 25px;font-size: 15px;color:#fff;background:#000;text-decoration: none;">ENVIAR</a>
                             </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>                

<?php } if($prod_id == 7){  ?>  
<div class="row">
            <div class="col-md-3">
                <?php 
                $this->load->view('vistas/menu_productos');
                ?>
            </div>
            <div class="col-md-9" style="background: #fff;padding: 30px;">
                <div class="row">
                   <div class="col-md-4 text-center">
                       <img src="<?php echo base_url('public/theme/images/productos/valvulas/valvula_plug.jpg') ?>" alt=""  style="width: 100%;"/>
                   </div>
                   
                    <div class="col-md-8">
                        <h6 class="titulo_producto">Válvulas plug</h6>
                        <fieldset></fieldset>
                        <div class="section-text text-justify">
                            Las válvulas plug poseen un dispositivo de cierre u obturador que está formado por una especie de tapón troncocónico el cual gira sobre el eje central. La apertura del obturador se efectúa girando sobre su propio eje, mediante una palanca, hasta hacer coincidir la ventana del mismo con los del cuerpo de la válvula. Su accionamiento suele ser muy rápido ya que al igual que la de mariposa basta un cuarto de vuelta de la palanca para pasar de la posición cerrada a la abierta y viceversa. Su pérdida de carga en posición abierta es muy pequeña y suelen emplearse en instalaciones poco vigiladas, ya que al colocarse sin palanca de accionamiento no se puede alterar su posición.
                        </div>                         
                    </div>
                </div>
                <div class="row pt-40 pb-40">                    
                      <div class="col-md-4" >                        
                     
                     </div>                      
                    <div class="col-md-8">  
                               <h6 class="titulo_producto" style="font-size: 16px;">Marca del Produto</h6>
                             <div class="row">
                            <div class=" col-md-12">
                                <div class="row">                                
                                <div class="col-md-12">
                                    <img src="<?php echo base_url('public/theme/images/productos/valvulas/p/logo_milliken.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>
                                </div>
                                </div>
                            </div>
                           </div>
                    </div>
                 
                </div>
                <div class="row">
                    <div class="col-md-12" style="text-align:justify;">
                        <h4 class="mb-10">Cotizaciones o información de fichas técnicas</h4>
                         <fieldset></fieldset>
                        <form name="frm" id="frm" action="http://mrwasabiserver.com/webpage/ksh17/api-correo/index.php/api/correo_ksh" method="post">
                            <input type="hidden" name="tipo" id="tipo" value="Válvulas plug" />
                            <div class="row pb-20">
                            <div class="col-md-3"> 
                                Nombre*:<br/>
                                <input type="text" name="nombre" id="nombre" placeholder="Nombre" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3"> 
                                Empresa*:<br>
                                <input type="text" name="empresa" id="empresa"  placeholder="Empresa" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3">
                                Teléfono*:<br>
                                <input type="text" name="telefono" id="telefono"  placeholder="Teléfono" class="input-md round form-control" onkeypress="return isNumberKey(event)" maxlength="10" /> 
                            </div>
                            <div class="col-md-3">
                                E-mail*:<br>
                                <input type="text" name="correo" id="correo"  placeholder="E-mail" class="input-md round form-control"/>
                            </div>
                            </div>
                            <div class="row pb-20">
                           <div class="col-md-3">
                                Ciudad*:<br>
                                <input type="text" name="ciudad" id="ciudad"  placeholder="Ciudad a la que pertences" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-9">
                                Mensaje*:<br>
                                <textarea name="mensaje" id="mensaje" style="width: 100%;height:60px;" placeholder="Mensaje." class="input-md round form-control"></textarea>
                            </div>
                            </div>
                            <div class="row pb-20">
                             <div class="col-md-12 align-right">
                                       <a href="javascript:EnviaProdContacto()" style="padding:10px 25px;font-size: 15px;color:#fff;background:#000;text-decoration: none;">ENVIAR</a>
                             </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>                    

<?php } if($prod_id == 8){  ?>  
<div class="row">
            <div class="col-md-3">
                <?php 
                $this->load->view('vistas/menu_productos');
                ?>
            </div>
            <div class="col-md-9" style="background: #fff;padding: 30px;">
                <div class="row">
                   <div class="col-md-4 text-center">
                       <img src="<?php echo base_url('public/theme/images/productos/valvulas/marco_con_tapa2.jpg') ?>" alt=""  style="width: 100%;"/>
                   </div>                   
                    <div class="col-md-8">
                        <h6 class="titulo_producto">Marco con tapa</h6>
                        <fieldset></fieldset>
                        <div class="section-text text-justify">
                           Los marcos con tapa son empleados para tapar las cajas de válvulas, y pueden ser de polipropileno, hierro dúctil y hierro fundido.
                        </div>                         
                    </div>
                </div>
                <div class="row pt-40 pb-40">                    
                      <div class="col-md-4">
                        
                     </div>                      
                    <div class="col-md-8">  
     <h6 class="titulo_producto" style="font-size: 16px;">Marca del Produto</h6>
                             <div class="row">
                            <div class=" col-md-12">
                                <div class="row">                                
                                <div class="col-md-4">
                                    <img src="<?php echo base_url('public/theme/images/productos/valvulas/p/INJAL_LOGO.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>
                                </div>  
                               <div class="col-md-4">
                                    <img src="<?php echo base_url('public/theme/images/productos/valvulas/p/LOGO_naresa.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>
                                </div>  
                                <div class="col-md-4">
                                    <img src="<?php echo base_url('public/theme/images/productos/valvulas/p/logo.jpg') ?>" alt="" style="width: 100%;max-width: 162px;" class="mt-30"/>
                                </div>  
                                     
                                </div>
                            </div>
                           </div>
                    </div>
                 
                </div>
                <div class="row">
                    <div class="col-md-12" style="text-align:justify;">
                        <h4 class="mb-10">Cotizaciones o información de fichas técnicas</h4>
                         <fieldset></fieldset>
                        <form name="frm" id="frm" action="http://mrwasabiserver.com/webpage/ksh17/api-correo/index.php/api/correo_ksh" method="post">
                            <input type="hidden" name="tipo" id="tipo" value="Marco con tapa" />
                            <div class="row pb-20">
                            <div class="col-md-3"> 
                                Nombre*:<br/>
                                <input type="text" name="nombre" id="nombre" placeholder="Nombre" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3"> 
                                Empresa*:<br>
                                <input type="text" name="empresa" id="empresa"  placeholder="Empresa" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3">
                                Teléfono*:<br>
                                <input type="text" name="telefono" id="telefono"  placeholder="Teléfono" class="input-md round form-control" onkeypress="return isNumberKey(event)" maxlength="10" /> 
                            </div>
                            <div class="col-md-3">
                                E-mail*:<br>
                                <input type="text" name="correo" id="correo"  placeholder="E-mail" class="input-md round form-control"/>
                            </div>
                            </div>
                            <div class="row pb-20">
                            <div class="col-md-3">
                                Ciudad*:<br>
                                <input type="text" name="ciudad" id="ciudad"  placeholder="Ciudad a la que pertences" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-9">
                                Mensaje*:<br>
                                <textarea name="mensaje" id="mensaje" style="width: 100%;height:60px;" placeholder="Mensaje." class="input-md round form-control"></textarea>
                            </div>
                            </div>
                            <div class="row pb-20">
                             <div class="col-md-12 align-right">
                                   <a href="javascript:EnviaProdContacto()" style="padding:10px 25px;font-size: 15px;color:#fff;background:#000;text-decoration: none;">ENVIAR</a>
                             </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>                           

<?php } if($prod_id == 9){  ?>  
<div class="row">
            <div class="col-md-3">
                <?php 
                $this->load->view('vistas/menu_productos');
                ?>
            </div>
            <div class="col-md-9" style="background: #fff;padding: 30px;">
                <div class="row">
                   <div class="col-md-4 text-center">
                       <img src="<?php echo base_url('public/theme/images/productos/valvulas/registro_telescopiado.png') ?>" alt=""  style="width: 100%;max-width: 90px;"/>
                   </div>
                   
                    <div class="col-md-8">
                        <h6 class="titulo_producto">Registro telescopiado</h6>
                        <fieldset></fieldset>
                        <div class="section-text text-justify">
                            Los registros telescopiados se usan  para la operación de una válvula, están diseñados para ajustarse desde el dado operador de la válvula al nivel del piso de la calle. Su diámetro interior permite operar la válvula libremente. El empaque de la tapa está diseñado para contener inclusiones de cuerpos extraños y logrando hermeticidad absoluta.
                        </div>                        
                    </div>
                </div>
                <div class="row pt-40 pb-40">                    
                      <div class="col-md-4">                        
                         
                     </div>                      
                    <div class="col-md-8">  
    <h6 class="titulo_producto" style="font-size: 16px;">Marca del Produto</h6>
                             <div class="row">
                            <div class=" col-md-12">
                                <div class="row">                                
                                <div class="col-md-12">
                                    <img src="<?php echo base_url('public/theme/images/productos/valvulas/p/logo_mueller_co.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>
                                </div> 
                                </div>
                            </div>
                           </div>
                    </div>
                 
                </div>
                <div class="row">
                    <div class="col-md-12" style="text-align:justify;">
                        <h4 class="mb-10">Cotizaciones o información de fichas técnicas</h4>
                         <fieldset></fieldset>
                        <form name="frm" id="frm" action="http://mrwasabiserver.com/webpage/ksh17/api-correo/index.php/api/correo_ksh" method="post">
                            <input type="hidden" name="tipo" id="tipo" value="Registro telescopiado" />
                            <div class="row pb-20">
                            <div class="col-md-3"> 
                                Nombre*:<br/>
                                <input type="text" name="nombre" id="nombre" placeholder="Nombre" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3"> 
                                Empresa*:<br>
                                <input type="text" name="empresa" id="empresa"  placeholder="Empresa" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3">
                                Teléfono*:<br>
                                <input type="text" name="telefono" id="telefono"  placeholder="Teléfono" class="input-md round form-control" onkeypress="return isNumberKey(event)" maxlength="10" /> 
                            </div>
                            <div class="col-md-3">
                                E-mail*:<br>
                                <input type="text" name="correo" id="correo"  placeholder="E-mail" class="input-md round form-control"/>
                            </div>
                            </div>
                            <div class="row pb-20">
                            <div class="col-md-3">
                                Ciudad*:<br>
                                <input type="text" name="ciudad" id="ciudad"  placeholder="Ciudad a la que pertences" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-9">
                                Mensaje*:<br>
                                <textarea name="mensaje" id="mensaje" style="width: 100%;height:60px;" placeholder="Mensaje." class="input-md round form-control"></textarea>
                            </div>
                            </div>
                            <div class="row pb-20">
                             <div class="col-md-12 align-right">
                                   <a href="javascript:EnviaProdContacto()" style="padding:10px 25px;font-size: 15px;color:#fff;background:#000;text-decoration: none;">ENVIAR</a>
                             </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>          

<?php } if($prod_id == 10){  ?>  
 <div class="row">
            <div class="col-md-3">
                <?php 
                $this->load->view('vistas/menu_productos');
                ?>
            </div>
            <div class="col-md-9" style="background: #fff;padding: 30px;">
                <div class="row">
                   <div class="col-md-4 text-center">
                       <!--<img src="<?php // echo base_url('public/theme/images/productos/valvulas/PVC_C40_.jpg') ?>" alt=""  style="width: 100%;"/>-->
                   </div>
                   
                    <div class="col-md-8">
                        <h6 class="titulo_producto">Tapa superior y campana</h6>
                        <fieldset></fieldset>
                        <div class="section-text text-justify">
                
                        </div>
                    </div>
                </div>
                <div class="row pt-40 pb-40">                    
                      <div class="col-md-4" style="text-align:justify;">                        
                             <h6 class="titulo_producto" style="font-size: 16px;">Marca del Produto</h6>
                             <div class="row">
                            <div class=" col-md-10" style="">
                                <div class="row">                                
                                <div class="col-md-6" style="">
                                    <!--<img src="<?php // echo base_url('public/theme/images/productos/valvulas/p/DURMAN.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>-->
                                </div>  
                               <div class="col-md-6" style="">
                                    <!--<img src="<?php // echo base_url('public/theme/images/productos/valvulas/p/AMANCO.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>-->
                                </div>  
                                
                                     
                                </div>
                            </div>
                           </div>
                     </div>                      
                    <div class="col-md-8">  
<!--                        <div class="row">
                            <div class="col-md-6">
                           
                            </div>
                            <div class="col-md-6">
                           
                            </div>
                        </div>-->
                    </div>
                 
                </div>
                <div class="row">
                    <div class="col-md-12" style="text-align:justify;">
                        <h4 class="mb-10">Cotizaciones o información de fichas técnicas</h4>
                         <fieldset></fieldset>
                        <form name="frm" id="frm" action="http://mrwasabiserver.com/webpage/ksh17/api-correo/index.php/api/correo_ksh" method="post">
                            <input type="hidden" name="tipo" id="tipo" value="Tapa superior y campana" />
                            <div class="row pb-20">
                            <div class="col-md-3"> 
                                Nombre*:<br/>
                                <input type="text" name="nombre" id="nombre" placeholder="Nombre" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3"> 
                                Empresa*:<br>
                                <input type="text" name="empresa" id="empresa"  placeholder="Empresa" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3">
                                Teléfono*:<br>
                                <input type="text" name="telefono" id="telefono"  placeholder="Teléfono" class="input-md round form-control" onkeypress="return isNumberKey(event)" maxlength="10" /> 
                            </div>
                            <div class="col-md-3">
                                E-mail*:<br>
                                <input type="text" name="correo" id="correo"  placeholder="E-mail" class="input-md round form-control"/>
                            </div>
                            </div>
                            <div class="row pb-20">
                            <div class="col-md-3">
                                Ciudad*:<br>
                                <input type="text" name="ciudad" id="ciudad"  placeholder="Ciudad a la que pertences" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-9">
                                Mensaje*:<br>
                                <textarea name="mensaje" id="mensaje" style="width: 100%;height:60px;" placeholder="Mensaje." class="input-md round form-control"></textarea>
                            </div>
                            </div>
                            <div class="row pb-20">
                             <div class="col-md-12 align-right">
                                   <a href="javascript:EnviaProdContacto()" style="padding:10px 25px;font-size: 15px;color:#fff;background:#000;text-decoration: none;">ENVIAR</a>
                             </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>          
        
<?php } if($prod_id == 11){  ?>  
<div class="row">
            <div class="col-md-3">
                <?php 
                $this->load->view('vistas/menu_productos');
                ?>
            </div>
            <div class="col-md-9" style="background: #fff;padding: 30px;">
                <div class="row">
                   <div class="col-md-4 text-center">
                       <img src="<?php echo base_url('public/theme/images/productos/valvulas/valvula_blackflow.jpg') ?>" alt=""  style="width: 100%;"/>
                   </div>                   
                    <div class="col-md-8">
                        <h6 class="titulo_producto">Válvulas backflow</h6>
                        <fieldset></fieldset>
                        <div class="section-text text-justify">
                           Es una válvula  de retención de doble ensamble esta diseñada para proteger los suministros de agua potable y proporcionar un cierre integral. Se trata de la construcción de un cuerpo de aleación de bronce y elastómeros resistentes al cloro. La serie 719 cuenta con un acceso independiente, el diseño de la válvula facilita el mantenimiento y es para el uso de conexiones cruzadas en aplicaciones de riesgo no sanitarios.
                        </div>
                    </div>
                </div>
                <div class="row pt-40 pb-40">                    
                      <div class="col-md-4">                        
                             
                     </div>                      
                    <div class="col-md-8">  
<h6 class="titulo_producto" style="font-size: 16px;">Marca del Produto</h6>
                             <div class="row">
                            <div class=" col-md-12" >
                                <div class="row">                                
                                <div class="col-md-12">
                                    <img src="<?php echo base_url('public/theme/images/productos/valvulas/p/logo_watts.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>
                                </div>  
                            </div>
                            </div>
                           </div>
                    </div>
                 
                </div>
                <div class="row">
                    <div class="col-md-12" style="text-align:justify;">
                        <h4 class="mb-10">Cotizaciones o información de fichas técnicas</h4>
                         <fieldset></fieldset>
                        <form name="frm" id="frm" action="http://mrwasabiserver.com/webpage/ksh17/api-correo/index.php/api/correo_ksh" method="post">
                            <input type="hidden" name="tipo" id="tipo" value="Válvulas backflow" />
                            <div class="row pb-20">
                            <div class="col-md-3"> 
                                Nombre*:<br/>
                                <input type="text" name="nombre" id="nombre" placeholder="Nombre" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3"> 
                                Empresa*:<br>
                                <input type="text" name="empresa" id="empresa"  placeholder="Empresa" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3">
                                Teléfono*:<br>
                                <input type="text" name="telefono" id="telefono"  placeholder="Teléfono" class="input-md round form-control" onkeypress="return isNumberKey(event)" maxlength="10"/> 
                            </div>
                            <div class="col-md-3">
                                E-mail*:<br>
                                <input type="text" name="correo" id="correo"  placeholder="E-mail" class="input-md round form-control"/>
                            </div>
                            </div>
                            <div class="row pb-20">
                           <div class="col-md-3">
                                Ciudad*:<br>
                                <input type="text" name="ciudad" id="ciudad"  placeholder="Ciudad a la que pertences" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-9">
                                Mensaje*:<br>
                                <textarea name="mensaje" id="mensaje" style="width: 100%;height:60px;" placeholder="Mensaje." class="input-md round form-control"></textarea>
                            </div>
                            </div>
                            <div class="row pb-20">
                             <div class="col-md-12 align-right">
                                   <a href="javascript:EnviaProdContacto()" style="padding:10px 25px;font-size: 15px;color:#fff;background:#000;text-decoration: none;">ENVIAR</a>
                             </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>             

<?php } ?>        
    </div>
</section>