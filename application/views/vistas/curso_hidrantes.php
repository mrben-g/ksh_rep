<section class="page-section pt-20 pb-40">
    <div class="container relative">
        <!-- Intro Text -->
        <div class="row">
            <div class="col-md-12">
                <div class="section-text align-left">
                    <h3 class="tituloproductos1 mt-0 mb-0" style="">Hidratantes y Equipo contra Incendio</h3>
                    <div class="linea"></div>
                </div>
            </div>
        </div>
        <!-- End Intro Text -->
        <!--<div class="clearfix"></div>-->
        <div class="row" style="background: #fff;padding: 30px 10px;">
            <div class="col-md-12">
                <div class="row pb-20">
                    <div class="col-md-6">
                        <img src="<?php echo base_url('public/theme/images/cursos/hidratantes/hidratantes.png') ?>" alt=""  style="width: 100%;"/>
                    </div>                       
                     <div class="col-md-6 text-center">
                         <div class="section-text mt-140 mt-xs-20 text-center" style="color:#333;">Los hidrantes salvan vidas.Instalar, reparar y dar mantenimiento correcto a los Hidrantes es de vital importancia.<br> ¡Nosotros te enseñamos!</div>
                     </div>
                </div>
                <div class="row pb-20">
                     <div class=" col-md-12">
                         <p style="text-align:justify;color:#333;font-size: 1.5em;" class="mb-10">Mediante este curso práctico los participantes aprenderán a:</p>
                         <span class="section-text" style="display:block;">- Conocer cómo funcionan los Hidrantes de Barril Seco marca Mueller y todos sus componentes.</span>
                         <span class="section-text" style="display:block;">- Preparar las herramientas y hacer una adecuada instalación.</span>
                         <span class="section-text" style="display:block;">- Realizar una reparación por choque.</span>
                         <span class="section-text" style="display:block;">- Realizar un mantenimiento e inspección a la válvulas principal.</span>
                         <span class="section-text" style="display:block;">- Realizar una extensión de la longitud de barril.</span>
                         <span class="section-text" style="display:block;">- Compartir experiencias de casos.</span>
                     </div>
                </div>
                <div class="row pb-20">
                    <div class=" col-md-12">
                        <p style="text-align:justify;color:#333;font-size: 1.5em;"  class="mb-10">Dirigido a:</p>
                        <span class="section-text" style="display:block;">- Instaladores de hidrantes o equipo contra incendio.</span>
                        <span class="section-text" style="display:block;">- Supervisores y Personal de campo de los Organismos Operadores de Agua</span>
                        <span class="section-text" style="display:block;">- Ingenieros interesados en conocer los productos comerciales en el área contra incendios.</span>
                        <span class="section-text" style="display:block;">- Personal administrativo y de compras interesado en conocer la selección de productos y componentes, kits de reparación y repuestos. </span>
                    </div>
                </div>
                <div class="row pb-20">
                    <div class="col-md-6">
                        <p style="text-align:justify;color:#333;font-size: 1.5em;" class="mb-10">Lugar:</p>
                        <span class="section-text" style="display:block;">Instalaciones de KSH ubicado en</span>
                        <span class="section-text" style="display:block;">Calle Alamo  #3040, Col. Moderna.</span>
                        <span class="section-text" style="display:block;">Monterrey, Nuevo León. México. CP: 6430. </span>
                    </div>
                    <div class="col-md-6">
                        <p style="text-align:justify;color:#333;font-size: 1.5em;" class="mb-10">Costo:</p>
                        <span class="section-text" style="display:block;">El costo es GRATUITO en fechas asignadas. </span>
                        <span class="section-text" style="display:block;">NO incluye traslados nacionales, locales, ni estancia en la ciudad (hoteles).</span>
                    </div>
                </div>
                <div class="row pb-20">
                    <div class="col-md-6">
                        <p style="text-align:justify;color:#333;font-size: 1.5em;" class="mb-10">Duración:</p>
                        <span class="section-text" style="display:block;">5 horas – incluye 1 hora de comida cortesía.</span>
                    </div>
                    <div class=" col-md-6">
                        <p style="text-align:justify;color:#333;font-size: 1.5em;" class="mb-10">Cupo:</p>
                        <span class="section-text" style="display:block;">Cupo limitado a 15 personas.</span>
                        <span class="section-text" style="display:block;">Nos reservamos derecho de registro a clientes.</span>
                    </div>                    
                </div>
                <div class="row pb-20">
                    <div class=" col-md-12">
                        <p style="text-align:justify;color:#333;font-size: 1.5em;" class="mb-10">Fechas:</p>
                        <span class="section-text" style="display:block;">Contáctenos para  conocer próximas fechas o crear una fecha especial para su grupo. </span>
                    </div>
                </div>
                   <div class="row pb-20">
                       <div class=" col-md-4 mb-xs-20">                     
                               <img src="<?php echo base_url('public/theme/images/cursos/hidratantes/IMG_0734.JPG') ?>" alt="" style="width: 100%;"/>                          
                       </div>
                       <div class=" col-md-4 mb-xs-20">                   
                               <img src="<?php echo base_url('public/theme/images/cursos/hidratantes/IMG_0765.JPG') ?>" alt="" style="width: 100%;"/>                          
                       </div>
                       <div class=" col-md-4">                      
                               <img src="<?php echo base_url('public/theme/images/cursos/hidratantes/IMG_0804.JPG') ?>" alt="" style="width: 100%;"/>                         
                       </div>
                   </div>
                <div class="row pb-20">
                       <div class=" col-md-4 mb-xs-20">                         
                               <img src="<?php echo base_url('public/theme/images/cursos/hidratantes/IMG_0826.JPG') ?>" alt="" style="width: 100%;"/>                           
                       </div>
                       <div class=" col-md-4">                      
                               <img src="<?php echo base_url('public/theme/images/cursos/hidratantes/IMG_0846.JPG') ?>" alt="" style="width: 100%;"/>                        
                       </div>
                   </div>
                </div>
            </div>
      </div>
  </section>