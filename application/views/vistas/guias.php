<?php
$prod_id = $this->input->get('id', TRUE);
?>
<style type="text/css">
    
/*REPARACION DE FUGAS*/    
 .uno2{
     max-width: 285px;width: 97%;position: absolute;right:26%;top: 16%;z-index: 1;
     background: url(<?php echo base_url('public/theme/images/guias/prod/tuberia/4.png') ?>);background-size: cover;height: 131px;
 }
 .uno2:hover{background: url(<?php echo base_url('public/theme/images/guias/prod/tuberia/4o.png') ?>);background-size: cover;max-width: 285px;height: 131px;}
 
 .dos2{
     max-width: 167px;width: 100%;position: absolute;right: 19.9%;bottom: 9%;z-index: 2;
     background: url(<?php echo base_url('public/theme/images/guias/prod/tuberia/3.png') ?>);background-size: cover;height: 208px;
 }
 .dos2:hover{background: url(<?php echo base_url('public/theme/images/guias/prod/tuberia/3o.png') ?>);background-size: cover;max-width: 167px;height: 208px;}   
 
 .tres2{
     max-width: 128px;width: 100%;position: absolute;left: 44.2%;bottom: 5%;z-index: 5;
     background: url(<?php echo base_url('public/theme/images/guias/prod/tuberia/2.png') ?>);background-size: cover;height: 221px;
 }
 .tres2:hover{background: url(<?php echo base_url('public/theme/images/guias/prod/tuberia/2o.png') ?>);background-size: cover;max-width: 128px;height: 221px;}
 
 .cuatro2{
     max-width: 148px;width: 100%;position: absolute;left: 23.3%;bottom: 3%;z-index: 4;
     background: url(<?php echo base_url('public/theme/images/guias/prod/tuberia/1.png') ?>);background-size: cover;height: 242px;
 }
 .cuatro2:hover{background: url(<?php echo base_url('public/theme/images/guias/prod/tuberia/1o.png') ?>);background-size: cover;max-width: 148px;height: 242px;}    
    
/*CONDUCCION DE AGUA*/
.uno {
     max-width: 200px;width: 100%;position: absolute;left: 16%;top:22%;z-index: 1;
     background: url(<?php echo base_url('public/theme/images/guias/prod/conduccion_agua/1.png') ?>);background-size: cover;height: 120px;
 }
 .uno:hover { background: url(<?php echo base_url('public/theme/images/guias/prod/conduccion_agua/1_over.png') ?>);background-size: cover;max-width: 200px;height: 120px; }
.dos {
    max-width: 178px;width: 100%;position: absolute;left:  12.5%;top: 39.7%;z-index: 2;
    background: url(<?php echo base_url('public/theme/images/guias/prod/conduccion_agua/2.png') ?>);background-size: cover;height: 126px;
}
.dos:hover {background: url(<?php echo base_url('public/theme/images/guias/prod/conduccion_agua/2_over.png') ?>);background-size: cover;max-width: 178px;height: 126px;}

.tres {
    max-width: 180px;width: 100%;position: absolute;left: 13%;bottom: 28%;z-index: 5;
    background: url(<?php echo base_url('public/theme/images/guias/prod/conduccion_agua/3.png') ?>);background-size: cover;height: 152px;
}
.tres:hover { background: url(<?php echo base_url('public/theme/images/guias/prod/conduccion_agua/3_over.png') ?>);background-size: cover;max-width: 180px;height: 152px; }    
.cuatro{
    max-width: 200px;width: 100%;position: absolute;left: 13%;bottom: 9%;z-index: 4;
    background: url(<?php echo base_url('public/theme/images/guias/prod/conduccion_agua/4.png') ?>);background-size: cover;height: 266px;
}
.cuatro:hover { background: url(<?php echo base_url('public/theme/images/guias/prod/conduccion_agua/4_over.png') ?>);background-size: cover;max-width: 200px;height: 266px; }    

.cinco{
    max-width: 123px;width: 100%;position: absolute;left: 24%;bottom: 16%;z-index: 5;
    background: url(<?php echo base_url('public/theme/images/guias/prod/conduccion_agua/5.png') ?>);background-size: cover;height: 219px;
}
.cinco:hover { background: url(<?php echo base_url('public/theme/images/guias/prod/conduccion_agua/5_over.png') ?>);background-size: cover;max-width: 123px;height: 219px; }

.seis{
    max-width: 99px;width: 100%;position: absolute;left: 32.5%;bottom: 19%;z-index: 6;
    background: url(<?php echo base_url('public/theme/images/guias/prod/conduccion_agua/6.png') ?>);background-size: cover;height: 195px;
}
.seis:hover { background: url(<?php echo base_url('public/theme/images/guias/prod/conduccion_agua/6_over.png') ?>);background-size: cover;max-width: 99px;height: 195px; }

.siete{ 
    max-width: 162px;width: 100%;position: absolute;left: 40.3%;bottom: 11%;z-index: 7;
    background: url(<?php echo base_url('public/theme/images/guias/prod/conduccion_agua/7.png') ?>);background-size: cover;height: 312px;
}
.siete:hover{ background: url(<?php echo base_url('public/theme/images/guias/prod/conduccion_agua/7_over.png') ?>);background-size: cover;max-width: 162px;height: 312px; }

.ocho {
    max-width: 120px;width: 100%; position: absolute;right: 40.2%;bottom: 10%;z-index: 8;
    background: url(<?php echo base_url('public/theme/images/guias/prod/conduccion_agua/8.png') ?>);background-size: cover;height: 262px;
}
.ocho:hover {background: url(<?php echo base_url('public/theme/images/guias/prod/conduccion_agua/8_over.png') ?>);background-size: cover;max-width: 120px;height: 262px; }

.nueve{
    max-width: 144px;width: 100%;position: absolute;right: 31%;bottom: 14%;z-index:16;
    background: url(<?php echo base_url('public/theme/images/guias/prod/conduccion_agua/9.png') ?>);background-size: cover;height: 245px;
}
.nueve:hover{ background: url(<?php echo base_url('public/theme/images/guias/prod/conduccion_agua/9_over.png') ?>);background-size: cover;max-width: 144px;height: 245px; }

.diez{
    max-width: 225px;width: 100%;position: absolute;right: 22.5%;bottom: 13%;z-index: 12;
    background: url(<?php echo base_url('public/theme/images/guias/prod/conduccion_agua/10.png') ?>);background-size: cover;height: 246px;
}
.diez:hover{ background: url(<?php echo base_url('public/theme/images/guias/prod/conduccion_agua/10_over.png') ?>);background-size: cover;max-width: 225px;height: 246px; }

.once{ 
    max-width: 292px;width: 100%;position: absolute;right: 13.5%;bottom: 4%;z-index: 11;
    background: url(<?php echo base_url('public/theme/images/guias/prod/conduccion_agua/11.png') ?>);background-size: cover;height: 318px;
}
.once:hover{ background: url(<?php echo base_url('public/theme/images/guias/prod/conduccion_agua/11_over.png') ?>);background-size: cover;max-width: 292px;height: 318px; }

.doce {
    max-width: 109px;width: 100%;position: absolute;right: 14%;  top: 29%;z-index: 14;
    background: url(<?php echo base_url('public/theme/images/guias/prod/conduccion_agua/12.png') ?>);background-size: cover;height: 157px;
}
.doce:hover {background: url(<?php echo base_url('public/theme/images/guias/prod/conduccion_agua/12_over.png') ?>);background-size: cover;max-width: 109px;height: 157px; }

.trece{
    max-width: 222px;width: 100%;position: absolute;right: 13%;top: 6%;z-index: 0;
    background: url(<?php echo base_url('public/theme/images/guias/prod/conduccion_agua/13.png') ?>);background-size: cover;height: 333px;
}
.trece:hover{ background: url(<?php echo base_url('public/theme/images/guias/prod/conduccion_agua/13_over.png') ?>);background-size: cover;max-width: 222px;height: 333px;}

/*ALCANTARILLADO*/
.siete1{ 
    max-width: 291px;width: 100%;position: absolute;right: 12.3%;top: 15%;z-index: 7;
    background: url(<?php echo base_url('public/theme/images/guias/prod/alcantarillado/7.png') ?>);background-size: cover;height: 144px;
}
.siete1:hover{ background: url(<?php echo base_url('public/theme/images/guias/prod/alcantarillado/7over.png') ?>);background-size: cover;max-width: 291px;height: 144px; }

.uno1{
    max-width: 130px;width: 100%;position: absolute;left:19%;bottom: 10%;z-index: 1;
    background: url(<?php echo base_url('public/theme/images/guias/prod/alcantarillado/1.png') ?>);background-size: cover;height: 282px;
}
.uno1:hover{ background: url(<?php echo base_url('public/theme/images/guias/prod/alcantarillado/1over.png') ?>);background-size: cover;max-width: 130px;height: 282px; } 

.dos1{
    max-width: 123px;width: 100%;position: absolute;left: 31.3%;bottom: 9.7%;z-index: 2;
    background: url(<?php echo base_url('public/theme/images/guias/prod/alcantarillado/2.png') ?>);background-size: cover;height: 282px; }
.dos1:hover{ background: url(<?php echo base_url('public/theme/images/guias/prod/alcantarillado/2over.png') ?>);background-size: cover;max-width: 123px;height:  282px; }

.tres1 {max-width: 132px;width: 100%;position: absolute;left: 42.6%;bottom:13%;z-index: 5;
        background: url(<?php echo base_url('public/theme/images/guias/prod/alcantarillado/3.png') ?>);background-size: cover;height: 257px; }
.tres1:hover{ background: url(<?php echo base_url('public/theme/images/guias/prod/alcantarillado/3over.png') ?>);background-size: cover;max-width: 132px;height: 257px; }

.cuatro1{
    max-width: 184px;width: 100%;position: absolute;left: 46.3%;bottom: 16%;z-index: 4;
    background: url(<?php echo base_url('public/theme/images/guias/prod/alcantarillado/4.png') ?>);background-size: cover;height: 306px;
}
.cuatro1:hover{ background: url(<?php echo base_url('public/theme/images/guias/prod/alcantarillado/4over.png') ?>);background-size: cover;max-width:184px;height: 306px;}

.cinco1{
    max-width: 172px;width: 100%;position: absolute;right: 26%;bottom: 16%;z-index: 8;
    background: url(<?php echo base_url('public/theme/images/guias/prod/alcantarillado/5.png') ?>);background-size: cover;height: 232px;}
.cinco1:hover{ background: url(<?php echo base_url('public/theme/images/guias/prod/alcantarillado/5over.png') ?>);background-size: cover;max-width: 172px;height:232px;}

.seis1{
    max-width: 233px;width: 100%;position: absolute;right: 17%;bottom: 13%;z-index: 6;
    background: url(<?php echo base_url('public/theme/images/guias/prod/alcantarillado/6.png') ?>);background-size: cover;height: 296px;
}
.seis1:hover{ background: url(<?php echo base_url('public/theme/images/guias/prod/alcantarillado/6over.png') ?>);background-size: cover;max-width: 233px;height: 296px; }






    area #1{
      //  background: url(images/prod/1.png) 100% 100% no-repeat;*/
    }
      
         
.uno3{
max-width: 199px;
width: 100%;
position: absolute;
left: 21%;
top: 64%;
z-index: 1;
background: url(<?php echo base_url('public/theme/images/guias/prod/contra_incendio/6.png') ?>);
background-size: cover;
height: 163px;
visibility: hidden;
}
.uno3:hover{
    background: url(<?php echo base_url('public/theme/images/guias/prod/contra_incendio/6o.png') ?>);
    background-size: cover;
    max-width: 199px;
     height: 163px;
}

.uno4{
max-width: 210px;
width: 100%;
position: absolute;
left: 17.9%;
bottom: 74%;
z-index: 6;
background: url(<?php echo base_url('public/theme/images/guias/prod/contra_incendio1/2.png') ?>);
background-size: cover;
height: 163px;
   }
.uno4:hover{
    background: url(<?php echo base_url('public/theme/images/guias/prod/contra_incendio1/2o.png') ?>);
    background-size: cover;
   max-width: 210px;
   height: 163px;
}




.dos3{
max-width: 197px;
width: 100%;
position: absolute;
left: 21.9%;
top: 71%;
z-index: 6;
background: url(<?php echo base_url('public/theme/images/guias/prod/contra_incendio/1.png') ?>);
background-size: cover;
height: 235px;
}
.dos3:hover{
background: url(<?php echo base_url('public/theme/images/guias/prod/contra_incendio/1o.png') ?>);
background-size: cover;
max-width: 197px;
height: 235px;
}
.dos4{
max-width: 305px;
width: 100%;
position: absolute;
left: 17.9%;
bottom: 63.2%;
z-index: 6;
background: url(<?php echo base_url('public/theme/images/guias/prod/contra_incendio1/1.png') ?>);
background-size: cover;
height: 159px;
}
.dos4:hover{
background: url(<?php echo base_url('public/theme/images/guias/prod/contra_incendio1/1o.png') ?>);
background-size: cover;
max-width: 305px;
height: 159px;
}




.tres3{
max-width: 145px;
width: 100%;
position: absolute;
left: 25%;
top: 74%;
z-index: 5;
background: url(<?php echo base_url('public/theme/images/guias/prod/contra_incendio/4.png') ?>);
background-size: cover;
height: 347px;
}
.tres3:hover{
background: url(<?php echo base_url('public/theme/images/guias/prod/contra_incendio/4o.png') ?>);
background-size: cover;
max-width: 145px;
height: 347px;
}
.tres4{
max-width: 269px;
width: 100%;
position: absolute;
left: 23%;
bottom: 52%;
z-index: 5;
background: url(<?php echo base_url('public/theme/images/guias/prod/contra_incendio1/3.png') ?>);
background-size: cover;
height: 304px;
}
.tres4:hover{
    background: url(<?php echo base_url('public/theme/images/guias/prod/contra_incendio1/3o.png') ?>);
    background-size: cover;
  max-width: 269px;
    height: 304px;
}



.cuatro3{
max-width: 260px;
width: 100%;
position: absolute;
left: 37.8%;
top: 79%;
z-index: 4;
background: url(<?php echo base_url('public/theme/images/guias/prod/contra_incendio/3.png') ?>);
background-size: cover;
height: 277px;
}
.cuatro3:hover{
   background: url(<?php echo base_url('public/theme/images/guias/prod/contra_incendio/3o.png') ?>);
   background-size: cover;
     max-width: 260px;
   height: 277px;
}
.cuatro4{
max-width: 156px;
width: 100%;
position: absolute;
left: 37.8%;
bottom: 51.7%;
z-index: 4;
background: url(<?php echo base_url('public/theme/images/guias/prod/contra_incendio1/4.png') ?>);
background-size: cover;
height: 307px;
}
.cuatro4:hover{
   background: url(<?php echo base_url('public/theme/images/guias/prod/contra_incendio1/4o.png') ?>);
   background-size: cover;
   max-width: 156px;
   height: 307px;
}


 .cinco3{
max-width: 130px;
width: 100%;
position: absolute;
right: 35%;
top:79.5%;
z-index: 8;
background: url(<?php echo base_url('public/theme/images/guias/prod/contra_incendio/5.png') ?>);
background-size: cover;
height: 250px;
}
.cinco3:hover{
   background: url(<?php echo base_url('public/theme/images/guias/prod/contra_incendio/5o.png') ?>);
   background-size: cover;
  max-width: 130px;
height: 250px;
}
.cinco4{
max-width: 249px;
 width: 100%;
 position: absolute;
 right: 29%;
 bottom:  54.1%;
 z-index: 8;
 background: url(<?php echo base_url('public/theme/images/guias/prod/contra_incendio1/5.png') ?>);
 background-size: cover;
 height: 403px;
}
.cinco4:hover{
   background: url(<?php echo base_url('public/theme/images/guias/prod/contra_incendio1/5o.png') ?>);
   background-size: cover;
   max-width: 249px;
height: 403px;
}


.seis3{
max-width: 193px;
width: 100%;
position: absolute;
right: 21%;
top: 79.5%;
z-index: 6;
background: url(<?php echo base_url('public/theme/images/guias/prod/contra_incendio/2.png') ?>);
background-size: cover;
height: 236px;
}
.seis3:hover{
  background: url(<?php echo base_url('public/theme/images/guias/prod/contra_incendio/2o.png') ?>);
  background-size: cover;
 max-width: 193px;
    height: 236px;
}
       .seis4{
max-width: 148px;
width: 100%;
position: absolute;
right: 24%;
bottom:52.5%;
z-index: 6;
background: url(<?php echo base_url('public/theme/images/guias/prod/contra_incendio1/6.png') ?>);
background-size: cover;
height: 330px;
}
.seis4:hover{
   background: url(<?php echo base_url('public/theme/images/guias/prod/contra_incendio1/6o.png') ?>);
   background-size: cover;
   max-width: 148px;
   height: 330px;
}
 
 
.siete3{
max-width: 238px;
width: 100%;
position: absolute;
right: 22.3%;
top: 69%;
z-index: 7;
background: url(<?php echo base_url('public/theme/images/guias/prod/contra_incendio/7.png') ?>);
background-size: cover;
height: 152px;
}
.siete3:hover{
   background: url(<?php echo base_url('public/theme/images/guias/prod/contra_incendio/7o.png') ?>);
   background-size: cover;
       max-width: 238px;
   height: 152px;
}
         .siete4{
max-width: 202px;
width: 100%;
position: absolute;
right: 17.3%;
bottom: 76.5%;
z-index: 7;
background: url(<?php echo base_url('public/theme/images/guias/prod/contra_incendio1/7.png') ?>);
background-size: cover;
height: 194px;
}
.siete4:hover{
   background: url(<?php echo base_url('public/theme/images/guias/prod/contra_incendio1/7o.png') ?>);
   background-size: cover;
  max-width: 202px;
  height: 194px;
}

       .ocho4{
max-width: 270px;
width: 100%;
position: absolute;
right: 17.2%;
bottom: 82.2%;
z-index: 8;
background: url(<?php echo base_url('public/theme/images/guias/prod/contra_incendio1/8.png') ?>);
background-size: cover;
height: 186px;
}
.ocho4:hover{
       background: url(<?php echo base_url('public/theme/images/guias/prod/contra_incendio1/8o.png') ?>);
   background-size: cover;
    max-width: 270px;
   height: 186px;
}

   
    
    @media(max-width:1440px){
/*REPARACION DE FUGAS*/  
 .uno2 {right: 25%;}
 .dos2 {right: 20%;bottom: 9%;}
 .tres2 {left: 44.2%;bottom: 5%;}
 .cuatro2 {left: 23.2%;bottom: 4%;}
 
/*CONDUCCION DE AGUA*/
.uno{ max-width: 200px;left: 17%;top: 22%; }
.dos{ max-width: 178px;left: 12.8%;top: 40.2%;}
.tres{ max-width: 180px;left: 13.2%;bottom: 27.5%;}
.cuatro{ max-width: 200px;left: 14%;bottom: 10%;}
.cinco{ max-width: 123px;left: 24%;}
.seis{ max-width: 99px;left: 32%;}
.siete{ max-width: 162px;left: 40.3%;}
.ocho{ right: 39.9%;}
.nueve{ max-width: 144px;right: 31%;bottom: 13%;}
.diez{ max-width: 225px;right: 22.5%; }
.once{ max-width: 292px;right: 13.7%;bottom: 5%; }
.doce{ max-width: 109px;right: 14%;top: 29%; }
.trece{ max-width: 222px;right: 13%;top: 5%; }
     
/*ALCANTARILLADO*/
.uno1 {left: 19%;bottom: 11%;}
.dos1 {left: 31%;bottom: 10%;}
.tres1{left: 42.5%;bottom: 15%;}
.cuatro1 {left: 46.5%;bottom: 16%;}
.cinco1 {right: 25.3%;bottom: 16%;}
.seis1 {right: 17%;}
.siete1 {right: 12.3%;top: 15%;}

  
  
  
  
  
           .uno3 { 
  left: 11%;
    top: 64%;}
           .uno4 {
    left: 7.9%;}
      
      
       
          .dos3 {
  left: 10.9%;
      top: 71%;}
          .dos4 {
    left: 7.9%;
   }
        
      
        
          .tres3 {
     left: 17%;}
          .tres4 {
    left: 14%;
    }
      
      
         
          .cuatro3 {
  left: 33.8%;
  bottom: 19%;}
          .cuatro4 {
    left: 33.8%;
    bottom: 50.8%;}
       
       
         
         .cinco3 {  
      right: 29.5%;}
         .cinco4 {
    right: 23%;
    bottom: 10%;
         }
      
       
         .seis3 {
      right: 12%;}
         .seis4 {
    right: 15%;
    bottom:49.6%}
        
         .siete3 {  
     right: 13.3%;}
         .siete4 {
    right: 6.3%;
    bottom:75.5%}
     
         .ocho4 {
    right: 7.2%;}
         
    }
    
    
    @media(max-width:1336px){
        
/*REPARACION DE FUGAS*/  
.uno2 {right: 26%;top: 16%;}
.dos2 {right: 19.8%;}
.tres2 {left: 44.3%;bottom: 5%;}
.cuatro2 {left: 22.4%;}  

/*CONDUCCION DE AGUA*/
.uno{ max-width: 200px;left: 16%;}
.dos{ max-width: 178px;left: 12.9%;top: 40.5%;}
.tres{ max-width: 180px;left: 13%;bottom: 26%;}
.cuatro{ max-width: 200px;left: 13%;bottom: 9%; }
.cinco{ max-width: 123px;left: 24%; }
.seis{ max-width: 99px;left: 32.3%; }
.siete{ max-width: 162px;left: 40.5%;}
.ocho{ max-width: 120px;right: 40%;}
.nueve{ max-width: 144px;right: 31%; }
.diez{ max-width: 225px;right: 22.5%;}
.once{ max-width: 292px;right: 11.5%;}
.doce{ max-width: 109px;right: 14%; }
.trece{ max-width: 222px;right: 13%; }

/*ALCANTARILLADO*/
.uno1 { left: 18%;}
.dos1 { left: 30.3%;}
.tres1{ left: 42%;}
.cuatro1 { left: 46.3%;bottom: 16%;}
.cinco1 { right: 26%;}
.seis1 { right: 17%;}
.siete1 { right: 12.6%;top: 15%;}


           .uno3 {
 left: 16%;}
           .uno4 {
    left: 11.9%;
}
      
          
           
             .dos3 {
 left: 15.9%;}
             .dos4 {
    left: 11.9%;}
    
        



           .tres3 {
      left: 21%;
}
.tres4 {
    left: 18%;
}
       
          
          
            .cuatro3 {
  left: 35.8%;}
            .cuatro4 {
    left: 35.8%;}
        
          
          .cinco3 {
     right: 32.5%;
}
.cinco4 {
    right: 26%;
    bottom:  51.2%;}
    
          
          .seis3 {
      right: 16%;}
          .seis4 {
    right: 19%;}
       
          
          .siete3 {
      right: 17.3%;
}
.siete4 {
    right: 11.3%;}
       
         .ocho4 {
    right: 12.2%;
}
       
    }
    
    @media(max-width:1280px){
        
/*REPARACION DE FUGAS*/  
.dos2 { right: 19.8%; }

/*CONDUCCION DE AGUA*/
.uno{ max-width: 200px;left: 15%; }
.dos{ max-width: 178px;left: 13%;top: 39.9%; }
.tres{ max-width: 180px;left: 13%;bottom: 27%; }
.cuatro{ max-width: 200px;left: 13%; }
.cinco{ max-width: 123px;left: 24%; }
.seis{ max-width: 99px;left: 32.3%;}
.siete{ max-width: 162px;left: 40.5%; }
.ocho{ max-width: 120px;right: 40.2%; }
.nueve{ max-width: 144px;right: 31%; }
.diez{ max-width: 225px;right: 22.8%; }
.once{ max-width: 292px;right: 13.7%; }
.doce{ max-width: 109px;right: 14%; }
.trece{ max-width: 222px;right: 13%; }

/*ALCANTARILLADO*/
.uno1 { left: 18%;}
.dos1 { left: 30.3%;}
.tres1{ left: 42%;}
.cuatro1 { left: 46.3%;bottom: 16%; height: 309px;}
.cuatro1:hover { height: 309px;}
.cinco1 { right: 26%;}
.seis1 { right: 17%;}
.siete1 { right: 12.6%;top: 15%;height: 143px;}
.siete1:hover { height: 143px; }




.tres3 {
    left: 20%;
}
       
       
        .cuatro2 {left: 23.2%;}
        
        
.cinco3 {
    right: 31.5%;
}
.cinco4 {
    right: 25%;}
        
   
.seis3 {
    right: 15%;
}
.seis4 {
    right: 18%;
}
         
        
.siete3 {
    right: 16.3%;
}
          
    }
        
    
@media(max-width:1200px){
/*REPARACION DE FUGAS*/  
.uno2 { right: 26%; }
.dos2 { right: 19.8%;}
.tres2 { left: 44.3%; }
.cuatro2 { left: 23.3%; }

/*CONDUCCION DE AGUA*/
.uno{ max-width: 200px;left: 15%; }
.dos{ max-width: 178px;left: 12.8%;top: 40.3%; }
.tres{ max-width: 180px;left: 13%;bottom: 28%; }
.cuatro{ max-width: 200px;left: 13.5%; }
.cinco{ max-width: 123px;left: 23.7%; }
.seis{ max-width: 99px;left: 32.3%; }
.siete{ max-width: 162px;left: 40.5%; }
.ocho{ max-width: 120px;right: 39.5%; }
.nueve{ max-width: 144px;right: 31%; }
.diez{ max-width: 225px;right: 22.8%; }
.once { max-width: 292px;right: 13.7%; }
.doce{ max-width: 109px;right: 14%;top: 29%; }
.trece{ max-width: 222px;right: 13%; }

/*ALCANTARILLADO*/
.uno1 { left: 20%;}
.dos1 { left: 30.3%;}
.tres1{ left: 42.5%;}
.cuatro1 {left: 46.5%;}         
.cinco1 {right: 25.3%;}
.seis1 { right: 17%;}        
.siete1 {right: 12.3%;height: 144px;}      
      


.uno3 {
      left: 13%;
}
.uno4 {
    left: 8.9%;
}
  
  
.dos3 {
  left: 12.9%;
}
.dos4 {
    left: 7.9%;
}
        
        
  
.tres3 {
 left: 18%;
}
.tres4 {
    left: 15%;
}
     
        
 
.cuatro3 {
  left: 33.8%;
}
.cuatro4 {
    left: 33.8%;
}
        
         
.cinco3 {
     right: 30%;
}
.cinco4 {
    right: 23%;}
      
         
.seis3 {
  right: 13%;
}
.seis4 {
    right: 16%;
}
       
         
.siete3 {
 right: 14.3%;
}
.siete4 {
    right: 7.3%;
}
       
         .ocho4 {
    right: 9.2%;
}
        
    }
    

@media(max-width:1024px){
/*REPARACION DE FUGAS*/  
.uno2 { right: 21%; }
.dos2 { right: 13.3%; }
.tres2 { left: 43.3%; }
.cuatro2 { left: 17.7%; }

/*CONDUCCION DE AGUA*/
.uno{ max-width: 200px;left:9%; }
.dos{ max-width: 178px;left: 5.1%;top: 40.2%; }
.tres{ max-width: 180px;left:5.5%;bottom: 26.5%; }
.cuatro{ max-width: 200px;left:5.5%;bottom: 9%; }
.cinco{ max-width: 123px;left:18.5%; }
.seis{ max-width: 99px;left: 28.3%; }
.siete{ max-width: 162px;left: 38.5%; }
.ocho{ max-width: 120px;right: 38.2%; }
.nueve{ max-width: 144px;right: 27%; }
.diez{ max-width: 225px;right: 16.8%; }
.once{ max-width: 292px;right: 6.2%; }
.doce{ max-width: 109px; right:7%; }
.trece{ max-width: 222px;right:5%; }

/*ALCANTARILLADO*/
.uno1 { left: 14%; }
.dos1 { left: 27.3%;}
.tres1 { left: 41%;}
.cuatro1 { left: 45.5%;}
.cinco1 { right: 20.3%; }
.seis1 { right: 10%; }
.siete1 { right: 4.5%;}





.uno3 {
     left: 7%;
}
.uno4 {
    left: 2.9%;
}
      
        

.dos3 {
  left: 6.9%;
}
.dos4 {
    left: 1.9%;
}
       
        
 
.tres3 {
  left: 14%;
}
.tres4 {
    left: 11%;
}
    
        
  
.cuatro3 {
  left: 31.8%;
}
.cuatro4 {
    left: 31.8%;
}
      
         
.cinco3 {
  right: 28%;
}
.cinco4 {
    right: 20%;
}
       
         
.seis3 {
      right: 8%;
}
.seis4 {
    right: 11%;
}
      
         
         .siete3 {
      right: 9.3%;
}
.siete4 {
    right: 2.3%;
}
          
         .ocho4 {
    right: 3.2%;
}
         
        
    }
    
    
@media(max-width:768px){
    /*REPARACION DE FUGAS*/  
    .uno2 { right: 12%;top: 14%;}  
    .dos2 { right: 11.5%;bottom: 3%;}
    .tres2 { left: 41.3%;bottom: -3%; }
    .cuatro2 { left: 15.7%;bottom: -5%; }
    
    /*CONDUCCION DE AGUA*/
    .uno{ max-width: 169px;left: 8%;top: 21%;height: 106px; }
    .uno:hover{height: 106px;max-width: 169px;}
    
    .dos{ max-width: 122px;left:7.3%;top: 41.2%;height: 81px; }
    .dos:hover{ height: 81px;max-width: 122px; }
    
    .tres{ max-width: 134px;left: 6.5%;top: 51%;height: 110px; }
    .tres:hover{ max-width: 134px;height: 110px; }
    
    .cuatro{ max-width: 149px;left:7%;top: 52%;height: 197px; }
    .cuatro:hover{ max-width: 149px;height: 197px;}
    
    .cinco{ max-width: 93px;left:19%;top: 53%;height: 167px; }
    .cinco:hover{ max-width: 93px;height: 167px; }
    
    .seis{ max-width: 82px;left: 28.3%;top:54%;height: 165px; }
    .seis:hover{ max-width: 82px;height: 165px; }
    
    .siete{ max-width: 120px;left: 39.5%;top: 44%;height: 229px; }    
    .siete:hover{ max-width: 120px;height: 229px; } 
    
    .ocho{ max-width: 94px;left: 49.6%;top: 52.8%;height: 203px; }
    .ocho:hover{ max-width: 94px;height: 203px; }
    
    .nueve{ max-width: 112px;left: 58%;top:52.3%;height: 189px; }
    .nueve:hover{max-width: 112px;height: 189px; }
    
    .diez{max-width: 175px;left:59.7%;top:52.3%;height: 187px; }
    .diez:hover{max-width: 175px;height: 187px; }
    
    .once{ max-width: 227px;left: 63.8%;top: 50%;height: 248px; }
    .once:hover{ max-width: 227px;height: 248px; }
    
    .doce{ max-width: 90px;right: 7%;top: 28%;height: 123px;z-index: 8; }
    .doce:hover{ max-width: 90px; height: 123px; }
    
    .trece{ max-width: 168px;right: 6%;top: 7%;height: 242px; }
    .trece:hover{ max-width: 168px;height: 242px; }
    
    /*ALCANTARILLADO*/    
     .uno1 {max-width: 112px;left: 13%;top: 49%;height: 240px;}
     .uno1:hover{ height: 240px;max-width: 112px; }
     
     .dos1 { max-width: 114px;left: 26.3%;top: 48.3%;height: 247px;}
     .dos1:hover{ height: 247px;max-width: 114px; }
     
     .tres1 { max-width: 117px;left: 40%;top: 48%; height: 225px; }
     .tres1:hover{ max-width: 117px;height: 225px; }
     
     .cuatro1 { max-width: 172px;left: 45.5%;top: 40%;height: 291px; }
     .cuatro1:hover{ max-width: 172px;height:  291px; }
     
     .cinco1 { max-width: 161px;left: 61.3%;top: 50%;height: 219px; }
     .cinco1:hover{ max-width: 161px;height: 219px; }
     
     .seis1 { max-width: 224px;left: 65.5%;top: 42%;height: 285px; }
     .seis1:hover{ max-width: 224px;height: 285px; }
     
     .siete1 { max-width: 247px;left: 65%;top: 14%;height: 139px; }
     .siete1:hover{ max-width: 247px;height: 139px; }
        

       
        
    }
     @media(max-width:640px){
     .trece{ max-width: 168px;
  /* width: 156px; */
  right: 3%;
  top: 11%;
  height: 184px;
     }
     .doce {
  max-width: 90px;
  right: 3%;
  top: 25%;
  height: 98px;
  z-index: 8;
}
.once {
  max-width: 217px;
  left: 58.8%;
  padding-left: 29px;
  top: 34%;
  height: 239px;
}
.diez {
  max-width: 175px;
  left: 54%;
  top: 39%;
  height: 187px;
}
.siete {
  max-width: 120px;
  left: 34.5%;
  top: 37%;
  height: 229px;
}
.siete1 {
  max-width: 204px;
  left: 62.3%;
  top: 23.5%;
  height: 114px;
}
.siete1:hover
{
    max-width: 204px;
    height: 114px;
}
.nueve {
  max-width: 112px;
  left: 53%;
  top: 40%;
  height: 189px;
}
.ocho {
  max-width: 94px;
  left: 44%;
  top: 39%;
  height: 203px;
}
.siete {
  max-width: 120px;
  left: 32.5%;
  top: 35%;
  height: 229px;
}
.uno {
  max-width: 169px;
  left: 1%;
  top: 17.5%;
  height: 106px;
}
.uno1 {
  max-width: 102px;
  left: 7%;
  top: 44%;
  height: 218px;
}
.uno1:hover
{
     max-width: 102px;height: 218px;
}
.dos {
  max-width: 122px;
  left: 1.3%;
  top: 33.2%;
  height: 81px;
}
.dos1 {
  max-width: 91px;
  left: 21.3%;
  top: 44.7%;
  height: 213px;
}
.dos1:hover
{
    max-width: 91px;height: 213px;
}
.tres {
  max-width: 134px;
  left: 1.5%;
  top: 42%;
  height: 110px;
}
.tres1 {
  max-width: 93px;
  left: 36%;
  top: 44%;
  height: 194px;
}
.tres1:hover
{
    max-width: 93px;height: 194px;
}
.cuatro {
  max-width: 149px;
  left: 1%;
  top: 40%;
  height: 197px;
}
.cuatro1 {
  max-width: 136px;
  left: 42.5%;
  top: 39%;
  height: 230px;
}
.cuatro1:hover
{
     max-width: 136px;  height: 230px;
}
.cinco {
  max-width: 93px;
  left: 14%;
  top: 42%;
  height: 167px;
}
.cinco1 {
  max-width: 137px;
  left: 58%;
  top: 45%;
  height: 181px;
}
.cinco1:hover
{ max-width: 137px;height: 181px;}
.seis {
  max-width: 82px;
  left: 24.3%;
  top: 42%;
  height: 165px;
}
.seis1 {
  max-width: 181px;
  left: 63.5%;
  top: 40%;
  height: 227px;
}
.seis1:hover
{max-width: 181px;height: 227px;}
     }
    @media(max-width:480px){
        
        /*html.no-js { @import "_hover-states"; }*/ 
        
        /*#principal{
            display: none;
        }*/
        .c4 {  
  margin-top: -218px!important; 
  width:100%!important;
}      
#principal{
   max-width: 300px;
   left: 0;
   top: 0;   
   background-size: cover;
   display: block;
   margin-bottom: 1.5em;
  position: relative;
/*  height: 206px;*/
        }
          .uno{
          max-width: 300px;
   left: 0;
   top: 0;
   background: url(<?php echo base_url('public/theme/images/guias/prod/m/conduccion_agua/1_movil.png') ?>);
   background-size: cover;
   display: block;
  padding-bottom: 10px;
  position: relative;
  height: 156px;
        }
        .uno:hover{
           max-width: 300px;
           background: url(<?php echo base_url('public/theme/images/guias/prod/m/conduccion_agua/1_movil.png') ?>);
           background-size: cover;
           display: block;
           padding-bottom: 10px;
           position: relative;
           height: 156px;
        }
         .uno1{
          max-width: 300px;
   left: 0;
   top: 0;
   background: url(<?php echo base_url('public/theme/images/guias/prod/m/alcantarillado/2_movil.png') ?>);
   background-size: cover;
   display: block;
  padding-bottom: 10px;
  position: relative;
  height: 156px;
        }
        .uno1:hover{
           max-width: 300px;
           background: url(<?php echo base_url('public/theme/images/guias/prod/m/alcantarillado/2_movil.png') ?>);
           background-size: cover;
           display: block;
           padding-bottom: 10px;
           position: relative;
           height: 156px;
        }
         .uno2{
          max-width: 300px;
   left: 0;
   top: 0;
   background: url(<?php echo base_url('public/theme/images/guias/prod/m/tuberia/4_movil.png') ?>);
   background-size: cover;
   display: block;
  padding-bottom: 10px;
  position: relative;
  height: 156px;
        }
        .uno2:hover{
           max-width: 300px;
           background: url(<?php echo base_url('public/theme/images/guias/prod/m/tuberia/4_movil.png') ?>);
           background-size: cover;
           display: block;
           padding-bottom: 10px;
           position: relative;
           height: 156px;
        }
          .uno3{
          max-width: 300px;
   left: 0;
   top: 0;
   background: url(<?php echo base_url('public/theme/images/guias/prod/m/contra_incendio/6m.png') ?>);
   background-size: cover;
   display: block;
  padding-bottom: 10px;
  position: relative;
  height: 156px;
        }
        .uno3:hover{
           max-width: 300px;
           background: url(<?php echo base_url('public/theme/images/guias/prod/m/contra_incendio/6m.png') ?>);
           background-size: cover;
           display: block;
           padding-bottom: 10px;
           position: relative;
           height: 156px;
        }
                 .uno4{
          max-width: 300px;
   left: 0;
   top: 0;
   background: url(<?php echo base_url('public/theme/images/guias/prod/m/contra_incendio1/2m.png') ?>);
   background-size: cover;
   display: block;
  padding-bottom: 10px;
  position: relative;
  height: 156px;
        }
        .uno4:hover{
           max-width: 300px;
           background: url(<?php echo base_url('public/theme/images/guias/prod/m/contra_incendio1/2m.png') ?>);
           background-size: cover;
           display: block;
           padding-bottom: 10px;
           position: relative;
           height: 156px;
        }
        .dos{
           max-width: 300px;
left: 0;
top: 0;
background: url(<?php echo base_url('public/theme/images/guias/prod/m/conduccion_agua/2_movil.png') ?>);
background-size: cover;
display: block;
padding-bottom: 10px;
position: relative;
height: 156px;
        }
        .dos:hover{
           max-width: 300px;
           background: url(<?php echo base_url('public/theme/images/guias/prod/m/conduccion_agua/2_movil.png') ?>);
           background-size: cover;
           display: block;
           padding-bottom: 10px;
           position: relative;
           height: 156px;
        }   
         .dos1{
           max-width: 300px;
left: 0;
top: 0;
background: url(<?php echo base_url('public/theme/images/guias/prod/m/alcantarillado/3_movil.png') ?>);
background-size: cover;
display: block;
padding-bottom: 10px;
position: relative;
height: 156px;
        }
        .dos1:hover{
           max-width: 300px;
           background: url(<?php echo base_url('public/theme/images/guias/prod/m/alcantarillado/3_movil.png') ?>);
           background-size: cover;
           display: block;
           padding-bottom: 10px;
           position: relative;
           height: 156px;
        }      
          .dos2{
           max-width: 300px;
left: 0;
top: 0;
background: url(<?php echo base_url('public/theme/images/guias/prod/m/tuberia/3_movil.png') ?>);
background-size: cover;
display: block;
padding-bottom: 10px;
position: relative;
height: 156px;
        }
        .dos2:hover{
           max-width: 300px;
           background: url(<?php echo base_url('public/theme/images/guias/prod/m/tuberia/3_movil.png') ?>);
           background-size: cover;
           display: block;
           padding-bottom: 10px;
           position: relative;
           height: 156px;
        }    
         .dos3{
           max-width: 300px;
left: 0;
top: 0;
background: url(<?php echo base_url('public/theme/images/guias/prod/m/contra_incendio/1m.png') ?>);
background-size: cover;
display: block;
padding-bottom: 10px;
position: relative;
height: 156px;
        }
        .dos3:hover{
           max-width: 300px;
           background: url(<?php echo base_url('public/theme/images/guias/prod/m/contra_incendio/1m.png') ?>);
           background-size: cover;
           display: block;
           padding-bottom: 10px;
           position: relative;
           height: 156px;
        }   
                 .dos4{
           max-width: 300px;
left: 0;
top: 0;
background: url(<?php echo base_url('public/theme/images/guias/prod/m/contra_incendio1/1m.png') ?>);
background-size: cover;
display: block;
padding-bottom: 10px;
position: relative;
height: 156px;
        }
        .dos4:hover{
           max-width: 300px;
           background: url(<?php echo base_url('public/theme/images/guias/prod/m/contra_incendio1/1m.png') ?>);
           background-size: cover;
           display: block;
           padding-bottom: 10px;
           position: relative;
           height: 156px;
        }  
          .tres{
            max-width: 300px;
  left: 0;
  top: 0;
  background: url(<?php echo base_url('public/theme/images/guias/prod/m/conduccion_agua/3_movil.png') ?>);
  background-size: cover;
  display: block;
  padding-bottom: 10px;
  position: relative;
  height: 156px;
}

        .tres:hover{
           max-width: 300px;
           background: url(<?php echo base_url('public/theme/images/guias/prod/m/conduccion_agua/3_movil.png') ?>);
           background-size: cover;
           display: block;
           padding-bottom: 10px;
           position: relative;
           height: 156px;
        }
 .tres1{
            max-width: 300px;
  left: 0;
  top: 0;
  background: url(<?php echo base_url('public/theme/images/guias/prod/m/alcantarillado/4_movil.png') ?>);
  background-size: cover;
  display: block;
  padding-bottom: 10px;
  position: relative;
  height: 156px;
}

        .tres1:hover{
           max-width: 300px;
           background: url(<?php echo base_url('public/theme/images/guias/prod/m/alcantarillado/4_movil.png') ?>);
           background-size: cover;
           display: block;
           padding-bottom: 10px;
           position: relative;
           height: 156px;
        }
        .tres2{
            max-width: 300px;
  left: 0;
  top: 0;
  background: url(<?php echo base_url('public/theme/images/guias/prod/m/tuberia/2_movil.png') ?>);
  background-size: cover;
  display: block;
  padding-bottom: 10px;
  position: relative;
  height: 156px;
}

        .tres2:hover{
           max-width: 300px;
           background: url(<?php echo base_url('public/theme/images/guias/prod/m/tuberia/2_movil.png') ?>);
           background-size: cover;
           display: block;
           padding-bottom: 10px;
           position: relative;
           height: 156px;
        }
         .tres3{
            max-width: 300px;
  left: 0;
  top: 0;
  background: url(<?php echo base_url('public/theme/images/guias/prod/m/contra_incendio/4m.png') ?>);
  background-size: cover;
  display: block;
  padding-bottom: 10px;
  position: relative;
  height: 156px;
}

        .tres3:hover{
           max-width: 300px;
           background: url(<?php echo base_url('public/theme/images/guias/prod/m/contra_incendio/4m.png') ?>);
           background-size: cover;
           display: block;
           padding-bottom: 10px;
           position: relative;
           height: 156px;
        }
                 .tres4{
            max-width: 300px;
  left: 0;
  top: 0;
  background: url(<?php echo base_url('public/theme/images/guias/prod/m/contra_incendio1/3m.png') ?>);
  background-size: cover;
  display: block;
  padding-bottom: 10px;
  position: relative;
  height: 156px;
}

        .tres4:hover{
           max-width: 300px;
           background: url(<?php echo base_url('public/theme/images/guias/prod/m/contra_incendio1/3m.png') ?>);
           background-size: cover;
           display: block;
           padding-bottom: 10px;
           position: relative;
           height: 156px;
        }
        .cuatro{
          max-width: 300px;          
          left: 0;
   top: 0;
            background: url(<?php echo base_url('public/theme/images/guias/prod/m/conduccion_agua/4_movil.png') ?>);
            background-size: cover;
              display: block;
  padding-bottom: 10px;
  position: relative;
  height: 156px;
        }
                .cuatro:hover{
           max-width: 300px;
           background: url(<?php echo base_url('public/theme/images/guias/prod/m/conduccion_agua/4_movil.png') ?>);
           background-size: cover;
           display: block;
           padding-bottom: 10px;
           position: relative;
           height: 156px;
        }
         .cuatro1{
          max-width: 300px;          
          left: 0;
   top: 0;
            background: url(<?php echo base_url('public/theme/images/guias/prod/m/alcantarillado/5_movil.png') ?>);
            background-size: cover;
              display: block;
  padding-bottom: 10px;
  position: relative;
  height: 156px;
        }
                .cuatro1:hover{
           max-width: 300px;
           background: url(<?php echo base_url('public/theme/images/guias/prod/m/alcantarillado/5_movil.png') ?>);
           background-size: cover;
           display: block;
           padding-bottom: 10px;
           position: relative;
           height: 156px;
        }
          .cuatro2{
          max-width: 300px;          
          left: 0;
   top: 0;
            background: url(<?php echo base_url('public/theme/images/guias/prod/m/tuberia/1_movil.png') ?>);
            background-size: cover;
              display: block;
  padding-bottom: 10px;
  position: relative;
  height: 156px;
        }
                .cuatro2:hover{
           max-width: 300px;
           background: url(<?php echo base_url('public/theme/images/guias/prod/m/tuberia/1_movil.png') ?>);
           background-size: cover;
           display: block;
           padding-bottom: 10px;
           position: relative;
           height: 156px;
        }
         .cuatro3{
          max-width: 300px;          
          left: 0;
   top: 0;
            background: url(<?php echo base_url('public/theme/images/guias/prod/m/contra_incendio/3m.png') ?>);
            background-size: cover;
              display: block;
  padding-bottom: 10px;
  position: relative;
  height: 156px;
        }
                .cuatro3:hover{
           max-width: 300px;
           background: url(<?php echo base_url('public/theme/images/guias/prod/m/contra_incendio/3m.png') ?>);
           background-size: cover;
           display: block;
           padding-bottom: 10px;
           position: relative;
           height: 156px;
        }
              .cuatro4{
          max-width: 300px;          
          left: 0;
   top: 0;
            background: url(<?php echo base_url('public/theme/images/guias/prod/m/contra_incendio1/4m.png') ?>);
            background-size: cover;
              display: block;
  padding-bottom: 10px;
  position: relative;
  height: 156px;
        }
                .cuatro4:hover{
           max-width: 300px;
           background: url(<?php echo base_url('public/theme/images/guias/prod/m/contra_incendio1/4m.png') ?>);
           background-size: cover;
           display: block;
           padding-bottom: 10px;
           position: relative;
           height: 156px;
        }
        .cinco{
           max-width: 300px;          
           left: 0;
   top: 0;
             background: url(<?php echo base_url('public/theme/images/guias/prod/m/conduccion_agua/5_movil.png') ?>);
            background-size: cover;
              display: block;
  padding-bottom: 10px;
  position: relative;
  height: 156px;
          }
        .cinco:hover{
           max-width: 300px;
           background: url(<?php echo base_url('public/theme/images/guias/prod/m/conduccion_agua/5_movil.png') ?>);
           background-size: cover;
           display: block;
           padding-bottom: 10px;
           position: relative;
           height: 156px;
        }     
         .cinco1{
           max-width: 300px;          
           left: 0;
   top: 0;
             background: url(<?php echo base_url('public/theme/images/guias/prod/m/alcantarillado/6_movil.png') ?>);
            background-size: cover;
              display: block;
  padding-bottom: 10px;
  position: relative;
  height: 156px;
          }
        .cinco1:hover{
           max-width: 300px;
           background: url(<?php echo base_url('public/theme/images/guias/prod/m/alcantarillado/6_movil.png') ?>);
           background-size: cover;
           display: block;
           padding-bottom: 10px;
           position: relative;
           height: 156px;
        }    
         .cinco3{
           max-width: 300px;          
           left: 0;
   top: 0;
             background: url(<?php echo base_url('public/theme/images/guias/prod/m/contra_incendio/5m.png') ?>);
            background-size: cover;
              display: block;
  padding-bottom: 10px;
  position: relative;
  height: 156px;
          }
        .cinco3:hover{
           max-width: 300px;
           background: url(<?php echo base_url('public/theme/images/guias/prod/m/contra_incendio/5m.png') ?>);
           background-size: cover;
           display: block;
           padding-bottom: 10px;
           position: relative;
           height: 156px;
        }  
                 .cinco4{
           max-width: 300px;          
           left: 0;
   top: 0;
             background: url(<?php echo base_url('public/theme/images/guias/prod/m/contra_incendio1/5m.png') ?>);
            background-size: cover;
              display: block;
  padding-bottom: 10px;
  position: relative;
  height: 156px;
          }
        .cinco4:hover{
           max-width: 300px;
           background: url(<?php echo base_url('public/theme/images/guias/prod/m/contra_incendio1/5m.png') ?>);
           background-size: cover;
           display: block;
           padding-bottom: 10px;
           position: relative;
           height: 156px;
        } 
         .seis{
           max-width: 300px;       
           left: 0;
   top: 0;
             background: url(<?php echo base_url('public/theme/images/guias/prod/m/conduccion_agua/6_movil.png') ?>);
            background-size: cover;
              display: block;
  padding-bottom: 10px;
  position: relative;
  height: 156px;
         }
                 .seis:hover{
           max-width: 300px;
           background: url(<?php echo base_url('public/theme/images/guias/prod/m/conduccion_agua/6_movil.png') ?>);
           background-size: cover;
           display: block;
           padding-bottom: 10px;
           position: relative;
           height: 156px;
        }
         .seis1{
           max-width: 300px;       
           left: 0;
   top: 0;
             background: url(<?php echo base_url('public/theme/images/guias/prod/m/alcantarillado/7_movil.png') ?>);
            background-size: cover;
              display: block;
  padding-bottom: 10px;
  position: relative;
  height: 156px;
         }
                 .seis1:hover{
           max-width: 300px;
           background: url(<?php echo base_url('public/theme/images/guias/prod/m/alcantarillado/7_movil.png') ?>);
           background-size: cover;
           display: block;
           padding-bottom: 10px;
           position: relative;
           height: 156px;
        }
        .seis3{
           max-width: 300px;       
           left: 0;
   top: 0;
             background: url(<?php echo base_url('public/theme/images/guias/prod/m/contra_incendio/2m.png') ?>);
            background-size: cover;
              display: block;
  padding-bottom: 10px;
  position: relative;
  height: 156px;
         }
                 .seis3:hover{
           max-width: 300px;
           background: url(<?php echo base_url('public/theme/images/guias/prod/m/contra_incendio/2m.png') ?>);
           background-size: cover;
           display: block;
           padding-bottom: 10px;
           position: relative;
           height: 156px;
        }
                .seis4{
           max-width: 300px;       
           left: 0;
   top: 0;
             background: url(<?php echo base_url('public/theme/images/guias/prod/m/contra_incendio1/6m.png') ?>);
            background-size: cover;
              display: block;
  padding-bottom: 10px;
  position: relative;
  height: 156px;
         }
                 .seis4:hover{
           max-width: 300px;
           background: url(<?php echo base_url('public/theme/images/guias/prod/m/contra_incendio1/6m.png') ?>);
           background-size: cover;
           display: block;
           padding-bottom: 10px;
           position: relative;
           height: 156px;
        }
         .siete{
          max-width: 300px;
left: 0;
   top: 0;
            background: url(<?php echo base_url('public/theme/images/guias/prod/m/conduccion_agua/7_movil.png') ?>);
            background-size: cover;
              display: block;
  padding-bottom: 10px;
  position: relative;
height: 156px;            
         }
                 .siete:hover{
           max-width: 300px;
           background: url(<?php echo base_url('public/theme/images/guias/prod/m/conduccion_agua/7_movil.png') ?>);
           background-size: cover;
           display: block;
           padding-bottom: 10px;
           position: relative;
           height: 156px;
        }
         .siete1{
          max-width: 300px;
left: 0;
   top: 0;
            background: url(<?php echo base_url('public/theme/images/guias/prod/m/alcantarillado/1_movil.png') ?>);
            background-size: cover;
              display: block;
  padding-bottom: 10px;
  position: relative;
height: 156px;            
         }
                 .siete1:hover{
           max-width: 300px;
           background: url(<?php echo base_url('public/theme/images/guias/prod/m/alcantarillado/1_movil.png') ?>);
           background-size: cover;
           display: block;
           padding-bottom: 10px;
           position: relative;
           height: 156px;
        }
         .siete3{
          max-width: 300px;
left: 0;
   top: 0;
            background: url(<?php echo base_url('public/theme/images/guias/prod/m/contra_incendio/7m.png') ?>);
            background-size: cover;
              display: block;
  padding-bottom: 10px;
  position: relative;
height: 156px;            
         }
                 .siete3:hover{
           max-width: 300px;
           background: url(<?php echo base_url('public/theme/images/guias/prod/m/contra_incendio/7m.png') ?>);
           background-size: cover;
           display: block;
           padding-bottom: 10px;
           position: relative;
           height: 156px;
        }
                 .siete4{
          max-width: 300px;
left: 0;
   top: 0;
            background: url(<?php echo base_url('public/theme/images/guias/prod/m/contra_incendio1/7m.png') ?>);
            background-size: cover;
              display: block;
  padding-bottom: 10px;
  position: relative;
height: 156px;            
         }
                 .siete4:hover{
           max-width: 300px;
           background: url(<?php echo base_url('public/theme/images/guias/prod/m/contra_incendio1/7m.png') ?>);
           background-size: cover;
           display: block;
           padding-bottom: 10px;
           position: relative;
           height: 156px;
        }
          .ocho{
             max-width: 300px;
             /*left: 47.8%;*/
             left: 0;
  top: 0;
  background: url(<?php echo base_url('public/theme/images/guias/prod/m/conduccion_agua/8_movil.png') ?>);
  background-size: cover;
  display: block;
  padding-bottom: 10px;
  position: relative;
  height: 156px;
         }
                 .ocho:hover{
           max-width: 300px;
           background: url(<?php echo base_url('public/theme/images/guias/prod/m/conduccion_agua/8_movil.png') ?>);
           background-size: cover;
           display: block;
           padding-bottom: 10px;
           position: relative;
           height: 156px;
        }
               .ocho4{
             max-width: 300px;
             /*left: 47.8%;*/
             left: 0;
  top: 0;
  background: url(<?php echo base_url('public/theme/images/guias/prod/m/contra_incendio1/8m.png') ?>);
  background-size: cover;
  display: block;
  padding-bottom: 10px;
  position: relative;
  height: 156px;
         }
                 .ocho4:hover{
           max-width: 300px;
           background: url(<?php echo base_url('public/theme/images/guias/prod/m/contra_incendio1/8m.png') ?>);
           background-size: cover;
           display: block;
           padding-bottom: 10px;
           position: relative;
           height: 156px;
        }
         .nueve{
         max-width: 300px;
         /*left: 58%;*/
         left: 0;
   top: 0;
         background: url(<?php echo base_url('public/theme/images/guias/prod/m/conduccion_agua/9_movil.png') ?>);
         background-size: cover;
           display: block;
  padding-bottom: 10px;
  position: relative;
  height: 156px;
         }
                 .nueve:hover{
           max-width: 300px;
           background: url(<?php echo base_url('public/theme/images/guias/prod/m/conduccion_agua/9_movil.png') ?>);
           background-size: cover;
           display: block;
           padding-bottom: 10px;
           position: relative;
           height: 156px;
        }
         .diez{
            max-width: 300px;
            /*left: 60.5%;*/
            left: 0;
   top: 0;
            background: url(<?php echo base_url('public/theme/images/guias/prod/m/conduccion_agua/10_movil.png') ?>);
            background-size: cover;
              display: block;
  padding-bottom: 10px;
  position: relative;
  height: 156px;
         }
                 .diez:hover{
           max-width: 300px;
           background: url(<?php echo base_url('public/theme/images/guias/prod/m/conduccion_agua/10_movil.png') ?>);
           background-size: cover;
           display: block;
           padding-bottom: 10px;
           position: relative;
           height: 156px;
        }
         .once{
          max-width: 300px;
          /*left: 64.8%;*/
          left: 0;
   top: 0;
          background: url(<?php echo base_url('public/theme/images/guias/prod/m/conduccion_agua/11_movil.png') ?>);
          background-size: cover;
            display: block;
  padding-bottom: 10px;
  position: relative;
  height: 156px;
         }
                 .once:hover{
           max-width: 300px;
           background: url(<?php echo base_url('public/theme/images/guias/prod/m/conduccion_agua/11_movil.png') ?>);
           background-size: cover;
           display: block;
           padding-bottom: 10px;
           position: relative;
           height: 156px;
        }
         .doce{
           max-width: 300px;
/*  right: 2%;
  top: 37%;*/
left: 0;
   top: 0;
    background: url(<?php echo base_url('public/theme/images/guias/prod/m/conduccion_agua/12_movil.png') ?>);
            background-size: cover;
              display: block;
  padding-bottom: 10px;
  position: relative;
  height: 156px;
         }
                 .doce:hover{
           max-width: 300px;
           background: url(<?php echo base_url('public/theme/images/guias/prod/m/conduccion_agua/12_movil.png') ?>);
           background-size: cover;
           display: block;
           padding-bottom: 10px;
           position: relative;
           height: 156px;
        }
         .trece{
             max-width: 300px;
/*  right: 3%;
  top: 21%;*/
left: 0;
   top: 0;
    background: url(<?php echo base_url('public/theme/images/guias/prod/m/conduccion_agua/13_movil.png') ?>);
            background-size: cover;
              display: block;
  padding-bottom: 10px;
  position: relative;
  height: 156px;
         }
                 .trece:hover{
           max-width: 300px;
           background: url(<?php echo base_url('public/theme/images/guias/prod/m/conduccion_agua/13_movil.png') ?>);
           background-size: cover;
           display: block;
           padding-bottom: 10px;
           position: relative;
           height: 156px;
        }
        
    }
    @media(max-width:320px){
        
            .uno{
            max-width: 283px;
  left: 0%;
  top: 33%;
        }
        #principal{
   max-width: 300px;
   left: 0;
   top: 0;   
   background-size: cover;
   display: block;
   margin-bottom: 1.5em;
  position: relative;
  height: 206px;
        }
         .c4 {  
  margin-top: -160px!important;   
}
        .dos{
               max-width: 283px;
  left: 0%;
  top: 52.2%
        }
          .tres{
              max-width: 283px;
  left: 0%;
  top: 61%;
}
        .cuatro{
             max-width: 283px;
  left: 0%;
  top: 61%
          
        }
        .cinco{
             max-width: 283px;
  left: 0%;
  top: 61%;
         }
         .seis{
         max-width: 283px;
         left: 0%;
         top: 62%;
         }
         .siete{
           max-width: 283px;
  left: 0%;
  top: 57%;
            
         }
          .ocho{
            max-width: 283px;
  left: 0%;
  top: 61%;
         }
         .nueve{
      max-width: 283px;
  left: 0%;
  top: 61.3%;

         }
         .diez{
         max-width: 283px;
  left: 0%;
  top: 61%;
         }
         .once{
     max-width: 283px;
  left: 0%;
  top: 60%;
         }
         .doce{
      max-width: 283px;
  right: 0%;
  top: 39%;
         }
         .trece{
          max-width: 283px;
  right: 0%;
  top: 27%;
         }
        
    }
    
    input[type=text], select{
        width:90%;
        padding: 8px; 
    }    
    select{
        padding: 11px; 
    }    
    .texto, .lista{
        width: 45%!important;
    }
</style>        
<?php if($prod_id == 1){  ?>          
<section class="page-section pt-20 pb-40">
    <div class="container relative">
        <!-- Intro Text -->
        <div class="row">
            <div class="col-md-12">
                <div class="section-text align-left">
                    <h3 class="tituloproductos1 mt-0 mb-0">Reparación de fugas</h3>
                    <div class="linea"></div>
                </div>
            </div>
        </div>
        <!-- End Intro Text -->
        <!--<div class="clearfix"></div>-->
        <div class="row">
            <!--<div class="col-md-3">-->
                <?php 
//                $this->load->view('vistas/menu_productos');
                ?>
            <!--</div>-->
            <div class="col-md-12 text-center" style="background: #fff;padding: 30px;">
                  <img id="principal" class="img" src="<?php echo base_url('public/theme/images/guias/tuberia.png') ?>" alt="" style="width: 100%;max-width: 906px;position: relative;margin-top: -1%;" />
                                   <a href="reparacion_fuga.php?action=1"><div class="uno2"></div></a>
                                 <a href="reparacion_fuga.php?action=2"><div class="dos2"></div></a>
                                 <a href="reparacion_fuga.php?action=3"><div class="tres2"></div></a>
                                 <a href="reparacion_fuga.php?action=4"><div class="cuatro2"></div></a>     
                
              
            </div>
        </div>
  </div>
</section>        
<?php } if($prod_id == 2){  ?>  
  <section class="page-section pt-20 pb-40">
    <div class="container relative">
        <!-- Intro Text -->
        <div class="row">
            <div class="col-md-12">
                <div class="section-text align-left">
                    <h3 class="tituloproductos1 mt-0 mb-0">Conducción de agua</h3>
                    <div class="linea"></div>
                </div>
            </div>
        </div>
        <!-- End Intro Text -->
        <!--<div class="clearfix"></div>-->
        <div class="row">           
            <div class="col-md-12 text-center" style="background: #fff;padding: 30px;">
                   <img id="principal" class="img" src="<?php echo base_url('public/theme/images/guias/CONDUCCION.png') ?>" alt="" style="width: 100%;max-width: 906px;position: relative;margin-top: -1%;" />
                                 <a href="index.html"><div class="uno"></div></a>
                                 <a href="index.html"><div class="dos"></div></a>
                                 <a href="index.html"><div class="tres"></div></a>
                                 <a href="index.html"><div class="cuatro"></div></a>
                                 <a href="index.html"><div class="cinco"></div></a>
                                 <a href="index.html"><div class="seis"></div></a>   
                                <a href="index.html"><div class="siete"></div></a>
                                <a href="index.html"><div class="ocho"></div></a>
                                <a href="index.html"><div class="nueve"></div></a>
                                <a href="index.html"><div class="diez"></div></a>
                                <a href="index.html"><div class="once"></div></a>
                                <a href="index.html"><div class="doce"></div></a>
                                <a href="index.html"><div class="trece"></div></a>
                
              
            </div>
        </div>
  </div>
</section>        

<?php } if($prod_id == 3){  ?>  
<section class="page-section pt-20 pb-40">
    <div class="container relative">
        <!-- Intro Text -->
        <div class="row">
            <div class="col-md-12">
                <div class="section-text align-left">
                    <h3 class="tituloproductos1 mt-0 mb-0">Alcantarillado</h3>
                    <div class="linea"></div>
                </div>
            </div>
        </div>
        <!-- End Intro Text -->
        <!--<div class="clearfix"></div>-->
         <div class="row">           
            <div class="col-md-12 text-center" style="background: #fff;padding: 30px;">
                    <img id="principal" class="img" src="<?php echo base_url('public/theme/images/guias/alcantarillado1.png') ?>" alt="" style="width: 100%;max-width: 906px;position: relative;margin-top: -1%;" />
                                  <a href="index.html"><div class="siete1"></div></a>  
                                   <a href="index.html"><div class="uno1"></div></a>
                                 <a href="index.html"><div class="dos1"></div></a>
                                 <a href="index.html"><div class="tres1"></div></a>
                                 <a href="index.html"><div class="cuatro1"></div></a>
                                 <a href="index.html"><div class="cinco1"></div></a>
                                 <a href="index.html"><div class="seis1"></div></a>   
                
              
            </div>
        </div>
  </div>
</section>        
<?php } if($prod_id == 4){  ?>  
 <section class="page-section pt-20 pb-40">
    <div class="container relative">
        <!-- Intro Text -->
        <div class="row">
            <div class="col-md-12">
                <div class="section-text align-left">
                    <h3 class="tituloproductos1 mt-0 mb-0">Contra incendio</h3>
                    <div class="linea"></div>
                </div>
            </div>
        </div>
        <!-- End Intro Text -->
        <!--<div class="clearfix"></div>-->
       <div class="row">           
            <div class="col-md-12" style="background: #fff;padding: 30px;">
              <img id="principal1" class="img" src="<?php echo base_url('public/theme/images/guias/contra-incendio1.png') ?>" alt="" style="width: 100%;max-width: 906px;text-align:center;position: relative;background-color: #fff;margin-top: -1%;" />                                   
                                   <a href="index.html"><div class="uno4"></div></a>
                                 <a href="index.html"><div class="dos4"></div></a>
                                 <a href="index.html"><div class="tres4"></div></a>
                                 <a href="index.html"><div class="cuatro4"></div></a>                           
                                 <a href="index.html"><div class="cinco4"></div></a>
                                 <a href="index.html"><div class="seis4"></div></a>
                                 <a href="index.html"><div class="siete4"></div></a>
                                 <a href="index.html"><div class="ocho4"></div></a>
                                        <div class="clearfix"> </div>
                                    <img id="principal2" class="img" src="<?php echo base_url('public/theme/images/guias/contra_incendio.png') ?>" alt="" style="width: 100%;max-width: 906px;text-align:center;position: relative;background-color: #fff;margin-top: 4%;" />                                   
                                 <a href="index.html"><div class="uno3"></div></a>
                                 <a href="index.html"><div class="dos3"></div></a>
                                 <a href="index.html"><div class="tres3"></div></a>
                                 <a href="index.html"><div class="cuatro3"></div></a>                           
                                 <a href="index.html"><div class="cinco3"></div></a>
                                 <a href="index.html"><div class="seis3"></div></a>
                                 <a href="index.html"><div class="siete3"></div></a>
                                        <div class="clearfix"> </div>    
                
              
            </div>
        </div>
        </div>
  </div>
</section>        

<?php } if($prod_id == 5){  ?>  
   <section class="page-section pt-20 pb-40">
    <div class="container relative">
        <!-- Intro Text -->
        <div class="row">
            <div class="col-md-12">
                <div class="section-text align-left">
                    <h3 class="tituloproductos1 mt-0 mb-0">Sistemas de bombeo</h3>
                    <div class="linea"></div>
                </div>
            </div>
        </div>
        <!-- End Intro Text -->
        <!--<div class="clearfix"></div>-->
     <div class="row">           
            <div class="col-md-12" style="background: #fff;padding: 30px;">
                <div class="col-md-12 mb-10 text-left">
                    <h5 style="text-align:left;">Guía de Selección</h5>
                </div>
               <form name="frm" id="frm" action="" method="post">
                   <div class="row">
                    <div class="col-md-6 ">
                                        <div class="col-md-12" style="margin-bottom: 10px;text-align:left;"> 
                                            ¿Cuál es el gasto? * <br>
                                            <input type="text" name="gasto" id="gasto" class="texto"/>
                                            <select id="medida_gasto" name="medida_gasto" class="lista">
                                            <option value="">Unidad</option>
                                            <option value="L/h">L/h</option>
                                            <option value="L/m">L/m</option>
                                            <option value="L/s">L/s</option>
                                            <option value="m³/h">m&sup3;/h</option>
                                            <option value="GPM">GPM</option>
                                        </select>
                                     </div>
                                  <div class="col-md-12" style="margin-bottom: 10px;text-align:left;"> 
                                      ¿Cuál es la presión? *<br>
                                   <input type="text" name="presion" id="presion" class="texto"/>
                                       <select id="medida_presion" name="medida_presion" class="lista">
                                            <option value="">Unidad</option>
                                            <option value="PSI">PSI</option>
                                            <option value="Pascal">Pascal</option>
                                            <option value="Bar">Bar</option>
                                            <option value="N/mm²">N/mm&sup2;</option>
                                            <option value="kp/m²">kp/m&sup2;</option>
                                            <option value="kp/cm²">kp/cm&sup2;</option>
                                            <option value="atm">atm</option>
                                            <option value="Torr">Torr</option>                                            
                                        </select>
                                </div>
                                  <div class="col-md-12" style="margin-bottom: 10px;text-align:left;"> 
                                      ¿Qué tipo de fluido manejas? *<br>                                   
                                      <select name="tipo_flujo" id="tipo_flujo" onclick="TipoFlujo(this.value)">
                                          <option value="">Selecciona</option>
                                            <option value="1">Agua limpia</option>
                                            <option value="2">Agua Residual</option>                                            
                                        </select>                                   
                                    </div>
                                  <div class="col-md-12" style="margin-bottom: 10px;text-align:left;"> 
                                  
                                      <div id="opera"></div>                                  
                               </div>
                                         <div class="col-md-12" style="margin-bottom: 10px;text-align:left;"> 
                                           
                                             <div id="aplica"></div>
                               </div>
                                         <div class="col-md-12" style="margin-bottom: 10px;text-align:left;"> 
                                           
                                                <div id="instala" ></div>
                               </div>
                                        <div id="informa" style="display:none"></div>
                                         
                                   <div class="col-md-12" style="margin-bottom: 10px;text-align:left;"> 

                                       <div id="acciona" style="display:none"></div>  
                                    </div>
                                        <div id="informa_acciona" style="display:none" ></div>
                                     
                                    
                                     <div id="observa_text" class="col-md-12" style="margin-bottom: 10px;text-align: left;display:none;"> 
                                         Observaciones:<br>
                              <textarea name="observacion" id="observacion" style="width: 90%;height: 220px;"></textarea>                                                      
                               </div>
                                    </div>
                                    
                                    
                                    <div class="col-md-6">
                                        
                                         <div style="width:100%;height: 400px;">
                                             <img id="city" src="<?php echo base_url('public/theme/images/guias/bombeo/CIUDAD.jpg') ?>" alt="" style="display:block;width: 100%;"/>
                                             <img id="agr" src="<?php echo base_url('public/theme/images/guias/bombeo/AGROPECUARIO.gif') ?>" alt="" style="display:none;width: 100%;"/>
                                            <img id="domcis" src="<?php echo base_url('public/theme/images/guias/bombeo/DOMESTICOCONCISTERNA.gif') ?>" alt="" style="display:none;width: 100%;"/>
                                            <img id="dom" src="<?php echo base_url('public/theme/images/guias/bombeo/DOMESTICO.gif') ?>" alt="" style="display:none;width: 100%;"/>
                                            <img id="eci" src="<?php echo base_url('public/theme/images/guias/bombeo/EQUIPO-CONTRA-INCENDIO.gif') ?>" alt="" style="display:none;width: 100%;"/>
                                            <img id="epc" src="<?php echo base_url('public/theme/images/guias/bombeo/EQUIPO-DE-PRECION-CONSTANTE.gif') ?>" alt="" style="display:none;width: 100%;"/>
                                            <img id="inf" src="<?php echo base_url('public/theme/images/guias/bombeo/INFRAESTRUCTURA.gif') ?>" alt="" style="display:none;width: 100%;"/>
                                            <img id="min" src="<?php echo base_url('public/theme/images/guias/bombeo/MINERIA.gif') ?>" alt="" style="display:none;width: 100%;"/>
                                            <img id="obp" src="<?php echo base_url('public/theme/images/guias/bombeo/OBRAPUBLICA.gif') ?>" alt="" style="display:none;width: 100%;"/>
                                            <img id="rsum" src="<?php echo base_url('public/theme/images/guias/bombeo/residual_sumergible.gif') ?>" alt="" style="display:none;width: 100%;"/>
                                            <img id="rsup" src="<?php echo base_url('public/theme/images/guias/bombeo/ksh_residual_superficie.gif') ?>" alt="" style="display:none;width: 100%;"/>
                                            <img id="lim_sup" src="<?php echo base_url('public/theme/images/guias/bombeo/limpia_superficie.gif') ?>" alt="" style="display:none;width: 100%;"/>
                                            <img id="lim_sum" src="<?php echo base_url('public/theme/images/guias/bombeo/limpia-sumergible.gif') ?>" alt="" style="display:none;width: 100%;"/>
                                        </div>
                                        <div class="col-md-12 contact-right" style="text-align:justify;padding-top:5em;">   
                            
                           
		     <div class="col-md-12" style="margin-bottom: 10px"> 
                                    <div class=" col-md-4" style="">Nombre:</div>
                                    <div class=" col-md-8" style=""><input type="text" name="nombre" id="nombre"/></div>      
                                  </div>
                                  <div class="col-md-12" style="margin-bottom: 10px"> 
                                    <div class=" col-md-4" style="">Empresa:</div>
                                    <div class=" col-md-8" style=""><input type="text" name="empresa" id="empresa"/></div>      
                                </div>
                                  <div class="col-md-12" style="margin-bottom: 10px"> 
                                    <div class=" col-md-4" style="">Teléfono:</div>
                                    <div class=" col-md-8" style=""><input type="text" name="telefono" id="telefono"/></div> 
                                    </div>
                                  <div class="col-md-12" style="margin-bottom: 10px"> 
                                    <div class=" col-md-4" style="">Correo:</div>
                                    <div class=" col-md-8" style=""><input type="text" name="correo" id="correo"/></div>                                                       
                               </div>
                                  <div  class="col-md-12" style="margin-bottom: 10px;"> 
                              <div class=" col-md-4" style="">Mensaje:</div>
                              <div class=" col-md-8" style=""><textarea name="mensaje" id="mensaje" style="width: 98%;height: 220px;margin-bottom: 20px;"></textarea></div>                                                       
                               </div>
                             <div class="col-md-12" style="margin-bottom: 30px">   
                                  <div class=" col-md-8" style="position: relative;float: right;">
                                     <a href="javascript:EnviaInformacion()" style="padding:10px 25px;font-size: 15px;color:#fff;background:#000;text-decoration: none;">ENVIAR</a>
                                  </div>
                                 </div>
                                                           
                           </div>
                                    </div>
                       
                       
                       
                   </div>
               </form>
                
            </div>
        </div>
  </div>
</section>


<?php } if($prod_id == 6){  ?>  
  <section class="page-section pt-20 pb-40">
    <div class="container relative">
        <!-- Intro Text -->
        <div class="row">
            <div class="col-md-12">
                <div class="section-text align-left">
                    <h3 class="tituloproductos1 mt-0 mb-0">Válvulas de control</h3>
                    <div class="linea"></div>
                </div>
            </div>
        </div>
        <!-- End Intro Text -->
        <!--<div class="clearfix"></div>-->
         <div class="row">           
            <div class="col-md-12" style="background: #fff;padding: 30px;">
                
                
              
            </div>
        </div>
  </div>
</section>        
<?php } ?>  

    <section class="page-section hidden-lg hidden-md pt-sm-20 pb-sm-20 pt-xs-60 pb-xs-60"></section>
    
    