<?php

// echo $this->uri->segment(2);

?>
     
            <!-- Section -->
            <section class="page-section pt-30 pb-0">
                <div class="container relative">
                     <div class="row">
                         
                         <div class="loading" style="display: none;">
                             <div class="content">
                
                             </div>
                         </div>
                     </div>
                    <div class="row">
                        
                        <!-- Content -->
                        <div class="col-sm-12">
                            
                            <!-- Shop options --
                            <div class="clearfix mb-40">
                                
                                <div class="left section-text mt-10">
                                    Showing 1–12 of 23 results
                                </div>
                                
                                <div class="right">
                                    <form method="post" action="#" class="form">
                                        <select class="input-md round">
                                            <option>Default sorting</option>
                                            <option>Sort by price: low to high</option>
                                            <option>Sort by price: high to low</option>
                                        </select>
                                    </form>
                                </div>
                                
                            </div>
                            <!-- End Shop options -->
                           
<!--                            
                             <div class="list-item"></div>
                            -->
                                
                      
                            <div class="row multi-columns-row">
                                 <div class="post-list" id="postList">
                                      <div class="clearfix">
                              <?php  if(!empty($posts)): foreach($posts as $post): ?>
                                <!-- Shop Item -->
                                <div class="col-md-3 col-lg-3 mb-60 mb-xs-40">                                    
                                    <div class="post-prev-img">
                                         <?php 
                                        $sencillo = explode("-",$post['SKU']); 
                                        $sep_sen = $sencillo[0];
                                        if($sep_sen == '3121'){
                                        ?>
                                        <img src="<?php echo base_url('public/theme') ?>/images/power/3121.jpg" alt="" />
                                        <?php } else if ($sep_sen == '3121AI') { ?>
                                        <img src="<?php echo base_url('public/theme') ?>/images/power/3121as.jpg" alt="" />
                                        
                                        <?php  } else if ($sep_sen == '3122') { ?>
                                        <img src="<?php echo base_url('public/theme') ?>/images/power/3122.jpg" alt="" />
                                        <?php  } else if ($sep_sen == '3122AI') { ?>
                                        <img src="<?php echo base_url('public/theme') ?>/images/power/3122as.jpg" alt="" />
                                        <?php  } else if ($sep_sen == '3123') { ?>
                                        <img src="<?php echo base_url('public/theme') ?>/images/power/3123.jpg" alt="" />
                                        <?php  } else if ($sep_sen == '3123AI') { ?>
                                        <img src="<?php echo base_url('public/theme') ?>/images/power/3123as.jpg" alt="" />
                                        <?php  }   else if ($sep_sen == '3232') { ?>
                                        <img src="<?php echo base_url('public/theme') ?>/images/power/3232.jpg" alt="" />
                                        <?php  }    else if ($sep_sen == '3506') { ?>
                                        <img src="<?php echo base_url('public/theme') ?>/images/power/3506.jpg" alt="" />
                                         <?php  }    else if ($sep_sen == '3506LB') { ?>
                                        <img src="<?php echo base_url('public/theme') ?>/images/power/3506.jpg" alt="" />
                                        <?php  }    else if ($sep_sen == '3506AI') { ?>
                                        <img src="<?php echo base_url('public/theme') ?>/images/power/3506as.jpg" alt="" />
                                        <?php  }  ?>
                                        <!--<a href="shop-single.html"><img src="images/shop/shop-prev-1.jpg" alt="" /></a>-->                                        
                                        <div class="intro-label">
                                            <!--<span class="label label-danger bg-red">Sale</span>-->
                                        </div>                                        
                                    </div>
                                    
                                    <div class="post-prev-title font-alt align-center">
                                        <a href="javascript:AddCart('<?php echo $post['SKU']; ?>','<?php echo $post['Nombre'] ?>','<?php echo $post['Precio'] ?>','<?php echo $post['TipoMoneda'] ?>')">
                                            <i class="fa fa-shopping-cart"></i> <?php echo $post['Nombre']; ?>
                                        </a>
                                        <br><?php if(isset($post['MaterialOreja'])){ echo $post['MaterialOreja']; } ?>
                                        <br><?php echo $post['SKU']; ?>
                                        
                                    </div>

                                    <div class="post-prev-text align-center">
                                        <!--<del>$150.00</del>-->
                                        &nbsp;
                                        <!--<strong>$94.75</strong>-->
                                    </div>
                                    
                                    <div class="post-prev-more align-center">
                                        <!--<a href="#" class="btn btn-mod btn-gray btn-round"><i class="fa fa-shopping-cart"></i> Add to cart</a>-->
                                    </div>
                                    
                                </div>
                                <!-- End Shop Item -->
                                <?php endforeach; else: ?>
                                   <p>Post(s) not available.</p>
                                 <?php  endif; ?>
                                  </div>
                                     <?php echo $this->ajax_pagination->create_links(); ?>
                          
                                
                           
                                
                                
                                   </div>
                            </div>
                            
                            <!-- Pagination --
                            <div class="pagination">
                                <a href=""><i class="fa fa-angle-left"></i></a>
                                <a href="" class="active">1</a>
                                <a href="">2</a>
                                <a href="">3</a>
                                <a class="no-active">...</a>
                                <a href="">9</a>
                                <a href=""><i class="fa fa-angle-right"></i></a>
                            </div>
                            <!-- End Pagination -->
                            
                        </div>
                        <!-- End Content -->
                  
                                        
                    </div>
                    
                </div>
                   <?php echo form_open_multipart(base_url('welcome/crea_sesion_producto'), array('id' => 'frm-prod')) ?>
                    <input type="hidden" name="skus" id="skus" value="" />
                    <input type="hidden" name="nprod" id="nprod" value="" />
                    <input type="hidden" name="prices" id="prices" value="" />
                    <input type="hidden" name="tipo_moneda" id="tipo_moneda" value="" />
                    <input type="hidden" name="cantidad" id="cantidad" value="1" />
                  <?php echo form_close(); ?>
            </section>
            <!-- End Section -->