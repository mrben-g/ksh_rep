 <section class="page-section pt-20 pb-40">
                <div class="container relative">               
                                  <!-- Intro Text -->
                    <div class="row">
                        <div class="col-md-12 mb-20">
                            <div class="section-text align-left">
                              <h3 class="tituloproductos1 mt-0 mb-0" style="">Nuestros Productos de compra</h3>
                              <div class="linea"></div>
                            </div>
                        </div>
                    </div>
                                 
                    <!-- End Intro Text -->
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1 mb-sm-40">
              
                            <!-- Logo Grid -->
                    <div class="row multi-columns-row mb-20">
                        
                        <!-- Logo Item -->
                        <div class="col-sm-6 col-md-4 col-lg-4 mb-sm-20">
                        
                            <div class="blog-post-prev-small mb-20 clearfix" style="background:#fff;">
                                        <a href="<?php echo base_url('tienda_reparacion_de_tuberia') ?>"><img src="<?php echo base_url('public/theme/images/productos_home_menu/rept.png') ?>" alt="" class="widget-posts-img" /></a>
                                            <h3 class="alt-features-title font-alt mb-0 mt-0 pt-10 pb-10" style="padding: 10px;">REPARACIÓN DE TUBERÍA</h3>
<!--                                        <div class="widget-posts-descr" style="font-size:10px;">
                                                <label><a href="<?php echo base_url('producto_reparacion_de_tuberia') ?>?id=1">ABRAZADERAS DE REPARACIÓN</a></label>
                                                <label><a href="<?php echo base_url('producto_reparacion_de_tuberia') ?>?id=2">ABRAZADERAS CAMPANA BELL JOINT</a></label>
                                                <label><a href="<?php echo base_url('producto_reparacion_de_tuberia') ?>?id=3">JUNTAS UNIVERSALES</a></label>
                                                <label><a href="<?php echo base_url('producto_reparacion_de_tuberia') ?>?id=4">COPLES DAYTON</a></label>
                                        </div>-->
                                    </div>
              
                        </div>
                        <!-- End Logo Item -->             
                        <!-- Logo Item -->
                        <div class="col-sm-6 col-md-4 col-lg-4 mb-sm-20">                            
                            <div class="blog-post-prev-small mb-20 clearfix" style="background:#fff;">
                                <a href="<?php echo base_url('tienda_contra_incendio') ?>"><img src="<?php echo base_url('public/theme/images/productos_home_menu/incendio.png') ?>" alt="" class="widget-posts-img" style="width:88px;" /></a>
                                <h3 class="alt-features-title font-alt mb-0 mt-0 pt-10 pb-10" style="padding: 10px;">CONTRA INCENDIO</h3>
                                    </div>                        
                        </div>
                        <!-- End Logo Item -->
                   
                        <!-- Logo Item -->
                        <div class="col-sm-6 col-md-4 col-lg-4 mb-sm-20">                           
                            <div class="blog-post-prev-small mb-20 clearfix" style="background:#fff;">
                                <a href="<?php echo base_url('tienda_conduccion_derivacion') ?>"><img src="<?php echo base_url('public/theme/images/productos_home_menu/conduccionderivacion.png') ?>" alt="" class="widget-posts-img" style="width:88px;" /></a>
                                  <h3 class="alt-features-title font-alt mb-0 mt-0 pt-10 pb-10" style="padding: 10px;">CONDUCCIÓN Y DERIVACIÓN</h3>
                                    </div>

                        </div>
                        <!-- End Logo Item -->
                        
                        <!-- Logo Item -->
                        <div class="col-sm-6 col-md-4 col-lg-4 mb-sm-20">                               
                            <div class="blog-post-prev-small mb-20 clearfix" style="background:#fff;">
                                <a href="<?php echo base_url('tienda_alcantarillado') ?>"><img src="<?php echo base_url('public/theme/images/productos_home_menu/alcantarillado.png') ?>" alt="" class="widget-posts-img" style="width:88px;" /></a>
                                 <h3 class="alt-features-title font-alt mb-0 mt-0 pt-10 pb-10" style="padding: 10px;">ALCANTARILLADO</h3>
                            </div>
                        </div>
                        <!-- End Logo Item -->
                        
                             <!-- Logo Item -->
                        <div class="col-sm-6 col-md-4 col-lg-4 mb-sm-20">                          
                            <div class="blog-post-prev-small mb-20 clearfix" style="background:#fff;">
                                <a href="<?php echo base_url('tienda_filtros') ?>"><img src="<?php echo base_url('public/theme/images/productos_home_menu/filtros.png') ?>" alt="" class="widget-posts-img" style="width:88px;" /></a>
                                     <h3 class="alt-features-title font-alt mb-0 mt-0 pt-10 pb-10" style="padding: 10px;">FILTROS </h3>
                               </div>                  
                        </div>
                        <!-- End Logo Item -->
                             <!-- Logo Item -->
                        <div class="col-sm-6 col-md-4 col-lg-4 mb-sm-20">                           
                            <div class="blog-post-prev-small mb-20 clearfix" style="background:#fff;">
                                <a href="<?php echo base_url('tienda_medidores') ?>"><img src="<?php echo base_url('public/theme/images/productos_home_menu/medidores.png') ?>" alt="" class="widget-posts-img" style="width:88px;" /></a>
                                    <h3 class="alt-features-title font-alt mb-0 mt-0 pt-10 pb-10" style="padding: 10px;">MEDIDORES </h3>
                                    </div>                      
                        </div>
                        <!-- End Logo Item -->
                    </div>
                    <!-- End Logo Grid -->                           
                  </div>                      
                       
                    </div>
                </div>
            </section>
            <!-- End Section -->