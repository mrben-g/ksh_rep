   <?php 
   if(!isset($_SESSION['login']['nombre'])){
        redirect(base_url(''));
   }
   // var_dump($this->data['data_perfil']);
    
      $nombre_ef="";$apellidos_ef ="";$puesto="";$razon_social ="";$rfc="";$estado_ef="";$ciudad_ef="";$email_ef="";$zipcode_ef="";$dir_ef="";
      $no_dir="";$col="";$lada="";$tel_ef="";$ladacel ="";$cel="";$dir_envio ="";
      $nom2="";$ap2="";$edo2="";$cd2="";$email2="";$cp2="";
      $dir2="";$no_dir_2="";$col_dir_2="";$lada_tel_2="";$tel2="";$lada_cel_2 ="";$cel2="";$direccion_completa="";
      
      if(isset($_SESSION['login']['nombre'])){
          
                    $idr =  $this->data['data_perfil']->IdCliente;
                    $nombre_ef= $this->data['data_perfil']->Nombre;
                    $apellidos_ef = $this->data['data_perfil']->Apellidos;
                    $puesto = $this->data['data_perfil']->Puesto;
                    $razon_social = $this->data['data_perfil']->RazonSocial;
                    $rfc= $this->data['data_perfil']->Rfc;                                                             
                    $estado_ef= $this->data['data_perfil']->Estado;
                    $ciudad_ef= $this->data['data_perfil']->Ciudad;                                                                 
                    $email_ef= $this->data['data_perfil']->email;
                    $zipcode_ef= $this->data['data_perfil']->CodigoPostal;                                                             
                    $dir_ef = $this->data['data_perfil']->Calle;
                    $no_dir =  $this->data['data_perfil']->NoDireccion;
                    $col =  $this->data['data_perfil']->ColDireccion;                                                             
                    $lada = $this->data['data_perfil']->lada;
                    $tel_ef= $this->data['data_perfil']->Telefono;
                    $ladacel = $this->data['data_perfil']->lada2;                                                             
                    $cel = $this->data['data_perfil']->Celular;

                    $dir_envio = $this->data['data_perfil']->OtraDireccion;
             //       $direccion_completa = $dir.' '.$no_dir.' '.$col.' C.P. '.$zipcode;
                    
                    if($dir_envio == 2){

                    $nom2= $this->data['data_perfil']->NombreEnvio;
                    $ap2= $this->data['data_perfil']->ApellidoEnvio;
                    $edo2= $this->data['data_perfil']->EdoEnvio;
                    $cd2= $this->data['data_perfil']->CdEnvio;
                    $email2= $this->data['data_perfil']->EmailEnvio;
                    $cp2= $this->data['data_perfil']->CodigoPostalEnvio;
                    $dir2= $this->data['data_perfil']->CalleEnvio;
                    $no_dir_2= $this->data['data_perfil']->NoDirEnvio;
                    $col_dir_2= $this->data['data_perfil']->ColDirEnvio;
                    $lada_tel_2 = $this->data['data_perfil']->ladaEnvio;
                    $tel2= $this->data['data_perfil']->TelEnvio;                                                 
                    $lada_cel_2 = $this->data['data_perfil']->ladaCelEnvio;
                    $cel2= $this->data['data_perfil']->CelEnvio;
                  //  $direccion_completa = $dir2.' '.$nu_dir_2.' '.$no_dir_2.' C.P. '.$cp2;
                    
                     }

          
             
       
  }
  
 ?>
<style type="text/css">
    .label{ color:#000;}
</style>

   <section class="page-section  pt-20 pb-40">
                <div class="container relative">
<div class="row">
            <div class="col-md-12">
                <div class="section-text align-left">
                    <h3 class="tituloproductos1 mt-0 mb-0">Información del Perfil</h3>
                    <div class="linea"></div>
                </div>
            </div>
        </div>  
<?php echo form_open_multipart(base_url('welcome/update_perfil'), array('id' => 'perfil_form', 'class' => 'form')) ?>                    
<div class="col_large col-md-12" >
 <div class="row">   
<div class="col-sm-4">
<label class="label">Nombre (s) : <span>*</span></label>
<label class="input"><input id="nombre" name="nombre" type="text" value="<?php echo ($nombre_ef); ?>"></label>
</div>
<div class="col-sm-4">
<label class="label">Apellidos: <span>*</span></label>
<label class="input"><input name="apellidos" id="apellidos" type="text" value="<?php echo ($apellidos_ef); ?>"></label>
</div>
<div class="col-sm-4">
<label class="label">Puesto: <span>*</span></label>
<label class="input"><input name="puesto" id="puesto" type="text" value="<?php echo ($puesto); ?>"></label>
</div>
</div>

<!--<p class="intro" style="padding-top: 15px;"></p>-->
<div class="row">
<div class="col-sm-7">
<label class="label">Razón Social: </label>
<label class="input"><input name="razon_social" id="razon_social" type="text" value="<?php echo $razon_social; ?>"></label>
</div>
<div class="col-sm-5">
<label class="label">R.F.C.: </label>
<label class="input"><input type="text" id="rfc" name="rfc" value="<?php echo $rfc;  ?>"></label>    
</div>
</div>         

<div class="col-sm-12"><p class="intro" style="padding-top: 15px;text-align: left;">Dirección</p></div>
<div class="row">    
<div class="col-sm-5">
<label class="label">Calle: <span>*</span></label>
<label class="input"><input type="text" id="direccion_ef" name="direccion_ef" value="<?php echo ($dir_ef); ?>"></label>
</div>
<div class="col-sm-2">
<label class="label">Número: <span>*</span></label>
<label class="input"><input type="text" id="nu_dir" name="nu_dir" value="<?php echo ($no_dir); ?>"></label>
</div>
<div class="col-sm-5">
 <label class="label">Colonia: <span>*</span></label>
<label class="input"><input type="text" id="col_dir" name="col_dir" value="<?php echo ($col); ?>"></label>   
</div>
</div>
<div class="row">
 <div class="col-sm-4">
<label class="label">Elige tu estado: <span>*</span></label>
<label class="select"> 
                <select id="edo" name="edo" onchange="Ciudades(1,this.value,0)" >
                    <option value="">Selecciona estado</option>
<?php if(!empty($this->data['estados'])): foreach($this->data['estados'] as $edo): ?>
                    <option value="<?php echo $edo['id']; ?>" <?php if($estado_ef == $edo['id']){ ?>selected <?php } ?>><?php echo $edo['nombre']; ?></option>
 <?php 
   endforeach; else:

       endif;     
 ?>
</select><i></i>
</label>
 </div>
  <div class="col-sm-4">
<label  class="label">Elige tu Municipio: <span>*</span></label>
<label id="cdl" class="select">
    <select id="cd" name="cd" onchange="" >
<?php 
 if(isset($_SESSION['login']['nombre'])){
          
  if(!empty($this->data['ciudad'])): foreach($this->data['ciudad'] as $cds): ?>
                    <option value="<?php echo $cds['id']; ?>" <?php if($ciudad_ef == $cds['id']){ ?>selected <?php } ?>><?php echo $cds['nombre']; ?></option>
 <?php 
   endforeach; else:

       endif;     
 }
 ?>  
</select><i></i>
</label>
</div>
<div class="col-sm-4">
<label class="label">Código Postal: <span>*</span></label>
<label class="input"><input type="text" name="codigopostal" id="codigopostal" value="<?php echo $zipcode_ef; ?>"></label>
</div>
</div>
<div class="row">
<div class="col-sm-1">
<label class="label">Lada <span>*</span></label>
<label class="input"><input type="text" id="lada" name="lada" value="<?php echo $lada; ?>"></label>
</div>
<div class="col-sm-3">
<label class="label">Teléfono: <span>*</span></label>
<label class="input"><input type="text" id="tel" name="tel" value="<?php echo $tel_ef; ?>"></label>
</div>
<div class="col-sm-1">
<label class="label">Lada </label>
<label class="input"><input type="text" id="lada2" name="lada2" value="<?php echo $ladacel; ?>"></label>
</div>
<div class="col-sm-3">
<label class="label">Celular:</label>
<label class="input"><input type="text" name="cel" id="cel" value="<?php echo $cel; ?>"></label>
</div>
<div class="col-sm-4">
<label class="label">E-mail: <span>*</span></label>
<label class="input"><input name="email_ef" id="email_ef" type="text" value="<?php  echo $email_ef; ?>" <?php if(isset($_SESSION['login']['nombre'])) { ?>readonly="true"<?php } ?>></label>
</div>
</div>        
<div class="row">
<div class=" col-sm-12">
<label class="radio"><input type="radio" name="billing" id="billing" value="1" onchange="Despliega(0)" <?php if($dir_envio == 1){ ?>checked<?php }else if($dir_envio != 2){ ?>checked<?php } ?>><i></i>Enviar a esta Dirección</label>
</div>
<div class="col-sm-12">
<label class="radio"><input type="radio" name="billing" id="billing-2" value="2" onchange="Despliega(1)" <?php if($dir_envio == 2){ ?>checked <?php } ?> ><i></i>Enviar a Dirección diferente</label>
</div>
</div>
</div>

<div id="frm_factura" action="#" class="form" style="<?php if($dir_envio == 2){ ?>display: block;<?php }else{ ?>display: none;<?php } ?>">    
<div class="">
<div class="col_large col-md-12">
    <div class="col-sm-12">
        <label class="label">Nombre y Apellido de la persona que va recibir el producto.</label>
    </div>
<div class="row">    
<div class="col-sm-6">
<label class="label">Nombre(s): <span>*</span></label>
<label class="input"><input type="text" name="nombre_dos" id="nombre_dos" value="<?php echo ($nom2); ?>"></label>
</div>
<div class="col-sm-6">   
<label class="label">Apellidos: <span>*</span></label>
<label class="input"><input type="text" id="apellidos_dos" name="apellidos_dos" value="<?php echo ($ap2); ?>"></label>
</div>    
</div>
    
<div class="row">
<div class="col-sm-5">
<label class="label">Calle: <span>*</span></label>
<label class="input"><input type="text" name="dir_dos" id="dir_dos" value="<?php echo ($dir2) ?>"></label>             
</div>
<div class="col-sm-2">
<label class="label">Número: <span>*</span></label>
<label class="input"><input type="text" id="no_dir_2" name="no_dir_2" value="<?php echo ($no_dir_2); ?>"></label>
</div>
<div class="col-sm-5">
<label class="label">Colonia: <span>*</span></label>
<label class="input"><input type="text" id="col_dir_2" name="col_dir_2" value="<?php echo ($col_dir_2); ?>"></label>   
</div>
</div> 

<div class="row">    
<div class="col-sm-4">    
<label class="label">Elige tu Estado: <span>*</span></label>
<label class="select"><select id="edo2" name="edo2" onchange="Ciudades(2,this.value,0)">
<option value="">Selecciona estado</option>
<?php if(!empty($this->data['estados'])): foreach($this->data['estados'] as $edo): ?>
<option value="<?php echo $edo['id']; ?>" <?php if($edo2== $edo['id']){ ?>selected <?php } ?>><?php echo $edo['nombre']; ?></option>
 <?php 
   endforeach; else:

       endif;     
 ?>
</select><i></i>
</label>
</div>      
<div class="col-sm-4">
<label  class="label">Elige tu Municipio: <span>*</span></label>
<label id="cdl2" class="select" >  
<select id="cd2" name="cd2" onchange="" >
<?php 
 if(isset($_SESSION['login']['nombre'])){
  if(!empty($this->data['ciudad_dos'])): foreach($this->data['ciudad_dos'] as $cds): ?>
                    <option value="<?php echo $cds['id']; ?>" <?php if($cd2 == $cds['id']){ ?>selected <?php } ?>><?php echo $cds['nombre']; ?></option>
 <?php 
   endforeach; else:

       endif;     
 }
 ?>                                                                                                                                
</select><i></i>
</label>
</div>
<div class="col-sm-4">
        <label class="label">Código Postal: <span>*</span></label>
        <label class="input"><input type="text" name="codigopostal_dos" id="codigopostal_dos" value="<?php echo $cp2; ?>"></label>
</div>
</div>
<div class="row">  
<div class="col-sm-1">
<label class="label">Lada <span>*</span></label>
<label class="input"><input type="text" id="lada_2_1" name="lada_2_1" value="<?php echo $lada_tel_2; ?>"></label>
</div>
<div class="col-sm-3">
<label class="label">Teléfono: <span>*</span></label>
<label class="input"><input type="text" name="tel_dos" id="tel_dos" value="<?php echo $tel2 ?>"></label>
</div>
<div class="col-sm-1">
<label class="label">Lada </label>
<label class="input"><input type="text" id="lada_2_2" name="lada_2_2" value="<?php echo $lada_cel_2; ?>"></label>
</div>
<div class="col-sm-3">
<label class="label">Celular:</label>
<label class="input"><input type="text" name="cel_dos" id="cel_dos" value="<?php echo $cel2; ?>"></label>
</div>
<div class="col-sm-4">
<label class="label">E-mail: <span>*</span></label>
<label class="input"><input type="email" name="email_dos" id="email_dos" value="<?php echo $email2; ?>"></label>
</div>
</div>
</div>    
</div>					
<input type="hidden" name="user_id" id="user_id" value="<?php if(isset($_SESSION['login']['id'])){echo $_SESSION['login']['id'];} ?>" />
<input type="hidden" name="tipo_user" id="tipo_user" value="<?php //if(isset($_SESSION['TipoUsuario'])){echo $_SESSION['TipoUsuario'];} ?>" />
<input type="hidden" name="session_active" id="session_active" value="0" />
<input type="hidden" name="session_guest" id="session_guest" value="0" />
</div>
<?php echo form_close(); ?>
                    
<div id="" class=""  >
<div class="clearfix">
<button type="button" class="button" onclick="ActualizaDatos()">Actualizar Datos de Perfil</button>
</div>
</div>
                    
      <div class="clearfix">
                                        <div class="alerta">
                                            <?php echo '<br>'.$this->session->flashdata('msg_update');
//                                             if($auth['valid']){redirect(base_url(''), 'refresh');}else{echo $auth['mensaje'];}
                                            ?>
                                        </div>
                                    </div>                 
                </div>
   </section>