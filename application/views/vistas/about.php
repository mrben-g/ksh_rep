<section class="page-section pt-20 pb-40">
    <div class="container relative">
        <!-- Intro Text -->
        <div class="row">
            <div class="col-md-12">
                <div class="section-text align-left">
                    <h3 class="tituloproductos1 mt-0 mb-0">KSH</h3>
                    <div class="linea"></div>
                </div>
            </div>
        </div>
        <!-- End Intro Text -->
        <!--<div class="clearfix"></div>-->
        <div class="row" style="background: #fff;padding: 30px 10px;">
            <div class="col-md-12">
                    <div class="row mb-40">
                        <div class="col-md-6">
                            <div class="section-text text-justify">
                                Kuroda Soluciones Hidráulicas (KSH) es la unidad de negocio de KS Tubería, SA de CV, KST890224TU9, especializada en productos y asesoría en el manejo de agua y drenaje, que forma parte de Grupo Kuroda.<br><br>                                Brindamos soluciones a través de ingeniería a las necesidades de los Organismos Operadores de Agua,  constructoras, urbanizadoras, proyectistas, plantas tratadoras y actuamos como mayorista para comerciantes intermediarios.<br><br> Grupo Kuroda cuenta con más de 40 años de experiencia en plomería, tubería y sistemas de bombeo y con 20 años de experiencia atendiendo las necesidades en redes municipales de agua y drenaje en todo el país y proyectos Federales.
                            </div>
                        </div>
                        <div class="col-md-6 text-center">
                            <img src="<?php echo base_url('public/theme/images/about/nosotros1.png') ?>" alt="" style="max-height: 320px;" />
                        </div>
                    </div>
                    <div class="row mb-40">
                        <div class="col-md-6 text-center">
                            <img src="<?php echo base_url('public/theme/images/about/mapa.jpg') ?>" alt="" style="max-height: 286px;" />
                        </div>
                        <div class="col-md-6">
                            <div class="row mb-20">
                            <div class="col-md-12">
                                 <div class="section-text" style="text-align:justify;">Tenemos operaciones en toda la República Mexicana a través de nuestros centros de distribución en:<br>
                            </div>
                            </div>
                            </div>
                               <div class="row mb-20">
                            <div class="col-md-6">
                                <span class="section-text" style="display: block;"> • Monterrey, Nuevo León.</span> 
                                <span class="section-text" style="display: block;"> • Guadalajara, Jalisco.</span> 
                                </div>
                            <div class="col-md-6 section-text">
                                <span style="display: block;"> • Silao, Guanajuato.</span> 
                                <span style="display: block;"> • Ciudad de México.</span> 
                            </div>
                            </div>
                               <div class="row mb-20">
                            <div class="col-md-12">
                                <div class="section-text" style="text-align:justify;">Además de nuestras representaciones en:<br></div>
                            </div> 
                            </div>
                               <div class="row mb-20">
                            <div class="col-md-6 section-text">
                                <span  style="display: block;"> • Coahuila.</span> 
                                <span  style="display: block;"> • Tamaulipas.</span> 
                            </div>
                            <div class="col-md-6 section-text">
                                <span style="display: block;">• Querétaro.</span> 
                                <span style="display: block;">• Estado de México.</span> 
                            </div>
                              </div>
                                   
                        </div>
                    </div>
                   <div class="row mt-40 mb-40">
                       <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="section-text align-center">
                                <blockquote>
                                    <p>
                                        Buscamos la cercanía con nuestros clientes para brindarles nuestra experiencia, seguridad y confianza a través de productos de la más alta calidad y de asesoría y capacitación en su uso.<br>
                                        <span style="color: #333;"><a href="<?php echo base_url('curso_reparacion_fugas') ?>" style="color:#ff9e07;">CONOCE NUESTROS PROGRAMAS DE CURSOS</a></span>
                                    </p>
                                </blockquote>
                                
                                <div class="local-scroll">                                   
                                </div>                                
                            </div>
                        </div>
                    </div>
                       
                    </div>
                    <div class="row mb-40">
                        <div class="col-md-6 text-center">
                                <img src="<?php echo base_url('public/theme/images/about/aguahoynew.jpg') ?>" alt=""  />
                        </div>
                        <div class="col-md-6">
                            <div class="section-text text-justify">
                                Grupo Kuroda cuenta con el distintivo de Empresa Socialmente Responsable ESR. Apoyamos activamente las iniciativas de organizaciones para la conservación del agua y divulgación de tecnologías y mejores prácticas como es <span style="color: #333;"><a href="http://www.aguahoy.org">www.aguahoy.org</a></span> Inscríbete y recibirás información de eventos y tecnologías en el sector del agua.
                            </div>
                        </div>
                    </div>                    
                </div>
            </div>
      </div>
  </section>