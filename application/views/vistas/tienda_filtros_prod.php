<?php
$prod_id = $this->uri->segment(2);
$i = 0;
?>
<section class="page-section pt-20 pb-40">
    <div class="container relative">
        <!-- Intro Text -->
        <div class="row">
            <div class="col-md-12 mb-10">
                <div class="section-text align-left">
                    <h3 class="tituloproductos1 mt-0 mb-0" style="">Contra incendio</h3>
                    <div class="linea"></div>                        
                </div>                    
            </div>
            <div class="col-md-12 mb-30">
                <div class="section-text align-left">
                    Tienda > <a href="<?php echo base_url('tienda') ?>" style="color: #2d77b3;">Nuestros productos</a> > <a href="<?php echo base_url('tienda_filtros') ?>" style="color: #2d77b3;">Filtros</a> 
                                > <?php if($prod_id == 1){echo "FILTRO CANASTA SUPERIOR"; } ?>
                                             
                </div>
            </div>
        </div>
        <!-- End Intro Text -->
        <div class="clearfix"></div>
        <?php if($prod_id == 1){
    
                        $total_ananido=0;
                        if(isset($_SESSION['carro'])){
                            $cart_session =  $_SESSION['carro'];
                            if($cart_session){
                                if(!empty($cart_session)): foreach($cart_session as $cs=>$carro_shop):
                                    if($cs == "3121-3BX16"){$total_ananido = $carro_shop['cantidad']; }
                                    endforeach; else:
                                        
                                        endif;
                                    
                                    }
                                    
                                    }
                        ?>
           <div class="row">            
            <div class="col-md-12" style="padding: 30px;">
                <div class="row">
                   <div class="col-md-5 text-center">
                       <div style="height: 297px;width: 100%;background: #fff;margin-bottom: 2em;">
                         <img src="<?php  echo base_url('public/theme/images/filtros/filtros_SOLO.jpg') ?>" alt=""  style="width: 100%;margin-bottom: 2em;max-width:169px"/>
                     </div>

                   </div>
                    <div class="col-md-7" style="text-align:justify;">
                        <h6 class="titulo_producto">FILTRO CANASTA SUPERIOR</h6>
                        <fieldset></fieldset>                           
                        <div  class="section-text"  style="color:#000;">
Los filtros de canastilla superior retienen cuerpos extraños ocasionales con una menor pérdida de carga posible. Permiten una limpieza fácil del filtro, siempre que sea necesario y cuentan con la instalación de una purga en la parte inferior.
                        </div>  
                         <div class="clearfix"></div>                       
                        <div class="section-text pt-20 pb-20">                            
                            <?php echo form_open_multipart(base_url('welcome/crea_sesion_producto'), array('id' => 'frm-prod')) ?>
                    
                            <div class="row">                                
                                <div id="" class="col-md-6 text-left">
                                         Tamaño<br>
                                    <select id="tamano" name="tamano"  onchange="MuestraProdFiltro(this.value)">
                                       <option value="">Selecciona</option>                                            
                                            <?php if(!empty($this->data['filtro_canasta'])): foreach($this->data['filtro_canasta'] as $canasta):?>
                                            <option value="<?php echo $canasta['Tamanio'];?>"><?php echo $canasta['Tamanio'];?></option>
                                             <?php 
                                               endforeach; else:                                              
                                               endif;
                                             ?>                                                                
                                   </select>                               
                                </div>
                                <div class="col-md-6">
                                    
                                </div>
                            </div>

                            
                            
                             <br>
                            <input type="hidden" name="skus" id="skus" value="" />
                            <input type="hidden" name="nprod" id="nprod" value="" />
                            <input type="hidden" name="prices" id="prices" value="" />
                            <input type="hidden" name="tipo_moneda" id="tipo_moneda" value="" />
                            <input type="hidden" name="cantidad" id="cantidad" value="<?php if($total_ananido != 0){ echo $total_ananido+1;}else{ echo "1";}?>" />
                            <!--<input type="hidden" name="images" id="images" value="<?php ?>" />-->
                            <?php echo form_close(); ?>                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div id="filtros_de_productos">
                                <?php 
                              
                                ?>
                            </div>
                        </div>
                    </div>
   <?php } ?>
    

    </div>
</section>