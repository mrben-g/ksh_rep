<?php
$prod_id = "";
$prod_id_2 = "";
$prod_id_3 = "";
$prod_id_4 = "";
$prod_id_5 = "";
$prod_id_6 = "";
$prod_id_7 = "";
$prod_id_8 = "";
$prod_id_9 = "";

if ($this->uri->segment(1) == 'producto_reparacion_de_tuberia') { $prod_id = $this->uri->segment(2); }
if ($this->uri->segment(1) == 'producto_tuberia_conexiones') { $prod_id_2 = $this->uri->segment(2);}
if ($this->uri->segment(1) == 'producto_contra_incendio') { $prod_id_3 = $this->uri->segment(2);} 
if ($this->uri->segment(1) == 'producto_conduccion_derivacion') { $prod_id_4 = $this->uri->segment(2);}
if ($this->uri->segment(1) == 'producto_alcantarillado') {$prod_id_5 = $this->uri->segment(2); }
if ($this->uri->segment(1) == 'producto_sistema_bombeo') { $prod_id_6 = $this->uri->segment(2);}
if ($this->uri->segment(1) == 'producto_valvulas') { $prod_id_7 = $this->uri->segment(2);}
if ($this->uri->segment(1) == 'producto_filtros') { $prod_id_8 = $this->uri->segment(2);} 
if ($this->uri->segment(1) == 'producto_medidores') { $prod_id_9 = $this->uri->segment(2); }    
?>
<!-- Accordion -->
                            <dl class="accordion">
                                <dt>
                                    <a href="">REPARACIÓN DE TUBERÍA</a>
                                </dt>
                                <dd>
<a href="<?php echo base_url('producto_reparacion_de_tuberia/1') ?>" style="display: block;<?php if($prod_id == 1){ ?>color:#2d77b3;<?php } ?>">Abrazaderas de reparación</a>
<a href="<?php echo base_url('producto_reparacion_de_tuberia/2') ?>" style="display: block;<?php if($prod_id == 2){ ?>color:#2d77b3;<?php } ?>">Abrazaderas campana Bell Joint</a>
<a href="<?php echo base_url('producto_reparacion_de_tuberia/3') ?>" style="display: block;<?php if($prod_id == 3){ ?>color:#2d77b3;<?php } ?>">Juntas universales</a>
<a href="<?php echo base_url('producto_reparacion_de_tuberia/4') ?>" style="display: block;<?php if($prod_id == 4){ ?>color:#2d77b3;<?php } ?>">Coples dayton</a>
                                </dd>
                                <dt>
                                    <a href="">TUBERÍA Y CONEXIONES</a>
                                </dt>
                                <dd>

<a href="<?php echo base_url('producto_tuberia_conexiones/1') ?>" style="display: block;<?php if($prod_id_2 == 1){ ?>color:#2d77b3;<?php } ?>">Fierro vaciado (FOFO)</a>
<a href="<?php echo base_url('producto_tuberia_conexiones/2') ?>" style="display: block;<?php if($prod_id_2 == 2){ ?>color:#2d77b3;<?php } ?>">Hierro dúctil</a>
<a href="<?php echo base_url('producto_tuberia_conexiones/3') ?>" style="display: block;<?php if($prod_id_2 == 3){ ?>color:#2d77b3;<?php } ?>">PVC C-900</a>
<a href="<?php echo base_url('producto_tuberia_conexiones/4') ?>" style="display: block;<?php if($prod_id_2 == 4){ ?>color:#2d77b3;<?php } ?>">Alcantarillado S. 20 y 25</a>
<a href="<?php echo base_url('producto_tuberia_conexiones/5') ?>" style="display: block;<?php if($prod_id_2 == 5){ ?>color:#2d77b3;<?php } ?>">Pead grandes diametros</a>
<a href="<?php echo base_url('producto_tuberia_conexiones/6') ?>" style="display: block;<?php if($prod_id_2 == 6){ ?>color:#2d77b3;<?php } ?>">Acero</a>
<a href="<?php echo base_url('producto_tuberia_conexiones/7') ?>" style="display: block;<?php if($prod_id_2 == 7){ ?>color:#2d77b3;<?php } ?>">Cobre</a>
<a href="<?php echo base_url('producto_tuberia_conexiones/8') ?>" style="display: block;<?php if($prod_id_2 == 8){ ?>color:#2d77b3;<?php } ?>">PVC SI y SM</a>
<a href="<?php echo base_url('producto_tuberia_conexiones/9') ?>" style="display: block;<?php if($prod_id_2 == 9){ ?>color:#2d77b3;<?php } ?>">PVC C-80</a>
<a href="<?php echo base_url('producto_tuberia_conexiones/10') ?>" style="display: block;<?php if($prod_id_2 == 10){ ?>color:#2d77b3;<?php } ?>">PVC C-40</a>
<a href="<?php echo base_url('producto_tuberia_conexiones/11') ?>" style="display: block;<?php if($prod_id_2 == 11){ ?>color:#2d77b3;<?php } ?>">Polipropileno</a>
<a href="<?php echo base_url('producto_tuberia_conexiones/12') ?>" style="display: block;<?php if($prod_id_2 == 12){ ?>color:#2d77b3;<?php } ?>">DWV</a>                              
                                </dd>
                                <dt>
                                    <a href="">CONTRA INCENDIO</a>
                                </dt>
                                <dd>
<a href="<?php echo base_url('producto_contra_incendio/1') ?>" style="display: block;<?php if($prod_id_3 == 1){ ?>color:#2d77b3;<?php } ?>">Hidrantes barril seco</a>
<a href="<?php echo base_url('producto_contra_incendio/2') ?>" style="display: block;<?php if($prod_id_3 == 2){ ?>color:#2d77b3;<?php } ?>">Hidrantes barril húmedo</a>
<a href="<?php echo base_url('producto_contra_incendio/3') ?>" style="display: block;<?php if($prod_id_3 == 3){ ?>color:#2d77b3;<?php } ?>">Extensiones de hidrante</a>
<a href="<?php echo base_url('producto_contra_incendio/4') ?>" style="display: block;<?php if($prod_id_3 == 4){ ?>color:#2d77b3;<?php } ?>">Repuestos y kits de reparación</a>
<a href="<?php echo base_url('producto_contra_incendio/5') ?>" style="display: block;<?php if($prod_id_3 == 5){ ?>color:#2d77b3;<?php } ?>">Poste de indicador vertical</a>
<a href="<?php echo base_url('producto_contra_incendio/6') ?>" style="display: block;<?php if($prod_id_3 == 6){ ?>color:#2d77b3;<?php } ?>">Poste indicador de pared</a>
<a href="<?php echo base_url('producto_contra_incendio/7') ?>" style="display: block;<?php if($prod_id_3 == 7){ ?>color:#2d77b3;<?php } ?>">Equipo contra incendio integral</a>
<a href="<?php echo base_url('producto_contra_incendio/8') ?>" style="display: block;<?php if($prod_id_3 == 8){ ?>color:#2d77b3;<?php } ?>">Válvulas UL/FM</a>
<a href="<?php echo base_url('producto_contra_incendio/9') ?>" style="display: block;<?php if($prod_id_3 == 9){ ?>color:#2d77b3;<?php } ?>">Llaves y herramientas</a>
                                </dd>
                                 
                                <dt>
                                    <a href="">CONDUCCIÓN Y DERIVACIÓN</a>
                                </dt>
                                <dd>
<a href="<?php echo base_url('producto_conduccion_derivacion/1') ?>" style="display: block;<?php if($prod_id_4 == 1){ ?>color:#2d77b3;<?php } ?>">Tee partida</a>
<a href="<?php echo base_url('producto_conduccion_derivacion/2') ?>" style="display: block;<?php if($prod_id_4 == 2){ ?>color:#2d77b3;<?php } ?>">Collarines (abrazaderas)</a>
<a href="<?php echo base_url('producto_conduccion_derivacion/3') ?>" style="display: block;<?php if($prod_id_4 == 3){ ?>color:#2d77b3;<?php } ?>">Juntas universales</a>
<a href="<?php echo base_url('producto_conduccion_derivacion/4') ?>" style="display: block;<?php if($prod_id_4 == 4){ ?>color:#2d77b3;<?php } ?>">Juntas GIBAULT</a>
<a href="<?php echo base_url('producto_conduccion_derivacion/5') ?>" style="display: block;<?php if($prod_id_4 == 5){ ?>color:#2d77b3;<?php } ?>">Adaptadores bridados</a>
<a href="<?php echo base_url('producto_conduccion_derivacion/6') ?>" style="display: block;<?php if($prod_id_4 == 6){ ?>color:#2d77b3;<?php } ?>">Bridas rápidas</a>
<a href="<?php echo base_url('producto_conduccion_derivacion/7') ?>" style="display: block;<?php if($prod_id_4 == 7){ ?>color:#2d77b3;<?php } ?>">Taladro VEGA</a>
<a href="<?php echo base_url('producto_conduccion_derivacion/8') ?>" style="display: block;<?php if($prod_id_4 == 8){ ?>color:#2d77b3;<?php } ?>">Tomas domiciliarias</a>
<a href="<?php echo base_url('producto_conduccion_derivacion/9') ?>" style="display: block;<?php if($prod_id_4 == 9){ ?>color:#2d77b3;<?php } ?>">Herramientas</a>
                                </dd>
                                 <dt>
                                    <a href="">ALCANTARILLADO</a>
                                </dt>
                                <dd>
<a href="<?php echo base_url('producto_alcantarillado/1') ?>" style="display: block;<?php if($prod_id_5 == 1){ ?>color:#2d77b3;<?php } ?>">Brocales</a>
<a href="<?php echo base_url('producto_alcantarillado/2') ?>" style="display: block;<?php if($prod_id_5 == 2){ ?>color:#2d77b3;<?php } ?>">Escalones</a>
<a href="<?php echo base_url('producto_alcantarillado/3') ?>" style="display: block;<?php if($prod_id_5 == 3){ ?>color:#2d77b3;<?php } ?>">Válvula anti-retorno</a>
<a href="<?php echo base_url('producto_alcantarillado/4') ?>" style="display: block;<?php if($prod_id_5 == 4){ ?>color:#2d77b3;<?php } ?>">Mangas de empotramiento</a>
                                </dd>
                                 <dt>
                                    <a href="">SISTEMAS DE BOMBEO</a>
                                </dt>
                                <dd>
<a href="<?php echo base_url('producto_sistema_bombeo/1') ?>" style="display: block;<?php if($prod_id_6 == 1){ ?>color:#2d77b3;<?php } ?>">Bombas verticales</a>
<a href="<?php echo base_url('producto_sistema_bombeo/2') ?>" style="display: block;<?php if($prod_id_6 == 2){ ?>color:#2d77b3;<?php } ?>">Bombas horizontales bipartidas</a>
<a href="<?php echo base_url('producto_sistema_bombeo/3') ?>" style="display: block;<?php if($prod_id_6 == 3){ ?>color:#2d77b3;<?php } ?>">Bombas sumergibles de pozo profundo</a>
<a href="<?php echo base_url('producto_sistema_bombeo/4') ?>" style="display: block;<?php if($prod_id_6 == 4){ ?>color:#2d77b3;<?php } ?>">Bombas sumergibles para de lodos</a>
<a href="<?php echo base_url('producto_sistema_bombeo/5') ?>" style="display: block;<?php if($prod_id_6 == 5){ ?>color:#2d77b3;<?php } ?>">Bombas tragasolidos</a>
<a href="<?php echo base_url('producto_sistema_bombeo/6') ?>" style="display: block;<?php if($prod_id_6 == 6){ ?>color:#2d77b3;<?php } ?>">Bombas ANSI</a>
<a href="<?php echo base_url('producto_sistema_bombeo/7') ?>" style="display: block;<?php if($prod_id_6 == 7){ ?>color:#2d77b3;<?php } ?>">Equipos pre-ensamblados</a>
<a href="<?php echo base_url('producto_sistema_bombeo/8') ?>" style="display: block;<?php if($prod_id_6 == 8){ ?>color:#2d77b3;<?php } ?>">Dosificadoras</a>
                       
                                </dd>
                                 <dt>
                                   <a href="">VÁLVULAS Y CAJAS DE VÁLVULAS</a>
                                </dt>
                                <dd>
<a href="<?php echo base_url('producto_valvulas/1') ?>" style="display: block;<?php if($prod_id_7 == 1){ ?>color:#2d77b3;<?php } ?>">Válvulas compuerta</a>
<a href="<?php echo base_url('producto_valvulas/2') ?>" style="display: block;<?php if($prod_id_7 == 2){ ?>color:#2d77b3;<?php } ?>">Válvulas mariposa</a>
<a href="<?php echo base_url('producto_valvulas/3') ?>" style="display: block;<?php if($prod_id_7 == 3){ ?>color:#2d77b3;<?php } ?>">Válvulas cuchilla</a>
<a href="<?php echo base_url('producto_valvulas/4') ?>" style="display: block;<?php if($prod_id_7 == 4){ ?>color:#2d77b3;<?php } ?>">Válvulas de expulsión y/o admisión de aire</a>
<a href="<?php echo base_url('producto_valvulas/5') ?>" style="display: block;<?php if($prod_id_7 == 5){ ?>color:#2d77b3;<?php } ?>">Válvulas de control</a>
<a href="<?php echo base_url('producto_valvulas/6') ?>" style="display: block;<?php if($prod_id_7 == 6){ ?>color:#2d77b3;<?php } ?>">Válvulas check</a>
<a href="<?php echo base_url('producto_valvulas/7') ?>" style="display: block;<?php if($prod_id_7 == 7){ ?>color:#2d77b3;<?php } ?>">Válvulas plug</a>
<a href="<?php echo base_url('producto_valvulas/8') ?>" style="display: block;<?php if($prod_id_7 == 8){ ?>color:#2d77b3;<?php } ?>">Marco con tapa</a>
<a href="<?php echo base_url('producto_valvulas/9') ?>" style="display: block;<?php if($prod_id_7 == 9){ ?>color:#2d77b3;<?php } ?>">Registro telecospiado</a>
<!--<a href="<?php echo base_url('producto_valvulas/10') ?>?id=10" style="display: block;">Tapa superior y campana</a>-->
<a href="<?php echo base_url('producto_valvulas/11') ?>" style="display: block;<?php if($prod_id_7 == 11){ ?>color:#2d77b3;<?php } ?>">Válvulas backflow</a>
                                </dd>
                                 <dt>
                                  <a href="">FILTROS</a>
                                </dt>
                                <dd>
<a href="<?php echo base_url('producto_filtros/1') ?>" style="display: block;<?php if($prod_id_8 == 1){ ?>color:#2d77b3;<?php } ?>">Filtro canasta superior</a>
<a href="<?php echo base_url('producto_filtros/2') ?>" style="display: block;<?php if($prod_id_8 == 2){ ?>color:#2d77b3;<?php } ?>">Filtro canasta</a>
<a href="<?php echo base_url('producto_filtros/3') ?>" style="display: block;<?php if($prod_id_8 == 3){ ?>color:#2d77b3;<?php } ?>">Filtro duplex</a>
<a href="<?php echo base_url('producto_filtros/4') ?>" style="display: block;<?php if($prod_id_8 == 4){ ?>color:#2d77b3;<?php } ?>">Filtro yee</a>
                                </dd>
                                 <dt>
                                  <a href="">MEDIDORES</a>
                                </dt>
                                <dd>
<a href="<?php echo base_url('producto_medidores/1') ?>" style="display: block;<?php if($prod_id_9 == 1){ ?>color:#2d77b3;<?php } ?>">Residenciales</a>
<a href="<?php echo base_url('producto_medidores/2') ?>" style="display: block;<?php if($prod_id_9 == 2){ ?>color:#2d77b3;<?php } ?>">Comercial</a>
                                </dd>
                                
                            </dl>
                            <!-- End Accordion -->