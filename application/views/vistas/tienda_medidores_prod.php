<?php
$prod_id = $this->uri->segment(2);
$i = 0;
?>
<section class="page-section pt-20 pb-40">
    <div class="container relative">
        <!-- Intro Text -->
        <div class="row">
            <div class="col-md-12 mb-10">
                <div class="section-text align-left">
                    <h3 class="tituloproductos1 mt-0 mb-0" style="">Contra incendio</h3>
                    <div class="linea"></div>                        
                </div>                    
            </div>
            <div class="col-md-12 mb-30">
                <div class="section-text align-left">
                    Tienda > <a href="<?php echo base_url('tienda') ?>" style="color: #2d77b3;">Nuestros productos</a> > <a href="<?php echo base_url('tienda_medidores') ?>" style="color: #2d77b3;">Medidores</a> 
                                > <?php if($prod_id == 1){echo "RESIDENCIALES"; } ?>
                                   <?php if($prod_id == 2){echo "COMERCIAL"; } ?>          
                </div>
            </div>
        </div>
        <!-- End Intro Text -->
        <div class="clearfix"></div>
        <?php if($prod_id == 1){
    
                        $total_ananido=0;
                        if(isset($_SESSION['carro'])){
                            $cart_session =  $_SESSION['carro'];
                            if($cart_session){
                                if(!empty($cart_session)): foreach($cart_session as $cs=>$carro_shop):
                                    if($cs == "3121-3BX16"){$total_ananido = $carro_shop['cantidad']; }
                                    endforeach; else:
                                        
                                        endif;
                                    
                                    }
                                    
                                    }
                        ?>
           <div class="row">            
            <div class="col-md-12" style="padding: 30px;">
                <div class="row">
                   <div class="col-md-5 text-center">
                       <div style="height: 297px;width: 100%;background: #fff;margin-bottom: 2em;">
                         <img src="<?php  echo base_url('public/theme/images/medidores/medidores.jpg') ?>" alt=""  style="width: 100%;margin-bottom: 2em;max-width:325px"/>
                     </div>

                   </div>
                    <div class="col-md-7" style="text-align:justify;">
                        <h6 class="titulo_producto">RESIDENCIALES</h6>
                        <fieldset></fieldset>                           
                        <div  class="section-text"  style="color:#000;">
Un medidor de agua es un artefacto que permite contabilizar la cantidad de agua que pasa a través de él y es utilizado en las instalaciones residenciales acueductos para realizar los cobros pertinentes a los usuarios del mismo.
                        </div>  
                         <div class="clearfix"></div>                       
                        <div class="section-text pt-20 pb-20">                            
                            <?php echo form_open_multipart(base_url('welcome/crea_sesion_producto'), array('id' => 'frm-prod')) ?>
                    
                            <div class="row">                                
                                <div id="" class="col-md-6 text-left">
                                         Tamaño<br>
                                    <select id="tamano" name="tamano"  onchange="MuestraTipoRoscaResidencial(this.value)">
                                       <option value="">Selecciona</option>                                            
                                            <?php if(!empty($this->data['tamano_residencial'])): foreach($this->data['tamano_residencial'] as $tam):?>
                                            <option value="<?php echo $tam['Tamano'];?>"><?php echo $tam['Tamano'];?></option>
                                             <?php 
                                               endforeach; else:                                              
                                               endif;
                                             ?>                                                                
                                   </select>                               
                                </div>
                                <div class="col-md-6">
                                    
                                </div>
                            </div>
                            <div class="row">                                
                                <div id="lista_rosca" class="col-md-6 text-left">
                                    Tipo de Rosca<br>
                                    <select id="rosca" name="rosca" >
                                       <option value="">Selecciona</option>                                              
                                   </select>       
                                </div>
                                 <div class="col-md-6">
                                    
                                </div>
                            </div>
                            <div class="row">                                
                                <div id="lista_material" class="col-md-6 text-left">
                                    Material<br>
                                    <select id="material" name="material">
                                       <option value="">Selecciona</option>                                        
                                   </select>                                  
                                </div> 
                                 <div class="col-md-6">
                                    
                                </div>
                            </div>
                                <div class="row">                                
                                <div id="lista_tipo_medidor" class="col-md-6 text-left">
                                    Tipo de Medidor<br>
                                    <select id="medidor" name="medidor" onchange="MuestraProdHidranteSeco(this.value)">
                                       <option value="">Selecciona</option>
                                              <?php if(!empty($this->data['tipo_junta'])): foreach($this->data['tipo_junta'] as $tipo_junta):?>
                                       <option value="<?php echo $tipo_junta['TipoJunta'];?>"><?php echo $tipo_junta['TipoJunta'];?></option>
                                        <?php 
                                          endforeach; else:
                                              
                                              endif;     
                                        ?>
                                   </select>                                  
                                </div>
                                <div class="col-md-6">
                                    
                                </div>
                            </div>
                            
                             <br>
                            <input type="hidden" name="skus" id="skus" value="" />
                            <input type="hidden" name="nprod" id="nprod" value="" />
                            <input type="hidden" name="prices" id="prices" value="" />
                            <input type="hidden" name="tipo_moneda" id="tipo_moneda" value="" />
                            <input type="hidden" name="cantidad" id="cantidad" value="<?php if($total_ananido != 0){ echo $total_ananido+1;}else{ echo "1";}?>" />
                            <!--<input type="hidden" name="images" id="images" value="<?php ?>" />-->
                            <?php echo form_close(); ?>                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div id="filtros_de_productos">
                                <?php 
                              
                                ?>
                            </div>
                        </div>
                    </div>
   <?php } ?>
    <?php if($prod_id == 2){
    
                        $total_ananido=0;
                        if(isset($_SESSION['carro'])){
                            $cart_session =  $_SESSION['carro'];
                            if($cart_session){
                                if(!empty($cart_session)): foreach($cart_session as $cs=>$carro_shop):
                                    if($cs == "3121-3BX16"){$total_ananido = $carro_shop['cantidad']; }
                                    endforeach; else:
                                        
                                        endif;
                                    
                                    }
                                    
                                    }
                        ?>
           <div class="row">            
            <div class="col-md-12" style="padding: 30px;">
                <div class="row">
                   <div class="col-md-5 text-center">
                       <div style="height: 297px;width: 100%;background: #fff;margin-bottom: 2em;">
                         <img src="<?php  echo base_url('public/theme/images/medidores/medidor.jpg') ?>" alt=""  style="width: 100%;margin-bottom: 2em;max-width:216px"/>
                     </div>

                   </div>
                    <div class="col-md-7" style="text-align:justify;">
                        <h6 class="titulo_producto">COMERCIAL</h6>
                        <fieldset></fieldset>                           
                        <div  class="section-text"  style="color:#000;">
Los medidores comerciales, están diseñados para medir altos caudales con una mínima pérdida de carga, ofreciendo alta confiabilidad y exactitud de funcionamiento por un largo tiempo de uso. Están diseñados y producidos de acuerdo a los requisitos de la norma Internacional ISO 4064 Clase B. y cumplen con la norma Mexicana NOM-008-SCFI-2002.
<br><br>
Este tipo de medidores se clasifican igual que los residenciales pero de un diámetro mayor que va de 1” a diámetros mayores. Los más usados en la industria son los electromagnéticos ya que son más precisos a los demás tipos ya que no posee partes móviles en contacto con el agua y para su instalación requiere una pequeña longitud de tramo recto aguas arriba. Su error de medición ¼ - ½ % y pueden manejar líquidos con sólidos en suspensión
                        </div>  
                         <div class="clearfix"></div>                       
                        <div class="section-text pt-20 pb-20">                            
                            <?php echo form_open_multipart(base_url('welcome/crea_sesion_producto'), array('id' => 'frm-prod')) ?>
                    
                            <div class="row">                                
                                <div id="" class="col-md-6 text-left">
                                         Tamaño<br>
                                    <select id="tamano" name="tamano"  onchange="MuestraTipoRoscaComercial(this.value)">
                                       <option value="">Selecciona</option>                                            
                                            <?php if(!empty($this->data['tamano_comercial'])): foreach($this->data['tamano_comercial'] as $tam):?>
                                            <option value="<?php echo $tam['Tamano'];?>"><?php echo $tam['Tamano'];?></option>
                                             <?php 
                                               endforeach; else:                                              
                                               endif;
                                             ?>                                                                
                                   </select>                               
                                </div>
                                <div class="col-md-6">
                                    
                                </div>
                            </div>
                            <div class="row">                                
                                <div id="lista_rosca" class="col-md-6 text-left">
                                    Tipo de Rosca<br>
                                    <select id="rosca" name="rosca" >
                                       <option value="">Selecciona</option>                                              
                                   </select>       
                                </div>
                                 <div class="col-md-6">
                                    
                                </div>
                            </div>
                            <div class="row">                                
                                <div id="lista_material" class="col-md-6 text-left">
                                    Material<br>
                                    <select id="material" name="material">
                                       <option value="">Selecciona</option>                                        
                                   </select>                                  
                                </div> 
                                 <div class="col-md-6">
                                    
                                </div>
                            </div>
                                <div class="row">                                
                                <div id="lista_tipo_medidor" class="col-md-6 text-left">
                                    Tipo de Medidor<br>
                                    <select id="medidor" name="medidor" onchange="MuestraProdHidranteSeco(this.value)">
                                       <option value="">Selecciona</option>
                                              <?php if(!empty($this->data['tipo_junta'])): foreach($this->data['tipo_junta'] as $tipo_junta):?>
                                       <option value="<?php echo $tipo_junta['TipoJunta'];?>"><?php echo $tipo_junta['TipoJunta'];?></option>
                                        <?php 
                                          endforeach; else:
                                              
                                              endif;     
                                        ?>
                                   </select>                                  
                                </div>
                                <div class="col-md-6">
                                    
                                </div>
                            </div>
                            
                             <br>
                            <input type="hidden" name="skus" id="skus" value="" />
                            <input type="hidden" name="nprod" id="nprod" value="" />
                            <input type="hidden" name="prices" id="prices" value="" />
                            <input type="hidden" name="tipo_moneda" id="tipo_moneda" value="" />
                            <input type="hidden" name="cantidad" id="cantidad" value="<?php if($total_ananido != 0){ echo $total_ananido+1;}else{ echo "1";}?>" />
                            <!--<input type="hidden" name="images" id="images" value="<?php ?>" />-->
                            <?php echo form_close(); ?>                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div id="filtros_de_productos">
                                <?php 
                              
                                ?>
                            </div>
                        </div>
                    </div>
   <?php } ?>
    

    </div>
</section>