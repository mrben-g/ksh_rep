<?php
//$prod_id = $this->input->get('id', TRUE);
$prod_id = $this->uri->segment(2);
$i = 0;
?>
<section class="page-section pt-20 pb-40">
    <div class="container relative">
        <!-- Intro Text -->
        <div class="row">
            <div class="col-md-12 mb-10">
                <div class="section-text align-left">
                    <h3 class="tituloproductos1 mt-0 mb-0" style="">Contra incendio</h3>
                    <div class="linea"></div>                        
                </div>                    
            </div>
            <div class="col-md-12 mb-30">
                <div class="section-text align-left">
                    Tienda > <a href="<?php echo base_url('tienda') ?>" style="color: #2d77b3;">Nuestros productos</a> > <a href="<?php echo base_url('tienda_conduccion_derivacion') ?>" style="color: #2d77b3;">Conducción y derivación</a> 
                                > <?php if($prod_id == 1){echo "TEE PARTIDA"; } ?>
                                   <?php if($prod_id == 2){echo "COLLARINES (ABRAZADERAS)"; } ?> 
                                   <?php if($prod_id == 3){echo "JUNTAS UNIVERSALES"; } ?> 
                                   <?php if($prod_id == 4){echo "ADAPTADORES BRIDADOS"; } ?>
                                   <?php if($prod_id == 5){echo "BRIDAS RÁPIDAS"; } ?>
                </div>
            </div>
        </div>
        <!-- End Intro Text -->
        <div class="clearfix"></div>
        <?php if($prod_id == 1){
    
                        $total_ananido=0;
                        if(isset($_SESSION['carro'])){
                            $cart_session =  $_SESSION['carro'];
                            if($cart_session){
                                if(!empty($cart_session)): foreach($cart_session as $cs=>$carro_shop):
                                    if($cs == "3121-3BX16"){$total_ananido = $carro_shop['cantidad']; }
                                    endforeach; else:
                                        
                                        endif;
                                    
                                    }
                                    
                                    }
                        ?>
           <div class="row">            
            <div class="col-md-12" style="padding: 30px;">
                <div class="row">
                   <div class="col-md-5 text-center">
                       <div style="height: 297px;width: 100%;background: #fff;margin-bottom: 2em;">
                         <img src="<?php  echo base_url('public/theme/images/bridas/tee_partida_3490.png') ?>" alt=""  style="width: 100%;margin-bottom: 2em;max-width: 250px"/>
                     </div>

                   </div>
                    <div class="col-md-7" style="text-align:justify;">
                        <h6 class="titulo_producto">TEE PARTIDA</h6>
                        <fieldset></fieldset>                           
                        <div  class="section-text"  style="color:#000;">
                            Este tipo de TEE PARTIDA es el método más rápido, económico y simple para hacer derivaciones sobre líneas de conducción de agua potable. Una de sus principales ventajas es que no necesitas interrumpir el suministro de agua.<br><br>
                            
                            <b>Brida:</b> Acero inoxidable 304 (18-8) según normas ASTM A240 ó acero al carbón según norma AWWA C115. Bridas según norma ISO disponibles.<br>
                            <b>Cuerpo:</b> Acero inoxidable tipo 304 (18-8) según norma ASTM A240.<br>
                            <b>Empaque de Paneles:</b> Check-O-Seal. Espesor 3/16" con diseño waffle que forma una red que garantiza el comportamiento del sello bajo condiciones cambiantes de presión. Ideal para agua potable, agua salada, ácidos diluidos, bases, y algunos compuestos químicos, temperatura de trabajo -40ºC (-40ºF) a 110ºC (225ºF). Según Norma ASTM D-2000.<br>
                            <b>Empaque de Salida:</b> "Hydro-Twin" patentado y diseñado con doble "O-Ring" para incorporar tanto los esfuerzos mecánicos como hidráulicos que afectan a la dinámica de sellado de la pieza.<br>
                            <b>Tornillos y Tuercas:</b> Acero inoxidable 304 (18-8) según normas ASTM A193 y ASTM A194.<br>
                            <b>Arandela:</b> Acero inoxidable tipo 304 (18-8) según norma ASTM 240
                        </div>  
                         <div class="clearfix"></div>                       
                        <div class="section-text pt-20 pb-20">                            
                            <?php echo form_open_multipart(base_url('welcome/crea_sesion_producto'), array('id' => 'frm-prod')) ?>
                    
                            <div class="row">                                
                                <div id="" class="col-md-6 text-left">
                                    Modelo<br>
                                    <select id="modelo" name="modelo" >
                                       <option value="">Selecciona</option>                                            
                                       <option value="3480AI">3480</option>
                                       <option value="3490AI">3490</option> 
                                   </select>                                  
                                </div>
                                <div class="col-md-6">
                                    
                                </div>
                            </div>
                             
                           <div class="row">                                
                                <div id="" class="col-md-6 text-left">
                                    Diámetro Nominal<br>
                                    <select id="diam_nom" name="diam_nom" onchange="MuestraRangoPartida(this.value)">
                                       <option value="">Selecciona</option>
                                        <?php if(!empty($this->data['diam_partida'])): foreach($this->data['diam_partida'] as $diam):?>
                                       <option value="<?php echo $diam['DiametroNominal'];?>"><?php echo $diam['DiametroNominal'];?></option>
                                        <?php 
                                          endforeach; else:                                              
                                          endif;
                                        ?>
                                   </select>                                  
                                </div>
                                <div class="col-md-6">
                                    
                                </div>
                            </div>
                            
                            <div class="row">                                
                                <div id="lista_rango" class="col-md-6 text-left">
                                    Rango del Diámetro<br>
                                    <select id="rango" name="rango" >
                                       <option value="">Selecciona</option>
                                              <?php if(!empty($this->data['rang_partida'])): foreach($this->data['rang_partida'] as $rango):?>
                                       <option value="<?php echo $rango['RangoDiametro'];?>"><?php echo $rango['RangoDiametro'];?></option>
                                        <?php 
                                          endforeach; else:
                                              
                                              endif;     
                                        ?>
                                   </select>                                  
                                </div>
                                <div class="col-md-6">                                    
                                </div>
                            </div>
                            
                            <div class="row">                                
                                <div id="lista_brida" class="col-md-6 text-left">
                                    Brida<br>
                                    <select id="brida" name="brida" onchange="MuestraProdTPartida(this.value)">
                                       <option value="">Selecciona</option>
                                              <?php if(!empty($this->data['tipo_junta'])): foreach($this->data['tipo_junta'] as $tipo_junta):?>
                                       <option value="<?php echo $tipo_junta['TipoJunta'];?>"><?php echo $tipo_junta['TipoJunta'];?></option>
                                        <?php 
                                          endforeach; else:                                              
                                              endif;     
                                        ?>
                                   </select>       
                                </div>
                                <div class="col-md-6">                                    
                                </div>
                            </div>
                             <br>
                            <input type="hidden" name="skus" id="skus" value="" />
                            <input type="hidden" name="nprod" id="nprod" value="" />
                            <input type="hidden" name="prices" id="prices" value="" />
                            <input type="hidden" name="tipo_moneda" id="tipo_moneda" value="" />
                            <input type="hidden" name="cantidad" id="cantidad" value="<?php if($total_ananido != 0){ echo $total_ananido+1;}else{ echo "1";}?>" />
                            <!--<input type="hidden" name="images" id="images" value="<?php ?>" />-->
                            <?php echo form_close(); ?>                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div id="filtros_de_productos">
                                <?php 
                              
                                ?>
                            </div>
                        </div>
                    </div>
   <?php } ?>
    <?php if($prod_id == 2){
    
                        $total_ananido=0;
                        if(isset($_SESSION['carro'])){
                            $cart_session =  $_SESSION['carro'];
                            if($cart_session){
                                if(!empty($cart_session)): foreach($cart_session as $cs=>$carro_shop):
                                    if($cs == "3121-3BX16"){$total_ananido = $carro_shop['cantidad']; }
                                    endforeach; else:
                                        
                                        endif;
                                    
                                    }
                                    
                                    }
                        ?>
           <div class="row">            
            <div class="col-md-12" style="padding: 30px;">
                <div class="row">
                   <div class="col-md-5 text-center">
                       <div style="height: 297px;width: 100%;background: #fff;margin-bottom: 2em;">
                         <img src="<?php  echo base_url('public/theme/images/conduccion_derivacion/collarines.jpg') ?>" alt=""  style="width: 100%;margin-bottom: 2em;max-width: 190px"/>
                     </div>

                   </div>
                    <div class="col-md-7" style="text-align:justify;">
                        <h6 class="titulo_producto">COLLARINES (ABRAZADERAS)</h6>
                        <fieldset></fieldset>                           
                        <div  class="section-text"  style="color:#000;">
Los collarines también conocidos como abrazaderas proporcionan un método conveniente para hacer instalaciones en cualquier clase de tubería para agua de hierro dúctil, PVC y asbesto cemento. Están disponibles en bronce y acero inoxidable. Unos de sus principales usos es el de separación de tuberías y para reparar irregularidades y daños en los collarines de PVC que originan fugas. 
<br><br>
También pueden reforzar el punto crítico de conexiones al tubo dada a su estructura rígida y su empaque especial. Una de sus principales ventajas es que pueden ser instalados sin interrumpir el suministro de agua.
                        </div>  
                         <div class="clearfix"></div>                       
                        <div class="section-text pt-20 pb-20">                            
                            <?php echo form_open_multipart(base_url('welcome/crea_sesion_producto'), array('id' => 'frm-prod')) ?>
                            <div class="row">
                                <div class="col-md-5">
                            <div class="row">                 
                                <div id="" class="col-md-12 text-left">
                                    Modelo<br>
                                    <select id="modelo" name="modelo"  onchange="MuestraInfoCollarin(this.value); MuestraDiamCollarin(this.value)">
                                       <option value="">Selecciona</option>                                            
                                       <option value="3411">3411</option>
                                       <option value="3415">3415</option>
                                       <option value="3417">3417</option>
                                       <option value="3422AI">3422</option>                                       
                                   </select>                                  
                                </div>                              
                            </div>
                             
                           <div class="row">                                
                                <div id="lista_diam" class="col-md-12 text-left">
                                    Diámetro Nominal<br>
                                    <select id="diam_nom" name="diam_nom" onchange="MuestraRangoCollarin(this.value)">
                                       <option value="">Selecciona</option>
                                        <?php if(!empty($this->data['diam_partida'])): foreach($this->data['diam_partida'] as $diam):?>
                                       <option value="<?php echo $diam['DiametroNominal'];?>"><?php echo $diam['DiametroNominal'];?></option>
                                        <?php 
                                          endforeach; else:                                              
                                          endif;
                                        ?>
                                   </select>                                  
                                </div>                               
                            </div>
                            
                            <div class="row">                                
                                <div id="lista_rango" class="col-md-12 text-left">
                                    Rango del Diámetro<br>
                                    <select id="rango" name="rango" >
                                       <option value="">Selecciona</option>
                                              <?php if(!empty($this->data['rang_partida'])): foreach($this->data['rang_partida'] as $rango):?>
                                       <option value="<?php echo $rango['RangoDiametro'];?>"><?php echo $rango['RangoDiametro'];?></option>
                                        <?php 
                                          endforeach; else:
                                              
                                              endif;     
                                        ?>
                                   </select>                                  
                                </div>                                
                            </div>
                            
                            <div class="row">                                
                                <div id="lista_salida" class="col-md-12 text-left">
                                    Salida<br>
                                    <select id="salida" name="salida" >
                                       <option value="">Selecciona</option>                                             
                                   </select>       
                                </div>                               
                            </div>
                            <div class="row">                                
                                <div id="lista_rosca" class="col-md-12 text-left">
                                    Tipo de Rosca<br>
                                    <select id="rosca" name="rosca" onchange="MuestraProdTPartida(this.value)">
                                       <option value="">Selecciona</option>
                                              <?php if(!empty($this->data['tipo_junta'])): foreach($this->data['tipo_junta'] as $tipo_junta):?>
                                       <option value="<?php echo $tipo_junta['TipoJunta'];?>"><?php echo $tipo_junta['TipoJunta'];?></option>
                                        <?php 
                                          endforeach; else:                                              
                                              endif;     
                                        ?>
                                   </select>       
                                </div>                               
                            </div>
                                </div>
                                <div class="col-md-7">
                                         <div id="hierro" class="section-text"  style="color:#000;display: none;">
                                             <b>Cuerpo Fundido:</b> Hierro dúctil según la norma ASTM A536.<br>
                                        <b>Zunchos:</b> Acero al carbono por AISI 1080, con electro-galvanizado-di cromato acabado para resistencia a la corrosión.<br>
                                        <b>Tuercas:</b> Acero al carbono ASTM A563, con acabado electrogalvanizado-di cromato para la resistencia a la corrosión.<br>
                                        <b>Empaque:</b> NBR según ASTM D2000 con un diseño de doble anillo unido de forma permanente en el cuerpo del collarín, con un rango de temperatura de -25°F a + 248°F.<br>
                                        <b>Acabado:</b> Pintura epóxica.<br>


                                    </div>
                                    <div id="acero" class="section-text"  style="color:#000;display: none;">
                                        <b>Cuerpo y Arandela de Ensamble:</b> Tipo 304 (18-8) de acero inoxidable según norma ASTM A240.<br>
                                        <b>Tornillos y Tuercas:</b> Tipo 304 (18-8) de acero inoxidable según normas ASTM A193 y A194.<br>
                                        <b>NOTA:</b> Las tuercas hexagonales están equipadas con recubrimiento para evitar el estancamiento.<br>
                                        <b>Arandelas:</b> Tipo delrin según norma ASTM D6778.<br>
                                        <b>Orejas de Ensamble:</b> Tipo 304 (18-8) de acero inoxidable según normas ASTM A240.<br>
                                        <b>Salida:</b> Tipo 304 (18-8) de acero inoxidable según norma ASTM A276.<br>
                                        <b>Empaque:</b> NBR según ASTM D2000; empaque con diseño patentado de doble TwinSeal® que incorporan tanto fuerzas hidrostáticas y mecánicas para afectar un sello dinámico, con un rango de temperatura de -25°F a + 248°F.<br><br>
                                        <b>Ancho del Cuerpo:</b> 8"<br>
                                        <b>Ancho del Zuncho:</b> 2"<br>
                                    </div>
                                </div>
                            </div>
                            
                            
                            
                             <br>
                            <input type="hidden" name="skus" id="skus" value="" />
                            <input type="hidden" name="nprod" id="nprod" value="" />
                            <input type="hidden" name="prices" id="prices" value="" />
                            <input type="hidden" name="tipo_moneda" id="tipo_moneda" value="" />
                            <input type="hidden" name="cantidad" id="cantidad" value="<?php if($total_ananido != 0){ echo $total_ananido+1;}else{ echo "1";}?>" />
                            <!--<input type="hidden" name="images" id="images" value="<?php ?>" />-->
                            <?php echo form_close(); ?>                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div id="filtros_de_productos">
                                <?php 
                              
                                ?>
                            </div>
                        </div>
                    </div>
   <?php } ?>     
<?php if($prod_id == 3){
    
                        $total_ananido=0;
                        if(isset($_SESSION['carro'])){
                            $cart_session =  $_SESSION['carro'];
                            if($cart_session){
                                if(!empty($cart_session)): foreach($cart_session as $cs=>$carro_shop):
                                    if($cs == "3121-3BX16"){$total_ananido = $carro_shop['cantidad']; }
                                    endforeach; else:
                                        
                                        endif;
                                    
                                    }
                                    
                                    }
                        ?>
           <div class="row">            
            <div class="col-md-12" style="padding: 30px;">
                <div class="row">
                   <div class="col-md-5 text-center">
                       <div style="height: 297px;width: 100%;background: #fff;margin-bottom: 2em;">
                         <img src="<?php  echo base_url('public/theme/images/reparacion_tuberia/juntas_universales.jpg') ?>" alt=""  style="width: 100%;margin-bottom: 2em;max-width:350px"/>
                     </div>

                   </div>
                    <div class="col-md-7" style="text-align:justify;">
                        <h6 class="titulo_producto">JUNTAS UNIVERSALES</h6>
                        <fieldset></fieldset>                           
                        <div  class="section-text"  style="color:#000;">
Se les conoce así porque pueden reparar una fuga en todo tipo de tubería, teniendo una conexión flexible y segura en reparaciones en donde las secciones de tubería dañadas son demasiado largas para utilizar una abrazadera de reparación.                      
                        </div>  
                        
                         <h6 class="titulo_producto" style="font-size: 16px;">Ventajas</h6>
                        <div>
                            <ul id="ven" style="">
                                <li><span>Empaque con diseño cónico para garantizar un desempeño 100% bajo condiciones diferentes de presión.</span></li>
                                <li><span>Se adaptan a cualquier tipo de material</span></li>                             
                            </ul>
                        </div>
                         <div class="clearfix"></div>                       
                        <div class="section-text pt-20 pb-20">                            
                            <?php echo form_open_multipart(base_url('welcome/crea_sesion_producto'), array('id' => 'frm-prod')) ?>
                            <div class="row">
                                <div class="col-md-5">
                            <div class="row">                 
                                <div id="" class="col-md-12 text-left">                                    
                                     Material<br>
                                     <label for="hie" style="display: inline-block;">
                                     <input type="checkbox" name="material" id="hie" value="1"  onchange="MuestraDiamJU(this.value)">  
                                     Hierro ductil</label>
                                     <label for="ace" style="display: inline-block;">
                                     <input type="checkbox" name="material" id="ace" value="2"  onchange="MuestraDiamJU(this.value)"> 
                                    Acero Inoxidable</label>
                                </div>                              
                            </div>
                             
                           <div class="row">                                
                                <div id="lista_diam" class="col-md-12 text-left">
                                    Diámetro Nominal<br>
                                    <select id="diam_nom" name="diam_nom" onchange="MuestraRangoJU(this.value)">
                                       <option value="">Selecciona</option>                                        
                                   </select>                                  
                                </div>                               
                            </div>
                            
                            <div class="row">                                
                                <div id="lista_rango" class="col-md-12 text-left">
                                    Rango del Diámetro<br>
                                    <select id="rango" name="rango" >
                                       <option value="">Selecciona</option>                                            
                                   </select>                                  
                                </div>                                
                            </div>
                            
                            <div class="row">                                
                                <div id="lista_barril" class="col-md-12 text-left">
                                    Tamaño de Barril<br>
                                    <select id="barril" name="barril" >
                                       <option value="">Selecciona</option>                                             
                                   </select>       
                                </div>                               
                            </div>
                            
                                </div>
                                <div class="col-md-7">
                                         
                                </div>
                            </div>
                            
                            
                            
                             <br>
                            <input type="hidden" name="skus" id="skus" value="" />
                            <input type="hidden" name="nprod" id="nprod" value="" />
                            <input type="hidden" name="prices" id="prices" value="" />
                            <input type="hidden" name="tipo_moneda" id="tipo_moneda" value="" />
                            <input type="hidden" name="cantidad" id="cantidad" value="<?php if($total_ananido != 0){ echo $total_ananido+1;}else{ echo "1";}?>" />
                            <!--<input type="hidden" name="images" id="images" value="<?php ?>" />-->
                            <?php echo form_close(); ?>                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div id="filtros_de_productos">
                                <?php 
                              
                                ?>
                            </div>
                        </div>
                    </div>
   <?php } ?>             
        <?php if($prod_id == 4){
    
                        $total_ananido=0;
                        if(isset($_SESSION['carro'])){
                            $cart_session =  $_SESSION['carro'];
                            if($cart_session){
                                if(!empty($cart_session)): foreach($cart_session as $cs=>$carro_shop):
                                    if($cs == "3121-3BX16"){$total_ananido = $carro_shop['cantidad']; }
                                    endforeach; else:
                                        
                                        endif;
                                    
                                    }
                                    
                                    }
                        ?>
           <div class="row">            
            <div class="col-md-12" style="padding: 30px;">
                <div class="row">
                   <div class="col-md-5 text-center">
                       <div style="height: 297px;width: 100%;background: #fff;margin-bottom: 2em;">
                         <img src="<?php  echo base_url('public/theme/images/conduccion_derivacion/adapt_bridad_ps_1.jpg') ?>" alt=""  style="width: 100%;margin-bottom: 2em;max-width:350px"/>
                     </div>

                   </div>
                    <div class="col-md-7" style="text-align:justify;">
                        <h6 class="titulo_producto">ADAPTADORES BRIDADOS</h6>
                        <fieldset></fieldset>                           
                        <div  class="section-text"  style="color:#000;">
         Este tipo de adaptadores se conectan fácil y rápidamente a los extremos del tubo de acero, PBVC hierro y asbesto cemento. Es una combinación de junta y brida convencional.
                        </div>
                         
                         <div class="clearfix"></div>
                        <div class="section-text pt-20 pb-20">                            
                            <?php echo form_open_multipart(base_url('welcome/crea_sesion_producto'), array('id' => 'frm-prod')) ?>
                            <div class="row">
                                <div class="col-md-5">
                            <div class="row">                 
                                <div id="" class="col-md-12 text-left">                                    
                                    Modelo<br>
                                    <select id="modelo" name="modelo"  onchange="MuestraDiamAdapt(this.value)">
                                       <option value="">Selecciona</option>                                            
                                       <option value="3521">3521</option>
                                       <option value="3526">3526</option>                                                                             
                                   </select>         
                                </div>                              
                            </div>
                             
                           <div class="row">                                
                                <div id="lista_diam" class="col-md-12 text-left">
                                    Diámetro Nominal<br>
                                    <select id="diam_nom" name="diam_nom" >
                                       <option value="">Selecciona</option>                                        
                                   </select>                                  
                                </div>                               
                            </div>
                            
                            <div class="row">                                
                                <div id="lista_rango" class="col-md-12 text-left">
                                    Rango del Diámetro<br>
                                    <select id="rango" name="rango" >
                                       <option value="">Selecciona</option>                                            
                                   </select>                                  
                                </div>                                
                            </div>
                            
                                </div>
                                <div class="col-md-7">
                                         
                                </div>
                            </div>
                            
                            
                            
                             <br>
                            <input type="hidden" name="skus" id="skus" value="" />
                            <input type="hidden" name="nprod" id="nprod" value="" />
                            <input type="hidden" name="prices" id="prices" value="" />
                            <input type="hidden" name="tipo_moneda" id="tipo_moneda" value="" />
                            <input type="hidden" name="cantidad" id="cantidad" value="<?php if($total_ananido != 0){ echo $total_ananido+1;}else{ echo "1";}?>" />
                            <!--<input type="hidden" name="images" id="images" value="<?php ?>" />-->
                            <?php echo form_close(); ?>                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div id="filtros_de_productos">
                                <?php 
                              
                                ?>
                            </div>
                        </div>
                    </div>
   <?php } ?>          
<?php if($prod_id == 5){
    
                        $total_ananido=0;
                        if(isset($_SESSION['carro'])){
                            $cart_session =  $_SESSION['carro'];
                            if($cart_session){
                                if(!empty($cart_session)): foreach($cart_session as $cs=>$carro_shop):
                                    if($cs == "3121-3BX16"){$total_ananido = $carro_shop['cantidad']; }
                                    endforeach; else:
                                        
                                        endif;
                                    
                                    }
                                    
                                    }
                        ?>
           <div class="row">            
            <div class="col-md-12" style="padding: 30px;">
                <div class="row">
                   <div class="col-md-5 text-center">
                       <div style="height: 297px;width: 100%;background: #fff;margin-bottom: 2em;">
                         <img src="<?php  echo base_url('public/theme/images/conduccion_derivacion/bridas.jpg') ?>" alt=""  style="width: 100%;margin-bottom: 2em;max-width:350px"/>
                     </div>

                   </div>
                    <div class="col-md-7" style="text-align:justify;">
                        <h6 class="titulo_producto">BRIDAS RÁPIDAS</h6>
                        <fieldset></fieldset>                           
                        <div  class="section-text"  style="color:#000;">
                         Este tipo de brida se adapta rápidamente a cualquier tipo de tubería, su diseño permite una perfecta hermeticidad sin necesidad de soldar.
                        </div>
                         
                         <div class="clearfix"></div>
                        <div class="section-text pt-20 pb-20">                            
                            <?php echo form_open_multipart(base_url('welcome/crea_sesion_producto'), array('id' => 'frm-prod')) ?>
                            <div class="row">
                                <div class="col-md-5">
                            <div class="row">                 
                                <div id="" class="col-md-12 text-left">                                    
                                    Modelo<br>
                                    <select id="modelo" name="modelo"  onchange="MuestraBridaBR(this.value)">
                                       <option value="">Selecciona</option>                                            
                                       <option value="3531">3531</option>
                                       <option value="3532">3532</option>   
                                       <option value="3531AI">3531 AI</option>
                                       <option value="3532AI">3532 AI</option>   
                                   </select>
                                </div>                              
                            </div>
                             
                           <div class="row">                                
                                  <div id="lista_brida" class="col-md-12 text-left">
                                    Brida<br>
                                    <select id="brida" name="brida">
                                       <option value="">Selecciona</option>                                              
                                   </select>
                                </div>
                            </div>                            
                            <div class="row">                                
                                <div id="lista_rango" class="col-md-12 text-left">
                                    Rango del Diámetro<br>
                                    <select id="rango" name="rango" >
                                       <option value="">Selecciona</option>                                            
                                   </select>                                  
                                </div>                                
                            </div>
                            
                                </div>
                                <div class="col-md-7">
                                         
                                </div>
                            </div>
                            
                            
                            
                             <br>
                            <input type="hidden" name="skus" id="skus" value="" />
                            <input type="hidden" name="nprod" id="nprod" value="" />
                            <input type="hidden" name="prices" id="prices" value="" />
                            <input type="hidden" name="tipo_moneda" id="tipo_moneda" value="" />
                            <input type="hidden" name="cantidad" id="cantidad" value="<?php if($total_ananido != 0){ echo $total_ananido+1;}else{ echo "1";}?>" />
                            <!--<input type="hidden" name="images" id="images" value="<?php ?>" />-->
                            <?php echo form_close(); ?>                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div id="filtros_de_productos">
                                <?php 
                              
                                ?>
                            </div>
                        </div>
                    </div>
   <?php } ?>          
        
    </div>
</section>