<section class="page-section pt-20 pb-40">
                <div class="container relative">               
                                  <!-- Intro Text -->
                    <div class="row">
                        <div class="col-md-12 mb-10">
                            <div class="section-text align-left">
                              <h3 class="tituloproductos1 mt-0 mb-0" style="">Válvulas y cajas de válvulas</h3>
                              <div class="linea"></div>
                            </div>
                        </div>
                        <div class="col-md-12 mb-30">
                            <div class="section-text align-left">
                               Tienda > <a href="<?php echo base_url('tienda') ?>" style="color: #2d77b3;">Nuestros productos</a> > Válvulas y cajas de válvulas
                            </div>
                        </div>
                    </div>
                                 
                    <!-- End Intro Text -->
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1 mb-sm-40">                       
                            <!-- Logo Grid -->
                    <div class="row multi-columns-row mb-20">                        
                        <!-- Logo Item -->
                        <div class="col-sm-6 col-md-6 col-lg-6 mb-sm-20">                        
                            <div class="blog-post-prev-small mb-20 clearfix" style="background:#cccccc;">
                                 <h3 class="alt-features-title font-alt mb-0 mt-0 pt-10 pb-10" style="padding: 10px;">
                                 <a href="<?php echo base_url('producto_valvulas/1') ?>">VÁLVULAS MARIPOSA</a>
                                </h3>
                            </div>              
                        </div>
                        <!-- End Logo Item -->
                        
                        <!-- Logo Item -->
                        <div class="col-sm-6 col-md-6 col-lg-6 mb-sm-20">                             
                            <div class="blog-post-prev-small mb-20 clearfix" style="background:#cccccc;">          
                                  <h3 class="alt-features-title font-alt mb-0 mt-0 pt-10 pb-10" style="padding: 10px;">
                                 <a href="<?php echo base_url('producto_valvulas/2') ?>">VÁLVULAS CUCHILLA</a>
                                </h3>
                            </div>
                        </div>
                        <!-- End Logo Item -->
                        
                        <!-- Logo Item -->
                        <div class="col-sm-6 col-md-6 col-lg-6 mb-sm-20">
                            <div class="blog-post-prev-small mb-20 clearfix" style="background:#ccc;">
                                 <h3 class="alt-features-title font-alt mb-0 mt-0 pt-10 pb-10" style="padding: 10px;">
                                   <a href="<?php echo base_url('producto_valvulas/3') ?>">VÁLVULAS DE EXPULSIÓN Y/O ADMISIÓN DE AIRE</a>
                                </h3>
                            </div>
                        </div>
                        <!-- End Logo Item -->
                   
                        <!-- Logo Item -->
                        <div class="col-sm-6 col-md-6 col-lg-6 mb-sm-20">                           
                            <div class="blog-post-prev-small mb-20 clearfix" style="background:#ccc;">                             
                                   <h3 class="alt-features-title font-alt mb-0 mt-0 pt-10 pb-10" style="padding: 10px;">
                                 <a href="<?php echo base_url('producto_valvulas/4') ?>">VÁLVULAS DE CONTROL</a>
                                </h3>
                            </div>
                        </div>
                        <!-- End Logo Item -->                        
                      
                    </div>
                    <!-- End Logo Grid -->
                            
                            
                        </div>
                        
                       
                    </div>
                </div>
            </section>
            <!-- End Section -->