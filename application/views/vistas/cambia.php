<?php 
$login_id="";
$login_id = $this->uri->segment(2);
?>
<!-- Section -->
            <section class="page-section">
                <div class="container relative">
                    
                 
                    
                    <!-- Tab panes -->
                    <div class="tab-content tpl-minimal-tabs-cont section-text">
                        
                        
                            
                                        
                        <div class="tab-pane fade in active" id="mini-two">
                            
                            <!-- Registry Form -->                            
                            <div class="row">
                                <div class="col-md-4 col-md-offset-4">
                                    <?php echo form_open_multipart(base_url('welcome/actualiza_pwd'), array('id' => 'form_update_pwd', 'class' => 'form contact-form')) ?>
                                    <!--<form class="form contact-form" id="contact_form">-->
                                    <input type="hidden" name="id" id="id" value="<?php echo $login_id  ?>"/>
                                        <div class="clearfix">  
                                            <div class="form-group">
                                                Actualiza tu Contraseña
                                            </div>
                                            <!-- Password -->
                                            <div class="form-group">
                                                <input type="password" name="pwd" id="pwd" class="input-md round form-control" placeholder="Contraseña" required>
                                            </div>                                            
                                            <!-- Re-enter Password -->
                                            <div class="form-group">
                                                <input type="password" name="re-password" id="re-password" class="input-md round form-control" placeholder="Confirmar contraseña" required>
                                            </div>                                                
                                        </div>                                        
                                        <!-- Send Button -->
                                        <div class="pt-10">
                                            <button type="submit" class="submit_btn btn btn-mod btn-medium btn-round btn-full" id="reg-btn">Actualizar Contraseña</button>
                                        </div>
                                        
                                    </form>
                                    <div class="clearfix">
                                        <div class="alerta">
                                            <?php echo '<br>'.$this->session->flashdata('msg_cambia');
//                                             if($auth['valid']){redirect(base_url(''), 'refresh');}else{echo $auth['mensaje'];}
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End Registry Form -->
                            
                        </div>
                        
                    </div>
                    
                </div>
            </section>
            <!-- End Section -->