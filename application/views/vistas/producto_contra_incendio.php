<?php
//$prod_id = $this->input->get('id', TRUE);

$prod_id = $this->uri->segment(2);
?>
 
<section class="page-section pt-20 pb-40">
    <div class="container relative">
        <!-- Intro Text -->
        <div class="row">
            <div class="col-md-12">
                <div class="section-text align-left">
                    <h3 class="tituloproductos1 mt-0 mb-0">Contra incendio</h3>
                    <div class="linea"></div>
                </div>
            </div>
        </div>
        <!-- End Intro Text -->
        <!--<div class="clearfix"></div>-->
        
<?php if($prod_id == 1){  ?>          
        <div class="row">
            <div class="col-md-3">
                <?php 
                $this->load->view('vistas/menu_productos');
                ?>
            </div>
            <div class="col-md-9" style="background: #fff;padding: 30px;">
                <div class="row">
                   <div class="col-md-4">
                       <img src="<?php echo base_url('public/theme/images/contra_incendio/hidrante_catego.jpg') ?>" alt=""  style="width: 100%;"/>
                   </div>
                    <div class="col-md-8" style="text-align:justify;">
                        <h6 class="titulo_producto">Hidrantes barril seco</h6>
                        <fieldset></fieldset>
                        <div  class="section-text"  style="color:#000;">
                            Este tipo de hidrante mantiene su barril seco hasta el momento de ser necesario, es activado manualmente por una persona que debe abrir o cerrar la válvula para dejar pasar el fluido o cortarlo. Cuando la válvula se abre, el agua comienza a fluir por el sistema de forma automática. Esto funciona debido a la presión de agua en el sistema. Cuando la válvula está abierta, el aire y el agua se dirigen hacia la entrada.
                        </div>                            
                    </div>
                </div>
                <div class="row pt-0 pb-0">                    
                      <div class="col-md-4" style="text-align:justify;"></div>
                    <div class="col-md-8">
                    <h6 class="titulo_producto" style="font-size: 16px;">Características</h6>
                        <div>
                            <ul id="ven" style="">
                                <li><span>Presión de trabajo de 250 psig.</span></li>
                                <li><span>Conveniente válvula principal reversible.</span></li>
                                <li><span>Un avanzado acoplamiento de seguridad y diseño de brida, reducen el daño ocasionado a los hidrantes por accidentes de tráfico.</span></li>
                                <li><span>Un cople de seguridad de acero inoxidable del vástago resiste la corrosión y asegura una conexión firme entre las secciones del vástago.</span></li>
                                <li><span>El diseño hidráulico eficiente suministra un flujo máximo.</span></li>
                                <li><span>Las tomas para mangueras y bomba son roscadas y fáciles de reemplazar en el campo.</span></li>
                                <li><span>Sistema de lubricación forzado y arandelas antifricción facilitan la operación.</span></li>
                                <li><span>La válvula principal es fácilmente removida ya sea de la brida del bonete o de la brida de la línea a tierra.</span></li>
                            </ul>
                        </div>
                    </div>                    
            </div>
                <div class="row pb-40">
                    <div class="col-md-4">
                        
                    </div>
                    <div class="col-md-8">
                         <h6 class="titulo_producto" style="font-size: 16px;">Marca del Produto</h6>
                             <div class="row">
                            <div class=" col-md-10" style="">
                                  <div class="col-md-12"> 
                                      <img src="<?php echo base_url('public/theme/images/contra_incendio/p/logo_mueller_co.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>
                                  </div>
                                <div class="col-md-12"> 
                                      <a href="pdf/contra_incendio/FICHA_TECNICA_HIDRANTE_250.pdf" target="_blank">Ficha Técnica</a>
                                </div>
                            </div>
                           </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" style="text-align:justify;">
                        <h4 class="mb-10">Cotizaciones o información de fichas técnicas</h4>
                         <fieldset></fieldset>
                        <form name="frm" id="frm" action="http://mrwasabiserver.com/webpage/ksh17/api-correo/index.php/api/correo_ksh" method="post">
                            <input type="hidden" name="tipo" id="tipo" value="Hidrantes barril seco" />
                            <div class="row pb-20">
                            <div class="col-md-3"> 
                                Nombre*:<br/>
                                <input type="text" name="nombre" id="nombre" placeholder="Nombre" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3"> 
                                Empresa*:<br>
                                <input type="text" name="empresa" id="empresa"  placeholder="Empresa" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3">
                                Teléfono*:<br>
                                <input type="text" name="telefono" id="telefono"  placeholder="Teléfono" class="input-md round form-control" onkeypress="return isNumberKey(event)" maxlength="10"/> 
                            </div>
                            <div class="col-md-3">
                                E-mail*:<br>
                                <input type="text" name="correo" id="correo"  placeholder="E-mail" class="input-md round form-control"/>
                            </div>
                            </div>
                            <div class="row pb-20">
                            <div class="col-md-3">
                                Ciudad*:<br>
                                <input type="text" name="ciudad" id="ciudad"  placeholder="Ciudad a la que pertences" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-9">
                                Mensaje*:<br>
                                <textarea name="mensaje" id="mensaje" style="width: 100%;height:60px;" placeholder="Mensaje." class="input-md round form-control"></textarea>
                            </div>
                            </div>
                            <div class="row pb-20">
                             <div class="col-md-12 align-right">
                                <a href="javascript:EnviaProdContacto()" style="padding:10px 25px;font-size: 15px;color:#fff;background:#000;text-decoration: none;">ENVIAR</a>
                             </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
<?php } if($prod_id == 2){  ?>  
   <div class="row">
            <div class="col-md-3">
                <?php 
                $this->load->view('vistas/menu_productos');
                ?>
            </div>
            <div class="col-md-9" style="background: #fff;padding: 30px;">
                <div class="row">
                   <div class="col-md-4">
                       <img src="<?php echo base_url('public/theme/images/contra_incendio/hidrante_barril_humedo_jones.jpg') ?>" alt=""  style="width: 100%;"/>
                   </div>
                    <div class="col-md-8" style="text-align:justify;">
                        <h6 class="titulo_producto">Hidrantes barril húmedo</h6>
                        <fieldset></fieldset>
                        <div  class="section-text"  style="color:#000;">
                        A este tipo de hidrante se le conoce como barril húmedo porque el agua fluye constantemente a través de las tuberías y la válvula de suministro de los hidrantes se mantiene abierta en todo momento. Una de las principales ventajas es que el agua siempre estará disponible en cualquier momento y sus salidas son independientes por lo tanto se puede conectar una manguera sin necesidad de cerrar todo el paso de agua.
                        </div>                            
                    </div>
                </div>
                <div class="row pt-0 pb-40">
                    <!--<div class="col-md-8 col-md-offset-4" >-->
                      <div class="col-md-4" style="text-align:justify;">                        
                        
                     </div>                      
                    <div class="col-md-8">
                            <h6 class="titulo_producto" style="font-size: 16px;">Marca del Produto</h6>
                             <div class="row">
                            <div class=" col-md-10" style="">                                
                                    <img src="<?php echo base_url('public/theme/images/contra_incendio/p/jonesLogo.gif') ?>" alt="" style="width: 100%;max-width: 162px;"/>     
                                
                            </div>
                           </div>                  
                    </div>
                    <!--</div>-->  
            </div>
                <div class="row">
                    <div class="col-md-12" style="text-align:justify;">
                        <h4 class="mb-10">Cotizaciones o información de fichas técnicas</h4>
                         <fieldset></fieldset>
                        <form name="frm" id="frm" action="http://mrwasabiserver.com/webpage/ksh17/api-correo/index.php/api/correo_ksh" method="post">
                            <input type="hidden" name="tipo" id="tipo" value="Hidrantes barril húmedo" />
                            <div class="row pb-20">
                            <div class="col-md-3"> 
                                Nombre*:<br/>
                                <input type="text" name="nombre" id="nombre" placeholder="Nombre" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3"> 
                                Empresa*:<br>
                                <input type="text" name="empresa" id="empresa"  placeholder="Empresa" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3">
                                Teléfono*:<br>
                                <input type="text" name="telefono" id="telefono"  placeholder="Teléfono" class="input-md round form-control" onkeypress="return isNumberKey(event)" maxlength="10"/> 
                            </div>
                            <div class="col-md-3">
                                E-mail*:<br>
                                <input type="text" name="correo" id="correo"  placeholder="E-mail" class="input-md round form-control"/>
                            </div>
                            </div>
                            <div class="row pb-20">
                           <div class="col-md-3">
                                Ciudad*:<br>
                                <input type="text" name="ciudad" id="ciudad"  placeholder="Ciudad a la que pertences" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-9">
                                Mensaje*:<br>
                                <textarea name="mensaje" id="mensaje" style="width: 100%;height:60px;" placeholder="Mensaje." class="input-md round form-control"></textarea>
                            </div>
                            </div>
                            <div class="row pb-20">
                             <div class="col-md-12 align-right">
                                    <a href="javascript:EnviaProdContacto()" style="padding:10px 25px;font-size: 15px;color:#fff;background:#000;text-decoration: none;">ENVIAR</a>
                             </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>        

<?php } if($prod_id == 3){  ?>  
<div class="row">
            <div class="col-md-3">
                <?php 
                $this->load->view('vistas/menu_productos');
                ?>
            </div>
            <div class="col-md-9" style="background: #fff;padding: 30px;">
                <div class="row">
                   <div class="col-md-4">
                       <img src="<?php echo base_url('public/theme/images/contra_incendio/extension_hidrante.jpg') ?>" alt=""  style="width: 100%;margin-bottom: 2em;"/>
                   </div>
                   
                    <div class="col-md-8" style="text-align:justify;">
                        <h6 class="titulo_producto">Extensiones de hidrante</h6>
                        <fieldset></fieldset>
                        <div  class="section-text"  style="color:#000;">
                           Las extensiones para hidrantes son usadas cuando desconoces la longitud en donde deberá de estar tu hidrante.  Este te permite agrandar el vástago en caso de que no coincida con la longitud que deseas.
                          <!--  <div class="row">
                            <div class="col-md-8"></div>
                            <div class="col-md-4"></div>
                           </div>-->
                        </div>                            
                    </div>
                </div>
                <div class="row pt-40 pb-40">                    
                      <div class="col-md-4" style="text-align:justify;">                        
                             
                     </div>                      
                    <div class="col-md-8">                                             
                               <h6 class="titulo_producto" style="font-size: 16px;">Marca del Produto</h6>
                             <div class="row">
                                 <div class="col-md-12" >
                                    <img src="<?php echo base_url('public/theme/images/contra_incendio/p/logo_mueller_co.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>     
                                 </div>
                             </div>
                    </div>           
            </div>
                <div class="row">
                    <div class="col-md-12" style="text-align:justify;">
                        <h4 class="mb-10">Cotizaciones o información de fichas técnicas</h4>
                         <fieldset></fieldset>
                        <form name="frm" id="frm" action="http://mrwasabiserver.com/webpage/ksh17/api-correo/index.php/api/correo_ksh" method="post">
                            <input type="hidden" name="tipo" id="tipo" value="Extensiones de hidrante" />
                            <div class="row pb-20">
                            <div class="col-md-3"> 
                                Nombre*:<br/>
                                <input type="text" name="nombre" id="nombre" placeholder="Nombre" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3"> 
                                Empresa*:<br>
                                <input type="text" name="empresa" id="empresa"  placeholder="Empresa" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3">
                                Teléfono*:<br>
                                <input type="text" name="telefono" id="telefono"  placeholder="Teléfono" class="input-md round form-control" onkeypress="return isNumberKey(event)" maxlength="10"/> 
                            </div>
                            <div class="col-md-3">
                                E-mail*:<br>
                                <input type="text" name="correo" id="correo"  placeholder="E-mail" class="input-md round form-control"/>
                            </div>
                            </div>
                            <div class="row pb-20">
                           <div class="col-md-3">
                                Ciudad*:<br>
                                <input type="text" name="ciudad" id="ciudad"  placeholder="Ciudad a la que pertences" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-9">
                                Mensaje*:<br>
                                <textarea name="mensaje" id="mensaje" style="width: 100%;height:60px;" placeholder="Mensaje." class="input-md round form-control"></textarea>
                            </div>
                            </div>
                            <div class="row pb-20">
                             <div class="col-md-12 align-right">
                                   <a href="javascript:EnviaProdContacto()" style="padding:10px 25px;font-size: 15px;color:#fff;background:#000;text-decoration: none;">ENVIAR</a>
                             </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>                

<?php } if($prod_id == 4){  ?>  
 <div class="row">
            <div class="col-md-3">
                <?php 
                $this->load->view('vistas/menu_productos');
                ?>
            </div>
            <div class="col-md-9" style="background: #fff;padding: 30px;">
                <div class="row">
                   <div class="col-md-4">
                       <img src="<?php echo base_url('public/theme/images/contra_incendio/kit_reparacion.jpg') ?>" alt=""  style="width: 100%;"/>
                   </div>
                   
                    <div class="col-md-8" style="text-align:justify;">
                        <h6 class="titulo_producto">Repuestos y kits de reparación</h6>
                        <fieldset></fieldset>
                        <div  class="section-text"  style="color:#000;">
                        Los kits de reparación  facilitan y simplifican el mantenimiento de rutina y las mejoras al hidrante.
                        </div>                            
                    </div>
                </div>
                <div class="row pt-40 pb-0">
                      <div class="col-md-4" style="text-align:justify;"></div>
                    <div class="col-md-8">  
                        <div class="row">
                            <div class="col-md-12">                               
                               Los juegos se ofrecen para cinco diferentes secciones del hidrante.
                               <h6 class="titulo_producto" style="font-size: 16px;">Tipos de juegos:</h6>
                               <div>
                                   <ol id="ven" style="">
                                         <li><span>Juego de reparación del bonete</span></li>
                                         <li><span>Juego de reparación de la brida de Seguridad</span></li>
                                         <li><span>Juego de Extensión</span></li> 
                                         <li><span>Juego de la válvula principal</span></li> 
                                         <li><span>Juego de reparación de la zapata</span></li> 
                                   </ol>
                               </div>
                            </div>
                           
                        </div>
                    </div>                   
                </div>
                 <div class="row pt-0 pb-40">                    
                     <div class="col-md-4" style="text-align:justify;"></div>
                    <div class="col-md-8">                      
                             <h6 class="titulo_producto" style="font-size: 16px;">Marca del Produto</h6>
                             <div class="row">
                            <div class=" col-md-10" style="">
                                <div class="row">                                
                                <div class="col-md-12" style="">
                                    <img src="<?php echo base_url('public/theme/images/contra_incendio/p/logo_mueller_co.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>
                                </div>                                  
                                </div>
                            </div>
                           </div>
                     </div>            
                 </div>
                <div class="row">
                    <div class="col-md-12" style="text-align:justify;">
                        <h4 class="mb-10">Cotizaciones o información de fichas técnicas</h4>
                         <fieldset></fieldset>
                        <form name="frm" id="frm" action="http://mrwasabiserver.com/webpage/ksh17/api-correo/index.php/api/correo_ksh" method="post">
                            <input type="hidden" name="tipo" id="tipo" value="Repuestos y kits de reparación" />
                            <div class="row pb-20">
                            <div class="col-md-3"> 
                                Nombre*:<br/>
                                <input type="text" name="nombre" id="nombre" placeholder="Nombre" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3"> 
                                Empresa*:<br>
                                <input type="text" name="empresa" id="empresa"  placeholder="Empresa" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3">
                                Teléfono*:<br>
                                <input type="text" name="telefono" id="telefono"  placeholder="Teléfono" class="input-md round form-control" onkeypress="return isNumberKey(event)" maxlength="10"/> 
                            </div>
                            <div class="col-md-3">
                                E-mail*:<br>
                                <input type="text" name="correo" id="correo"  placeholder="E-mail" class="input-md round form-control"/>
                            </div>
                            </div>
                            <div class="row pb-20">
                            <div class="col-md-3">
                                Ciudad*:<br>
                                <input type="text" name="ciudad" id="ciudad"  placeholder="Ciudad a la que pertences" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-9">
                                Mensaje*:<br>
                                <textarea name="mensaje" id="mensaje" style="width: 100%;height:60px;" placeholder="Mensaje." class="input-md round form-control"></textarea>
                            </div>
                            </div>
                            <div class="row pb-20">
                             <div class="col-md-12 align-right">
                                <a href="javascript:EnviaProdContacto()" style="padding:10px 25px;font-size: 15px;color:#fff;background:#000;text-decoration: none;">ENVIAR</a>
                             </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>                     

<?php } if($prod_id == 5){  ?>  
 <div class="row">
            <div class="col-md-3">
                <?php 
                $this->load->view('vistas/menu_productos');
                ?>
            </div>
            <div class="col-md-9" style="background: #fff;padding: 30px;">
                <div class="row">
                   <div class="col-md-4 text-center">
                       <img src="<?php echo base_url('public/theme/images/contra_incendio/Postes_indicadores.png') ?>" alt=""  style="width: 100%;max-width:80px;"/>
                   </div>
                   
                    <div class="col-md-8" style="text-align:justify;">
                        <h6 class="titulo_producto">Poste indicador vertical</h6>
                        <fieldset></fieldset>
                        <div  class="section-text"  style="color:#000;">
                       Los postes indicadores verticales son diseñados para operar válvulas de compuerta de vástago fijo, este tipo de postes son usados para que el operado se dé cuenta si la válvula compuerta está abierta o cerrada, los de este tipo son instalados en las banquetas.                            
                        </div>                            
                    </div>
                </div>
                <div class="row pt-40 pb-40">                    
                    <div class="col-md-4" style="text-align:justify;"></div>
                    <div class="col-md-8">  
                       <h6 class="titulo_producto" style="font-size: 16px;">Marca del Produto</h6>
                             <div class="row">
                            <div class=" col-md-10" >
                                <div class="row">                                
                                <div class="col-md-12" >
                                    <img src="<?php echo base_url('public/theme/images/contra_incendio/p/logo_mueller_co.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>
                                </div>  
                                    <div class="col-md-12 ">
                                         <a href="pdf/contra_incendio/Ficha_poste_indicador_vertical.pdf" target="_blank">Ficha Técnica</a>
                                    </div>
                                </div>
                            </div>
                           </div>
                    </div>                 
                </div>
                <div class="row">
                    <div class="col-md-12" style="text-align:justify;">
                        <h4 class="mb-10">Cotizaciones o información de fichas técnicas</h4>
                         <fieldset></fieldset>
                        <form name="frm" id="frm" action="http://mrwasabiserver.com/webpage/ksh17/api-correo/index.php/api/correo_ksh" method="post">
                            <input type="hidden" name="tipo" id="tipo" value="Poste indicador vertical" />
                            <div class="row pb-20">
                            <div class="col-md-3"> 
                                Nombre*:<br/>
                                <input type="text" name="nombre" id="nombre" placeholder="Nombre" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3"> 
                                Empresa*:<br>
                                <input type="text" name="empresa" id="empresa"  placeholder="Empresa" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3">
                                Teléfono*:<br>
                                <input type="text" name="telefono" id="telefono"  placeholder="Teléfono" class="input-md round form-control" onkeypress="return isNumberKey(event)" maxlength="10"/> 
                            </div>
                            <div class="col-md-3">
                                E-mail*:<br>
                                <input type="text" name="correo" id="correo"  placeholder="E-mail" class="input-md round form-control"/>
                            </div>
                            </div>
                            <div class="row pb-20">
                            <div class="col-md-3">
                                Ciudad*:<br>
                                <input type="text" name="ciudad" id="ciudad"  placeholder="Ciudad a la que pertences" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-9">
                                Mensaje*:<br>
                                <textarea name="mensaje" id="mensaje" style="width: 100%;height:60px;" placeholder="Mensaje." class="input-md round form-control"></textarea>
                            </div>
                            </div>
                            <div class="row pb-20">
                             <div class="col-md-12 align-right">
                                 <a href="javascript:EnviaProdContacto()" style="padding:10px 25px;font-size: 15px;color:#fff;background:#000;text-decoration: none;">ENVIAR</a>
                             </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>                

<?php } if($prod_id == 6){  ?>  
  <div class="row">
            <div class="col-md-3">
                <?php 
                $this->load->view('vistas/menu_productos');
                ?>
            </div>
            <div class="col-md-9" style="background: #fff;padding: 30px;">
                <div class="row">
                   <div class="col-md-4">
                       <img src="<?php echo base_url('public/theme/images/contra_incendio/postes_pared.jpg') ?>" alt=""  style="width: 100%;"/>
                   </div>
                   
                    <div class="col-md-8">
                        <h6 class="titulo_producto">Poste indicador de pared</h6>
                        <fieldset></fieldset>
                        <div class="section-text text-justify">
                           Los postes indicadores de pared son diseñados para operar válvulas de compuerta de vástago fijo, este tipo de postes son usados para que el operado se dé cuenta si la válvula compuerta está abierta o cerrada y los de este tipo tiene la ventaja que lo puedes instalar sobre la pared en interiores.
                        </div>
                         <!--<div class="row mt-10">                             
                            <div class="section-text col-md-8 text-justify">                            
                            </div>
                            <div class="section-text col-md-4">
                            </div>
                        </div>-->
                    </div>
                </div>
                <div class="row pt-40 pb-40">                    
                    <div class="col-md-4" style="text-align:justify;"></div>
                    <div class="col-md-8">  
                         <h6 class="titulo_producto" style="font-size: 16px;">Marca del Produto</h6>
                             <div class="row">
                            <div class=" col-md-10" style="">
                                <div class="row">                                
                                <div class="col-md-12" style="">
                                    <img src="<?php echo base_url('public/theme/images/contra_incendio/p/logo_mueller_co.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>
                                </div>  
                                </div>
                            </div>
                           </div>
                    </div>                 
                </div>
                <div class="row">
                    <div class="col-md-12" style="text-align:justify;">
                        <h4 class="mb-10">Cotizaciones o información de fichas técnicas</h4>
                         <fieldset></fieldset>
                        <form name="frm" id="frm" action="http://mrwasabiserver.com/webpage/ksh17/api-correo/index.php/api/correo_ksh" method="post">
                            <input type="hidden" name="tipo" id="tipo" value="Poste indicador de pared" />
                            <div class="row pb-20">
                            <div class="col-md-3"> 
                                Nombre*:<br/>
                                <input type="text" name="nombre" id="nombre" placeholder="Nombre" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3"> 
                                Empresa*:<br>
                                <input type="text" name="empresa" id="empresa"  placeholder="Empresa" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3">
                                Teléfono*:<br>
                                <input type="text" name="telefono" id="telefono"  placeholder="Teléfono" class="input-md round form-control" onkeypress="return isNumberKey(event)" maxlength="10"/> 
                            </div>
                            <div class="col-md-3">
                                E-mail*:<br>
                                <input type="text" name="correo" id="correo"  placeholder="E-mail" class="input-md round form-control"/>
                            </div>
                            </div>
                            <div class="row pb-20">
                            <div class="col-md-3">
                                Ciudad*:<br>
                                <input type="text" name="ciudad" id="ciudad"  placeholder="Ciudad a la que pertences" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-9">
                                Mensaje*:<br>
                                <textarea name="mensaje" id="mensaje" style="width: 100%;height:60px;" placeholder="Mensaje." class="input-md round form-control"></textarea>
                            </div>
                            </div>
                            <div class="row pb-20">
                             <div class="col-md-12 align-right">
                                   <a href="javascript:EnviaProdContacto()" style="padding:10px 25px;font-size: 15px;color:#fff;background:#000;text-decoration: none;">ENVIAR</a>
                             </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>                

<?php } if($prod_id == 7){  ?>  
<div class="row">
            <div class="col-md-3">
                <?php 
                $this->load->view('vistas/menu_productos');
                ?>
            </div>
            <div class="col-md-9" style="background: #fff;padding: 30px;">
                <div class="row">
                   <div class="col-md-4">
                       <img src="<?php echo base_url('public/theme/images/contra_incendio/contra_incendio_integral.jpg') ?>" alt=""  style="width: 100%;"/>
                   </div>
                   
                    <div class="col-md-8">
                        <h6 class="titulo_producto">Equipo contra incendio integral</h6>
                        <fieldset></fieldset>
                        <div class="section-text text-justify">
                            Toda instalación debe de contar con un sistema contra incendio, estos constituyen un conjunto de equipamientos diversos integrados en la estructura de la instalación con los requerimientos y especificaciones de la norma NFPA 20. Garantizando calidad, adaptabilidad y buen  funcionamiento en todos sus equipos para que tenga la certeza que está totalmente protegido en caso de un incendio.
                        </div>
                         <!--<div class="row mt-10">                             
                            <div class="section-text col-md-7 text-justify"></div>
                            <div class="section-text col-md-5"></div>
                        </div>-->
                    </div>
                </div>
                <div class="row pt-40 pb-0">
                    <div class="col-md-4" style="text-align:justify;"></div>
                    <div class="col-md-8">  
                           <h6 class="titulo_producto" style="font-size: 16px;">Los equipos integrados cuentan con:</h6>
                               <div>
                                   <ol id="ven" style="">
                                     <li><span>FPS Bomba Diésel (ECI)</span></li>
                                         <li><span>FPS Bomba Diésel/Jockey BMB</span></li>
                                         <li><span>FPS Bomba Diésel/Eléctrica</span></li>
                                         <li><span>FPS Bomba Diésel/ Eléctrica/ Jockey</span></li>
                                   </ol>
                               </div>
                    </div>
                </div>
                <div class="row pt-0 pb-40">                    
                     <div class="col-md-4" style="text-align:justify;"></div>
                    <div class="col-md-8">                   
                             <h6 class="titulo_producto" style="font-size: 16px;">Marca del Produto</h6>
                             <div class="row">
                            <div class=" col-md-10">
                                <div class="row">                          
                                <div class="col-md-12">
                                    <img src="<?php echo base_url('public/theme/images/contra_incendio/p/logo_barnes.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>
                                </div>  
                                <div class="col-md-12 mt-20">
                                    <a href="pdf/contra_incendio/Ficha_Barnes_Equipos_contra_Incendio_SR.pdf" target="_blank" >Ficha Técnica</a>
                                </div>
                                </div>
                            </div>
                           </div>
                     </div>        
                </div>
                <div class="row">
                    <div class="col-md-12" style="text-align:justify;">
                        <h4 class="mb-10">Cotizaciones o información de fichas técnicas</h4>
                         <fieldset></fieldset>
                        <form name="frm" id="frm" action="http://mrwasabiserver.com/webpage/ksh17/api-correo/index.php/api/correo_ksh" method="post">
                            <input type="hidden" name="tipo" id="tipo" value="Equipo contra incendio integral" />
                            <div class="row pb-20">
                            <div class="col-md-3"> 
                                Nombre*:<br/>
                                <input type="text" name="nombre" id="nombre" placeholder="Nombre" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3"> 
                                Empresa*:<br>
                                <input type="text" name="empresa" id="empresa"  placeholder="Empresa" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3">
                                Teléfono*:<br>
                                <input type="text" name="telefono" id="telefono"  placeholder="Teléfono" class="input-md round form-control" onkeypress="return isNumberKey(event)" maxlength="10"/> 
                            </div>
                            <div class="col-md-3">
                                E-mail*:<br>
                                <input type="text" name="correo" id="correo"  placeholder="E-mail" class="input-md round form-control"/>
                            </div>
                            </div>
                            <div class="row pb-20">
                            <div class="col-md-3">
                                Ciudad*:<br>
                                <input type="text" name="ciudad" id="ciudad"  placeholder="Ciudad a la que pertences" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-9">
                                Mensaje*:<br>
                                <textarea name="mensaje" id="mensaje" style="width: 100%;height:60px;" placeholder="Mensaje." class="input-md round form-control"></textarea>
                            </div>
                            </div>
                            <div class="row pb-20">
                             <div class="col-md-12 align-right">
                                    <a href="javascript:EnviaProdContacto()" style="padding:10px 25px;font-size: 15px;color:#fff;background:#000;text-decoration: none;">ENVIAR</a>
                             </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>                    

<?php } if($prod_id == 8){  ?>  
<div class="row">
            <div class="col-md-3">
                <?php 
                $this->load->view('vistas/menu_productos');
                ?>
            </div>
            <div class="col-md-9" style="background: #fff;padding: 30px;">
                <div class="row">
                   <div class="col-md-4 text-center">
                       <img src="<?php echo base_url('public/theme/images/contra_incendio/valvulas_fm_uno.jpg') ?>" alt=""  style="width: 100%;max-width: 150px;"/>
                   </div>
                   
                    <div class="col-md-8">
                        <h6 class="titulo_producto">Válvulas UL/FM</h6>
                        <fieldset></fieldset>
                         <div class="row mt-10">                             
                            <div class="section-text col-md-8 text-justify">
                          Las válvulas UL/FM son de tipo compuerta y pueden ser de dos tipos con vástago fijo y vástago ascendente, las de vástago fijo tienen la ventaja de que pueden estar subterráneas y su vástago estará más protegido, las de vástago ascendente tienen la ventaja que están a la vista y su accionamiento en caso de una emergencia es más rápido.
                         
                            </div>
                            <div class="section-text col-md-4">
                                <img src="<?php echo base_url('public/theme/images/contra_incendio/valvulas_fm_dos.jpg') ?>" alt=""  style="width: 100%;" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row pt-40 pb-40">                    
                      <div class="col-md-4" style="text-align:justify;">                        
                           
                     </div>                      
                    <div class="col-md-8">  
  <h6 class="titulo_producto" style="font-size: 16px;">Marca del Produto</h6>
                             <div class="row">
                            <div class=" col-md-10">
                                <div class="row">                                
                                <div class="col-md-12">
                                    <img src="<?php echo base_url('public/theme/images/contra_incendio/p/logo_mueller_co.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>
                                </div>  
                               <div class="col-md-12">
                                    <a href="pdf/contra_incendio/catalago_de_valvulas.pdf" target="_blank">Catálogo de Valvulas</a>
                                </div>  
                                
                                     
                                </div>
                            </div>
                           </div>
                    </div>
                 
                </div>
                <div class="row">
                    <div class="col-md-12" style="text-align:justify;">
                        <h4 class="mb-10">Cotizaciones o información de fichas técnicas</h4>
                         <fieldset></fieldset>
                        <form name="frm" id="frm" action="http://mrwasabiserver.com/webpage/ksh17/api-correo/index.php/api/correo_ksh" method="post">
                            <input type="hidden" name="tipo" id="tipo" value="Válvulas UL/FM" />
                            <div class="row pb-20">
                            <div class="col-md-3"> 
                                Nombre*:<br/>
                                <input type="text" name="nombre" id="nombre" placeholder="Nombre" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3"> 
                                Empresa*:<br>
                                <input type="text" name="empresa" id="empresa"  placeholder="Empresa" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3">
                                Teléfono*:<br>
                                <input type="text" name="telefono" id="telefono"  placeholder="Teléfono" class="input-md round form-control" onkeypress="return isNumberKey(event)" maxlength="10"/> 
                            </div>
                            <div class="col-md-3">
                                E-mail*:<br>
                                <input type="text" name="correo" id="correo"  placeholder="E-mail" class="input-md round form-control"/>
                            </div>
                            </div>
                            <div class="row pb-20">
                            <div class="col-md-3">
                                Ciudad*:<br>
                                <input type="text" name="ciudad" id="ciudad"  placeholder="Ciudad a la que pertences" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-9">
                                Mensaje*:<br>
                                <textarea name="mensaje" id="mensaje" style="width: 100%;height:60px;" placeholder="Mensaje." class="input-md round form-control"></textarea>
                            </div>
                            </div>
                            <div class="row pb-20">
                             <div class="col-md-12 align-right">
                                  <a href="javascript:EnviaProdContacto()" style="padding:10px 25px;font-size: 15px;color:#fff;background:#000;text-decoration: none;">ENVIAR</a>
                             </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>                           

<?php } if($prod_id == 9){  ?>  
<div class="row">
            <div class="col-md-3">
                <?php 
                $this->load->view('vistas/menu_productos');
                ?>
            </div>
            <div class="col-md-9" style="background: #fff;padding: 30px;">
                <div class="row">
                   <div class="col-md-4">
                       <img src="<?php echo base_url('public/theme/images/contra_incendio/llaves_herramientas_uno.jpg') ?>" alt=""  style="width: 100%;"/>
                   </div>                   
                    <div class="col-md-8">
                        <h6 class="titulo_producto">Llaves y herramientas</h6>
                        <fieldset></fieldset>
                        <div class="section-text text-justify">Accesorios especiales para  hidrantes usados para darles el correcto mantenimiento.<br/><br/>
                          <ul id="ven"><li><span>Llave para asiento A-359 de ajuste universal</span></li></ul>
                            Se usa para extraer la válvula principal y el anillo del asiento desde el nivel del bonete o la línea del piso. Esta llave se centra por sí misma en las bridas del barril.
                          <ul id="ven"><li><span>Llave de operación de tuercas A-311</span></li></ul>
                            Opera las tapas de las tomas, coples para manguera tipo pasador y agarradera y las tuercas de operación y presión hacia abajo del hidrante.
                          <ul id="ven"><li><span>Manga de Bronce A-367</span></li></ul>
                            Protege los anillos en “O” de daños cuando el bonete es retirado del vástago superior.
                          <ul id="ven"><li><span>Aceite de lubricación A-51 para el hidrante</span></li></ul>
                            Envase de 10.5 oz, resiste todas las temperaturas, y llena exactamente el reservorio de aceite.
                         
                        </div>
                        
                         <div class="row mt-10">                             
                            <div class="section-text col-md-6 text-justify">
                                  <img src="<?php echo base_url('public/theme/images/contra_incendio/llaves_herramientas_dos.jpg') ?>" alt=""  style="width: 100%;" class=""/>
                            </div>
                            <div class="section-text col-md-6">
                                <img src="<?php echo base_url('public/theme/images/contra_incendio/llaves_herramientas_tres.jpg') ?>" alt=""  style="width: 100%;" class=""/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row pt-40 pb-40">                    
                      <div class="col-md-4" style="text-align:justify;">                        
                        
                     </div>                      
                    <div class="col-md-8">  
                      <h6 class="titulo_producto" style="font-size: 16px;">Marca del Produto</h6>
                             <div class="row">
                            <div class=" col-md-10" style="">
                                <div class="row">                                
                                <div class="col-md-12" style="">
                                    <img src="<?php echo base_url('public/theme/images/contra_incendio/p/logo_mueller_co.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>
                                </div>                             
                                </div>
                            </div>
                           </div>
                    </div>
                 
                </div>
                <div class="row">
                    <div class="col-md-12" style="text-align:justify;">
                        <h4 class="mb-10">Cotizaciones o información de fichas técnicas</h4>
                         <fieldset></fieldset>
                        <form name="frm" id="frm" action="http://mrwasabiserver.com/webpage/ksh17/api-correo/index.php/api/correo_ksh" method="post">
                            <input type="hidden" name="tipo" id="tipo" value="Llaves y herramientas" />
                            <div class="row pb-20">
                            <div class="col-md-3"> 
                                Nombre*:<br/>
                                <input type="text" name="nombre" id="nombre" placeholder="Nombre" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3"> 
                                Empresa*:<br>
                                <input type="text" name="empresa" id="empresa"  placeholder="Empresa" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3">
                                Teléfono*:<br>
                                <input type="text" name="telefono" id="telefono"  placeholder="Teléfono" class="input-md round form-control" onkeypress="return isNumberKey(event)" maxlength="10"/> 
                            </div>
                            <div class="col-md-3">
                                E-mail*:<br>
                                <input type="text" name="correo" id="correo"  placeholder="E-mail" class="input-md round form-control"/>
                            </div>
                            </div>
                            <div class="row pb-20">
                           <div class="col-md-3">
                                Ciudad*:<br>
                                <input type="text" name="ciudad" id="ciudad"  placeholder="Ciudad a la que pertences" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-9">
                                Mensaje*:<br>
                                <textarea name="mensaje" id="mensaje" style="width: 100%;height:60px;" placeholder="Mensaje." class="input-md round form-control"></textarea>
                            </div>
                            </div>
                            <div class="row pb-20">
                             <div class="col-md-12 align-right">
                                    <a href="javascript:EnviaProdContacto()" style="padding:10px 25px;font-size: 15px;color:#fff;background:#000;text-decoration: none;">ENVIAR</a>
                             </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>          

<?php } ?>        
    </div>
</section>