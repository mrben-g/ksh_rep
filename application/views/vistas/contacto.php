<style media="screen">

</style>
<section class="page-section pt-20 pb-40">
    <div class="container relative">
        <!-- Intro Text -->
        <div class="row">
            <div class="col-md-12">
                <div class="section-text align-left">
                    <h3 class="tituloproductos1 mt-0 mb-0" style="">Contáctanos</h3>
                    <div class="linea"></div>
                </div>
            </div>
        </div>
        <!-- End Intro Text -->
        <!--<div class="clearfix"></div>-->
        <div class="row" style="background: #fff;padding: 30px 10px;">
              <!-- Contact Form -->                          
             
               <div class="google-map">		
                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3595.118206561893!2d-100.29125099999999!3d25.700509999999998!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x86629512d6b3267b%3A0xbf90b818f3732a56!2sComercializadora+Tamex!5e0!3m2!1ses!2smx!4v1435241912785" width="100%" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>	
                            </div> 
              
                    <div class="row mt-40">
                        <div class="col-md-10 col-md-offset-1">
                            
                            <form class="form contact-form" id="contact_form">
                                <div class="clearfix">
                                    
                                    <div class="cf-left-col">
                                        
                                        <!-- Name -->
                                        <div class="form-group">
                                            <input type="text" name="name" id="name" class="input-md round form-control" placeholder="Nombre" pattern=".{3,100}" required>
                                        </div>
                                        
                                        <!-- Email -->
                                        <div class="form-group">
                                            <input type="email" name="email" id="email" class="input-md round form-control" placeholder="E-mail" pattern=".{5,100}" required>
                                        </div>
                                        
                                    </div>
                                    
                                    <div class="cf-right-col">
                                        
                                        <!-- Message -->
                                        <div class="form-group">                                            
                                            <textarea name="message" id="message" class="input-md round form-control" style="height: 84px;" placeholder="Mensaje"></textarea>
                                        </div>
                                        
                                    </div>
                                    
                                </div>
                                
                                <div class="clearfix">
                                    
                                    <div class="cf-left-col">
                                        
                                        <!-- Inform Tip -->                                        
                                        <div class="form-tip pt-20">
                                            <i class="fa fa-info-circle"></i> Todos los campos son requeridos
                                        </div>
                                        
                                    </div>
                                    
                                    <div class="cf-right-col">
                                        
                                        <!-- Send Button -->
                                        <div class="align-right pt-10">
                                            <button class="submit_btn btn btn-mod btn-medium btn-round" id="submit_btn">Enviar</button>
                                        </div>
                                        
                                    </div>
                                    
                                </div>
                                
                                
                                
                                <div id="result"></div>
                            </form>
                            
                        </div>
                    </div>
                    <!-- End Contact Form -->
        </div>
    </div>
</section>