<section class="page-section pt-20 pb-40">
    <div class="container">
        <div class="row">
            <div class="col-md-12 mb-40">
                <div class="section-text align-left">
                    <h3 class="tituloproductos1 mt-0 mb-0" style="">Avisos</h3>
                    <div class="linea"></div>
                </div>
            </div>
        </div>
        <!-- End Intro Text -->
        <div class="clearfix"></div>
                <div class="row" style="background: #fff;padding: 30px 10px;">
                     <div class="col-md-12 text-justify" style="padding-top:1em;"> 
<label style="font-size:14px;">Kuroda Soluciones Hidráulicas es una marca de la empresa KS Tubería S.A. de C.V., con RFC KST890224TU9 y dirección fiscal en la Calle Dr. R. Michel #2142</label><br/><br/>
<label style="font-size:14px;">Col. Atlas, Guadalajara, Jalisco, México, Código Postal 44767, (en lo adelante referida como "KSH"), en cumplimiento a la Ley Federal de Protección de Datos Personales en Posesión de Particulares (La Ley) y su Reglamento, hacen de su conocimiento que recaba y trata sus datos personales para las finalidades que establece este Aviso de Privacidad.</label><br/><br/>

<label style="font-size:14px;">KSH recaba sus datos personales de manera directa por medios electrónicos ya sea a través de formas de registro de usuario, formas de contacto, chat, E-commerce, cookies o por correo electrónico y/o por teléfono, presencial en eventos y de manera indirecta por medio de transferencias que le hacen llegar sus empresas filiales y/o terceros.</label><br/><br/>

<label style="font-size:14px;">Los datos que recabamos de forma directa e indirecta, se refieren al nombre de la empresa, giro de la empresa, RFC, dirección y/o ubicación, datos fiscales, correo electrónico de contacto, teléfono, fax, celular, nombre completo de representante legal o de puestos clave, nombre de puesto, grado de influencia sobre la compra, históricos de compra, patrones de compra, entre otros.</label><br/><br/>
<label style="font-size:14px;">KSH es responsable del resguardo, privacidad y uso apropiado de los datos personales que facilite el usuario en el sitio web así como información recabada por otros medios digitales como encuestas, sorteos, redes sociales.</label><br/><br/>
<label style="font-size:14px;">Dentro del sitio web y sus secciones de E-commerce, KSH utiliza servicios tercerizados de seguridad y procesamiento de órdenes de pago, por lo que la responsabilidad recae en dichos proveedores de servicio, quedando sin almacenar en KSH datos de tarjeta electrónica.</label><br/><br/>
<label style="font-size:14px;">KSH no compartirá o traspasará información de sus usuarios a terceros sin conocimiento y aprobación previa de estos, y se realizará con el único fin de contactarlos con nuestros filiales de servicio de envíos, mantenimiento de algún producto adquirido o buscado por ellos y en cuestiones de transacciones monetarias para realizar los pagos de sus pedidos.</label><br/><br/>
<label style="font-size:14px;">Su información será utilizada para gestión de venta y cobros así como para dar seguimiento a comentarios, consultas, quejas, reclamaciones y solicitudes de cotizaciones u ordenes de compra y servicios de E-commerce que usted presente en relación con nuestros productos y servicios. A su vez podrán ser utilizados para mantener controles estadísticos y administrativos y, en su caso, para fines de publicidad, prospección comercial y encuestas de nivel de servicio.   </label><br/><br/>
<label style="font-size:14px;">En caso que usted no desee que se traten sus datos personales para recibir publicidad y ofertas, usted nos lo puede hacer saber a nuestro Departamento de E-commerce en contacto@ksh.mx</label><br/><br/>
<label style="font-size:14px;">KSH no es responsable por la información que los usuarios proporcionen en apartados de comentarios o foros; pero se reserva el derecho de denunciar o eliminar comentarios de carácter ofensivo, irrelevante o en calidad de spam, basados en el criterio de sus operadores.</label><br/><br/>
<label style="font-size:14px;">Las cuentas de los usuarios podrán ser modificadas, restringidas o eliminadas por KSH sin aviso previo y en base a su propia consideración.</label><br/><br/>
<label style="font-size:14px;">Información como nombre de usuario y contraseña deberá de ser salvaguardada por el propio usuario y mantener su divulgación a criterio propio, ya que KSH no se hará responsable de suplantación, robo de identidad, u otros agravios.</label><br/><br/>
<label style="font-size:14px;">En el momento en el que el usuario elija la opciones de envío por parte de una de las compañías de envío que despliegue como opción la tienda en línea estará aceptando que la información pertinente para efectuar el envío y entrega de su pedido sea transmitida a dicha compañía. Los datos que se den a conocer en esta operación serán aquellos estrictamente necesarios para cumplimiento de esta.</label><br/><br/>
<label style="font-size:14px;">Así mismo el usuario se compromete a mantener en confidencia los datos a los que su cuenta en la tienda en línea le permita acceso y visibilidad.</label><br/><br/>


                                
                                </div>
                                  
                                            
                </div>
                
                    
            </div>
    
</section>