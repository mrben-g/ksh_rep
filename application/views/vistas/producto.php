 <section class="page-section pt-20 pb-40">
                <div class="container relative">               
                                  <!-- Intro Text -->
                    <div class="row">
                        <div class="col-md-12 mb-20">
                            <div class="section-text align-left">
                              <h3 class="tituloproductos1 mt-0 mb-0" style="">Productos</h3>
                              <div class="linea"></div>
                            </div>
                        </div>
                    </div>
                                 
                    <!-- End Intro Text -->
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-12 mb-sm-40">
                      
                            <!-- Logo Grid -->
                    <div class="row multi-columns-row mb-20">
                        
                        <!-- Logo Item -->
                        <div class="col-sm-6 col-md-4 col-lg-4 mb-sm-20">
                            <h3 class="alt-features-title font-alt mb-0 mt-0 pt-10 pb-10" style="background: #dddddd;padding: 10px;">REPARACIÓN DE TUBERÍA</h3>
                            <div class="blog-post-prev-small mb-20 clearfix" style="background:#fff;">
                                        <a href=""><img src="<?php echo base_url('public/theme/images/productos_home_menu/rept.png') ?>" alt="" class="widget-posts-img" /></a>
                                        <div class="widget-posts-descr" style="font-size:10px;">
                                                <label><a href="<?php echo base_url('producto_reparacion_de_tuberia') ?>/1">ABRAZADERAS DE REPARACIÓN</a></label>
                                                <label><a href="<?php echo base_url('producto_reparacion_de_tuberia') ?>/2">ABRAZADERAS CAMPANA BELL JOINT</a></label>
                                                <label><a href="<?php echo base_url('producto_reparacion_de_tuberia') ?>/3">JUNTAS UNIVERSALES</a></label>
                                                <label><a href="<?php echo base_url('producto_reparacion_de_tuberia') ?>/4">COPLES DAYTON</a></label>
                                        </div>
                                    </div>
              
                        </div>
                        <!-- End Logo Item -->
                        
                        <!-- Logo Item -->
                        <div class="col-sm-6 col-md-4 col-lg-4 mb-sm-20">
                              <h3 class="alt-features-title font-alt mb-0 mt-0 pt-10 pb-10" style="background: #dddddd;padding: 10px;">TUBERÍA Y CONEXIONES</h3>
                            <div class="blog-post-prev-small mb-20 clearfix" style="background:#fff;">
                                <a href=""><img src="<?php echo base_url('public/theme/images/productos_home_menu/conex.png') ?>" alt="" class="widget-posts-img" style="width:88px;" /></a>
                                        <div class="widget-posts-descr" style="font-size:10px;">
<label><a href="<?php echo base_url('producto_tuberia_conexiones') ?>/1">FIERRO VACIADO (FOFO)</a></label>
<label><a href="<?php echo base_url('producto_tuberia_conexiones') ?>/2">HIERRO DÚCTIL</a></label>
<label><a href="<?php echo base_url('producto_tuberia_conexiones') ?>/3">PVC C-900</a></label>
<label><a href="<?php echo base_url('producto_tuberia_conexiones') ?>/4">ALCANTARILLADO S. 20 y 25</a></label>
<label> <a href="<?php echo base_url('producto_tuberia_conexiones') ?>/5">PEAD GRANDES DIAMETROS</a></label>
<label><a href="<?php echo base_url('producto_tuberia_conexiones') ?>/6">ACERO</a></label>
<label> <a href="<?php echo base_url('producto_tuberia_conexiones') ?>/7">COBRE</a></label>
<label><a href="<?php echo base_url('producto_tuberia_conexiones') ?>/8">PVC SI y SM</a></label>
<label><a href="<?php echo base_url('producto_tuberia_conexiones') ?>/9">PVC C-80</a></label>
<label><a href="<?php echo base_url('producto_tuberia_conexiones') ?>/10">PVC C-40</a></label>
<label><a href="<?php echo base_url('producto_tuberia_conexiones') ?>/11">POLIPROPILENO</a></label>
<label><a href="<?php echo base_url('producto_tuberia_conexiones') ?>/12">DWV</a></label>
                                        </div>
                                    </div>
                            
                        
                        </div>
                        <!-- End Logo Item -->
                        
                        <!-- Logo Item -->
                        <div class="col-sm-6 col-md-4 col-lg-4 mb-sm-20">
                            <h3 class="alt-features-title font-alt mb-0 mt-0 pt-10 pb-10" style="background: #dddddd;padding: 10px;">CONTRA INCENDIO</h3>
                            <div class="blog-post-prev-small mb-20 clearfix" style="background:#fff;">
                                <a href=""><img src="<?php echo base_url('public/theme/images/productos_home_menu/incendio.png') ?>" alt="" class="widget-posts-img" style="width:88px;" /></a>
                                        <div class="widget-posts-descr" style="font-size:10px;">
             <label><a href="<?php echo base_url('producto_contra_incendio') ?>/1">HIDRANTES BARRIL SECO</a></label>	
                                                               <label><a href="<?php echo base_url('producto_contra_incendio') ?>/2">HIDRANTES BARRIL HÚMEDO</a></label>
                                                               <label><a href="<?php echo base_url('producto_contra_incendio') ?>/3">EXTENSIONES DE HIDRANTE</a></label>
                                                               <label><a href="<?php echo base_url('producto_contra_incendio') ?>/4">REPUESTOS Y KITS DE REPARACIÓN</a></label>
                                                               <label><a href="<?php echo base_url('producto_contra_incendio') ?>/5">POSTE INDICADOR VERTICAL</a></label>
                                                               <label><a href="<?php echo base_url('producto_contra_incendio') ?>/6">POSTE INDICADOR DE PARED</a></label>
                                                               <label><a href="<?php echo base_url('producto_contra_incendio') ?>/7">EQUIPO CONTRA INCENDIO INTEGRAL</a></label>
                                                               <label><a href="<?php echo base_url('producto_contra_incendio') ?>/8">VÁLVULAS UL/FM</a></label>
                                                               <label><a href="<?php echo base_url('producto_contra_incendio') ?>/9">LLAVES Y HERRAMIENTAS</a></label>
                                        </div>
                                    </div>
                        
                        </div>
                        <!-- End Logo Item -->
                   
                        <!-- Logo Item -->
                        <div class="col-sm-6 col-md-4 col-lg-4 mb-sm-20">
                             <h3 class="alt-features-title font-alt mb-0 mt-0 pt-10 pb-10" style="background: #dddddd;padding: 10px;">CONDUCCIÓN Y DERIVACIÓN</h3>
                            <div class="blog-post-prev-small mb-20 clearfix" style="background:#fff;">
                                <a href=""><img src="<?php echo base_url('public/theme/images/productos_home_menu/conduccionderivacion.png') ?>" alt="" class="widget-posts-img" style="width:88px;" /></a>
                                        <div class="widget-posts-descr" style="font-size:10px;">
                                                        <label><a href="<?php echo base_url('producto_conduccion_derivacion') ?>/1">TEE PARTIDA</a></label>
                                                        <label><a href="<?php echo base_url('producto_conduccion_derivacion') ?>/2">COLLARINES (ABRAZADERAS)</a></label>
                                                        <label><a href="<?php echo base_url('producto_conduccion_derivacion') ?>/3">JUNTAS UNIVERSALES</a></label>
                                                        <label><a href="<?php echo base_url('producto_conduccion_derivacion') ?>/4">JUNTAS GIBAULT</a></label>
                                                        <label> <a href="<?php echo base_url('producto_conduccion_derivacion') ?>/5">ADAPTADORES BRIDADOS</a></label>
                                                        <label><a href="<?php echo base_url('producto_conduccion_derivacion') ?>/6">BRIDAS RÁPIDAS</a></label>
                                                        <label> <a href="<?php echo base_url('producto_conduccion_derivacion') ?>/7">TALADRO VEGA</a></label>
                                                        <label><a href="<?php echo base_url('producto_conduccion_derivacion') ?>/8">TOMAS DOMICILIARIAS</a></label>
                                                        <label><a href="<?php echo base_url('producto_conduccion_derivacion') ?>/9">HERRAMIENTAS</a></label>        
                                        </div>
                                    </div>

                        </div>
                        <!-- End Logo Item -->
                        
                        <!-- Logo Item -->
                        <div class="col-sm-6 col-md-4 col-lg-4 mb-sm-20">
                                <h3 class="alt-features-title font-alt mb-0 mt-0 pt-10 pb-10" style="background: #dddddd;padding: 10px;">ALCANTARILLADO</h3>
                            <div class="blog-post-prev-small mb-20 clearfix" style="background:#fff;">
                                <a href=""><img src="<?php echo base_url('public/theme/images/productos_home_menu/alcantarillado.png') ?>" alt="" class="widget-posts-img" style="width:88px;" /></a>
                                        <div class="widget-posts-descr" style="font-size:10px;">
                                                    <label><a href="<?php echo base_url('producto_alcantarillado') ?>/1">BROCALES </a></label>
                                                                    <label><a href="<?php echo base_url('producto_alcantarillado') ?>/2">ESCALONES</a></label>
                                                                    <label><a href="<?php echo base_url('producto_alcantarillado') ?>/3">VÁLVULA ANTI-RETORNO</a></label>
                                                                    <label><a href="<?php echo base_url('producto_alcantarillado') ?>/4">MANGAS DE EMPOTRAMIENTO</a></label>  
                                        </div>
                                    </div>
        
                        </div>
                        <!-- End Logo Item -->
                        
                        <!-- Logo Item -->
                        <div class="col-sm-6 col-md-4 col-lg-4 mb-sm-20">
                              <h3 class="alt-features-title font-alt mb-0 mt-0 pt-10 pb-10" style="background: #dddddd;padding: 10px;">SISTEMAS DE BOMBEO</h3>
                            <div class="blog-post-prev-small mb-20 clearfix" style="background:#fff;">
                                <a href=""><img src="<?php echo base_url('public/theme/images/productos_home_menu/sistemabombeo.png') ?>" alt="" class="widget-posts-img" style="width:88px;" /></a>
                                        <div class="widget-posts-descr" style="font-size:10px;">
                                                      <label><a href="<?php echo base_url('producto_sistema_bombeo') ?>/1">BOMBAS VERTICALES</a></label>
                                                            <label><a href="<?php echo base_url('producto_sistema_bombeo') ?>/2">BOMBAS HORIZONTALES BIPARTIDAS</a></label>
                                                            <label><a href="<?php echo base_url('producto_sistema_bombeo') ?>/3">BOMBAS SUMERGIBLES DE POZO PROFUNDO</a></label>
                                                            <label><a href="<?php echo base_url('producto_sistema_bombeo') ?>/4">BOMBAS SUMERGIBLES PARA DE LODOS</a></label>
                                                            <label> <a href="<?php echo base_url('producto_sistema_bombeo') ?>/5">BOMBAS TRAGASOLIDOS</a></label>
                                                            <label><a href="<?php echo base_url('producto_sistema_bombeo') ?>/6">BOMBAS ANSI</a></label>
                                                            <label><a href="<?php echo base_url('producto_sistema_bombeo') ?>/7">EQUIPOS PRE-ENSAMBLADOS</a></label>
                                                            <label><a href="<?php echo base_url('producto_sistema_bombeo') ?>/8">DOSIFICADORAS</a></label>
                                        </div>
                                    </div>
   
                        </div>
                        <!-- End Logo Item -->
                       
                                <!-- Logo Item -->
                        <div class="col-sm-6 col-md-4 col-lg-4 mb-sm-20">
                               <h3 class="alt-features-title font-alt mb-0 mt-0 pt-10 pb-10" style="background: #dddddd;padding: 10px;">VÁLVULAS Y CAJAS DE VÁLVULAS </h3>
                            <div class="blog-post-prev-small mb-20 clearfix" style="background:#fff;">
                                <a href=""><img src="<?php echo base_url('public/theme/images/productos_home_menu/cajas.png') ?>" alt="" class="widget-posts-img" style="width:88px;" /></a>
                                        <div class="widget-posts-descr" style="font-size:10px;">
                            <label><a href="<?php echo base_url('producto_valvulas') ?>/1">VÁLVULAS COMPUERTA</a></label>
                                                                <label><a href="<?php echo base_url('producto_valvulas') ?>/2">VÁLVULAS MARIPOSA</a></label>
                                                                <label><a href="<?php echo base_url('producto_valvulas') ?>/3">VÁLVULAS CUCHILLA</a></label>
                                                                <label><a href="<?php echo base_url('producto_valvulas') ?>/4">VÁLVULAS DE EXPULSIÓN Y/O ADMISIÓN DE AIRE</a></label>
                                                                <label> <a href="<?php echo base_url('producto_valvulas') ?>/5">VÁLVULAS DE CONTROL</a></label>
                                                                <label><a href="<?php echo base_url('producto_valvulas') ?>/6">VÁLVULAS CHECK</a></label>
                                                                <label> <a href="<?php echo base_url('producto_valvulas') ?>/7">VÁLVULAS PLUG</a></label>
                                                                <label><a href="<?php echo base_url('producto_valvulas') ?>/8">MARCO CON TAPA</a></label>
                                                                <label><a href="<?php echo base_url('producto_valvulas') ?>/9">REGISTRO TELESCOPIADO</a></label>
                                                                 <!--<label><a href="<?php echo base_url('producto_valvulas') ?>/10">TAPA SUPERIOR Y CAMPANA</a></label>-->
                                                                 <label><a href="<?php echo base_url('producto_valvulas') ?>/11">VÁLVULAS BACKFLOW</a></label>
                                        </div>
                                    </div>
               
                        </div>
                        <!-- End Logo Item -->
                             <!-- Logo Item -->
                        <div class="col-sm-6 col-md-4 col-lg-4 mb-sm-20">
                               <h3 class="alt-features-title font-alt mb-0 mt-0 pt-10 pb-10" style="background: #dddddd;padding: 10px;">FILTROS </h3>
                            <div class="blog-post-prev-small mb-20 clearfix" style="background:#fff;">
                                <a href=""><img src="<?php echo base_url('public/theme/images/productos_home_menu/filtros.png') ?>" alt="" class="widget-posts-img" style="width:88px;" /></a>
                                        <div class="widget-posts-descr" style="font-size:10px;">
                              <label><a href="<?php echo base_url('producto_filtros') ?>/1">FILTRO CANASTA SUPERIOR</a></label>
                                                            <label><a href="<?php echo base_url('producto_filtros') ?>/2">FILTRO CANASTA</a></label>
                                                            <label><a href="<?php echo base_url('producto_filtros') ?>/3">FILTRO DUPLEX</a></label>
                                                            <label><a href="<?php echo base_url('producto_filtros') ?>/4">FILTRO YEE</a></label>
                                        </div>
                                    </div>
                  
                        </div>
                        <!-- End Logo Item -->
                             <!-- Logo Item -->
                        <div class="col-sm-6 col-md-4 col-lg-4 mb-sm-20">
                               <h3 class="alt-features-title font-alt mb-0 mt-0 pt-10 pb-10" style="background: #dddddd;padding: 10px;">MEDIDORES </h3>
                            <div class="blog-post-prev-small mb-20 clearfix" style="background:#fff;">
                                <a href=""><img src="<?php echo base_url('public/theme/images/productos_home_menu/medidores.png') ?>" alt="" class="widget-posts-img" style="width:88px;" /></a>
                                        <div class="widget-posts-descr" style="font-size:10px;">
                                           <label><a href="<?php echo base_url('producto_medidores') ?>/1">RESIDENCIALES</a></label>
                                            <label><a href="<?php echo base_url('producto_medidores') ?>/2">COMERCIAL</a></label>
                                        </div>
                                    </div>
                      
                        </div>
                        <!-- End Logo Item -->
                    </div>
                    <!-- End Logo Grid -->
                            
                            
                        </div>
                        
                       
                    </div>
                </div>
            </section>
            <!-- End Section -->