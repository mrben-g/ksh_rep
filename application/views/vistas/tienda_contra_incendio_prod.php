<?php
//$prod_id = $this->input->get('id', TRUE);
$prod_id = $this->uri->segment(2);
$i = 0;
?>
<section class="page-section pt-20 pb-40">
    <div class="container relative">
        <!-- Intro Text -->
        <div class="row">
            <div class="col-md-12 mb-10">
                <div class="section-text align-left">
                    <h3 class="tituloproductos1 mt-0 mb-0" style="">Contra incendio</h3>
                    <div class="linea"></div>                        
                </div>                    
            </div>
            <div class="col-md-12 mb-30">
                <div class="section-text align-left">
                    Tienda > <a href="<?php echo base_url('tienda') ?>" style="color: #2d77b3;">Nuestros productos</a> > <a href="<?php echo base_url('tienda_contra_incendio') ?>" style="color: #2d77b3;">Contra incendio</a> 
                                > <?php if($prod_id == 1){echo "HIDRANTES BARRIL SECO"; } ?>
                                   <?php if($prod_id == 2){echo "EXTENSIONES DE HIDRANTE"; } ?> 
                                   <?php if($prod_id == 3){echo "REPUESTOS Y KITS DE REPARACIÓN"; } ?> 
                                   <?php if($prod_id == 4){echo "VÁLVULAS UL/FM"; } ?>
                </div>
            </div>
        </div>
        <!-- End Intro Text -->
        <div class="clearfix"></div>
<?php if($prod_id == 1){
    
                        $total_ananido=0;
                        if(isset($_SESSION['carro'])){
                            $cart_session =  $_SESSION['carro'];
                            if($cart_session){
                                if(!empty($cart_session)): foreach($cart_session as $cs=>$carro_shop):
                                    if($cs == "3121-3BX16"){$total_ananido = $carro_shop['cantidad']; }
                                    endforeach; else:
                                        
                                        endif;
                                    
                                    }
                                    
                                    }
                        ?>          
        <div class="row">            
            <div class="col-md-12" style="padding: 30px;">
                <div class="row">
                   <div class="col-md-5">
                       <div style="height: 297px;width: 100%;background: #fff;margin-bottom: 2em;">
                         <img src="<?php  echo base_url('public/theme/images/contra_incendio/hidrante_catego.jpg') ?>" alt=""  style="width: 100%;margin-bottom: 2em;"/>
                     </div>

                   </div>
                    <div class="col-md-7" style="text-align:justify;">
                        <h6 class="titulo_producto">HIDRANTES BARRIL SECO</h6>
                        <fieldset></fieldset>                           
                        <div  class="section-text"  style="color:#000;">
                          Este tipo de hidrante mantiene su barril seco hasta el momento de ser necesario, es activado manualmente por una persona que debe abrir o cerrar la válvula para dejar pasar el fluido o cortarlo. Cuando la válvula se abre, el agua comienza a fluir por el sistema de forma automática. Esto funciona debido a la presión de agua en el sistema. Cuando la válvula está abierta, el aire y el agua se dirigen hacia la entrada.
                        </div>  
                         <h6 class="titulo_producto" style="font-size: 16px;">Características</h6>
                        <div>
                            <ul id="ven" style="">
                               <li><span>Presión de trabajo de 250 psig.</span></li>
<li><span>Conveniente válvula principal reversible.</span></li>
<li><span>Un avanzado acoplamiento de seguridad y diseño de brida, reducen el daño ocasionado a los hidrantes por accidentes de tráfico.</span></li>
<li><span>Un cople de seguridad de acero inoxidable del vástago resiste la corrosión y asegura una conexión firme entre las secciones del vástago.</span></li>
<li><span>El diseño hidráulico eficiente suministra un flujo máximo.</span></li>
<li><span>Las tomas para mangueras y bomba son roscadas y fáciles de reemplazar en el campo.</span></li>
<li><span>Sistema de lubricación forzado y arandelas antifricción facilitan la operación.</span></li>
<li><span>La válvula principal es fácilmente removida ya sea de la brida del bonete o de la brida de la línea a tierra.</span></li>
                            </ul>

                            
                        </div>
                         <div class="clearfix"></div>                       
                        <div class="section-text pt-20 pb-20">                            
                            <?php echo form_open_multipart(base_url('welcome/crea_sesion_producto'), array('id' => 'frm-prod')) ?>
                            <div class="row">                                
                                <div class="col-md-6 text-left">
                                    Apertura de la válvula:<br>
                                    51/4"
                                </div>
                                <div class="col-md-6">                                    
                                </div>
                            </div>                            
                            <div class="row">                                
                                <div id="" class="col-md-6 text-left">
                                    Tamaño de la válvula<br>
                                    <select id="valvula" name="valvula" >
                                       <option value="">Selecciona</option>
                                              <?php if(!empty($this->data['tam_valvulas'])): foreach($this->data['tam_valvulas'] as $tamano):?>
                                       <option value="<?php echo $tamano['TamanoValvula'];?>"><?php echo $tamano['TamanoValvula'];?></option>
                                        <?php 
                                          endforeach; else:
                                              
                                              endif;     
                                        ?>
                                   </select>                                  
                                </div>
                                <div class="col-md-6">
                                    
                                </div>
                            </div>
                             
                           <div class="row">                                
                                <div id="" class="col-md-6 text-left">
                                    Número de vías<br>
                                    <select id="vias" name="vias" >
                                       <option value="">Selecciona</option>
                                              <?php if(!empty($this->data['no_vias'])): foreach($this->data['no_vias'] as $vias):?>
                                       <option value="<?php echo $vias['NumeroVias'];?>"><?php echo $vias['NumeroVias'];?></option>
                                        <?php 
                                          endforeach; else:
                                              
                                              endif;     
                                        ?>
                                   </select>                                  
                                </div>
                                <div class="col-md-6">
                                    
                                </div>
                            </div>
                            <div class="row">                                
                                <div id="" class="col-md-6 text-left">
                                    Largo de barril<br>
                                    <select id="barril" name="barril" >
                                       <option value="">Selecciona</option>
                                              <?php if(!empty($this->data['barril'])): foreach($this->data['barril'] as $barril):?>
                                       <option value="<?php echo $barril['LargoBarril'];?>"><?php echo $barril['LargoBarril'];?></option>
                                        <?php 
                                          endforeach; else:
                                              
                                              endif;     
                                        ?>
                                   </select>                                  
                                </div>
                                <div class="col-md-6">
                                    
                                </div>
                            </div>
                            <div class="row">                                
                                <div id="" class="col-md-6 text-left">
                                    Tipo de junta<br>
                                    <select id="junta" name="junta" onchange="MuestraProdHidranteSeco(this.value)">
                                       <option value="">Selecciona</option>
                                              <?php if(!empty($this->data['tipo_junta'])): foreach($this->data['tipo_junta'] as $tipo_junta):?>
                                       <option value="<?php echo $tipo_junta['TipoJunta'];?>"><?php echo $tipo_junta['TipoJunta'];?></option>
                                        <?php 
                                          endforeach; else:
                                              
                                              endif;     
                                        ?>
                                   </select>                                  
                                </div>
                                <div class="col-md-6">
                                    
                                </div>
                            </div>
                             <br>
                            <input type="hidden" name="skus" id="skus" value="" />
                            <input type="hidden" name="nprod" id="nprod" value="" />
                            <input type="hidden" name="prices" id="prices" value="" />
                            <input type="hidden" name="tipo_moneda" id="tipo_moneda" value="" />
                            <input type="hidden" name="cantidad" id="cantidad" value="<?php if($total_ananido != 0){ echo $total_ananido+1;}else{ echo "1";}?>" />
                            <!--<input type="hidden" name="images" id="images" value="<?php ?>" />-->
                            <?php echo form_close(); ?>                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div id="filtros_de_productos">
                                <?php 
                              
                                ?>
                            </div>
                        </div>
                    </div>
<?php }                    
                    ?>
<?php if($prod_id == 2){
    
                        $total_ananido=0;
                        if(isset($_SESSION['carro'])){
                            $cart_session =  $_SESSION['carro'];
                            if($cart_session){
                                if(!empty($cart_session)): foreach($cart_session as $cs=>$carro_shop):
                                    if($cs == "3121-3BX16"){$total_ananido = $carro_shop['cantidad']; }
                                    endforeach; else:
                                        
                                        endif;
                                    
                                    }
                                    
                                    }
                        ?>          
        <div class="row">            
            <div class="col-md-12" style="padding: 30px;">
                <div class="row">
                   <div class="col-md-5">
                       <div style="height: 297px;width: 100%;background: #fff;margin-bottom: 2em;">
                         <img src="<?php  echo base_url('public/theme/images/contra_incendio/extension_hidrante.jpg') ?>" alt=""  style="width: 100%;margin-bottom: 2em;"/>
                     </div>

                   </div>
                    <div class="col-md-7" style="text-align:justify;">
                        <h6 class="titulo_producto">EXTENSIONES DE HIDRANTE</h6>
                        <fieldset></fieldset>                           
                        <div  class="section-text"  style="color:#000;">
                        Las extensiones para hidrantes son usadas cuando desconoces la longitud en donde deberá de estar tu hidrante. Este te permite agrandar el vástago en caso de que no coincida con la longitud que deseas.
                        </div>  
                         
                         <div class="clearfix"></div>                       
                        <div class="section-text pt-20 pb-20">                            
                            <?php echo form_open_multipart(base_url('welcome/crea_sesion_producto'), array('id' => 'frm-prod')) ?>
                            <div class="row">                                
                                <div class="col-md-6 text-left">
                                    Apertura de la válvula:<br>
                                    <select id="valvula" name="valvula">
                                       <option value="">Selecciona </option>
                                              <?php if(!empty($this->data['apertura_valvula'])): foreach($this->data['apertura_valvula'] as $valvula):?>
                                       <option value="<?php echo $valvula['AperturaValvula'];?>"><?php echo $valvula['AperturaValvula'];?></option>
                                        <?php 
                                          endforeach; else:
                                              
                                              endif;     
                                        ?>
                                   </select>          
                                </div>
                                <div class="col-md-6">                                    
                                </div>
                            </div>
                            <div class="row">                                
                                <div id="" class="col-md-6 text-left">
                                    Largo de barril<br>
                                    <select id="barril" name="barril" onchange="MuestraProdHidrante(this.value)">
                                       <option value="">Selecciona</option>
                                              <?php if(!empty($this->data['barril_hidrante'])): foreach($this->data['barril_hidrante'] as $barril):?>
                                       <option value="<?php echo $barril['LargoBarril'];?>"><?php echo $barril['LargoBarril'];?></option>
                                        <?php 
                                          endforeach; else:
                                              
                                              endif;     
                                        ?>
                                   </select>                                  
                                </div>
                                <div class="col-md-6">
                                    
                                </div>
                            </div>
                            
                             <br>
                            <input type="hidden" name="skus" id="skus" value="" />
                            <input type="hidden" name="nprod" id="nprod" value="" />
                            <input type="hidden" name="prices" id="prices" value="" />
                            <input type="hidden" name="tipo_moneda" id="tipo_moneda" value="" />
                            <input type="hidden" name="cantidad" id="cantidad" value="<?php if($total_ananido != 0){ echo $total_ananido+1;}else{ echo "1";}?>" />
                            <!--<input type="hidden" name="images" id="images" value="<?php ?>" />-->
                            <?php echo form_close(); ?>                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div id="filtros_de_productos">
                                <?php 
                              
                                ?>
                            </div>
                        </div>
                    </div>
<?php }                    
                    ?>        
<?php if($prod_id == 3){
    
                        $total_ananido=0;
                        if(isset($_SESSION['carro'])){
                            $cart_session =  $_SESSION['carro'];
                            if($cart_session){
                                if(!empty($cart_session)): foreach($cart_session as $cs=>$carro_shop):
                                    if($cs == "3121-3BX16"){$total_ananido = $carro_shop['cantidad']; }
                                    endforeach; else:
                                        
                                        endif;
                                    
                                    }
                                    
                                    }
                        ?>          
        <div class="row">            
            <div class="col-md-12" style="padding: 30px;">
                <div class="row">
                   <div class="col-md-5">
                       <div style="height: 297px;width: 100%;background: #fff;margin-bottom: 2em;">
                         <img src="<?php  echo base_url('public/theme/images/contra_incendio/kit_reparacion.jpg') ?>" alt=""  style="width: 100%;margin-bottom: 2em;"/>
                     </div>

                   </div>
                    <div class="col-md-7" style="text-align:justify;">
                        <h6 class="titulo_producto">REPUESTOS Y KITS DE REPARACIÓN</h6>
                        <fieldset></fieldset>                           
                        <div  class="section-text"  style="color:#000;">
                            Los kits de reparación facilitan y simplifican el mantenimiento de rutina y las mejoras al hidrante.<br>
                            Los juegos se ofrecen para cinco diferentes secciones del hidrante.
                        </div>  
                         
                         <h6 class="titulo_producto" style="font-size: 16px;">Tipos de juegos:</h6>
                        <div>
                            <ol id="ven" style="">
                               <li><span>Juego de reparación del bonete</span></li>
<li><span>Juego de reparación de la brida de Seguridad</span></li>
<li><span>Juego de Extensión</span></li>
<li><span>Juego de la válvula principal</span></li>
<li><span>Juego de reparación de la zapata</span></li>
                            </ol>
                        </div>
                         <div class="clearfix"></div>                                            
                        <div class="section-text pt-20 pb-20">                            
                            <?php echo form_open_multipart(base_url('welcome/crea_sesion_producto'), array('id' => 'frm-prod')) ?>
                            <div class="row">                                
                                <div class="col-md-6 text-left">
                                    Apertura de la válvula:<br>
                                    <select id="valvula" name="valvula" onchange="MuestraProdKit(this.value)">
                                       <option value="">Selecciona</option>
                                              <?php if(!empty($this->data['apertura_valvula_kit'])): foreach($this->data['apertura_valvula_kit'] as $valvula):?>
                                       <option value="<?php echo $valvula['AperturaValvula'];?>"><?php echo $valvula['AperturaValvula'];?></option>
                                        <?php 
                                          endforeach; else:
                                              
                                              endif;     
                                        ?>
                                   </select>          
                                </div>
                                <div class="col-md-6">                                    
                                </div>
                            </div>                           
                            
                             <br>
                            <input type="hidden" name="skus" id="skus" value="" />
                            <input type="hidden" name="nprod" id="nprod" value="" />
                            <input type="hidden" name="prices" id="prices" value="" />
                            <input type="hidden" name="tipo_moneda" id="tipo_moneda" value="" />
                            <input type="hidden" name="cantidad" id="cantidad" value="<?php if($total_ananido != 0){ echo $total_ananido+1;}else{ echo "1";}?>" />
                            <!--<input type="hidden" name="images" id="images" value="<?php ?>" />-->
                            <?php echo form_close(); ?>                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div id="filtros_de_productos">
                                <?php 
                              
                                ?>
                            </div>
                        </div>
                    </div>
<?php }                    
                    ?>
<?php if($prod_id == 4){
    
                        $total_ananido=0;
                        if(isset($_SESSION['carro'])){
                            $cart_session =  $_SESSION['carro'];
                            if($cart_session){
                                if(!empty($cart_session)): foreach($cart_session as $cs=>$carro_shop):
                                    if($cs == "3121-3BX16"){$total_ananido = $carro_shop['cantidad']; }
                                    endforeach; else:
                                        
                                        endif;
                                    
                                    }
                                    
                                    }
                        ?>          
        <div class="row">            
            <div class="col-md-12" style="padding: 30px;">
                <div class="row">
                   <div class="col-md-5 text-center">
                       <div style="height: 297px;width: 100%;background: #fff;margin-bottom: 2em;">
                         <img src="<?php  echo base_url('public/theme/images/contra_incendio/valvulas_fm_uno.jpg') ?>" alt=""  style="width: 100%;margin-bottom: 2em;max-width:149px;"/>
                     </div>

                   </div>
                    <div class="col-md-7" style="text-align:justify;">
                        <h6 class="titulo_producto">VÁLVULAS UL/FM</h6>
                        <fieldset></fieldset>                           
                        <div  class="section-text"  style="color:#000;">
                        Las válvulas UL/FM son de tipo compuerta y pueden ser de dos tipos con vástago fijo y vástago ascendente, las de vástago fijo tienen la ventaja de que pueden estar subterráneas y su vástago estará más protegido, las de vástago ascendente tienen la ventaja que están a la vista y su accionamiento en caso de una emergencia es más rápido.
                        </div>  
                         <div class="clearfix"></div>                       
                        <div class="section-text pt-20 pb-20">                            
                            <?php echo form_open_multipart(base_url('welcome/crea_sesion_producto'), array('id' => 'frm-prod')) ?>
                             <div class="row">                                
                                <div class="col-md-6 text-left">
                                    Tipo de Válvula:<br>
                                    <select id="nombre_valvula" name="nombr_valvula" onchange="MuestraProdNombreVal(this.value)">
                                       <option value="">Selecciona</option>
                                              <?php if(!empty($this->data['valvulas'])): foreach($this->data['valvulas'] as $nomVal):?>
                                       <option value="<?php echo $nomVal['Nombre'];?>"><?php echo $nomVal['Nombre'];?></option>
                                        <?php 
                                          endforeach; else:
                                              
                                              endif;     
                                        ?>
                                   </select>          
                                </div>
                                <div class="col-md-6">                                    
                                </div>
                            </div>
                            <div class="row">                                
                                <div class="col-md-6 text-left">
                                    Diámetro:<br>
                                    <select id="diam" name="diam" onchange="MuestraProdDiamVal(this.value)">
                                       <option value="">Selecciona diámetro</option>
                                              <?php if(!empty($this->data['diam_valvulas'])): foreach($this->data['diam_valvulas'] as $diametro):?>
                                       <option value="<?php echo $diametro['LargoBarril'];?>"><?php echo $diametro['LargoBarril'];?></option>
                                        <?php 
                                          endforeach; else:
                                              
                                              endif;     
                                        ?>
                                   </select>          
                                </div>
                                <div class="col-md-6">                                    
                                </div>
                            </div>
                            <div class="row">                                
                                <div id="" class="col-md-6 text-left">
                                    Tipo de junta<br>
                                    <select id="tipo_junta" name="tipo_junta" onchange="MuestraProdValvula(this.value)">
                                       <option value="">Selecciona </option>
                                              <?php if(!empty($this->data['tipo_junta_val'])): foreach($this->data['tipo_junta_val'] as $tipo):?>
                                       <option value="<?php echo $tipo['TipoJunta'];?>"><?php echo $tipo['TipoJunta'];?></option>
                                        <?php 
                                          endforeach; else:
                                              
                                              endif;     
                                        ?>
                                   </select>                                  
                                </div>
                                <div class="col-md-6">
                                    
                                </div>
                            </div>
                            
                             <br>
                            <input type="hidden" name="skus" id="skus" value="" />
                            <input type="hidden" name="nprod" id="nprod" value="" />
                            <input type="hidden" name="prices" id="prices" value="" />
                            <input type="hidden" name="tipo_moneda" id="tipo_moneda" value="" />
                            <input type="hidden" name="cantidad" id="cantidad" value="<?php if($total_ananido != 0){ echo $total_ananido+1;}else{ echo "1";}?>" />
                            <!--<input type="hidden" name="images" id="images" value="<?php ?>" />-->
                            <?php echo form_close(); ?>                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div id="filtros_de_productos">
                                <?php 
                              
                                ?>
                            </div>
                        </div>
                    </div>
<?php }                    
                    ?>                
                  </div>
</section>                    