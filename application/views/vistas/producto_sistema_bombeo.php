<?php
//$prod_id = $this->input->get('id', TRUE);

$prod_id = $this->uri->segment(2);
?>
 
<section class="page-section pt-20 pb-40">
    <div class="container relative">
        <!-- Intro Text -->
        <div class="row">
            <div class="col-md-12">
                <div class="section-text align-left">
                    <h3 class="tituloproductos1 mt-0 mb-0">Sistemas de Bombeo</h3>
                    <div class="linea"></div>
                </div>
            </div>
        </div>
        <!-- End Intro Text -->
        <!--<div class="clearfix"></div>-->
        
<?php if($prod_id == 1){  ?>          
        <div class="row">
            <div class="col-md-3">
                <?php 
                $this->load->view('vistas/menu_productos');
                ?>
            </div>
            <div class="col-md-9" style="background: #fff;padding: 30px;">
                <div class="row">
                   <div class="col-md-4 text-center">
                       <img src="<?php echo base_url('public/theme/images/productos/sistema_bombeo/Imagen-PeerlessPump.jpg') ?>" alt=""  style="width: 100%;max-width: 50px;"/>
                   </div>
                    <div class="col-md-8" style="text-align:justify;">
                        <h6 class="titulo_producto">Bombas Verticales</h6>
                        <fieldset></fieldset>
                        <div  class="section-text"  style="color:#000;">
                            Se les conoce así por la posición en que se encuentra el motor en vertical con la bomba, son de forma tipo lapicero y están diseñadas para aplicaciones de bombeo con líquidos limpios o ligeramente contaminados, su principal aplicación es en bombeo de pozo profundo o cárcamos.                   
                        </div>                        
                    </div>
                </div>
                <div class="row pt-40 pb-0">              
                      <div class="col-md-4" style="text-align:justify;"></div>
                    <div class="col-md-8">
                         <div class="row">
                            <div class="section-text col-md-12 text-justify">
                          <h6 class="titulo_producto" style="font-size: 16px;">Ventajas</h6>
                        <div>
                            <ul id="ven" style="">
                                <li><span>Espacio, pues éstas requieren un área pequeña horizontal.</span></li>
                                <li><span>El montaje y desmontaje se simplifica contando sólo con un amplio espacio vertical superior, para permitir el libre movimiento.</span></li>
                                <li><span>Transmisión por motores eléctricos verticales de eje hueco y eje sólido o por cabezal de engranajes para acoplar a motores diesel.</span></li>
                                <li><span>Lubricación por agua o aceite.</span></li>
                                <li><span>Impulsor del tipo cerrado o semi-abierto.</span></li>
                                <li><span>Fabricación en materiales estándar y especiales.</span></li>
                                <li><span>Empaque convencional o por sello mecánico.</span></li>
                            </ul>
                        </div>                    
                            </div>
                            <div class="section-text col-md-12">
                                                   <h6 class="titulo_producto" style="font-size: 16px;">Algunos conceptos importantes para la selección de la bomba</h6>
                        <div>
                            <ul id="ven" style="">
                                <li><span>Elaborar un diagrama de la disposición de bomba y tuberías</span></li>
                                <li><span>Calcular el gasto </span></li>
                                <li><span>Saber la carga a vencer (CDT)</span></li>
                                <li><span>Conocer las condiciones del liquido </span></li>
                                <li><span>El adame, ancho del pozo para que quepa el ancho de la bomba. Se recomienda mínimo ½” por lado.</span></li>
                                <li><span>Sumergencia de la bomba para garantizar el gasto de la misma. Este se da por la profundidad total del pozo menos la distancia hacia el espejo de agua.</span></li>
                                <li><span>Voltaje</span></li>
                            </ul>
                        </div>
                            </div>
                        </div>

                    </div>
                   
            </div>
                 <div class="row pt-0 pb-40">              
                      <div class="col-md-4" style="text-align:justify;"></div>
                    <div class="col-md-8">
                             <h6 class="titulo_producto" style="font-size: 16px;">Marca del Produto</h6>
                             <div class="row">
                            <div class=" col-md-12">
                                <div class="row">
                                    <div class="col-md-3">
                                        <img src="<?php echo base_url('public/theme/images/productos/sistema_bombeo/p/Logo-RuhRPumpen.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>
                                        <br><a href="pdf/sistema_bombeo/Catalago_RuhRPumpen-Bombas_Verticales.pdf" target="_blank">ficha técnica  </a>
                                    </div>
                                    <div class="col-md-3">
                                        <img src="<?php echo base_url('public/theme/images/productos/sistema_bombeo/p/logo-goulds.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>
                                        <br><a href="pdf/sistema_bombeo/Catalago_Goulds-Bombas_Verticales.pdf" target="_blank">ficha técnica </a>
                                    </div>
                                    <div class="col-md-3">
                                        <img src="<?php echo base_url('public/theme/images/productos/sistema_bombeo/p/Logo-Peerless.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>
                                        <br><a href="pdf/sistema_bombeo/Catalago_PeerlessPump-Bombas_Verticales.pdf" target="_blank">ficha técnica </a>
                                    </div>
                                    <div class="col-md-3">
                                        <img src="<?php echo base_url('public/theme/images/productos/sistema_bombeo/p/logo_franklin.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>
                                    </div>
                                </div>
                                
                                
                                
                                
                            </div>
                           </div>
                     </div>                      
                 </div>
                     
                <div class="row">
                    <div class="col-md-12" style="text-align:justify;">
                        <h4 class="mb-10">Cotizaciones o información de fichas técnicas</h4>
                         <fieldset></fieldset>
                        <form name="frm" id="frm" action="" method="post">
                            <input type="hidden" name="tipo" id="tipo" value="Bombas Verticales" />
                            <div class="row pb-20">
                            <div class="col-md-3"> 
                                Nombre*:<br/>
                                <input type="text" name="nombre" id="nombre" placeholder="Nombre" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3"> 
                                Empresa*:<br>
                                <input type="text" name="empresa" id="empresa"  placeholder="Empresa" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3">
                                Teléfono*:<br>
                                <input type="text" name="telefono" id="telefono"  placeholder="Teléfono" class="input-md round form-control" onkeypress="return isNumberKey(event)" maxlength="10"/> 
                            </div>
                            <div class="col-md-3">
                                E-mail*:<br>
                                <input type="text" name="correo" id="correo"  placeholder="E-mail" class="input-md round form-control"/>
                            </div>
                            </div>
                            <div class="row pb-20">
                           <div class="col-md-3">
                                Ciudad*:<br>
                                <input type="text" name="ciudad" id="ciudad"  placeholder="Ciudad a la que pertences" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-9">
                                Mensaje*:<br>
                                <textarea name="mensaje" id="mensaje" style="width: 100%;height:60px;" placeholder="Mensaje." class="input-md round form-control"></textarea>
                            </div>
                            </div>
                            <div class="row pb-20">
                             <div class="col-md-12 align-right">
                                   <a href="javascript:EnviaProdContacto()" style="padding:10px 25px;font-size: 15px;color:#fff;background:#000;text-decoration: none;">ENVIAR</a>
                             </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
<?php } if($prod_id == 2){  ?>  
   <div class="row">
            <div class="col-md-3">
                <?php 
                $this->load->view('vistas/menu_productos');
                ?>
            </div>
            <div class="col-md-9" style="background: #fff;padding: 30px;">
                <div class="row">
                   <div class="col-md-4">
                       <img src="<?php echo base_url('public/theme/images/productos/sistema_bombeo/imagen-RuhRPumpen.jpg') ?>" alt=""  style="width: 100%;"/>
                   </div>
                    <div class="col-md-8" style="text-align:justify;">
                        <h6 class="titulo_producto">Bombas horizontales bipartidas</h6>
                        <fieldset></fieldset>
                        <div  class="section-text"  style="color:#000;">
                            Es un tipo de bomba centrífuga y se les llama bipartidas por la forma en que se encuentra su caja dividida en dos, por donde pasa su eje de rotación, y su succión y descarga se hacen horizontalmente. Su principal aplicación es en rebombeo. 
                        </div>                            
                    </div>
                </div>
                <div class="row pt-0 pb-40">                   
                      <div class="col-md-4" style="text-align:justify;">                          
                     </div>                      
                    <div class="col-md-8">
                         <div class="row">
                            <div class="col-md-12">
                          <h6 class="titulo_producto" style="font-size: 16px;">Ventajas</h6>
                        <div>
                            <ul id="ven" style="">
                                <li><span>Su construcción es simple, en la mayoría de los casos su precio es menor al de otro tipo de bombas al igual que su mantenimiento.</span></li>
                                <li><span>Son fácil de instalar</span></li>
                                <li><span>Su impulsor y eje son las únicas partes en movimiento</span></li>
                                <li><span>Algunos tipos pueden manejar  fluidos con presencia de solidos </span></li>
                                <li><span>Especiales para altos caudales</span></li>
                            </ul>
                        </div>
                            </div>
                            <div class="col-md-12">
                              <h6 class="titulo_producto" style="font-size: 16px;">Algunos conceptos importantes para la selección de la bomba</h6>
                        <div>
                            <ul id="ven" style="">
                                <li><span>Elaborar un diagrama de la disposición de bomba y tuberías</span></li>
                                <li><span>Calcular el gasto </span></li>
                                <li><span>Saber la carga a vencer (CDT)</span></li>
                                <li><span>Conocer las condiciones del liquido </span></li>
                                <li><span>Elegir la clase y tipo de bomba</span></li>
                                <li><span>Voltaje</span></li>
                            </ul>
                        </div>
                            </div>
                        </div>
                         
                    </div>              
            </div>
                <div class="row pt-0 pb-40">                   
                      <div class="col-md-4" style="text-align:justify;">                          
                     </div>                      
                    <div class="col-md-8">                     
                             <h6 class="titulo_producto" style="font-size: 16px;">Marca del Produto</h6>
                             <div class="row">
                            <div class=" col-md-12">
                                <div class="row">
                                <div class=" col-md-4">
                                    <img src="<?php echo base_url('public/theme/images/productos/sistema_bombeo/p/Logo-RuhRPumpen.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>   
                                    <br><a href="pdf/sistema_bombeo/Catalago_RuhRPumpen.pdf" target="_blank">ficha técnica </a>
                                </div>  
                                <div class="col-md-4" >
                                    <img src="<?php echo base_url('public/theme/images/productos/sistema_bombeo/p/logo_barnes.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>
                                    <br><a href="pdf/sistema_bombeo/catalago_Barnes.pdf" target="_blank">ficha técnica </a>
                                </div>
                                </div>
                            </div>
                           </div>
                     </div>     
                </div>
                <div class="row">
                    <div class="col-md-12" style="text-align:justify;">
                        <h4 class="mb-10">Cotizaciones o información de fichas técnicas</h4>
                         <fieldset></fieldset>
                        <form name="frm" id="frm" action="" method="post">
                            <input type="hidden" name="tipo" id="tipo" value="Bombas horizontales bipartidass" />
                            <div class="row pb-20">
                            <div class="col-md-3"> 
                                Nombre*:<br/>
                                <input type="text" name="nombre" id="nombre" placeholder="Nombre" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3"> 
                                Empresa*:<br>
                                <input type="text" name="empresa" id="empresa"  placeholder="Empresa" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3">
                                Teléfono*:<br>
                                <input type="text" name="telefono" id="telefono"  placeholder="Teléfono" class="input-md round form-control" onkeypress="return isNumberKey(event)" maxlength="10"/> 
                            </div>
                            <div class="col-md-3">
                                E-mail*:<br>
                                <input type="text" name="correo" id="correo"  placeholder="E-mail" class="input-md round form-control"/>
                            </div>
                            </div>
                            <div class="row pb-20">
                            <div class="col-md-3">
                                Ciudad*:<br>
                                <input type="text" name="ciudad" id="ciudad"  placeholder="Ciudad a la que pertences" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-9">
                                Mensaje*:<br>
                                <textarea name="mensaje" id="mensaje" style="width: 100%;height:60px;" placeholder="Mensaje." class="input-md round form-control"></textarea>
                            </div>
                            </div>
                            <div class="row pb-20">
                             <div class="col-md-12 align-right">
                                    <a href="javascript:EnviaProdContacto()" style="padding:10px 25px;font-size: 15px;color:#fff;background:#000;text-decoration: none;">ENVIAR</a>
                             </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>        

<?php } if($prod_id == 3){  ?>  
<div class="row">
            <div class="col-md-3">
                <?php 
                $this->load->view('vistas/menu_productos');
                ?>
            </div>
            <div class="col-md-9" style="background: #fff;padding: 30px;">
                <div class="row">
                   <div class="col-md-4 text-center">
                       <img src="<?php echo base_url('public/theme/images/productos/sistema_bombeo/SP.jpg') ?>" alt=""  style="width: 100%;max-width: 90px"/>
                   </div>
                   
                    <div class="col-md-8" style="text-align:justify;">
                        <h6 class="titulo_producto">Bombas sumergibles de pozo profundo</h6>
                        <fieldset></fieldset>
                        <div  class="section-text"  style="color:#000;">
                           Se les llama así porque están diseñadas especialmente para que puedan sumergirse en líquidos y espacios muy reducidos, por lo que poseen un motor sumergible con protección que lo mantiene seco.
                        </div>                            
                    </div>
                </div>
                <div class="row pt-40 pb-0">                    
                     <div class="col-md-4"></div>
                     <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-12">
                          <h6 class="titulo_producto" style="font-size: 16px;">Ventajas</h6>
                        <div>
                            <ul id="ven" style="">
                              <li><span>Fuera de la vista</span></li>
                                         <li><span>Seguridad</span></li>
                                         <li><span>Nivel de ruido</span></li>
                                         <li><span>Eficiencia</span></li>
                            </ul>
                        </div>
                            </div>
                            <div class="col-md-12">
                              <h6 class="titulo_producto" style="font-size: 16px;">Algunos conceptos importantes para la selección de la bomba</h6>
                        <div>
                            <ul id="ven" style="">
                          <li><span>Elaborar un diagrama de la disposición de bomba y tuberías</span></li>
                                         <li><span>Calcular el gasto</span></li>
                                         <li><span>Saber la carga a vencer (CDT)</span></li>
                                         <li><span>Conocer las condiciones del liquido</span></li>
                                         <li><span>El adame, ancho del pozo para que quepa el ancho de la bomba. Se recomienda mínimo ½” por lado.</span></li>
                                         <li><span>Sumergencia de la bomba para garantizar el gasto de la misma. Este se da por la profundidad total del pozo menos la distancia hacia el espejo de agua.</span></li>
                                         <li><span>Voltaje</span></li>
                            </ul>
                        </div>
                            </div>
                        </div>                                        
                    </div>           
            </div>
                <div class="row pt-0 pb-40">                    
                         <div class="col-md-4"></div>
                     <div class="col-md-8">               
                             <h6 class="titulo_producto" style="font-size: 16px;">Marca del Produto</h6>
                             <div class="row">
                            <div class=" col-md-12">
                                <div class="row">
                                <div class="col-md-4">
                                    <img src="<?php echo base_url('public/theme/images/productos/sistema_bombeo/p/logo_barnes.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>  
                                    <br><a href="pdf/sistema_bombeo/Catalago_Caracteristicas_Barnes.pdf" target="_blank">ficha técnica</a>
                                </div>  
                                <div class="col-md-4">
                                    <img src="<?php echo base_url('public/theme/images/productos/sistema_bombeo/p/grundfos-logo.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>                                   
                                    <br><a href="pdf/sistema_bombeo/Catalago_Curvas_Barnes.pdf" target="_blank">ficha técnica</a>
                                </div>  
                                 <div class="col-md-4">
                                    <img src="<?php echo base_url('public/theme/images/productos/sistema_bombeo/p/logo_franklin.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>                                     
                                     <br><a href="pdf/sistema_bombeo/Catalago_Grundfos.pdf" target="_blank">ficha técnica</a>
                                </div>
                                </div>
                            </div>
                           </div>
                     </div>            
                </div>
                <div class="row">
                    <div class="col-md-12" style="text-align:justify;">
                        <h4 class="mb-10">Cotizaciones o información de fichas técnicas</h4>
                         <fieldset></fieldset>
                        <form name="frm" id="frm" action="" method="post">
                            <input type="hidden" name="tipo" id="tipo" value="Bombas sumergibles de pozo profundo" />
                            <div class="row pb-20">
                            <div class="col-md-3"> 
                                Nombre*:<br/>
                                <input type="text" name="nombre" id="nombre" placeholder="Nombre" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3"> 
                                Empresa*:<br>
                                <input type="text" name="empresa" id="empresa"  placeholder="Empresa" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3">
                                Teléfono*:<br>
                                <input type="text" name="telefono" id="telefono"  placeholder="Teléfono" class="input-md round form-control" onkeypress="return isNumberKey(event)" maxlength="10"/> 
                            </div>
                            <div class="col-md-3">
                                E-mail*:<br>
                                <input type="text" name="correo" id="correo"  placeholder="E-mail" class="input-md round form-control"/>
                            </div>
                            </div>
                            <div class="row pb-20">
                            <div class="col-md-3">
                                Ciudad*:<br>
                                <input type="text" name="ciudad" id="ciudad"  placeholder="Ciudad a la que pertences" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-9">
                                Mensaje*:<br>
                                <textarea name="mensaje" id="mensaje" style="width: 100%;height:60px;" placeholder="Mensaje." class="input-md round form-control"></textarea>
                            </div>
                            </div>
                            <div class="row pb-20">
                             <div class="col-md-12 align-right">
                                     <a href="javascript:EnviaProdContacto()" style="padding:10px 25px;font-size: 15px;color:#fff;background:#000;text-decoration: none;">ENVIAR</a>
                             </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>                

<?php } if($prod_id == 4){  ?>  
 <div class="row">
            <div class="col-md-3">
                <?php 
                $this->load->view('vistas/menu_productos');
                ?>
            </div>
            <div class="col-md-9" style="background: #fff;padding: 30px;">
                <div class="row">
                   <div class="col-md-4">
                       <img src="<?php echo base_url('public/theme/images/productos/sistema_bombeo/profundo_lodos.jpg') ?>" alt=""  style="width: 100%;"/>
                   </div>
                   
                    <div class="col-md-8" style="text-align:justify;">
                        <h6 class="titulo_producto">Bombas sumergibles para lodos</h6>
                        <fieldset></fieldset>
                        <div  class="section-text"  style="color:#000;">
                            Se les llama así porque pueden bombear líquidos con un alto contenido de sólidos en pozos, cárcamos de aguas negras o durante el proceso de perforación en construcciones.
                        </div>                            
                    </div>
                </div>
                <div class="row pt-40 pb-0">                    
                      <div class="col-md-4" style="text-align:justify;">                        
                           
                     </div>                      
                    <div class="col-md-8">  
                        <div class="row">
                            <div class="col-md-12">
                               <h6 class="titulo_producto" style="font-size: 16px;">Ventajas</h6>
                               <div>
                                   <ul id="ven" style="">
                                        <li><span>Arranque fácil.</span></li>
                                        <li><span>Alta resistencia al desgaste</span></li>
                                        <li><span>Mantenimiento.</span></li>   
                                   </ul>
                               </div>
                            </div>
                            <div class="col-md-12">
                               <h6 class="titulo_producto" style="font-size: 16px;">Algunos conceptos importantes para la selección de la bomba</h6>
                        <div>
                            <ul id="ven" style="">
                                <li><span>Elaborar un diagrama de la disposición de bomba y tuberías</span></li>
                                <li><span>Calcular el gasto</span></li>
                                <li><span>Saber la carga a vencer (CDT)</span></li>
                                <li><span>Conocer las condiciones del liquido</span> </li>
                                <li><span>Capacidad del cárcamo</span></li>
                                <li><span>Sumergencia de la bomba para garantizar el gasto de la misma. Este se da por la profundidad total del pozo menos la distancia hacia el espejo de agua.</span></li>
                                <li><span>Voltaje</span></li>
                            </ul>
                        </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="row pt-0 pb-40">                    
                     <div class="col-md-4" style="text-align:justify;">                        
                           
                     </div>                      
                    <div class="col-md-8">        
                             <h6 class="titulo_producto" style="font-size: 16px;">Marca del Produto</h6>
                             <div class="row">
                            <div class=" col-md-12">
                                <div class="row">                                
                                <div class="col-md-4">
                                    <img src="<?php echo base_url('public/theme/images/productos/sistema_bombeo/p/logo_barnes.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>
                                </div>  
                                    <div class="col-md-5"><br>
                                            <a href="pdf/sistema_bombeo/FichaBarnesSerie_3BWSE-DS.pdf" target="_blank">Catalogó Serie 3BWSE-DS </a><br>
                                             <a href="pdf/sistema_bombeo/FichaBarnesSerie_4BSE-HLDS.pdf" target="_blank">Catalogó Serie 4BSE-HLDS</a><br>
                                             <a href="pdf/sistema_bombeo/FichaBarnesSerie_4BWSE.pdf" target="_blank">Catalogó Serie 4BWSE</a><br>
                                             <a href="pdf/sistema_bombeo/FichaBarnesSerie_4SEH.pdf" target="_blank">Catalogó Serie 4SEH</a><br>
                                             <a href="pdf/sistema_bombeo/FichaBarnesSerie_4SESS.pdf" target="_blank">Catalogó Serie 4SESS</a><br>
                                             <a href="pdf/sistema_bombeo/FichaBarnesSerie_6SE-L.pdf" target="_blank">Catalogó Serie 6SE-L</a><br>
                                             <a href="pdf/sistema_bombeo/FichaBarnesSerie_8SE-HA.pdf" target="_blank">Catalogó Serie 8SE-HA</a><br>
                                             <a href="pdf/sistema_bombeo/FichaBarnesSerie_8SEHL.pdf" target="_blank">Catalogó Serie 8SEHL</a>
                                </div>  
                                </div>
                            </div>
                           </div>
                     </div>              
                </div>
                <div class="row">
                    <div class="col-md-12" style="text-align:justify;">
                        <h4 class="mb-10">Cotizaciones o información de fichas técnicas</h4>
                         <fieldset></fieldset>
                        <form name="frm" id="frm" action="" method="post">
                            <input type="hidden" name="tipo" id="tipo" value="Bombas sumergibles para lodos" />
                            <div class="row pb-20">
                            <div class="col-md-3"> 
                                Nombre*:<br/>
                                <input type="text" name="nombre" id="nombre" placeholder="Nombre" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3"> 
                                Empresa*:<br>
                                <input type="text" name="empresa" id="empresa"  placeholder="Empresa" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3">
                                Teléfono*:<br>
                                <input type="text" name="telefono" id="telefono"  placeholder="Teléfono" class="input-md round form-control" onkeypress="return isNumberKey(event)" maxlength="10"/> 
                            </div>
                            <div class="col-md-3">
                                E-mail*:<br>
                                <input type="text" name="correo" id="correo"  placeholder="E-mail" class="input-md round form-control"/>
                            </div>
                            </div>
                            <div class="row pb-20">
                            <div class="col-md-3">
                                Ciudad*:<br>
                                <input type="text" name="ciudad" id="ciudad"  placeholder="Ciudad a la que pertences" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-9">
                                Mensaje*:<br>
                                <textarea name="mensaje" id="mensaje" style="width: 100%;height:60px;" placeholder="Mensaje." class="input-md round form-control"></textarea>
                            </div>
                            </div>
                            <div class="row pb-20">
                             <div class="col-md-12 align-right">
                                    <a href="javascript:EnviaProdContacto()" style="padding:10px 25px;font-size: 15px;color:#fff;background:#000;text-decoration: none;">ENVIAR</a>
                             </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>                     

<?php } if($prod_id == 5){  ?>  
 <div class="row">
            <div class="col-md-3">
                <?php 
                $this->load->view('vistas/menu_productos');
                ?>
            </div>
            <div class="col-md-9" style="background: #fff;padding: 30px;">
                <div class="row">
                   <div class="col-md-4">
                       <img src="<?php echo base_url('public/theme/images/productos/sistema_bombeo/tragasolidos.jpg') ?>" alt=""  style="width: 100%;"/>
                   </div>
                   
                    <div class="col-md-8" style="text-align:justify;">
                        <h6 class="titulo_producto">Bombas tragasolidos</h6>
                        <fieldset></fieldset>
                        <div  class="section-text"  style="color:#000;">
                            Es un tipo de bomba centrifuga y se le conoce así  por la capacidad que tiene de dejar pasar fácilmente un alto contenido de sólidos que las bombas centrífugas convencionales no permiten, con esto minimizan el tiempo muerto que causan taponamientos. Estas bombas por lo general manejan sólidos con un tamaño que puede llegar hasta la mitad del de la abertura de descarga lo que hace más difícil obstruirlas.
                        </div>                            
                    </div>
                </div>
                <div class="row pt-40 pb-0">                    
                      <div class="col-md-4" style="text-align:justify;">                        
                          
                     </div>                      
                    <div class="col-md-8">  
                        <div class="row">
                            <div class="col-md-12">
                               <h6 class="titulo_producto" style="font-size: 16px;">Ventajas</h6>
                               <div>
                                   <ul id="ven" style="">
                                        <li><span>Autocebante</span></li>
                                        <li><span>Alto grado de resistencia a la corrosión </span></li>
                                        <li><span>Se puede desarmarse rápida y fácilmente en el lugar de trabajo para retirar los restos en caso de que la bomba se tape</span></li>                                         
                                   </ul>
                               </div>
                            </div>
                            <div class="col-md-12">
                               <h6 class="titulo_producto" style="font-size: 16px;">Algunos conceptos importantes para la selección de la bomba</h6>
                        <div>
                            <ul id="ven" style="">
                                <li><span>Elaborar un diagrama de la disposición de bomba y tuberías</span></li>
                                <li><span>Calcular el gasto</span></li>
                                <li><span>Saber la carga a vencer (CDT)</span></li>
                                <li><span>Conocer las condiciones del liquido</span> </li>
                                <li><span>Elegir la clase y tipo de bomba</span></li>
                                <li><span>Voltaje</span></li>
                            </ul>
                        </div>
                            </div>
                        </div>
                    </div>
                 
                </div>
                <div class="row pt-0 pb-40">                    
                   <div class="col-md-4" style="text-align:justify;">                        
                          
                     </div>                      
                    <div class="col-md-8">                    
                             <h6 class="titulo_producto" style="font-size: 16px;">Marca del Produto</h6>
                             <div class="row">
                            <div class=" col-md-12" style="">
                                <div class="row">                                
                                <div class="col-md-4">
                                    <img src="<?php echo base_url('public/theme/images/productos/sistema_bombeo/p/logo_barnes.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>
                                </div>  
                                    <div class="col-md-4">
                                          <a href="pdf/sistema_bombeo/FichaBarnesSerie_SH3-U.pdf" target="_blank">Catalogó SH3U </a><br>
                                            <a href="pdf/sistema_bombeo/FichaBarnesSerie_SH6-U.pdf" target="_blank">Catalogó SH6U</a><br>
                                            <a href="pdf/sistema_bombeo/FichaBarnesSerie_SH8-U.pdf" target="_blank">Catalogó SH8U</a><br>
                                            <a href="pdf/sistema_bombeo/FichaBarnesSerie_SH4-U.pdf" target="_blank">Catalogó SH4U</a><br>
                                    </div>
                                    <div class="col-md-4">
                                         <img src="<?php echo base_url('public/theme/images/productos/sistema_bombeo/p/Logo-WDM.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>
                                   <br>
                                          <a href="pdf/sistema_bombeo/Ficha_WDM.pdf" target="_blank">Catalogó WDM</a>
                                    </div>
                                
                                </div>
                                
                            </div>
                           </div>
                     </div>                     
                <div class="row">
                    <div class="col-md-12" style="text-align:justify;">
                        <h4 class="mb-10">Cotizaciones o información de fichas técnicas</h4>
                         <fieldset></fieldset>
                        <form name="frm" id="frm" action="" method="post">
                            <input type="hidden" name="tipo" id="tipo" value="Bombas tragasolidos" />
                            <div class="row pb-20">
                            <div class="col-md-3"> 
                                Nombre*:<br/>
                                <input type="text" name="nombre" id="nombre" placeholder="Nombre" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3"> 
                                Empresa*:<br>
                                <input type="text" name="empresa" id="empresa"  placeholder="Empresa" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3">
                                Teléfono*:<br>
                                <input type="text" name="telefono" id="telefono"  placeholder="Teléfono" class="input-md round form-control" onkeypress="return isNumberKey(event)" maxlength="10"/> 
                            </div>
                            <div class="col-md-3">
                                E-mail*:<br>
                                <input type="text" name="correo" id="correo"  placeholder="E-mail" class="input-md round form-control"/>
                            </div>
                            </div>
                            <div class="row pb-20">
                           <div class="col-md-3">
                                Ciudad*:<br>
                                <input type="text" name="ciudad" id="ciudad"  placeholder="Ciudad a la que pertences" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-9">
                                Mensaje*:<br>
                                <textarea name="mensaje" id="mensaje" style="width: 100%;height:60px;" placeholder="Mensaje." class="input-md round form-control"></textarea>
                            </div>
                            </div>
                            <div class="row pb-20">
                             <div class="col-md-12 align-right">
                                      <a href="javascript:EnviaProdContacto()" style="padding:10px 25px;font-size: 15px;color:#fff;background:#000;text-decoration: none;">ENVIAR</a>
                             </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>                

<?php } if($prod_id == 6){  ?>  
  <div class="row">
            <div class="col-md-3">
                <?php 
                $this->load->view('vistas/menu_productos');
                ?>
            </div>
            <div class="col-md-9" style="background: #fff;padding: 30px;">
                <div class="row">
                   <div class="col-md-4">
                       <img src="<?php echo base_url('public/theme/images/productos/sistema_bombeo/ansi.jpg') ?>" alt=""  style="width: 100%;"/>
                   </div>
                    <div class="col-md-8">
                        <h6 class="titulo_producto">Bombas ANSI</h6>
                        <fieldset></fieldset>
                        <div class="section-text text-justify">
                            Es un tipo de bomba centrifuga y se les llama ANSI porque tienen una medida estándar internacional. Esta bomba se puede usar para reemplazar el equipo existente sin ningún problema ni habrá necesidad de cambios en las tuberías o la base.
                        </div>                         
                    </div>
                </div>
                
                <div class="row pt-40 pb-0">                    
                      <div class="col-md-4">                        
                         
                     </div>                      
                   <div class="col-md-8">  
                        <div class="row">
                            <div class="col-md-12">
                               <h6 class="titulo_producto" style="font-size: 16px;">Ventajas</h6>
                               <div>
                                   <ul id="ven" style="">
                                       <li><span>Piezas reemplazables fácilmente</span></li>
                                       <li><span>Mantenimiento</span></li>
                                       <li><span>Fabricación en materiales estándar y especiales</span></li>
                                   </ul>
                               </div>
                            </div>
                            <div class="col-md-12">
                               <h6 class="titulo_producto" style="font-size: 16px;">Algunos conceptos importantes para la selección de la bomba</h6>
                        <div>
                            <ul id="ven" style="">
                                <li><span>Elaborar un diagrama de la disposición de bomba y tuberías</span></li>
                                <li><span>Calcular el gasto</span></li>
                                <li><span>Saber la carga a vencer (CDT)</span></li>
                                <li><span>Conocer las condiciones del liquido</span></li>
                                <li><span>Elegir la clase y tipo de bomba</span></li>
                                <li><span>Voltaje</span></li>
                            </ul>
                        </div>
                            </div>
                        </div>
                    </div>                 
                </div>
                    <div class="row pt-40 pb-40">                    
                     <div class="col-md-4">                        
                         
                     </div>                      
                   <div class="col-md-8">        
                             <h6 class="titulo_producto" style="font-size: 16px;">Marca del Produto</h6>
                             <div class="row">
                            <div class=" col-md-12">
                                <div class="row">                                
                                <div class="col-md-4" >
                                    <img src="<?php echo base_url('public/theme/images/productos/sistema_bombeo/p/LOGO_man-pumps.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>
                                    <br><a href="pdf/sistema_bombeo/Catalago_Bombas_ANSI 911_MANN_PUMPS.pdf" target="_blank">Catálogo</a>
                                </div>  
                               <div class="col-md-4">
                                    <img src="<?php echo base_url('public/theme/images/productos/sistema_bombeo/p/Logo-Peerless.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>
                                    <br><a href="pdf/sistema_bombeo/CatalagoBomba_B-8196_ANSI_Peerless.pdf" target="_blank">Catálogo</a>
                                </div>  
                                    <div class="col-md-4">
                                    <img src="<?php echo base_url('public/theme/images/productos/sistema_bombeo/p/Logo-RuhRPumpen.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>
                                    <br><a href="pdf/sistema_bombeo/catalago-RUHRPUMPEN.pdf" target="_blank">Catálogo</a>
                                </div>  
                                </div>
                            </div>
                           </div>
                     </div>                      
                    </div>
                <div class="row">
                    <div class="col-md-12" style="text-align:justify;">
                        <h4 class="mb-10">Cotizaciones o información de fichas técnicas</h4>
                         <fieldset></fieldset>
                        <form name="frm" id="frm" action="" method="post">
                            <input type="hidden" name="tipo" id="tipo" value="Bombas ANSI" />
                            <div class="row pb-20">
                            <div class="col-md-3"> 
                                Nombre*:<br/>
                                <input type="text" name="nombre" id="nombre" placeholder="Nombre" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3"> 
                                Empresa*:<br>
                                <input type="text" name="empresa" id="empresa"  placeholder="Empresa" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3">
                                Teléfono*:<br>
                                <input type="text" name="telefono" id="telefono"  placeholder="Teléfono" class="input-md round form-control" onkeypress="return isNumberKey(event)" maxlength="10"/> 
                            </div>
                            <div class="col-md-3">
                                E-mail*:<br>
                                <input type="text" name="correo" id="correo"  placeholder="E-mail" class="input-md round form-control"/>
                            </div>
                            </div>
                            <div class="row pb-20">
                            <div class="col-md-3">
                                Ciudad*:<br>
                                <input type="text" name="ciudad" id="ciudad"  placeholder="Ciudad a la que pertences" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-9">
                                Mensaje*:<br>
                                <textarea name="mensaje" id="mensaje" style="width: 100%;height:60px;" placeholder="Mensaje." class="input-md round form-control"></textarea>
                            </div>
                            </div>
                            <div class="row pb-20">
                             <div class="col-md-12 align-right">
                                     <a href="javascript:EnviaProdContacto()" style="padding:10px 25px;font-size: 15px;color:#fff;background:#000;text-decoration: none;">ENVIAR</a>
                             </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>                

<?php } if($prod_id == 7){  ?>  
<div class="row">
            <div class="col-md-3">
                <?php 
                $this->load->view('vistas/menu_productos');
                ?>
            </div>
            <div class="col-md-9" style="background: #fff;padding: 30px;">
                <div class="row">
                   <div class="col-md-4 text-center">
                       <img src="<?php echo base_url('public/theme/images/productos/sistema_bombeo/booster.jpg') ?>" alt=""  style="width: 100%;"/>
                   </div>
                   
                    <div class="col-md-8">
                        <h6 class="titulo_producto">Equipos Pre-ensamblados Booster</h6>
                        <fieldset></fieldset>
                        <div class="section-text text-justify">
                             Son equipos  que le permiten un gran ahorro en el consumo de energía ya que la potencia de la bomba depende de la demanda del sistema, manteniendo siempre una presión constante. El equipo puede ser ensamblado con las bombas Verticales, Centrifugas Horizontales y Sumergibles de pozo Profundo.
                        </div>

                    </div>
                </div>
                <div class="row pt-40 pb-40">                    
                    <div class="col-md-4 text-center">
                       <img src="<?php echo base_url('public/theme/images/productos/sistema_bombeo/7.jpg') ?>" alt=""  style="width: 100%;max-width: 180px"/>
                   </div>                   
                    <div class="col-md-8">
                        <h6 class="titulo_producto">Sistemas Hidroneumáticos presión variable</h6>
                        <fieldset></fieldset>
                        <div class="section-text text-justify">
                             Es un equipo totalmente diseñado e integrado, compacto, eficiente y versátil evitando construir tanques elevados para mantener presión en la red principal.<br>
                             Puede estar  compuesto de 2 a 4 bombas las cuales se alternan simultáneamente para degaste uniforme y de 1 a 2 tanques de diafragma precargados de 119 Galones según sea la aplicación y la demanda.
                        </div>
                    </div>
                </div>
                
                <div class="row pt-40 pb-40">                    
                      <div class="col-md-4">                        
                         
                     </div>                      
                   <div class="col-md-8">                      
                             <h6 class="titulo_producto" style="font-size: 16px;">Marca del Produto</h6>
                             <div class="row">
                            <div class=" col-md-12" style="">
                                <div class="row">                                
                                <div class="col-md-6">
                                    <img src="<?php echo base_url('public/theme/images/productos/sistema_bombeo/p/grundfos-logo.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>
                                      <a href="pdf/sistema_bombeo/Folleto_BoosterpaQ_Grundfos.pdf" target="_blank">Catálogo</a>
                                </div>  
                               <div class="col-md-6" >
                                    <img src="<?php echo base_url('public/theme/images/productos/sistema_bombeo/p/logo_barnes.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/><br>
                                    <a href="pdf/sistema_bombeo/Ficha_Barnes_serie _ BOOSTER_SYSTEM.pdf" target="_blank">BOOSTER</a><br>
                                    <a href="pdf/sistema_bombeo/Ficha_Barnes_Sistemas_Hidroneumatico_serie_HIDRO3B2T.pdf" target="_blank">Hidroneumático serie HIDRO3B2T</a><br>
                                    <a href="pdf/sistema_bombeo/Ficha_Barnes_Sistemas_Hidroneumatico.pdf" target="_blank">Sistemas Hidroneumático</a>
                                </div>                                  
                                      
                                </div>
                            </div>
                           </div>
                     </div>                      
                    <div class="col-md-8">  
                        
                    </div>
                 
                </div>
                <div class="row">
                    <div class="col-md-12" style="text-align:justify;">
                        <h4 class="mb-10">Cotizaciones o información de fichas técnicas</h4>
                         <fieldset></fieldset>
                        <form name="frm" id="frm" action="" method="post">
                            <input type="hidden" name="tipo" id="tipo" value="Equipos Pre-ensamblados" />
                            <div class="row pb-20">
                            <div class="col-md-3"> 
                                Nombre*:<br/>
                                <input type="text" name="nombre" id="nombre" placeholder="Nombre" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3"> 
                                Empresa*:<br>
                                <input type="text" name="empresa" id="empresa"  placeholder="Empresa" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3">
                                Teléfono*:<br>
                                <input type="text" name="telefono" id="telefono"  placeholder="Teléfono" class="input-md round form-control" onkeypress="return isNumberKey(event)" maxlength="10"/> 
                            </div>
                            <div class="col-md-3">
                                E-mail*:<br>
                                <input type="text" name="correo" id="correo"  placeholder="E-mail" class="input-md round form-control"/>
                            </div>
                            </div>
                            <div class="row pb-20">
                            <div class="col-md-3">
                                Ciudad*:<br>
                                <input type="text" name="ciudad" id="ciudad"  placeholder="Ciudad a la que pertences" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-9">
                                Mensaje*:<br>
                                <textarea name="mensaje" id="mensaje" style="width: 100%;height:60px;" placeholder="Mensaje." class="input-md round form-control"></textarea>
                            </div>
                            </div>
                            <div class="row pb-20">
                             <div class="col-md-12 align-right">
                                   <a href="javascript:EnviaProdContacto()" style="padding:10px 25px;font-size: 15px;color:#fff;background:#000;text-decoration: none;">ENVIAR</a>
                             </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>                    

<?php } if($prod_id == 8){  ?>  
<div class="row">
            <div class="col-md-3">
                <?php 
                $this->load->view('vistas/menu_productos');
                ?>
            </div>
            <div class="col-md-9" style="background: #fff;padding: 30px;">
                <div class="row">
                   <div class="col-md-4">
                       <img src="<?php echo base_url('public/theme/images/productos/sistema_bombeo/dosificador.gif') ?>" alt=""  style="width: 100%;"/>
                   </div>                   
                    <div class="col-md-8">
                        <h6 class="titulo_producto">Bombas Dosificadoras</h6>
                        <fieldset></fieldset>
                        <div class="section-text text-justify">
                      Se les llama así porque pueden bombear líquidos con un alto contenido de sólidos en pozos, cárcamos de aguas negras o durante el proceso de perforación en construcciones.
                        </div>                         
                    </div>
                </div>
                <div class="row pt-40 pb-40">                    
                      <div class="col-md-4">                        
                         
                     </div>                      
                    <div class="col-md-8">  
                        <div class="row">
                            <div class="col-md-12">
                             <h6 class="titulo_producto" style="font-size: 16px;">Ventajas</h6>
                               <div>
                                   <ul id="ven" style="">
                                        <li><span>Funcionan a bajo caudal</span></li>
                                        <li><span>Mantenimiento fácil en campo</span></li>
                                        <li><span>Alta resistencia a los químicos</span></li> 
                                        <li><span>Alta disolución de los aditivos</span></li> 
                                        <li><span>Gran resistencia a los rayos UV</span></li> 
                                        <li><span>Fácil de instalar</span></li> 
                                        <li><span>Fácil ajuste del porcentaje de dosificación</span></li>      
                                   </ul>
                               </div>
                            </div>
                            
                        </div>
                    </div>
                 
                </div>
                <div class="row pt-0 pb-40">                    
                     <div class="col-md-4">                        
                         
                     </div>                      
                    <div class="col-md-8">               
                             <h6 class="titulo_producto" style="font-size: 16px;">Marca del Produto</h6>
                             <div class="row">
                            <div class=" col-md-12">
                                <div class="row">                                
                                <div class="col-md-4">
                                    <img src="<?php echo base_url('public/theme/images/productos/sistema_bombeo/p/grundfos-logo.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>
                                    <br><a href="pdf/sistema_bombeo/CatalagoGrundfos.pdf" target="_blank">Catálogo</a>
                                </div>  
                               <div class="col-md-4">
                                    <img src="<?php echo base_url('public/theme/images/productos/sistema_bombeo/p/milton-roy-logo.jpg') ?>" alt="" style="width: 100%;max-width: 162px;"/>
                                    <br> <a href="pdf/sistema_bombeo/CatalagoMiltonRoy.pdf" target="_blank">Catálogo</a>
                                </div>  
                                </div>
                            </div>
                           </div>
                     </div>                      
                </div>
                <div class="row">
                    <div class="col-md-12" style="text-align:justify;">
                        <h4 class="mb-10">Cotizaciones o información de fichas técnicas</h4>
                         <fieldset></fieldset>
                        <form name="frm" id="frm" action="" method="post">
                            <input type="hidden" name="tipo" id="tipo" value="Bombas Dosificadoras" />
                            <div class="row pb-20">
                            <div class="col-md-3"> 
                                Nombre*:<br/>
                                <input type="text" name="nombre" id="nombre" placeholder="Nombre" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3"> 
                                Empresa*:<br>
                                <input type="text" name="empresa" id="empresa"  placeholder="Empresa" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-3">
                                Teléfono*:<br>
                                <input type="text" name="telefono" id="telefono"  placeholder="Teléfono" class="input-md round form-control" onkeypress="return isNumberKey(event)" maxlength="10"/> 
                            </div>
                            <div class="col-md-3">
                                E-mail*:<br>
                                <input type="text" name="correo" id="correo"  placeholder="E-mail" class="input-md round form-control"/>
                            </div>
                            </div>
                            <div class="row pb-20">
                            <div class="col-md-3">
                                Ciudad*:<br>
                                <input type="text" name="ciudad" id="ciudad"  placeholder="Ciudad a la que pertences" class="input-md round form-control"/>
                            </div>
                            <div class="col-md-9">
                                Mensaje*:<br>
                                <textarea name="mensaje" id="mensaje" style="width: 100%;height:60px;" placeholder="Mensaje." class="input-md round form-control"></textarea>
                            </div>
                            </div>
                            <div class="row pb-20">
                             <div class="col-md-12 align-right">
                                   <a href="javascript:EnviaProdContacto()" style="padding:10px 25px;font-size: 15px;color:#fff;background:#000;text-decoration: none;">ENVIAR</a>
                             </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>                           


<?php } ?>        
    </div>
</section>