<?php
//$prod_id = $this->input->get('id', TRUE);
$prod_id = $this->uri->segment(2);
$i = 0;
?>

<section class="page-section pt-20 pb-40">
                <div class="container relative">               
                                  <!-- Intro Text -->
                    <div class="row">
                        <div class="col-md-12 mb-10">
                            <div class="section-text align-left">
                              <h3 class="tituloproductos1 mt-0 mb-0" style="">Reparación de tubería</h3>
                              <div class="linea"></div>
                            </div>
                        </div>
                        <div class="col-md-12 mb-30">
                            <div class="section-text align-left">
                                Tienda > <a href="<?php echo base_url('tienda') ?>" style="color: #2d77b3;">Nuestros productos</a> > <a href="<?php echo base_url('tienda_reparacion_de_tuberia') ?>" style="color: #2d77b3;">Reparación de tubería</a> 
                                > <?php if($prod_id == 1){echo "Abrazaderas de reparación"; } ?>
                                <?php if($prod_id == 2){echo "Abrazaderas campana bell joint"; } ?>
                                <?php if($prod_id == 3){echo "Juntas universales"; } ?>
                                <?php if($prod_id == 4){echo "Coples dayton"; } ?>
                            </div>
                        </div>
                    </div>
                                 
                    <!-- End Intro Text -->
                    <div class="clearfix"></div>
<?php if($prod_id == 1){ 
                        $total_ananido=0;
                        if(isset($_SESSION['carro'])){
                            $cart_session =  $_SESSION['carro'];
                            if($cart_session){
                                if(!empty($cart_session)): foreach($cart_session as $cs=>$carro_shop):
                                    if($cs == "3121-3BX16"){$total_ananido = $carro_shop['cantidad']; }
                                    endforeach; else:
                                        
                                        endif;
                                    
                                    }
                                    
                                    }
                        ?>          
        <div class="row">            
            <div class="col-md-12" style="padding: 30px;">
                <div class="row">
                   <div class="col-md-5 text-center">
                       <div style="height: 297px;width: 100%;background: #fff;margin-bottom: 2em;">
                        <img id="img_simple" src="<?php echo base_url('public/theme/') ?>images/paneles/panel_simple.png" alt="" style="margin:auto;"/>
                        <img id="img_doble" src="<?php echo base_url('public/theme/') ?>images/paneles/panel_doble.png" alt="" style="display:none;margin:auto;"/>
                        <img id="img_triple" src="<?php echo base_url('public/theme/') ?>images/paneles/panel_triple.png" alt="" style="display:none;margin:auto;"/>
                        <img id="img_simple_ai" src="<?php echo base_url('public/theme/') ?>images/paneles/panel_simple_ai.png" alt="" style="display:none;margin:auto;"/>
                        <img id="img_doble_ai" src="<?php echo base_url('public/theme/') ?>images/paneles/panel_doble_ai.png" alt="" style="display:none;margin:auto;"/>
                        <img id="img_triple_ai" src="<?php echo base_url('public/theme/') ?>images/paneles/panel_triple_ai.png" alt="" style="display:none;margin:auto;"/>
                     </div>
<!--                       <img src="<?php // echo base_url('public/theme/images/reparacion_tuberia/abrazadera_reparacion.jpg') ?>" alt=""  style="width: 100%;margin-bottom: 2em;"/>-->
                   </div>
                    <div class="col-md-7" style="text-align:justify;">
                        <h6 class="titulo_producto">Abrazaderas de Reparación</h6>
                        <fieldset></fieldset>                           
                        <div  class="section-text"  style="color:#000;">
                            La abrazadera es sencilla, rápida de instalar y no requiere herramientas especiales. Una vez ajustadas en la posición correcta, la abrazadera envuelve a la tubería en la zona dañada. El efecto logrado es el comprimir el empaque contra la pared  de la tubería, abarcando toda la circunferencia y formando un sello perfecto.
                        </div>  
                         <h6 class="titulo_producto" style="font-size: 16px;">Ventajas</h6>
                        <div>
                            <ul id="ven" style="">
                                <li><span>Rápido de instalar y de alta durabilidad.</span></li>
                                <li><span>No necesita conexiones extras.</span></li>
                                <li><span>No necesita cortar el suministro .</span></li>
                                <li><span>No se necesitan herramientas especiales para su instalación.</span></li>
                            </ul>
                        </div>
                         <div class="clearfix"></div>
                       <!-- <div class="section-text pt-0 pb-20">
                            <h1 class="mt-0 mb-10">$ 0.000 MXN.</h1>
                        </div>-->
                        <div class="section-text pt-20 pb-20">
                            <!--<form id="frm-prod" name="frm-prod" action="" method="post">-->
                            <?php echo form_open_multipart(base_url('welcome/crea_sesion_producto'), array('id' => 'frm-prod')) ?>
                            <div class="row">
                                <?php 
//                                    echo '<pre>';
//                                    print_r( $this->data['diam']);
//                                    echo '</pre>';
                                ?>
                                <div id="lista_nominal" class="col-md-6 text-left">
                                    Diámetro Nominal 
                                    <select id="diam" name="diam" onchange="MuestraProdconDiam(this.value)">
                                       <option value="">Selecciona diámetro nominal</option>
                                              <?php if(!empty($this->data['diam'])): foreach($this->data['diam'] as $diametro):?>
                                       <option value="<?php echo $diametro['DiametroNominal'];?>"><?php echo $diametro['DiametroNominal'];?></option>
                                        <?php 
                                          endforeach; else:
                                              
                                              endif;     
                                        ?>
                                   </select>
                                        <!--<input type="text" name="diam" id="diam">-->
                                </div>
                                <div class="col-md-6">
                                    
                                </div>
                            </div>
                             <div class="row">
                                <div id="paneles"  class="col-md-6">
                                    Número de Paneles<br>
                                     <label for="sen" style="display: inline-block;">
                                    <input type="checkbox" name="panel[]" id="sen" onchange="MuestraProdPanel(this.value)" value="1">
                                   Panel Sencillo</label>
                                     <label for="dob" style="display: inline-block;">
                                 <input type="checkbox" name="panel[]" id="dob" onchange="MuestraProdPanel(this.value)" value="2"> 
                                Panel Doble</label>
                                    <label for="tri" style="display: inline-block;">
                                 <input type="checkbox" name="panel[]" id="tri" onchange="MuestraProdPanel(this.value)" value="3"> 
                                 Panel Triple</label>
                                </div>
                                <div class="col-md-6">
                                    
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    Material<br>
                                     <label for="hie" style="display: inline-block;">
                                 <input type="checkbox" name="material" id="hie" value="1"  onchange="MuestraProdMaterial(this.value)">  
                                Hierro ductil</label>
                                     <label for="ace" style="display: inline-block;">
                                 <input type="checkbox" name="material" id="ace" value="2"  onchange="MuestraProdMaterial(this.value)"> 
                                Acero Inoxidable</label>
                                </div>
                                <div class="col-md-6">
                                    
                                </div>
                            </div>
                             <div class="row">
                                <div id="lista_rango" class="col-md-6">
                                    Rango<br>
                                   <select id="rango" name="rango" onchange="MuestraProdRango(this.value)">
                                       <option value="">Selecciona rango</option>
                                           <?php if(!empty($this->data['rang'])): foreach($this->data['rang'] as $rango):?>
                                       <option value="<?php echo $rango['RangoDiametro'];?>"><?php echo $rango['RangoDiametro'];?></option>
                                        <?php 
                                          endforeach; else:
                                              
                                              endif;     
                                        ?>
                                   </select>
                                </div>
                                <div class="col-md-6">
                                    
                                </div>
                             </div><br>
<!--                                  <input type="hidden" name="skus" id="skus" value="3121-3BX16" />
                            <input type="hidden" name="nprod" id="nprod" value="Abrazadera Panel Simple 3121 Power Seal" />
                            <input type="hidden" name="prices" id="prices" value="150.00" />
                            <input type="hidden" name="tipo_moneda" id="tipo_moneda" value="MXN" />-->
                            
                            <input type="hidden" name="skus" id="skus" value="" />
                            <input type="hidden" name="nprod" id="nprod" value="" />
                            <input type="hidden" name="prices" id="prices" value="" />
                            <input type="hidden" name="tipo_moneda" id="tipo_moneda" value="" />
                            <input type="hidden" name="cantidad" id="cantidad" value="<?php if($total_ananido != 0){ echo $total_ananido+1;}else{ echo "1";}?>" />
                            <!--<input type="hidden" name="images" id="images" value="<?php ?>" />-->
                            <!--<button  type="submit"  class="button button_cart left" id="login-btn">Añadir al Carrito</button>-->
                            
                            <?php echo form_close(); ?>
                            <!--</form>-->
                        </div>
                     
                        
                    </div>
                </div>
              
                
            </div>
        </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div id="filtros_de_productos">
                                <?php 
                              
                                ?>
                            </div>
                        </div>
                    </div>
<?php }  if($prod_id == 2){
    
     $total_ananido=0;
     if(isset($_SESSION['carro'])){
         $cart_session =  $_SESSION['carro'];
         if($cart_session){
             if(!empty($cart_session)): foreach($cart_session as $cs=>$carro_shop):
                 if($cs == "3121-3BX16"){$total_ananido = $carro_shop['cantidad']; }
                 endforeach; else:
                     
                     endif;
                 
                 }
                 
                 }
?>  
                <div class="row">            
            <div class="col-md-12" style="padding: 30px;">
                <div class="row">
                   <div class="col-md-5">
                       <img src="<?php echo base_url('public/theme/images/reparacion_tuberia/abrazadera_campana_bell_joint.jpg') ?>" alt=""  style="width: 100%;margin-bottom: 2em;"/>
                   </div>
                    <div class="col-md-7" style="text-align:justify;">
                        <h6 class="titulo_producto">Abrazaderas campana Bell Joint</h6>
                        <fieldset></fieldset>
                        <div  class="section-text"  style="color:#000;">
                            Es un tipo de abrazadera que proporciona una solución definitiva a las fugas entre uniones y se le llama así porque sirve para reparar fugas en donde haya uniones en juntas tipo campana proporcionando una unión entre formas no uniformes.
                        </div>  
                         <h6 class="titulo_producto" style="font-size: 16px;">Ventajas</h6>
                        <div>
                            <ul id="ven" style="">
                                <li><span>Su empaque proporciona un desempeño del 100% bajo diferentes condiciones de presión y material.</span></li>
                                <li><span>Soportan rangos de temperaturas altas.</span></li>
                                <li><span>Construidas con material normado según la norma ASTM A325 y ASTM A563.</span></li>
                                <li><span>Pintura epoxica.</span></li>
                            </ul>
                        </div>
                         <div class="clearfix"></div>                         
                         <!-- <div class="section-text pt-0 pb-20"><h1 class="mt-0 mb-10">$ 0.000 MXN.</h1></div>-->
                         
                        <div class="section-text pt-20 pb-20">
                            <!--<form id="frm-prod" name="frm-prod" action="" method="post">-->
                            <?php echo form_open_multipart(base_url('welcome/crea_sesion_producto'), array('id' => 'frm-prod')) ?>
                            <div id="lista_nominal" class="col-md-6 text-left">
                                    Diámetro Nominal 
                                    <select id="diam" name="diam" onchange="MuestraProdconDiamJoint(this.value)">
                                       <option value="">Selecciona diámetro nominal</option>
                                              <?php if(!empty($this->data['diam_campana'])): foreach($this->data['diam_campana'] as $diametro):?>
                                       <option value="<?php echo $diametro['DiametroNominal'];?>"><?php echo $diametro['DiametroNominal'];?></option>
                                        <?php 
                                          endforeach; else:
                                              
                                              endif;     
                                        ?>
                                   </select>                                        
                                </div>                            
                            
                            <input type="hidden" name="skus" id="skus" value="" />
                            <input type="hidden" name="nprod" id="nprod" value="" />
                            <input type="hidden" name="prices" id="prices" value="" />
                            <input type="hidden" name="tipo_moneda" id="tipo_moneda" value="" />
                            <input type="hidden" name="cantidad" id="cantidad" value="<?php if($total_ananido != 0){ echo $total_ananido+1;}else{ echo "1";}?>" />
                            
                            <?php echo form_close(); ?>
                            <!--</form>-->
                        </div>
                         
                    </div>
                </div>
              
                
            </div>
        </div>
                     <div class="row">
                        <div class="col-md-12">
                            <div id="filtros_de_productos">
                                <?php 
                              
                                ?>
                            </div>
                        </div>
                    </div>
<?php } if($prod_id == 3){
    
      $total_ananido=0;
     if(isset($_SESSION['carro'])){
         $cart_session =  $_SESSION['carro'];
         if($cart_session){
             if(!empty($cart_session)): foreach($cart_session as $cs=>$carro_shop):
                 if($cs == "3121-3BX16"){$total_ananido = $carro_shop['cantidad']; }
                 endforeach; else:
                     
                     endif;
                 
                 }
                 
                 }
                 
    ?>  
                    
            <div class="row">            
            <div class="col-md-12" style="padding: 30px;">
                <div class="row">
                   <div class="col-md-5">
                       <div style="height: 297px;width: 100%;background: #fff;margin-bottom: 2em;">
                        <img src="<?php  echo base_url('public/theme/images/reparacion_tuberia/juntas_universales.jpg') ?>" alt=""  style="width: 100%;margin-bottom: 2em;"/>
                     </div>
                       
                   </div>
                    <div class="col-md-7" style="text-align:justify;">
                        <h6 class="titulo_producto">JUNTAS UNIVERSALES</h6>
                        <fieldset></fieldset>                           
                        <div  class="section-text"  style="color:#000;">
                            Se les conoce así porque pueden reparar una fuga en todo tipo de tubería, teniendo una conexión flexible y segura en reparaciones en donde las secciones de tubería dañadas son demasiado largas para utilizar una abrazadera de reparación.
                        </div>  
                         <h6 class="titulo_producto" style="font-size: 16px;">Ventajas</h6>
                        <div>
                            <ul id="ven" style="">
                                <li><span>Empaque con diseño cónico para garantizar un desempeño 100% bajo condiciones diferentes de presión.</span></li>
                                <li><span>Se adaptan a cualquier tipo de material</span></li>                                
                            </ul>
                        </div>
                         <div class="clearfix"></div>                       
                        <div class="section-text pt-20 pb-20">                            
                            <?php echo form_open_multipart(base_url('welcome/crea_sesion_producto'), array('id' => 'frm-prod')) ?>
                             <div class="row">
                                <div id="lista_nominal" class="col-md-6 text-left">
                                    Diametro<br>
                                    <?php  //var_dump($this->data['diam_universal']);?>
                                    <select id="diam" name="diam" onchange="MuestraRangoUni(this.value)">
                                       <option value="">Selecciona diámetro nominal</option>
                                              <?php if(!empty($this->data['diam_universal'])): foreach($this->data['diam_universal'] as $diametro):?>
                                       <option value="<?php echo $diametro['DiametroNominal'];?>"><?php echo $diametro['DiametroNominal'];?></option>
                                        <?php 
                                         endforeach; else:                                              
                                         endif;
                                        ?>
                                   </select>
                                </div>
                                <div class="col-md-6">
                                    
                                </div>
                            </div> 
                            
                            <div class="row">
                                <div id="lista_rango_uni" class="col-md-6 text-left">
                                    Rango<br>
                                    <select id="rango" noame="rango" onchange="MuestraTamano(this.value)">
                                       <option value="">Selecciona Rango</option>
                                              <?php //if(!empty($this->data['diam'])): foreach($this->data['diam'] as $diametro):?>
                                       <option value="<?php //echo $diametro['DiametroNominal'];?>"><?php //echo $diametro['DiametroNominal'];?></option>
                                        <?php 
                                         //endforeach; else:                                              
                                         //endif;
                                        ?>
                                   </select>
                                </div>
                                <div class="col-md-6">
                                    
                                </div>
                            </div>  
                            <div class="row">
                                <div id="lista_tamano" class="col-md-6">
                                    Tamaño<br>
                                   <select id="tamanio" name="tamanio" onchange="MuestraProdRango(this.value)">
                                       <option value="">Selecciona Tamaño</option>
                                           <?php //if(!empty($this->data['rang'])): foreach($this->data['rang'] as $rango):?>
                                       <option value="<?php //echo $rango['RangoDiametro'];?>"><?php //echo $rango['RangoDiametro'];?></option>
                                        <?php 
                                          //endforeach; else:                                              
                                          //    endif;     
                                        ?>
                                   </select>
                                </div>
                                <div class="col-md-6">
                                    
                                </div>
                             </div>
                            <div class="row">
                                <div class="col-md-6">
                                    Material<br>
                                     <label for="hie" style="display: inline-block;">
                                 <input type="checkbox" name="material" id="hie" value="1" onchange="MuestraProdMaterialUni(this.value)">  
                                Hierro ductil</label>
                                     <label for="ace" style="display: inline-block;">
                                 <input type="checkbox" name="material" id="ace" value="2" onchange="MuestraProdMaterialUni(this.value)"> 
                                Acero Inoxidable</label>
                                </div>
                                <div class="col-md-6">
                                    
                                </div>
                            </div>
                             <br>
                            <input type="hidden" name="skus" id="skus" value="" />
                            <input type="hidden" name="nprod" id="nprod" value="" />
                            <input type="hidden" name="prices" id="prices" value="" />
                            <input type="hidden" name="tipo_moneda" id="tipo_moneda" value="" />
                            <input type="hidden" name="cantidad" id="cantidad" value="<?php if($total_ananido != 0){ echo $total_ananido+1;}else{ echo "1";}?>" />                                                  
                            <?php echo form_close(); ?>
                        </div>
                     
                        
                    </div>
                </div>
            </div>
        </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div id="filtros_de_productos">
                                <?php 
                              
                                ?>
                            </div>
                        </div>
                    </div>
                    
<?php } if($prod_id == 4){
    
    $total_ananido=0;
     if(isset($_SESSION['carro'])){
         $cart_session =  $_SESSION['carro'];
         if($cart_session){
             if(!empty($cart_session)): foreach($cart_session as $cs=>$carro_shop):
                 if($cs == "3121-3BX16"){$total_ananido = $carro_shop['cantidad']; }
                 endforeach; else:
                     
                     endif;
                 
                 }
                 
                 }
                 
    ?>  
                    
            <div class="row">            
            <div class="col-md-12" style="padding: 30px;">
                <div class="row">
                   <div class="col-md-5">
                       <div style="height: 297px;width: 100%;background: #fff;margin-bottom: 2em;">
                        <img src="<?php  echo base_url('public/theme/images/reparacion_tuberia/coples_dayton.jpg') ?>" alt=""  style="width: 100%;margin-bottom: 2em;"/>
                     </div>
                       
                   </div>
                    <div class="col-md-7" style="text-align:justify;">
                        <h6 class="titulo_producto">COPLES DAYTON</h6>
                        <fieldset></fieldset>                           
                        <div  class="section-text"  style="color:#000;">
                            Este tipo de coples son usados en la unión de segmentos de tuberías. Permiten que los tubos que son unidos resistan tanto las fuerzas internas como externas, las vibraciones y la presión ejercida por los líquidos que fluyen a lo largo de la tubería. Basta aflojar la tuerca (sin llegar a extraerla) y el tubo puede introducirse.
                        </div>  
                         <h6 class="titulo_producto" style="font-size: 16px;">Ventajas</h6>
                        <div>
                            <ul id="ven" style="">
                                <li><span>Menor tiempo de instalación.</span></li>
                                <li><span>Excelentes para distribución de agua.</span></li>
                                <li><span>Empaque resistente a la corrosión.</span></li>
                            </ul>
                        </div>
                         <div class="clearfix"></div>                       
                        <div class="section-text pt-20 pb-20">                            
                            <?php echo form_open_multipart(base_url('welcome/crea_sesion_producto'), array('id' => 'frm-prod')) ?>
                             <div class="row">
                                <div id="lista_nominal" class="col-md-6 text-left">
                                    Rango<br>
                                    <?php   //var_dump($this->data['diam_cople']); ?>
                                    <select id="diam" name="diam" onchange="MuestraProdCople(this.value)">
                                       <option value="">Selecciona el Rango</option>
                                              <?php
                                              
                                              if(!empty($this->data['diam_cople'])): foreach($this->data['diam_cople'] as $diametro):?>
                                       <option value="<?php echo $diametro['RangoDiametro'];?>"><?php echo $diametro['RangoDiametro'];?></option>
                                        <?php 
                                         endforeach; else:                                              
                                         endif;
                                        ?>
                                   </select>
                                </div>
                                <div class="col-md-6">
                                    
                                </div>
                            </div> 
                             <br>
                            <input type="hidden" name="skus" id="skus" value="" />
                            <input type="hidden" name="nprod" id="nprod" value="" />
                            <input type="hidden" name="prices" id="prices" value="" />
                            <input type="hidden" name="tipo_moneda" id="tipo_moneda" value="" />
                            <input type="hidden" name="cantidad" id="cantidad" value="<?php if($total_ananido != 0){ echo $total_ananido+1;}else{ echo "1";}?>" />                                                  
                            <?php echo form_close(); ?>
                        </div>
                     
                        
                    </div>
                </div>
            </div>
        </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div id="filtros_de_productos">
                                <?php 
                              
                                ?>
                            </div>
                        </div>
                    </div>
                    
<?php } ?>                    
                </div>
            </section>
            <!-- End Section -->