<style>
    /*--------------------
4.3. Portfolio
--------------------*/

figure {
	position: relative;
	overflow: hidden;
	margin: 0 1px 1px 0;
	/*background: #726FB9;*/
	text-align: center;
	cursor: pointer;	
}

figcaption {
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;	
}

.ot-portfolio-item figure figcaption > a {
	z-index: 1000;
	text-indent: 200%;
	white-space: nowrap;
	font-size: 0;
	opacity: 0;
}
.ot-portfolio-item figure figcaption, .ot-portfolio-item figure figcaption > a {
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
}

figure.effect-bubba {
	background: #000;
	margin-bottom: 20px;
}

figure.effect-bubba img {
	/* opacity: 0.95; */
	-webkit-transition: opacity 0.35s;
	transition: opacity 0.35s;
}

figure.effect-bubba:hover img {
	opacity: 0.4;
}

figure.effect-bubba figcaption::before,
figure.effect-bubba figcaption::after {
	position: absolute;
	top: 30px;
	right: 30px;
	bottom: 30px;
	left: 30px;
	content: '';
	opacity: 0;
	-webkit-transition: opacity 0.35s, -webkit-transform 0.35s;
	transition: opacity 0.35s, transform 0.35s;
}

figure.effect-bubba figcaption::before {
	border-top: 1px solid #fff;
	border-bottom: 1px solid #fff;
	-webkit-transform: scale(0,1);
	transform: scale(0,1);
}

figure.effect-bubba figcaption::after {
	border-right: 1px solid #fff;
	border-left: 1px solid #fff;
	-webkit-transform: scale(1,0);
	transform: scale(1,0);
}

figure.effect-bubba h2 {
	opacity: 0;
	color: #fff;
	font-size: 15px;
	font-weight: 700;
	text-transform: uppercase;
	padding-top:22%;
	-webkit-transition: -webkit-transform 0.35s;
	transition: transform 0.35s;
	-webkit-transform: translate3d(0,-20px,0);
	transform: translate3d(0,-20px,0);
}

figure.effect-bubba p {
	color: #fff;
	font-size: 13px;
	font-weight: 500;
	padding: 20px 2.5em;
	opacity: 0;
	-webkit-transition: opacity 0.35s, -webkit-transform 0.35s;
	transition: opacity 0.35s, transform 0.35s;
	-webkit-transform: translate3d(0,20px,0);
	transform: translate3d(0,20px,0);
}

figure.effect-bubba:hover figcaption::before,
figure.effect-bubba:hover figcaption::after {
	opacity: 1;
	-webkit-transform: scale(1);
	transform: scale(1);
}

figure.effect-bubba:hover h2,
figure.effect-bubba:hover p {
	opacity: 1;
	-webkit-transform: translate3d(0,0,0);
	transform: translate3d(0,0,0);
}

#myList .row {
    display:none;
}
#loadMore {
  /*  color:green;*/
    cursor:pointer;
    display:inline-block;
}
#loadMore:hover {
    color:black;
}
#showLess {
   /* color:red;*/
    cursor:pointer;
    display:inline-block;
}
#showLess:hover {
    color:black;
}

</style>

<section class="page-section pt-20 pb-40">
    <div class="container relative">
        <!-- Intro Text -->
        <div class="row">
            <div class="col-md-12 mb-40">
                <div class="section-text align-left">
                    <h3 class="tituloproductos1 mt-0 mb-0" style="">Proyectos</h3>
                    <div class="linea"></div>
                </div>
            </div>
        </div>
        <!-- End Intro Text -->
        <div class="clearfix"></div>
        <div class="row" style="background: #fff;padding: 30px 10px;">
            <div class="col-md-12">
                   <div class="row row-0-gutter">
				
				<!-- start portfolio item -->
				<div class="col-md-6 col-0-gutter">
					<div class="ot-portfolio-item">
						<figure class="effect-bubba">							
                                                                                 <img src="<?php echo base_url('public/theme/images/proyectos/altata.jpg') ?>" alt="" class="img-responsive"/>
							<figcaption>
								<h2>Proyecto Nuevo Altata</h2>
								<p>suministramos la red de alcantarillado de la vialidad principal en la ciudad de Navolato, Sinaloa.</p>
								<a href="#" data-toggle="modal" data-target="#Modal-01">View more</a>
							</figcaption>
						</figure>
					</div>
				</div>
				<!-- end portfolio item -->
			
				<!-- start portfolio item -->
				<div class="col-md-6 col-0-gutter">
					<div class="ot-portfolio-item">
						<figure class="effect-bubba">							
                                                        <img src="<?php echo base_url('public/theme/images/proyectos/poligono_empresarial_san_miguel.jpg') ?>" alt="" class="img-responsive"/>
							<figcaption>
                                                            <h2>Parque industrial Polígono Empresarial
                                                                <!--<span style="display:block;font-size:12px;text-transform:none;"></span>-->
                                                                
                                                                </h2>
								<p>En San Miguel de Allende, Gto., suministramos la red contra incendio externa del perímetro total del parque.</p>
								<a href="#" data-toggle="modal" data-target="#Modal-02">View more</a>
							</figcaption>
						</figure>
					</div>
				</div>
				<!-- end portfolio item -->
                                </div>
                   
                     <div class="row row-0-gutter">
				
				<!-- start portfolio item -->
				<div class="col-md-6 col-0-gutter">
					<div class="ot-portfolio-item">
						<figure class="effect-bubba">							
                                                                                 <img src="<?php echo base_url('public/theme/images/proyectos/ElRealito.jpg') ?>" alt="" class="img-responsive"/>
							<figcaption>
								<h2>Proyecto el Realito</h2>
								<p>suministramos más de 8000 mts de tuberías de 42” en hierro dúctil y 4000 mts de 36” para conectar el acueducto El Novillo con el Reservorio en la Cd de Hermosillo, Son.</p>
								<a href="#" data-toggle="modal" data-target="#Modal-01">View more</a>
							</figcaption>
						</figure>
					</div>
				</div>
				<!-- end portfolio item -->
			
				<!-- start portfolio item -->
                                <div class="col-md-6  col-0-gutter">
                                    <div class="ot-portfolio-item">
                                        <figure class="effect-bubba">
                                            <img src="<?php echo base_url('public/theme/images/proyectos/LosBagotes.jpg') ?>" alt="" class="img-responsive"/>
                                            <figcaption>
                                                <h2>Proyecto Los Bagotes</h2>
                                                <p>suministramos aproximadamente 3000 mts de tubería RD32 de 36” en la Cd. de Hermosillo, Son.</p>
                                                <a href="#" data-toggle="modal" data-target="#Modal-02">View more</a>
                                            </figcaption>
                                        </figure>
                                    </div>
                                </div>
				<!-- end portfolio item -->
                                </div>
                   
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                                <div class="section-text">
                              Suministramos los Equipos Contra Incendio subterráneos para las empresas de Honda, Mazda y General Motors en la  ciudad de Guanajuato, también para Nissan 3 en Aguascalientes y Audi en puebla.
                                 </div>
                                  
                                <div  class="pt-20 text-center">
                                    <div class="col-md-2">
                                        <img src="<?php echo base_url('public/theme/images/proyectos/audi-logo.jpg') ?>" alt="" style="width:100%;max-width: 50%;"/>
                                    </div>
                                    <div class="col-md-2">
                                        <img src="<?php echo base_url('public/theme/images/proyectos/General-Motors-logo.jpg') ?>" alt="" style="width:100%;max-width: 50%;"/>
                                    </div>
                                    <div class="col-md-3">
                                        <img src="<?php echo base_url('public/theme/images/proyectos/Nissan-logo.jpg') ?>" alt="" style="width:100%;max-width: 50%;"/>
                                    </div>
                                    
                                    <div class="col-md-2">
                                        <img src="<?php echo base_url('public/theme/images/proyectos/Mazda_logo.jpg') ?>" alt="" style="width:100%;max-width: 50%;"/>
                                    </div>
                                    <div class="col-md-2">
                                        <img src="<?php echo base_url('public/theme/images/proyectos/honda-logos-emblems.jpg') ?>" alt="" style="width:100%;max-width: 50%;"/>
                                        
                                    </div>
                                </div>   
                    </div>
                </div>
                </div>
            </div>
      </div>
  </section>