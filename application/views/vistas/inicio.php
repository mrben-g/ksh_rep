
<div class="pt-xs-50 pb-xs-50" style="">
    
    <!--
<div class="camera_wrap camera_black_skin" id="camera_wrap_2">
            <div data-src="<?php //echo base_url('public/theme/images/slider/1.png') ?>">
              <div class="camera_caption fadeFromBottom">
                    <em id="tituloindex"> Bienvenidos </em><br>
                    Somos KSH, la tienda especialista en soluciones para la conducción y control de agua y drenaje. Más de 15 años en el mercado y la confianza de las mejores marcas nos respaldan.<br>
                    Conoce más de <em><a href="quienessomos.php?id=1" target="_blank" style="color:#fff;">¿Quiénes somos?</a></em>

                </div>
            </div>
          <div data-src="<?php //echo base_url('public/theme/images/slider/2.png') ?>">
            
            </div>
          <div data-src="<?php //echo base_url('public/theme/images/slider/3.jpg') ?>">
            
            </div>
         <div data-src="<?php //echo base_url('public/theme/images/slider/4.jpg') ?>">
            
            </div>
        </div><!-- #camera_wrap_2 -->
        
 <!-- Fullwidth Slider -->
<div class="home-section fullwidth-slider" id="home">
    <!-- Slide Item -->
    <section class="home-section bg-scroll fixed-height-small " data-background="<?php echo base_url('public/theme/images/slider/1.png') ?>">
     <!--   <div class="js-height-parent container">
            <!-- Hero Content --
            <div class="home-content">
                <div class="home-text">
                    <div class="hs-line-8 no-transp font-alt mb-40 mb-xs-10">
                    </div>
                    <div class="hs-line-14 font-alt mb-50 mb-xs-10">
                    </div>
                    <div class="local-scroll">
                    </div>
                </div>
            </div>
            <!-- End Hero Content --
        </div>-->
    </section>
    <!-- End Slide Item -->
    <!-- Slide Item -->
    <section class="home-section bg-scroll fixed-height-small " data-background="<?php echo base_url('public/theme/images/slider/2.png') ?>">
    <!--<div class="js-height-parent container">
        <!-- Hero Content --
        <div class="home-content">
            <div class="home-text">
                <h1 class="hs-line-8 no-transp font-alt mb-50 mb-xs-10">
                </h1>
                <h2 class="hs-line-14 font-alt mb-50 mb-xs-10">
                </h2>
                <div class="local-scroll">
                </div>
            </div>
        </div>
        <!-- End Hero Content --
    </div>-->
</section>
<!-- End Slide Item -->              
<!-- Slide Item -->
<section class="home-section bg-scroll fixed-height-small " data-background="<?php echo base_url('public/theme/images/slider/3.jpg') ?>">
    <!--<div class="js-height-parent container">
        <!-- Hero Content --
        <div class="home-content">
            <div class="home-text">
                <h2 class="hs-line-8 no-transp font-alt mb-50 mb-xs-10">
                </h2>                       
                <h1 class="hs-line-14 font-alt mb-50 mb-xs-10">
                </h1>
                <div>
                </div>
            </div>
        </div>
        <!-- End Hero Content --
    </div>-->
</section>
<!-- End Slide Item -->
</div>
<!-- End Fullwidth Slider -->        
 </div> 

  <!-- Section -->
            <section class="page-section pt-20 pb-40">
                <div class="container relative">               
                                  <!-- Intro Text -->
                    <div class="row">
                        <div class="col-md-12 mb-20">
                            <div class="section-text align-left">
                              <h3 class="tituloproductos1 mt-0 mb-0" style="">Productos</h3>
                              <div class="linea"></div>
                            </div>
                        </div>
                    </div>
                                 
                    <!-- End Intro Text -->
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-9 mb-sm-40">
                      
                            <!-- Logo Grid -->
                    <div class="row multi-columns-row mb-20">
                        
                        <!-- Logo Item -->
                        <div class="col-sm-6 col-md-4 col-lg-4 mb-sm-20 banner_p">
                            <h3 class="alt-features-title font-alt mb-0 mt-0 pt-10 pb-10" style="background: #dddddd;padding: 10px;">REPARACIÓN DE TUBERÍA</h3>
                            <div class="blog-post-prev-small mb-20 clearfix" style="background:#fff;">
                                        <a href=""><img src="<?php echo base_url('public/theme/images/productos_home_menu/rept.png') ?>" alt="" class="widget-posts-img" /></a>
                                        <div class="widget-posts-descr" style="font-size:10px;">
                                                <label><a href="<?php echo base_url('producto_reparacion_de_tuberia/1') ?>">ABRAZADERAS DE REPARACIÓN</a></label>
                                                <label><a href="<?php echo base_url('producto_reparacion_de_tuberia/2') ?>">ABRAZADERAS CAMPANA BELL JOINT</a></label>
                                                <label><a href="<?php echo base_url('producto_reparacion_de_tuberia/3') ?>">JUNTAS UNIVERSALES</a></label>
                                                <label><a href="<?php echo base_url('producto_reparacion_de_tuberia/4') ?>">COPLES DAYTON</a></label>
                                        </div>
                                    </div>
              
                        </div>
                        <!-- End Logo Item -->
                        
                        <!-- Logo Item -->
                        <div class="col-sm-6 col-md-4 col-lg-4 mb-sm-20 banner_p">
                              <h3 class="alt-features-title font-alt mb-0 mt-0 pt-10 pb-10" style="background: #dddddd;padding: 10px;">TUBERÍA Y CONEXIONES</h3>
                            <div class="blog-post-prev-small mb-20 clearfix" style="background:#fff;">
                                <a href=""><img src="<?php echo base_url('public/theme/images/productos_home_menu/conex.png') ?>" alt="" class="widget-posts-img" style="width:88px;" /></a>
                                        <div class="widget-posts-descr" style="font-size:10px;">
                                            <label><a href="<?php echo base_url('producto_tuberia_conexiones/1') ?>">FIERRO VACIADO (FOFO)</a></label>
                                            <label><a href="<?php echo base_url('producto_tuberia_conexiones/2') ?>">HIERRO DÚCTIL</a></label>
                                            <label><a href="<?php echo base_url('producto_tuberia_conexiones/3') ?>">PVC C-900</a></label>
                                            <label><a href="<?php echo base_url('producto_tuberia_conexiones/4') ?>">ALCANTARILLADO S. 20 y 25</a></label>
                                            <label> <a href="<?php echo base_url('producto_tuberia_conexiones/5') ?>">PEAD GRANDES DIAMETROS</a></label>
                                            <label><a href="<?php echo base_url('producto_tuberia_conexiones/6') ?>">ACERO</a></label>
                                            <label> <a href="<?php echo base_url('producto_tuberia_conexiones/7') ?>">COBRE</a></label>
                                            <label><a href="<?php echo base_url('producto_tuberia_conexiones/8') ?>">PVC SI y SM</a></label>
                                            <label><a href="<?php echo base_url('producto_tuberia_conexiones/9') ?>">PVC C-80</a></label>
                                            <label><a href="<?php echo base_url('producto_tuberia_conexiones/10') ?>">PVC C-40</a></label>
                                            <label><a href="<?php echo base_url('producto_tuberia_conexiones/11') ?>">POLIPROPILENO</a></label>
                                            <label><a href="<?php echo base_url('producto_tuberia_conexiones/12') ?>">DWV</a></label>
                                        </div>
                                    </div>
                            
                        
                        </div>
                        <!-- End Logo Item -->
                        
                        <!-- Logo Item -->
                        <div class="col-sm-6 col-md-4 col-lg-4 mb-sm-20 banner_p">
                            <h3 class="alt-features-title font-alt mb-0 mt-0 pt-10 pb-10" style="background: #dddddd;padding: 10px;">CONTRA INCENDIO</h3>
                            <div class="blog-post-prev-small mb-20 clearfix" style="background:#fff;">
                                <a href=""><img src="<?php echo base_url('public/theme/images/productos_home_menu/incendio.png') ?>" alt="" class="widget-posts-img" style="width:88px;" /></a>
                                        <div class="widget-posts-descr" style="font-size:10px;">
                                                <label><a href="<?php echo base_url('producto_contra_incendio/1') ?>">HIDRANTES BARRIL SECO</a></label>	
                                                <label><a href="<?php echo base_url('producto_contra_incendio/2') ?>">HIDRANTES BARRIL HÚMEDO</a></label>
                                                <label><a href="<?php echo base_url('producto_contra_incendio/3') ?>">EXTENSIONES DE HIDRANTE</a></label>
                                                <label><a href="<?php echo base_url('producto_contra_incendio/4') ?>">REPUESTOS Y KITS DE REPARACIÓN</a></label>
                                                <label><a href="<?php echo base_url('producto_contra_incendio/5') ?>">POSTE INDICADOR VERTICAL</a></label>
                                                <label><a href="<?php echo base_url('producto_contra_incendio/6') ?>">POSTE INDICADOR DE PARED</a></label>
                                                <label><a href="<?php echo base_url('producto_contra_incendio/7') ?>">EQUIPO CONTRA INCENDIO INTEGRAL</a></label>
                                                <label><a href="<?php echo base_url('producto_contra_incendio/8') ?>">VÁLVULAS UL/FM</a></label>
                                                <label><a href="<?php echo base_url('producto_contra_incendio/9') ?>">LLAVES Y HERRAMIENTAS</a></label>
                                        </div>
                                    </div>
                        
                        </div>
                        <!-- End Logo Item -->
                   
                        <!-- Logo Item -->
                        <div class="col-sm-6 col-md-4 col-lg-4 mb-sm-20 banner_p">
                             <h3 class="alt-features-title font-alt mb-0 mt-0 pt-10 pb-10" style="background: #dddddd;padding: 10px;">CONDUCCIÓN Y DERIVACIÓN</h3>
                            <div class="blog-post-prev-small mb-20 clearfix" style="background:#fff;">
                                <a href=""><img src="<?php echo base_url('public/theme/images/productos_home_menu/conduccionderivacion.png') ?>" alt="" class="widget-posts-img" style="width:88px;" /></a>
                                        <div class="widget-posts-descr" style="font-size:10px;">
                                                        <label><a href="<?php echo base_url('producto_conduccion_derivacion/1') ?>">TEE PARTIDA</a></label>
                                                        <label><a href="<?php echo base_url('producto_conduccion_derivacion/2') ?>">COLLARINES (ABRAZADERAS)</a></label>
                                                        <label><a href="<?php echo base_url('producto_conduccion_derivacion/3') ?>">JUNTAS UNIVERSALES</a></label>
                                                        <label><a href="<?php echo base_url('producto_conduccion_derivacion/4') ?>">JUNTAS GIBAULT</a></label>
                                                        <label> <a href="<?php echo base_url('producto_conduccion_derivacion/5') ?>">ADAPTADORES BRIDADOS</a></label>
                                                        <label><a href="<?php echo base_url('producto_conduccion_derivacion/6') ?>">BRIDAS RÁPIDAS</a></label>
                                                        <label> <a href="<?php echo base_url('producto_conduccion_derivacion/7') ?>">TALADRO VEGA</a></label>
                                                        <label><a href="<?php echo base_url('producto_conduccion_derivacion/8') ?>">TOMAS DOMICILIARIAS</a></label>
                                                        <label><a href="<?php echo base_url('producto_conduccion_derivacion/9') ?>">HERRAMIENTAS</a></label>        
                                        </div>
                                    </div>

                        </div>
                        <!-- End Logo Item -->
                        
                        <!-- Logo Item -->
                        <div class="col-sm-6 col-md-4 col-lg-4 mb-sm-20 banner_p">
                                <h3 class="alt-features-title font-alt mb-0 mt-0 pt-10 pb-10" style="background: #dddddd;padding: 10px;">ALCANTARILLADO</h3>
                            <div class="blog-post-prev-small mb-20 clearfix" style="background:#fff;">
                                <a href=""><img src="<?php echo base_url('public/theme/images/productos_home_menu/alcantarillado.png') ?>" alt="" class="widget-posts-img" style="width:88px;" /></a>
                                        <div class="widget-posts-descr" style="font-size:10px;">
                                                    <label><a href="<?php echo base_url('producto_alcantarillado/1') ?>">BROCALES </a></label>
                                                    <label><a href="<?php echo base_url('producto_alcantarillado/2') ?>">ESCALONES</a></label>
                                                    <label><a href="<?php echo base_url('producto_alcantarillado/3') ?>">VÁLVULA ANTI-RETORNO</a></label>
                                                    <label><a href="<?php echo base_url('producto_alcantarillado/4') ?>">MANGAS DE EMPOTRAMIENTO</a></label>  
                                        </div>
                                    </div>
        
                        </div>
                        <!-- End Logo Item -->
                        
                        <!-- Logo Item -->
                        <div class="col-sm-6 col-md-4 col-lg-4 mb-sm-20 banner_p">
                              <h3 class="alt-features-title font-alt mb-0 mt-0 pt-10 pb-10" style="background: #dddddd;padding: 10px;">SISTEMAS DE BOMBEO</h3>
                            <div class="blog-post-prev-small mb-20 clearfix" style="background:#fff;">
                                <a href=""><img src="<?php echo base_url('public/theme/images/productos_home_menu/sistemabombeo.png') ?>" alt="" class="widget-posts-img" style="width:88px;" /></a>
                                        <div class="widget-posts-descr" style="font-size:10px;">
                                                            <label><a href="<?php echo base_url('producto_sistema_bombeo/1') ?>">BOMBAS VERTICALES</a></label>
                                                            <label><a href="<?php echo base_url('producto_sistema_bombeo/2') ?>">BOMBAS HORIZONTALES BIPARTIDAS</a></label>
                                                            <label> <a href="<?php echo base_url('producto_sistema_bombeo/3') ?>">BOMBAS SUMERGIBLES DE POZO PROFUNDO</a></label>
                                                            <label><a href="<?php echo base_url('producto_sistema_bombeo/4') ?>">BOMBAS SUMERGIBLES PARA DE LODOS</a></label>
                                                            <label> <a href="<?php echo base_url('producto_sistema_bombeo/5') ?>">BOMBAS TRAGASOLIDOS</a></label>
                                                            <label><a href="<?php echo base_url('producto_sistema_bombeo/6') ?>">BOMBAS ANSI</a></label>
                                                            <label><a href="<?php echo base_url('producto_sistema_bombeo/7') ?>">EQUIPOS PRE-ENSAMBLADOS</a></label>
                                                            <label><a href="<?php echo base_url('producto_sistema_bombeo/8') ?>">DOSIFICADORAS</a></label>
                                        </div>
                                    </div>
   
                        </div>
                        <!-- End Logo Item -->
                       
                                <!-- Logo Item -->
                        <div class="col-sm-6 col-md-4 col-lg-4 mb-sm-20 banner_p">
                               <h3 class="alt-features-title font-alt mb-0 mt-0 pt-10 pb-10" style="background: #dddddd;padding: 10px;">VÁLVULAS Y CAJAS DE VÁLVULAS </h3>
                            <div class="blog-post-prev-small mb-20 clearfix" style="background:#fff;">
                                <a href=""><img src="<?php echo base_url('public/theme/images/productos_home_menu/cajas.png') ?>" alt="" class="widget-posts-img" style="width:88px;" /></a>
                                        <div class="widget-posts-descr" style="font-size:10px;">
                                                                <label><a href="<?php echo base_url('producto_valvulas/1') ?>">VÁLVULAS COMPUERTA</a></label>
                                                                <label><a href="<?php echo base_url('producto_valvulas/2') ?>">VÁLVULAS MARIPOSA</a></label>
                                                                <label><a href="<?php echo base_url('producto_valvulas/3') ?>">VÁLVULAS CUCHILLA</a></label>
                                                                <label><a href="<?php echo base_url('producto_valvulas/4') ?>">VÁLVULAS DE EXPULSIÓN Y/O ADMISIÓN DE AIRE</a></label>
                                                                <label> <a href="<?php echo base_url('producto_valvulas/5') ?>">VÁLVULAS DE CONTROL</a></label>
                                                                <label><a href="<?php echo base_url('producto_valvulas/6') ?>">VÁLVULAS CHECK</a></label>
                                                                <label> <a href="<?php echo base_url('producto_valvulas/7') ?>">VÁLVULAS PLUG</a></label>
                                                                <label><a href="<?php echo base_url('producto_valvulas/8') ?>">MARCO CON TAPA</a></label>
                                                                <label><a href="<?php echo base_url('producto_valvulas/9') ?>">REGISTRO TELESCOPIADO</a></label>
                                                                 <!--<label><a href="<?php echo base_url('producto_valvulas/10') ?>">TAPA SUPERIOR Y CAMPANA</a></label>-->
                                                                 <label><a href="<?php echo base_url('producto_valvulas11') ?>">VÁLVULAS BACKFLOW</a></label>
                                        </div>
                                    </div>
               
                        </div>
                        <!-- End Logo Item -->
                             <!-- Logo Item -->
                        <div class="col-sm-6 col-md-4 col-lg-4 mb-sm-20 banner_p">
                               <h3 class="alt-features-title font-alt mb-0 mt-0 pt-10 pb-10" style="background: #dddddd;padding: 10px;">FILTROS </h3>
                            <div class="blog-post-prev-small mb-20 clearfix" style="background:#fff;">
                                <a href=""><img src="<?php echo base_url('public/theme/images/productos_home_menu/filtros.png') ?>" alt="" class="widget-posts-img" style="width:88px;" /></a>
                                        <div class="widget-posts-descr" style="font-size:10px;">
                                                <label><a href="<?php echo base_url('producto_filtros/1') ?>">FILTRO CANASTA SUPERIOR</a></label>
                                                <label><a href="<?php echo base_url('producto_filtros/2') ?>">FILTRO CANASTA</a></label>
                                                <label><a href="<?php echo base_url('producto_filtros/3') ?>">FILTRO DUPLEX</a></label>
                                                <label><a href="<?php echo base_url('producto_filtros/4') ?>">FILTRO YEE</a></label>
                                        </div>
                                    </div>
                  
                        </div>
                        <!-- End Logo Item -->
                             <!-- Logo Item -->
                        <div class="col-sm-6 col-md-4 col-lg-4 mb-sm-20 banner_p">
                               <h3 class="alt-features-title font-alt mb-0 mt-0 pt-10 pb-10" style="background: #dddddd;padding: 10px;">MEDIDORES </h3>
                            <div class="blog-post-prev-small mb-20 clearfix" style="background:#fff;">
                                <a href=""><img src="<?php echo base_url('public/theme/images/productos_home_menu/medidores.png') ?>" alt="" class="widget-posts-img" style="width:88px;" /></a>
                                        <div class="widget-posts-descr" style="font-size:10px;">
                                           <label><a href="<?php echo base_url('producto_medidores/1') ?>">RESIDENCIALES</a></label>
                                            <label><a href="<?php echo base_url('producto_medidores/2') ?>">COMERCIAL</a></label>
                                        </div>
                                    </div>
                      
                        </div>
                        <!-- End Logo Item -->
                    </div>
                    <!-- End Logo Grid -->
                            
                            
                        </div>
                        
                        <div class="col-md-3 col-lg-3 hidden-sm hidden-xs">
                            
                              
                            <!-- Gallery -->
                            <div class="work-full-media mt-0 white-shadow">
                                <ul class="clearlist work-full-slider owl-carousel">
                                    <li>
                                        <img src="<?php echo base_url('public/theme/images/slide_right/ANUNCIOANEAS.jpg') ?>" alt="" />
                                    </li>
                                    <li>
                                        <img src="<?php echo base_url('public/theme/images/slide_right/banner_right.png') ?>" alt="" />
                                    </li>
                              
                                </ul>
                            </div>
                            <!-- End Gallery -->
                            
                            
                        </div>
                    </div>
                </div>
            </section>
            <!-- End Section -->
            <section class="page-section pt-0 pb-40">
                <div class="container relative">               
                                  <!-- Intro Text -->
                    <div class="row">
                        <div class="col-md-12 mb-20">
                            <div class="section-text align-left">
                              <h3 class="tituloproductos1 mt-0 mb-0" style="">Diagramas y Guías de Compra</h3>
                               <div class="linea"></div>
                            </div>
                        </div>
                    </div>
                                 
                    <!-- End Intro Text -->
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-12 mb-sm-40">
                      
                         <!-- Photo Grid -->
                    <div class="row multi-columns-row mb-30 mb-xs-10 ">
                        
                        <!-- Photo Item -->
                        <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4 mb-md-10 banner_p">
                            <h3 class="alt-features-title font-alt mb-0 mt-0 pt-10 pb-10" style="background: #dddddd;padding: 10px;">REPARACIÓN DE FUGAS</h3>
                            <div class="post-prev-img">
                                <a href="<?php echo base_url('guias') ?>?id=1" class=""><img src="<?php echo base_url('public/theme/images/diagramas_menu/cero_fugas.png') ?>" alt="" /></a>
                            </div>

                        </div>
                        <!-- End Photo Item -->
                        
                        <!-- Photo Item -->
                        <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4 mb-md-10 banner_p">
                            <h3 class="alt-features-title font-alt mb-0 mt-0 pt-10 pb-10" style="background: #dddddd;padding: 10px;">CONDUCCIÓN DE AGUA</h3>
                            <div class="post-prev-img">
                                <a href="<?php echo base_url('guias') ?>?id=2" class=""><img src="<?php echo base_url('public/theme/images/diagramas_menu/conduccion_agua.png') ?>" alt="" /></a>
                            </div>

                        </div>
                        <!-- End Photo Item -->
                        
                        <!-- Photo Item -->
                        <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4 mb-md-10 banner_p">
                            <h3 class="alt-features-title font-alt mb-0 mt-0 pt-10 pb-10" style="background: #dddddd;padding: 10px;">ALCANTARILLADO</h3>
                            <div class="post-prev-img">
                                <a href="<?php echo base_url('guias') ?>?id=3" class=""><img src="<?php echo base_url('public/theme/images/diagramas_menu/alcantarillado.png') ?>" alt="" /></a>
                            </div>

                        </div>
                        <!-- End Photo Item -->
                        
                        <!-- Photo Item -->
                        <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4 mb-md-10 banner_p">
                            <h3 class="alt-features-title font-alt mb-0 mt-0 pt-10 pb-10" style="background: #dddddd;padding: 10px;">CONTRA INCENDIO</h3>
                            <div class="post-prev-img">
                                <a href="<?php echo base_url('guias') ?>?id=4" class=""><img src="<?php echo base_url('public/theme/images/diagramas_menu/contraincendio.png') ?>" alt="" /></a>
                            </div>

                        </div>
                        <!-- End Photo Item -->
                        
                        <!-- Photo Item -->
                        <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4 mb-md-10 banner_p">
                            <h3 class="alt-features-title font-alt mb-0 mt-0 pt-10 pb-10" style="background: #dddddd;padding: 10px;">SISTEMAS DE BOMBEO</h3>
                            <div class="post-prev-img">
                                <a href="<?php echo base_url('guias') ?>?id=5" class=""><img src="<?php echo base_url('public/theme/images/diagramas_menu/bombeo.png') ?>" alt="" /></a>
                            </div>

                        </div>
                        <!-- End Photo Item -->
                        
                        <!-- Photo Item -->
                        <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4 mb-md-10 banner_p">
                            <h3 class="alt-features-title font-alt mb-0 mt-0 pt-10 pb-10" style="background: #dddddd;padding: 10px;">VÁLVULAS DE CONTROL</h3>
                            <div class="post-prev-img">
                                <a href="<?php echo base_url('guias') ?>?id=6" class=""><img src="<?php echo base_url('public/theme/images/diagramas_menu/control_agua.png') ?>" alt="" /></a>
                            </div>

                        </div>
                        <!-- End Photo Item -->
                        
                      
                        
                    </div>
                    <!-- End Photo Grid -->
                            
                            
                        </div>
                        
                        <!--<div class="col-md-3 col-lg-3 ">                          
                            
                        </div>-->
                    </div>
                </div>
            </section>
            <!-- End Section -->
            
        
<section class="page-section hidden-lg hidden-md"></section>

