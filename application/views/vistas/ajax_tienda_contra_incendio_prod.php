<?php 
if(!empty($this->data['contra_incendio'])){
    $imgs = $this->uri->segment(2);
?>
<div class="row multi-columns-row">
                                 <div class="post-list" id="postList">
                                      <div class="clearfix">
                              <?php  if(!empty($this->data['contra_incendio'])): foreach($this->data['contra_incendio'] as $post): 
                                   $pesos_mx=0;
                              
                                  if($post['TipoMoneda'] == 'USD'):
                                  $dollar_a_peso = '';
                                  
                                   if(!empty($this->data['dollar'])):foreach($this->data['dollar'] as $dll): 
                                   $dollar_a_peso = $dll->PrecioPesos; 
                                   
                                   endforeach;                                      
                                  endif;
                                  if($post['Precio'] != 0):
                                      
                                      $pesos_mx =  ($post['Precio']*$dollar_a_peso);
                                  endif;                                  
                                elseif ($post['TipoMoneda'] == 'MXN'):
                                    $pesos_mx = $post['Precio'];
                                endif;
                                  ?>
                                <!-- Shop Item -->
                                <div class="col-md-3 col-lg-3 mb-60 mb-xs-40">                                    
                                    <div class="post-prev-img text-center">  
                                        <?php  
                                        //echo $imgs;        ?>                                
                                     
                                         <?php if($imgs == 'products_hidrante_seco'){
                                        ?>
                                        <img src="<?php echo base_url('public/theme') ?>/images/contra_incendio/hidrante_catego.jpg" alt="" />
                                        <?php } ?>
                                            <?php if($imgs == 'products_extension_hidrante'){
                                        ?>
                                        <img src="<?php echo base_url('public/theme') ?>/images/contra_incendio/extension_hidrante.jpg" alt="" />
                                        <?php } ?>
                                           <?php if($imgs == 'products_kit'){
                                               ?>
                                        <img src="<?php echo base_url('public/theme') ?>/images/contra_incendio/kit_reparacion.jpg" alt="" />
                                        <?php } ?>
                                        <?php if($imgs == 'products_valvula'){
                                               ?>
                                        <img src="<?php echo base_url('public/theme') ?>/images/contra_incendio/valvulas_fm_uno.jpg" alt="" style="width: 120px;"/>
                                        <?php } ?>
                                        <div class="intro-label">
                                            <!--<span class="label label-danger bg-red"></span>-->                                            
                                        </div>                                        
                                    </div>
                                    
                                    <div class="post-prev-title font-alt align-center">
                                        <a href="javascript:ShowDetail(<?php echo $post['id']; ?>);"><?php echo $post['Nombre']; ?></a>
                                        <br><?php echo $post['TipoJunta']; ?>
                                        <br><?php echo $post['SKU']; ?>
                                    </div>

                                    <div class="post-prev-text align-center">
                                        <!--<del>$150.00</del>-->
                                        &nbsp;
                                        <!--<strong>$94.75</strong>-->
                                    </div>
                                    
                                    <div class="post-prev-more align-center">
                                        <a href="javascript:AddCart('<?php echo htmlspecialchars($post['SKU']); ?>','<?php echo htmlspecialchars($post['Nombre']) ?>','<?php echo $pesos_mx ?>','<?php echo $post['TipoMoneda'] ?>')" class="btn btn-mod btn-gray btn-round"><i class="fa fa-shopping-cart"></i> Añadir Carrito</a>
                                    </div>
                                    
                                </div>
                                <!-- End Shop Item -->
                                <?php endforeach; else: ?>
                                   <p>Post(s) not available.</p>
                                 <?php  endif; ?>
                                  </div>
                                     <?php echo $this->ajax_pagination->create_links(); ?>
                                   </div>
                            </div>

<?php } ?>