<?php
$prod_id = $this->uri->segment(2);
$i = 0;
?>
<section class="page-section pt-20 pb-40">
    <div class="container relative">
        <!-- Intro Text -->
        <div class="row">
            <div class="col-md-12 mb-10">
                <div class="section-text align-left">
                    <h3 class="tituloproductos1 mt-0 mb-0" style="">Contra incendio</h3>
                    <div class="linea"></div>                        
                </div>                    
            </div>
            <div class="col-md-12 mb-30">
                <div class="section-text align-left">
                    Tienda > <a href="<?php echo base_url('tienda') ?>" style="color: #2d77b3;">Nuestros productos</a> > <a href="<?php echo base_url('tienda_alcantarillado') ?>" style="color: #2d77b3;">Alcantarillado</a> 
                                > <?php if($prod_id == 1){echo "BROCALES"; } ?>
                                   <?php if($prod_id == 2){echo "ESCALONES"; } ?> 
                                   <?php if($prod_id == 3){echo "VÁLVULA ANTI-RETORNO"; } ?> 
                                   <?php if($prod_id == 4){echo "MANGAS DE EMPOTRAMIENTO"; } ?>                                   
                </div>
            </div>
        </div>
        <!-- End Intro Text -->
        <div class="clearfix"></div>
        <?php if($prod_id == 1){
    
                        $total_ananido=0;
                        if(isset($_SESSION['carro'])){
                            $cart_session =  $_SESSION['carro'];
                            if($cart_session){
                                if(!empty($cart_session)): foreach($cart_session as $cs=>$carro_shop):
                                    if($cs == "3121-3BX16"){$total_ananido = $carro_shop['cantidad']; }
                                    endforeach; else:
                                        
                                        endif;
                                    
                                    }
                                    
                                    }
                        ?>
           <div class="row">            
            <div class="col-md-12" style="padding: 30px;">
                <div class="row">
                   <div class="col-md-5 text-center">
                       <div style="height: 297px;width: 100%;background: #fff;margin-bottom: 2em;">
                         <img src="<?php  echo base_url('public/theme/images/alcantarillado/brocal.jpg') ?>" alt=""  style="width: 100%;margin-bottom: 2em;max-width:375px"/>
                     </div>

                   </div>
                    <div class="col-md-7" style="text-align:justify;">
                        <h6 class="titulo_producto">BROCALES</h6>
                        <fieldset></fieldset>                           
                        <div  class="section-text"  style="color:#000;">
                          Los brocales son un elemento de seguridad que sirve de tapa a los posos de vista. Los hay de materiales diferentes como lo es el hierro fundido, concreto y polietileno de alta resistencia.
                        </div>  
                         <div class="clearfix"></div>                       
                        <div class="section-text pt-20 pb-20">                            
                            <?php echo form_open_multipart(base_url('welcome/crea_sesion_producto'), array('id' => 'frm-prod')) ?>
                    
                            <div class="row">                                
                                <div id="" class="col-md-6 text-left">
                                    Peso<br>
                                     <select id="peso" name="peso" onchange="MuestraProductoAlcantarillado(this.value)">
                                       <option value="">Selecciona</option>
                                        <?php if(!empty($this->data['peso_borcales'])): foreach($this->data['peso_borcales'] as $peso):?>
                                       <option value="<?php echo $peso['Peso'];?>"><?php echo $peso['Peso'];?></option>
                                        <?php 
                                          endforeach; else:                                              
                                          endif;
                                        ?>
                                   </select>                                
                                </div>
                                <div class="col-md-6">
                                    
                                </div>
                            </div>
                             
<!--                           <div class="row">                                
                                <div id="" class="col-md-6 text-left">
                                    Material<br>
                                    <select id="diam_nom" name="diam_nom" onchange="MuestraRangoPartida(this.value)">
                                       <option value="">Selecciona</option>                                      
                                  
                                   </select>                                  
                                </div>
                                <div class="col-md-6">
                                    
                                </div>
                            </div>-->
                            
                            
                             <br>
                            <input type="hidden" name="skus" id="skus" value="" />
                            <input type="hidden" name="nprod" id="nprod" value="" />
                            <input type="hidden" name="prices" id="prices" value="" />
                            <input type="hidden" name="tipo_moneda" id="tipo_moneda" value="" />
                            <input type="hidden" name="cantidad" id="cantidad" value="<?php if($total_ananido != 0){ echo $total_ananido+1;}else{ echo "1";}?>" />
                            <!--<input type="hidden" name="images" id="images" value="<?php ?>" />-->
                            <?php echo form_close(); ?>                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div id="filtros_de_productos">
                                <?php 
                              
                                ?>
                            </div>
                        </div>
                    </div>
   <?php } ?>
    <?php if($prod_id == 2){
    
                        $total_ananido=0;
                        if(isset($_SESSION['carro'])){
                            $cart_session =  $_SESSION['carro'];
                            if($cart_session){
                                if(!empty($cart_session)): foreach($cart_session as $cs=>$carro_shop):
                                    if($cs == "3121-3BX16"){$total_ananido = $carro_shop['cantidad']; }
                                    endforeach; else:
                                        
                                        endif;
                                    
                                    }
                                    
                                    }
                        ?>
           <div class="row">            
            <div class="col-md-12" style="padding: 30px;">
                <div class="row">
                   <div class="col-md-5 text-center">
                       <div style="height: 297px;width: 100%;background: #fff;margin-bottom: 2em;">
                         <img src="<?php  echo base_url('public/theme/images/alcantarillado/escalones.jpg') ?>" alt=""  style="width: 100%;margin-bottom: 2em;max-width: 320px"/>
                     </div>

                   </div>
                    <div class="col-md-7" style="text-align:justify;">
                        <h6 class="titulo_producto">ESCALONES</h6>
                        <fieldset></fieldset>                           
                        <div  class="section-text"  style="color:#000;">
                         Los escalones son usados en los pozos de vista para que una persona pueda bajar o subir al pozo y los hay en materiales como polietileno, fierro fundido y varilla cadminizada.                            
                        </div>  
                         <div class="clearfix"></div>                       
                        <div class="section-text pt-20 pb-20">                            
                            <?php echo form_open_multipart(base_url('welcome/crea_sesion_producto'), array('id' => 'frm-prod')) ?>
                            <div class="row">
                                <div class="col-md-5">
                            <div class="row">                 
                                <div id="" class="col-md-12 text-left">
                                    Material<br>
                                    <select id="modelo" name="modelo"  onchange="MuestraProdEscalones(this.value)">
                                         <option value="">Selecciona</option>
                                                    <?php if(!empty($this->data['material_escalones'])): foreach($this->data['material_escalones'] as $material):?>
                                             <option value="<?php echo $material['Material'];?>"><?php echo $material['Material'];?></option>
                                              <?php 
                                                endforeach; else:                                              
                                                    endif;     
                                              ?>                                       
                                   </select>                                  
                                </div>                              
                            </div>
                           
                                </div>
                                <div class="col-md-7">
                                         
                                    
                                </div>
                            </div>
                            
                            
                            
                             <br>
                            <input type="hidden" name="skus" id="skus" value="" />
                            <input type="hidden" name="nprod" id="nprod" value="" />
                            <input type="hidden" name="prices" id="prices" value="" />
                            <input type="hidden" name="tipo_moneda" id="tipo_moneda" value="" />
                            <input type="hidden" name="cantidad" id="cantidad" value="<?php if($total_ananido != 0){ echo $total_ananido+1;}else{ echo "1";}?>" />
                            <!--<input type="hidden" name="images" id="images" value="<?php ?>" />-->
                            <?php echo form_close(); ?>                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div id="filtros_de_productos">
                                <?php 
                              
                                ?>
                            </div>
                        </div>
                    </div>
   <?php } ?>     
<?php if($prod_id == 3){
    
                        $total_ananido=0;
                        if(isset($_SESSION['carro'])){
                            $cart_session =  $_SESSION['carro'];
                            if($cart_session){
                                if(!empty($cart_session)): foreach($cart_session as $cs=>$carro_shop):
                                    if($cs == "3121-3BX16"){$total_ananido = $carro_shop['cantidad']; }
                                    endforeach; else:
                                        
                                        endif;
                                    
                                    }
                                    
                                    }
                        ?>
           <div class="row">            
            <div class="col-md-12" style="padding: 30px;">
                <div class="row">
                   <div class="col-md-5 text-center">
                       <div style="height: 297px;width: 100%;background: #fff;margin-bottom: 2em;">
                         <img src="<?php  echo base_url('public/theme/images/alcantarillado/valvula_no_retorno.jpg') ?>" alt=""  style="width: 100%;margin-bottom: 2em;max-width:350px"/>
                     </div>

                   </div>
                    <div class="col-md-7" style="text-align:justify;">
                        <h6 class="titulo_producto">VÁLVULA ANTI-RETORNO</h6>
                        <fieldset></fieldset>                           
                        <div  class="section-text"  style="color:#000;">
Las Válvulas Anti-retorno están diseñadas para prevenir el flujo de retorno en numerosas aplicaciones en las que se necesita de un fácil acceso de servicio para el mantenimiento y limpieza. Excelente para ser utilizada en sistemas de drenaje sanitario o alcantarillado pluvial previniendo el retorno de desechos debido a la evacuación inadecuada.
                        </div>         
                         <div class="clearfix"></div>                       
                        <div class="section-text pt-20 pb-20">                            
                            <?php echo form_open_multipart(base_url('welcome/crea_sesion_producto'), array('id' => 'frm-prod')) ?>
                            <div class="row">
                                <div class="col-md-5">  
                           <div class="row">                                
                                <div id="lista_diam" class="col-md-12 text-left">
                                    Diámetro Nominal<br>
                                    <select id="diam_nom" name="diam_nom" onchange="MuestraProdAntiRetorno(this.value)">
                                       <option value="">Selecciona</option>     
                                          <?php if(!empty($this->data['diametro_valvula'])): foreach($this->data['diametro_valvula'] as $diam):?>
                                            <option value="<?php echo $diam['Peso'];?>"><?php echo $diam['Peso'];?></option>
                                             <?php 
                                               endforeach; else:                                              
                                               endif;
                                             ?>
                                   </select>
                                </div>
                            </div>
                                </div>
                                <div class="col-md-7">
                                         
                                </div>
                            </div>
                            
                            
                            
                             <br>
                            <input type="hidden" name="skus" id="skus" value="" />
                            <input type="hidden" name="nprod" id="nprod" value="" />
                            <input type="hidden" name="prices" id="prices" value="" />
                            <input type="hidden" name="tipo_moneda" id="tipo_moneda" value="" />
                            <input type="hidden" name="cantidad" id="cantidad" value="<?php if($total_ananido != 0){ echo $total_ananido+1;}else{ echo "1";}?>" />
                            <!--<input type="hidden" name="images" id="images" value="<?php ?>" />-->
                            <?php echo form_close(); ?>                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div id="filtros_de_productos">
                                <?php 
                              
                                ?>
                            </div>
                        </div>
                    </div>
   <?php } ?>             
        <?php if($prod_id == 4){
    
                        $total_ananido=0;
                        if(isset($_SESSION['carro'])){
                            $cart_session =  $_SESSION['carro'];
                            if($cart_session){
                                if(!empty($cart_session)): foreach($cart_session as $cs=>$carro_shop):
                                    if($cs == "3121-3BX16"){$total_ananido = $carro_shop['cantidad']; }
                                    endforeach; else:
                                        
                                        endif;
                                    
                                    }
                                    
                                    }
                        ?>
           <div class="row">            
            <div class="col-md-12" style="padding: 30px;">
                <div class="row">
                   <div class="col-md-5 text-center">
                       <div style="height: 297px;width: 100%;background: #fff;margin-bottom: 2em;">
                         <img src="<?php  echo base_url('public/theme/images/alcantarillado/manga_empotramiento.jpg') ?>" alt=""  style="width: 100%;margin-bottom: 2em;max-width:350px"/>
                     </div>

                   </div>
                    <div class="col-md-7" style="text-align:justify;">
                        <h6 class="titulo_producto">MANGAS DE EMPOTRAMIENTO</h6>
                        <fieldset></fieldset>                           
                        <div  class="section-text"  style="color:#000;">
        Las mangas de empotramiento se usan para unir las tuberías entre un pozo de vista, al mismo tiempo protegen al tubo de un movimiento brusco.
                        </div>
                         
                         <div class="clearfix"></div>
                        <div class="section-text pt-20 pb-20">                            
                            <?php echo form_open_multipart(base_url('welcome/crea_sesion_producto'), array('id' => 'frm-prod')) ?>
                            <div class="row">
                                <div class="col-md-5">
                            <div class="row">                 
                                <div id="" class="col-md-12 text-left">                                    
                                    Tamaño (PUL)<br>
                                    <select id="tamano" name="tamano"  onchange="MuestraProdManga(this.value)">
                                       <option value="">Selecciona</option>                                            
                                            <?php if(!empty($this->data['mangas'])): foreach($this->data['mangas'] as $manga):?>
                                            <option value="<?php echo $manga['Peso'];?>"><?php echo $manga['Peso'];?></option>
                                             <?php 
                                               endforeach; else:                                              
                                               endif;
                                             ?>                                                                
                                   </select>         
                                </div>                              
                            </div>
                             
                          
                            
                                </div>
                                <div class="col-md-7">
                                         
                                </div>
                            </div>
                            
                            
                            
                             <br>
                            <input type="hidden" name="skus" id="skus" value="" />
                            <input type="hidden" name="nprod" id="nprod" value="" />
                            <input type="hidden" name="prices" id="prices" value="" />
                            <input type="hidden" name="tipo_moneda" id="tipo_moneda" value="" />
                            <input type="hidden" name="cantidad" id="cantidad" value="<?php if($total_ananido != 0){ echo $total_ananido+1;}else{ echo "1";}?>" />
                            <!--<input type="hidden" name="images" id="images" value="<?php ?>" />-->
                            <?php echo form_close(); ?>                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div id="filtros_de_productos">
                                <?php 
                              
                                ?>
                            </div>
                        </div>
                    </div>
   <?php } ?>          

    </div>
</section>