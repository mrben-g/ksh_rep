<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ajax extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
        $this->load->model(array('product_model'));
  }
  
   public function flujos()
  {
 
  $tipo = ($this->input->post('id') != null)? $this->input->post('id') : '';
//if(isset($this->input->post('id')) && $this->input->post('id') != ""){ $tipo = $this->input->post('id');}else{$tipo = "";}
        if($tipo == 1){
            
            $data['html'] = '¿Qué condiciones de operación necesitas ?<br>
        <select id="condicion_operacion" name="condicion_operacion" onchange="Filtros(this.value)">
        <option value="">Selecciona</option>
        <option value="superficie">Superficie</option>
        <option value="sumergible">Sumergible</option>
        <option value="equipos de presión-booster">Equipos de presión-booster</option>
        <option value="equipos contra incendios">Equipos contra incendios</option>                                            
        </select>';

        }
        else if($tipo == 2){
            
       $data['html'] = '¿Qué condiciones de operación necesitas ?<br>
        <select id="condicion_operacion" name="condicion_operacion" onchange="Filtros(this.value)">
        <option value="">Selecciona</option>
        <option value="superficie_residual">Superficie</option>
        <option value="sumergible_residual">Sumergible</option>                                                                                      
        </select>';

        }
        
        echo(json_encode($data));
   }
   public function filtros() {
      // if(isset($_POST['id']) && $_POST['id'] != ""){ $tipo = $_POST['id'];}else{$tipo = "";}
     $tipo = ($this->input->post('id') != null)? $this->input->post('id') : '';
     
     if($tipo == 'superficie'){
         $data['html'] = '¿Qué aplicación requieres?<br>
             <select id="aplicacion_requiere" name="aplicacion_requiere" >
                <option value="">Selecciona</option>
                <option value="Domestisco">Doméstisco</option>
                <option value="Edificacion">Edificación</option>
                <option value="Industrial">Industrial</option>
                <option value="Mineria">Minería</option>
                 <option value="Obra Pública">Obra Pública</option>
                <option value="Agropecuario">Agropecuario</option>
                 <option value="Infraestructura">Infraestructura</option>
                <option value="Otro">Otro</option>
                </select>
                |
                ¿Qué tipo de instalación requieres?<br>
                <select id="instalacion_requiere" name="instalacion_requiere">
                    <option value="">Selecciona</option>
                    <option value="Cisterna">Cisterna</option>
                    <option value="Carcamo">Cárcamo</option>
                    <option value="Pozo Seco">Pozo Seco</option>
                    <option value="Pozo Somero">Pozo Somero</option>
                    <option value="Pozo Profundo">Pozo Profundo</option>
                    <option value="Embalse">Embalse</option>
                    <option value="En Remolque">En Remolque</option>
                    <option value="Otro">Otro</option>
                    </select>
                    <br>
                    ¿Qué tipo de faces requieres?&nbsp;&nbsp;&nbsp;
                    <input type="text" name="fase" id="fase" style="width: 60px"/> Fases<br>
                    ¿Qué tipo de voltaje requieres?
                    <input type="text" name="volt" id="volt" style="width: 60px"/> Volts<br>
                    <br><br>
                    ¿Requieres una selección avanzada?
                    <input type="radio" value="si" name="requerido" onclick="ShowR(1)" >Si
                    <input type="radio" value="no" name="requerido" onclick="ShowR(2)">No
                    |
                    <div class=" col-md-12"  style="margin-bottom: 10px;text-align: left;"><h5 style="text-align:left;margin: 15px 0px;">Proporciona la siguiente información</h5></div>
                    <div class=" col-md-12"  style="margin-bottom: 10px;text-align: left;">
    ¿Cuál es la distancia que recorre la tuberia de tu bomba hacia su descarga?
    <br><input type="text" name="profundidad" id="profundidad"  class="texto"/>
   <select id="mts_ft" name="mts_ft" class="lista">
        <option value="">Unidad</option>
        <option value="Metros">Metros</option>
        <option value="Pies">Pies</option>                     
    </select>
</div>
<div class=" col-md-12"  style="margin-bottom: 10px;text-align: left;">
    ¿Cuál es la altura que va de tu bomba a la cisterna?
    <br><input type="text" name="altura" id="altura"  class="texto"/>
   <select id="mts_al" name="mts_al" class="lista">
        <option value="">Unidad</option>
        <option value="Metros">Metros</option>                                           
    </select>
</div>
<div class=" col-md-12"  style="margin-bottom: 10px;text-align: left;">
 ¿Cuál será el diametro de la tubería?
    <br><input type="text" name="diametro" id="diametro"  class="texto"/>
   <select id="diamt" name="diamt" class="lista">
                                            <option value="">Unidad</option>
                                            <option value="pulgadas">Pulg.</option>
                                            <option value="milimetros">Mm.</option>
                                        </select>
</div>
<div class=" col-md-12"  style="margin-bottom: 10px;text-align: left;">
 
    ¿Cuál es la temperatura de tu fluido? 
    <br><input type="text" name="temperatura" id="temperatura"  class="texto"/>
   <select id="Tfluido" name="Tfluido" class="lista">
                                            <option value="">Unidad</option>
                                            <option value="centígrados">C&#176;</option>
                                            <option value="Fahrenheit">F&#176;</option>                                                                        
                                        </select>
</div>
<div class=" col-md-12"  style="margin-bottom: 10px;text-align: left;">Otros:<br><input type="text" name="otros" id="otros"/></div>
 |
 Qué tipo de accionamiento requieres:<br> 
 <select id="accionamiento" name="accionamiento" onchange="C(this.value)">
<option value="">Selecciona</option>                                       
<option value="Eléctrico">Eléctrico</option>
<option value="Combustión">Combustión</option>
<option value="Hidráhulico">Hidráhulico</option>
<option value="Otro">Otro</option>                                            
</select>
 |
<div id="comb" style="display: none;">
<div class=" col-md-12"  style="margin-bottom: 10px;text-align: left;"><h5 style="text-align:left;margin: 15px 0px;">Proporciona la siguiente información</h5></div>
<div class=" col-md-12"  style="margin-bottom: 10px;text-align: left;">Gasolina: <input type="radio" name="gasolinadiesel" id="gasolina" value="gasolina"/></div>
<div class=" col-md-12"  style="margin-bottom: 10px;text-align: left;">Diesel: <input type="radio" name="gasolinadiesel" id="diesel" value="disiel"/></div>
</div>';

}
else if($tipo == 'sumergible'){

$data['html'] = '¿Qué aplicación requieres?<br>
<select id="aplicacion_requiere" name="aplicacion_requiere">
    <option value="">Selecciona</option>
    <option value="Domestisco">Doméstisco</option>
    <option value="Edificacion">Edificación</option>
    <option value="Industrial">Industrial</option>
    <option value="Mineria">Minería</option>
     <option value="Obra Pública">Obra Pública</option>
    <option value="Agropecuario">Agropecuario</option>
     <option value="Infraestructura">Infraestructura</option>
    <option value="Otro">Otro</option>
</select>
|
¿Qué tipo de instalación requieres?<br>
  <select id="instalacion_requiere" name="instalacion_requiere">
    <option value="">Selecciona</option>
    <option value="Cisterna">Cisterna</option>
    <option value="Carcamo">Cárcamo</option>
    <option value="Pozo Seco">Pozo Seco</option>
    <option value="Pozo Somero">Pozo Somero</option>
    <option value="Pozo Profundo">Pozo Profundo</option>
    <option value="Embalse">Embalse</option>
    <option value="Otro">Otro</option>                                            
</select>
<br>
¿Qué tipo de faces requieres?&nbsp;&nbsp;&nbsp;
<input type="text" name="fase" id="fase" style="width: 60px"/> Fases<br>
¿Qué tipo de voltaje requieres?
<input type="text" name="volt" id="volt" style="width: 60px"/> Volts<br>
<br><br>
¿Requieres una selección avanzada?
<input type="radio" value="si" name="requerido" onclick="ShowR(1)" >Si
<input type="radio" value="no" name="requerido" onclick="ShowR(2)">No
|
<div class=" col-md-12"  style="margin-bottom: 10px;text-align: left;"><h5 style="text-align:left;margin: 15px 0px;">Proporciona la siguiente información</h5></div>
<div class=" col-md-12"  style="margin-bottom: 10px;text-align: left;">
¿A qué profundidad estará tu bomba?
<br><input type="text" name="profundidad" id="profundidad"  class="texto"/>
<select id="mts_ft" name="mts_ft" class="lista">
    <option value="">Unidad</option>
    <option value="Metros">Metros</option>
    <option value="Pies">Pies</option>                     
</select>
</div>
<div class=" col-md-12"  style="margin-bottom: 10px;text-align: left;">
    ¿Cuál será el diametro de la tubería?
   <br><input type="text" name="diametro" id="diametro"  class="texto"/>
   <select id="diamt" name="diamt" class="lista">
                                            <option value="">Unidad</option>
                                            <option value="pulgadas">Pulg.</option>
                                            <option value="milimetros">Mm.</option>                                                                                 
                                        </select>
</div>
<div class=" col-md-12"  style="margin-bottom: 10px;text-align: left;">
    ¿Cual es la temperatura de tu fluido?
    <br><input type="text" name="temperatura" id="temperatura"  class="texto"/>
   <select id="Tfluido" name="Tfluido" class="lista">
    <option value="">Unidad</option>
    <option value="centígrados">C&#176;</option>
    <option value="Fahrenheit">F&#176;</option>                                                                        
</select>
</div>

<div class=" col-md-12"  style="margin-bottom: 10px;text-align: left;">
    ¿Cuál es el porcentaje de sólidos?
    <br><input type="text" name="solidos" id="solidos"  class="texto"/> %   
</div>
<div class=" col-md-12"  style="margin-bottom: 10px;text-align: left;">
  ¿Requieres que tu bomba sea?
    <br>
    <input type="radio" value="Portatil" name="bomba_sea" >Portatil
<input type="radio" value="Fija" name="bomba_sea" >Fija
</div>
|
 Qué tipo de accionamiento requieres:<br> 
 <select id="accionamiento" name="accionamiento" onchange="C(this.value)">
<option value="">Selecciona</option>                                       
<option value="Eléctrico">Eléctrico</option>
<option value="Combustión">Combustión</option>
<option value="Hidráhulico">Hidráhulico</option>
<option value="Otro">Otro</option>                                              
</select> 
|
<div id="comb" style="display: none;">
<div class=" col-md-12"  style="margin-bottom: 10px;text-align: left;"><h5 style="text-align:left;margin: 15px 0px;">Proporciona la siguiente información</h5></div>
<div class=" col-md-12"  style="margin-bottom: 10px;text-align: left;">Gasolina: <input type="radio" name="gasolinadiesel" id="gasolina" value="gasolina"/></div>
<div class=" col-md-12"  style="margin-bottom: 10px;text-align: left;">Diesel: <input type="radio" name="gasolinadiesel" id="diesel" value="disiel"/></div>
</div>';

}
   else if($tipo == 'equipos de presión-booster'){
    
$data['html'] = '¿Qué aplicación requieres?<br>
<select id="aplicacion_requiere" name="aplicacion_requiere">
    <option value="">Selecciona</option>
    <option value="Habitacional">Habitacional</option>
    <option value="Comercial">Comercial</option>
    <option value="Hotel">Hotel</option>
    <option value="Hospital">Hospital</option>
    <option value="Industrial">Industrial</option>
<option value="Otro">Otro</option>
</select>
|
¿Qué tipo de instalación requieres?<br>
<select id="instalacion_requiere" name="instalacion_requiere">
    <option value="">Selecciona</option>
    <option value="Una Bomba">Una Bomba</option>
    <option value="Dos Bombas">Dos Bombas</option>
    <option value="Tres Bombas">Tres Bombas</option>                                                                    
</select>
<br>
¿Qué tipo de faces requieres?&nbsp;&nbsp;&nbsp;
<input type="text" name="fase" id="fase" style="width: 60px"/> Fases<br>
¿Qué tipo de voltaje requieres?
<input type="text" name="volt" id="volt" style="width: 60px"/> Volts<br>
<br><br>
¿Requieres una selección avanzada?
<input type="radio" value="si" name="requerido" onclick="ShowR(1)" >Si
<input type="radio" value="no" name="requerido" onclick="ShowR(2)">No
|
<div class=" col-md-12"  style="margin-bottom: 10px;text-align: left;"><h5 style="text-align:left;margin: 15px 0px;">Proporciona la siguiente información</h5></div>
<div class=" col-md-12"  style="margin-bottom: 10px;text-align: left;">
¿Requieres de una bomba de respaldo?<br>
<input type="radio" value="si" name="bomba_respaldo" >Si
<input type="radio" value="no" name="bomba_respaldo">No
</div>
<div class=" col-md-12"  style="margin-bottom: 10px;text-align: left;">
¿Cuál es el rango de presión que manejas?<br>
    <input type="text" name="bomba_presion" id="bomba_presion"/></div>
<div class=" col-md-12"  style="margin-bottom: 10px;text-align: left;">
¿Cuentas con una presión constante? <br>
<input type="radio" value="si" name="presion_constante" onclick="showText(1)">Si
<input type="radio" value="no" name="presion_constante" onclick="showText(2)">No<br>
<span id="tit" style="display:none;">¿CUANTO ES ?</span><br>
<input type="text" name="presion_constante_si" id="presion_constante_si" style="display:none;width:80px;"/>
</div>
<div class=" col-md-12"  style="margin-bottom: 10px;text-align: left;">
¿En que lugar de instalación ubicaras tu equipo?<br>
    <input type="text" name="lugar_instalacion" id="lugar_instalacion"/></div>
<div class=" col-md-12"  style="margin-bottom: 10px;text-align: left;">
    ¿Con que tipo de succión cuentas? <br>
<input type="radio" value="SUCCIÓN NEGATIVA" name="succiones" >Succión Negativa
<input type="radio" value="SUCCIÓN POSITIVA" name="succiones" >Succion Positiva
</div>
|
 Qué tipo de accionamiento requieres:<br> 
 <select id="accionamiento" name="accionamiento" onchange="C(this.value)">
<option value="">Selecciona</option>                                       
<option value="Eléctrico">Eléctrico</option>
<option value="Combustión">Combustión</option>
<option value="Hidráhulico">Hidráhulico</option>
<option value="Otro">Otro</option>       
</select>
|
<div id="comb" style="display: none;">
<div class=" col-md-12"  style="margin-bottom: 10px;text-align: left;"><h5 style="text-align:left;margin: 15px 0px;">Proporciona la siguiente información</h5></div>
<div class=" col-md-12"  style="margin-bottom: 10px;text-align: left;">Gasolina: <input type="radio" name="gasolinadiesel" id="gasolina" value="gasolina"/></div>
<div class=" col-md-12"  style="margin-bottom: 10px;text-align: left;">Diesel: <input type="radio" name="gasolinadiesel" id="diesel" value="disiel"/></div>
</div>';
}
else if($tipo == 'equipos contra incendios'){
    

    $data['html'] = '¿Qué aplicación requieres?<br>
        <select id="aplicacion_requiere" name="aplicacion_requiere">
            <option value="">Selecciona</option>
            <option value="Comercial">Comercial</option>
            <option value="Hotel">Hotel</option>
            <option value="Hospital">Hospital</option>
            <option value="Industrial">Industrial</option>
            <option value="Otro">Otro</option>
            </select><br>
¿Requieres que tu Equipo sea normado UL/FM?<br>
<input type="radio" value="si" name="normado" onclick="Abre(1)">Si
<input type="radio" value="no" name="normado" onclick="Abre(2)">No
<br>
|
<div id="no_normado" style="display:none;">
¿Selecciona con cuales bombas quieres que funcione tu equipo?<br>
<input type="checkbox" name="bmb1" value="Bomba Eléctrica"/>Bomba Eléctrica<br>
<input type="checkbox" name="bmb2" value="Bomba de Combustión" />Bomba de Combustión<br>
<input type="checkbox" name="bmb3" value=2Bomba Jockey" />Bomba Jockey<br>
</div>
<br>
<div id="si_normado"  style="display:none;">
¿Qué tipo de faces requieres?&nbsp;&nbsp;&nbsp;
<input type="text" name="fase" id="fase" style="width: 60px"/> Fases<br>
¿Qué tipo de voltaje requieres?
<input type="text" name="volt" id="volt" style="width: 60px"/> Volts<br>
|
<div class=" col-md-12"  style="margin-bottom: 10px;text-align: left;"><h5 style="text-align:left;margin: 15px 0px;">Proporciona la siguiente información</h5></div>
<div class=" col-md-12"  style="margin-bottom: 10px;text-align: left;">
    ¿Requieres de un equipo ensamblado tipo paquete?
    <br>
    <input type="radio" value="si" name="ensamblado_paquete" >Si
    <input type="radio" value="no" name="ensamblado_paquete" >No
</div>
<div class=" col-md-12"  style="margin-bottom: 10px;text-align: left;">
¿Que profundidad tiene tu cisterna?
<br><input type="text" name="profundidad_cisterna" id="profundidad_cisterna"/></div>
<div class=" col-md-12"  style="margin-bottom: 10px;text-align: left;">
¿Cuál sera el lugar de instalación de tu equipo?
<br><input type="text" name="lugar_instalacion" id="lugar_instalacion"/></div>
<div class=" col-md-12"  style="margin-bottom: 10px;text-align: left;">
¿Tu bomba estara en la superficie o será sumergible?<br>
<input type="radio" value="Bombas de Superficie" name="bomba_superficie" >SUPERFICIE
<input type="radio" value="Bombas Sumergible" name="bomba_superficie" >SUMERGIBLE
    <br>
</div>
|
 Qué tipo de accionamiento requieres:<br> 
 <select id="accionamiento" name="accionamiento" onchange="C(this.value)">
<option value="">Selecciona</option>                                       
<option value="Eléctrico">Eléctrico</option>
<option value="Combustión">Combustión</option>
<option value="Hidráhulico">Hidráhulico</option>
<option value="Otro">Otro</option>       
</select>
|
<div id="comb" style="display: none;">
<div class=" col-md-12"  style="margin-bottom: 10px;text-align: left;"><h5 style="text-align:left;margin: 15px 0px;">Proporciona la siguiente información</h5></div>
<div class=" col-md-12"  style="margin-bottom: 10px;text-align: left;">Gasolina: <input type="radio" name="gasolinadiesel" id="gasolina" value="gasolina"/></div>
<div class=" col-md-12"  style="margin-bottom: 10px;text-align: left;">Diesel: <input type="radio" name="gasolinadiesel" id="diesel" value="disiel"/></div>
</div>
</div>';

    
}
else if($tipo == 'superficie_residual'){
    
$data['html'] = '¿Qué aplicación requieres?<br>
<select id="aplicacion_requiere" name="aplicacion_requiere">
    <option value="">Selecciona</option>
    <option value="Domestisco">Doméstisco</option>
    <option value="Edificacion">Edificación</option>
    <option value="Industrial">Industrial</option>
    <option value="Mineria">Minería</option>
     <option value="Obra Pública">Obra Pública</option>
    <option value="Agropecuario">Agropecuario</option>
     <option value="Infraestructura">Infraestructura</option>
    <option value="Otro">Otro</option>
</select>
|
¿Qué tipo de instalación requieres?<br>
  <select id="instalacion_requiere" name="instalacion_requiere">
    <option value="">Selecciona</option>                                          
    <option value="Carcamo">Cárcamo</option>
    <option value="Pozo Seco">Pozo Seco</option>                                 
    <option value="En Remolque">En Remolque</option>
    <option value="Otro">Otro</option>                                            
</select>     
<br>
¿Qué tipo de faces requieres?&nbsp;&nbsp;&nbsp;
<input type="text" name="fase" id="fase" style="width: 60px"/> Fases<br>
¿Qué tipo de voltaje requieres?
<input type="text" name="volt" id="volt" style="width: 60px"/> Volts<br>
<br><br>
¿Requieres una selección avanzada?
<input type="radio" value="si" name="requerido" onclick="ShowR(1)">Si
<input type="radio" value="no" name="requerido" onclick="ShowR(2)">No
|
<div class=" col-md-12"  style="margin-bottom: 10px;text-align: left;"><h5 style="text-align:left;margin: 15px 0px;">Danos la siguiente información</h5></div>
<div class=" col-md-12"  style="margin-bottom: 10px;text-align: left;">¿ A que PROFUNDIDAD estara tu bomba?<br><input type="text" name="profundidad" id="profundidad" class="texto"/>
<select id="mts_ft" name="mts_ft" class="lista">
    <option value="">Unidad</option>
    <option value="Metros">Metros</option>
    <option value="Pies">Pies</option>                     
</select>
</div>
<div class=" col-md-12"  style="margin-bottom: 10px;text-align: left;">¿Cual sera el DIAMETRO DE LA TUBERÍA?<br><input type="text" name="diametro" id="diametro"  class="texto"/>
<select id="diamt" name="diamt" class="lista">
                                            <option value="">Unidad</option>
                                            <option value="pulgadas">Pulg.</option>
                                            <option value="milimetros">Mm.</option>                                                                                 
                                        </select>

</div>
<div class=" col-md-12"  style="margin-bottom: 10px;text-align: left;">¿Cuál es el PORCENTAJE DE SÓLIDOS?<br><input type="text" name="porcentaje_solido" id="porcentaje_solido"  class="texto"/> %</div>
<div class=" col-md-12"  style="margin-bottom: 10px;text-align: left;">
    ¿Requieres que tu bomba sea?    
  <br>
<input type="radio" value="Portatil" name="bomba_sea" >Portatil
<input type="radio" value="Fija" name="bomba_sea" >Fija
</div>
|
 Qué tipo de accionamiento requieres:<br> 
 <select id="accionamiento" name="accionamiento" onchange="C(this.value)">
<option value="">Selecciona</option>                                       
<option value="Eléctrico">Eléctrico</option>
<option value="Combustión">Combustión</option>
<option value="Hidráhulico">Hidráhulico</option>
<option value="Otro">Otro</option>                                                   
</select>
|
<div id="comb" style="display: none;">
<div class=" col-md-12"  style="margin-bottom: 10px;text-align: left;"><h5 style="text-align:left;margin: 15px 0px;">Proporciona la siguiente información</h5></div>
<div class=" col-md-12"  style="margin-bottom: 10px;text-align: left;">Gasolina: <input type="radio" name="gasolinadiesel" id="gasolina" value="gasolina"/></div>
<div class=" col-md-12"  style="margin-bottom: 10px;text-align: left;">Diesel: <input type="radio" name="gasolinadiesel" id="diesel" value="disiel"/></div>
</div>';


}
else if($tipo == 'sumergible_residual'){

$data['html'] = '¿Qué aplicación requieres?<br>
<select id="aplicacion_requiere" name="aplicacion_requiere">
    <option value="">Selecciona</option>
    <option value="Domestisco">Doméstisco</option>
    <option value="Edificacion">Edificación</option>
    <option value="Industrial">Industrial</option>
    <option value="Mineria">Minería</option>
     <option value="Obra Pública">Obra Pública</option>
    <option value="Agropecuario">Agropecuario</option>
     <option value="Infraestructura">Infraestructura</option>
    <option value="Otro">Otro</option>
</select>
|
¿Qué tipo de instalación requieres?<br>
  <select id="instalacion_requiere" name="instalacion_requiere">
    <option value="">Selecciona</option>                                          
    <option value="Carcamo">Cárcamo</option>
    <option value="Pozo Seco">Pozo Seco</option>                                 
    <option value="Otro">Otro</option>                                            
</select>
<br>
¿Qué tipo de faces requieres?&nbsp;&nbsp;&nbsp;
<input type="text" name="fase" id="fase" style="width: 60px"/> Fases<br>
¿Qué tipo de voltaje requieres?
<input type="text" name="volt" id="volt" style="width: 60px"/> Volts<br>
<br><br>
¿Requieres una selección avanzada?
<input type="radio" value="si" name="requerido" onclick="ShowR(1)" >Si
<input type="radio" value="no" name="requerido" onclick="ShowR(2)">No
|
<div class=" col-md-12"  style="margin-bottom: 10px;text-align: left;"><h5 style="text-align:left;margin: 15px 0px;">Danos la siguiente información</h5></div>
<div class=" col-md-12"  style="margin-bottom: 10px;text-align: left;">¿ A que PROFUNDIDAD estara tu bomba?<br><input type="text" name="profundidad" id="profundidad" class="texto"/>
   <select id="mts_ft" name="mts_ft" class="lista">
                                            <option value="">Unidad</option>
                                            <option value="Metros">Metros</option>
                                            <option value="Pies">Pies</option>                     
                                        </select>
</div>
<div class=" col-md-12"  style="margin-bottom: 10px;text-align: left;">¿Cuál sera el diametro de la tubería?<br><input type="text" name="diametro" id="diametro"  class="texto"/>
<select id="diamt" name="diamt" class="lista">
<option value="">Unidad</option>
<option value="Diametro">Diametro &#248;</option>                                                                                 
</select>
</div>
<div class=" col-md-12"  style="margin-bottom: 10px;text-align: left;">¿Cuál es el porcentaje de sólidos?<br><input type="text" name="porcentaje_solido" id="porcentaje_solido"  class="texto"/> %</div>
<div class=" col-md-12"  style="margin-bottom: 10px;text-align: left;">
    ¿Requieres que tu bomba sea?<br>
<input type="radio" value="Portatil" name="bomba_sea" >Portatil
<input type="radio" value="Fija" name="bomba_sea" >Fija
</div>
|
 Qué tipo de accionamiento requieres:<br> 
 <select id="accionamiento" name="accionamiento" onchange="C(this.value)">
<option value="">Selecciona</option>                                       
<option value="Eléctrico">Eléctrico</option>
<option value="Combustión">Combustión</option>
<option value="Hidráhulico">Hidráhulico</option>
<option value="Otro">Otro</option>       
</select>
|
<div id="comb" style="display: none;">
<div class=" col-md-12"  style="margin-bottom: 10px;text-align: left;"><h5 style="text-align:left;margin: 15px 0px;">Proporciona la siguiente información</h5></div>
<div class=" col-md-12"  style="margin-bottom: 10px;text-align: left;">Gasolina: <input type="radio" name="gasolinadiesel" id="gasolina" value="gasolina"/></div>
<div class=" col-md-12"  style="margin-bottom: 10px;text-align: left;">Diesel: <input type="radio" name="gasolinadiesel" id="diesel" value="disiel"/></div>
</div>';
 }
  echo(json_encode($data));
  }
   
   public function sistemas_de_bombeo() {
       
       
   }
   
     public function filtro_productos()
  {
    // echo json_encode($_POST);
    $search = $this->input->post('buscar');
    $result = $this->productos_model->search($search);
    echo json_encode($result);
  }

}
