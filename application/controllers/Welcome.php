<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	 {
		 parent::__construct();
		 //Codeigniter : Write Less Do More
                if (isset($_SESSION['carro'])) {
                       $this->data['carro_compra'] = $_SESSION['carro'];
               } else {
                       $this->data['carro'] = array();
               }
               
                if (isset($_SESSION['cotizacion'])) {
                       $this->data['cotizacion'] = $_SESSION['cotizacion'];
               } else {
                       $this->data['cotizacion'] = array();
               }
               
               $this->load->model(array('productos_model','pedidos_model','process_model'));
               $this->load->library('ajax_pagination');
               $this->perPage = 12;
               
	 }
         
          

	public function index()
	{
                 $this->data['vista'] = 'vistas/inicio';
                 $this->load->view('layouts/inicio');
	}
        
	public function about()
	{
	   $this->data['vista'] = 'vistas/about';
	   $this->load->view('layouts/inicio');
	}
        
              public function proyectos()
	{
	   $this->data['vista'] = 'vistas/proyectos';
	   $this->load->view('layouts/inicio');
	}
        
              public function curso_reparacion_fugas()
	{
	   $this->data['vista'] = 'vistas/curso_reparacion_fugas';
	   $this->load->view('layouts/inicio');
	}
        
              public function curso_hidrantes()
	{
	   $this->data['vista'] = 'vistas/curso_hidrantes';
	   $this->load->view('layouts/inicio');
	}
        
	public function contacto()
	{
	   $this->data['vista'] = 'vistas/contacto';
	   $this->load->view('layouts/inicio');
	}
        
              public function guias()
	{
                 $this->data['vista'] = 'vistas/guias';
                 $this->load->view('layouts/inicio');
	}

	/*PRODUCTOS*/
              public function producto()
	{
                $this->data['vista'] = 'vistas/producto';
                $this->load->view('layouts/inicio');
	}        
        
              public function producto_reparacion_de_tuberia()
	{
                $this->data['vista'] = 'vistas/producto_reparacion_de_tuberia';
                $this->load->view('layouts/inicio');
	}
              public function producto_tuberia_conexiones()
	{
                $this->data['vista'] = 'vistas/producto_tuberia_conexiones';
                $this->load->view('layouts/inicio');
	}
              public function producto_contra_incendio()
	{
                $this->data['vista'] = 'vistas/producto_contra_incendio';
                $this->load->view('layouts/inicio');
	}
              public function producto_conduccion_derivacion()
	{
                $this->data['vista'] = 'vistas/producto_conduccion_derivacion';
                $this->load->view('layouts/inicio');
	}
              public function producto_alcantarillado()
	{
                $this->data['vista'] = 'vistas/producto_alcantarillado';
                $this->load->view('layouts/inicio');
	}
               public function producto_sistema_bombeo()
	{
                $this->data['vista'] = 'vistas/producto_sistema_bombeo';
                $this->load->view('layouts/inicio');
	}
              public function producto_valvulas()
	{
                $this->data['vista'] = 'vistas/producto_valvulas';
                $this->load->view('layouts/inicio');
	}
              public function producto_filtros()
	{
                $this->data['vista'] = 'vistas/producto_filtros';
                $this->load->view('layouts/inicio');
	}
              public function producto_medidores()
	{
                $this->data['vista'] = 'vistas/producto_medidores';
                $this->load->view('layouts/inicio');
	}
              /*TIENDA*/
              public function tienda()
              {
                $this->data['vista'] = 'vistas/tienda';
                $this->load->view('layouts/inicio');
	}
               public function tienda_reparacion_de_tuberia()
	{
                $this->data['vista'] = 'vistas/tienda_reparacion_de_tuberia';
                $this->load->view('layouts/inicio');
	}
                  /**/
              public function tienda_reparacion_de_tuberia_prod()
	{                  
                $this->load->model(array('filtros_productos_model'));
                /*ARREGLOS ABRAZADERAS DE REPARACIÓN*/
                $this->data['diam'] = $this->filtros_productos_model->getDiametroNominal();                
                $this->data['rang'] = $this->filtros_productos_model->getRangoDiametro();
                /*ARREGLO ABRAZADERAS CAMPANA BELL JOINT*/
                $this->data['diam_campana'] = $this->filtros_productos_model->getDiametroNominalCampana();
                /*ARREGLO JUNTAS UNIVERSALES*/
                $this->data['diam_universal'] = $this->filtros_productos_model->getDiametroUniversal();
                /* ARREGLO COPLE DAYTON*/
                $this->data['diam_cople'] = $this->filtros_productos_model->getDiametroCople();                  
              
                $this->data['vista'] = 'vistas/tienda_reparacion_de_tuberia_prod';
                $this->load->view('layouts/inicio');               
	}
        
        
              /*FILTRO DIAMETROS*/
              public function diametros()
              {
                    
               /*    $this->load->model(array('filtros_productos_model'));
                 $diametro = ($this->input->post('diametro') != null)? $this->input->post('diametro') : '';   
                 $data = array('diametros' => $diametro);
                 $this->data['diametro'] = $this->filtros_productos_model->getDiametro($data);
                 $vista = $this->load->view('vistas/ajax_tienda_reparacion_de_tuberia_prod',array(),true);
                 echo json_encode($vista);
               */
                $this->load->model(array('filtros_productos_model'));
                $diametro = ($this->input->post('diametro') != null)? $this->input->post('diametro') : '';   
                $material=($this->input->post('material') != null)? $this->input->post('material') : '';
                $rango=($this->input->post('rango') != null)? $this->input->post('rango') : '';
                   $panel = '';
                     if($this->input->post('panel') != null){
                      foreach($this->input->post('panel') as $val){    
                          $panel .= $val.'-';
                      }
                      $panel =  substr($panel,0,strlen($panel)-1);
                     }
                
                
                $cadena = $diametro.'|'.$panel.'|'.$material.'|'.$rango;                
                $totalRec = count($this->filtros_productos_model->getRows_c(array(),$cadena));
                //pagination configuration
                $config['target']      = '#filtros_de_productos';
                $config['base_url']    = base_url().'welcome/ajaxPaginationData';
                $config['total_rows']  = $totalRec;
                $config['per_page']    = $this->perPage;
                $config['filter'] = $cadena;                
                $this->ajax_pagination->initialize($config);
                //get the posts data
                $this->data['diametro']= $this->filtros_productos_model->getRows_c(array('limit'=>$this->perPage),$cadena);
                $this->data['dollar'] = $this->productos_model->get_dollar();
                //var_dump($this->data['dollar']);
                $vista = $this->load->view('vistas/ajax_tienda_reparacion_de_tuberia_prod',array(),true);                
                echo $vista;
                
              }
                
              public function activa_panel()
              {    
                  
                $this->load->model(array('filtros_productos_model'));
                $diametro = ($this->input->post('diametro') != null)? $this->input->post('diametro') : ''; 
                //$panel =  ($this->input->post('panel') != null)? $this->input->post('panel') : '';
                $panel = '';
                     if($this->input->post('panel') != null){
                      foreach($this->input->post('panel') as $val){
                          $panel .= $val.'-';
                      }
                      $panel =  substr($panel,0,strlen($panel)-1);
                     }
                     
                $this->data['panel'] = $this->filtros_productos_model->getPanel_activo($diametro);     
                $this->data['id_panel'] = $panel;
                $vista = $this->load->view('vistas/ajax_panel_activa',array(),true);                
                echo $vista;          
              }
              
              public function rango_de_diametro()
              {              
                  $this->load->model(array('filtros_productos_model'));
                  $diametro = ($this->input->post('diametro') != null)? $this->input->post('diametro') : '';                     
                  $this->data['rang']= $this->filtros_productos_model->getRango_de_diametro($diametro);
                  
                  $vista = $this->load->view('vistas/ajax_lista_rango',array(),true);                      
                  echo $vista;          
              }
                
              public function paneles()
              {
                     $this->load->model(array('filtros_productos_model'));
                     $diametro = ($this->input->post('diametro') != null)? $this->input->post('diametro') : '';
                     //$panel = array('panel' =>($this->input->post('panel') != null)? $this->input->post('panel') : '');
                     $material=($this->input->post('material') != null)? $this->input->post('material') : '';
                     $rango=($this->input->post('rango') != null)? $this->input->post('rango') : '';
                     
                     $panel = '';
                     if($this->input->post('panel') != null){
                      foreach($this->input->post('panel') as $val){    
                          $panel .= $val.'-';
                      }
                      $panel =  substr($panel,0,strlen($panel)-1);
                     }
                     $cadena = $diametro.'|'.$panel.'|'.$material.'|'.$rango;
                    
                     //total rows count
                     $totalRec = count($this->filtros_productos_model->getRows_c(array(),$cadena));
                     //pagination configuration
                     $config['target']  = '#filtros_de_productos';
                     $config['base_url'] = base_url().'welcome/ajaxPaginationData';
                     $config['total_rows']  = $totalRec;
                     $config['per_page']    = $this->perPage;
                     $config['filter'] = $cadena;
                     $this->ajax_pagination->initialize($config);
                    //get the posts data
                     $this->data['diametro']= $this->filtros_productos_model->getRows_c(array('limit'=>$this->perPage),$cadena);
                     $this->data['dollar'] = $this->productos_model->get_dollar();
                     $vista = $this->load->view('vistas/ajax_tienda_reparacion_de_tuberia_prod',array(),true);
                     echo $vista;
              }
              
              public function panel_diam()
              {
                $this->load->model(array('filtros_productos_model'));
               // $panel = ($this->input->post('panel') != null)? $this->input->post('panel') : '';
                $diametro = ($this->input->post('diametro') != null)? $this->input->post('diametro') : '';
               
                 $panel = '';
                     if($this->input->post('panel') != null){
                      foreach($this->input->post('panel') as $val){    
                          $panel .= $val.'-';
                      }
                      $panel =  substr($panel,0,strlen($panel)-1);
                     }
                     // var_dump($panel);
                $this->data['diam'] = $this->filtros_productos_model->getDiametroNominal_panel($panel);
                $this->data['id_diam'] = $diametro;
                $vista = $this->load->view('vistas/ajax_lista_nominal',array(),true);
                echo $vista;
              }
                
              public function materiales()
              {
                $this->load->model(array('filtros_productos_model'));
                $diametro = ($this->input->post('diametro') != null)? $this->input->post('diametro') : '';
                //$panel = ($this->input->post('panel') != null)? $this->input->post('panel') : '';
                $material=($this->input->post('material') != null)? $this->input->post('material') : '';
                $rango=($this->input->post('rango') != null)? $this->input->post('rango') : '';
                 $panel = '';
                     if($this->input->post('panel') != null){
                      foreach($this->input->post('panel') as $val){    
                          $panel .= $val.'-';
                      }
                      $panel =  substr($panel,0,strlen($panel)-1);
                     }
                $cadena = $diametro.'|'.$panel.'|'.$material.'|'.$rango;
                //total rows count
                $totalRec = count($this->filtros_productos_model->getRows_c(array(),$cadena));
                //pagination configuration
                $config['target']      = '#filtros_de_productos';
                $config['base_url']    = base_url().'welcome/ajaxPaginationData';
                $config['total_rows']  = $totalRec;
                $config['per_page']    = $this->perPage;
                $config['filter'] = $cadena;
                $this->ajax_pagination->initialize($config);
                //get the posts data
                $this->data['diametro']= $this->filtros_productos_model->getRows_c(array('limit'=>$this->perPage),$cadena);
                $this->data['dollar'] = $this->productos_model->get_dollar();
                $vista = $this->load->view('vistas/ajax_tienda_reparacion_de_tuberia_prod',array(),true);
                echo $vista;
              }
                
              public function rangos()
              {
                $this->load->model(array('filtros_productos_model'));
                $diametro = ($this->input->post('diametro') != null)? $this->input->post('diametro') : '';
//                $panel = ($this->input->post('panel') != null)? $this->input->post('panel') : '';
                $material=($this->input->post('material') != null)? $this->input->post('material') : '';
                $rango=($this->input->post('rango') != null)? $this->input->post('rango') : '';
                  $panel = '';
                     if($this->input->post('panel') != null){
                      foreach($this->input->post('panel') as $val){    
                          $panel .= $val.'-';
                      }
                      $panel =  substr($panel,0,strlen($panel)-1);
                     }
                     
                $cadena = $diametro.'|'.$panel.'|'.$material.'|'.$rango;
                //total rows count
                $totalRec = count($this->filtros_productos_model->getRows_c(array(),$cadena));
                //pagination configuration
                $config['target']      = '#filtros_de_productos';
                $config['base_url']    = base_url().'welcome/ajaxPaginationData';
                $config['total_rows']  = $totalRec;
                $config['per_page']    = $this->perPage;
                $config['filter'] = $cadena;
                $this->ajax_pagination->initialize($config);
               //get the posts data
                $this->data['diametro']= $this->filtros_productos_model->getRows_c(array('limit'=>$this->perPage),$cadena);
                $this->data['dollar'] = $this->productos_model->get_dollar();
                $vista = $this->load->view('vistas/ajax_tienda_reparacion_de_tuberia_prod',array(),true);
                echo $vista;
               }
                
              public function panel_rang()                       
              {
                $this->load->model(array('filtros_productos_model'));
//                $panel=($this->input->post('panel') != null)? $this->input->post('panel') : '';
                $diametro = ($this->input->post('diametro') != null)? $this->input->post('diametro') : '';
                 $panel = '';
                     if($this->input->post('panel') != null){
                      foreach($this->input->post('panel') as $val){    
                          $panel .= $val.'-';
                      }
                      $panel =  substr($panel,0,strlen($panel)-1);
                     }
                $cadena = $diametro.'|'.$panel;
                $this->data['rang']= $this->filtros_productos_model->getDiametroNominal_rango($cadena);
                
                //var_dump($this->data['rang']);
                $vista = $this->load->view('vistas/ajax_lista_rango',array(),true);
                echo $vista;
               }
                
               /* JUNTAS UNIVERSALES*/
               function rango_universal(){
                   
                $this->load->model(array('filtros_productos_model'));
                $diametro = ($this->input->post('diametro') != null)? $this->input->post('diametro') : '';
                $this->data['rang_universal'] = $this->filtros_productos_model->getRangoUniversal($diametro);
                   
                $vista = $this->load->view('vistas/ajax_lista_rango_uni',array(),true);
                echo $vista;  
                
               }
               
               function tamano_universal()
               {
                $this->load->model(array('filtros_productos_model'));
                $rango = ($this->input->post('rango') != null)? $this->input->post('rango') : '';
                $this->data['taman_universal'] = $this->filtros_productos_model->getTamanoUniversal($rango);
                
                $vista = $this->load->view('vistas/ajax_lista_tamano_uni',array(),true);
                echo $vista;  
               }
               
               function product_universal()
               {
                $this->load->model(array('filtros_productos_model'));
                $tamano = ($this->input->post('tamano') != null)? $this->input->post('tamano') : '';
                $material=($this->input->post('material') != null)? $this->input->post('material') : '';
                $rango=($this->input->post('rango') != null)? $this->input->post('rango') : '';
                $cadena = $tamano.'|'.$material.'|'.$rango;
                $totalRec = count($this->filtros_productos_model->getRows_uni(array(),$cadena));
                //pagination configuration
                $config['target']      = '#filtros_de_productos';
                $config['base_url']    = base_url().'welcome/ajaxPaginationData';
                $config['total_rows']  = $totalRec;
                $config['per_page']    = $this->perPage;
                $config['filter'] = $cadena;
                $this->ajax_pagination->initialize($config);
               //get the posts data
                $this->data['diametro']= $this->filtros_productos_model->getRows_uni(array('limit'=>$this->perPage),$cadena);
                $this->data['dollar'] = $this->productos_model->get_dollar();
                $vista = $this->load->view('vistas/ajax_tienda_reparacion_de_tuberia_prod',array(),true);
                echo $vista;
                
                
               }
               
               /*BELL JOINT*/
               function products_joint()
               {
                $this->load->model(array('filtros_productos_model'));
                $diametro = ($this->input->post('diametro') != null)? $this->input->post('diametro') : '';
                $totalRec = count($this->filtros_productos_model->getRows_joint(array(),$diametro));
                //pagination configuration
                $config['target']      = '#filtros_de_productos';
                $config['base_url']    = base_url().'welcome/ajaxPaginationData';
                $config['total_rows']  = $totalRec;
                $config['per_page']    = $this->perPage;
                $config['filter'] = $diametro;
                $this->ajax_pagination->initialize($config);
               //get the posts data
                $this->data['diametro']= $this->filtros_productos_model->getRows_joint(array('limit'=>$this->perPage),$diametro);
                $this->data['dollar'] = $this->productos_model->get_dollar();
                $vista = $this->load->view('vistas/ajax_tienda_reparacion_de_tuberia_prod',array(),true);
                echo $vista;
                
               }
               /* COPLE DAYTON*/
               function products_cople()
               {
                $this->load->model(array('filtros_productos_model'));
                $rango=($this->input->post('rango') != null)? $this->input->post('rango') : '';
                $totalRec = count($this->filtros_productos_model->getRows_cople(array(),$rango));
                //pagination configuration
                $config['target']      = '#filtros_de_productos';
                $config['base_url']    = base_url().'welcome/ajaxPaginationData';
                $config['total_rows']  = $totalRec;
                $config['per_page']    = $this->perPage;
                $config['filter'] = $rango;
                $this->ajax_pagination->initialize($config);
               //get the posts data
                $this->data['diametro']= $this->filtros_productos_model->getRows_cople(array('limit'=>$this->perPage),$rango);
                $this->data['dollar'] = $this->productos_model->get_dollar();
                $vista = $this->load->view('vistas/ajax_tienda_reparacion_de_tuberia_prod',array(),true);
                echo $vista;
                
               }
               
              function ajaxPaginationData($page = 0,$filter='')
              {
                $this->load->model(array('filtros_productos_model'));
                $diametro = ($this->input->post('diametro') != null)? $this->input->post('diametro') : '';
                $panel = ($this->input->post('panel') != null)? $this->input->post('panel') : '';
                $material=($this->input->post('material') != null)? $this->input->post('material') : '';
                $rango=($this->input->post('rango') != null)? $this->input->post('rango') : '';
                $cadena = $diametro.'|'.$panel.'|'.$material.'|'.$rango;
                /*GENERAR UNA CADENA STRING CON LOS VALORES DE LOS FILTROS SEPARAR LA CADENA PARA OBTENER LOS VALORES*/
                if(!$page){
                    $offset = 0;
                }else{
                    $offset = $page;
                }
              
                if(!$filter){
                $search = $cadena;
                }else{
                $search = urldecode($filter);
                }
                //total rows count                
                $totalRec = count($this->filtros_productos_model->getRows_c(array(),$search));
                //pagination configuration
                $config['target']      = '#filtros_de_productos';
                $config['base_url']    = base_url().'/welcome/ajaxPaginationData';
                $config['total_rows']  = $totalRec;
                $config['per_page']    = $this->perPage;
                $config['filter'] = $search;
                $this->ajax_pagination->initialize($config);        
                //get the posts data                               
                $this->data['diametro'] = $this->filtros_productos_model->getRows_c(array('start'=>$offset,'limit'=>$this->perPage),$search); 
                //load the view        
                $this->data['dollar'] = $this->productos_model->get_dollar();
                $vista = $this->load->view('vistas/ajax_tienda_reparacion_de_tuberia_prod',array(),true);
                echo $vista;        
                }
                
                /**/
              public function tienda_tuberia_conexiones()
	{
                $this->data['vista'] = 'vistas/tienda_tuberia_conexiones';
                $this->load->view('layouts/inicio');
	}
        
               public function tienda_contra_incendio()
	{
                $this->data['vista'] = 'vistas/tienda_contra_incendio';
                $this->load->view('layouts/inicio');
	}
        
        
                public function tienda_contra_incendio_prod()
	{
                $this->load->model(array('filtro_contra_incendio_model'));
                 /*ARREGLOS ABRAZADERAS DE REPARACIÓN*/
                $this->data['tam_valvulas'] = $this->filtro_contra_incendio_model->getTamanoValvula();                
                $this->data['no_vias'] = $this->filtro_contra_incendio_model->getNumeroVias();
                $this->data['barril'] = $this->filtro_contra_incendio_model->getLargoBarril();
                $this->data['tipo_junta'] = $this->filtro_contra_incendio_model->getTipoJunta();
                /*extensiones de hidrante*/
                $this->data['apertura_valvula'] = $this->filtro_contra_incendio_model->getAperturaValvula();
                $this->data['barril_hidrante'] = $this->filtro_contra_incendio_model->getLargoBarrilHidrante();
                /*kits*/
                $this->data['apertura_valvula_kit'] = $this->filtro_contra_incendio_model->getAperturaValvulaKit();
                /*VALVULAS */
                $this->data['valvulas'] = $this->filtro_contra_incendio_model->getNombreValvulas();
                $this->data['diam_valvulas'] = $this->filtro_contra_incendio_model->getDiamValvulas();
                $this->data['tipo_junta_val'] = $this->filtro_contra_incendio_model->getTipoJuntaValvula();                
                //var_dump($this->data['tipo_junta_val']);
                $this->data['vista'] = 'vistas/tienda_contra_incendio_prod';
                $this->load->view('layouts/inicio');
	}
        
               function products_hidrante_seco()
               {                   
                $this->load->model(array('filtro_contra_incendio_model'));
                $valvula = ($this->input->post('valvula') != null)? $this->input->post('valvula') : '';
                $vias = ($this->input->post('vias') != null)? $this->input->post('vias') : '';
                $barril=($this->input->post('barril') != null)? $this->input->post('barril') : '';
                $junta=($this->input->post('junta') != null)? $this->input->post('junta') : '';                
                $data = array(
                   "valvula"=> $valvula,
                    "vias"=> $vias,
                    "barril"=> $barril,
                    "junta"=>$junta
                );
                    $this->data['contra_incendio'] = $this->filtro_contra_incendio_model->getProductContraIncendio($data);
                    $this->data['dollar'] = $this->productos_model->get_dollar();
                    $vista = $this->load->view('vistas/ajax_tienda_contra_incendio_prod',array(),true);
                    echo $vista;
               }
               
               function products_extension_hidrante()
               {
                $this->load->model(array('filtro_contra_incendio_model'));
                $valvula = ($this->input->post('valvula') != null)? $this->input->post('valvula') : '';
                $barril=($this->input->post('barril') != null)? $this->input->post('barril') : '';
                $data = array(
                   "valvula"=> $valvula,                    
                    "barril"=> $barril                    
                );
                    $this->data['contra_incendio'] = $this->filtro_contra_incendio_model->getProductExtHidrante($data);
                    $this->data['dollar'] = $this->productos_model->get_dollar();
                    $vista = $this->load->view('vistas/ajax_tienda_contra_incendio_prod',array(),true);
                    echo $vista;   
               }
               
               function products_kit()
               {
                $this->load->model(array('filtro_contra_incendio_model'));
                $valvula = ($this->input->post('valvula') != null)? $this->input->post('valvula') : '';
                $data = array(
                   "valvula"=> $valvula                     
                );
                
                    $this->data['contra_incendio'] = $this->filtro_contra_incendio_model->getProductKit($data);
                    $this->data['dollar'] = $this->productos_model->get_dollar();
                    $vista = $this->load->view('vistas/ajax_tienda_contra_incendio_prod',array(),true);
                    echo $vista;                   
               }
               
               function products_valvula()
               {
                $this->load->model(array('filtro_contra_incendio_model'));
                $valvula = ($this->input->post('valvula') != null)? $this->input->post('valvula') : '';
                $diametro = ($this->input->post('diametro') != null)? $this->input->post('diametro') : '';
                $junta = ($this->input->post('junta') != null)? $this->input->post('junta') : '';
                $data = array(
                   "valvula"=> $valvula,
                   "diametro"=>$diametro,
                   "junta"=>$junta
                );
                //var_dump($data);
                $this->data['contra_incendio'] = $this->filtro_contra_incendio_model->getProductValvula($data);
                $this->data['dollar'] = $this->productos_model->get_dollar();
                $vista = $this->load->view('vistas/ajax_tienda_contra_incendio_prod',array(),true);
                echo $vista;  
               }
               
               /*CONDUCCION DERIVACION*/
               public function tienda_conduccion_derivacion()
	{
                $this->data['vista'] = 'vistas/tienda_conduccion_derivacion';
                $this->load->view('layouts/inicio');
	}
              
               public function tienda_conduccion_derivacion_prod()
	{
                $this->load->model(array('filtro_conduccion_derivacion_model'));               
                /*TEE PARTIDA*/
                $this->data['diam_partida'] = $this->filtro_conduccion_derivacion_model->getDiamTEEPartida();
                //$this->data['rang_partida'] = $this->filtro_conduccion_derivacion_model->getRangTEEPartida();
                
                $this->data['vista'] = 'vistas/tienda_conduccion_derivacion_prod';
                $this->load->view('layouts/inicio');
	}
                /*TEE PARTIDA*/
              public function lista_partida_rango()
              {
                $this->load->model(array('filtro_conduccion_derivacion_model'));                  
                $modelo = ($this->input->post('modelo') != null)? $this->input->post('modelo') : '';
                $diametro = ($this->input->post('diametro') != null)? $this->input->post('diametro') : '';
                $brida = ($this->input->post('brida') != null)? $this->input->post('brida') : '';

                $data = array(
                   "modelo"=> $modelo,
                   "diametro"=>$diametro,
                   "brida"=>$brida
                );
                  $this->data['partida_rango'] = $this->filtro_conduccion_derivacion_model->getRangoPartida($data);
                  $vista = $this->load->view('vistas/ajax_lista_rango_partida',array(),true);
                   echo $vista;  
                  
               } 
               
              public function lista_partida_brida()
              {
                $this->load->model(array('filtro_conduccion_derivacion_model'));                  
                $modelo = ($this->input->post('modelo') != null)? $this->input->post('modelo') : '';
                $diametro = ($this->input->post('diametro') != null)? $this->input->post('diametro') : '';
                $rango = ($this->input->post('rango') != null)? $this->input->post('rango') : '';
                  
                $data = array(
                "modelo"=> $modelo,
                "diametro"=>$diametro,
                "rango"=>$rango
                 );
                
                $this->data['partida_brida'] = $this->filtro_conduccion_derivacion_model->getBridaPartida($data);
                $vista = $this->load->view('vistas/ajax_lista_brida_partida',array(),true);
                 echo $vista;  
                   
               }
               
               public function producto_tee_partida()
               {
                  $this->load->model(array('filtro_conduccion_derivacion_model'));                  
                  $modelo = ($this->input->post('modelo') != null)? $this->input->post('modelo') : '';
                  $diametro = ($this->input->post('diametro') != null)? $this->input->post('diametro') : '';
                  $rango = ($this->input->post('rango') != null)? $this->input->post('rango') : '';
                  $brida = ($this->input->post('brida') != null)? $this->input->post('brida') : '';
                  
                  $data = array(
                   "modelo"=> $modelo,
                   "diametro"=>$diametro,
                   "rango"=>$rango,
                   "brida"=>$brida
                );
                  $this->data['conduccion_derivacion'] = $this->filtro_conduccion_derivacion_model->getProductoPartida($data);
                  $this->data['dollar'] = $this->productos_model->get_dollar();
                  $vista = $this->load->view('vistas/ajax_tienda_conduccion_derivacion_prod',array(),true);
                   echo $vista;  
                  
               }
               /*Collarines*/
               public function lista_diametro_collarin()
               {
                 $this->load->model(array('filtro_conduccion_derivacion_model'));                  
                 $modelo = ($this->input->post('modelo') != null)? $this->input->post('modelo') : '';
                 $data = array(
                   "modelo"=> $modelo
                );
                  
                 $this->data['diam_collarin'] = $this->filtro_conduccion_derivacion_model->getDiamCollarin($data);
                 $vista = $this->load->view('vistas/ajax_lista_diam_collarin',array(),true);
                 echo $vista;  
               }
               
               public function lista_rango_collarin()
               {
                 $this->load->model(array('filtro_conduccion_derivacion_model'));                  
                 $modelo = ($this->input->post('modelo') != null)? $this->input->post('modelo') : '';
                 $diametro = ($this->input->post('diametro') != null)? $this->input->post('diametro') : '';
                 $data = array(
                   "modelo"=> $modelo,
                   "diametro"=>$diametro,
                );
                  
                 $this->data['rango_collarin'] = $this->filtro_conduccion_derivacion_model->getRangoCollarin($data);
                 $vista = $this->load->view('vistas/ajax_lista_rango_collarin',array(),true);
                 echo $vista;  
               }
               
               public function lista_collarin_salida()
               {
                   $this->load->model(array('filtro_conduccion_derivacion_model'));                  
                  $modelo = ($this->input->post('modelo') != null)? $this->input->post('modelo') : '';
                  $diametro = ($this->input->post('diametro') != null)? $this->input->post('diametro') : '';
                  $rango = ($this->input->post('rango') != null)? $this->input->post('rango') : '';
                  
                   $data = array(
                   "modelo"=> $modelo,
                   "diametro"=>$diametro,
                   "rango"=>$rango
                );
                  $this->data['salida_collarin'] = $this->filtro_conduccion_derivacion_model->getSalidaCollarin($data);
                  $vista = $this->load->view('vistas/ajax_lista_salida_collarin',array(),true);
                   echo $vista;  
                   
               }
               public function lista_collarin_rosca()
               {
                   $this->load->model(array('filtro_conduccion_derivacion_model'));                  
                  $modelo = ($this->input->post('modelo') != null)? $this->input->post('modelo') : '';
                  $diametro = ($this->input->post('diametro') != null)? $this->input->post('diametro') : '';
                  $rango = ($this->input->post('rango') != null)? $this->input->post('rango') : '';
                  $salida = ($this->input->post('salida') != null)? $this->input->post('salida') : '';
                  
                   $data = array(
                   "modelo"=> $modelo,
                   "diametro"=>$diametro,
                   "rango"=>$rango,
                   "salida"=>$salida
                );
                  $this->data['rosca_collarin'] = $this->filtro_conduccion_derivacion_model->getRoscaCollarin($data);
                  $vista = $this->load->view('vistas/ajax_lista_rosca_collarin',array(),true);
                   echo $vista;  
                   
               }
               
               public function producto_collarin()
               {
                  $this->load->model(array('filtro_conduccion_derivacion_model'));                  
                  $modelo = ($this->input->post('modelo') != null)? $this->input->post('modelo') : '';
                  $diametro = ($this->input->post('diametro') != null)? $this->input->post('diametro') : '';
                  $rango = ($this->input->post('rango') != null)? $this->input->post('rango') : '';
                  $salida = ($this->input->post('salida') != null)? $this->input->post('salida') : '';
                  $rosca = ($this->input->post('rosca') != null)? $this->input->post('rosca') : '';
                   $data = array(
                   "modelo"=> $modelo,
                   "diametro"=>$diametro,
                   "rango"=>$rango,
                   "salida"=>$salida,
                   "rosca"=>$rosca
                );
                  $this->data['conduccion_derivacion'] = $this->filtro_conduccion_derivacion_model->getProductoCollarin($data);
                  $this->data['dollar'] = $this->productos_model->get_dollar();
                  $vista = $this->load->view('vistas/ajax_tienda_conduccion_derivacion_prod',array(),true);
                   echo $vista;  
                   
               }
               
               /*JUNTAS UNIVERSALES*/               
               public function lista_diametro_ju()
               {
                $this->load->model(array('filtro_conduccion_derivacion_model'));                  
                $material = ($this->input->post('material') != null)? $this->input->post('material') : '';
                     $data = array(
                   "material"=> $material
                ); 
                     
                 $this->data['diam_ju'] = $this->filtro_conduccion_derivacion_model->getDiamJuntaUniversal($data);
                 $vista = $this->load->view('vistas/ajax_lista_diam_ju',array(),true);
                 echo $vista;       
               }
               
               public function lista_rango_ju()
               {
                 $this->load->model(array('filtro_conduccion_derivacion_model'));                  
                 $material = ($this->input->post('material') != null)? $this->input->post('material') : '';
                 $diametro = ($this->input->post('diametro') != null)? $this->input->post('diametro') : '';
                 $data = array(
                   "material"=> $material,
                   "diametro"=>$diametro,
                );
                  
                 $this->data['rango_ju'] = $this->filtro_conduccion_derivacion_model->getRangoJuntaUniversal($data);
                 $vista = $this->load->view('vistas/ajax_lista_rango_ju',array(),true);
                 echo $vista;  
               }
               
               public function lista_barril_ju()
               {
                  $this->load->model(array('filtro_conduccion_derivacion_model'));                  
                  $material = ($this->input->post('material') != null)? $this->input->post('material') : '';
                  $diametro = ($this->input->post('diametro') != null)? $this->input->post('diametro') : '';
                  $rango = ($this->input->post('rango') != null)? $this->input->post('rango') : '';
                  
                   $data = array(
                   "material"=> $material,
                   "diametro"=>$diametro,
                   "rango"=>$rango
                );
                  $this->data['barril_ju'] = $this->filtro_conduccion_derivacion_model->getBarrilJuntaUniversal($data);
                  $vista = $this->load->view('vistas/ajax_lista_barril_ju',array(),true);
                   echo $vista;  
               }
               
               public function producto_ju()
               {
                  $this->load->model(array('filtro_conduccion_derivacion_model'));                  
                  $material = ($this->input->post('material') != null)? $this->input->post('material') : '';
                  $diametro = ($this->input->post('diametro') != null)? $this->input->post('diametro') : '';
                  $rango = ($this->input->post('rango') != null)? $this->input->post('rango') : '';
                  $barril = ($this->input->post('barril') != null)? $this->input->post('barril') : '';
             
                   $data = array(
                   "material"=> $material,
                   "diametro"=>$diametro,
                   "rango"=>$rango,
                   "barril"=>$barril
                   );
                  $this->data['conduccion_derivacion'] = $this->filtro_conduccion_derivacion_model->getProductoJuntaUniversal($data);
                  $this->data['dollar'] = $this->productos_model->get_dollar();
                  $vista = $this->load->view('vistas/ajax_tienda_conduccion_derivacion_prod',array(),true);
                   echo $vista;  
                   
               }
               
               /*ADAPTADORES BRIDADOS*/
               public function lista_diam_adapt()
               {
                  $this->load->model(array('filtro_conduccion_derivacion_model'));                  
                  $modelo = ($this->input->post('modelo') != null)? $this->input->post('modelo') : '';
                 // $rango = ($this->input->post('rango') != null)? $this->input->post('rango') : '';
                   $data = array(
                   "modelo"=> $modelo
                   );
                   
                 $this->data['diam_adapt'] = $this->filtro_conduccion_derivacion_model->getDiamAdaptadores($data);
                 $vista = $this->load->view('vistas/ajax_lista_diam_adaptador',array(),true);
                 echo $vista;  
               }
               
               public function lista_rango_adapt()
               {        
                 $this->load->model(array('filtro_conduccion_derivacion_model'));                  
                 $modelo = ($this->input->post('modelo') != null)? $this->input->post('modelo') : '';
                 $diametro = ($this->input->post('diametro') != null)? $this->input->post('diametro') : '';
                 $data = array(
                   "modelo"=> $modelo,
                   "diametro"=>$diametro,
                 );
                  
                 $this->data['rango_adapt'] = $this->filtro_conduccion_derivacion_model->getRangoAdaptadores($data);
                 $vista = $this->load->view('vistas/ajax_lista_rango_adaptador',array(),true);
                 echo $vista;  
                   
               }
               
               public function producto_adapt()
               {
                  $this->load->model(array('filtro_conduccion_derivacion_model'));                  
                  $modelo = ($this->input->post('modelo') != null)? $this->input->post('modelo') : '';
                  $diametro = ($this->input->post('diametro') != null)? $this->input->post('diametro') : '';
                  $rango = ($this->input->post('rango') != null)? $this->input->post('rango') : '';
                  $data = array(
                   "modelo"=> $modelo,
                   "diametro"=>$diametro,
                   "rango"=>$rango                   
                   );
                  
                  $this->data['conduccion_derivacion'] = $this->filtro_conduccion_derivacion_model->getProductoAdaptadores($data);
                  $this->data['dollar'] = $this->productos_model->get_dollar();
                  $vista = $this->load->view('vistas/ajax_tienda_conduccion_derivacion_prod',array(),true);
                   echo $vista;  
                    
                   
               }
               
               /*BRIDAS RAPIDAS*/
               public function lista_brida_br()
               {        
                $this->load->model(array('filtro_conduccion_derivacion_model'));                  
                $modelo = ($this->input->post('modelo') != null)? $this->input->post('modelo') : '';
              //  $rango = ($this->input->post('rango') != null)? $this->input->post('rango') : '';
                  
                $data = array(
                "modelo"=> $modelo                
                 );
                
                $this->data['brida_rapida'] = $this->filtro_conduccion_derivacion_model->getBridaBR($data);
                $vista = $this->load->view('vistas/ajax_lista_brida_br',array(),true);
                 echo $vista;  
                   
               }
               
               public function lista_rango_br()
               {
                $this->load->model(array('filtro_conduccion_derivacion_model'));                  
                $modelo = ($this->input->post('modelo') != null)? $this->input->post('modelo') : '';
                $brida = ($this->input->post('brida') != null)? $this->input->post('brida') : '';
                
                $data = array(
                "modelo"=> $modelo,
                "brida"=>$brida
                 );
                
                $this->data['rango_br'] = $this->filtro_conduccion_derivacion_model->getRangoBR($data);
                $vista = $this->load->view('vistas/ajax_lista_rango_br',array(),true);
                 echo $vista;  
               }
               
               public function producto_br()
               {
                  $this->load->model(array('filtro_conduccion_derivacion_model'));                  
                  $modelo = ($this->input->post('modelo') != null)? $this->input->post('modelo') : '';
                  $brida = ($this->input->post('brida') != null)? $this->input->post('brida') : '';
                  $rango = ($this->input->post('rango') != null)? $this->input->post('rango') : '';
                  $data = array(
                   "modelo"=> $modelo,
                   "brida"=>$brida,
                   "rango"=>$rango                   
                   );                  
                  $this->data['conduccion_derivacion'] = $this->filtro_conduccion_derivacion_model->getProductoBridasRapidas($data);
                  $this->data['dollar'] = $this->productos_model->get_dollar();
                  $vista = $this->load->view('vistas/ajax_tienda_conduccion_derivacion_prod',array(),true);
                   echo $vista;  
               }
               
               /*ALCANTARILLADO*/
               public function tienda_alcantarillado()
	{
                $this->data['vista'] = 'vistas/tienda_alcantarillado';
                $this->load->view('layouts/inicio');
	}
        
              public function tienda_alcantarillado_prod()
	{
                $this->load->model(array('filtro_alcantarillado_model'));
                /*BROCALES*/
                $this->data['peso_borcales'] = $this->filtro_alcantarillado_model->getPesoBrocales();   
                /*ESCALONES*/
                $this->data['material_escalones'] = $this->filtro_alcantarillado_model->getMaterialEscalones();
                  /*VALVULAS ANTI RETORNO*/ 
                $this->data['diametro_valvula'] = $this->filtro_alcantarillado_model->getDiamValvulaAntiRetorno();
                /*MANGAS*/ 
                $this->data['mangas'] = $this->filtro_alcantarillado_model->getTamanoMangas();
                
                $this->data['vista'] = 'vistas/tienda_alcantarillado_prod';
                $this->load->view('layouts/inicio');
	}
                
               /*BROCALES*/
               public function product_alcantarillado() 
               {
                  $this->load->model(array('filtro_alcantarillado_model'));                  
                  $peso = ($this->input->post('peso') != null)? $this->input->post('peso') : '';                  
                  $data = array(
                   "peso"=> $peso                   
                   );
                  
                  $this->data['alcantarillado'] = $this->filtro_alcantarillado_model->getProductoAlcantarillado($data);
                  $this->data['dollar'] = $this->productos_model->get_dollar();
                  $vista = $this->load->view('vistas/ajax_tienda_alcantarillado_prod',array(),true);
                   echo $vista;  
                   
               }
               
               /*ESCALONES*/
               public function product_escalones()
               {
                  $this->load->model(array('filtro_alcantarillado_model'));                  
                  $material = ($this->input->post('material') != null)? $this->input->post('material') : '';                  
                  $data = array(
                   "material"=> $material                   
                   );                  
                  
                  $this->data['alcantarillado'] = $this->filtro_alcantarillado_model->getProductoEscalones($data);
                  $this->data['dollar'] = $this->productos_model->get_dollar();
                  $vista = $this->load->view('vistas/ajax_tienda_alcantarillado_prod',array(),true);
                   echo $vista;  
               }
               /*VALVULAS ANTI RETORNO*/ 
               public function product_antiretorno()
               {
                  $this->load->model(array('filtro_alcantarillado_model'));                  
                  $diametro = ($this->input->post('diametro') != null)? $this->input->post('diametro') : '';
                  $data = array(
                   "diametro"=> $diametro                 
                   );                  
                  
                  $this->data['alcantarillado'] = $this->filtro_alcantarillado_model->getProductoAntiRetorno($data);
                  $this->data['dollar'] = $this->productos_model->get_dollar();
                  $vista = $this->load->view('vistas/ajax_tienda_alcantarillado_prod',array(),true);
                   echo $vista;    
                   
               }
               /*MANGAs*/
               public function product_manga()
               {
                  $this->load->model(array('filtro_alcantarillado_model'));                  
                  $tamano = ($this->input->post('tamano') != null)? $this->input->post('tamano') : '';
                  $data = array(
                   "tamano"=> $tamano                 
                   );                  
                  
                  $this->data['alcantarillado'] = $this->filtro_alcantarillado_model->getProductoManga($data);
                  $this->data['dollar'] = $this->productos_model->get_dollar();
                  $vista = $this->load->view('vistas/ajax_tienda_alcantarillado_prod',array(),true);
                   echo $vista;    
                   
               }
               
               public function tienda_sistema_bombeo()
	{
                $this->data['vista'] = 'vistas/tienda_sistema_bombeo';
                $this->load->view('layouts/inicio');
	}
               public function tienda_valvulas()
	{
                $this->data['vista'] = 'vistas/tienda_valvulas';
                $this->load->view('layouts/inicio');
	}
        
               /*FILTROS*/
               public function tienda_filtros()
	{
                $this->data['vista'] = 'vistas/tienda_filtros';
                $this->load->view('layouts/inicio');
	}
               public function tienda_filtros_prod()
	{
                 $this->load->model(array('filtro_filtros_model'));
                /*CANASTA*/
                $this->data['filtro_canasta'] = $this->filtro_filtros_model->getTamanoFiltro();   
                   
                $this->data['vista'] = 'vistas/tienda_filtros_prod';
                $this->load->view('layouts/inicio');
                
	}
              
              public function product_filtro_canasta()
              {
                  $this->load->model(array('filtro_filtros_model'));                  
                  $tamano = ($this->input->post('tamano') != null)? $this->input->post('tamano') : '';
                  $data = array(
                   "tamano"=> $tamano                 
                   );                  
                  
                  $this->data['filtros'] = $this->filtro_filtros_model->getProductoFiltroCanasta($data);
                  $this->data['dollar'] = $this->productos_model->get_dollar();
                  $vista = $this->load->view('vistas/ajax_tienda_filtros_prod',array(),true);
                   echo $vista;    
                  
              }  
        
        
              /*MEDIDORES*/
              public function tienda_medidores()
	{
                $this->data['vista'] = 'vistas/tienda_medidores';
                $this->load->view('layouts/inicio');
	}
               
              public function tienda_medidores_prod()
	{
                $this->load->model(array('filtro_medidor_model'));
                /*tamano residencial*/
                $this->data['tamano_residencial'] = $this->filtro_medidor_model->getTamanoResidencial();   
                $this->data['tamano_comercial'] = $this->filtro_medidor_model->getTamanoComercial();   
                  
                $this->data['vista'] = 'vistas/tienda_medidores_prod';
                $this->load->view('layouts/inicio');
	}
                
              public function lista_rosca_residencial()
              {
                  $this->load->model(array('filtro_medidor_model'));                  
                  $tamano = ($this->input->post('tamano') != null)? $this->input->post('tamano') : '';
                 
                  $data = array(
                   "tamano"=> $tamano
                  );
                  
                  $this->data['rosca_residencial'] = $this->filtro_medidor_model->getTipoRoscaResidencial($data);
                  $vista = $this->load->view('vistas/ajax_lista_rosca_residencial',array(),true);
                  echo $vista;              
                  
              }
              
              public function lista_material_residencial()
              {
                  $this->load->model(array('filtro_medidor_model'));                  
                  $tamano = ($this->input->post('tamano') != null)? $this->input->post('tamano') : '';
                  $rosca = ($this->input->post('rosca') != null)? $this->input->post('rosca') : '';
                  $data = array(
                   "tamano"=> $tamano,
                   "rosca"=>$rosca   
                  );
                  
                  $this->data['material_residencial'] = $this->filtro_medidor_model->getMaterialResidencial($data);
                  $vista = $this->load->view('vistas/ajax_lista_material_residencial',array(),true);
                  echo $vista;  
              }
              
              public function lista_tipo_medidor_residencial()
               {
                  $this->load->model(array('filtro_medidor_model'));                  
                  $tamano = ($this->input->post('tamano') != null)? $this->input->post('tamano') : '';
                  $rosca = ($this->input->post('rosca') != null)? $this->input->post('rosca') : '';
                  $material = ($this->input->post('material') != null)? $this->input->post('material') : '';
                  $data = array(
                   "tamano"=> $tamano,
                   "rosca"=>$rosca,
                   "material"=>$material                     
                  );
                  
                  $this->data['medidor_residencial'] = $this->filtro_medidor_model->getMedidorResidencial($data);
                  $vista = $this->load->view('vistas/ajax_lista_medidor_residencial',array(),true);
                  echo $vista;  
              }
              
              public function product_residencial()
              {
                  $this->load->model(array('filtro_medidor_model'));                  
                  $tamano = ($this->input->post('tamano') != null)? $this->input->post('tamano') : '';
                  $rosca = ($this->input->post('rosca') != null)? $this->input->post('rosca') : '';
                  $material = ($this->input->post('material') != null)? $this->input->post('material') : '';
                  $medidor = ($this->input->post('medidor') != null)? $this->input->post('medidor') : '';
                  $data = array(
                   "tamano"=> $tamano,
                   "rosca"=>$rosca,
                   "material"=>$material,
                   "medidor"=>$medidor
                  );
                  
                  $this->data['medidores'] = $this->filtro_medidor_model->getProductoResidencial($data);
                  $this->data['dollar'] = $this->productos_model->get_dollar();
                  $vista = $this->load->view('vistas/ajax_tienda_medidores_prod',array(),true);
                  echo $vista;  
                  
              }
              /*MEDIDOR COMERCIAL*/
              public function lista_rosca_comercial()
              {
                  $this->load->model(array('filtro_medidor_model'));                  
                  $tamano = ($this->input->post('tamano') != null)? $this->input->post('tamano') : '';                 
                  $data = array(
                   "tamano"=> $tamano
                  );                  
                  $this->data['rosca_comercial'] = $this->filtro_medidor_model->getTipoRoscaComercial($data);
                  $vista = $this->load->view('vistas/ajax_lista_rosca_comercial',array(),true);
                  echo $vista;              
                  
              }
               public function lista_material_comercial()
              {
                  $this->load->model(array('filtro_medidor_model'));                  
                  $tamano = ($this->input->post('tamano') != null)? $this->input->post('tamano') : '';
                  $rosca = ($this->input->post('rosca') != null)? $this->input->post('rosca') : '';
                  $data = array(
                   "tamano"=> $tamano,
                   "rosca"=>$rosca   
                  );
                  
                  $this->data['material_comercial'] = $this->filtro_medidor_model->getMaterialComercial($data);
                  $vista = $this->load->view('vistas/ajax_lista_material_comercial',array(),true);
                  echo $vista;  
              }
              
              public function lista_tipo_medidor_comercial()
               {
                  $this->load->model(array('filtro_medidor_model'));                  
                  $tamano = ($this->input->post('tamano') != null)? $this->input->post('tamano') : '';
                  $rosca = ($this->input->post('rosca') != null)? $this->input->post('rosca') : '';
                  $material = ($this->input->post('material') != null)? $this->input->post('material') : '';
                  $data = array(
                   "tamano"=> $tamano,
                   "rosca"=>$rosca,
                   "material"=>$material                     
                  );
                  
                  $this->data['medidor_comercial'] = $this->filtro_medidor_model->getMedidorComercial($data);
                  $vista = $this->load->view('vistas/ajax_lista_medidor_comercial',array(),true);
                  echo $vista;  
              }
              
              public function product_comercial()
              {
                  $this->load->model(array('filtro_medidor_model'));                  
                  $tamano = ($this->input->post('tamano') != null)? $this->input->post('tamano') : '';
                  $rosca = ($this->input->post('rosca') != null)? $this->input->post('rosca') : '';
                  $material = ($this->input->post('material') != null)? $this->input->post('material') : '';
                  $medidor = ($this->input->post('medidor') != null)? $this->input->post('medidor') : '';
                  $data = array(
                   "tamano"=> $tamano,
                   "rosca"=>$rosca,
                   "material"=>$material,
                   "medidor"=>$medidor
                  );
                  
                  $this->data['medidores'] = $this->filtro_medidor_model->getProductoComercial($data);
                  $this->data['dollar'] = $this->productos_model->get_dollar();
                  $vista = $this->load->view('vistas/ajax_tienda_medidores_prod',array(),true);
                  echo $vista;  
                  
              }
        
        

                /*LOGIN*/
               public function login()
	{
                $this->data['vista'] = 'vistas/login';
                $this->load->view('layouts/inicio');
	}
              
             
               public function inicia_session()
               {
                   
                $this->load->model(array('auth_model'));
                $data = array(
                'email' => $this->input->post('username'),
                'password' => $this->input->post('password'));
                $auth = $this->auth_model->inicia_session($data);
                
                if($auth['valid'] === true){
                    
                    $nombre="";$apellidos ="";$puesto="";$razon_social ="";$rfc="";$estado="";$ciudad="";$ciudad_otra ="";$email="";$zipcode="";
                   $dir="";$nu_dir="";$col_dir="";$lada_tel="";$tel=""; $lada_cel ="";$cel="";$dir_envio ="";$nom2="";$ap2="";$edo2="";
                   $cd2="";$ciudad_otra_2 = "";$email2="";$cp2="";$dir2="";$nu_dir_2="";$col_dir_2="";$lada_tel_2="";$tel2="";$lada_cel_2 ="";$cel2="";
                   $direccion_completa="";
                   
                    $idr =  $auth['datos']->IdCliente;
                    $nombre=$auth['datos']->Nombre;
                    $apellidos =$auth['datos']->Apellidos;
                    $puesto =$auth['datos']->Puesto;
                    $razon_social =$auth['datos']->RazonSocial;
                    $rfc=$auth['datos']->Rfc;                                                             
                    $estado= $auth['datos']->Estado;
                    $ciudad=$auth['datos']->Ciudad;                                                                 
                    $email=$auth['datos']->email;
                    $zipcode=$auth['datos']->CodigoPostal;                                                             
                    $dir=$auth['datos']->Calle;
                    $nu_dir =  $auth['datos']->NoDireccion;
                    $col_dir =  $auth['datos']->ColDireccion;                                                             
                    $lada_tel = $auth['datos']->lada;
                    $tel=$auth['datos']->Telefono;
                    $lada_cel = $auth['datos']->lada2;                                                             
                    $cel_ef= $auth['datos']->Celular;

                    $dir_envio =$auth['datos']->OtraDireccion;
                    $direccion_completa = $dir.' '.$nu_dir.' '.$col_dir.' C.P. '.$zipcode;
                    
                    if($dir_envio == 2){

                    $nom2= $auth['datos']->NombreEnvio;
                    $ap2=$auth['datos']->ApellidoEnvio;
                    $edo2=$auth['datos']->EdoEnvio;
                    $cd2=$auth['datos']->CdEnvio;
                    $email2=$auth['datos']->EmailEnvio;
                    $cp2=$auth['datos']->CodigoPostalEnvio;
                    $dir2= $auth['datos']->CalleEnvio;
                    $nu_dir_2= $auth['datos']->NoDirEnvio;
                    $col_dir_2= $auth['datos']->ColDirEnvio;
                    $lada_tel_2 = $auth['datos']->ladaEnvio;
                    $tel2=$auth['datos']->TelEnvio;                                                 
                    $lada_cel_2 = $auth['datos']->ladaCelEnvio;
                    $cel2=$auth['datos']->CelEnvio;
    //                                                             $idcliente = $auth['datos']->clienteid;
    //                                                             $tipo_us = $auth['datos']->usuariotipoid;
                    $direccion_completa = $dir2.' '.$nu_dir_2.' '.$col_dir_2.' C.P. '.$cp2;
                     }

                     /*CREAR SESSION PARA LOS DATOS DE USUARIO*/  
                     if(isset($_SESSION['paso_dos']))
                         $step_dos = $_SESSION['paso_dos'];
                     else
                         $step_dos = array();
                     if($dir_envio == 2){
                         $step_dos[$idr] = array('nombre' => $nombre,
                                'apellido' => $apellidos,
                                'puesto'=>$puesto,
                                'razon_social'=>$razon_social,
                                'rfc'=>$rfc,
                                'estado' => $estado,
                                'ciudad' => $ciudad,
                                'email' => $email,
                                'zipcode'=>$zipcode,
                                'direccion'=>$dir,
                                'numero_dir'=>$nu_dir,
                                'colonia'=>$col_dir,
                                'lada_tel'=> $lada_tel,
                                'tel'=>$tel,
                                'lada_cel'=>$lada_cel,
                                'cel'=>$cel,
                                'dirEnvio'=>$dir_envio,
                                'nombre_dos' => $nom2,
                                'apellido_dos' => $ap2,
                                'estado_dos' => $edo2,
                                'ciudad_dos' => $cd2,
                                'email_dos' => $email2,
                                'zipcode_dos'=>$cp2,
                                'direccion_dos'=>$dir2,
                                'numero_dir_dos'=>$nu_dir_2,
                                'colonia_dos'=>$col_dir_2,    
                                'lada_tel_dos'=>$lada_tel_2,
                                'tel_dos'=>$tel2,
                                'lada_cel_dos'=>$lada_cel_2,
                                'cel_dos'=>$cel2               
                                );
                         
                     }else{
                         $step_dos[$idr] = array('nombre' => $nombre,
                                'apellido' => $apellidos,
                                'puesto'=>$puesto,
                                'razon_social'=>$razon_social,
                                'rfc'=>$rfc,
                                'estado' => $estado,
                                'ciudad' => $ciudad,
                                'email' => $email,
                                'zipcode'=>$zipcode,
                                'direccion'=>$dir,
                                'numero_dir'=>$nu_dir,
                                'colonia'=>$col_dir,
                                'lada_tel'=> $lada_tel,
                                'tel'=>$tel,
                                'lada_cel'=>$lada_cel,
                                'cel'=>$cel,
                                'dirEnvio'=>$dir_envio
                                );
                     }
                     $_SESSION['paso_dos'] = $step_dos;
                     redirect(base_url(''), 'refresh');                
                }else{
                    $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Usuario o contraseña incorrectos. Por favor ingresar datos correctos.</div>');
                     redirect(base_url('login/1'));
                }
               }
               
               public function begin_session_from_checkout()
                {
                $user = $this->input->post('email_user');
                $pwd = $this->input->post('pwd_user');
                if($user !="" && $pwd != ""){
                $this->load->model(array('auth_model'));
                $data = array(
                'email' => $this->input->post('email_user'),
                'password' => $this->input->post('pwd_user'));
                
                $auth = $this->auth_model->inicia_session($data);
                
                if($auth['valid']){
                //var_dump($auth['datos']);
                /* obtenemmos datos de facturacion y envio si existen*/
                   $nombre="";$apellidos ="";$puesto="";$razon_social ="";$rfc="";$estado="";$ciudad="";$ciudad_otra ="";$email="";$zipcode="";
                   $dir="";$nu_dir="";$col_dir="";$lada_tel="";$tel=""; $lada_cel ="";$cel="";$dir_envio ="";$nom2="";$ap2="";$edo2="";
                   $cd2="";$ciudad_otra_2 = "";$email2="";$cp2="";$dir2="";$nu_dir_2="";$col_dir_2="";$lada_tel_2="";$tel2="";$lada_cel_2 ="";$cel2="";
                   $direccion_completa="";
                   
                    $idr =  $auth['datos']->IdCliente;
                    $nombre=$auth['datos']->Nombre;
                    $apellidos =$auth['datos']->Apellidos;
                    $puesto =$auth['datos']->Puesto;
                    $razon_social =$auth['datos']->RazonSocial;
                    $rfc=$auth['datos']->Rfc;                                                             
                    $estado= $auth['datos']->Estado;
                    $ciudad=$auth['datos']->Ciudad;                                                                 
                    $email=$auth['datos']->email;
                    $zipcode=$auth['datos']->CodigoPostal;                                                             
                    $dir=$auth['datos']->Calle;
                    $nu_dir =  $auth['datos']->NoDireccion;
                    $col_dir =  $auth['datos']->ColDireccion;                                                             
                    $lada_tel = $auth['datos']->lada;
                    $tel=$auth['datos']->Telefono;
                    $lada_cel = $auth['datos']->lada2;                                                             
                    $cel_ef= $auth['datos']->Celular;

                    $dir_envio =$auth['datos']->OtraDireccion;
                    $direccion_completa = $dir.' '.$nu_dir.' '.$col_dir.' C.P. '.$zipcode;
                    
                    if($dir_envio == 2){

                    $nom2= $auth['datos']->NombreEnvio;
                    $ap2=$auth['datos']->ApellidoEnvio;
                    $edo2=$auth['datos']->EdoEnvio;
                    $cd2=$auth['datos']->CdEnvio;
                    $email2=$auth['datos']->EmailEnvio;
                    $cp2=$auth['datos']->CodigoPostalEnvio;
                    $dir2= $auth['datos']->CalleEnvio;
                    $nu_dir_2= $auth['datos']->NoDirEnvio;
                    $col_dir_2= $auth['datos']->ColDirEnvio;
                    $lada_tel_2 = $auth['datos']->ladaEnvio;
                    $tel2=$auth['datos']->TelEnvio;                                                 
                    $lada_cel_2 = $auth['datos']->ladaCelEnvio;
                    $cel2=$auth['datos']->CelEnvio;
    //                                                             $idcliente = $auth['datos']->clienteid;
    //                                                             $tipo_us = $auth['datos']->usuariotipoid;
                    $direccion_completa = $dir2.' '.$nu_dir_2.' '.$col_dir_2.' C.P. '.$cp2;
                     }

                     /*CREAR SESSION PARA LOS DATOS DE USUARIO*/  
                     if(isset($_SESSION['paso_dos']))
                         $step_dos = $_SESSION['paso_dos'];
                     else
                         $step_dos = array();
                     if($dir_envio == 2){
                         $step_dos[$idr] = array('nombre' => $nombre,
                                'apellido' => $apellidos,
                                'puesto'=>$puesto,
                                'razon_social'=>$razon_social,
                                'rfc'=>$rfc,
                                'estado' => $estado,
                                'ciudad' => $ciudad,
                                'email' => $email,
                                'zipcode'=>$zipcode,
                                'direccion'=>$dir,
                                'numero_dir'=>$nu_dir,
                                'colonia'=>$col_dir,
                                'lada_tel'=> $lada_tel,
                                'tel'=>$tel,
                                'lada_cel'=>$lada_cel,
                                'cel'=>$cel,
                                'dirEnvio'=>$dir_envio,
                                'nombre_dos' => $nom2,
                                'apellido_dos' => $ap2,
                                'estado_dos' => $edo2,
                                'ciudad_dos' => $cd2,
                                'email_dos' => $email2,
                                'zipcode_dos'=>$cp2,
                                'direccion_dos'=>$dir2,
                                'numero_dir_dos'=>$nu_dir_2,
                                'colonia_dos'=>$col_dir_2,    
                                'lada_tel_dos'=>$lada_tel_2,
                                'tel_dos'=>$tel2,
                                'lada_cel_dos'=>$lada_cel_2,
                                'cel_dos'=>$cel2               
                                );
                         
                     }else{
                         $step_dos[$idr] = array('nombre' => $nombre,
                                'apellido' => $apellidos,
                                'puesto'=>$puesto,
                                'razon_social'=>$razon_social,
                                'rfc'=>$rfc,
                                'estado' => $estado,
                                'ciudad' => $ciudad,
                                'email' => $email,
                                'zipcode'=>$zipcode,
                                'direccion'=>$dir,
                                'numero_dir'=>$nu_dir,
                                'colonia'=>$col_dir,
                                'lada_tel'=> $lada_tel,
                                'tel'=>$tel,
                                'lada_cel'=>$lada_cel,
                                'cel'=>$cel,
                                'dirEnvio'=>$dir_envio
                                );
                     }
                     $_SESSION['paso_dos'] = $step_dos;
                     $r = '1|'.$email.'|'.$idr.'|'.utf8_encode($nombre).'|'.$apellidos.'|'.$puesto.'|'.$razon_social.'|'.$rfc.'|'.$dir.'|'.$nu_dir.'|'.$col_dir.'|'.$estado.'|'.$ciudad.'|'.$zipcode.'|'.$lada_tel.'|'.$tel.'|'.$lada_cel.'|'.$cel.'|'.$email.'|'.$dir_envio.'|'.$nom2.'|'.$ap2.'|'.$dir2.'|'.$nu_dir_2.'|'.$col_dir_2.'|'.$edo2.'|'.$cd2.'|'.$cp2.'|'.$lada_tel_2.'|'.$tel2.'|'.$lada_cel_2.'|'.$cel2.'|'.$email2.'|'.$direccion_completa;
                     echo $r;
                              }else{
                    //$this->session->set_flashdata('msg_checkout', '<div class="alert alert-danger text-center">Usuario o contraseña incorrectos. Por favor ingresar datos correctos.</div>');
                    echo '0|<div class="alert alert-danger text-center">Usuario o contraseña incorrectos. Por favor ingresar datos correctos.</div>';
                   // redirect(base_url('checkout'),'refresh');
                }
                }  else{
                    $this->session->set_flashdata('msg_checkout', '<div class="alert alert-danger text-center">Usuario o contraseña incorrectos. Por favor ingresar datos correctos.</div>');
                    redirect(base_url('checkout'));     

                }
                
                }
             
              public function log_out()
              {
                    //session_destroy();
                    $_SESSION['login']['nombre'] = null;
                    $_SESSION['login']['id'] = null;
                    redirect(base_url(''), 'refresh');
              }
              
              /*RECUPERA CONTRASENIA */
              public function recupera()
              {        
                $this->data['vista'] = 'vistas/recupera';
                $this->load->view('layouts/inicio');
              }
              
              public function recupera_process()
              {
                  
              $correo =  ($this->input->post('correo') != null)? $this->input->post('correo') : '';
              $user = $this->process_model->get_cliente($correo);
              $id = $user->IdCliente;
              if($id != 0){
              $correo = base64_encode($correo);
              $url = "http://mrwasabiserver.com/webpage/ksh17/api-correo/index.php/api/recupera_password_ksh/{$correo}/{$id}";
              $conexion = curl_init();
              curl_setopt($conexion, CURLOPT_URL,$url);// --- Url
              curl_setopt($conexion, CURLOPT_HTTPGET, TRUE);// --- Petición GET.
              curl_setopt($conexion, CURLOPT_HTTPHEADER,array('Content-Type: application/json'));// --- Cabecera HTTP.
              curl_setopt($conexion, CURLOPT_RETURNTRANSFER, 1);// --- Para recibir respuesta de la conexión. 
              // --- Respuesta
              $respuesta=curl_exec($conexion);
              $pedidos = json_decode($respuesta);
              redirect('http://mrwasabiserver.com/webpage/ksh17', 'refresh');
              }else{
                  $this->session->set_flashdata('msg_recupera', '<div class="alert alert-danger text-center">El correo que usted ingreso no se encuentra registrado.</div>');
                  redirect(base_url('recupera'));     
                  
              }
              
              }
               public function cambia()
              {        
                $this->data['vista'] = 'vistas/cambia';
                $this->load->view('layouts/inicio');
              }
              public function actualiza_pwd(){
                  $id = ($this->input->post('id') != null) ? $this->input->post('id') : '';
                  $pwd = ($this->input->post('pwd') != null) ? $this->input->post('pwd') : '';
                  $pwd_confirm = ($this->input->post('re-password') != null) ? $this->input->post('re-password') : '';
                  
                  if($pwd != "" && $pwd_confirm != ""){
                  if($pwd === $pwd_confirm){
                       $pass = $this->process_model->update_pwd($id,$pwd);
                       if($pass){
                             $this->session->set_flashdata('msg_cambia', '<div class="alert alert-danger text-center">Se ha actualizado su contraseña. Puede iniciar sesión</div>');
                  redirect(base_url('cambia/'.$id));     
                       }
                       
                  }else{
                   $this->session->set_flashdata('msg_cambia', '<div class="alert alert-danger text-center">La contraseña no coincide. Por favor verifica los campos.</div>');
                  redirect(base_url('cambia/'.$id));     
                  }
                  }else{
                   $this->session->set_flashdata('msg_cambia', '<div class="alert alert-danger text-center">Ingrese una contraseña. Los campos vacios son invalidos.</div>');
                  redirect(base_url('cambia/'.$id));     
                  }
                  
              }

              public function elimina_session()
              {
                   if(isset($_SESSION['paso_dos'])){
                       $_SESSION['paso_dos'] = null;                       
                   }
                   redirect(base_url(''), 'refresh');
              }
              
              public function registra_usuario()
              {
                $this->load->model('auth_model');
                $data = array(
                    'email' => $_POST['Email'],
                    'password' => $this->encryption->encrypt($_POST['pwd'])
                );
                $response = $this->auth_model->insert($data);
                if($response){
                 //   echo 'Correcto';
                    $this->session->set_flashdata('msg_register', '<div class="alert alert-danger text-center">Se ha registrado el usuario correctamente.INICIE SESIÓN</div>');
                    redirect(base_url('login/2'));
                 }
              }
              public function perfil()
              {
                /*MUESTRA ESTADOS */  
                $this->load->model(array('checkout_dropdownlist_model'));
                $this->data['estados'] = $this->checkout_dropdownlist_model->getEstados();
                /*MUESTRA CIUDADES*/
                
                /*DATOS DEL  USUARIO*/
                $this->load->model(array('perfil_usuario_update_model'));
                $auth = $this->perfil_usuario_update_model->getDatosUsuario($_SESSION['login']['id']);
                
                if($auth['valid']){
                
                   $this->data['data_perfil'] =  $auth['datos'];
                   $ed = array('edo' => $this->data['data_perfil']->Estado);
                   $this->data['ciudad'] = $this->checkout_dropdownlist_model->getCiudades($ed);   
                   if($this->data['data_perfil']->OtraDireccion == 2){
                   $ed2 = array('edo' => $this->data['data_perfil']->EdoEnvio);
                   $this->data['ciudad_dos'] = $this->checkout_dropdownlist_model->getCiudades($ed2);   
                   }
                }
                
                $this->data['vista'] = 'vistas/perfil';
                $this->load->view('layouts/inicio');
                  
              }
              
              /*ACTUALIZAR INFORMACION DE USUARIO*/
              public function update_perfil(){
                /*MODELO DE PERFIL*/
                $this->load->model(array('perfil_usuario_update_model'));
                $nombre_us= ($this->input->post('nombre') != null)? $this->input->post('nombre') : '';
                $apellidos_us = ($this->input->post('apellidos') != null)? $this->input->post('apellidos') : '';
                $puesto = ($this->input->post('puesto') != null)? $this->input->post('puesto') : '';
                $razon_social = ($this->input->post('razon_social') != null)? $this->input->post('razon_social') : '';
                $rfc = ($this->input->post('rfc') != null)? $this->input->post('rfc') : ''; 
                $dir_ef=($this->input->post('direccion_ef') != null)? $this->input->post('direccion_ef') : '';
                $no_dir=($this->input->post('nu_dir') != null)? $this->input->post('nu_dir') : '';
                $col=($this->input->post('col_dir') != null)? $this->input->post('col_dir') : '';
                $estado_ef= ($this->input->post('edo') != null)? $this->input->post('edo') : '';
                $ciudad_ef=($this->input->post('cd') != null)? $this->input->post('cd') : '';
                $zipcode_ef=($this->input->post('codigopostal') != null)? $this->input->post('codigopostal') : '';
                $lada=($this->input->post('lada') != null)? $this->input->post('lada') : '';
                $tel_ef=($this->input->post('tel') != null)? $this->input->post('tel') : '';
                $ladacel =($this->input->post('lada2') != null)? $this->input->post('lada2') : '';
                $cel=($this->input->post('cel') != null)? $this->input->post('cel') : '';
                $email_ef=($this->input->post('email_ef') != null)? $this->input->post('email_ef') : '';
                
                $dir_envio =($this->input->post('billing') != null)? $this->input->post('billing') : '';
           
                $nom2=($this->input->post('nombre_dos') != null)? $this->input->post('nombre_dos') : '';
                $ap2=($this->input->post('apellidos_dos') != null)? $this->input->post('apellidos_dos') : '';
                $dir2=($this->input->post('dir_dos') != null)? $this->input->post('dir_dos') : '';
                $no_dir_2=($this->input->post('no_dir_2') != null)? $this->input->post('no_dir_2') : '';
                $col_dir_2=($this->input->post('col_dir_2') != null)? $this->input->post('col_dir_2') : '';
                $edo2=($this->input->post('edo2') != null)? $this->input->post('edo2') : '';
                $cd2=($this->input->post('cd2') != null)? $this->input->post('cd2') : '';
                $cp2=($this->input->post('codigopostal_dos') != null)? $this->input->post('codigopostal_dos') : '';             
                $lada_tel_2=($this->input->post('lada_2_1') != null)? $this->input->post('lada_2_1') : '';
                $tel2=($this->input->post('tel_dos') != null)? $this->input->post('tel_dos') : '';
                $lada_cel_2 =($this->input->post('lada_2_2') != null)? $this->input->post('lada_2_2') : '';
                $cel2=($this->input->post('cel_dos') != null)? $this->input->post('cel_dos') : '';
                $email2=($this->input->post('email_dos') != null)? $this->input->post('email_dos') : '';
                /* ENVIAR  INFORMACION DEL USUARIO*/  
                   if($dir_envio == 2){
                 $data = array(
                    "Nombre"=> $nombre_us,
                    "Apellidos" => $apellidos_us,
                    "Puesto"=> $puesto,
                    "RazonSocial"=> $razon_social,
                    "Rfc" => $rfc,
                    "Calle" => $dir_ef,
                    "NoDireccion"=> $no_dir,
                    "ColDireccion" => $col,
                    "Estado" => $estado_ef,
                    "Ciudad"=> $ciudad_ef,
                    "CodigoPostal"=> $zipcode_ef,
                    "lada" => $lada,
                    "Telefono"=> $tel_ef,
                    "lada2"=> $ladacel,
                    "Celular"=> $cel,
                    "OtraDireccion"=>$dir_envio,
                    "NombreEnvio" => $nom2,
                    "ApellidoEnvio"=> $ap2,
                    "CalleEnvio"=> $dir2,
                    "NoDirEnvio" => $no_dir_2,
                    "ColDirEnvio"=> $col_dir_2,
                    "EdoEnvio"  => $edo2,
                    "CdEnvio"=> $cd2,
                    "CodigoPostalEnvio"=> $cp2,
                    "ladaEnvio"=> $lada_tel_2,
                    "TelEnvio" => $tel2,
                    "ladaCelEnvio"=> $lada_cel_2,
                    "CelEnvio" =>$cel2,
                    "EmailEnvio"=> $email2,
                    "FechaAlta"=> date('d/m/Y')
                  );
                   }else{
                       
                    $data = array(
                     "Nombre"=> $nombre_us,
                    "Apellidos" => $apellidos_us,
                    "Puesto"=> $puesto,
                    "RazonSocial"=> $razon_social,
                    "Rfc" => $rfc,
                    "Calle" => $dir_ef,
                    "NoDireccion"=> $no_dir,
                    "ColDireccion" => $col,
                    "Estado" => $estado_ef,
                    "Ciudad"=> $ciudad_ef,
                    "CodigoPostal"=> $zipcode_ef,
                    "lada" => $lada,
                    "Telefono"=> $tel_ef,
                    "lada2"=> $ladacel,
                    "Celular"=> $cel,
                    "OtraDireccion"=>$dir_envio,
                    "FechaAlta"=> date('d/m/Y')
                  );
                   }                 
                $response = $this->perfil_usuario_update_model->update_data($data);
                
                 if($response['valid']){
                 //   echo 'Correcto';
                    $this->session->set_flashdata('msg_update', '<div class="alert alert-danger text-center">Se ha actualizado tu perfil</div>');
                    redirect(base_url('perfil'));
                 }
              }
              
              /*HISTORIAL DE COMPRA DEL USUARIO*/
              public function historial_compra_usuario()
              {
                $this->data['vista'] = 'vistas/historial_compra_usuario';
                $this->load->view('layouts/inicio'); 
                  
              }
              public function pedidos_usuario()
               {
                  
    $start = $this->input->get('start');
    $length = $this->input->get('length');
    $columns = array(
        0 => "t_ventas.IdPedido",
        1 => "tbl_usuarios.Nombre",
        2 => "t_ventas.FechaPedido",
        3 => "t_ventas.TotalCompra",
        4 => "t_ventas.IdPedido"
    );
    
    $search = $this->input->get('search');
    //ORDER
    $order = $this->input->get('order');
    $column_order = (isset($order[0]) && isset($order[0]['column']) ? $columns[$order[0]['column']] : $columns[0]);
    $column_direction = (isset($order[0]) && isset($order[0]['dir']) ? $order[0]['dir'] : 'asc');

    // Create search
    $records = $this->pedidos_model->get_records($start, $length, $column_order, $column_direction, $search);
    $records_all = $this->pedidos_model->get_records(0, 1000, $column_order, $column_direction, $search);
    $total = count($records);
    $arrayOut = array(
            "draw"            => intval($this->input->get('draw')),
            "recordsTotal"    => count($records_all),
            "recordsFiltered" => count($records_all),
            "data"            => array()
    );
    $infoArrayx = array();

foreach ($records as $row)
{
$infoArray = array();
$infoArray[0] = $row->IdPedido;
$infoArray[1] = 'Invitado';
$infoArray[2] = $row->FechaPedido;
$infoArray[3] = $row->TotalCompra;
$bufferButtons = '';
$bufferButtons .= '<div class="btn-group"><a href="'.base_url("pedido_usuario_detalle/{$row->CodigoId}").'" class="btn btn-primary">Ver</a></div>';
$infoArray[4] = '<div class="text-center" style="margin: 0 auto; width: 200px;">' . $bufferButtons . '</div>';
$infoArrayx[] = $infoArray;
}

$arrayOut['data'] = $infoArrayx;
echo json_encode($arrayOut);
  }
  
  public function pedido_usuario_detalle($num = 0)
  {
      //$num = $this->uri->segment(1);
      $this->data['record'] = $this->pedidos_model->get($num);
      
      $this->data['vista'] = 'vistas/pedido_usuario_detalle';
      $this->load->view('layouts/inicio'); 
  }
              
              
              /* CARRITO */
              public function cart()
	{
                $this->data['vista'] = 'vistas/cart';
                $this->load->view('layouts/inicio');
	}
              
              /*SESSION CARRITO*/
              public function crea_sesion_producto()
              {
                  
                $this->load->model(array('existencia_product_model'));
                $data = array(
                'sku' => $this->input->post('skus'),
                'nombre' => $this->input->post('nprod'),
                'precio'=> $this->input->post('prices'),
                'cantidad'=> $this->input->post('cantidad'),
                'tipo_moneda'=> $this->input->post('tipo_moneda'),
                );
              //  var_dump($data);
               $exist = $this->existencia_product_model->existe_prod_compra($data);
              // $this->data['existe'] = $exist;
               //var_dump($exist);
               if($exist['valid']){
                    //redirect($exist['redirect'], 'refresh');               
                    if ($this->input->is_ajax_request()) {
                        
                        if($exist['cotiza'] == 'no'){                            
                             $vista = $this->load->view('vistas/ajax_cart',array(),true);
                             echo json_encode('no|'.$vista.'|'.$exist['dime_cantidad']);   exit(0);
                             
                        }else if($exist['cotiza'] == 'si'){                            
                             $vista = $this->load->view('vistas/ajax_cotizacion',array(),true);
                             echo json_encode('si|'.$vista.'|'.$exist['dime_cantidad']);   exit(0);
                        }
                                                   
                            
                    } else {
                    redirect(base_url('cart'), 'refresh');                    
                    }
                     // $this->data['carro_compra'] = $exist['carro_compra'];
                     // $this->data['vista'] = 'vistas/cart';
                     // $this->load->view('layouts/inicio');

                   }else{
//                    $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Usuario o contraseña incorrectos. Por favor ingresar datos correctos.</div>');

                }
              }
              
              /*SESSION COTIZADOR*/
              public function crea_sesion_cotizador()
              {
                  
                $this->load->model(array('existencia_product_model'));
                $data = array(
                'sku' => $this->input->post('skus'),
                'nombre' => $this->input->post('nprod'),
                'precio'=> $this->input->post('prices'),
                'cantidad'=> $this->input->post('cantidad'),
                'tipo_moneda'=> $this->input->post('tipo_moneda'),
                );
               $exist = $this->existencia_product_model->existe_prod_cotiza($data);
               
               if($exist['valid']){
                    if ($this->input->is_ajax_request()) {
                            $vista = $this->load->view('vistas/ajax_cotizacion',array(),true);
                            echo json_encode($vista);                            
                    } else {
                    redirect(base_url('cart'), 'refresh');                    
                    }
                    
                   }
                   else {
                       
                   }
              }
        
              public function elimina_producto()
              {
                  $sku = $this->uri->segment(3);                  
                  $carro = $_SESSION['carro'];
                  unset($carro[$sku]);
                  $_SESSION['carro'] = $carro;
                  redirect(base_url('cart'), 'refresh');
              }
              
              public function elimina_producto_cotizador()
              {
                $sku = $this->uri->segment(3);                  
                $cotizacion = $_SESSION['cotizacion'];
                unset($cotizacion[$sku]);
                $_SESSION['cotizacion'] = $cotizacion;
                redirect(base_url('cart'), 'refresh');
              }
              
              public function CancelaCompra()
              {
                  // session_destroy();
                  $_SESSION['carro'] = null;
                  $_SESSION['cotizacion'] = null;
                  redirect(base_url(''), 'refresh');
              }
              
              /* CHECKOUT */
              public function checkout()
	{
                $this->load->model(array('checkout_dropdownlist_model'));
                $this->data['estados'] = $this->checkout_dropdownlist_model->getEstados();      
                
                if(isset($_SESSION['login']['nombre'])){
                    if(isset($_SESSION['paso_dos'] )){
                    foreach($_SESSION['paso_dos'] as $value){ 
                        $edo = $value['estado'];
                    }
                    
                 }
                $data = array('edo' => $edo);
                $this->data['ciudad'] = $this->checkout_dropdownlist_model->getCiudades($data);                
                }
                
                $this->data['vista'] = 'vistas/checkout';
                $this->load->view('layouts/inicio');                
	}
        
              public function ciudades()
              {
                  
                $this->load->model(array('checkout_dropdownlist_model'));
                $data = array(
                'edo' => $this->input->post('edo'),
                'select' => $this->input->post('select'),
                'idc'=> $this->input->post('idc')
                  );
                $idc= $this->input->post('idc');
                $this->data['ciudades'] = $this->checkout_dropdownlist_model->getCiudades($data);
                
                if($data['select'] == 1){
                    
                    $data['html'] ='<select id="cd" name="cd" onchange="ValidaCargoEnvio(this.value)" >'
                            . '<option value="">Selecciona ciudad</option>';                    
                    if(!empty($this->data['ciudades'])): foreach($this->data['ciudades'] as $cd):
                   $data['html'] .= '<option value="'. $cd['id'].'"'; if($idc == $cd['id']){ $data['html'] .= 'SELECTED'; } $data['html'] .= '>'.($cd['nombre']).'</option>';
                    endforeach; else:
                        endif;    
                    $data['html'] .= '</select><i></i>';
                    
                }else if($data['select'] == 2){
                    
                    $data['html'] = '<select id="cd2" name="cd2" onchange="ValidaCargoEnvio(this.value)" >'
                            . '<option value="">Selecciona ciudad</option>';
                    if(!empty($this->data['ciudades'])): foreach($this->data['ciudades'] as $cd):
                        $data['html'] .= '<option value="'. $cd['id'].'"'; if($idc == $cd['id']){ $data['html'] .= 'SELECTED'; } $data['html'] .= '>'.($cd['nombre']).'</option>';
                    endforeach; else:
                        endif;    
                    $data['html'] .= '</select><i></i>';
                    
                }else { 
                    
                    $data['html'] .= '<select id="cd" name="cd" onchange="" >
                        <option value="">Selecciona ciudad</option>
                        </select>';                    
                }
                echo(($data['html']));                
              }
              
            /*  public function ciudades_dos($edo='')
              {
                $this->load->model(array('checkout_dropdownlist_model'));
                 $data = array(
                'edo' => $edo);
                $this->data['ciudad'] = $this->checkout_dropdownlist_model->getCiudades($data);
                
               }*/
               
              public function checkout_step_one()
              {
                 if(isset($_POST['shipping']) && $_POST['shipping'] !=""){$invitado= $_POST['shipping'];}               
                 $_SESSION['PASO1'] = 'check';echo 2;exit(0);
              }
              
              public function checkout_step_two()
              {
                  $_SESSION['PASO2'] = 'check';
                   if(!empty($this->data['carro_compra'])): foreach($this->data['carro_compra'] as $cs=>$carro_shop):
                            $subtotal = $carro_shop['cantidad']*$carro_shop['precio'];
                            $suma += $subtotal;
                            endforeach;else:
                                
                            endif;     
                            echo '1|'.$suma;exit(0);
              }
              
              public function checkout_step_three()
              {
                  $envio = $this->input->post('cargo');
                  $suc   =  $this->input->post('suc');                  
                  $_SESSION['PASO3'] = 'check';
                  $_SESSION['CARGO_ENVIO'] = $envio;
                  $_SESSION['SUCURSAL'] = $suc;
                  
              }
              
              public function checkout_step_four()
              {
                   $mpago =  $this->input->post('payment');
                   $suma = 0;
                   if(!empty($this->data['carro_compra'])): foreach($this->data['carro_compra'] as $cs=>$carro_shop):
                    $subtotal = $carro_shop['cantidad']*$carro_shop['precio'];
                    $suma += $subtotal;
                    endforeach;else:                                
                    endif;     
                    if($mpago == 3){
                         if(isset($_SESSION['login']['id'])){ 
                         $this->load->model(array('checkout_dropdownlist_model'));
                         $this->data['credito'] = $this->checkout_dropdownlist_model->getCiudades($_SESSION['login']['id']);
                         }
                          if(!empty($this->data['credito'])): foreach($this->data['credito'] as $credito):                              
                          $credito_disponible = $credito['CreditoDisponible'];                          
                          endforeach; else:                              
                          endif;   
                    if($suma < $credito_disponible){
                        echo '0|Su crédito actual no alcanza cubrir su compra.';exit(0);
                        
                    }else{
                        $_SESSION['metodo_pago'] = $mpago;
                        $_SESSION['PASO4'] = 'check';
                        echo '1|';exit(0);
                        
                    }
                    
                    }
                    $_SESSION['metodo_pago'] = $mpago;
                    $_SESSION['PASO4'] = 'check';
                    echo '1|';exit(0);
                   
              }
              
              public function delete_session_step()
              {
                  
                   $paso =  $this->input->post('step');
                  if($paso == 1){
                    $_SESSION['PASO1'] = null;
                    }else if($paso == 2){
                    $_SESSION['PASO2'] = null;
                    }else if($paso == 3){
                    $_SESSION['PASO3'] = null;
                    }else if($paso == 4){
                    $_SESSION['PASO4'] = null;
                    }else if($paso == 5){
                     $_SESSION['PASO5'] = null;    
                    }
              }

              /*Busqueda pagina*/
              public function busqueda()
	{
                  $this->load->model(array('productos_model'));
                  $search = $this->uri->segment(2);                  
                  $this->data['busqueda'] = $this->productos_model->search($search);       
                  
                  
                  $this->data['vista'] = 'vistas/busqueda';
                  $this->load->view('layouts/inicio');
	}
        
               public function terminos()
	{
                $this->data['vista'] = 'vistas/terminos';
                $this->load->view('layouts/inicio');
	}
        
              public function avisos()
	{
                $this->data['vista'] = 'vistas/avisos';
                $this->load->view('layouts/inicio');
	}
                
              public function procces_compra()
              {
                   
                   require_once(APPPATH."libraries/Openpay/Openpay.php");
                   $openpay = Openpay::getInstance('mvyigtngqjcot65czodu', 'sk_fb0ea604be1d4af6b901675f7f33ffb8'); 
                   //$openpay = Openpay::getInstance('moibhqpusa4fbtyrucrc', 'sk_99a5810b96dc421a81b8c412f4185df7'); 
                   $this->load->model('nueva_venta_model');
                   
                   $tipo =  $this->input->post('tipo_pago');
                   $order = $this->input->post('orden');
                   $charge=array(); 

                    $arrays = array();
                    $total_articulos=0;
                    $subtotal=0;
                    $suma=0;
                    $id_us;
                     if(!empty($this->data['carro_compra'])): foreach($this->data['carro_compra'] as $cs=>$carro_shop):
                         $array= array(
                            "name"=> $carro_shop['nombre'],
                            "sku"=>$cs,
                            "unit_price"=> $carro_shop['precio'],
                            "description"=>$carro_shop['nombre'],
                            "quantity"=>  $carro_shop['cantidad'],
                            "type"=>''
                          );
                    array_push($arrays,$array);
                    $subtotal = $carro_shop['cantidad']*$carro_shop['precio'];
                    $suma += $subtotal;
                    endforeach; else:

                                endif;   
                     if(isset($_SESSION['login']['nombre'])){
                          if(isset($_SESSION['paso_dos'] )){
                              
                               foreach($_SESSION['paso_dos'] as $id_us => $value){ 
                                   
                                $nombre_ef = $value['nombre'];
                                $apellidos_ef  = $value['apellido'];
                                $puesto = $value['puesto'];
                                $razon_social = $value['razon_social'];
                                $rfc=$value['rfc'];
                                $estado_ef = $value['estado'];
                                $ciudad_ef = $value['ciudad'];
                                $email_ef = $value['email'];
                                $zipcode_ef =   $value['zipcode'];       
                                $dir_ef = $value['direccion'];
                                $no_dir = $value['numero_dir'];
                                $col = $value['colonia'];
                                $lada = $value['lada_tel'];
                                $tel_ef = $value['tel'];
                                $ladacel= $value['lada_cel'];
                                $cel = $value['cel'];
                                $dir_envio = $value['dirEnvio'];
                                $direccion_completa = $dir_ef.' '.$no_dir.' '.$col.' C.P. '.$zipcode_ef;
                                if($dir_envio == 2){
                    
                                $nom2 = $value['nombre_dos'];
                                $ap2  = $value['apellido_dos'];
                                $edo2= $value['estado_dos'];
                                $cd2 = $value['ciudad_dos'];                
                                $email2 = $value['email_dos'];
                                $cp2 = $value['zipcode_dos'];
                                $dir2 = $value['direccion_dos'];                                                             
                                $no_dir_2 = $value['numero_dir_dos'];
                                $col_dir_2 = $value['colonia_dos'];
                                $lada_tel_2 = $value['lada_tel_dos'];                                                             
                                $tel2 = $value['tel_dos'];                                                             
                                $lada_cel_2 = $value['lada_cel_dos'];                                                             
                                $cel2 = $value['cel_dos'];

                //                $idcliente = $row['clienteid'];
                //                $tipo_us = $row['usuariotipoid'];
                                $direccion_completa = $dir2.' '.$no_dir_2.' '.$col_dir_2.' C.P. '.$cp2;
                }
             }
         }
  }            
                   
                    if($tipo == 1){
                     /*Fecha*/
                     $fecha = date('Y-m-d');
                    // Store request params in an array            
                   
                try{
                    $nombre_ef="";$apellidos_ef ="";$puesto="";$razon_social ="";$rfc="";$estado_ef="";$ciudad_ef="";$ciudad_otra ="";$email_ef="";$zipcode_ef="";$dir_ef="";
                    $no_dir="";$col="";$lada="";$tel_ef="";$ladacel ="";$cel_ef="";$dir_envio ="";
                    $nom2="";$ap2="";$edo2="";$cd2="";$ciudad_otra_2 = "";$email2="";$cp2="";
                    $dir2="";$no_dir_2="";$col_dir_2="";$lada_tel_2="";$tel2="";$lada_cel_2 ="";$cel2="";$direccion_completa="";            

                  
 $customer = array(
     'name' =>$nombre_ef,
     'last_name' => $apellidos_ef,
     'phone_number' => $tel_ef,
     'email' => $email_ef
    );
 
$chargeRequest = array(
    'method' => 'card',
    'source_id' =>$_POST["token_id"],
    'amount' => (float)$suma, //$_POST['price_tot'], corregir
    'currency' => 'MXN',
    'description' =>  'KSH',
    'order_id' =>$order,
    'device_session_id' => $_POST["deviceIdHiddenFieldName"],
    'customer' => $customer);
$charge = $openpay->charges->create($chargeRequest);

  } catch (OpenpayApiTransactionError $e) {
    error_log('ERROR on the transaction: ' . $e->getMessage() . 
          ' [error code: ' . $e->getErrorCode() . 
          ', error category: ' . $e->getCategory() . 
          ', HTTP code: '. $e->getHttpCode() . 
          ', request ID: ' . $e->getRequestId() . ']', 0);

} catch (OpenpayApiRequestError $e) {
    error_log('ERROR on the request: ' . $e->getMessage(), 0);

} catch (OpenpayApiConnectionError $e) {
    error_log('ERROR while connecting to the API: ' . $e->getMessage(), 0);

} catch (OpenpayApiAuthError $e) {
    error_log('ERROR on the authentication: ' . $e->getMessage(), 0);

} catch (OpenpayApiError $e) {
    error_log('ERROR on the API: ' . $e->getMessage(), 0);

} catch (Exception $e) {
    error_log('Error on the script: ' . $e->getMessage(), 0);
}
   /*ENVIAR FACTURA DE PAGO AL CORREO*/   


   if($charge->status == "completed"){
       
                 /*INSERTAR EL PEDIDO */
                $venta = array(
                    'UsuarioId'=> $id_us,
                    'CodigoId'=> $charge->id,
                    'TipoPagoId'=>1,
                    'CargoEnvioId'=>1,
                    'FechaPedido'=>$fecha,
                    'EstatusPedido'=>1,
                    'TipoUsuarioId'=>2,
                    'TotalCompra'=> $suma                   
                );

                $response = $this->nueva_venta_model->insert_venta($venta);
                if($response){
                     /*INSERTAR EL DETALLE DEL PEDIDO*/
                 if(!empty($this->data['carro_compra'])): foreach($this->data['carro_compra'] as $cs=>$carro_shop):                    
                        $venta_detalle = array(      
                            'PedidoId'=>$response,
                            'ProductoId'=>$cs,
                            'CantidadProducto'=>$carro_shop['cantidad'],
                            'PrecioProducto'=>$carro_shop['precio']
                             );
                      $response_detalle = $this->nueva_venta_model->insert_venta_detalle($venta_detalle);
                     endforeach; else:

                                endif;   
                }
                echo '1|<label> </label><br>'.$charge->status.'|';
                
                } else {
                    echo '0|Su compra no se pudo realizar. '.$e->getMessage();                    
                }
        

        
   }else if($tipo == 2){
       
       /*FECHA EXPIRACION*/
      $fecha = date('Y-m-d'); 
      /*$fecha_exp = strtotime ( '+2 day' , strtotime ( $fecha ) ) ;
      $fecha_exp = date ( 'Y-m-d' , $fecha_exp );             
      */
        
              
                $venta = array(
                    'UsuarioId'=> $id_us,
                    'CodigoId'=> $order,
                    'TipoPagoId'=>2,
                    'CargoEnvioId'=>1,
                    'FechaPedido'=>$fecha,
                    'EstatusPedido'=>1,
                    'TipoUsuarioId'=>2,
                    'TotalCompra'=> $suma                   
                );
                //var_dump($venta);

                $response = $this->nueva_venta_model->insert_venta($venta);
                
                if($response){
                 /*INSERTAR LA VENTA REALIZADA*/
                 if(!empty($this->data['carro_compra'])): foreach($this->data['carro_compra'] as $cs=>$carro_shop):                    
                        $venta_detalle = array(      
                            'PedidoId'=>$response,
                            'ProductoId'=>$cs,
                            'CantidadProducto'=>$carro_shop['cantidad'],
                            'PrecioProducto'=>$carro_shop['precio']
                             );
                      $response_detalle = $this->nueva_venta_model->insert_venta_detalle($venta_detalle);
                     endforeach; else:

                                endif;   
                }
          
          
            /*ENVIAR FACTURA DE PAGO AL CORREO*/
            
             echo '2|<label>Tipo de pago: Transferencia Bancaría<br>Fecha de compra:'.$fecha.'<br>Método de pago: Transferencia Bancaria<br>Descripción: Pago de la orden '.$order.' <br>Total a pagar:'.$suma.'<br></label>';
       
               }else if($tipo == 3){
                     $fecha = date('Y-m-d'); 
                 //  $fecha_exp = strtotime ( '+2 day' , strtotime ( $fecha ));
                 //  $fecha_exp = date ( 'Y-m-d' , $fecha_exp );
            
                $venta = array(
                    'UsuarioId'=> $id_us,
                    'CodigoId'=> $order,
                    'TipoPagoId'=>3,
                    'CargoEnvioId'=>1,
                    'FechaPedido'=>$fecha,
                    'EstatusPedido'=>1,
                    'TipoUsuarioId'=>2,
                    'TotalCompra'=> $suma                   
                );
                $response = $this->nueva_venta_model->insert_venta($venta);
                
                if($response){
                 if(!empty($this->data['carro_compra'])): foreach($this->data['carro_compra'] as $cs=>$carro_shop):                    
                        $venta_detalle = array(      
                            'PedidoId'=>$response,
                            'ProductoId'=>$cs,
                            'CantidadProducto'=>$carro_shop['cantidad'],
                            'PrecioProducto'=>$carro_shop['precio']
                             );
                      $response_detalle = $this->nueva_venta_model->insert_venta_detalle($venta_detalle);
                     endforeach; else:

                                endif;   
                }  
          /*INSERTAR EL PEDIDO *
            
            $sql_in_pedido =" INSERT INTO t_ventas(UsuarioId,CodigoId,TipoPagoId,CargoEnvioId,FechaPedido,EstatusPedido,TipoUsuarioId,TotalCompra)"
                    . "VALUES($id,'".$order."',3,1,'".$fecha."',1,$TipoUser,'".$suma."')";
            $rs_in_pe = mysqli_query($dbc, $sql_in_pedido);
            $id_pedido3 = mysqli_insert_id($dbc);
            /*INSERTAR EL DETALLE DEL PEDIDO*            
            $sql_in_detalle3 = "INSERT INTO t_ventas_detalle(PedidoId,ProductoId,CantidadProducto,PrecioProducto)  VALUES ";
                    
               foreach($carro as $sku => $value){              
                 //  $cant =  $value['cantidad'];                
                   $sql_in_detalle3 .= "($id_pedido3,'$sku',".$value['cantidad'].",'".$value['precio']."'),";
               }               
            $sql_in_detalle3 = substr($sql_in_detalle3,0,strlen($sql_in_detalle3)-1);
            $rs_in_de3 = mysqli_query($dbc, $sql_in_detalle3);*/
            
            /*INSERTAR LA VENTA REALIZADA*/
      //      echo '3|<h3>CLABE: </h3>'.$charge->payment_method->clabe.'<br><h3>Forma de pago: </h3>'.$charge->payment_method->bank.'';            
               echo '3|<label>Tipo de pago: Línea de Credito<br>Fecha de compra:'.$fecha.'<br>Método de pago:  Línea de Credito<br>Descripción: Pago de la orden '.$order.' <br>Total a pagar:'.$suma.'<br></label>';            
         
                   
   }             
                
              }
              
              public function exito()
              {
               $this->load->model(array('checkout_dropdownlist_model'));
                /*DATOS DE COMPRA*/
                $tipopago ="";$nombre_estado ="";$nombre_ciudad ="";$nombre_estado_dos ="";$nombre_ciudad_dos ="";
                $coment =  $this->input->post('comentario');
                $bar_code  =  $this->input->post('barcode');
                $id_envio = $this->input->post('id_envio');
                     
                if($this->input->post('tipopago')== 1){
                    $tipopago = utf8_encode("Tarjeta de Crédito");
                } else if($this->input->post('tipopago') == 2){
                    $tipopago = "Transacción Bancaría";
                }else if($this->input->post('tipopago') == 3){
                    $tipopago = utf8_encode("Línea de Crédito");
                }
                /*DATOS DE CLIENTE*/
               if(isset($_SESSION['login']['nombre'])){          
                          if(isset($_SESSION['paso_dos'] )){   
                              
                               foreach($_SESSION['paso_dos'] as $id_us => $value){ 
                                   
                                $nombre_ef = $value['nombre'];
                                $apellidos_ef  = $value['apellido'];
                                $puesto = $value['puesto'];
                                $razon_social = $value['razon_social'];
                                $rfc=$value['rfc'];
                                $estado_ef = $value['estado'];
                                $ciudad_ef = $value['ciudad'];
                                $email_ef = $value['email'];
                                $zipcode_ef =   $value['zipcode'];       
                                $dir_ef = $value['direccion'];
                                $no_dir = $value['numero_dir'];
                                $col = $value['colonia'];
                                $lada = $value['lada_tel'];
                                $tel_ef = $value['tel'];
                                $ladacel= $value['lada_cel'];
                                $cel = $value['cel'];
                                $dir_envio = $value['dirEnvio'];
                                $direccion_completa = $dir_ef.' '.$no_dir.' '.$col.' C.P. '.$zipcode_ef;
                                if($dir_envio == 2){
                    
                                $nom2 = $value['nombre_dos'];
                                $ap2  = $value['apellido_dos'];
                                $edo2= $value['estado_dos'];
                                $cd2 = $value['ciudad_dos'];                
                                $email2 = $value['email_dos'];
                                $cp2 = $value['zipcode_dos'];
                                $dir2 = $value['direccion_dos'];                                                             
                                $no_dir_2 = $value['numero_dir_dos'];
                                $col_dir_2 = $value['colonia_dos'];
                                $lada_tel_2 = $value['lada_tel_dos'];                                                             
                                $tel2 = $value['tel_dos'];                                                             
                                $lada_cel_2 = $value['lada_cel_dos'];                                                             
                                $cel2 = $value['cel_dos'];
                                $direccion_completa = $dir2.' '.$no_dir_2.' '.$col_dir_2.' C.P. '.$cp2;
                }
             }
         }
         
         
             $this->data['est'] = $this->checkout_dropdownlist_model->getEstadoNombre($estado_ef);                    
             $this->data['cds'] = $this->checkout_dropdownlist_model->getCiudadNombre($ciudad_ef);
             if(!empty($this->data['est'])): foreach($this->data['est'] as $edo):
                 $nombre_estado = $edo['nombre'];
                  endforeach;
                      endif;   
                   if(!empty($this->data['cds'])): foreach($this->data['cds'] as $cd):
                       $nombre_ciudad = $cd['nombre'];
                   endforeach;
                       
                       endif;   
            
            
           if($dir_envio == 2){
             $this->data['est2'] = $this->checkout_dropdownlist_model->getEstadoNombre($edo2);         
             $this->data['cds2'] = $this->checkout_dropdownlist_model->getCiudadNombre($cd2);
             if(!empty($this->data['est2'])): foreach($this->data['est2'] as $edo):
                 $nombre_estado_dos = $edo['nombre'];
                  endforeach; else:
                      endif;   
                   if(!empty($this->data['cds2'])): foreach($this->data['cds2'] as $cd):
                       $nombre_ciudad_dos = $cd['nombre'];
                   endforeach;                        
                       endif;   
           }
         
  }                
                $arrays=array();
                $data = array(
                'barcode' => $bar_code,
                'comentario' => $coment,
                'id_envio'=> $id_envio,
                'tipopago'=> $tipopago,
                'estado_nombre' =>$nombre_estado,
                'ciudad_nombre'=>$nombre_ciudad,
                'envio'=>$dir_envio,    
                'estado_nombre_dos'=>$nombre_estado_dos,
                'ciudad_nombre_dos'=>$nombre_ciudad_dos
                         );
                array_push($arrays,$data);
                $this->data['datos_succes'] = $arrays;
                
                /*ENVIO DE E-MAIL*/
                $this->data['vista'] = 'vistas/exito';
                $this->load->view('layouts/inicio');
                
              }



}

//            [IdProducto] => 1
//            [NombreProducto] => Abrazadera Panel Simple 3121 Power Seal
//            [DiametroNominal] => 2
//            [RangoDiametro] => 2: 2.35"-2.63" - 60-67mm
//            [AnchoPanel] => 8
//            [MaterialOreja] => Hierro ductil con epoxica
//            [SKU] => 3121-2X8

  