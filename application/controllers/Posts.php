<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Posts Management class created by CodexWorld
 */
class Posts extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        $this->load->model('post');
        $this->load->library('ajax_pagination');
        $this->perPage = 12;
    }
    
    public function index(){
        $data = array();        
        //total rows count
        $search = $this->uri->segment(2);
        $totalRec = count($this->post->getRows(array(),$search));
       // var_dump($search);
        //pagination configuration
        $config['target']      = '#postList';
        $config['base_url']    = base_url().'posts/ajaxPaginationData';
        $config['total_rows']  = $totalRec;
        $config['per_page']    = $this->perPage;
        $config['filter'] = $this->uri->segment(2);
        $this->ajax_pagination->initialize($config);
        //get the posts data
        $data['posts'] = $this->post->getRows(array('limit'=>$this->perPage),$search);
        //
       
//        echo '<pre>';
//        print_r($data['posts']);
//        echo '</pre>';
        //load the view
        $this->data['vista'] = 'vistas/busqueda';        
        $this->load->view('layouts/inicio', $data);
      //  $this->load->view('layouts/inicio', $data);
        
    }
    
    function ajaxPaginationData($page = 0,$filter=''){
       
        //$page = $this->input->post('page');
        if(!$page){
            $offset = 0;
        }else{
            $offset = $page;
        }        
        //total rows count
        if(!$filter){
        $search = $this->uri->segment(2);
        }else{
            $search = $filter;
        }
        //var_dump($search);
        $totalRec = count($this->post->getRows(array(),$search));
        //pagination configuration
        $config['target']      = '#postList';
        $config['base_url']    = base_url().'/posts/ajaxPaginationData';
        $config['total_rows']  = $totalRec;
        $config['per_page']    = $this->perPage;
        $config['filter'] = $search;
        $this->ajax_pagination->initialize($config);        
        //get the posts data
        $data['posts'] = $this->post->getRows(array('start'=>$offset,'limit'=>$this->perPage),$search);        
        //load the view
        //$this->load->view('vistas/ajax-pagination-data', $data, false);
        //$this->data['vista'] = 'vistas/busqueda';
        $this->load->view('vistas/busqueda', $data);
    }
}