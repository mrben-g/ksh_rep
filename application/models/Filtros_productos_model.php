<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Filtros_productos_model extends CI_Model{

    function getDiametroNominal()
    {
        //$this->db->distinct('DiametroNominal');
        $this->db->select('DiametroNominal');
        $this->db->from('t_producto');
        $this->db->where('DiametroNominal !=',"");
        $this->db->order_by('IdProducto','ASC');
        $this->db->group_by('DiametroNominal');
        $query = $this->db->get();
        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
    }
    
    function getPanel_activo($diam="")
    {
        $this->db->select('t_diametros.PanelId');
        $this->db->from('t_diametros');
        $this->db->like('t_diametros.Nominal', $diam);
        $this->db->order_by('t_diametros.IdDiametro','ASC');
        $this->db->group_by('t_diametros.PanelId');
        $query = $this->db->get();
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
        
    }
    
    function getDiametroNominal_panel($panel=""){
        
        $this->db->select('t_diametros.Nominal');
        $this->db->from('t_diametros');
         if($panel != ""){
                 $sep_panel = explode('-', $panel);
                 $coun_pan = count($sep_panel);
                // var_dump($coun_pan);
                 
                 for($i=0;$i<$coun_pan;$i++){
                     if($i == 0)
                     {
                         $this->db->like('t_diametros.PanelId', $sep_panel[$i]);
                     }else{
                         $this->db->or_like('t_diametros.PanelId', $sep_panel[$i]);
                     }
                 }
            }      
        $this->db->order_by('t_diametros.IdDiametro','ASC');
        $this->db->group_by('t_diametros.Nominal');
        $query = $this->db->get();
        
//             print_r($query);$this->output->enable_profiler(TRUE);
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
    }
    
    function getDiametroNominal_material($material){
        
    }
    
    function getDiametroNominal_rango($cadena='')
    {
       $sep_cad = explode('|', $cadena);
       $diametro = $sep_cad[0];
       $panel =     $sep_cad[1];
       
        $this->db->select('t_diametros.Rango');
        $this->db->from('t_diametros');
           
            if($diametro != ""){
            $this->db->where('t_diametros.Nominal', $diametro);     
            }
              if($panel != ""){
                 $sep_panel = explode('-', $panel);
                 $coun_pan = count($sep_panel);
                 for($i=0;$i<$coun_pan;$i++){
             
                     if($i == 0)
                     {
                          $this->db->where('t_diametros.PanelId', $sep_panel[$i]);
                     }else{
                           $this->db->or_where('t_diametros.PanelId', $sep_panel[$i]);
                     }
                 }
            }      
//             if($panel != ""){
//            $this->db->where('t_diametros.PanelId', $panel);
//            }
        
        $this->db->order_by('t_diametros.IdDiametro','ASC');
//        $this->db->group_by('t_diametros.Nominal');
        $query = $this->db->get();
        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
        
    }
    
    function getRango_de_diametro($diam = ""){
        
        $this->db->select('t_diametros.Rango');
        $this->db->from('t_diametros');
        $this->db->where('t_diametros.Nominal',$diam);
        $this->db->order_by('t_diametros.IdDiametro','ASC');

        $query = $this->db->get();
        // print_r($query);$this->output->enable_profiler(TRUE);
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
        
    }
    
    function getRangoDiametro(){
        
        $this->db->select('RangoDiametro');
        $this->db->from('t_producto');
        $this->db->where('RangoDiametro !=',"");
        $this->db->order_by('IdProducto','ASC');
        $this->db->group_by('RangoDiametro');
        $query = $this->db->get();
        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
    }
     function getDiametro($valor= array())
    {
        
        $this->db->select('*');
        $this->db->from('t_producto');
        $this->db->where('DiametroNominal',$valor['diametros']);
        $this->db->order_by('IdProducto','ASC');
//        $this->db->group_by('DiametroNominal');
        $query = $this->db->get();
        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
    }
      
    /*ABRAZADERA DE CAMPANA*/
    function getDiametroNominalCampana()
    {
        
        $this->db->select('DiametroNominal');
        $this->db->from('t_producto');        
        $this->db->where('DiametroNominal !=',"");
        $this->db->like('Nombre', 'ABRAZ BELL JOINT 3232 POWERSEAL','both');
        $this->db->order_by('IdProducto','ASC');
        $this->db->group_by('DiametroNominal');
        $query = $this->db->get();
        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
    }    
    /*JUNTAS UNIVERSALES*/
    function  getDiametroUniversal()
    {
        $this->db->select('DiametroNominal');
        $this->db->from('t_producto');
        $this->db->where('DiametroNominal !=',"");
        $this->db->like('Nombre', 'Junta','both');
        $this->db->order_by('IdProducto','ASC');
        $this->db->group_by('DiametroNominal');
        $query = $this->db->get();
        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
    }
    
    function getRangoUniversal($diam='')
    {
        $this->db->select('RangoDiametro');
        $this->db->from('t_producto');
        $this->db->where('DiametroNominal !=',"");
        $this->db->like('Nombre', 'Junta','both');
        $this->db->like('DiametroNominal', $diam,'both');
        $this->db->order_by('IdProducto','ASC');
        $this->db->group_by('RangoDiametro');
        $query = $this->db->get();
        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
        
    }
    
    function getTamanoUniversal($rang='')
    {
        $this->db->select('AnchoPanel');
        $this->db->from('t_producto');
        $this->db->where('DiametroNominal !=',"");
        $this->db->like('Nombre', 'Junta','both');
        $this->db->like('RangoDiametro', $rang,'both');
        $this->db->order_by('IdProducto','ASC');
        $this->db->group_by('AnchoPanel');
        $query = $this->db->get();
        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
    }
  
    /*****COPLE DAYTON *****/
    function getDiametroCople()
    {
        $this->db->select('RangoDiametro');
        $this->db->from('t_producto');
        $this->db->where('RangoDiametro !=',"");
        $this->db->like('Nombre', 'COPLE DAYTON PVC ');
        $this->db->or_like('Nombre', 'COPLE DAYTON PVC ','both');
        $this->db->or_like('Nombre', 'COPLE DAYTON PVC ','after');
        $this->db->order_by('IdProducto','ASC');
        $this->db->group_by('RangoDiametro');
        $query = $this->db->get();
        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
        
    }
    
    /**/
    function getRows_d($params = array(),$cadena='')
    {
       $sep_cad = explode('|', $cadena);
       $diametro = $sep_cad[0];
       $panel =     $sep_cad[1];
       $material=   $sep_cad[2];
       $rango=      $sep_cad[3];
//        var_dump($filter);
       
        $this->db->select('*');
        $this->db->from('t_producto');
        
        if(array_key_exists("start",$params) && array_key_exists("limit",$params)){ 
            
            
            if($diametro != ""){
            $this->db->like('t_producto.DiametroNominal', $diametro);     
            }
             if($panel != ""){
            $this->db->like('t_diametros.PanelId', $panel);
            }
//            $this->db->like('DiametroNominal', $filter);
            $this->db->limit($params['limit'],$params['start']);            
            
        }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){            
            
//            $this->db->like('DiametroNominal', $filter);
            if($diametro != ""){
            $this->db->like('t_producto.DiametroNominal', $diametro);     
            }
             if($panel != ""){
            $this->db->like('t_diametros.PanelId', $panel);
            }
            $this->db->limit($params['limit']);  
            
        }else{
            
             if($diametro != ""){
            $this->db->like('t_producto.DiametroNominal', $diametro);     
            }
             if($panel != ""){
            $this->db->like('t_diametros.PanelId', $panel);
            }
           //$this->db->like('DiametroNominal', $filter);        
        }
        
        $this->db->order_by('IdProducto','ASC');
        $query = $this->db->get();
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
    }
     
    function getRows_c($params = array(),$cadena='')
    {
//        var_dump($filter);
        
        $sep_cad = explode('|', $cadena);
        $diametro = $sep_cad[0];
        $panel =     $sep_cad[1];
        $material=   $sep_cad[2];
        $rango=      $sep_cad[3];
        
//        var_dump($panel);
        /*
SELECT P.*, D.Nominal,D.Rango 
FROM t_producto P 
INNER JOIN t_diametros D 
ON (P.RangoDiametro = D.Rango)
INNER JOIN t_materiales M
ON(P.MaterialOreja = M.Material)
INNER JOIN tbl_codigos C
ON (C.SKU = P.SKU)
WHERE P.DiametroNominal = '4' AND D.PanelId='2'*/
        
/*
SELECT t_producto.*,t_diametros.Nominal,t_diametros.Rango,t_materiales.IdMaterial,tbl_codigos.Precio,tbl_codigos.Existencia,tbl_codigos.TipoMoneda
FROM t_producto
LEFT JOIN tbl_codigos ON (t_producto.SKU = tbl_codigos.SKU)
LEFT JOIN t_diametros ON (t_producto.RangoDiametro = t_diametros.Rango)
LEFT JOIN t_materiales ON (t_producto.MaterialOreja = t_materiales.Material);
 *  */        
        
       $this->db->select('t_producto.*,t_diametros.Nominal,t_diametros.Rango,t_materiales.IdMaterial,tbl_codigos.Precio,tbl_codigos.Existencia,tbl_codigos.TipoMoneda');
       $this->db->from('t_producto');
       $this->db->join('tbl_codigos', 't_producto.SKU = tbl_codigos.SKU', 'LEFT');
       $this->db->join('t_diametros', 't_producto.RangoDiametro = t_diametros.Rango', 'LEFT');
       $this->db->join('t_materiales', 't_producto.MaterialOreja = t_materiales.Material', 'LEFT');       
        
        if(array_key_exists("start",$params) && array_key_exists("limit",$params)){ 
            
            if($panel != ""){
                 $sep_panel = explode('-', $panel);
                 $coun_pan = count($sep_panel);
                 
                 //var_dump($coun_pan);
                 for($i=0;$i<$coun_pan;$i++){
                 $this->db->or_like('t_diametros.PanelId', $sep_panel[$i]);
                 }
            }
            if($diametro != ""){
            $this->db->like('t_producto.DiametroNominal', $diametro);     
            }
            if($material != ""){
                 $this->db->like('t_materiales.IdMaterial', $material);     
            }
            if($rango != ""){
                 $this->db->like('t_producto.RangoDiametro', $rango,'both');     
            }
            $this->db->limit($params['limit'],$params['start']);            
            
        }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){            
       
            if($panel != ""){
                 $sep_panel = explode('-', $panel);
                 $coun_pan = count($sep_panel);
                   for($i=0;$i<$coun_pan;$i++){
                 $this->db->or_like('t_diametros.PanelId', $sep_panel[$i]);
                 }
                 // $this->db->like('t_diametros.PanelId', $panel);
            }
            if($diametro != ""){
            $this->db->like('t_producto.DiametroNominal', $diametro);
            }
            if($material != ""){
               $this->db->like('t_materiales.IdMaterial', $material);
            }
             if($rango != ""){
                 $this->db->like('t_producto.RangoDiametro', $rango,'both');     
            }
            $this->db->limit($params['limit']);  
            
        }else{
          
        if($panel != ""){
              $sep_panel = explode('-', $panel);
                 $coun_pan = count($sep_panel);
                   for($i=0;$i<$coun_pan;$i++){
                 $this->db->or_like('t_diametros.PanelId', $sep_panel[$i]);
                 }
          //$this->db->like('t_diametros.PanelId', $panel);    
            }
        if($diametro != ""){
        $this->db->like('t_producto.DiametroNominal', $diametro);
        }
        if($material != ""){
                $this->db->like('t_materiales.IdMaterial', $material);
            }
        if($rango != ""){
                 $this->db->like('t_producto.RangoDiametro', $rango,'both');     
         }
            
        }
                
        $this->db->order_by('t_producto.IdProducto','ASC');
        $query = $this->db->get();
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
        
    }
    
    /*UNIVERSALES*/
    function getRows_uni($params = array(),$cadena='')
    {
        $sep_cad = explode('|', $cadena);
        $tamano = $sep_cad[0];
        $material=   $sep_cad[1];
        $rango=      $sep_cad[2];
 
        
       $this->db->select('t_producto.*,t_diametros.Nominal,t_diametros.Rango,t_materiales.IdMaterial,tbl_codigos.Precio,tbl_codigos.Existencia,tbl_codigos.TipoMoneda');
       $this->db->from('t_producto'); 
       $this->db->join('tbl_codigos', 't_producto.SKU = tbl_codigos.SKU', 'LEFT');
       $this->db->join('t_diametros', 't_producto.RangoDiametro = t_diametros.Rango', 'LEFT');
       $this->db->join('t_materiales', 't_producto.MaterialOreja = t_materiales.Material', 'LEFT');       
       $this->db->like('t_producto.Nombre', 'Junta','both');
       
        if(array_key_exists("start",$params) && array_key_exists("limit",$params)){ 

            if($tamano != ""){
            $this->db->like('t_producto.AnchoPanel', $tamano);     
            }
            if($material != ""){
                 $this->db->like('t_materiales.IdMaterial', $material);     
            }
            if($rango != ""){
                 $this->db->like('t_producto.RangoDiametro', $rango,'both');     
            }
            $this->db->limit($params['limit'],$params['start']);            
            
        }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){            
       
            if($tamano != ""){
            $this->db->like('t_producto.AnchoPanel', $tamano);     
            }
            if($material != ""){
               $this->db->like('t_materiales.IdMaterial', $material);
            }
             if($rango != ""){
                 $this->db->like('t_producto.RangoDiametro', $rango,'both');     
            }
            $this->db->limit($params['limit']);  
            
        }else{
          
         if($tamano != ""){
            $this->db->like('t_producto.AnchoPanel', $tamano);     
            }
        if($material != ""){
                $this->db->like('t_materiales.IdMaterial', $material);
            }
        if($rango != ""){
                 $this->db->like('t_producto.RangoDiametro', $rango,'both');     
         }
            
        }
                
        $this->db->order_by('t_producto.IdProducto','ASC');
        $query = $this->db->get();
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
        
    }
    /*BELL JOINT*/
    function getRows_joint($params = array(),$diametro='')
    {
        
        //var_dump($diametro);
       $this->db->select('t_producto.*,t_diametros.Nominal,t_diametros.Rango,t_materiales.IdMaterial,tbl_codigos.Precio,tbl_codigos.Existencia,tbl_codigos.TipoMoneda');
       $this->db->from('t_producto'); 
       $this->db->join('tbl_codigos', 't_producto.SKU = tbl_codigos.SKU', 'LEFT');
       $this->db->join('t_diametros', 't_producto.RangoDiametro = t_diametros.Rango', 'LEFT');
       $this->db->join('t_materiales', 't_producto.MaterialOreja = t_materiales.Material', 'LEFT');       
       $this->db->where('Nombre', 'ABRAZ BELL JOINT 3232 POWERSEAL','both');
       
        if(array_key_exists("start",$params) && array_key_exists("limit",$params)){ 

           if($diametro != ""){
            $this->db->like('t_producto.DiametroNominal', (int)$diametro);     
            }
            $this->db->limit($params['limit'],$params['start']);            
            
        }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){            
       
          if($diametro != ""){
            $this->db->like('t_producto.DiametroNominal', (int)$diametro);     
            }
            $this->db->limit($params['limit']);  
            
        }else{
            
        if($diametro != ""){
                 $this->db->like('t_producto.DiametroNominal' ,(int)$diametro);     
        }
            
        }
                
        $this->db->order_by('t_producto.IdProducto','ASC');
        $query = $this->db->get();
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
        
    }
    /*COPLE DAYTON*/
    function getRows_cople($params = array(),$rango='')
    {
         //var_dump($diametro);
       $this->db->select('t_producto.*,t_diametros.Nominal,t_diametros.Rango,t_materiales.IdMaterial,tbl_codigos.Precio,tbl_codigos.Existencia,tbl_codigos.TipoMoneda');
       $this->db->from('t_producto'); 
       $this->db->join('tbl_codigos', 't_producto.SKU = tbl_codigos.SKU', 'LEFT');
       $this->db->join('t_diametros', 't_producto.RangoDiametro = t_diametros.Rango', 'LEFT');
       $this->db->join('t_materiales', 't_producto.MaterialOreja = t_materiales.Material', 'LEFT');       
       $this->db->where('t_producto.RangoDiametro !=',"");
       $this->db->like('t_producto.Nombre', 'COPLE DAYTON PVC ');
//       $this->db->or_like('t_producto.NombreProducto', 'COPLE DAYTON PVC ','both');
//       $this->db->or_like('t_producto.NombreProducto', 'COPLE DAYTON PVC ','after');
       
        if(array_key_exists("start",$params) && array_key_exists("limit",$params)){ 

            if($rango != ""){
                 $this->db->like('t_producto.RangoDiametro', $rango);     
            }
            $this->db->limit($params['limit'],$params['start']);            
            
        }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){            
       
            if($rango != ""){
                 $this->db->like('t_producto.RangoDiametro', $rango);     
            }
            $this->db->limit($params['limit']);  
            
        }else{
            
           if($rango != ""){
                 $this->db->like('t_producto.RangoDiametro="'.$rango.'"');     
            }
            
        }
                
        $this->db->order_by('t_producto.IdProducto','ASC');
        $query = $this->db->get();
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
        
    }
    
    
    
}