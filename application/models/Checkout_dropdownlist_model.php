<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Checkout_dropdownlist_model extends CI_Model{
    
      function getEstados()
    {
        $this->db->select('id,nombre');
        $this->db->from('tbl_estados');
        $this->db->where('nombre !=',"");
        $this->db->order_by('id','ASC');
        $query = $this->db->get();
        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
    }
    
    function getCiudades($data = array())
    {
        $this->db->select('*');
        $this->db->from('tbl_municipios');
        $this->db->where('estado_id',$data['edo']);
        $this->db->order_by('id','ASC');
        //$this->db->group_by('');
        $query = $this->db->get();        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
    }
  
    function getCredito($id)
    {
        $this->db->select('*');
        $this->db->from('tbl_credito');
        $this->db->where('UsuarioId',$id);
        //$this->db->order_by('id','ASC');
        //$this->db->group_by('');
        $query = $this->db->get();        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
    }
    
    function getEstadoNombre($id='')
    {
        $this->db->select('nombre');
        $this->db->from('tbl_estados');
        $this->db->where('id',$id);
        $this->db->order_by('id','ASC');
        $query = $this->db->get();
        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
    }
    
    function getCiudadNombre($id='')
    {
        $this->db->select('nombre');
        $this->db->from('tbl_municipios');
        $this->db->where('id',$id);
        $this->db->order_by('id','ASC');
        //$this->db->group_by('');
        $query = $this->db->get();        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
        
    }
}