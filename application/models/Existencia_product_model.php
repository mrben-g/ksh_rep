<?php

class Existencia_product_model extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }
  
  public function existe_prod_compra($data= array())
  {
    // $this->output->enable_profiler(TRUE);
    $valid=false;
    $result = array();
    $product_cart = array();
    $prod = $this->check_prod($data);
    $bit='';    
    $cantidad_en_cotizacion ='';
//   var_dump($prod);
   // mayor > menor <
   if (is_null($prod)) {
      $valid = false;
      $mensaje = 'No existe el producto';      
    }else{
        if($data['tipo_moneda'] != "no"){       
            if($data['cantidad'] <= $prod->Existencia ){
                
              $exist_cot = $this->existe_en_cotizacion($prod->SKU);
              
              $cantidad_en_cotizacion = $exist_cot['canti'];
              
            if($exist_cot['bit'] == 1){                
             $cart = $this->add_cotizador($prod->SKU,$prod->Nombre,$data['precio'],($exist_cot['canti']-1),'0','no');
             $bit = 'si';             
            }else{
            $cart = $this->add_cart($prod->SKU,$prod->Nombre,$data['precio'],$data['cantidad'],$prod->Existencia,$prod->TipoMoneda);
            $bit = 'no';
            }
            
            }else{            
             $cart = $this->add_cotizador($prod->SKU,$prod->Nombre,$data['precio'],$data['cantidad'],'0','no');
             $bit = 'si';
        }
        }else{            
       $cart = $this->add_cotizador($prod->SKU,$prod->Nombre,$data['precio'],$data['cantidad'],'0','no');
        $bit = 'si';
        }
        
        $result['carro_compra'] = $cart;
        $valid = true;
        $mensaje = '';
        $redirect = base_url('cart');
        $result['redirect'] = $redirect;     
        $result['cotiza'] = $bit;
     }
     
    $result['valid'] = $valid;
    $result['dime_cantidad'] = $cantidad_en_cotizacion;
    $result['mensaje'] = $mensaje;
    return $result;    
  }

  public function existe_prod_cotiza($data= array())
  {
      
    $valid=false;
    $result = array();
    $product_cart = array();
    $prod = $this->check_prod($data);
    $bit='';
    
//   var_dump($data['cantidad']);
   
   if (is_null($prod)) {
      $valid = false;
      $mensaje = 'No existe el producto';      
    }else{
        
        if($data['tipo_moneda'] != "no"){               
           if($data['cantidad'] <= $prod->Existencia ){
         $cart = $this->add_cart($prod->SKU,$prod->Nombre,$data['precio'],$data['cantidad'],$prod->Existencia,$prod->TipoMoneda);     
          $bit = 'no';
            }else{            
          $cart = $this->add_cotizador($prod->SKU,$prod->Nombre,$data['precio'],$data['cantidad'],'0','no');
           $bit = 'si';
        }
        }else{
          $cart = $this->add_cotizador($prod->SKU,$prod->Nombre,$data['precio'],$data['cantidad'],'0','no');
           $bit = 'si';
        }
        $result['carro_compra'] = $cart;
        $valid = true;
        $mensaje = '';
        $redirect = base_url('cart');
        $result['redirect'] = $redirect;   
        $result['cotiza'] = $bit;

     }
     
    $result['valid'] = $valid;
    $result['mensaje'] = $mensaje;
    return $result;
  }
  
     public function check_prod($data = array())
  {
         
    $this->db->select('t_producto.*,tbl_codigos.Precio,tbl_codigos.Existencia,tbl_codigos.TipoMoneda');
    $this->db->from('t_producto');
    $this->db->join('tbl_codigos', 't_producto.SKU = tbl_codigos.SKU', 'LEFT');
    $this->db->where('t_producto.SKU', $data['sku']);    
    $query = $this->db->get();
     if($query->num_rows() > 0 || $query->num_rows() != 'NULL'){         
     return $query->row();     
     
     }else{
         
    $this->db->select('tbl_contra_incendio.*,tbl_codigos.Precio,tbl_codigos.Existencia,tbl_codigos.TipoMoneda');
    $this->db->from('tbl_contra_incendio');
    $this->db->join('tbl_codigos', 'tbl_contra_incendio.SKU = tbl_codigos.SKU', 'LEFT');
    $this->db->like('tbl_contra_incendio.SKU',($data['sku']),'both');        
    $query = $this->db->get();
//    print_r($query);$this->output->enable_profiler(TRUE);
   // var_dump($data['sku']);
    if($query->num_rows() > 0){
     return $query->row();
    
    }else{
         $this->db->select('t_diametro_derivacion.*,tbl_codigos.Precio,tbl_codigos.Existencia,tbl_codigos.TipoMoneda');
    $this->db->from('t_diametro_derivacion');
    $this->db->join('tbl_codigos', 't_diametro_derivacion.SKU = tbl_codigos.SKU', 'LEFT');
    $this->db->like('t_diametro_derivacion.SKU',($data['sku']),'both');        
    $query = $this->db->get();
    if($query->num_rows() > 0){
     return $query->row();
    
    }else{
 
        $this->db->select('t_alcantarillado.*,tbl_codigos.Precio,tbl_codigos.Existencia,tbl_codigos.TipoMoneda');
    $this->db->from('t_alcantarillado');
    $this->db->join('tbl_codigos', 't_alcantarillado.SKU = tbl_codigos.SKU', 'LEFT');
    $this->db->like('t_alcantarillado.SKU',($data['sku']),'both');        
    $query = $this->db->get();
    if($query->num_rows() > 0){
     return $query->row();
    
    }else{
    $this->db->select('t_filtros.*,tbl_codigos.Precio,tbl_codigos.Existencia,tbl_codigos.TipoMoneda');
    $this->db->from('t_filtros');
    $this->db->join('tbl_codigos', 't_filtros.SKU = tbl_codigos.SKU', 'LEFT');
    $this->db->like('t_filtros.SKU',($data['sku']),'both');        
    $query = $this->db->get();
        if($query->num_rows() > 0){
     return $query->row();
    
    }else{
    $this->db->select('t_medidores.*,tbl_codigos.Precio,tbl_codigos.Existencia,tbl_codigos.TipoMoneda');
    $this->db->from('t_medidores');
    $this->db->join('tbl_codigos', 't_medidores.SKU = tbl_codigos.SKU', 'LEFT');
    $this->db->like('t_medidores.SKU',($data['sku']),'both');        
    $query = $this->db->get();
      if($query->num_rows() > 0){
     return $query->row();
    
    }else{
        
    }
   }
    }
        
    }      
   }
  } 
  }
  
    function existe_en_cotizacion($sku= "")
    {
        $bit=0;
        $canti ="";
        if(isset($_SESSION['cotizacion']) && !empty($_SESSION['cotizacion'])){
           
            $cotizacion = $_SESSION['cotizacion'];
             foreach($cotizacion as $ids => $value){ 
                if($ids == $sku){                  
                    $bit = 1;
                    $canti = $value['cantidad'];
                }

                }
        
        }
        return array('bit' => $bit, 'canti' => $canti);
    }

  public function add_cart($sku="",$nombre="",$precio="", $cantidad="",$existencia="",$tipo_moneda="")
  {
      
      $cantidad_agregar = 1;
//      $precio="0";
//      $existencia="0";
//      $tipo_moneda="no";
//      $product_cart = array();
//      $product_cart->SKU
//        var_dump($cantidad);
      if(isset($_SESSION['carro']))
          {
          // Actualizar carrito
          
          $carro = $_SESSION['carro'];          
           foreach($carro as $ids => $value){ 
              if($ids == $sku){                  
                  $cantidad_agregar = $cantidad;
                  }
              }
                           
                 //'img_thumb'=>$thumb,
           
                 $carro[$sku] = array(
                     'sku'=>$sku,
                     'nombre' => $nombre,
                     'precio' => $precio,
                     "existencia"=>$existencia,
                     "tipo_moneda"=>$tipo_moneda,
                     'cantidad' => $cantidad_agregar                                            
                            );
                 
                  $_SESSION['carro'] = $carro;
                  
          }else{
              
              // Agregar producto nuevo
                 $carro = array();
                 //'img_thumb'=>$thumb,
                 $carro[$sku] = array(
                     'sku'=>$sku,
                     'nombre' => $nombre,
                     'precio' => $precio,
                     "existencia"=>$existencia,
                     "tipo_moneda"=>$tipo_moneda,
                     'cantidad' => $cantidad_agregar                                            
                            );
                 $_SESSION['carro'] = $carro;
                  
          }
          return $_SESSION['carro'];
		
	}


 public function add_cotizador($sku="",$nombre="",$precio="", $cantidad="",$existencia="",$tipo_moneda="")
  {
      
      $cantidad_agregar = 1;
      
      if(isset($_SESSION['cotizacion']))
          {
          // Actualizar carrito
          $cotizacion = $_SESSION['cotizacion'];
           foreach($cotizacion as $ids => $value){ 
              if($ids == $sku){                  
                  $cantidad_agregar = $cantidad;
                  }
              }
                 //'img_thumb'=>$thumb,
           
                 $cotizacion[$sku] = array(
                     'sku'=>$sku,
                     'nombre' => $nombre,
                     'precio' => $precio,
                     "existencia"=>$existencia,
                     "tipo_moneda"=>$tipo_moneda,
                     'cantidad' => $cantidad_agregar                                            
                            );
                 
                  $_SESSION['cotizacion'] = $cotizacion;
                  
          }else{
              
              // Agregar producto nuevo
                 $cotizacion = array();
                 //'img_thumb'=>$thumb,
                 $cotizacion[$sku] = array(
                     'sku'=>$sku,
                     'nombre' => $nombre,
                     'precio' => $precio,
                     "existencia"=>$existencia,
                     "tipo_moneda"=>$tipo_moneda,
                     'cantidad' => $cantidad_agregar                                            
                            );
                 $_SESSION['cotizacion'] = $cotizacion;
                  
          }
          return $_SESSION['cotizacion'];
		
	}
        
         
  
  
}

