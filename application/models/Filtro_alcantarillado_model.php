<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Filtro_alcantarillado_model extends CI_Model{
    
    /*BROCALES*/
    function getPesoBrocales()
    {
        $this->db->select('Peso');
        $this->db->from('t_alcantarillado');
        $this->db->where('Peso !=',"");
        $this->db->like('Material', 'FOFO','both');
        $this->db->order_by('id','ASC');
        $this->db->group_by('Peso');
        $query = $this->db->get();
        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
    }
    
    function getProductoAlcantarillado($data = array())
    {        
       $this->db->select('t_alcantarillado.*,tbl_codigos.Precio,tbl_codigos.Existencia,tbl_codigos.TipoMoneda');
       $this->db->from('t_alcantarillado');       
       $this->db->join('tbl_codigos', 't_alcantarillado.SKU = tbl_codigos.SKU', 'LEFT');
       $this->db->like('t_alcantarillado.Material', 'FOFO','both');
       
       if($data['peso'] != ""){
           $this->db->like('t_alcantarillado.Peso', $data['peso'] ,'both');
       } 
       
        $this->db->order_by('t_alcantarillado.id','ASC');
        $query = $this->db->get();
        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
        
    }
  
    /*ESCALONES*/
     function getMaterialEscalones()
    {
        $this->db->select('Material');
        $this->db->from('t_alcantarillado');
        $this->db->where('Material !=',"");
        $this->db->like('Nombre', 'ESCALON','both');
        $this->db->order_by('id','ASC');
        $this->db->group_by('Material');
        $query = $this->db->get();
        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
    }
    
    function getProductoEscalones($data = array())
    {        
       $this->db->select('t_alcantarillado.*,tbl_codigos.Precio,tbl_codigos.Existencia,tbl_codigos.TipoMoneda');
       $this->db->from('t_alcantarillado');       
       $this->db->join('tbl_codigos', 't_alcantarillado.SKU = tbl_codigos.SKU', 'LEFT');
//       $this->db->like('t_alcantarillado.Material', 'FOFO','both');
       
       if($data['material'] != ""){
           $this->db->like('t_alcantarillado.Material', $data['material'] ,'both');
       } 
       
        $this->db->order_by('t_alcantarillado.id','ASC');
        $query = $this->db->get();
        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
        
    }
   /*ANTI RETORNO*/
    
    function getDiamValvulaAntiRetorno()
      {
        $this->db->select('Peso');
        $this->db->from('t_alcantarillado');
        $this->db->where('Peso !=',"");
        $this->db->like('Nombre', 'RETORNO','both');
        $this->db->order_by('id','ASC');
        $this->db->group_by('Peso');
        $query = $this->db->get();
        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
    }
    
    function getProductoAntiRetorno($data = array())
    {        
       $this->db->select('t_alcantarillado.*,tbl_codigos.Precio,tbl_codigos.Existencia,tbl_codigos.TipoMoneda');
       $this->db->from('t_alcantarillado');       
       $this->db->join('tbl_codigos', 't_alcantarillado.SKU = tbl_codigos.SKU', 'LEFT');
       $this->db->like('Nombre', 'RETORNO','both');
       
       if($data['diametro'] != ""){
           $this->db->like('t_alcantarillado.Peso', $data['diametro'] ,'both');
       } 
       
        $this->db->order_by('t_alcantarillado.id','ASC');
        $query = $this->db->get();
        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
        
    }
    
    /*MANGAs*/
    function getTamanoMangas()
    {
        $this->db->select('Peso');
        $this->db->from('t_alcantarillado');
        $this->db->where('Peso !=',"");
        $this->db->like('Nombre', 'MANGA','both');
        $this->db->order_by('id','ASC');
        $this->db->group_by('Peso');
        $query = $this->db->get();
        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
    }
    
    function getProductoManga($data = array())
    {        
       $this->db->select('t_alcantarillado.*,tbl_codigos.Precio,tbl_codigos.Existencia,tbl_codigos.TipoMoneda');
       $this->db->from('t_alcantarillado');       
       $this->db->join('tbl_codigos', 't_alcantarillado.SKU = tbl_codigos.SKU', 'LEFT');
       $this->db->like('Nombre', 'MANGA','both');
       
       if($data['tamano'] != ""){
           $this->db->like('t_alcantarillado.Peso', $data['tamano'],'after');
       } 
       
        $this->db->order_by('t_alcantarillado.id','ASC');             
        $query = $this->db->get();
        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
        
    }
    
    
    
}

?>