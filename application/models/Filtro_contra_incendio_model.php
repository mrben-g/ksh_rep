<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Filtro_contra_incendio_model extends CI_Model{

    function getTamanoValvula()
    {
        $this->db->select('TamanoValvula');
        $this->db->from('tbl_contra_incendio');
        $this->db->where('TamanoValvula !=',"");
        $this->db->order_by('TamanoValvula','ASC');
        $this->db->group_by('TamanoValvula');
        $query = $this->db->get();
        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
    }
    
    function getNumeroVias()
    {
        $this->db->select('NumeroVias');
        $this->db->from('tbl_contra_incendio');
        $this->db->where('NumeroVias !=',"");
        $this->db->order_by('NumeroVias','ASC');
        $this->db->group_by('NumeroVias');
        $query = $this->db->get();
        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
    }
    
    function getLargoBarril()
    {
        $this->db->select('LargoBarril');
        $this->db->from('tbl_contra_incendio');
        $this->db->where('LargoBarril !=',"");
        $this->db->like('Nombre', 'HIDRANTE 3V','both');
        $this->db->or_like('Nombre', 'HIDRANTE 2V','both');
        $this->db->order_by('LargoBarril','ASC');
        $this->db->group_by('LargoBarril');
        $query = $this->db->get();
        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;       
    }
    
    function getTipoJunta()
    {
        $this->db->select('TipoJunta');
        $this->db->from('tbl_contra_incendio');
        $this->db->where('TipoJunta !=',"");
        $this->db->like('Nombre', 'HIDRANTE 3V','both');
        $this->db->or_like('Nombre', 'HIDRANTE 2V','both');
        $this->db->order_by('TipoJunta','ASC');
        $this->db->group_by('TipoJunta');
        $query = $this->db->get();
        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;     
        
    }
    
    function getAperturaValvula()
    {
        $this->db->select('AperturaValvula');
        $this->db->from('tbl_contra_incendio');
        $this->db->where('AperturaValvula !=',"");
        $this->db->like('Nombre', 'KIT EXTENSION','both');        
        $this->db->order_by('AperturaValvula','ASC');
        $this->db->group_by('AperturaValvula');
        $query = $this->db->get();
        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;  
        
       
    }
    
    function getLargoBarrilHidrante()
    {
         $this->db->select('LargoBarril');
        $this->db->from('tbl_contra_incendio');
        $this->db->where('LargoBarril !=',"");
        $this->db->like('Nombre', 'KIT EXTENSION','both');        
        $this->db->order_by('LargoBarril','ASC');
        $this->db->group_by('LargoBarril');
        $query = $this->db->get();
        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;  
    }
    
    function getAperturaValvulaKit()
    {
        $this->db->select('AperturaValvula');
        $this->db->from('tbl_contra_incendio');
        $this->db->where('AperturaValvula !=',"");
        $this->db->like('Nombre', 'KIT REPARACION','both');        
        $this->db->order_by('AperturaValvula','ASC');
        $this->db->group_by('AperturaValvula');
        $query = $this->db->get();
        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;  
        
    }
    
    function getNombreValvulas()
    {
        $this->db->select('Nombre');
        $this->db->from('tbl_contra_incendio');
        $this->db->where('Nombre !=',"");
        $this->db->like('Nombre', 'COMP','both');        
        $this->db->order_by('Nombre','ASC');
        $this->db->group_by('Nombre');
        $query = $this->db->get();        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;  
        
    }
    
    function getDiamValvulas()
    {
        $this->db->select('LargoBarril');
        $this->db->from('tbl_contra_incendio');
        $this->db->where('LargoBarril !=',"");
        $this->db->like('Nombre', 'COMP','both');        
        $this->db->order_by('LargoBarril','ASC');
        $this->db->group_by('LargoBarril');
        $query = $this->db->get();        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;  
    }
    
    function getTipoJuntaValvula()
    {
        $this->db->select('TipoJunta');
        $this->db->from('tbl_contra_incendio');
        $this->db->where('TipoJunta !=',"");
        $this->db->like('Nombre', 'COMP','both');        
        $this->db->order_by('TipoJunta','ASC');
        $this->db->group_by('TipoJunta');
        $query = $this->db->get();        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;  
        
    }
public function my_escapeshellarg($datos) {
         $datos = str_replace('\'', '\\\'', $datos);
        return '\''.$datos.'\'';                
      }
    function getProductContraIncendio($data = array())
    {
        
       $this->db->select('tbl_contra_incendio.*,tbl_codigos.Precio,tbl_codigos.Existencia,tbl_codigos.TipoMoneda');
       $this->db->from('tbl_contra_incendio');
       $this->db->join('tbl_codigos', 'tbl_contra_incendio.SKU = tbl_codigos.SKU', 'LEFT');
//       $this->db->like('Nombre', 'HIDRANTE 3V','both');
//       $this->db->or_like('Nombre', 'HIDRANTE 2V','both');
       
       if($data['valvula'] != ""){
           $this->db->like('tbl_contra_incendio.TamanoValvula', $data['valvula'],'both');
       }
       if($data['vias'] != ""){
        $this->db->like('tbl_contra_incendio.NumeroVias', $data['vias'],'both');
       }
       if($data['barril'] != ""){
                $this->db->like('tbl_contra_incendio.LargoBarril', $data['barril'] ,'both');
       }
       if($data['junta'] != ""){
                 $this->db->like('tbl_contra_incendio.TipoJunta', $data['junta'],'both');     
       }
            
     
                
        $this->db->order_by('tbl_contra_incendio.id','ASC');
        $query = $this->db->get();
        return ($query->num_rows() > 0)? $query->result_array():FALSE;
        
    }
  
    function getProductExtHidrante($data = array())
    {
        
       $this->db->select('tbl_contra_incendio.*,tbl_codigos.Precio,tbl_codigos.Existencia,tbl_codigos.TipoMoneda');
       $this->db->from('tbl_contra_incendio');
       $this->db->join('tbl_codigos', 'tbl_contra_incendio.SKU = tbl_codigos.SKU', 'LEFT');
       $this->db->like('tbl_contra_incendio.Nombre', 'KIT EXTENSION','both');
       
       if($data['valvula'] != ""){
           $this->db->like('tbl_contra_incendio.AperturaValvula', $data['valvula'],'both');
       }
       if($data['barril'] != ""){
           $this->db->like('tbl_contra_incendio.LargoBarril', $data['barril'] ,'both');
       }
        $this->db->order_by('tbl_contra_incendio.id','ASC');
        $query = $this->db->get();
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
        
    }
    
    function getProductKit($data = array())
    {
        
       $this->db->select('tbl_contra_incendio.*,tbl_codigos.Precio,tbl_codigos.Existencia,tbl_codigos.TipoMoneda');
       $this->db->from('tbl_contra_incendio');
       $this->db->join('tbl_codigos', 'tbl_contra_incendio.SKU = tbl_codigos.SKU', 'LEFT');
       $this->db->like('tbl_contra_incendio.Nombre', 'KIT REPARACION','both');
       
       if($data['valvula'] != ""){
           $this->db->like('tbl_contra_incendio.AperturaValvula', $data['valvula'],'both');
       }     
        $this->db->order_by('tbl_contra_incendio.id','ASC');
        $query = $this->db->get();
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
        
    }
    
    function getProductValvula($data = array())
    {
        
       $this->db->select('tbl_contra_incendio.*,tbl_codigos.Precio,tbl_codigos.Existencia,tbl_codigos.TipoMoneda');
       $this->db->from('tbl_contra_incendio');
       $this->db->join('tbl_codigos', 'tbl_contra_incendio.SKU = tbl_codigos.SKU', 'LEFT');
       $this->db->like('tbl_contra_incendio.Nombre', 'COMP','both');
       
       if($data['valvula'] != ""){
           $this->db->like('tbl_contra_incendio.Nombre', $data['valvula'],'both');
       }
       if($data['diametro'] != ""){
                $this->db->like('tbl_contra_incendio.LargoBarril', $data['diametro'],'both');
       }
       if($data['junta'] != ""){
                $this->db->like('tbl_contra_incendio.TipoJunta', $data['junta'],'both');
       }
        $this->db->order_by('tbl_contra_incendio.id','ASC');
        $query = $this->db->get();
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
        
    }
    
    
    
}