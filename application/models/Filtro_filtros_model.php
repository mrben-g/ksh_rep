<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Filtro_filtros_model extends CI_Model{
    
    /*FILTRO CANASTA*/
    function getTamanoFiltro()
    {
        $this->db->select('Tamanio');
        $this->db->from('t_filtros');
        $this->db->where('Tamanio !=',"");
        $this->db->order_by('idFiltro','ASC');
        $this->db->group_by('Tamanio');
        $query = $this->db->get();
        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
    }
    
    function getProductoFiltroCanasta($data = array())
    {        
       $this->db->select('t_filtros.*,tbl_codigos.Precio,tbl_codigos.Existencia,tbl_codigos.TipoMoneda');
       $this->db->from('t_filtros');       
       $this->db->join('tbl_codigos', 't_filtros.SKU = tbl_codigos.SKU', 'LEFT');
       
       if($data['tamano'] != ""){
           $this->db->like('t_filtros.Tamanio', $data['tamano'] ,'after');
       } 
       
        $this->db->order_by('t_filtros.idFiltro','ASC');
        $query = $this->db->get();
        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
        
    }
  

    
}

?>