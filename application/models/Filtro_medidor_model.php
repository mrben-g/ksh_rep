<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Filtro_medidor_model extends CI_Model{
    
    /*Tamano Residencial*/
    function getTamanoResidencial()
    {
        $this->db->select('Tamano');
        $this->db->from('t_medidores');
        $this->db->where('Tamano !=',"");
        $this->db->like('Medidor', '1');
        $this->db->order_by('id','ASC');
        $this->db->group_by('Tamano');
        $query = $this->db->get();
        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
    }
    /*Rosca residencial*/
    function getTipoRoscaResidencial($data = array())
    {
        
        $this->db->select('TipoRosca');
        $this->db->from('t_medidores');
        $this->db->where('TipoRosca !=',"");
        $this->db->like('Medidor', '1');
        if($data['tamano'] != ""){
           $this->db->like('Tamano', $data['tamano'] ,'after');
       } 
        $this->db->order_by('id','ASC');
        $this->db->group_by('TipoRosca');
        $query = $this->db->get();
        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
    
    }
    /*Material Residencial*/
    function getMaterialResidencial($data = array())
    {
       $this->db->select('Material');
        $this->db->from('t_medidores');
        $this->db->where('Material !=',"");
        $this->db->like('Medidor', '1');
        if($data['tamano'] != ""){
           $this->db->like('Tamano', $data['tamano']);
       } 
       if($data['rosca'] != ""){
           $this->db->like('TipoRosca', $data['rosca']);
       } 
       
        $this->db->order_by('id','ASC');
        $this->db->group_by('Material');
        $query = $this->db->get();
        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;    
    }
    
    /*TIPO MEDIDOR RESIDENCIAL*/
    function getMedidorResidencial($data = array())
    {
       $this->db->select('TipoMedidor');
        $this->db->from('t_medidores');
        $this->db->where('TipoMedidor !=',"");
        $this->db->like('Medidor', '1');
        
        if($data['tamano'] != ""){
           $this->db->like('Tamano', $data['tamano']);
       } 
       if($data['rosca'] != ""){
           $this->db->like('TipoRosca', $data['rosca']);
       } 
       if($data['material'] != ""){
           $this->db->like('Material', $data['material']);
       } 
        $this->db->order_by('id','ASC');
        $this->db->group_by('TipoMedidor');
        
        $query = $this->db->get();        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;    
    }
    
    /*PRODUCTOS RESIDENCIALES*/
    function getProductoResidencial($data = array())
    {        
       $this->db->select('t_medidores.*,tbl_codigos.Precio,tbl_codigos.Existencia,tbl_codigos.TipoMoneda');
       $this->db->from('t_medidores');       
       $this->db->join('tbl_codigos', 't_medidores.SKU = tbl_codigos.SKU', 'LEFT');
       
       if($data['tamano'] != ""){
           $this->db->like('t_medidores.Tamano', $data['tamano']);
       } 
       if($data['rosca'] != ""){
           $this->db->like('t_medidores.TipoRosca', $data['rosca']);
       } 
       if($data['material'] != ""){
           $this->db->like('t_medidores.Material', $data['material']);
       } 
       if($data['medidor'] != ""){
           $this->db->like('t_medidores.TipoMedidor', $data['medidor']);
       } 
       
        $this->db->order_by('t_medidores.id','ASC');
        $query = $this->db->get();
        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
        
    }
    
    
    
     /*Tamano Comercial*/
    function getTamanoComercial()
    {
        $this->db->select('Tamano');
        $this->db->from('t_medidores');
        $this->db->where('Tamano !=',"");
        $this->db->like('Medidor', '2');        
        $this->db->order_by('id','ASC');
        $this->db->group_by('Tamano');
        $query = $this->db->get();
        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
    }
    
       /*Rosca Comercial*/
    function getTipoRoscaComercial($data = array())
    {
        
        $this->db->select('TipoRosca');
        $this->db->from('t_medidores');
        $this->db->where('TipoRosca !=',"");
        $this->db->like('Medidor', '2');
        
        if($data['tamano'] != ""){
           $this->db->like('Tamano', $data['tamano']);
       } 
        $this->db->order_by('id','ASC');
        $this->db->group_by('TipoRosca');
        $query = $this->db->get();
        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
    
    }
    /*Material Comercial*/
    function getMaterialComercial($data = array())
    {
       $this->db->select('Material');
        $this->db->from('t_medidores');
        $this->db->where('Material !=',"");
        $this->db->like('Medidor', '2');
        if($data['tamano'] != ""){
           $this->db->like('Tamano', $data['tamano']);
       } 
       if($data['rosca'] != ""){
           $this->db->like('TipoRosca', $data['rosca']);
       } 
       
        $this->db->order_by('id','ASC');
        $this->db->group_by('Material');
        $query = $this->db->get();
        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;    
    }
    
    /*TIPO MEDIDOR COmercial*/
    function getMedidorComercial($data = array())
    {
       $this->db->select('TipoMedidor');
        $this->db->from('t_medidores');
        $this->db->where('TipoMedidor !=',"");
        $this->db->like('Medidor', '2');
        
        if($data['tamano'] != ""){
           $this->db->like('Tamano', $data['tamano'] ,'after');
       } 
       if($data['rosca'] != ""){
           $this->db->like('TipoRosca', $data['rosca']);
       } 
       if($data['material'] != ""){
           $this->db->like('Material', $data['material']);
       } 
        $this->db->order_by('id','ASC');
        $this->db->group_by('TipoMedidor');
        
        $query = $this->db->get();        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;    
    }
    
    /*PRODUCTOS Comercial*/
    function getProductoComercial($data = array())
    {        
       $this->db->select('t_medidores.*,tbl_codigos.Precio,tbl_codigos.Existencia,tbl_codigos.TipoMoneda');
       $this->db->from('t_medidores');       
       $this->db->join('tbl_codigos', 't_medidores.SKU = tbl_codigos.SKU', 'LEFT');
       $this->db->like('Medidor', '2');
       
       if($data['tamano'] != ""){
           $this->db->like('t_medidores.Tamano', $data['tamano']);
       } 
       if($data['rosca'] != ""){
           $this->db->like('t_medidores.TipoRosca', $data['rosca'],'both');
       } 
       if($data['material'] != ""){
           $this->db->like('t_medidores.Material', $data['material']);
       } 
       if($data['medidor'] != ""){
           $this->db->like('t_medidores.TipoMedidor', $data['medidor']);
       } 
       
        $this->db->order_by('t_medidores.id','ASC');
        $query = $this->db->get();
        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
        
    }
    
   
  

    
}

?>