<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Process_model extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  public function insert($table = '', $data = array())
  {
    return $this->db->insert($table, $data);
  }
  
  public function clear($table = '')
  {
    $this->db->from($table); 
    $this->db->truncate(); 
  }
  
  public function update($table = '', $data = array(),$campo = '', $id = 0)
  {
      $this->db->where($campo, $id);
      return $this->db->update($table, $data);
  }

  public function update_no_w($table = '', $data = array())
  {
     return $this->db->update($table, $data);
  }

  public function get_all($table = '')
  {
    // $this->output->enable_profiler(TRUE);
    $this->db->select('*');
    $this->db->from($table);
    $query = $this->db->get();
    return $query->result();
  }
  

    public function get_cliente($correo = '') 
  { 
      $this->db->select('*'); 
      $this->db->from('t_clients'); 
      $this->db->where('email', $correo); 
      $query = $this->db->get(); 
      //return $query->row(); 
      if($query->num_rows() > 0){
        return $query->row();        
      }else{
        return 0;
    }
  }
 
  public function update_pwd($id = 0,$pass='')
  {
        $pass = $this->encryption->encrypt($pass);
        $this->db->where('IdCliente', $id);
        return $this->db->update('t_clients',array('password'=>$pass));
  }

}
