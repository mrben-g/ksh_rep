<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perfil_usuario_update_model extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  public function update_data($data = array())
  {
    $valid = false;
    $mensaje = '';
    $redirect = '';
    $result = array();
    
    
    $user = $this->update_user($_SESSION['login']['id'],$data);
    if (is_null($user)) {
      $valid = false;
      $mensaje = 'No existe el usuario';
    }else {

        $valid = true;
        $mensaje = '';
        $redirect = base_url('perfil');
        $result['redirect'] = $redirect;

    }
    
    $result['valid'] = $valid;
    $result['mensaje'] = $mensaje;
    return $result;
  }

  public function update_user($id = 0,$data = array())
  {
     $this->db->where('IdCliente', $id);
     return $this->db->update('t_clients',$data);
  }

    public function getDatosUsuario($id = 0)
  {
    $valid = false;
    $this->db->select('*');
    $this->db->from('t_clients');
    $this->db->where('IdCliente', $id);
    $query = $this->db->get();
    
    if (is_null($query->row())){
         $valid = false;
    }else{
          $valid = true;
          $result['datos'] = $query->row();
      }
   // return $query->row();
    $result['valid'] = $valid;    
    return $result;
  }

}
