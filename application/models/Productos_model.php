<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Productos_model extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  public function get_records($start = 1, $length = 1, $column_order, $column_direction, $search = false)
  {
    $start = ((($start > 0 ? $start : 1) - 1) * $length);

		$query = $this->db
			      ->from('t_producto')
			      ->limit($length, $start)
			      ->order_by($column_order, $column_direction);

		if (!empty($search))
		{
			$search_terms = explode(" ", $search['value']);

			foreach ($search_terms as $term)
			{
				if (!empty($term))
				{
					$query = $query->or_like('t_producto.Nombre', $term, 'both');				       
                                                                                             
                   
				}
			}
		}
		$query = $query->get();

		return $query->result();
  }

  public function count_productos()
  {
    $this->db->select('idProducto');
    $this->db->from('t_producto');
    $query = $this->db->get();
    return $query->num_rows();
  }

  public function get_all()
  {
    $this->db->select('*');
    $this->db->from('t_producto');
//    $this->db->where('Estatus', '1');
    $query = $this->db->get();
    return $query->result();
  }

  public function get($id = 0)
  {
    $this->db->select('*');
    $this->db->from('t_producto');
    $this->db->where('IdProducto', $id);
    $query = $this->db->get();
    return $query->row();
  }
  
  public function search($filter = '')
  {
    // $this->output->enable_profiler(TRUE);
    $this->db->select('*');
    $this->db->from('t_producto');
    if($filter !== ''){
      $this->db->group_start();
      $this->db->like('t_producto.Nombre', $filter, 'both');
      $this->db->group_end();
    }
    $query = $this->db->get();    
    //print_r($query);$this->output->enable_profiler(TRUE);
     
    if($query->num_rows() > 0 || $query->num_rows() != 'NULL'){
        return $query->result();
        
    }else{        
    $this->db->select('*');
    $this->db->from('tbl_contra_incendio');
    if ($filter !== '') {
      $this->db->group_start();
      $this->db->like('tbl_contra_incendio.Nombre', $filter, 'both');
      $this->db->group_end();
   }
    $query = $this->db->get();    
    if($query->num_rows() > 0){
      return $query->result();
    
    }else{   
    $this->db->select('*');
    $this->db->from('t_diametro_derivacion');
    if ($filter !== '') {
      $this->db->group_start();
      $this->db->like('t_diametro_derivacion.Nombre', $filter, 'both');
      $this->db->group_end();
   }
    $query = $this->db->get();
    
    if($query->num_rows() > 0){
     return $query->result();
    
    }else{ 
    $this->db->select('*');
    $this->db->from('t_alcantarillado');
    if ($filter !== '') {
      $this->db->group_start();
      $this->db->like('t_alcantarillado.Nombre', $filter, 'both');
      $this->db->group_end();
   }
    $query = $this->db->get();
    if($query->num_rows() > 0){
      return $query->result();
      
    }else{       
    $this->db->select('*');
    $this->db->from('t_filtros');
    if ($filter !== '') {
      $this->db->group_start();
      $this->db->like('t_filtros.Nombre', $filter, 'both');
      $this->db->group_end();
   }
     $query = $this->db->get();
    if($query->num_rows() > 0){
      return $query->result();
    
    }else{        
    $this->db->select('*');
    $this->db->from('t_medidores');
    if ($filter !== '') {
      $this->db->group_start();
      $this->db->like('t_medidores.Nombre', $filter, 'both');
      $this->db->group_end();
   }
    $query = $this->db->get();
    if($query->num_rows() > 0){
      return $query->result();
   }else{
        
    }
   }
  }
 }  
}
}   
    
   
  }
  
  public function get_dollar()
  {
    $this->db->select('PrecioPesos');
    $this->db->from('tbl_conversion_dollar');
    $query = $this->db->get();
    return $query->result();
  }
  
  
  
  
}