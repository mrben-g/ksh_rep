<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Post extends CI_Model{

    function getRows($params = array(),$filter = '')
    {
       
       $this->db->select('t_producto.*,t_diametros.Nominal,t_diametros.Rango,t_materiales.IdMaterial,tbl_codigos.Precio,tbl_codigos.Existencia,tbl_codigos.TipoMoneda');
       $this->db->from('t_producto');
       $this->db->join('tbl_codigos', 't_producto.SKU = tbl_codigos.SKU', 'LEFT');
       $this->db->join('t_diametros', 't_producto.RangoDiametro = t_diametros.Rango', 'LEFT');
       $this->db->join('t_materiales', 't_producto.MaterialOreja = t_materiales.Material', 'LEFT');  
       
       if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
            $this->db->like('t_producto.Nombre', $filter, 'both');
            $this->db->limit($params['limit'],$params['start']);
        }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
            $this->db->like('t_producto.Nombre', $filter, 'both');
            $this->db->limit($params['limit']);
        }else{
        $this->db->like('t_producto.Nombre', $filter, 'both');
        }
        $this->db->order_by('t_producto.IdProducto','ASC');
        $query = $this->db->get();
        
        if($query->num_rows() > 0 || $query->num_rows() != 'NULL'){         
          return ($query->num_rows() > 0)?$query->result_array():FALSE;
     
     }else{
         
    $this->db->select('tbl_contra_incendio.*,tbl_codigos.Precio,tbl_codigos.Existencia,tbl_codigos.TipoMoneda');
    $this->db->from('tbl_contra_incendio');
    $this->db->join('tbl_codigos', 'tbl_contra_incendio.SKU = tbl_codigos.SKU', 'LEFT');
  
    if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
            $this->db->like('tbl_contra_incendio.Nombre', $filter, 'both');
            $this->db->limit($params['limit'],$params['start']);
    
    }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
    
        $this->db->like('tbl_contra_incendio.Nombre', $filter, 'both');
        $this->db->limit($params['limit']);
        
        
    }else{
        $this->db->like('tbl_contra_incendio.Nombre', $filter, 'both');
        }
        $this->db->order_by('tbl_contra_incendio.id','ASC');
        $query = $this->db->get();
        
        if($query->num_rows() > 0){
        return ($query->num_rows() > 0)?$query->result_array():FALSE;    
    }else{
        
        $this->db->select('t_diametro_derivacion.*,tbl_codigos.Precio,tbl_codigos.Existencia,tbl_codigos.TipoMoneda');
        $this->db->from('t_diametro_derivacion');
        $this->db->join('tbl_codigos', 't_diametro_derivacion.SKU = tbl_codigos.SKU', 'LEFT');
        
        if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
            $this->db->like('t_diametro_derivacion.Nombre', $filter, 'both');
            $this->db->limit($params['limit'],$params['start']);
        }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
            $this->db->like('t_diametro_derivacion.Nombre', $filter, 'both');
            $this->db->limit($params['limit']);
        }else{
        $this->db->like('t_diametro_derivacion.Nombre', $filter, 'both');
        }
        $this->db->order_by('t_diametro_derivacion.id','ASC');
        $query = $this->db->get();
    if($query->num_rows() > 0){
      return ($query->num_rows() > 0)?$query->result_array():FALSE;
    
    }else{
 
    $this->db->select('t_alcantarillado.*,tbl_codigos.Precio,tbl_codigos.Existencia,tbl_codigos.TipoMoneda');
    $this->db->from('t_alcantarillado');
    $this->db->join('tbl_codigos', 't_alcantarillado.SKU = tbl_codigos.SKU', 'LEFT');
    if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
            $this->db->like('t_alcantarillado.Nombre', $filter, 'both');
            $this->db->limit($params['limit'],$params['start']);
        }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
            $this->db->like('t_alcantarillado.Nombre', $filter, 'both');
            $this->db->limit($params['limit']);
        }else{
        $this->db->like('t_alcantarillado.Nombre', $filter, 'both');
        }
        $this->db->order_by('t_alcantarillado.id','ASC');
        $query = $this->db->get();
    if($query->num_rows() > 0){
    return ($query->num_rows() > 0)?$query->result_array():FALSE;
    
    }else{
        
    $this->db->select('t_filtros.*,tbl_codigos.Precio,tbl_codigos.Existencia,tbl_codigos.TipoMoneda');
    $this->db->from('t_filtros');
    $this->db->join('tbl_codigos', 't_filtros.SKU = tbl_codigos.SKU', 'LEFT');    
     if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
            $this->db->like('t_filtros.Nombre', $filter, 'both');
            $this->db->limit($params['limit'],$params['start']);
        }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
            $this->db->like('t_filtros.Nombre', $filter, 'both');
            $this->db->limit($params['limit']);
        }else{
        $this->db->like('t_filtros.Nombre', $filter, 'both');
        }
        $this->db->order_by('t_filtros.idFiltro','ASC');
         $query = $this->db->get();
        if($query->num_rows() > 0){
    return ($query->num_rows() > 0)?$query->result_array():FALSE;
    
    }else{
        
    $this->db->select('t_medidores.*,tbl_codigos.Precio,tbl_codigos.Existencia,tbl_codigos.TipoMoneda');
    $this->db->from('t_medidores');
    $this->db->join('tbl_codigos', 't_medidores.SKU = tbl_codigos.SKU', 'LEFT');
    
    if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
            $this->db->like('t_medidores.Nombre', $filter, 'both');
            $this->db->limit($params['limit'],$params['start']);
        }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
            $this->db->like('t_medidores.Nombre', $filter, 'both');
            $this->db->limit($params['limit']);
        }else{
        $this->db->like('t_medidores.Nombre', $filter, 'both');
        }
        $this->db->order_by('t_medidores.id','ASC');
    $query = $this->db->get();
      if($query->num_rows() > 0){
     return ($query->num_rows() > 0)?$query->result_array():FALSE;
    
    }else{
        
    }
   }
    }
        
    }      
   }
  } 
        
        
        
//        return ($query->num_rows() > 0)?$query->result_array():FALSE;
        
        
        
        
    }

}