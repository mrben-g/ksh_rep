<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Nueva_venta_model extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  public function insert_venta($data = array())
  {
    $this->db->insert('t_ventas', $data);
    $insert_id = $this->db->insert_id();
    return  $insert_id;
  }
  
  public function insert_venta_detalle($data = array())
  {
    return $this->db->insert('t_ventas_detalle', $data);
  }

  

}
