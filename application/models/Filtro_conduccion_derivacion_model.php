<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Filtro_conduccion_derivacion_model extends CI_Model{

    function getDiamTEEPartida()
    {
        $this->db->select('DiametroNominal');
        $this->db->from('t_diametro_derivacion');
        $this->db->where('DiametroNominal !=',"");
        $this->db->like('Nombre', 'TEE','both');
        $this->db->order_by('id','ASC');
        $this->db->group_by('DiametroNominal');
        $query = $this->db->get();
        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
    }
    
     function getRangTEEPartida()
    {
        $this->db->select('RangoDiametro');
        $this->db->from('t_diametro_derivacion');
        $this->db->where('RangoDiametro !=',"");
        $this->db->like('Nombre', 'TEE','both');
        $this->db->order_by('id','ASC');
        $this->db->group_by('RangoDiametro');
        $query = $this->db->get();
        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
    }
    
    function getRangoPartida($data = array())
    {
        $this->db->select('RangoDiametro');
        $this->db->from('t_diametro_derivacion');
        $this->db->where('RangoDiametro !=',"");
        $this->db->like('Nombre', 'TEE','both');
        
       if($data['modelo'] != ""){           
           if($data['modelo'] == '3480AI'){
           $this->db->like('Nombre', 'TEE PARTIDA 3480AI POWERSEAL','both');
           }
           if($data['modelo'] == '3490AI'){
            $this->db->like('Nombre', 'TEE PARTIDA 3490AI POWERSEAL','both');     
           }
       }       
       if($data['diametro'] != ""){
                $this->db->like('DiametroNominal', $data['diametro'] ,'both');
       }
       if($data['brida'] != ""){
        $this->db->like('Brida', $data['brida'],'both');
       }
        $this->db->order_by('id','ASC');
        $this->db->group_by('RangoDiametro');
        $query = $this->db->get();
        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
        
    }
    
    function getBridaPartida($data = array())
    {
        $this->db->select('Brida');
        $this->db->from('t_diametro_derivacion');
        $this->db->where('Brida !=',"");
        $this->db->like('Nombre', 'TEE','both');
        
       if($data['modelo'] != ""){           
           if($data['modelo'] == '3480AI'){
           $this->db->like('Nombre', 'TEE PARTIDA 3480AI POWERSEAL','both');
           }
           if($data['modelo'] == '3490AI'){
            $this->db->like('Nombre', 'TEE PARTIDA 3490AI POWERSEAL','both');     
           }
       }       
       if($data['diametro'] != ""){
                $this->db->like('DiametroNominal', $data['diametro'] ,'both');
       }
       if($data['rango'] != ""){
        $this->db->like('RangoDiametro', $data['rango'],'both');
       }
        $this->db->order_by('id','ASC');
        $this->db->group_by('Brida');
        $query = $this->db->get();
        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
        
    }
    
    function getProductoPartida($data = array())
    {
        
       $this->db->select('t_diametro_derivacion.*,tbl_codigos.Precio,tbl_codigos.Existencia,tbl_codigos.TipoMoneda');
       $this->db->from('t_diametro_derivacion');       
       $this->db->join('tbl_codigos', 't_diametro_derivacion.SKU = tbl_codigos.SKU', 'LEFT');
       $this->db->like('t_diametro_derivacion.Nombre', 'TEE','both');
       
      if($data['modelo'] != ""){           
           if($data['modelo'] == '3480AI'){
           $this->db->like('t_diametro_derivacion.Nombre', 'TEE PARTIDA 3480AI POWERSEAL','both');
           }
           if($data['modelo'] == '3490AI'){
            $this->db->like('t_diametro_derivacion.Nombre', 'TEE PARTIDA 3490AI POWERSEAL','both');     
           }
       }
       if($data['diametro'] != ""){
                $this->db->like('t_diametro_derivacion.DiametroNominal', $data['diametro'] ,'both');
       }
       if($data['rango'] != ""){
        $this->db->like('t_diametro_derivacion.RangoDiametro', $data['rango'],'both');
       }
       if($data['brida'] != ""){
                 $this->db->like('t_diametro_derivacion.Brida', $data['brida'],'both');     
       }
               
        $this->db->order_by('t_diametro_derivacion.id','ASC');
        $query = $this->db->get();
        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
        
    }
  /*COLLARINES*/
    
     function getDiamCollarin($data = array())
    {
        $this->db->select('DiametroNominal');
        $this->db->from('t_diametro_derivacion');
        $this->db->where('DiametroNominal !=',"");
    //    $this->db->like('NombrePartida', 'TEE','both');
        
       if($data['modelo'] != ""){  
              $this->db->like('Nombre', $data['modelo'],'both');
         /*  if($data['modelo'] == '3411'){
           $this->db->like('NombrePartida', $data['modelo'],'both');
           }
           if($data['modelo'] == '3415'){
            $this->db->like('NombrePartida', 'TEE PARTIDA 3490AI POWERSEAL','both');     
           }
               if($data['modelo'] == '3417'){
            $this->db->like('NombrePartida', 'TEE PARTIDA 3490AI POWERSEAL','both');     
           }
               if($data['modelo'] == '3421'){
            $this->db->like('NombrePartida', 'TEE PARTIDA 3490AI POWERSEAL','both');     
           }*/
       }       
     
        $this->db->order_by('id','ASC');
        $this->db->group_by('DiametroNominal');
        $query = $this->db->get();
        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
        
    }
    
         function getRangoCollarin($data = array())
    {
        $this->db->select('RangoDiametro');
        $this->db->from('t_diametro_derivacion');
        $this->db->where('RangoDiametro !=',"");
    //    $this->db->like('NombrePartida', 'TEE','both');
        
       if($data['modelo'] != ""){  
          $this->db->like('Nombre', $data['modelo'],'both');      
       }
        if($data['diametro'] != ""){
          $this->db->like('DiametroNominal', $data['diametro'] ,'both');
       }
     
        $this->db->order_by('id','ASC');
        $this->db->group_by('RangoDiametro');
        $query = $this->db->get();
        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
        
    }
    
    function getSalidaCollarin($data = array())
    {
        $this->db->select('Brida');
        $this->db->from('t_diametro_derivacion');
        $this->db->where('Brida !=',"");
      //  $this->db->like('NombrePartida', 'TEE','both');
        
       if($data['modelo'] != ""){  
          $this->db->like('Nombre', $data['modelo'],'both');      
       }
       if($data['diametro'] != ""){
                $this->db->like('DiametroNominal', $data['diametro'] ,'both');
       }
       if($data['rango'] != ""){
        $this->db->like('RangoDiametro', $data['rango'],'both');
       }
        $this->db->order_by('id','ASC');
        $this->db->group_by('Brida');
        $query = $this->db->get();
        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
        
    }
    
    function getRoscaCollarin($data = array())
    {
        $this->db->select('Material');
        $this->db->from('t_diametro_derivacion');
        $this->db->where('Material !=',"");
      //  $this->db->like('NombrePartida', 'TEE','both');
        
       if($data['modelo'] != ""){  
          $this->db->like('Nombre', $data['modelo'],'both');      
       }
       if($data['diametro'] != ""){
                $this->db->like('DiametroNominal', $data['diametro'] ,'both');
       }
       if($data['rango'] != ""){
        $this->db->like('RangoDiametro', $data['rango'],'both');
       }
       if($data['salida'] != ""){
        $this->db->like('Brida', $data['salida'],'both');
       }
       
        $this->db->order_by('id','ASC');
        $this->db->group_by('Material');
        $query = $this->db->get();
        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
        
    }
    
    function getProductoCollarin($data = array())
    {
        
       $this->db->select('t_diametro_derivacion.*,tbl_codigos.Precio,tbl_codigos.Existencia,tbl_codigos.TipoMoneda');
       $this->db->from('t_diametro_derivacion');       
       $this->db->join('tbl_codigos', 't_diametro_derivacion.SKU = tbl_codigos.SKU', 'LEFT');
       
       if($data['modelo'] != ""){       
           $this->db->like('t_diametro_derivacion.Nombre',$data['modelo'],'both');       
       }
       if($data['diametro'] != ""){
                $this->db->like('t_diametro_derivacion.DiametroNominal', $data['diametro'] ,'both');
       }
       if($data['rango'] != ""){
        $this->db->like('t_diametro_derivacion.RangoDiametro', $data['rango'],'both');
       }
       if($data['salida'] != ""){
                 $this->db->like('t_diametro_derivacion.Brida', $data['salida'],'both');     
       }
       if($data['rosca'] != ""){
                 $this->db->like('t_diametro_derivacion.Material', $data['rosca'],'both');     
       }        
       
        $this->db->order_by('t_diametro_derivacion.id','ASC');
        $query = $this->db->get();
        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
        
    }
    
    
    /*JUNTAS UNIVERSALES*/
    function getDiamJuntaUniversal($data = array())
    {
        $this->db->select('DiametroNominal');
        $this->db->from('t_diametro_derivacion');
        $this->db->where('DiametroNominal !=',"");
        $this->db->like('Nombre', 'JUNTA','both');
        
       if($data['material'] != ""){  
           
           if($data['material'] == 1){
           $this->db->like('Material', 'Hierro ductil con epoxica','both');       
           }
           if($data['material'] == 2){
           $this->db->like('Material', 'Acero 100% Inoxidable','both');       
           }
       }       
     
        $this->db->order_by('id','ASC');
        $this->db->group_by('DiametroNominal');
        $query = $this->db->get();
        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
        
    }
    
    function getRangoJuntaUniversal($data = array())
    {
        $this->db->select('RangoDiametro');
        $this->db->from('t_diametro_derivacion');
        $this->db->where('RangoDiametro !=',"");
        $this->db->like('Nombre', 'JUNTA','both');
        
      if($data['material'] != ""){             
           if($data['material'] == 1){
           $this->db->like('Material', 'Hierro ductil con epoxica','both');       
           }
           if($data['material'] == 2){
           $this->db->like('Material', 'Acero 100% Inoxidable','both');       
           }
       }       
       
        if($data['diametro'] != ""){
          $this->db->like('DiametroNominal', $data['diametro'] ,'both');
       }
     
        $this->db->order_by('id','ASC');
        $this->db->group_by('RangoDiametro');
        $query = $this->db->get();
        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
        
    }
    
    function getBarrilJuntaUniversal($data = array())
    {
        $this->db->select('Brida');
        $this->db->from('t_diametro_derivacion');
        $this->db->where('Brida !=',"");
        $this->db->like('Nombre', 'JUNTA','both');
        
       if($data['material'] != ""){             
           if($data['material'] == 1){
           $this->db->like('Material', 'Hierro ductil con epoxica','both');       
           }
           if($data['material'] == 2){
           $this->db->like('Material', 'Acero 100% Inoxidable','both');       
           }
       }       
       if($data['diametro'] != ""){
                $this->db->like('DiametroNominal', $data['diametro'] ,'both');
       }
       if($data['rango'] != ""){
        $this->db->like('RangoDiametro', $data['rango'],'both');
       }
        $this->db->order_by('id','ASC');
        $this->db->group_by('Brida');
        $query = $this->db->get();
        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
        
    }
    
    function getProductoJuntaUniversal($data = array())
    {
        
       $this->db->select('t_diametro_derivacion.*,tbl_codigos.Precio,tbl_codigos.Existencia,tbl_codigos.TipoMoneda');
       $this->db->from('t_diametro_derivacion');       
       $this->db->join('tbl_codigos', 't_diametro_derivacion.SKU = tbl_codigos.SKU', 'LEFT');
       
       if($data['material'] != ""){             
           if($data['material'] == 1){
           $this->db->like('Material', 'Hierro ductil con epoxica','both');       
           }
           if($data['material'] == 2){
           $this->db->like('Material', 'Acero 100% Inoxidable','both');       
           }
       }       
       if($data['diametro'] != ""){
                $this->db->like('t_diametro_derivacion.DiametroNominal', $data['diametro'] ,'both');
       }
       if($data['rango'] != ""){
        $this->db->like('t_diametro_derivacion.RangoDiametro', $data['rango'],'both');
       }
       if($data['barril'] != ""){
                 $this->db->like('t_diametro_derivacion.Brida', $data['barril'],'both');     
       }
       
       
        $this->db->order_by('t_diametro_derivacion.id','ASC');
        $query = $this->db->get();
        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
        
    }
    
    /*ADAPTADORES BRIDADOS*/
    
    function getDiamAdaptadores($data = array())
    {
        $this->db->select('DiametroNominal');
        $this->db->from('t_diametro_derivacion');
        $this->db->where('DiametroNominal !=',"");
        $this->db->like('Nombre', 'ADAPT','both');
        
       if($data['modelo'] != ""){
           $this->db->like('Nombre',$data['modelo'],'both');       
 
       }       
     
        $this->db->order_by('id','ASC');
        $this->db->group_by('DiametroNominal');
        $query = $this->db->get();
        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
        
    }
    function getRangoAdaptadores($data = array())
    {
        $this->db->select('RangoDiametro');
        $this->db->from('t_diametro_derivacion');
        $this->db->where('RangoDiametro !=',"");
        $this->db->like('Nombre', 'ADAPT','both');
        
        if($data['modelo'] != ""){
           $this->db->like('Nombre',$data['modelo'],'both');  
       }
       
        if($data['diametro'] != ""){
          $this->db->like('DiametroNominal', $data['diametro'] ,'both');
       }
     
        $this->db->order_by('id','ASC');
        $this->db->group_by('RangoDiametro');
        $query = $this->db->get();
        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
        
    }
    
    function getProductoAdaptadores($data = array())
    {
        
       $this->db->select('t_diametro_derivacion.*,tbl_codigos.Precio,tbl_codigos.Existencia,tbl_codigos.TipoMoneda');
       $this->db->from('t_diametro_derivacion');       
       $this->db->join('tbl_codigos', 't_diametro_derivacion.SKU = tbl_codigos.SKU', 'LEFT');
       $this->db->like('Nombre', 'ADAPT','both');
       
       if($data['modelo'] != ""){
           $this->db->like('Nombre',$data['modelo'],'both');  
       }
       if($data['diametro'] != ""){
                $this->db->like('t_diametro_derivacion.DiametroNominal', $data['diametro'] ,'both');
       }
       if($data['rango'] != ""){
        $this->db->like('t_diametro_derivacion.RangoDiametro', $data['rango'],'both');
       }
      
       
       $this->db->order_by('t_diametro_derivacion.id','ASC');
       $query = $this->db->get();
       return ($query->num_rows() > 0)?$query->result_array():FALSE;
        
    }
   
     /*BRIDA RAPIDA*/
    function getBridaBR($data = array())
    {
        $this->db->select('Brida');
        $this->db->from('t_diametro_derivacion');
        $this->db->where('Brida !=',"");
        $this->db->like('Nombre', 'BRIDA','both');
        
       if($data['modelo'] != ""){
           
            if($data['modelo'] == '3531'){
           $this->db->like('Nombre', $data['modelo'],'both');
           $this->db->like('Nombre', 'P/HD','both');
           $this->db->like('Material', 'HIERRO DUCTIL','both');
           }
           if($data['modelo'] == '3532'){
            $this->db->like('Nombre',$data['modelo'],'both');     
            $this->db->like('Nombre', 'P/HD','both');
            $this->db->like('Material', 'HIERRO DUCTIL','both');
           }
           
           if($data['modelo'] == '3531AI'){
           $this->db->like('Nombre', '3531','both');
           $this->db->like('Nombre', 'P/A','both');
           $this->db->like('Material', 'ACERO','both');
           }
           if($data['modelo'] == '3532AI'){
            $this->db->like('Nombre','3532','both');     
            $this->db->like('Nombre', 'P/A','both');
            $this->db->like('Material', 'ACERO','both');
           }
       }       
   
        $this->db->order_by('id','ASC');
        $this->db->group_by('Brida');
        $query = $this->db->get();
        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
        
    }
    
    function getRangoBR($data = array())
    {
        $this->db->select('RangoDiametro');
        $this->db->from('t_diametro_derivacion');
        $this->db->where('RangoDiametro !=',"");
        $this->db->like('Nombre', 'BRIDA','both');
        
        if($data['modelo'] != ""){
           
            if($data['modelo'] == '3531'){
           $this->db->like('Nombre', $data['modelo'],'both');
           $this->db->like('Nombre', 'P/HD','both');
           $this->db->like('Material', 'HIERRO DUCTIL','both');
           }
           if($data['modelo'] == '3532'){
            $this->db->like('Nombre',$data['modelo'],'both');     
            $this->db->like('Nombre', 'P/HD','both');
            $this->db->like('Material', 'HIERRO DUCTIL','both');
           }
           
           if($data['modelo'] == '3531AI'){
           $this->db->like('Nombre', '3531','both');
           $this->db->like('Nombre', 'P/A','both');
           $this->db->like('Material', 'ACERO','both');
           }
           if($data['modelo'] == '3532AI'){
            $this->db->like('Nombre','3532','both');     
            $this->db->like('Nombre', 'P/A','both');
            $this->db->like('Material', 'ACERO','both');
           }
       }       
        if($data['brida'] != ""){
          $this->db->like('t_diametro_derivacion.Brida', $data['brida'],'both');
         }
     
        $this->db->order_by('id','ASC');
        $this->db->group_by('RangoDiametro');
        $query = $this->db->get();
        
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
        
    }
    
    function getProductoBridasRapidas($data = array())
    {
        
       $this->db->select('t_diametro_derivacion.*,tbl_codigos.Precio,tbl_codigos.Existencia,tbl_codigos.TipoMoneda');
       $this->db->from('t_diametro_derivacion');       
       $this->db->join('tbl_codigos', 't_diametro_derivacion.SKU = tbl_codigos.SKU', 'LEFT');
       $this->db->like('Nombre', 'BRIDA','both');
       
      if($data['modelo'] != ""){
           
            if($data['modelo'] == '3531'){
           $this->db->like('Nombre', $data['modelo'],'both');
           $this->db->like('Nombre', 'P/HD','both');
           $this->db->like('Material', 'HIERRO DUCTIL','both');
           }
           if($data['modelo'] == '3532'){
            $this->db->like('Nombre',$data['modelo'],'both');     
            $this->db->like('Nombre', 'P/HD','both');
            $this->db->like('Material', 'HIERRO DUCTIL','both');
           }
           
           if($data['modelo'] == '3531AI'){
           $this->db->like('Nombre', '3531','both');
           $this->db->like('Nombre', 'P/A','both');
           $this->db->like('Material', 'ACERO','both');
           }
           if($data['modelo'] == '3532AI'){
            $this->db->like('Nombre','3532','both');     
            $this->db->like('Nombre', 'P/A','both');
            $this->db->like('Material', 'ACERO','both');
           }
       }       
        if($data['brida'] != ""){
          $this->db->like('t_diametro_derivacion.Brida', $data['brida'],'both');
         }
     
       if($data['rango'] != ""){
        $this->db->like('t_diametro_derivacion.RangoDiametro', $data['rango'],'both');
       }
      
       
       $this->db->order_by('t_diametro_derivacion.id','ASC');
       $query = $this->db->get();
       return ($query->num_rows() > 0)?$query->result_array():FALSE;
        
    }
    
    
}