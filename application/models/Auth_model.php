<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_model extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  public function inicia_session($data = array())
  {
    $valid = false;
    $mensaje = 'Ocurrio un error desconocido';
    $redirect = '';
    $result = array();
    $user = $this->check_user($data);
    
    if (is_null($user)) {
      $valid = false;
      $mensaje = 'No existe el usuario';
    } else {
      $pass = $this->encryption->decrypt($user->password);
      if ($pass === $data['password']) {
        $valid = true;
        $mensaje = '';
        $redirect = base_url('');
        $result['redirect'] = $redirect;
        $result['datos'] = $user;
        if(!$user->Nombre){
        $_SESSION['login']['nombre'] = $user->email;
        }else{
            $_SESSION['login']['nombre'] = $user->Nombre;
        }
        $_SESSION['login']['id'] = $user->IdCliente;
        //$_SESSION['login']['rol']
      } else {
        $valid = false;
        $mensaje = 'Usuario o contraseña incorrectas';
      }

    }
    $result['valid'] = $valid;
    $result['mensaje'] = $mensaje;
    return $result;
  }

  public function check_user($data = array())
  {
    $this->db->select('*');
    $this->db->from('t_clients');
    $this->db->where('email', $data['email']);
    $query = $this->db->get();
    return $query->row();
  }

  public function insert($data = array())
  {
    return $this->db->insert('t_clients', $data);
  }

  public function update()
  {
      #code
  }

  public function get_all()
  {

  }

  public function get($id = 0)
  {
      #code
  }
  
  public function update_password($id = 0, $data = array())
  {
      $this->db->where('IdCliente', $id);
      return $this->db->update('t_clients',$data);
  }

}
